package simulator;

/*
 * Title:        GridSim Toolkit
 * Description:  GridSim (Grid Simulation) Toolkit for Modeling and Simulation
 *               of Parallel and Distributed Systems such as Clusters and Grids
 * License:      GPL - http://www.gnu.org/copyleft/gpl.html
 *
 */


import java.util.Collections;
import java.util.Iterator;
import gridsim.*;


/**
 * SpaceShared class is an allocation policy for GridResource that behaves
 * exactly like First Come First Serve (FCFS). This is a basic and simple
 * scheduler that runs each Gridlet to one Processing Element (PE).
 * If a Gridlet requires more than one PE, then this scheduler only assign
 * this Gridlet to one PE.
 *
 * @author       Manzur Murshed and Rajkumar Buyya
 * @author       Anthony Sulistio (re-written this class)
 * @author       Marcos Dias de Assuncao (has made some methods synchronized)
 * @since        GridSim Toolkit 2.2
 * @see gridsim.GridSim
 * @see gridsim.ResourceCharacteristics
 * @invariant $none
 */
public class PolicyLPT extends Policy{


    /**
     * Allocates a new SpaceShared object
     * @param resourceName    the GridResource entity name that will contain
     *                        this allocation policy
     * @param entityName      this object entity name
     * @throws Exception This happens when one of the following scenarios occur:
     *      <ul>
     *          <li> creating this entity before initializing GridSim package
     *          <li> this entity name is <tt>null</tt> or empty
     *          <li> this entity has <tt>zero</tt> number of PEs (Processing
     *              Elements). <br>
     *              No PEs mean the Gridlets can't be processed.
     *              A GridResource must contain one or more Machines.
     *              A Machine must contain one or more PEs.
     *      </ul>
     * @see gridsim.GridSim#init(int, Calendar, boolean, String[], String[],
     *          String)
     * @pre resourceName != null
     * @pre entityName != null
     * @post $none
     */
    public PolicyLPT(String resourceName, String entityName, SimulatorLog log_, String args) throws Exception
    {
        super(resourceName, entityName, log_);
        
        // ***************************************
        //		POLICY ARGUMENTS
        // ***************************************
        
        String arguments = args;
        while(arguments.length()>0){
        	if(arguments.startsWith("B")){
	        	this.AllowBackFilling = true;
	        	arguments = arguments.replaceAll("B", "");
	        
	        // Si no se han especificado correctamente los argumentos
	        }else{
	        	System.out.println("Bad LPT-policy arguments");
	        	log.addMessage("Bad LPT-policy arguments");
	        	log.close();
	        	System.exit(1);
	        }
        }
        System.out.println("LPT policy initialization:");
        if (this.AllowBackFilling == true){
        	System.out.println("\tLPT policy. BackFilling enabled");
        }else{
        	System.out.println("\tLPT policy. BackFilling disabled");
        }
        System.out.println("");
    }

    
    /**
     * Schedules a new Gridlet that has been received by the GridResource
     * entity.
     * @param   gl    a Gridlet object that is going to be executed
     * @param   ack   an acknowledgement, i.e. <tt>true</tt> if wanted to know
     *        whether this operation is success or not, <tt>false</tt>
     *        otherwise (don't care)
     * @pre gl != null
     * @post $none
     */
    public synchronized void gridletSubmit(Gridlet gl, boolean ack)
    {
        // update the current Gridlets in exec list up to this point in time
        updateGridletProcessing();
        
        ResJob rgl = new ResJob(gl);
        rgl.setNumClusters(this.resource_.getNumMachines());
        boolean success = false;
        
        log.addMessage(super.get_name() + ".gridletSubmit(): Trying to allocate gridlet " + gl.getGridletID() + " with " + gl.getNumPE()+ " tasks. There are " + this.resource_.getNumFreePE() + " PEs available.");
        
        if (gridletQueueList_.size() == 0){
	        if (gl.getNumPE() > this.resource_.getNumFreePE()){
	        	String userName = GridSim.getEntityName( gl.getUserID() );
	        	log.addMessage("\tJob " + gl.getGridletID() + " requires " + gl.getNumPE() + " PEs.");
	        	log.addMessage("\tThe system has only " + this.resource_.getNumFreePE() + " PEs available.");
	        	log.addMessage("\tJob " + gl.getGridletID()+" sent to the waiting queue.");
	            rgl.setGridletStatus(Gridlet.QUEUED);
	            gridletQueueList_.add(rgl);
	            
	        }else{
	        	log.addMessage("\tEnough free PEs. Allocate!");
		        // if there are available PE slots, then allocate immediately
		        if (gridletInExecList_.size() < super.totalPE_) {
		            success = allocatePEtoGridlet(rgl);
		        }
	        }
	
	        // sends back an ack if required
	        if (ack)
	        {
	            super.sendAck(GridSimTags.GRIDLET_SUBMIT_ACK, true,
	                          gl.getGridletID(), gl.getUserID()
	            );
	        }
        }else{
        	// I there are jobs in te queue, put this job to the end
        	log.addMessage("There are jobs waiting in the queue. Put Job " + rgl.getGridletID() + " in the queue.");
        	rgl.setGridletStatus(Gridlet.QUEUED);
            gridletQueueList_.add(rgl);
        }
    }



    ///////////////////////////// PRIVATE METHODS /////////////////////

    /**
     * Allocates the first Gridlet in the Queue list (if any) to execution list
     * @pre $none
     * @post $none
     */
    public void allocateQueueGridlet(){
    	
    	// LPT
    	// Re-order the queue
    	// Allocate the first job in the queue
    	
    	
    	log.addMessage("***************************");
    	log.addMessage("Re-order the queue.");
    	Collections.sort(this.gridletQueueList_, new WorkloadComparatorByGridletLengthReverse());
    	log.addMessage("Allocate first queued job");
    	
    	log.addMessage("Run list (" + gridletInExecList_.size() + "):");
    	Iterator<ResGridlet> iter = gridletInExecList_.iterator();
    	String str ="\t";
    	while(iter.hasNext()){
    		str = str + iter.next().getGridletID() + " ";
    	}
    	log.addMessage(str);
    	log.addMessage("--------------------------");
    	
    	log.addMessage("Queue list (" + gridletQueueList_.size() + "):");
    	iter = gridletQueueList_.iterator();
    	str ="\t";
    	while(iter.hasNext()){
    		str = str + iter.next().getGridletID() + " ";
    	}
    	log.addMessage(str);
    	log.addMessage("--------------------------");
    	
    	if(super.AllowBackFilling == false){
	    	// if there are many Gridlets in the QUEUE, then allocate a
	        // PE to the first Gridlet in the list since it follows FCFS
	        // (First Come First Serve) approach. Then removes the Gridlet from
	        // the Queue list
	    	while((resource_.getNumFreePE() > 0) && (gridletQueueList_.size()>0)){
	            ResJob obj = (ResJob) gridletQueueList_.get(0);
	
	            // allocate the Gridlet into an empty PE slot and remove it from
	            // the queue list
	            if(obj.getNumPE() > resource_.getNumFreePE()){
	            	break;
	            }
	            boolean success = allocatePEtoGridlet(obj);
	            if (success) {
	            	log.addMessage("Job " + obj.getGridletID()+ " allocated. Removed from queue list.");
	                gridletQueueList_.remove(obj);
	            }
	            else{
	            	break;
	            }
	    	}
	    
	    // BACKFILLING
    	}else{
    		
    		boolean advancing = false;
    		// If backfilling is enabled
    		Iterator it = gridletQueueList_.iterator();
    		while(it.hasNext()){
    			double max = findTimeLastRunningJobToFinish(gridletInExecList_);
    			log.addMessage("max: " + max);
    			ResJob rj = (ResJob)it.next();
    			if (advancing == false){
	    			if (rj.getNumPE() > resource_.getNumFreePE()){
	    				advancing = true;
	    				// Job can't be allocated
	    			}else{
	    				// If the job fits
	    				boolean success = allocatePEtoGridlet(rj);
	    	            if (success) {
	    	            	log.addMessage("Job " + rj.getGridletID()+ " allocated. Removed from queue list.");
	    	                //gridletQueueList_.remove(rj);
	    	                it.remove();
	    	            }else{
	    	            	advancing = true;
	    	            }
	    			}
    			}else{
    				// If we are trying to advance a job
    				if (rj.getNumPE() > resource_.getNumFreePE()){
	    				// Job can't be allocated
	    			}else{
	    				// check an estimated execution time for the job
	    				//double dur = ((rj.getGridletLength()/1000)*rj.getSigma()) + ((1-rj.getSigma())*(rj.getGridletLength()/1000));
	    				double dur = rj.getRemainingEstimatedExecTimeNotAllocated(machineRating_);
	    				
	    				// if the job does not slows the last runing, allocate it
	    				log.addMessage("Try advance Job " + rj.getGridletID() + " dur:" + dur + " max:" + max);
	    				if (dur <= max){
	    					boolean success = allocatePEtoGridlet(rj);
		    	            if (success) {
		    	            	log.addMessage("Job " + rj.getGridletID()+ " advanced. Removed from queue list.");
		    	                //gridletQueueList_.remove(rj);
		    	            	it.remove();
		    	            }
	    				}
	    			}
    			}
    			//advancing = false;
    		}
    		
    	}
    }


    
    /**
     * Allocates a Gridlet into a free PE and sets the Gridlet status into
     * INEXEC and PE status into busy afterwards
     * @param rgl  a ResGridlet object
     * @return <tt>true</tt> if there is an empty PE to process this Gridlet,
     *         <tt>false</tt> otherwise
     * @pre rgl != null
     * @post $none
     */
    /*
    public boolean allocatePEtoGridlet(ResJob rgl){
        MachineList list = this.resource_.getMachineList();
        // Check if there are enough free PEs
        if (super.resource_.getNumFreePE() < rgl.getNumPE()){
        	return false;
        }
        
        try{
	    	// Will try to locate machines with enough free PEs
	        rgl.setGridletStatus(Gridlet.INEXEC);   // change Gridlet status
	        //System.out.println("Find free machines.");
	        //System.out.println("Job " + rgl.getGridletID() + " reqNodes:" + rgl.getNumPE() + " freeNodes:" + super.resource_.getNumFreePE());
	        rgl.setIndexToZero();
	        Iterator<Machine> iter = list.iterator();
	        int PEsToAlloc=rgl.getNumPE();
	        int count = 0;
	        boolean bol= true;
	        int count2=0;
	        while(iter.hasNext()){
	        	bol=false;
	        	if (PEsToAlloc > 0){
		        	Machine m = iter.next();
		        	PEList MyPEList = m.getPEList();
		            int freePE = MyPEList.getNumFreePE();
		            int i = 0;
		            if (PEsToAlloc>freePE){
		            	for(i=0; i<freePE;i++){
		            		int freePEID = MyPEList.getFreePEID();
			            	rgl.setMachineAndPEID(m.getMachineID(), freePEID);
			            	rgl.addCapacity(count2, m.getMIPSRating());
			            	bol =super.resource_.setStatusPE(PE.BUSY, m.getMachineID(), freePEID);
			            	//System.out.println("count2="+count2 + " rating=" + m.getMIPSRating());
			            	count2++;
			            	
		            	}
		            	PEsToAlloc = PEsToAlloc-freePE;
		            	log.addMessage("\tAllocate " + freePE + " PEs from Cluster "+ m.getMachineID());
		            }else{
		            	for(i=0; i<PEsToAlloc;i++){
		            		int freePEID = MyPEList.getFreePEID();
			            	rgl.setMachineAndPEID(m.getMachineID(), freePEID);
			            	rgl.addCapacity(count2, m.getMIPSRating());
			            	bol=super.resource_.setStatusPE(PE.BUSY, m.getMachineID(), freePEID);
			            	//System.out.println("count2="+count2 + " rating=" + m.getMIPSRating());
			            	count2++;
		            	}
		            	//System.out.println("Alloc " + PEsToAlloc + " PEs from machine "+ m.getMachineID()+" (0)");
		            	PEsToAlloc = 0;
		            }
	        	}else{
	        		break;
	        	}
	        	
	        	count++;
	        }
	        // Calculate its consumed BW
	        rgl.calculateConsumedBandWidth(log);
	        
	        // Identify Completion Time and Set Interrupt
	        //int rating = machineRating_[ rgl.getMachineID() ];
	        int rating = rgl.getSlowestMachineCapacity();
	        double time = forecastFinishTime( rating, rgl.getRemainingGridletLength() );
	        int roundUpTime = (int) (time+1);   // rounding up
	        log.addMessage("\tJob " + rgl.getGridletID() + " slowest=" + rating + " set potential finish time=" + roundUpTime);
	        rgl.setFinishTime(roundUpTime);

	        // then send this into itself
	        super.sendInternalEvent(roundUpTime);   
	        // Add job to the execution list
	        gridletInExecList_.add(rgl);
        }catch(Exception e){
        	e.printStackTrace();
        	System.exit(100);
        }        
        return true;
    }
    */

    
    /**
     * Checks all Gridlets in the execution list whether they are finished or
     * not.
     * @pre $none
     * @post $none
     */
    /*
    private synchronized void checkGridletCompletion()
    {
        ResGridlet obj = null;
        int i = 0;
        // NOTE: This one should stay as it is since gridletFinish()
        // will modify the content of this list if a Gridlet has finished.
        // Can't use iterator since it will cause an exception
        while ( i < gridletInExecList_.size() )
        {
            obj = (ResGridlet) gridletInExecList_.get(i);
            //System.out.println("Job " + obj.getGridletID() + " sh:" + obj.getRemainingGridletLength());
            if (obj.getRemainingGridletLength() == 0.0)
            {
                gridletInExecList_.remove(obj);
                gridletFinish((ResJob)obj, Gridlet.SUCCESS);
                continue;
            }

            i++;
        }

        // if there are still Gridlets left in the execution
        // then send this into itself for an hourly interrupt
        // NOTE: Setting the internal event time too low will make the
        //       simulation more realistic, BUT will take longer time to
        //       run this simulation. Also, size of sim_trace will be HUGE!
        if (gridletInExecList_.size() > 0) {
            super.sendInternalEvent(60.0*60.0);
        }
    }*/
    

    /**
     * Updates the Gridlet's properties, such as status once a
     * Gridlet is considered finished.
     * @param rgl   a ResGridlet object
     * @param status   the Gridlet status
     * @pre rgl != null
     * @pre status >= 0
     * @post $none
     */
    /*
    private void gridletFinish(ResJob rgl, int status)
    {
    	int[] mL = rgl.getListMachineID();
    	int[] pL = rgl.getListPEID();
    	int i=0;
    	log.addMessage("Job ID: " + rgl.getGridletID());
    	String str = "";
    	for(i=0; i<mL.length;i++){
    		str = str + mL[i]+", ";
    	}
    	for(i=0; i<pL.length;i++){
    		str = str + pL[i]+", ";
    	}
    	log.addMessage(str);
    	for (int p=0; p<rgl.getNumPE();p++){
	        // Set PE on which Gridlet finished to FREE
	        //super.resource_.setStatusPE(PE.FREE, rgl.getMachineID(), rgl.getPEID());
    		super.resource_.setStatusPE(PE.FREE, mL[p], pL[p]);
    		//System.out.println("Job " + rgl.getGridletID() + " frees PE " + pL[p] + " on machine " + mL[p]);
    	}
        // the order is important! Set the status first then finalize
        // due to timing issues in ResGridlet class
    	rgl.setGridletStatus(status);
        rgl.finalizeGridlet();
        super.sendFinishGridlet( rgl.getGridlet() );

        allocateQueueGridlet();   // move Queued Gridlet into exec list
    }*/

    /**
     * Handles an operation of canceling a Gridlet in either execution list
     * or paused list.
     * @param gridletId    a Gridlet ID
     * @param userId       the user or owner's ID of this Gridlet
     * @return an ResGridlet object <tt>null</tt> if this Gridlet is not found
     * @pre gridletId > 0
     * @pre userId > 0
     * @post $none
     */
    private ResGridlet cancel(int gridletId, int userId)
    {
        ResGridlet rgl = null;

        // Find in EXEC List first
        int found = gridletInExecList_.indexOf(gridletId, userId);
        if (found >= 0)
        {
            // update the gridlets in execution list up to this point in time
            updateGridletProcessing();

            // Get the Gridlet from the execution list
            rgl = (ResGridlet) gridletInExecList_.remove(found);

            // if a Gridlet is finished upon cancelling, then set it to success
            // instead.
            if (rgl.getRemainingGridletLength() == 0.0) {
                rgl.setGridletStatus(Gridlet.SUCCESS);
            }
            else {
                rgl.setGridletStatus(Gridlet.CANCELED);
            }

            // Set PE on which Gridlet finished to FREE
            super.resource_.setStatusPE( PE.FREE, rgl.getMachineID(),
                                        rgl.getPEID() );
            allocateQueueGridlet();
            return rgl;
        }

        // Find in QUEUE list
        found = gridletQueueList_.indexOf(gridletId, userId);
        if (found >= 0)
        {
            rgl = (ResGridlet) gridletQueueList_.remove(found);
            rgl.setGridletStatus(Gridlet.CANCELED);
        }

        // if not, then find in the Paused list
        else
        {
            found = gridletPausedList_.indexOf(gridletId, userId);

            // if found in Paused list
            if (found >= 0)
            {
                rgl = (ResGridlet) gridletPausedList_.remove(found);
                rgl.setGridletStatus(Gridlet.CANCELED);
            }

        }
        return rgl;
    }
}