package simulator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;


import gridsim.AllocPolicy;
import gridsim.Machine;
import gridsim.MachineList;
import gridsim.ResourceCalendar;
import gridsim.ResourceCharacteristics;
import gridsim.net.SimpleLink;

public class MCEnvironment {
	//LinkedList<MachineList> clusters;
	private MachineList machines;
	private int MTU;
	private double delay;
	private int baud_rate;
	private String policy;
	
	//
	private MultiCluster multicluster;
	private String path;
	private int lastID = 0;
	private double timeToLoad = 0;
	
	private SimulatorLog log;
	private String policyArguments;
	private String outputFolder;
	private String inputFolder;
	
	
	public MCEnvironment(String policy_, SimulatorLog log_){
		this.machines = new MachineList();
		this.MTU = 1500;
		this.delay = 10;
		this.baud_rate = 1000000;
		this.policy = policy_;
		this.log = log_;
	}
	
	public void addCluster(int clusterID, int numNodes, int MIPS){
			machines.add( new Machine(lastID, numNodes, MIPS));
			lastID++;
	}
	
	public MultiCluster getMultiCluster(){
	    return this.multicluster;
	}
	
	
	public void setPath(String path_){
		this.path = path_;
	}
	
	public boolean readFile(){
		// start time for reading
		long start = System.currentTimeMillis();
		
		//--------------------------------
		// READ FROM FILE
		//--------------------------------
	    // add the clusters into the multicluster objects
	    // if the check is ok, proceed with the reading of the files
 		if (checkPath() == true){
 			BufferedReader reader = null;
 			try {
 				System.out.println("Reading environment file...");
 				log.addMessage("Reading environment file...");
 				FileInputStream file = new FileInputStream(this.path);
 				reader = new BufferedReader(new InputStreamReader(file));
 				// read one line at the time
 	            int line = 1;
 	            while ( reader.ready() ){
 	                parseValue(reader.readLine(), line);
 	                line++;
 	            }
 	            reader.close();    // close the file
 			} catch (FileNotFoundException e) {
 				e.printStackTrace();
 				return false;
 			} catch (IOException e) {
 				e.printStackTrace();
 				return false;
 			}
 			
 		}else{
 			return false;
 		}	
		 		
		//--------------------------------
		// CREATE BASIC STRUCTURE
		//--------------------------------
		
		// 4. Create a ResourceCharacteristics object that stores the
	    //    properties of a Grid resource: architecture, OS, list of
	    //    Machines, allocation policy: time- or space-shared, time zone
	    //    and its price (G$/PE time unit).
	    String arch = "Sun Ultra";      // system architecture
	    String os = "Solaris";          // operating system
	    double time_zone = 9.0;         // time zone this resource located
	    double cost = 3.0;              // the cost of using this resource
		ResourceCharacteristics resConfig = new ResourceCharacteristics(
	            arch, os, this.machines, ResourceCharacteristics.SPACE_SHARED,
	            time_zone, cost);

		// 5. Finally, we need to create a GridResource object.
	    long seed = 11L*13*17*19*23+1;
	    double peakLoad = 0.0;        // the resource load during peak hour
	    double offPeakLoad = 0.0;     // the resource load during off-peak hr
	    double holidayLoad = 0.0;     // the resource load during holiday
	
	    // incorporates weekends so the grid resource is on 7 days a week
	    LinkedList Weekends = new LinkedList();
	    Weekends.add(new Integer(Calendar.SATURDAY));
	    Weekends.add(new Integer(Calendar.SUNDAY));
	
	    // incorporates holidays. However, no holidays are set in this example
	    LinkedList Holidays = new LinkedList();
	    
		this.multicluster = null;
	    try
	    {
	    	ResourceCalendar resCalendar = new ResourceCalendar(time_zone,
	                peakLoad, offPeakLoad, holidayLoad, Weekends,
	                Holidays, seed);
	        
	    	// With policy specified
	    	if (policy.equals("FCFS-policy")){
	    		this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicyFCFS("MultiC", policy, log, this.policyArguments));
	    	}else if(policy.equals("SJF-policy")){
	    		this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicySJF("MultiC", policy, log, this.policyArguments));
	    	}else if(policy.equals("BJF-policy")){
	    		this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicyBJF("MultiC", policy, log, this.policyArguments));
	    	}else if(policy.equals("SPT-policy")){
	    		this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicySPT("MultiC", policy, log, this.policyArguments));
	    	}else if(policy.equals("LPT-policy")){
	    		this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicyLPT("MultiC", policy, log, this.policyArguments));
	    	}else if(policy.equals("HAS-policy")){
	    		this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicyHAS("MultiC", policy, log, this.policyArguments, outputFolder));
	    	}else if(policy.equals("HAS2-policy")){
	    		this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicyHAS2("MultiC", policy, log, this.policyArguments, outputFolder));
	    	}else if(policy.equals("RESERVATION-policy")){
	    		this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicyReservation("MultiC", policy, log, this.policyArguments, inputFolder));
	    	}else if(policy.equals("FPFS-policy")){
	    		this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicyFPFS("MultiC", policy, log, this.policyArguments));
	    	}else if(policy.equals("HAS3-policy")){
	    		this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicyHAS3("MultiC", policy, log, this.policyArguments, outputFolder));
	    	}else if(policy.equals("HAS4-policy")){
	    		this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicyHAS4("MultiC", policy, log, this.policyArguments, outputFolder));
	    	}else if(policy.equals("Genetic-policy")){
		    	this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicyGenetic("MultiC", policy, log, this.policyArguments, outputFolder));
	    	}else if(policy.equals("Test-policy")){
		    	this.multicluster = new MultiCluster("MultiC", new SimpleLink("MultiCluster_link", baud_rate, delay, MTU), resConfig, resCalendar, new PolicyTest("MultiC", policy, log, this.policyArguments, outputFolder));
	    	}
	    	

	    }catch (Exception e) {
	    	System.out.println("MCEnvironment ERROR: ");
	    	log.addMessage("MCEnvironment ERROR: ");
	        e.printStackTrace();
	        System.out.println("Exit.");
	        log.addMessage("Exit.");
	        System.exit(1);
	    }

	    this.timeToLoad = ((double)System.currentTimeMillis() - (double)start)/1000;
	    
	    
	    // 
	    log.addMessage("\nLIST OF CLUSTERS");
	    Iterator<Machine> iter = this.machines.iterator();
        while (iter.hasNext()){
        	Machine m = iter.next();
        	log.addMessage("Cluster " + m.getMachineID() + ": nodes=" + m.getNumPE() + " MIPS:" + m.getMIPSRating());
        }
        log.addMessage("");
	    
	    return true;
	}
	

	private void parseValue(String line, int lineNumber) {
		// skip a comment line
        if (line.startsWith(";")) {
            return;
        }
        // arguments to read
        int id = 0;
        int numMachines = 0;
        int mips = 0;
        double acbw;
        
        // Array indexes
        int CLUSTER_ID = 1 - 1;
        int NUM_MACHINES = 2 - 1;
        int MIPS = 3 - 1;
        int ACBW = 4 - 1;
        
        // max number of fields
        int MAX_FIELD = 4;
        
        String[] fieldArray = new String[MAX_FIELD];
        
        String[] sp = line.split("\\s+");  // split the fields based on a space
        int len = 0;        // length of a string
        int index = 0;      // the index of an array
        // check for each field in the array
        for (int i = 0; i < sp.length; i++){
            len = sp[i].length();  // get the length of a string
            // if it is empty then ignore
            if (len == 0) {
                continue;
            }else{
            	// if not, then put into the array
            	fieldArray[index] = sp[i];
                index++;
            }
        }
        
        try{
        	// Extract values and then, create a cluster object
	        Integer obj = null;
	        // Cluster ID
	        obj = new Integer( fieldArray[CLUSTER_ID].trim() );
	        id = obj.intValue();
	        
	        // number of machines
	        obj = new Integer( fieldArray[NUM_MACHINES].trim() );
	        numMachines = obj.intValue();
	        
	        // MIPS rating
	        obj = new Integer(fieldArray[MIPS].trim());
	        mips = obj.intValue();
	        
	        // ACBW  ( CURRENTLY NOT USED!! )
	        // **********
	        Double obj2 = null;
	        obj2 = new Double(fieldArray[ACBW].trim());
	        acbw = obj2.doubleValue();
	        // **********
	        
	        this.addCluster(id, numMachines, mips);
	        
        }catch (Exception e){
            System.out.println("Environment" + ": Exception in reading file at line #" + line +  ", exception: " + e.getMessage());
            log.addMessage("Environment" + ": Exception in reading file at line #" + line +  ", exception: " + e.getMessage());
        }
	}

	private boolean checkPath() {
		File file = new File(this.path);
		if (!file.isFile() && !file.isDirectory()){
			System.out.println("MCEnviroment: error checking environment file: " + this.path );
			System.out.println("Exit.");
			log.addMessage("MCEnviroment: error checking environment file: " + this.path );
			log.addMessage("Exit.");
			return false;
		}else{
			return true;
		}
	}
	
	public void printReport() {
		DecimalFormat df2 = new DecimalFormat( "#.##" );
		String timeNeeded = df2.format(this.timeToLoad);
		
		// Report
		log.addMessage("-------------------------------------");
		log.addMessage("   Environment report   ");
		log.addMessage("-------------------------------------");
		log.addMessage("Environment file:" + this.path);
		log.addMessage("Loaded clusters:" + this.machines.size() + " in " + timeNeeded + " seconds");
		log.addMessage("");
	}
	
	
	public int getNumClusters(){
		return this.machines.size();
	}
	
	public int getClusterNumNodes(int clusterID){
		return this.machines.getMachine(clusterID).getNumPE();
	}

	public int getNumNodes() {
		int nodes=0;
		Iterator iter = this.machines.iterator();
		while(iter.hasNext()){
			Machine m = (Machine) iter.next();
			nodes = nodes + m.getNumPE();
		}
		return nodes;
	}

	public void setPolicyArguments(String policyModificator, String outputFolder, String inputFolder) {
		this.policyArguments = policyModificator;
		this.outputFolder = outputFolder;
		this.inputFolder = inputFolder;
	}
	
	
	public MachineList getMachineList(){
		return this.machines;
	}
	
}
