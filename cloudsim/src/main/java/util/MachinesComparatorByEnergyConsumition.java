package util;

import gridsim.Machine;

import java.util.Comparator;

import simulator.EnergyMachine;

public class MachinesComparatorByEnergyConsumition implements Comparator<Machine>{

	@Override
	public int compare(Machine o1, Machine o2) {
		EnergyMachine m1=(EnergyMachine)o1;
		EnergyMachine m2=(EnergyMachine)o2;
		if(m1.getConsumition()<m2.getConsumition())
			return -1;
		if(m1.getConsumition()>m2.getConsumition())
			return 1;
		
		return 0;
	}

}
