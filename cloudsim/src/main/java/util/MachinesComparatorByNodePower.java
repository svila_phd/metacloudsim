package util;

import gridsim.Machine;

import java.util.Comparator;

public class MachinesComparatorByNodePower implements Comparator<Machine>{

	@Override
	public int compare(Machine o1, Machine o2) {
		if(o1.getPEList().getMIPSRating(0)<o2.getPEList().getMIPSRating(0))
			return -1;
		if(o1.getPEList().getMIPSRating(0)>o2.getPEList().getMIPSRating(0))
			return 1;
		return 0;
	}

}
