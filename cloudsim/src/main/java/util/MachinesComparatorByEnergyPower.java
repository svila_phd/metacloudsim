package util;

import gridsim.Machine;

import java.util.Comparator;

import simulator.EnergyMachine;

public class MachinesComparatorByEnergyPower implements Comparator<Machine>{

	@Override
	public int compare(Machine o1, Machine o2) {
		EnergyMachine m1=(EnergyMachine)o1;
		EnergyMachine m2=(EnergyMachine)o2;
		if(m1.getEnergyComputing()<m2.getEnergyComputing())
			return -1;
		if(m1.getEnergyComputing()>m2.getEnergyComputing())
			return 1;
		
		return 0;
	}

}
