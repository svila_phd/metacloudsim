package util;

import gridsim.Machine;

import java.util.Comparator;

public class MachinesComparatorByFreePE implements Comparator<Machine>{

	@Override
	public int compare(Machine o1, Machine o2) {
		if(o1.getNumFreePE()<o2.getNumFreePE())
			return -1;
		if(o1.getNumFreePE()>o2.getNumFreePE())
			return 1;
		return 0;
	}

}
