package system.eaclustersim;

import java.util.ArrayList;

import system.jobs.Job;

public class Cluster {
	private int cluster_id;
	private int num_resources;
	private double mips;
	private double banwidth;
	private double banwidth_used;
	private double energyIdle;
	private double energyComputing;
	private double energyStart;
	private double energyStop;
	private double energyConsumed;
	private int resources_used;
	private ArrayList<Job> inExecution;
	/** Creates a cluster with indicated specifications
	 * 
	 * @param pid The ID of the cluster
	 * @param nodes the number of nodes available
	 * @param pmips the MIPS of this cluster nodes
	 * @param bandwidth the maximum bandwidth of the connection
	 */
	public Cluster(int pid, int nodes,double pmips,double bandwidth,double energyIdle,double energyComputing,double energyStart,double energyStop){
		this.cluster_id=pid;
		this.num_resources=nodes;
		this.mips=pmips;
		this.resources_used=0;
		this.banwidth_used=0;
		this.banwidth=bandwidth;
		this.inExecution=new ArrayList<Job>();
		this.energyIdle=energyIdle;
		this.energyComputing=energyComputing;
		this.energyStart=energyStart;
		this.energyStop=energyStop;
	}
	/** Obtain the energy consumed of the cluster when idle
	 * 
	 * @return the energy consumed of this cluster when idle
	 */
	public double getEnergyIdle(){
		return energyIdle;
	}

	/** Obtain the energy consumed of the cluster when computing
	 * 
	 * @return the energy consumed of this cluster when computing
	 */
	public double getEnergyComputing(){
		return energyComputing;
	}
	/** Obtain the energy consumed of the cluster when starts
	 * 
	 * @return the energy consumed of this cluster when starts
	 */
	public double getEnergyStart(){
		return energyStart;
	}
	/** Obtain the energy consumed of the cluster when stops
	 * 
	 * @return the energy consumed of this cluster when stops
	 */
	public double getEnergyStop(){
		return energyStop;
	}
	/** Obtain the ID of the cluster
	 * 
	 * @return the ID of this cluster
	 */
	public int getId(){
		return cluster_id;
	}
	/** Obtain the number of resources in the Cluster
	 * 
	 * @return the number of the resources
	 */
	public int getMaxRecources(){
		return num_resources;
	}
	/** Obtain the resources being used
	 * 
	 * @return the number of used resources
	 */
	public int getUsedResources(){
		return resources_used;
	}
	/** Obtain the number of free resources
	 * 
	 * @return the number of free resources
	 */
	public int getFreeResources(){
		return num_resources-resources_used;
	}
	/** Obtain the MIPS of the nodes
	 * 
	 * @return the MIPS of the nodes
	 */
	public double getMIPS(){
		return mips;
	}
	/** Obtain the bandwidth of the cluster
	 * 
	 * @return the bandwidth
	 */
	public double getBandwidth(){
		return this.banwidth;
	}
	/** Sets the bandwidth used by the jobs in this cluster
	 * 
	 * @param br bandwidth consumed by the jobs
	 */
	public void setBandwidthUsed(double br){
		this.banwidth_used=br;
	}
	/** Obtain the bandwidth used by the jobs in this cluster
	 * 
	 * @return the bandwidth used
	 */
	public double getBrandwidthUsed(){
		return this.banwidth_used;
	}
	/** Obtain the slowdown of communication of this cluster
	 * 
	 * @return the slowdown of communication
	 */
	public double getSC(){
		if(banwidth_used==0){
			return 1.0;
		}else{
			double sat=this.banwidth/this.banwidth_used;
			sat=1.0/sat;
			if(sat<1.0){
				return 1.0;
			}else{
				return sat;
			}
		}
	}
	/** return the slowdown of processing of this cluster
	 * 
	 * @return the slowdown of processing
	 */
	public double getSP(){
		return 1000/mips;
	}
	
	/** Allocates a Job in this Cluster
	 * 
	 * @param job the Job to allocate
	 * @param requirements the number of requirements to use
	 * @return True if the job was allocated, False instead
	 */
	public boolean allocateJob(Job job,int requirements){
		if(resources_used+requirements>num_resources){
			return false;
		}
		this.inExecution.add(job);
		job.allocate(this.cluster_id, requirements);
		resources_used+=requirements;
		return true;
	}
	/** Finishes the execution of a job in this cluster
	 * 
	 * @param job the Job to finish 
	 * @param requirements the requirements used by this Job
	 * @return True if the job finishes correctly, False instead
	 */
	public boolean finish(Job job,int requirements){
		if(this.inExecution.remove(job)){
			resources_used-=requirements;
			return true;
		}
		return false;
	}
	/** adds the energy consumed to this cluster **/
	public void addEnergyConsumed(double e){
		this.energyConsumed+=e;
	}
	/** gets the energy consumed by this cluster **/
	public double getEnergyConsumed(){
		return this.energyConsumed;
	}
	
	/** resets the cluster to the initial situation
	 * 
	 */
	public void reset(){
		this.inExecution=new ArrayList<Job>();
		this.resources_used=0;
		this.banwidth_used=0;
		this.energyConsumed=0;
	}
	/** (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return new String("Cluster "+this.cluster_id+" "+this.resources_used+"/"+this.num_resources+" "+this.getMIPS());
		
	}

}
