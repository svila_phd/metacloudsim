package system.eaclustersim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import system.jobs.Job;
import util.BadAllocationException;


public class MultiCluster {
	private static MultiCluster instance;
	private static boolean seting=false;
	ArrayList<Job> inExecution;
	private HashMap<Job,ArrayList<Integer>> initialState_;
	ArrayList<Cluster> clusters;
	private double time;
	private double lastTimeChange;
	/** Set the singelton instance of the cluster simulator
	 *
	 * @param nodes An array of Cluster that forms the MultiCluster
	 * @param initialState a HashMap containg the jobs allocated at the start of the simulation and the allocation of the job
	 * @throws BadAllocationException 
	 */
	public static void setInstance(ArrayList<Cluster>nodes,HashMap<Job,ArrayList<Integer>> initialState) throws BadAllocationException{
		instance=new MultiCluster(nodes,initialState);
		
		seting=true;
	}
	/** The obtain the current simulation 
	 * 
	 * @return The current simulation MultiCluster
	 * @throws Exception If the MultiCluster was not previously set.
	 */
	public static MultiCluster instance() throws IllegalStateException{
		if(seting==false){
			throw new IllegalStateException();
		}
		return instance;
	}
	
	/** Initializate a MultiCluster simulation, called by the setInstance
	 * 
	 * @param nodes 
	 * @param initialState
	 * @throws BadAllocationException 
	 */
	private MultiCluster( ArrayList<Cluster> nodes,HashMap<Job,ArrayList<Integer>> initialState) throws BadAllocationException{
		this.inExecution=new ArrayList<Job>();
		this.clusters=new ArrayList<Cluster>();
		this.clusters.addAll(nodes);
		this.time=0;
		this.lastTimeChange=0;
		this.initialState_=initialState;
		this.setInitialState();
	}
	
	/** Returns the number of clusters that form the MultCluster
	 * 
	 * @return the number of clusters
	 */
	public int size(){
		return clusters.size();
	}
	/** Obtain the number of free resources available in the whole MultiCluster
	 * 
	 * @return The number of free nodes in the whole MultiCluster
	 */
	public int getFreeResources(){
		int freeR=0;
		for(Cluster c:clusters){
				freeR+=c.getFreeResources();
		}
		return freeR;
	}
	/** Obtain the maximum number of resources available in the MultiCluster
	 * 
	 * @return The maximum number of resources
	 */
	public int getMaxResources(){
		int resources=0;
		for(Cluster c:clusters){
			resources+=c.getMaxRecources();
		}
		return resources;
	}
	
	/** Obtain the maximum number of resources available in the MultiCluster
	 * 
	 * @return The maximum number of resources
	 */
	
	/** Obtain the maximum number of resources available in the Cluster specified by clusterSet
	 * 
	 * @param clusterSet The set of Cluster to take into account
	 * @return The maximum number of available resources in clusterSet
	 */
	public int getMaxResources(int[] clusterSet){
		int resources=0;
		for(int c:clusterSet){
			resources+=this.getCluster(c).getMaxRecources();
		}
		return resources;
	}
	/** Obtain the number of free resources available in Cluster specified by clusterSet
	 * 
	 * @param clusterSet The set of Cluster to take into account
	 * @return The number of free resources in clusterSet
	 */
	public int getFreeResources(int[] clusterSet){
		int freeR=0;
		for(int c: clusterSet){
			freeR+=this.getCluster(c).getFreeResources();
		}
		return freeR;
	}
	
	/** Calculates the Branwidth consumition by the running jobs in the MultiCluster
	 * 
	 */
	private void SaturationDetection(){
		for (Cluster c:clusters){
			c.setBandwidthUsed(0);
		}
		for(Job j:this.inExecution){
			if(j.getRequirements()>1){
				for(Map.Entry<Integer, Integer> alloc:j.getAllocation().entrySet()){
					double sat=((double)alloc.getValue()*j.getPpbw())*(((double)j.getRequirements()-(double)alloc.getValue())/((double)j.getRequirements()-(double)1));
					double prevSat=this.getCluster(alloc.getKey()).getBrandwidthUsed();
					this.getCluster(alloc.getKey()).setBandwidthUsed(sat+prevSat);
				}
			}
		}
	}
	
	/** Returns the current simulation to the initial state, reverting all changes
	 * @throws BadAllocationException Throws and exception if the allocation is impossible to do
	 * 
	 */
	private void setInitialState() throws BadAllocationException{
		if(initialState_!=null && !initialState_.isEmpty()){
			for(Job j : this.initialState_.keySet()){
				Integer []alloc=new Integer[j.getRequirements()];
				this.initialState_.get(j).toArray(alloc);
				if(this.sheduleWithAllocation(j, alloc)==false){
					throw new BadAllocationException();
				}
			}
		}
		
	}
	/** Allocates the Job using a predefined allocation
	 * 
	 * @param j the Job to allocate
	 * @param alloc the predefined allocation
	 * @return returns True if the Job was correctly allocated, False instead
	 */
	public boolean sheduleWithAllocation(Job j,Integer [] alloc){
		if(this.getFreeResources()>=j.getRequirements()){
			HashMap<Integer,Integer> allocation=new HashMap<Integer,Integer>();
			for(Integer i : alloc){
				if(!allocation.containsKey(i)){
					allocation.put(new Integer(i), new Integer(1));
				}else{
					int v=allocation.get(i);
					v++;
					allocation.put(new Integer(i), v);
				}
			}
			j.setStartTime(this.time);
			int[] preAssign=new int[allocation.keySet().size()];
			int index=0;
			for(Integer cluster:allocation.keySet()){
				try{
					this.getCluster(cluster).allocateJob(j,allocation.get(cluster));
				}catch(NullPointerException e){
					e.printStackTrace();
					System.exit(0);
				}
				preAssign[index++]=cluster;
			}
			j.setPreAssign(preAssign);
			j.setCoAssignationNumber(preAssign.length-1);
			this.inExecution.add(j);
			return true;
		}
		
		return false;
	}
	
	/** Executes the Jobs allocated in the MultiCluster until a Job finishes its execution
	 * 
	 * @return The time when the Job finishes
	 */
	public double execute(){
		this.calculateUnusedEnergyConsumition();
		if(!this.inExecution.isEmpty()){
			double minTime=Double.MAX_VALUE;
			this.SaturationDetection();
			ArrayList<Job> finishedJob=new ArrayList<Job>();
			for(Job j: this.inExecution){
				double maxSP=0;
				double maxSC=1;
				for(Integer cluster:j.getAllocation().keySet()){
					if(this.getCluster(cluster).getSP()>maxSP) maxSP=this.getCluster(cluster).getSP();
					if(this.getCluster(cluster).getSC()>maxSC) maxSC=this.getCluster(cluster).getSC();
				}
				
				int jobfinish=(int)(j.getStartTime()+(j.getTimeBase()*(j.getSigma()*maxSP+(1-j.getSigma())*maxSC)));
				j.setFinishTime(jobfinish);
				if(minTime>jobfinish) minTime=jobfinish;;
			}
		
			for(Job j:this.inExecution){
				if(j.getFinishTime()<=minTime){
					finishedJob.add(j);
				}
			}
		
			for(Job j:finishedJob){
				for(Integer cluster:j.getAllocation().keySet()){
					this.getCluster(cluster).finish(j,j.getAllocation().get(cluster));
				}
				this.calculateJobEnergyConsumition(j);
				this.inExecution.remove(j);
			}
			time=minTime;			
			this.calculateUnusedEnergyConsumition();
			return minTime;
			
		}
		return 0;
	}
	
	/** Executes all the Jobs scheduled in the MultiCluster 
	 * 
	 * @return The time when the last Job finishes its execution
	 */
	public double finishExecution(){
		double maxTime=0;
		if(!this.inExecution.isEmpty()){
			this.SaturationDetection();
			for(Job j: this.inExecution){
				double maxSP=0;
				double maxSC=0;
				for(Integer cluster:j.getAllocation().keySet()){
					if(this.getCluster(cluster).getSP()>maxSP) maxSP=this.getCluster(cluster).getSP();
					if(this.getCluster(cluster).getSC()>maxSC) maxSC=this.getCluster(cluster).getSC();
				}
				int jobfinish=(int) ((j.getStartTime())+(j.getTimeBase()*(j.getSigma()*maxSP+(1-j.getSigma())*maxSC)));
				j.setFinishTime(jobfinish);
				if(maxTime<jobfinish){
					maxTime=jobfinish;
				}
			}
		}else{
			maxTime=time;
		}
		return maxTime;
	}
	
	/** Resets the simulation to the starting point
	 * 
	 * @throws BadAllocationException
	 */
	public void reset() throws BadAllocationException{
		this.time=0;
		this.lastTimeChange=0;
		this.inExecution.clear();
		for(Cluster c:this.clusters){
			c.reset();
		}
		this.setInitialState();
	}
	
	/** Obtain the number of Jobs currently running in the MultiCluster
	 * 
	 * @return the number of Jobs running
	 */
	public int ExecutingSize(){
		return this.inExecution.size();
	}

	/** Obtain the cluster specifyied by an id
	 * 
	 * @param id the id of the cluster 
	 * @return the cluster with id=id
	 */
	public Cluster getCluster(int id){
		for(Cluster c:clusters){
			if(c.getId()==id){
				return c;
			}
		}
		return null;
	}
	/** Obtain the cluster in a position on the MultiCluster
	 * 
	 * @param pos The position in the array
	 * @return the cluster at the position specifyed
	 */
	public Cluster getClusterInPos(int pos){
		return clusters.get(pos);
	}
	/**(non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String ret=new String("MultiCluster: \n");
		for(Cluster c: this.clusters){
			ret+=c+"\n";
		}
		for(Job j:this.inExecution){
			ret+=j.getId()+" "+j.getTimeBase()+"\n";
		}
		return ret;
	}
	
	
	/** Orders the Cluster in the MultiCluster by computational power
	 * 
	 * @return A list with the Cluster ordered
	 */
	public ArrayList<Cluster> orderClustersByPower(){
		ArrayList<Cluster> orderedClusters=new ArrayList<Cluster>();
		ArrayList<Cluster> notOrderedClusters=new ArrayList<Cluster>();
		notOrderedClusters.addAll(clusters);
		while(notOrderedClusters.size()>0){
			Cluster faster=notOrderedClusters.get(0);
			for(Cluster c:notOrderedClusters){
				if(c.getMIPS()>faster.getMIPS()){
					faster=c;
				}
			}
			orderedClusters.add(faster);
			notOrderedClusters.remove(faster);
		}
		return orderedClusters;
	}
	/** Orders the Cluster in the MultiCluster by computational power
	 * 
	 * @return A list with the Cluster ordered
	 */
	public ArrayList<Cluster> orderClustersByPowerEnergy(){
		ArrayList<Cluster> orderedClusters=new ArrayList<Cluster>();
		ArrayList<Cluster> notOrderedClusters=new ArrayList<Cluster>();
		notOrderedClusters.addAll(clusters);
		while(notOrderedClusters.size()>0){
			Cluster faster=notOrderedClusters.get(0);
			for(Cluster c:notOrderedClusters){
				if(c.getEnergyConsumed()<faster.getEnergyConsumed()){	
					faster=c;
				}
			}
			orderedClusters.add(faster);
			notOrderedClusters.remove(faster);
		}
		return orderedClusters;
	}
	/** Obtain tha allocation of the job in the MultiCluster usnig the MESD algorithm
	 * 
	 * @param j the Job to allocate
	 * @return the allocation
	 */
	public Integer[] getMESDAlloc(Job j){
		if(j==null){
			System.out.println("panoli");
			System.exit(2);
		}
	if(j.getRequirements()<=this.getFreeResources()){
		//order the clusters from faster to slower.
		ArrayList<Cluster> orderedClusters=orderClustersByPower();
		//search for the best assignation
		int requirements=j.getRequirements();
		int resources=0;
		int startingCluster=0;
		boolean jobFits=false;
		Integer[] alloc=new Integer[requirements];
		//search for the starting cluster( the slowest cluster needed to execute the job)
		
		while(!jobFits){
			resources+=orderedClusters.get(startingCluster).getFreeResources();
			if(resources>=requirements){
				jobFits=true;
			}else{
				startingCluster++;
			}
		}
		//now get the allocation
		int i=0;
		while(requirements>i){
			if(orderedClusters.get(startingCluster).getFreeResources()<requirements-i){
				for(int k=0;k<orderedClusters.get(startingCluster).getFreeResources();k++){
					alloc[i]=orderedClusters.get(startingCluster).getId();
					i++;
				}
				startingCluster--;
			}else{
				for(int k=0;k<requirements-i;k++){
					alloc[i]=orderedClusters.get(startingCluster).getId();
					i++;
				}
			}
		}
		return alloc;
	}
	return null;	
}
	
	public Integer[] getJPRAlloc(Job j){
		if(j==null){
			System.out.println("panoli");
			System.exit(2);
		}
	if(j.getRequirements()<=this.getFreeResources()){
		//order the clusters from faster to slower.
		ArrayList<Cluster> orderedClusters=orderClustersByPower();
		//search for the best assignation
		int requirements=j.getRequirements();
		int startingCluster=0;
		Integer[] alloc=new Integer[requirements];
		//search for the starting cluster( the slowest cluster needed to execute the job)
		
		//now get the allocation
		int i=0;
		while(requirements>i){
			if(orderedClusters.get(startingCluster).getFreeResources()<requirements-i){
				for(int k=0;k<orderedClusters.get(startingCluster).getFreeResources();k++){
					alloc[i]=orderedClusters.get(startingCluster).getId();
					i++;
				}
				startingCluster++;
			}else{
				for(int k=0;k<requirements-i;k++){
					alloc[i]=orderedClusters.get(startingCluster).getId();
					i++;
				}
			}
		}
		return alloc;
	}
	return null;	
}
	
	/** Obtain the allocation of the job in the MultiCluster usnig Energy Aware Allocation
	 * @param j the Job to allocate
	 * @return the allocation
	 */
	public Integer[] getEAAlloc(Job j){
		if(j==null){
			System.out.println("panoli");
			System.exit(2);
		}
		ArrayList<Cluster> orderedClusters=orderClustersByPowerEnergy();
		if(j.getRequirements()<=this.getFreeResources()){
			//order the clusters from faster to slower.
			//search for the best assignation
			int requirements=j.getRequirements();
			int startingCluster=0;
			Integer[] alloc=new Integer[requirements];
			//now get the allocation
			int i=0;
			while(requirements>i){
				if(orderedClusters.get(startingCluster).getFreeResources()<requirements-i){
					for(int k=0;k<orderedClusters.get(startingCluster).getFreeResources();k++){
						alloc[i]=orderedClusters.get(startingCluster).getId();
						i++;
					}
					startingCluster++;
				}else{
					for(int k=0;k<requirements-i;k++){
						alloc[i]=orderedClusters.get(startingCluster).getId();
						i++;
					}
				}
			}
			return alloc;
		}
		return null;	
}
	
	/** Obtain the SlowDown Difference between the current allocation and the allocation in an idle system
	 * 
	 * @param j the Job to allocate
	 * @return the slowDown difference
	 */
	public double getEnergyDifference(Job j) {
		//calculate the execution slowdown of this job with the MESD allocation
		if (this.getFreeResources()<j.getRequirements()){
			return -1;
		}
		double currentEnergy=0;
		//get the MESD Allocation.
		Integer[] allocEA=this.getEAAlloc(j);
		HashMap<Integer,Integer>alloc=new HashMap<Integer,Integer>();
		for(Integer node:allocEA){
			if(alloc.containsKey(node)){
				Integer val=alloc.get(node);
				alloc.put(node, val+1);
			}else{
				alloc.put(node, 1);
			}
		}
		//slowdown of computation
		double maxSP=0.0;
		for(Integer cluster:alloc.keySet()){
			if(this.getCluster(cluster).getSP()>maxSP) maxSP=this.getCluster(cluster).getSP();
		}
		//slowdown of communication
		double maxSC=1.0;
		this.SaturationDetection();
		if(j.getRequirements()>1){
			double SC=1.0;
			for(Map.Entry<Integer, Integer> cluster:alloc.entrySet()){
				double sat=((double)cluster.getValue()*j.getPpbw())*(((double)j.getRequirements()-(double)cluster.getValue())/((double)j.getRequirements()-(double)1));
				double prevSat=this.getCluster(cluster.getKey()).getBrandwidthUsed();
				double bwUse=sat+prevSat;
				if(bwUse==0.0){
					SC=1.0;
				}else{
					double bw=this.getCluster(cluster.getKey()).getBandwidth()/bwUse;
					SC=1.0/bw;
					if(SC<1.0){
						SC= 1.0;
					}
				}
				if(SC>maxSC) maxSC=SC;
			}
		}
		for(Integer cluster:alloc.keySet()){
			double sp=this.getCluster(cluster).getSP();
			double execTime=j.getTimeBase()*j.getSigma()*sp;
			double busyTime=(j.getSigma()*maxSP+(1-j.getSigma())*maxSC)*j.getTimeBase();
			double idleTime=busyTime-execTime;
			currentEnergy+=execTime*this.getCluster(cluster).getEnergyComputing()+idleTime*this.getCluster(cluster).getEnergyIdle();
		}
		
		//now the idle slowdown for the same job
		double idleEnergy=0;
		//slowdown of computation
		ArrayList<Cluster> orderedClusters=orderClustersByPower();
		int req=j.getRequirements();
		int slowestCluster=-1;
		while(req>0){
			slowestCluster++;
			req=req-orderedClusters.get(slowestCluster).getMaxRecources();
		}
		maxSP=orderedClusters.get(slowestCluster).getSP();
		//slowdown of comunication
		req=j.getRequirements();
		maxSC=1.0;
		while(req>0){
			int allocincluster=0;
			if(req>orderedClusters.get(slowestCluster).getMaxRecources()){
				allocincluster=orderedClusters.get(slowestCluster).getMaxRecources();
			}else{
				allocincluster=req;
			}
			req-=allocincluster;
			double SC=1.0;
			double bwUse=((double)allocincluster*j.getPpbw())*(((double)j.getRequirements()-(double)allocincluster)/((double)j.getRequirements()-(double)1));
			if(bwUse==0.0){
				SC=1.0;
			}else{
				double bw=orderedClusters.get(slowestCluster).getBandwidth()/bwUse;
				SC=1.0/bw;
				if(SC<1.0){
					SC= 1.0;
				}
			}
			if(SC>maxSC) maxSC=SC;
			slowestCluster--;
		}
		req=j.getRequirements();
		int cluster=-1;
		while(req>0){
			cluster++;
			req=req-orderedClusters.get(cluster).getMaxRecources();
			double sp=orderedClusters.get(cluster).getSP();
			double execTime=j.getTimeBase()*j.getSigma()*sp;
			double busyTime=(j.getSigma()*maxSP+(1-j.getSigma())*maxSC)*j.getTimeBase();
			double idleTime=busyTime-execTime;
			idleEnergy+=execTime*orderedClusters.get(cluster).getEnergyComputing()+idleTime*orderedClusters.get(cluster).getEnergyIdle();
			
		}
		
		return (currentEnergy)-(idleEnergy);
	}
	
	
	/** Obtain the SlowDown Difference between the current allocation and the allocation in an idle system
	 * 
	 * @param j the Job to allocate
	 * @return the slowDown difference
	 */
	public double getSlowDownDifference(Job j) {
		//calculate the execution slowdown of this job with the MESD allocation
		if (this.getFreeResources()<j.getRequirements()){
			return -1;
		}
		double currentslowdown=0;
		//get the MESD Allocation.
		Integer[] allocMESD=this.getMESDAlloc(j);
		HashMap<Integer,Integer>alloc=new HashMap<Integer,Integer>();
		for(Integer node:allocMESD){
			if(alloc.containsKey(node)){
				Integer val=alloc.get(node);
				alloc.put(node, val+1);
			}else{
				alloc.put(node, 1);
			}
		}
		//slowdown of computation
		double maxSP=0.0;
		for(Integer cluster:alloc.keySet()){
			if(this.getCluster(cluster).getSP()>maxSP) maxSP=this.getCluster(cluster).getSP();
		}
		//slowdown of communication
		double maxSC=1.0;
		this.SaturationDetection();
		if(j.getRequirements()>1){
			double SC=1.0;
			for(Map.Entry<Integer, Integer> cluster:alloc.entrySet()){
				double sat=((double)cluster.getValue()*j.getPpbw())*(((double)j.getRequirements()-(double)cluster.getValue())/((double)j.getRequirements()-(double)1));
				double prevSat=this.getCluster(cluster.getKey()).getBrandwidthUsed();
				double bwUse=sat+prevSat;
				if(bwUse==0.0){
					SC=1.0;
				}else{
					double bw=this.getCluster(cluster.getKey()).getBandwidth()/bwUse;
					SC=1.0/bw;
					if(SC<1.0){
						SC= 1.0;
					}
				}
				if(SC>maxSC) maxSC=SC;
			}
		}
		currentslowdown=j.getSigma()*maxSP+(1-j.getSigma())*maxSC;
		
		//now the idle slowdown for the same job
		double idleslowdown=0;
		//slowdown of computation
		ArrayList<Cluster> orderedClusters=orderClustersByPower();
		int req=j.getRequirements();
		int slowestCluster=-1;
		while(req>0){
			slowestCluster++;
			req=req-orderedClusters.get(slowestCluster).getMaxRecources();
		}
		maxSP=orderedClusters.get(slowestCluster).getSP();
		//slowdown of comunication
		req=j.getRequirements();
		maxSC=1.0;
		while(req>0){
			int allocincluster=0;
			if(req>orderedClusters.get(slowestCluster).getMaxRecources()){
				allocincluster=orderedClusters.get(slowestCluster).getMaxRecources();
			}else{
				allocincluster=req;
			}
			req-=allocincluster;
			double SC=1.0;
			double bwUse=((double)allocincluster*j.getPpbw())*(((double)j.getRequirements()-(double)allocincluster)/((double)j.getRequirements()-(double)1));
			if(bwUse==0.0){
				SC=1.0;
			}else{
				double bw=orderedClusters.get(slowestCluster).getBandwidth()/bwUse;
				SC=1.0/bw;
				if(SC<1.0){
					SC= 1.0;
				}
			}
			if(SC>maxSC) maxSC=SC;
			slowestCluster--;
		}
		idleslowdown=j.getSigma()*maxSP+(1-j.getSigma())*maxSC;
		return (j.getTimeBase()*idleslowdown)-(j.getTimeBase()*currentslowdown);
	}
	
	/** Obtain an array indicating the order of the clusters to perform the allocation
	 * 
	 * @param j the Job to calculate
	 * @return return the array of cluster preferences
	 */
	public int[] getAllocationPreferences(Job j){
		if(j.getRequirements()<=this.getFreeResources()){
			//order the clusters from faster to slower.
			ArrayList<Cluster> orderedClusters=orderClustersByPower();
			//search for the best assignation
			int requirements=j.getRequirements();
			int resources=0;
			int startingCluster=0;
			boolean jobFits=false;
			int[] palloc=new int[this.size()+1];
			for(int i=0;i<palloc.length;i++){
				palloc[i]=-1;
			}
			//search for the starting cluster( the slowest cluster needed to execute the job)
			while(!jobFits){
				resources+=orderedClusters.get(startingCluster).getFreeResources();
				if(resources>=requirements){
					jobFits=true;
				}else{
					startingCluster++;
				}
			}
			
			resources=0;
			int used=0;
			for(int i=0;i<palloc.length-1;i++){
				while(resources<requirements){
					if(orderedClusters.get(startingCluster).getFreeResources()>0 ){
						resources+=orderedClusters.get(startingCluster).getFreeResources();
						palloc[i]=orderedClusters.get(startingCluster).getId();
						used++;
						startingCluster--;
						break;
					}else{
						startingCluster--;
					}
				}
			}
			palloc[palloc.length-1]=used;
			return palloc;
		}
		return null;	
	}
	/** Calculates the energy consumed by the system since last time the function was called. This calculations
	 * will be added to the previous ones, obtaining the accumulated energy consumed by using the function getEnergyConsumition.
	 */
	private void calculateUnusedEnergyConsumition(){
		double elapsedTime=time-lastTimeChange;
		lastTimeChange=time;
		for(Cluster c:this.clusters){
			if((elapsedTime/3600)*c.getEnergyIdle()<c.getEnergyStart()+c.getEnergyStop()){
				c.addEnergyConsumed(c.getEnergyIdle()*c.getFreeResources()*elapsedTime/3600);
			}
		}
	}
	/** Calculates the energy consumed by the job in each machine **/
	private void calculateJobEnergyConsumition(Job j){
		
		for(Entry<Integer, Integer> alloc:j.getAllocation().entrySet()){
			double timeComputing=j.getTimeBase()*j.getSigma()*this.getCluster(alloc.getKey()).getSP();
			double timeIdle=j.getFinishTime()-j.getStartTime()-timeComputing;
			//add the computing time and the idle when running a job.
			this.getCluster(alloc.getKey()).addEnergyConsumed((timeComputing/3600)*alloc.getValue());
			this.getCluster(alloc.getKey()).addEnergyConsumed((timeIdle/3600)*alloc.getValue());
			
		}
	}
	/** Obtains the total amount of energy consumed by the system.
	 * @return The total amount of energy consumed by the system.
	 */
	public double getEnergyConsumition(){
		double energyConsumed=0;
		for(Cluster c:this.clusters){
			energyConsumed+=c.getEnergyConsumed();
		}
		return energyConsumed;
	}
}
