package PSOMO;


import java.util.ArrayList;
import java.util.List;

import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;

import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.problem.impl.AbstractGenericProblem;
import org.uma.jmetal.solution.DoubleSolution;


public abstract class PSOProblem extends AbstractGenericProblem<DoubleSolution> implements DoubleProblem {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2874411385013627221L;
	protected JobList jobs;
	protected MultiCluster mc;
	private List<Double> lowerLimit ;
	private List<Double> upperLimit ;

	public Double getUpperBound(int index) {
		return upperLimit.get(index);
	}
	public Double getLowerBound(int index) {
		return lowerLimit.get(index);
	}
	protected void setLowerLimit(List<Double> lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	protected void setUpperLimit(List<Double> upperLimit) {
		this.upperLimit = upperLimit;
	}
	
	public PSOProblem(JobList jobs,MultiCluster mc){
		super();
		this.setName("schedulingProblem");
		this.setNumberOfVariables(jobs.size()+jobs.size()*mc.size());
		this.jobs=jobs;
		this.mc=mc;
		ArrayList<Double>lLim=new ArrayList<Double>();
		ArrayList<Double>uLim=new ArrayList<Double>();
		
		for(int x=0;x<getNumberOfVariables();x++){
			lLim.add((double)0);
			uLim.add((double)1);
		}
		this.setLowerLimit(lLim);
		this.setUpperLimit(uLim);
	}
	
	public PSOSolution createSolution() {
		return new PSOSolution(this);
	}

	public int getPermutationLength() {
		return jobs.size();
	}
	
	public int getMultiClusterLength() {
		return mc.size();
	}
	
	public JobList getJobList(){
		return jobs;
	}
}
