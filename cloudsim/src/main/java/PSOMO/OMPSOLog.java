package PSOMO;

import genetic.JobData;
import genetic.SchedulingSolution;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.uma.jmetal.algorithm.multiobjective.omopso.OMOPSO;
import org.uma.jmetal.operator.impl.mutation.NonUniformMutation;
import org.uma.jmetal.operator.impl.mutation.UniformMutation;
import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.evaluator.SolutionListEvaluator;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

public class OMPSOLog extends OMOPSO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	long time;
	PrintWriter writer;
	JMetalRandom randomGenerator;
	PSOProblem problem;
	public OMPSOLog(DoubleProblem problem,
			SolutionListEvaluator<DoubleSolution> evaluator, int swarmSize,
			int maxIterations, int archiveSize,
			UniformMutation uniformMutation,
			NonUniformMutation nonUniformMutation,
			String logfile) {
		super(problem, evaluator, swarmSize, maxIterations, archiveSize,
				uniformMutation, nonUniformMutation);
		time=System.currentTimeMillis();
		randomGenerator = JMetalRandom.getInstance() ;
		this.problem=(PSOProblem) problem;
		try {
			writer = new PrintWriter(logfile, "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*@Override
	public void updateProgress(){
		super.updateProgress();
		System.out.println("--------------------------");
	}*/
	@Override
	public void initProgress(){
		super.initProgress();
		List<DoubleSolution> l=this.getResult();
		double bigestm=l.get(0).getObjective(0);
		for(DoubleSolution ss:l){
			if(bigestm>ss.getObjective(0)){
				bigestm=ss.getObjective(0);
			}
		}
		double bigeste=l.get(0).getObjective(1);
		for(DoubleSolution ss:l){
			if(bigeste>ss.getObjective(1)){
				bigeste=ss.getObjective(1);
			}
		}
		System.out.println(bigestm);
		writer.println(bigestm+";"+bigeste+";"+(float)(System.currentTimeMillis()-this.time)/(float)1000);
		writer.flush();
	}
	@Override
	public void updateProgress(){
		super.updateProgress();
		List<DoubleSolution> l=this.getResult();
		double bigestm=l.get(0).getObjective(0);
		for(DoubleSolution ss:l){
			if(bigestm>ss.getObjective(0)){
				bigestm=ss.getObjective(0);
			}
		}
		double bigeste=l.get(0).getObjective(1);
		for(DoubleSolution ss:l){
			if(bigeste>ss.getObjective(1)){
				bigeste=ss.getObjective(1);
			}
		}
		System.out.println(bigestm);
		writer.println(bigestm+";"+bigeste+";"+(float)(System.currentTimeMillis()-this.time)/(float)1000);
		writer.flush();
	}
	
	protected List<DoubleSolution> psoCrossoverOrder(List<DoubleSolution>swarm){
		Collections.shuffle(swarm);
		   for(int i=2;i<swarm.size();i+=2){
			   PSOSolution parent1=(PSOSolution) swarm.get(i-2);
			   PSOSolution parent2=(PSOSolution) swarm.get(i-1);
			   
			   ArrayList<JobData> jobsParent1=parent1.getJobs();
			   ArrayList<JobData> jobsParent2=parent2.getJobs();
			   ArrayList<JobData> jobsChild1 = new ArrayList<JobData>();
			   ArrayList<JobData> jobsChild2 = new ArrayList<JobData>();

			   int[] mask = new int[parent1.getJobs().size()];
			   for(int m=0;m<mask.length;m++){
				   mask[m]=randomGenerator.nextInt(0, 1);
			   }
			   
			   ArrayList<Integer> missingJobs1= new ArrayList<Integer>();
			   ArrayList<Integer> missingJobs2= new ArrayList<Integer>();
			   for(int ii=0;ii<mask.length;ii++){	
				   if(mask[ii]==0){
					   JobData jobParent1=jobsParent1.get(ii);
					   JobData jobParent2=jobsParent2.get(ii);
					   for(int j=0;j<mask.length;j++){
						   JobData temp1=jobsParent2.get(j);
						   JobData temp2=jobsParent1.get(j);
						   if(jobParent1.job.getId()==temp1.job.getId()){
						   //if(jobParent1.equals(temp1)){
							   missingJobs1.add(j);
						   }
						   if(jobParent2.job.getId()==temp2.job.getId()){
						   //if(jobParent2.equals(temp2)){
							   missingJobs2.add(j);
						   }
						}
					}
				}
				
				Collections.sort(missingJobs1);
				Collections.sort(missingJobs2);
				int indexp1=0;
				for(int ii=0;ii<mask.length;ii++){
					if(mask[ii]==1){
						jobsChild1.add(jobsParent1.get(ii));
						jobsChild2.add(jobsParent2.get(ii));
					}else{
						
						jobsChild1.add(jobsParent2.get(missingJobs1.get(indexp1)));
						jobsChild2.add(jobsParent1.get(missingJobs2.get(indexp1)));
						indexp1++;
					}
				}
				swarm.set(i-2, new PSOSolution(jobsChild1,this.problem));
				swarm.set(i-1, new PSOSolution(jobsChild2,this.problem));
				
		   }
		   return swarm;
		
	}
	
	protected List<DoubleSolution> psoCrossoverAlloc(List<DoubleSolution>swarm){
		Collections.shuffle(swarm);
		for(int ii=2;ii<swarm.size();ii+=2){
			PSOSolution parent1=(PSOSolution) swarm.get(ii-2);
			PSOSolution parent2=(PSOSolution) swarm.get(ii-1);
			ArrayList<JobData> jobsParent1=parent1.getJobs();
			ArrayList<JobData> jobsParent2=parent2.getJobs();
			ArrayList<JobData> jobsChild1 = new ArrayList<JobData>();
			ArrayList<JobData> jobsChild2 = new ArrayList<JobData>();
			jobsChild1.addAll(jobsParent1);
			jobsChild2.addAll(jobsParent2);
			double cross=randomGenerator.nextDouble(-0.2, 1.2);
			
			for(int i=0;i<jobsParent1.size();i++){
				JobData parent1Data=jobsParent1.get(i);
				for(int j=0;j<jobsParent2.size();j++){
					JobData parent2Data=jobsParent2.get(j);
					if(parent1Data.job.getId()==parent2Data.job.getId()){
						JobData child1Data=new JobData(parent1Data);
						JobData child2Data=new JobData(parent2Data);
						for(int fn=0;fn<parent1Data.forbidenNodes.length;fn++){
							if(parent1Data.forbidenNodes[fn]>parent2Data.forbidenNodes[fn]){
								child1Data.forbidenNodes[fn]=parent2Data.forbidenNodes[fn];
								child2Data.forbidenNodes[fn]=parent2Data.forbidenNodes[fn];
							}else{
								child1Data.forbidenNodes[fn]=cross*parent1Data.forbidenNodes[fn]+(1-cross)*parent2Data.forbidenNodes[fn];
								child2Data.forbidenNodes[fn]=cross*parent2Data.forbidenNodes[fn]+(1-cross)*parent1Data.forbidenNodes[fn];
							}
						}
						jobsChild1.set(i,child1Data);
						jobsChild2.set(j,child2Data);
						break;
					}
				}
			}
			swarm.set(ii-2, new PSOSolution(jobsChild1,this.problem));
			swarm.set(ii-1, new PSOSolution(jobsChild2,this.problem));
		}
		return swarm;
	}
	 @Override
	 protected List<DoubleSolution> evaluateSwarm(List<DoubleSolution> swarm) {
       double rand = randomGenerator.nextDouble();
       List<DoubleSolution> newSwarm;
       if(rand<0.33){
    	   newSwarm = this.psoCrossoverOrder(swarm);
       }else if(rand<0.66){
    	   newSwarm = this.psoCrossoverAlloc(swarm);
       }else{
    	   newSwarm = swarm;
       }
       newSwarm = super.evaluateSwarm(newSwarm);
       
	    /*for(DoubleSolution currentSwarm : newSwarm){
	    	PSOSolution psos=(PSOSolution) currentSwarm;
	    	/*for(JobData job : psos.getJobs()){
	    		System.out.print(job.job.getId()+" ");
	    	}
	    	System.out.println(currentSwarm.getObjective(0));
	    }*/
	    return newSwarm;
	  }
}
