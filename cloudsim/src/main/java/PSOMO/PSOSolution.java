package PSOMO;


import genetic.JobData;
import geneticsimulator.Job;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.solution.impl.AbstractGenericSolution;



public class PSOSolution extends AbstractGenericSolution<Double, PSOProblem> implements DoubleSolution {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1566646965472012015L;
	Random r = new Random();
	public PSOSolution(PSOProblem psoProblem) {
		super(psoProblem);
	    overallConstraintViolationDegree = 0.0 ;
	    numberOfViolatedConstraints = 0 ;
	    List<JobData> randomSequence = new ArrayList<JobData>(psoProblem.getPermutationLength());
	    for (int j = 0; j < psoProblem.getPermutationLength(); j++) {
	    	randomSequence.add(initializeJobData(psoProblem.getJobList().get(j)));
	    }
	    java.util.Collections.shuffle(randomSequence);
	    
	    this.encodeArray(randomSequence);
	}
	
	public void encodeArray(List<JobData> randomSequence){
		for (int position=0;position < this.problem.getPermutationLength(); position++) {
	    	JobData job=randomSequence.get(position);
	    	int index=0;
	    	for(;index<this.problem.getJobList().size();index++){
	    		if(job.job.getId()==this.problem.getJobList().get(index).getId()){
	    			break;
	    		}
	    	}
	    	setVariableValue(index,(double)((double)position/(double)this.problem.getPermutationLength()));
	    	
	    	int indexAlloc=(this.problem.getMultiClusterLength()*index)+this.problem.getPermutationLength();
	    	int endAlloc=indexAlloc+this.problem.getMultiClusterLength();
	    	for(int i=0;indexAlloc<endAlloc;indexAlloc++,i++){
	    		setVariableValue(indexAlloc,job.forbidenNodes[i]);
	    	}
	    }
	    this.repairSolution();
	}
	
	public PSOSolution(List<JobData> jobs,PSOProblem problem){
		super(problem);
		this.encodeArray(jobs);
	}
	
	public PSOSolution(PSOSolution sol){
		super(sol.problem);
		overallConstraintViolationDegree = 0.0 ;
	    numberOfViolatedConstraints = 0 ;
	    for (int i = 0; i < getNumberOfVariables(); i++) {
	    	setVariableValue(i, sol.getVariableValue(i));
		}
	    for (int i = 0; i < getNumberOfObjectives(); i++) {
	    	setObjective(i, sol.getObjective(i));
		}
	    this.repairSolution();
	}

	public String getVariableValueString(int index) {
		if(index<this.problem.getPermutationLength()){
			JobData variable=getJobs().get(index);
			String solution="Job "+variable.job.getId()+" {";
			for(int i=0;i<variable.forbidenNodes.length;i++){
			solution+=variable.forbidenNodes[i]+",";
			}
			return solution+"}";
		}else{
			return "";
		}
	}

	public PSOSolution copy() {
		return new PSOSolution(this);
	}
	
	public JobData initializeJobData(Job j){
		JobData n=new JobData(j,problem.getMultiClusterLength());
		for(int i=0;i<n.forbidenNodes.length;i++){
			n.forbidenNodes[i]=this.randomGenerator.nextDouble(0d, 1d);
		}
		return n;
	}
	
	public void repairSolution(){
		ArrayList<JobData> jobs=this.getJobs();
		for(int i=0;i<problem.getPermutationLength();i++){
			JobData j=jobs.get(i);
			boolean valid=false;
			while(!valid){
				int availableNodes=0;
				int index=0;
		    	for(;index<problem.getJobList().size();index++){
		    		if(j.job.getId()==problem.getJobList().get(index).getId()){
		    			break;
		    		}
		    	}
		    	int indexAlloc=this.problem.getMultiClusterLength()*index+this.problem.getPermutationLength();
				for(int c=0;c<j.forbidenNodes.length;c++){
					if(j.forbidenNodes[c]>1){j.forbidenNodes[c]=1;}
					if(j.forbidenNodes[c]<0){j.forbidenNodes[c]=0;}
					//if(new Double(j.forbidenNodes[c]).isNaN()){System.out.print("nan");j.forbidenNodes[c]=0.0;}
					setVariableValue(indexAlloc+c,j.forbidenNodes[c]);
				}
				for(int c=0;c<j.forbidenNodes.length;c++){
					int numNodesc=this.problem.mc.getClusterInPos(c).getMaxResources();
					availableNodes+=(int) (numNodesc*(1-j.forbidenNodes[c]));
				}
				if(availableNodes>=j.job.getRequirements()){
					valid=true;
				}else{
					ArrayList<Integer>cid = new ArrayList<Integer>();
					for(int c=0;c<j.forbidenNodes.length;c++){
						if(j.forbidenNodes[c]>0){
							cid.add(c);
						}
					}
					int ctemp=0;
					if(cid.size()>0){
						ctemp=r.nextInt(cid.size());
					}
					j.forbidenNodes[cid.get(ctemp)]-=0.1;

					
			    	setVariableValue(indexAlloc+cid.get(ctemp),j.forbidenNodes[cid.get(ctemp)]);

				}
			}
		}
		//System.out.print(this);

	}
	
	public Double getLowerBound(int index) {
		return problem.getLowerBound(index);
	}

	public Double getUpperBound(int index) {
		return problem.getUpperBound(index);
	}
	
	public ArrayList<JobData> getJobs(){
		ArrayList<JobData> jobs=new ArrayList<JobData>();
		ArrayList<Double> convert=new ArrayList<Double>();
		for (int i=0;i<this.problem.getPermutationLength();i++){
			convert.add(getVariableValue(i));
		}
		
		for (int position=0;position < this.problem.getPermutationLength(); position++) {
	    	int index=0;
	    	double min=Double.MAX_VALUE;
	    	for(int i=0;i<this.problem.getPermutationLength();i++){
	    		double value=convert.get(i);
	    		if(value<0){
	    			continue;
	    		}else{
	    			if(value < min){
	    				index=i;
	    				min=value;
	    			}
	    		}
	    	}
	    	JobData n=new JobData(this.problem.getJobList().get(index),this.problem.getMultiClusterLength());
	    	int indexAlloc=(problem.getMultiClusterLength()*index)+problem.getPermutationLength();
	    	for(int i=0;i<n.forbidenNodes.length;i++){
				n.forbidenNodes[i]=getVariableValue(indexAlloc+i);
			}
	    	jobs.add(position,n);
	    	convert.set(index, (double)-1);
		}
		return jobs;
	}
	
}
