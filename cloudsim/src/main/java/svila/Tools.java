package svila;

import java.util.Arrays;
import java.util.List;

public class Tools {
	public static Float[] doubleToFloatArray(double[] doubleArray) {
        Float[] floatArray = new Float[doubleArray.length];
        for (int i = 0 ; i < doubleArray.length; i++) {
            floatArray[i] = (float) doubleArray[i];
        }
        return floatArray;
	}
	
	public static double[] floatToDoubleArray(Float[] floatArray) {
        double[] doubleArray = new double[floatArray.length];
        for (int i = 0 ; i < floatArray.length; i++) {
        	doubleArray[i] = floatArray[i];
        }
        return doubleArray;
	}
	
	public static double getMax(Float[] array) {
		float max = array[0];
	    for(int i=1; i<array.length; i++) {
	    	if(max < array[i]) {
	    		max = array[i];
	    	}
	    }
	    return max;
	}
	
	public static void reverseArray(double intArray[]) 
    { 
        double temp;
        int i, size = intArray.length;
        for (i = 0; i < size / 2; i++) { 
            temp = intArray[i]; 
            intArray[i] = intArray[size - i - 1]; 
            intArray[size - i - 1] = temp; 
        }
    }
	
	public static void reverseArray(float intArray[]) 
    { 
		float temp;
        int i, size = intArray.length;
        for (i = 0; i < size / 2; i++) { 
            temp = intArray[i]; 
            intArray[i] = intArray[size - i - 1]; 
            intArray[size - i - 1] = temp; 
        }
    } 
	
	public static void reverseArray(int intArray[]) 
    { 
        int i, temp, size = intArray.length;
        for (i = 0; i < size / 2; i++) { 
            temp = intArray[i]; 
            intArray[i] = intArray[size - i - 1]; 
            intArray[size - i - 1] = temp; 
        }
    }
	
	public static float integrate(float[] x, float[] y) {
		float area = 0.0f;
		float leftX = x[0], leftY = y[0];
		float rightX, rightY;
		for(int i=1; i<x.length; i++) {
			rightX = x[i];
			rightY = y[i];
			area += (leftY+rightY) / 2 * (rightX - leftX); // Area of trapezoid
			leftX = rightX;
			leftY = rightY;
		}
		return area;
	}
	
	public static double integrate(double[] x, double[] y) {
		double area = 0.0f;
		double leftX = x[0], leftY = y[0];
		double rightX, rightY;
		for(int i=1; i<x.length; i++) {
			rightX = x[i];
			rightY = y[i];
			area += (leftY+rightY) / 2 * (rightX - leftX); // Area of trapezoid
			leftX = rightX;
			leftY = rightY;
		}
		return area;
	}
	
	public static Double integrate(List<Double> x, List<Double> y) {
		double area = 0.0f;
		double leftX = x.get(0), leftY = y.get(0);
		double rightX, rightY;
		for(int i=1; i<x.size(); i++) {
			rightX = x.get(i);
			rightY = y.get(i);
			area += (leftY+rightY) / 2 * (rightX - leftX); // Area of trapezoid
			leftX = rightX;
			leftY = rightY;
		}
		return area;
	}
}
