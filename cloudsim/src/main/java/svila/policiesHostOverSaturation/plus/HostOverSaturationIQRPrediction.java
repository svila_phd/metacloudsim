package svila.policiesHostOverSaturation.plus;

import java.util.List;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;
import org.cloudbus.cloudsim.util.MathUtil;

import svila.Tools;
import svila.metricsLogic.CPUCalculator.ForecastingTechnique;
import svila.metricsLogic.CPUCalculator.ForecastingTechniqueFactory;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethod;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethodFactory;
import svila.policiesHostOverSaturation.HostOverSaturationIQR;
import svila.vmOptimizerFix.VMOptimizerPolicy;

public class HostOverSaturationIQRPrediction extends HostOverSaturationIQR {

	public HostOverSaturationIQRPrediction(double safetyParameter, VMOptimizerPolicy fallbackVmAllocationPolicy,
			double utilizationThreshold) {
		super(safetyParameter, fallbackVmAllocationPolicy, utilizationThreshold);
	}

	@Override
	public boolean isHostOverUtilized(PowerHost host) {
		// SERGI
		//System.out.println("IQR: checking host " + host.getId());
		PowerHostUtilizationHistory _host = (PowerHostUtilizationHistory) host;
		double upperThreshold = 0;
		try {
			upperThreshold = 1 - getSafetyParameter() * getHostUtilizationIqr(_host);
		} catch (IllegalArgumentException e) {
			return getFallbackVmAllocationPolicy().isHostOverUtilized(host);
		}
		this.getVmOptimizerPolicy().migrationStatistics.addHistoryEntry(host, upperThreshold);
		
		SignalProcessingMethod signalProcessingMethod = SignalProcessingMethodFactory.getSignalProcessingMethod(SignalProcessingMethodFactory.bollinger);
		ForecastingTechnique forecastingTechnique = ForecastingTechniqueFactory.getForecastingTechnique(ForecastingTechniqueFactory.mean);
		
		PowerHostUtilizationHistory powerHost = (PowerHostUtilizationHistory) host;
		double[] history = powerHost.getUtilizationHistory();		
		Float[] historyFloat = Tools.doubleToFloatArray(history);
		Float[] result = signalProcessingMethod.apply(historyFloat);
		Float[] prediction = forecastingTechnique.apply(result);
		double maxPrediction = Tools.getMax(prediction);
		
		return maxPrediction > upperThreshold;
	}
}
