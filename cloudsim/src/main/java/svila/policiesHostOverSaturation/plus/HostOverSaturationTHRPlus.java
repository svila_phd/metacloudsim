package svila.policiesHostOverSaturation.plus;

import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

import svila.SvilaPrinter;
import svila.vmOptimizerFix.HostOverSaturationPolicy;

public class HostOverSaturationTHRPlus extends HostOverSaturationPolicy {
	/**
	 * The static host CPU utilization threshold to detect over utilization. It is a
	 * percentage value from 0 to 1 that can be changed when creating an instance of
	 * the class.
	 */
	private double utilizationThreshold = 0.9;

	/**
	 * Instantiates a new PowerVmAllocationPolicyMigrationStaticThreshold.
	 * 
	 * @param hostList
	 *            the host list
	 * @param vmSelectionPolicy
	 *            the vm selection policy
	 * @param utilizationThreshold
	 *            the utilization threshold
	 */
	public HostOverSaturationTHRPlus(double utilizationThreshold) {
		super();
		setUtilizationThreshold(utilizationThreshold);
	}

	/**
	 * Checks if a host is over utilized, based on CPU usage.
	 * 
	 * @param host
	 *            the host
	 * @return true, if the host is over utilized; false otherwise
	 */
	@Override
	public boolean isHostOverUtilized(PowerHost host) {
		this.getVmOptimizerPolicy().migrationStatistics.addHistoryEntry(host, getUtilizationThreshold());
		double totalRequestedMips = 0;
		for (Vm vm : host.getVmList()) {
			totalRequestedMips += vm.getCurrentRequestedTotalMips();
		}
		double utilization = totalRequestedMips / host.getTotalMips();
		SvilaPrinter.print(host.getId() + ": " + utilization);
		return utilization > getUtilizationThreshold();
	}

	/**
	 * Sets the utilization threshold.
	 * 
	 * @param utilizationThreshold
	 *            the new utilization threshold
	 */
	protected void setUtilizationThreshold(double utilizationThreshold) {
		this.utilizationThreshold = utilizationThreshold;
	}

	/**
	 * Gets the utilization threshold.
	 * 
	 * @return the utilization threshold
	 */
	public double getUtilizationThreshold() {
		return utilizationThreshold;
	}

	@Override
	public List<PowerHostUtilizationHistory> getOverUtilizedHosts() {
		return this.getVmOptimizerPolicy().getOverUtilizedHostsOriginal();
	}

	@Override
	public boolean isHostOverUtilizedAfterAllocation(PowerHost host, Vm vm) {
		return this.getVmOptimizerPolicy().isHostOverUtilizedAfterAllocationOriginal(host, vm);
	}
}
