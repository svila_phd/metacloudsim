package svila.policiesHostOverSaturation.plus;

import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;
import org.cloudbus.cloudsim.util.MathUtil;

import svila.vmOptimizerFix.HostOverSaturationPolicy;
import svila.vmOptimizerFix.VMOptimizerPolicy;
import svila.vmOptimizerFix.VMSelectionPolicy;

public class HostOverSaturationIQRPlus extends HostOverSaturationPolicy {
	/** The safety parameter in percentage (at scale from 0 to 1).
     * It is a tuning parameter used by the allocation policy to 
     * estimate host utilization (load). The host overload detection is based
     * on this estimation.
     * This parameter is used to tune the estimation
     * to up or down. If the parameter is set as 1.2, for instance, 
     * the estimated host utilization is increased in 20%, giving
     * the host a safety margin of 20% to grow its usage in order to try
     * avoiding SLA violations. As this parameter decreases, more
     * aggressive will be the consolidation (packing) of VMs inside a host,
     * what may lead to optimization of resource usage, but rising of SLA 
     * violations. Thus, the parameter has to be set in order to balance
     * such factors.
     */
	private double safetyParameter = 0;
	
	/** The fallback VM allocation policy to be used when
	     * the IQR over utilization host detection doesn't have
	     * data to be computed. */
	private PowerVmAllocationPolicyMigrationAbstract fallbackVmAllocationPolicy;
	
	/**
	 * Instantiates a new PowerVmAllocationPolicyMigrationInterQuartileRange.
	 * 
	 * @param hostList the host list
	 * @param safetyParameter the safety parameter
	 * @param utilizationThreshold the utilization threshold
	 */
	public HostOverSaturationIQRPlus(
			double safetyParameter,
			VMOptimizerPolicy fallbackVmAllocationPolicy,
			double utilizationThreshold) {
		super();
		setSafetyParameter(safetyParameter);
		setFallbackVmAllocationPolicy(fallbackVmAllocationPolicy);
	}
	
	/**
	 * Checks if the host is over utilized, based on CPU utilization.
	 * 
	 * @param host the host
	 * @return true, if the host is over utilized; false otherwise
	 */
	@Override
	public boolean isHostOverUtilized(PowerHost host) {
		// SERGI
		//System.out.println("IQR: checking host " + host.getId());
		PowerHostUtilizationHistory _host = (PowerHostUtilizationHistory) host;
		double CPUupperThreshold = 0;
		try {
			CPUupperThreshold = 1 - getSafetyParameter() * getHostUtilizationIqr(_host.getCPUUtilizationHistory());
		} catch (IllegalArgumentException e) {
			return getFallbackVmAllocationPolicy().isHostOverUtilized(host);
		}
		this.getVmOptimizerPolicy().migrationStatistics.addHistoryEntry(host, CPUupperThreshold);
		
		double BWupperThreshold = 0;
		try {
			BWupperThreshold = 1 - getSafetyParameter() * getHostUtilizationIqr(_host.getExternalBWUtilizationHistory());
		} catch (IllegalArgumentException e) {
			return getFallbackVmAllocationPolicy().isHostOverUtilized(host);
		}
		
		
		return CPUupperThreshold > 1.0 || BWupperThreshold > 1.0;
	}
	
	public boolean isHostOverUtilizedByBW(PowerHost host) {
		PowerHostUtilizationHistory _host = (PowerHostUtilizationHistory) host;
		double BWupperThreshold = 0;
		try {
			BWupperThreshold = 1 - getSafetyParameter() * getHostUtilizationIqr(_host.getExternalBWUtilizationHistory());
		} catch (IllegalArgumentException e) {
			return getFallbackVmAllocationPolicy().isHostOverUtilized(host);
		}
		
		return BWupperThreshold > 1.0;
	}
	
	/**
	 * Gets the host CPU utilization percentage IQR.
	 * 
	 * @param host the host
	 * @return the host CPU utilization percentage IQR
	 */
	protected double getHostUtilizationIqr(PowerHostUtilizationHistory host) throws IllegalArgumentException {
		double[] data = host.getUtilizationHistory();
		if (MathUtil.countNonZeroBeginning(data) >= 12) { // 12 has been suggested as a safe value
			return MathUtil.iqr(data);
		}
		throw new IllegalArgumentException();
	}
	
	protected double getHostUtilizationIqr(double[] resourceUtilization) throws IllegalArgumentException {
		double[] data = resourceUtilization;
		if (MathUtil.countNonZeroBeginning(data) >= 12) { // 12 has been suggested as a safe value
			return MathUtil.iqr(data);
		}
		throw new IllegalArgumentException();
	}
	
	/**
	 * Sets the safety parameter.
	 * 
	 * @param safetyParameter the new safety parameter
	 */
	protected void setSafetyParameter(double safetyParameter) {
		if (safetyParameter < 0) {
			Log.printConcatLine("The safety parameter cannot be less than zero. The passed value is: ",
					safetyParameter);
			System.exit(0);
		}
		this.safetyParameter = safetyParameter;
	}
	
	/**
	 * Gets the safety parameter.
	 * 
	 * @return the safety parameter
	 */
	protected double getSafetyParameter() {
		return safetyParameter;
	}
	
	/**
	 * Sets the fallback vm allocation policy.
	 * 
	 * @param fallbackVmAllocationPolicy the new fallback vm allocation policy
	 */
	public void setFallbackVmAllocationPolicy(
			PowerVmAllocationPolicyMigrationAbstract fallbackVmAllocationPolicy) {
		this.fallbackVmAllocationPolicy = fallbackVmAllocationPolicy;
	}
	
	/**
	 * Gets the fallback vm allocation policy.
	 * 
	 * @return the fallback vm allocation policy
	 */
	public PowerVmAllocationPolicyMigrationAbstract getFallbackVmAllocationPolicy() {
		return fallbackVmAllocationPolicy;
	}

	@Override
	public List<PowerHostUtilizationHistory> getOverUtilizedHosts() {
		return this.getVmOptimizerPolicy().getOverUtilizedHostsOriginal();
	}

	@Override
	public boolean isHostOverUtilizedAfterAllocation(PowerHost host, Vm vm) {
		return this.getVmOptimizerPolicy().isHostOverUtilizedAfterAllocationOriginal(host, vm);
	}
}
