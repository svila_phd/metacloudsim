package svila.policiesHostOverSaturation;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;

import svila.vmOptimizerFix.HostOverSaturationPolicy;
import svila.vmOptimizerFix.VMOptimizerPolicy;

public abstract class HostOverSaturationBase extends HostOverSaturationPolicy {

	private double safetyParameter = 0;
	private PowerVmAllocationPolicyMigrationAbstract fallbackVmAllocationPolicy;
	
	public HostOverSaturationBase(
			double safetyParameter,
			VMOptimizerPolicy fallbackVmAllocationPolicy,
			double utilizationThreshold) {

		setSafetyParameter(safetyParameter);
		setFallbackVmAllocationPolicy(fallbackVmAllocationPolicy);
	}
	
	protected void setSafetyParameter(double safetyParameter) {
		if (safetyParameter < 0) {
			Log.printConcatLine("The safety parameter cannot be less than zero. The passed value is: ",
					safetyParameter);
			System.exit(0);
		}
		this.safetyParameter = safetyParameter;
	}

	/**
	 * Gets the safety parameter.
	 * 
	 * @return the safety parameter
	 */
	protected double getSafetyParameter() {
		return safetyParameter;
	}
	
	public void setFallbackVmAllocationPolicy(
			PowerVmAllocationPolicyMigrationAbstract fallbackVmAllocationPolicy) {
		this.fallbackVmAllocationPolicy = fallbackVmAllocationPolicy;
	}
	
	public PowerVmAllocationPolicyMigrationAbstract getFallbackVmAllocationPolicy() {
		return fallbackVmAllocationPolicy;
	}
	
	@Override
	public abstract boolean isHostOverUtilized(PowerHost host);

	@Override
	public List<PowerHostUtilizationHistory> getOverUtilizedHosts() {
		return this.getVmOptimizerPolicy().getOverUtilizedHostsOriginal();
	}

	@Override
	public abstract boolean isHostOverUtilizedAfterAllocation(PowerHost host, Vm vm);
}
