package svila.policiesHostOverSaturation.future;

import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.util.MathUtil;

import svila.policiesVmOptimizer.future.FutureHost;

public class FutureHostOverSaturationIQR extends FutureHostOverSaturationBase {

	public FutureHostOverSaturationIQR(
			double safetyParameter,
			FutureHostOverSaturationBase fallbackOverSaturationPolicy) {
		super(safetyParameter, fallbackOverSaturationPolicy);
		setSafetyParameter(safetyParameter);
		setFallbackVmAllocationPolicy(fallbackOverSaturationPolicy);
	}

	@Override
	public OverSaturationData getOverSaturationData(FutureHost host) {

		PowerHostUtilizationHistory _host = host.getCurrentHost();
		double upperThreshold = 0;
		try {
			upperThreshold = 1 - getSafetyParameter() * getHostUtilizationIqr(_host);
		} catch (IllegalArgumentException e) {
			return getFallbackVmAllocationPolicy().getOverSaturationData(host);
		}
		this.vmOptimizerPolicy.migrationStatistics.addHistoryEntry(host.getCurrentHost(), upperThreshold);

		double utilization = host.getForecastedPercCPUWithMigrations();
		OverSaturationData osd = new OverSaturationData(host, upperThreshold, utilization);
		return osd;
	}
	
	protected double getHostUtilizationIqr(PowerHostUtilizationHistory host) throws IllegalArgumentException {
		double[] data = host.getUtilizationHistory();
		if (MathUtil.countNonZeroBeginning(data) >= 12) { // 12 has been suggested as a safe value
			return MathUtil.iqr(data);
		}
		throw new IllegalArgumentException();
	}
}
