package svila.policiesHostOverSaturation.future;

import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;

import svila.planetlabNetwork.StaticResources;
import svila.policiesFactories.HostOverSaturationPolicyFactory;
import svila.policiesHostOverSaturation.HostOverSaturationLRR;
import svila.policiesVmOptimizer.future.FutureHost;

public class FutureHostOverSaturationLRR extends FutureHostOverSaturationLR {

	private HostOverSaturationLRR hostOverSaturationLRR;
	
	public FutureHostOverSaturationLRR(
			double safetyParameter,
			int historyLength,
			int forwardSteps,
			FutureHostOverSaturationBase fallbackOverSaturationPolicy) {
		super(safetyParameter, historyLength, forwardSteps, fallbackOverSaturationPolicy);
		this.hostOverSaturationLRR = new HostOverSaturationLRR(
				safetyParameter,
				300.0,
				HostOverSaturationPolicyFactory.getHostOverSaturationPolicy("thr"),
				StaticResources.getCE().getUtilizationThreshold()
				);
	}
	
	@Override
	public OverSaturationData getOverSaturationData(FutureHost host) {
		PowerHostUtilizationHistory _host = (PowerHostUtilizationHistory) host.getCurrentHost();
		double[] utilizationHistory = _host.getUtilizationHistory();
		int length = 10; // we use 10 to make the regression responsive enough to latest values
		if (utilizationHistory.length < length) {
			return getFallbackVmAllocationPolicy().getOverSaturationData(host);
		}
		double[] utilizationHistoryReversed = new double[length];
		for (int i = 0; i < length; i++) {
			utilizationHistoryReversed[i] = utilizationHistory[length - i - 1];
		}
		double[] estimates = null;
		try {
			estimates = this.hostOverSaturationLRR.getParameterEstimates(utilizationHistoryReversed);
		} catch (IllegalArgumentException e) {
			return getFallbackVmAllocationPolicy().getOverSaturationData(host);
		}
		//double migrationIntervals = Math.ceil(this.hostOverSaturationLR.getMaximumVmMigrationTime(_host) / this.hostOverSaturationLR.getSchedulingInterval());
		//double predictedUtilization = estimates[0] + estimates[1] * (length + migrationIntervals);
		double predictedUtilization = estimates[0] + estimates[1] * (historyLength + forwardSteps);

		predictedUtilization *= getSafetyParameter();

		OverSaturationData osd = new OverSaturationData(host, 1, predictedUtilization);
		return osd;
	}
}
