package svila.policiesHostOverSaturation.future;

import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;

import svila.planetlabNetwork.StaticResources;
import svila.policiesFactories.HostOverSaturationPolicyFactory;
import svila.policiesHostOverSaturation.HostOverSaturationLR;
import svila.policiesVmOptimizer.future.FutureHost;

public class FutureHostOverSaturationLR extends FutureHostOverSaturationBase {

	private HostOverSaturationLR hostOverSaturationLR;
	int historyLength;
	int forwardSteps;
	
	public FutureHostOverSaturationLR(
			double safetyParameter,
			int historyLength,
			int forwardSteps,
			FutureHostOverSaturationBase fallbackOverSaturationPolicy) {
		super(safetyParameter, fallbackOverSaturationPolicy);
		setSafetyParameter(safetyParameter);
		setFallbackVmAllocationPolicy(fallbackOverSaturationPolicy);
		this.historyLength = historyLength;
		this.forwardSteps = forwardSteps;
		this.hostOverSaturationLR = new HostOverSaturationLR(
				safetyParameter,
				300.0,
				HostOverSaturationPolicyFactory.getHostOverSaturationPolicy("thr"),
				StaticResources.getCE().getUtilizationThreshold()
				);
	}
	
	@Override
	public OverSaturationData getOverSaturationData(FutureHost host) {
		PowerHostUtilizationHistory _host = (PowerHostUtilizationHistory) host.getCurrentHost();
		double[] utilizationHistory = _host.getUtilizationHistory();
		int length = 10; // we use 10 to make the regression responsive enough to latest values
		if (utilizationHistory.length < historyLength) {
			return getFallbackVmAllocationPolicy().getOverSaturationData(host);
		}
		double[] utilizationHistoryReversed = new double[historyLength];
		for (int i = 0; i < historyLength; i++) {
			utilizationHistoryReversed[i] = utilizationHistory[historyLength - i - 1];
		}
		double[] estimates = null;
		try {
			estimates = this.hostOverSaturationLR.getParameterEstimates(utilizationHistoryReversed);
		} catch (IllegalArgumentException e) {
			return getFallbackVmAllocationPolicy().getOverSaturationData(host);
		}
		//double migrationIntervals = Math.ceil(this.hostOverSaturationLR.getMaximumVmMigrationTime(_host) / this.hostOverSaturationLR.getSchedulingInterval());
		//double predictedUtilization = estimates[0] + estimates[1] * (length + migrationIntervals);
		// Y = mx + b
		double predictedUtilization = estimates[0] + estimates[1] * (historyLength + forwardSteps);
		
		predictedUtilization *= getSafetyParameter();

		OverSaturationData osd = new OverSaturationData(host, 1, predictedUtilization);
		return osd;
	}
}
