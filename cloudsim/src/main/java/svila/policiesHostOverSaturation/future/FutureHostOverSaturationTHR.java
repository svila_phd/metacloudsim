package svila.policiesHostOverSaturation.future;

import org.cloudbus.cloudsim.Vm;

import svila.policiesVmOptimizer.future.FutureHost;

public class FutureHostOverSaturationTHR extends FutureHostOverSaturationBase {

	private double utilizationThreshold = 0.9;
	
	public FutureHostOverSaturationTHR(double utilizationThreshold) {
		super();
		setUtilizationThreshold(utilizationThreshold);
	}

	@Override
	public OverSaturationData getOverSaturationData(FutureHost host) {
		this.vmOptimizerPolicy.migrationStatistics.addHistoryEntry(host.getCurrentHost(), getUtilizationThreshold());
		double utilization = host.getForecastedPercCPUWithMigrations();
		OverSaturationData osd = new OverSaturationData(host, getUtilizationThreshold(), utilization);
		return osd;
	}
	
	protected void setUtilizationThreshold(double utilizationThreshold) {
		this.utilizationThreshold = utilizationThreshold;
	}
	
	public double getUtilizationThreshold() {
		return utilizationThreshold;
	}

}
