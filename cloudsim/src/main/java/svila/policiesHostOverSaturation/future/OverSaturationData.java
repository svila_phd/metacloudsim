package svila.policiesHostOverSaturation.future;

import svila.policiesVmOptimizer.future.FutureHost;

public class OverSaturationData {
	public FutureHost futureHost;
	public double upperThreshold;
	public double futureUtilization;
	public boolean existUpperThresholdSaturation;
	public boolean existSaturationOver100Perc;
	
	public OverSaturationData(FutureHost futureHost, double upperThreshold, double futureUtilization) {
		this.futureHost = futureHost;
		this.upperThreshold = upperThreshold;
		this.futureUtilization = futureUtilization;
		this.existUpperThresholdSaturation = futureUtilization > upperThreshold;
		this.existSaturationOver100Perc = futureUtilization > 1.0;
	}
}
