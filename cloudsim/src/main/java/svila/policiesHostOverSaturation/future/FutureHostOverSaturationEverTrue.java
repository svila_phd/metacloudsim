package svila.policiesHostOverSaturation.future;

import svila.policiesVmOptimizer.future.FutureHost;

public class FutureHostOverSaturationEverTrue extends FutureHostOverSaturationBase {

	@Override
	public OverSaturationData getOverSaturationData(FutureHost host) {
		return new OverSaturationData(host, 0, host.getForecastedPercCPUWithMigrations());
	}

}
