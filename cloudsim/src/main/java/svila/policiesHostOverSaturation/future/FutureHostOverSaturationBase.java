package svila.policiesHostOverSaturation.future;

import java.util.HashMap;
import java.util.List;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;

import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;
import svila.vmOptimizerFix.VMOptimizerPolicy;
import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public abstract class FutureHostOverSaturationBase {
	FutureVMOptimizerPolicy vmOptimizerPolicy;
	protected double safetyParameter = 0;
	protected FutureHostOverSaturationBase fallbackVmAllocationPolicy;
	
	public FutureHostOverSaturationBase() {

	}
	
	public FutureHostOverSaturationBase(
			double safetyParameter,
			FutureHostOverSaturationBase fallbackOverSaturationPolicy) {

		setSafetyParameter(safetyParameter);
		setFallbackVmAllocationPolicy(fallbackVmAllocationPolicy);
	}
	
	protected void setSafetyParameter(double safetyParameter) {
		if (safetyParameter < 0) {
			Log.printConcatLine("The safety parameter cannot be less than zero. The passed value is: ",
					safetyParameter);
			System.exit(0);
		}
		this.safetyParameter = safetyParameter;
	}

	/**
	 * Gets the safety parameter.
	 * 
	 * @return the safety parameter
	 */
	protected double getSafetyParameter() {
		return safetyParameter;
	}
	
	public void setVMOptimizerPolicy(FutureVMOptimizerPolicy futureVMOptimizerPolicy) {
		this.vmOptimizerPolicy = futureVMOptimizerPolicy;
		if(this.fallbackVmAllocationPolicy != null) {
			this.fallbackVmAllocationPolicy.vmOptimizerPolicy = futureVMOptimizerPolicy;
		}
	}

	public FutureVMOptimizerPolicy getVMOptimizerPolicy() {
		return this.vmOptimizerPolicy;
	}
	
	public void setFallbackVmAllocationPolicy(
			FutureHostOverSaturationBase fallbackVmAllocationPolicy) {
		this.fallbackVmAllocationPolicy = fallbackVmAllocationPolicy;
	}
	
	public FutureHostOverSaturationBase getFallbackVmAllocationPolicy() {
		return fallbackVmAllocationPolicy;
	}
	
	public boolean isHostOverUtilized(FutureHost host) {
		OverSaturationData osd = getOverSaturationData(host);
		return osd.existUpperThresholdSaturation;
	}
	
	public abstract OverSaturationData getOverSaturationData(FutureHost host);

	public HashMap<Integer, FutureHost> getOverUtilizedHosts(HashMap<Integer, FutureHost> futureHosts) {
		HashMap<Integer, FutureHost> overUtilizedHosts = new HashMap<Integer, FutureHost>();
		for(FutureHost host : futureHosts.values()) {
			OverSaturationData osd = this.getOverSaturationData(host);
			if(osd.existUpperThresholdSaturation) {
				overUtilizedHosts.put(host.getCurrentHost().getNetworkId(), host);
			}
		}
		return overUtilizedHosts;
	}

	public OverSaturationData getOverSaturationDataAfterAllocation(FutureHost host, FutureVm vm) {
		host.putVm(vm);
		OverSaturationData osd = getOverSaturationData(host);
		host.clearPutVm(vm);
		return osd;
	}
	
	public double getPowerAfterAllocation(FutureHost host, FutureVm vm) {
		double power = 0;
		try {
			power = host.getCurrentHost().getPowerModel().getPower(getMaxUtilizationAfterAllocation(host, vm));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		return power;
	}
	
	public double getMaxUtilizationAfterAllocation(FutureHost host, FutureVm vm) {
		double requestedTotalMips = vm.getForecastedMIPS();
		double hostUtilizationMips = host.getForecastedMIPSWithMigrations();
		double hostPotentialUtilizationMips = hostUtilizationMips + requestedTotalMips;
		double pePotentialUtilization = hostPotentialUtilizationMips / host.getCurrentHost().getTotalMips();
		return pePotentialUtilization;
		
		/* Original
		double requestedTotalMips = vm.getCurrentRequestedTotalMips();
		double hostUtilizationMips = getUtilizationOfCpuMips(host);
		double hostPotentialUtilizationMips = hostUtilizationMips + requestedTotalMips;
		double pePotentialUtilization = hostPotentialUtilizationMips / host.getTotalMips();
		return pePotentialUtilization;
		*/
	}
}
