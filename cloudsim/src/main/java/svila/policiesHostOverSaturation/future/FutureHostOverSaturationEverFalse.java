package svila.policiesHostOverSaturation.future;

import svila.policiesVmOptimizer.future.FutureHost;

public class FutureHostOverSaturationEverFalse extends FutureHostOverSaturationBase {

	@Override
	public OverSaturationData getOverSaturationData(FutureHost host) {
		return new OverSaturationData(host, 1, 0);
	}

}
