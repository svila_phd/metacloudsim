package svila.policiesHostOverSaturation.future;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.util.MathUtil;

import svila.policiesVmOptimizer.future.FutureHost;

public class FutureHostOverSaturationMAD extends FutureHostOverSaturationBase {
	public FutureHostOverSaturationMAD(
			double safetyParameter,
			FutureHostOverSaturationBase fallbackOverSaturationPolicy) {
		super(safetyParameter, fallbackOverSaturationPolicy);
		setSafetyParameter(safetyParameter);
		setFallbackVmAllocationPolicy(fallbackOverSaturationPolicy);
	}

	@Override
	public OverSaturationData getOverSaturationData(FutureHost host) {
		/*if(host.getCurrentHost().getId() == 14 && CloudSim.clock() >= 55200.0 && CloudSim.clock() < 59000.0) {
			System.out.println("");
		}*/
		
		PowerHostUtilizationHistory _host = (PowerHostUtilizationHistory) host.getCurrentHost();
		double upperThreshold = 0;
		try {
    			upperThreshold = 1 - getSafetyParameter() * getHostUtilizationMad(_host);
		} catch (IllegalArgumentException e) {
			return getFallbackVmAllocationPolicy().getOverSaturationData(host);
		}
		this.vmOptimizerPolicy.migrationStatistics.addHistoryEntry(host.getCurrentHost(), upperThreshold);
		
		double utilization = host.getForecastedPercCPUWithMigrations();
		OverSaturationData osd = new OverSaturationData(host, upperThreshold, utilization);
		return osd;
	}
	
	protected double getHostUtilizationMad(PowerHostUtilizationHistory host) throws IllegalArgumentException {
		double[] data = host.getUtilizationHistory();
		if (MathUtil.countNonZeroBeginning(data) >= 12) { // 12 has been suggested as a safe value
			return MathUtil.mad(data);
		}
		throw new IllegalArgumentException();
	}
}
