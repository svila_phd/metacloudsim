package svila.policiesHostOverSaturation.hostHistory;

import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.util.MathUtil;

public class UtilizationHistory {
	
	
	
	/**
	 * Gets the host CPU utilization percentage history.
	 * 
	 * @return the host CPU utilization percentage history
	 */
	public double[] getUtilizationHistoryOriginal() {
		double[] utilizationHistory = new double[PowerVm.HISTORY_LENGTH];
		double hostMips = getTotalMips();
		for (PowerVm vm : this.<PowerVm> getVmList()) {
			for (int i = 0; i < vm.getUtilizationHistory().size(); i++) {
				utilizationHistory[i] += vm.getUtilizationHistory().get(i) * vm.getMips() / hostMips;
			}
		}
		return MathUtil.trimZeroTail(utilizationHistory);
	}
}
