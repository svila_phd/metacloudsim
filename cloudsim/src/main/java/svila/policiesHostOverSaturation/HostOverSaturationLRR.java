package svila.policiesHostOverSaturation;

import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.util.MathUtil;

import svila.vmOptimizerFix.HostOverSaturationPolicy;

/**
 * A VM allocation policy that uses Local Regression Robust (LRR) to predict host utilization (load)
 * and define if a host is overloaded or not.
 * 
 * <br/>If you are using any algorithms, policies or workload included in the power package please cite
 * the following paper:<br/>
 * 
 * <ul>
 * <li><a href="http://dx.doi.org/10.1002/cpe.1867">Anton Beloglazov, and Rajkumar Buyya, "Optimal Online Deterministic Algorithms and Adaptive
 * Heuristics for Energy and Performance Efficient Dynamic Consolidation of Virtual Machines in
 * Cloud Data Centers", Concurrency and Computation: Practice and Experience (CCPE), Volume 24,
 * Issue 13, Pages: 1397-1420, John Wiley & Sons, Ltd, New York, USA, 2012</a>
 * </ul>
 * 
 * @author Anton Beloglazov
 * @since CloudSim Toolkit 3.0
 */

public class HostOverSaturationLRR extends HostOverSaturationLR {
	/**
	 * Instantiates a new PowerVmAllocationPolicyMigrationLocalRegressionRobust.
	 * 
	 * @param hostList the host list
	 * @param vmSelectionPolicy the vm selection policy
	 * @param schedulingInterval the scheduling interval
	 * @param fallbackVmAllocationPolicy the fallback vm allocation policy
	 * @param utilizationThreshold the utilization threshold
	 */
	public HostOverSaturationLRR(
			//List<? extends Host> hostList,
			//PowerVmSelectionPolicy vmSelectionPolicy,
			double safetyParameter,
			double schedulingInterval,
			HostOverSaturationPolicy fallbackVmAllocationPolicy,
			double utilizationThreshold) {
		super(
				//hostList,
				///vmSelectionPolicy,
				safetyParameter,
				schedulingInterval,
				fallbackVmAllocationPolicy,
				utilizationThreshold);
	}

	/**
	 * Instantiates a new PowerVmAllocationPolicyMigrationLocalRegressionRobust.
	 * 
	 * @param hostList the host list
	 * @param vmSelectionPolicy the vm selection policy
	 * @param schedulingInterval the scheduling interval
	 * @param fallbackVmAllocationPolicy the fallback vm allocation policy
	 */
	public HostOverSaturationLRR(
			List<? extends Host> hostList,
			//PowerVmSelectionPolicy vmSelectionPolicy,
			double safetyParameter,
			double schedulingInterval,
			HostOverSaturationPolicy fallbackVmAllocationPolicy) {
		super(//hostList,
			  //vmSelectionPolicy,
			  safetyParameter,
			  schedulingInterval,
			  fallbackVmAllocationPolicy);
	}

	/**
	 * Gets the utilization estimates.
	 * 
	 * @param utilizationHistoryReversed the utilization history reversed
	 * @return the utilization estimates
	 */
	@Override
	public double[] getParameterEstimates(double[] utilizationHistoryReversed) {
		return MathUtil.getRobustLoessParameterEstimates(utilizationHistoryReversed);
	}
	
	@Override
	public List<PowerHostUtilizationHistory> getOverUtilizedHosts() {
		return this.getVmOptimizerPolicy().getOverUtilizedHostsOriginal();
	}

	@Override
	public boolean isHostOverUtilizedAfterAllocation(PowerHost host, Vm vm) {
		return this.getVmOptimizerPolicy().isHostOverUtilizedAfterAllocationOriginal(host, vm);
	}
}
