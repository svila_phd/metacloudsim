package svila.policiesHostOverSaturation;

import java.util.List;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;

import svila.Tools;
import svila.metricsLogic.CPUCalculator.ForecastingTechnique;
import svila.metricsLogic.CPUCalculator.ForecastingTechniqueFactory;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethod;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethodFactory;
import svila.vmOptimizerFix.HostOverSaturationPolicy;

public class HostOverSaturationSmoothingForecasting extends HostOverSaturationPolicy {

	SignalProcessingMethod signalProcessingMethod;
	ForecastingTechnique forecastingTechnique;

	public HostOverSaturationSmoothingForecasting(SignalProcessingMethod signalProcessingMethod,
			ForecastingTechnique forecastingTechnique) {
		this.signalProcessingMethod = signalProcessingMethod;
		this.forecastingTechnique = forecastingTechnique;
	}

	@Override
	public boolean isHostOverUtilized(PowerHost host) {
		PowerHostUtilizationHistory powerHost = (PowerHostUtilizationHistory) host;
		double[] history = powerHost.getUtilizationHistory();		
		Float[] historyFloat = Tools.doubleToFloatArray(history);
		Float[] result = signalProcessingMethod.apply(historyFloat);
		Float[] prediction = forecastingTechnique.apply(result);
		double maxPrediction = Tools.getMax(prediction);
		System.out.println("------ History");
		for(int i=0; i<history.length; i++) {
			System.out.println(history[i]);
		}
		System.out.println("------ Result");
		for(int i=0; i<result.length; i++) {
			System.out.println(result[i]);
		}
		System.out.println("------");
		
		return maxPrediction >= 1;
	}

	@Override
	public List<PowerHostUtilizationHistory> getOverUtilizedHosts() {
		return this.getVmOptimizerPolicy().getOverUtilizedHostsOriginal();
	}

	@Override
	public boolean isHostOverUtilizedAfterAllocation(PowerHost host, Vm vm) {
		return this.getVmOptimizerPolicy().isHostOverUtilizedAfterAllocationOriginal(host, vm);
	}

}
