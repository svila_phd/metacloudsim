package svila.policiesHostOverSaturation;

import java.util.List;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;

import svila.vmOptimizerFix.HostOverSaturationPolicy;

public class HostOverSaturationEverTrue extends HostOverSaturationPolicy {

	@Override
	public boolean isHostOverUtilized(PowerHost host) {
		return true;
	}

	@Override
	public List<PowerHostUtilizationHistory> getOverUtilizedHosts() {
		return this.getVmOptimizerPolicy().getOverUtilizedHostsOriginal();
	}

	@Override
	public boolean isHostOverUtilizedAfterAllocation(PowerHost host, Vm vm) {
		return false;
	}

}
