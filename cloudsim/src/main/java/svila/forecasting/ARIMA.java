package svila.forecasting;

import com.workday.insights.timeseries.arima.Arima;
import com.workday.insights.timeseries.arima.struct.ArimaParams;
import com.workday.insights.timeseries.arima.struct.ForecastResult;

public class ARIMA {
	ArimaParams params;
	
	public ARIMA(ArimaParams params) {
		this.params = params;
	}
	
	public ForecastResult applyForecast(double[] data, int forecastSize) {
		return Arima.forecast_arima(data, forecastSize, params);
	}
}
