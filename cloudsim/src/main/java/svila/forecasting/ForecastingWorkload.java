package svila.forecasting;

import java.io.IOException;
import java.util.Arrays;

import org.cloudbus.cloudsim.UtilizationModelPlanetLabInMemory;
import org.cloudbus.cloudsim.examples.power.Constants;

import com.workday.insights.timeseries.arima.Arima;
import com.workday.insights.timeseries.arima.struct.ArimaParams;
import com.workday.insights.timeseries.arima.struct.ForecastResult;

import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.plotly.Plot;
import tech.tablesaw.plotly.components.Figure;
import tech.tablesaw.plotly.traces.ScatterTrace;

public class ForecastingWorkload {
	
	public class TimeSeries {
		double x[];
		double y[];
		
		public TimeSeries(double[] x, double[] y) {
			this.x = x;
			this.y = y;
		}
	}
	
	public static void main(String[] args) {
		
		// Load workload
		
		String pathName = "C:\\Users\\Sergi\\Documents\\networkExperiments\\workloads\\planetlab\\20110303\\";
		String file = "146-179_surfsnel_dsl_internl_net_colostate_557";
		pathName += file;
		
		UtilizationModelPlanetLabInMemory utilization = null;
		try {
			utilization = new UtilizationModelPlanetLabInMemory(
					pathName, Constants.SCHEDULING_INTERVAL);
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		double[] realData = utilization.getData();
		DoubleColumn realColumn = DoubleColumn.create("real", realData);
		DoubleColumn forecastingColumn = DoubleColumn.create("forecasting", realData.length);
		Table t = Table.create("data", realColumn, forecastingColumn);
		
		int startReal=0;
		int endReal=250;
		
		// Prepare input timeseries data.
		double[] dataArray = realColumn.inRange(startReal, endReal).asDoubleArray();
		
		// Set ARIMA model parameters.
		int p = 3;
		int d = 0;
		int q = 3;
		int P = 1;
		int D = 1;
		int Q = 0;
		int m = 0;
		int forecastSize = 6;

		ArimaParams arimaParams = new ArimaParams(p, d, q, P, D, Q, m);
		// Obtain forecast result. The structure contains forecasted values and performance metric etc.
		ForecastResult forecastResult = Arima.forecast_arima(dataArray, forecastSize, arimaParams);

		// Read forecast values
		double[] forecastData = forecastResult.getForecast();

		// You can obtain upper- and lower-bounds of confidence intervals on forecast values.
		// By default, it computes at 95%-confidence level. This value can be adjusted in ForecastUtil.java
		double[] uppers = forecastResult.getForecastUpperConf();
		double[] lowers = forecastResult.getForecastLowerConf();

		// You can also obtain the root mean-square error as validation metric.
		double rmse = forecastResult.getRMSE();

		// It also provides the maximum normalized variance of the forecast values and their confidence interval.
		double maxNormalizedVariance = forecastResult.getMaxNormalizedVariance();

		// Finally you can read log messages.
		String log = forecastResult.getLog();
		
		int startForecasting = endReal;
		int endForecasting = startForecasting + forecastSize;
		
		int startOffset = forecastSize;
		int endOffset = forecastSize;
		int startCut = Math.max(0, startReal-startOffset);
		int endCut = Math.min(realData.length, endForecasting + endOffset);
		double[] realCut = realColumn.inRange(startCut, endCut).asDoubleArray();
		
		double[] xCut = new double[realCut.length];
		Arrays.setAll(xCut, i -> i+startCut);
		
		double[] xForecast = new double[forecastSize];
		Arrays.setAll(xForecast, i -> i + startForecasting);
		
		System.out.println("rmse: " + rmse);
		System.out.println("maxNormalizedVariance: " + maxNormalizedVariance);
		System.out.println(log);
		
		ScatterTrace realTrace = ScatterTrace
				.builder(xCut, realCut)
				.mode(ScatterTrace.Mode.LINE)
				.name("%")
				.build();
		ScatterTrace forecastingTrace = ScatterTrace
				.builder(xForecast, forecastData)
				.mode(ScatterTrace.Mode.LINE)
				.name("Forecasting")
				.build();
		ScatterTrace uppersTrace = ScatterTrace
				.builder(xForecast, uppers)
				.mode(ScatterTrace.Mode.LINE)
				.name("Uppers")
				.build();
		ScatterTrace lowersTrace = ScatterTrace
				.builder(xForecast, lowers)
				.mode(ScatterTrace.Mode.LINE)
				.name("Lowers")
				.build();
		
		Plot.show(new Figure(realTrace, forecastingTrace, uppersTrace, lowersTrace));
	}
}
