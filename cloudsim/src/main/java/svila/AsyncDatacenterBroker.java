package svila;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;
import org.cloudbus.cloudsim.lists.VmList;

import svila.staticAllocation.schedulingTechniques.StaticTaskSchedulingTechnique;

/**
 *
 * @author Sergi
 */
public class AsyncDatacenterBroker extends DatacenterBroker {
    
	StaticTaskSchedulingTechnique technique;
	boolean externalSchedule;
	
    public AsyncDatacenterBroker(String name) throws Exception {
        super(name);
        externalSchedule = false;
    }
    
    public void setTechnique(StaticTaskSchedulingTechnique technique) {
    	this.technique = technique;
    }
    
    public void setExternalSchedule(boolean externalSchedule) {
    	this.externalSchedule = externalSchedule;
    }

    /**
	 * Submit cloudlets to the created VMs.
	 * 
	 * @pre $none
	 * @post $none
         * @see #submitCloudletList(java.util.List) 
	 */
    @Override
	protected void submitCloudlets() {
    	if(!externalSchedule) {
    		technique.execute();
    	}
    	
		int vmIndex = 0;
		List<Cloudlet> successfullySubmitted = new ArrayList<Cloudlet>();
		for (Cloudlet cloudlet : getCloudletList()) {
			Vm vm;
			// if user didn't bind this cloudlet and it has not been executed yet
			if (cloudlet.getVmId() == -1) {
				vm = getVmsCreatedList().get(vmIndex);
			} else { // submit to the specific vm
				vm = VmList.getById(getVmsCreatedList(), cloudlet.getVmId());
				if (vm == null) { // vm was not created
					if(!Log.isDisabled()) {				    
					    Log.printConcatLine(CloudSim.clock(), ": ", getName(), ": Postponing execution of cloudlet ",
							cloudlet.getCloudletId(), ": bount VM not available");
					}
					continue;
				}
			}

			if (!Log.isDisabled()) {
			    Log.printConcatLine(CloudSim.clock(), ": ", getName(), ": Sending cloudlet ",
					cloudlet.getCloudletId(), " to VM #", vm.getId());
			}
			
			cloudlet.setVmId(vm.getId());

			SvilaPrinter.print("Cloudlet " + cloudlet.getCloudletId() + " Start time: " + cloudlet.getExecStartTime());
            
            send(getVmsToDatacentersMap().get(vm.getId()), cloudlet.getExecStartTime(), CloudSimTags.CLOUDLET_SUBMIT, cloudlet);
			cloudletsSubmitted++;
			vmIndex = (vmIndex + 1) % getVmsCreatedList().size();
			getCloudletSubmittedList().add(cloudlet);
			successfullySubmitted.add(cloudlet);
		}

		// remove submitted cloudlets from waiting list
		getCloudletList().removeAll(successfullySubmitted);
	}
    
}
