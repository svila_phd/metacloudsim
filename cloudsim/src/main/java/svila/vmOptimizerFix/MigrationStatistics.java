package svila.vmOptimizerFix;

import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.HostDynamicWorkload;

public class MigrationStatistics {
	
	VMOptimizerPolicy vmOptimizerPolicy;

	public VMOptimizerPolicy getVmOptimizerPolicy() {
		return vmOptimizerPolicy;
	}

	public void setVmOptimizerPolicy(VMOptimizerPolicy vmOptimizerPolicy) {
		this.vmOptimizerPolicy = vmOptimizerPolicy;
	}

	public Map<Integer, List<Double>> getMetricHistory() {
		return vmOptimizerPolicy.getMetricHistoryOriginal();
	}

	public Map<Integer, List<Double>> getTimeHistory() {
		return vmOptimizerPolicy.getTimeHistoryOriginal();
	}

	public List<Double> getExecutionTimeHistoryHostSelection() {
		return vmOptimizerPolicy.getExecutionTimeHistoryHostSelectionOriginal();
	}

	public List<Double> getExecutionTimeHistoryVmSelection() {
		return vmOptimizerPolicy.getExecutionTimeHistoryVmSelectionOriginal();
	}

	public List<Double> getExecutionTimeHistoryVmReallocation() {
		return vmOptimizerPolicy.getExecutionTimeHistoryVmReallocationOriginal();
	}

	public List<Double> getExecutionTimeHistoryTotal() {
		return vmOptimizerPolicy.getExecutionTimeHistoryTotalOriginal();
	}

	public void addHistoryEntry(HostDynamicWorkload host, double metric) {
		vmOptimizerPolicy.addHistoryEntryOriginal(host, metric);
	}
	
	
	
}
