package svila.vmOptimizerFix;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

public abstract class VMOptimizerPolicy extends PowerVmAllocationPolicyMigrationAbstract {
	public HostOverSaturationPolicy hostOverSaturationPolicy; 
	public HostUnderUtilisationPolicy hostUnderUtilisationPolicy;
	public VMSelectionPolicy vmSelPolicy;
	public VMMigrationPolicy vmMigrationPolicy;
	public MigrationStatistics migrationStatistics;
	
	public VMOptimizerPolicy(List<? extends Host> hostList, PowerVmSelectionPolicy vmSelectionPolicy) {
		super(hostList, vmSelectionPolicy);
	}
	
	public VMOptimizerPolicy(List<? extends Host> hostList) {
		super(hostList);
	}
	
	public void resolveCrossDependencies() {
		hostOverSaturationPolicy.setVmOptimizerPolicy(this);
		hostUnderUtilisationPolicy.setVmOptimizerPolicy(this);
		vmSelPolicy.setVmOptimizerPolicy(this);
		vmMigrationPolicy.setVmOptimizerPolicy(this);
		migrationStatistics.setVmOptimizerPolicy(this);
	}
	
	public HostOverSaturationPolicy getHostOverSaturationPolicy() {
		return hostOverSaturationPolicy;
	}

	public void setHostOverSaturationPolicy(HostOverSaturationPolicy hostOverSaturationPolicy) {
		this.hostOverSaturationPolicy = hostOverSaturationPolicy;
	}

	public HostUnderUtilisationPolicy getHostUnderUtilisationPolicy() {
		return hostUnderUtilisationPolicy;
	}

	public void setHostUnderUtilisationPolicy(HostUnderUtilisationPolicy hostUnderUtilisationPolicy) {
		this.hostUnderUtilisationPolicy = hostUnderUtilisationPolicy;
	}

	public VMSelectionPolicy getVmSelPolicy() {
		return vmSelPolicy;
	}

	public void setVmSelPolicy(VMSelectionPolicy vmSelPolicy) {
		this.vmSelPolicy = vmSelPolicy;
	}

	public VMMigrationPolicy getVmMigrationPolicy() {
		return vmMigrationPolicy;
	}

	public void setVmMigrationPolicy(VMMigrationPolicy vmMigrationPolicy) {
		this.vmMigrationPolicy = vmMigrationPolicy;
	}

	public MigrationStatistics getMigrationStatistics() {
		return migrationStatistics;
	}

	public void setMigrationStatistics(MigrationStatistics migrationStatistics) {
		this.migrationStatistics = migrationStatistics;
	}

	@Override
	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
		return super.optimizeAllocation(vmList);
	}
	
	@Override
	public void showMigrationMap(List<Map<String, Object>> migrationMap) {
		super.showMigrationMap(migrationMap);
	}
	
	public List<Map<String, Object>> getMigrationMapFromUnderUtilizedHostsOriginal(
			List<PowerHostUtilizationHistory> overUtilizedHosts){
		return super.getMigrationMapFromUnderUtilizedHosts(overUtilizedHosts);
	}
	
	@Override
	public List<Map<String, Object>> getMigrationMapFromUnderUtilizedHosts(
			List<PowerHostUtilizationHistory> overUtilizedHosts){
		return hostUnderUtilisationPolicy.getMigrationMapFromUnderUtilizedHosts(overUtilizedHosts);
	}
	
	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
		return super.findHostForVm(vm, excludedHosts);
	}
	
	public boolean isHostOverUtilizedAfterAllocationOriginal(PowerHost host, Vm vm) {
		return super.isHostOverUtilizedAfterAllocation(host, vm);
	}
	
	@Override
	protected boolean isHostOverUtilizedAfterAllocation(PowerHost host, Vm vm) {
		return hostOverSaturationPolicy.isHostOverUtilizedAfterAllocation(host, vm);
	}
	
	public PowerHost findHostForVmOriginal(Vm vm) {
		return super.findHostForVm(vm);
	}
	
	@Override
	public PowerHost findHostForVm(Vm vm) {
		return vmMigrationPolicy.findHostForVm(vm);
	}
	
	@Override
	protected List<PowerHost> extractHostListFromMigrationMap(List<Map<String, Object>> migrationMap) {
		return super.extractHostListFromMigrationMap(migrationMap);
	}
	
	public List<Map<String, Object>> getNewVmPlacementOriginal(
			List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return super.getNewVmPlacement(vmsToMigrate, excludedHosts);
	}
	
	@Override
	public List<Map<String, Object>> getNewVmPlacement(
			List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return vmMigrationPolicy.getNewVmPlacement(vmsToMigrate, excludedHosts);
	}

	@Override
	public List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(
			List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return super.getNewVmPlacementFromUnderUtilizedHost(vmsToMigrate, excludedHosts);
	}
	
	public List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHostOriginal(
			List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return super.getNewVmPlacementFromUnderUtilizedHost(vmsToMigrate, excludedHosts);
	}
	
	@Override
	public List<? extends Vm>
	  getVmsToMigrateFromHosts(List<PowerHostUtilizationHistory> overUtilizedHosts) {
		return super.getVmsToMigrateFromHosts(overUtilizedHosts);
	}
	
	public List<? extends Vm> getVmsToMigrateFromUnderUtilizedHostOriginal(PowerHost host) {
		return super.getVmsToMigrateFromUnderUtilizedHost(host);
	}
	
	@Override
	public List<? extends Vm> getVmsToMigrateFromUnderUtilizedHost(PowerHost host) {
		return hostUnderUtilisationPolicy.getVmsToMigrateFromUnderUtilizedHost(host);
	}

	public List<PowerHostUtilizationHistory> getOverUtilizedHostsOriginal() {
		return super.getOverUtilizedHosts();
	}

	@Override
	public List<PowerHostUtilizationHistory> getOverUtilizedHosts() {
		return hostOverSaturationPolicy.getOverUtilizedHosts();
	}
	
	@Override
	public List<PowerHost> getSwitchedOffHosts() {
		return super.getSwitchedOffHosts();
	}
	
	public PowerHost getUnderUtilizedHostOriginal(Set<? extends Host> excludedHosts) {
		return super.getUnderUtilizedHost(excludedHosts);
	}
	
	@Override
	public PowerHost getUnderUtilizedHost(Set<? extends Host> excludedHosts) {
		return hostUnderUtilisationPolicy.getUnderUtilizedHost(excludedHosts);
	}

	@Override
	protected boolean areAllVmsMigratingOutOrAnyVmMigratingIn(PowerHost host) {
		return super.areAllVmsMigratingOutOrAnyVmMigratingIn(host);
	}

	public boolean isHostOverUtilized(PowerHost host) {
		return hostOverSaturationPolicy.isHostOverUtilized(host);
	}
	
	protected void addHistoryEntryOriginal(HostDynamicWorkload host, double metric) {
		super.addHistoryEntry(host, metric);
	}
	
	@Override
	protected void addHistoryEntry(HostDynamicWorkload host, double metric) {
		migrationStatistics.addHistoryEntry(host, metric);
	}
	
	@Override
	public void saveAllocation() {
		super.saveAllocation();
	}

	@Override
	public void restoreAllocation() {
		super.restoreAllocation();
	}
	
	@Override
	protected double getPowerAfterAllocation(PowerHost host, Vm vm) {
		return super.getPowerAfterAllocation(host, vm);
	}
	
	public double getPowerAfterAllocationOriginal(PowerHost host, Vm vm) {
		return super.getPowerAfterAllocation(host, vm);
	}

	@Override
	protected double getMaxUtilizationAfterAllocation(PowerHost host, Vm vm) {
		return super.getMaxUtilizationAfterAllocation(host, vm);
	}
	
	public double getMaxUtilizationAfterAllocationOriginal(PowerHost host, Vm vm) {
		return super.getMaxUtilizationAfterAllocation(host, vm);
	}

	@Override
	protected double getUtilizationOfCpuMips(PowerHost host) {
		return super.getUtilizationOfCpuMips(host);
	}
	
	public double getUtilizationOfCpuMipsOriginal(PowerHost host) {
		return super.getUtilizationOfCpuMips(host);
	}
	
	@Override
	protected List<Map<String, Object>> getSavedAllocation() {
		return super.getSavedAllocation();
	}
	
	@Override
	public Map<Integer, List<Double>> getUtilizationHistory() {
		return super.getUtilizationHistory();
	}
	
	
	public Map<Integer, List<Double>> getMetricHistoryOriginal() {
		return super.getMetricHistory();
	}
	
	@Override
	public Map<Integer, List<Double>> getMetricHistory() {
		return migrationStatistics.getMetricHistory();
	}
	
	public Map<Integer, List<Double>> getTimeHistoryOriginal() {
		return super.getTimeHistory();
	}
	
	@Override
	public Map<Integer, List<Double>> getTimeHistory() {
		return migrationStatistics.getTimeHistory();
	}
	
	public List<Double> getExecutionTimeHistoryVmSelectionOriginal() {
		return super.getExecutionTimeHistoryVmSelection();
	}
	
	@Override	
	public List<Double> getExecutionTimeHistoryVmSelection() {
		return migrationStatistics.getExecutionTimeHistoryVmSelection();
	}
	
	public List<Double> getExecutionTimeHistoryHostSelectionOriginal() {
		return super.getExecutionTimeHistoryHostSelection();
	}
	
	@Override
	public List<Double> getExecutionTimeHistoryHostSelection() {
		return migrationStatistics.getExecutionTimeHistoryHostSelection();
	}
		
	public List<Double> getExecutionTimeHistoryVmReallocationOriginal() {
		return super.getExecutionTimeHistoryVmReallocation();
	}

	@Override			
	public List<Double> getExecutionTimeHistoryVmReallocation() {
		return migrationStatistics.getExecutionTimeHistoryVmReallocation();
	}
					
	public List<Double> getExecutionTimeHistoryTotalOriginal() {
		return super.getExecutionTimeHistoryTotal();
	}

	@Override				
	public List<Double> getExecutionTimeHistoryTotal() {
		return migrationStatistics.getExecutionTimeHistoryTotal();
	}
	
	public boolean allocateHostForVmOriginal(Vm vm) {
		return super.allocateHostForVm(vm);
	}
	
	@Override
	public boolean allocateHostForVm(Vm vm) {
		return vmMigrationPolicy.allocateHostForVm(vm);
	}

	public boolean allocateHostForVmOriginal(Vm vm, Host host) {
		return super.allocateHostForVm(vm, host);
	}
	
	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		return vmMigrationPolicy.allocateHostForVm(vm, host);
	}
	
	@Override
	public void setVmSelectionPolicy(PowerVmSelectionPolicy vmSelectionPolicy) {
		super.setVmSelectionPolicy(vmSelectionPolicy);
	}
	
	@Override
	public PowerVmSelectionPolicy getVmSelectionPolicy() {
		return super.getVmSelectionPolicy();
	}
	
	@Override
	public void deallocateHostForVm(Vm vm) {
		super.deallocateHostForVm(vm);
	}
	
	@Override
	public Host getHost(Vm vm) {
		return super.getHost(vm);
	}
	
	@Override
	public Host getHost(int vmId, int userId) {
		return super.getHost(vmId, userId);
	}
	
	@Override
	public Map<String, Host> getVmTable() {
		return super.getVmTable();
	}
	
	/**
	 * Gets a VM to migrate from a given host.
	 * 
	 * @param host the host
	 * @return the vm to migrate
	 */	
	public Vm getVmToMigrate(PowerHost host) {
		return vmSelPolicy.getVmToMigrate(host);
	}

	/**
	 * Gets the list of migratable VMs from a given host.
	 * 
	 * @param host the host
	 * @return the list of migratable VMs
	 */
	protected List<PowerVm> getMigratableVms(PowerHost host) {
		List<PowerVm> migratableVms = new ArrayList<PowerVm>();
		for (PowerVm vm : host.<PowerVm> getVmList()) {
			if (!vm.isInMigration()) {
				migratableVms.add(vm);
			}
		}
		return migratableVms;
	}
	
	public void setAllocationPolicy(PowerVmAllocationPolicyMigrationAbstract allocationPolicy) {
		vmSelectionPolicy.setAllocationPolicy(allocationPolicy);
	}

	public PowerVmAllocationPolicyMigrationAbstract getAllocationPolicy() {
		return vmSelectionPolicy.getAllocationPolicy();
	}
	
	public void setPowerDatacenter(PowerDatacenter powerDatacenter) {
		vmSelectionPolicy.setPowerDatacenter(powerDatacenter);
	}
	
	public PowerDatacenter getPowerDatacenter() {
		return vmSelectionPolicy.getPowerDatacenter();
	}
	
	public void updateOriginal() {
		super.update();
	}

}
