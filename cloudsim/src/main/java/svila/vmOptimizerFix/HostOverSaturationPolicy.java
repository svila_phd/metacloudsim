package svila.vmOptimizerFix;

import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

import svila.vmOptimizerFix.VMOptimizerPolicy;

public abstract class HostOverSaturationPolicy {
	
	VMOptimizerPolicy vmOptimizerPolicy;

	public HostOverSaturationPolicy() {

	}

	public HostOverSaturationPolicy(VMOptimizerPolicy vmOptimizerPolicy) {
		this.vmOptimizerPolicy = vmOptimizerPolicy;
	}
	
	public VMOptimizerPolicy getVmOptimizerPolicy() {
		return vmOptimizerPolicy;
	}

	public void setVmOptimizerPolicy(VMOptimizerPolicy vmOptimizerPolicy) {
		this.vmOptimizerPolicy = vmOptimizerPolicy;
	}

	public abstract boolean isHostOverUtilized(PowerHost host);
	public abstract List<PowerHostUtilizationHistory> getOverUtilizedHosts();
	public abstract boolean isHostOverUtilizedAfterAllocation(PowerHost host, Vm vm);

}
