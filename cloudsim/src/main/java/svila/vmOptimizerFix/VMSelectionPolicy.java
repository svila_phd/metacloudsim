package svila.vmOptimizerFix;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

public abstract class VMSelectionPolicy extends PowerVmSelectionPolicy {
	
	VMOptimizerPolicy vmOptimizerPolicy;
	
	public VMOptimizerPolicy getVmOptimizerPolicy() {
		return vmOptimizerPolicy;
	}

	public void setVmOptimizerPolicy(VMOptimizerPolicy vmOptimizerPolicy) {
		this.vmOptimizerPolicy = vmOptimizerPolicy;
	}

	public abstract Vm getVmToMigrate(PowerHost host);
}
