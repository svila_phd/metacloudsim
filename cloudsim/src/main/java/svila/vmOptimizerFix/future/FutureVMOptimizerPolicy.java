package svila.vmOptimizerFix.future;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;

import svila.metricsLogic.DatacenterAnalyzer;
import svila.metricsLogic.HostAnalyzer;
import svila.metricsLogic.VmAnalyzer;
import svila.metricsLogic.CPUCalculator.ForecastingTechnique;
import svila.metricsLogic.CPUCalculator.ForecastingTechniqueFactory;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethod;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethodFactory;
import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.GlobalRunner;
import svila.planetlabNetwork.StaticResources;
import svila.policiesHostOverSaturation.future.FutureHostOverSaturationBase;
import svila.policiesHostUnderUtilisation.future.FutureHostUnderUtilisationBase;
import svila.policiesVmMigration.future.FutureVmMigrationPolicyBase;
import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureTechnique;
import svila.policiesVmOptimizer.future.FutureVm;
import svila.policiesVmSelection.future.FutureVmSelectionBase;
import svila.vmOptimizerFix.MigrationStatistics;
import svila.vmOptimizerFix.VMOptimizerPolicy;

public class FutureVMOptimizerPolicy extends VMOptimizerPolicy {
	public FutureHostOverSaturationBase futureHostOverSaturationPolicy; 
	public FutureHostUnderUtilisationBase futureHostUnderUtilisationPolicy;
	public FutureVmSelectionBase futureVmSelectionPolicy;
	public FutureVmMigrationPolicyBase futureVmMigrationPolicy;
	
	public VmAnalyzer vmAnalyzer;
	public HostAnalyzer hostAnalyzer;
	public DatacenterAnalyzer datacenterAnalyzer;
	
	public HashMap<Integer, FutureHost> futureHosts;
	public HashMap<Integer, FutureVm> futureVms;
	public HashMap<String, Set<FutureHost>> futureHostClassified;
	
	public FutureVMOptimizerPolicy(List<? extends Host> hostList) {
		super(hostList);
		
		this.vmAnalyzer = new VmAnalyzer();
		this.hostAnalyzer = new HostAnalyzer();
		this.datacenterAnalyzer = new DatacenterAnalyzer();
	}
	
	@Override
	public void update() {
		this.vmAnalyzer.index++;
		
		this.vmAnalyzer.tryAddNewVms(getPowerDatacenter().getVmList());
				
		vmAnalyzer.updateStats();
		//vmAnalyzer.showInfo();
		
		
		this.hostAnalyzer.index++;
		
		if(this.hostAnalyzer.isFirstTime()) {
			this.hostAnalyzer.initHosts(getPowerDatacenter().getHostList());
			this.datacenterAnalyzer.init(getPowerDatacenter(), this.hostAnalyzer);
		}
		
		hostAnalyzer.updateStats();
		//hostAnalyzer.showInfo();
		
		datacenterAnalyzer.index++;
		datacenterAnalyzer.updateStats();
	}
	
	public void resolveCrossDependencies() {
		futureHostOverSaturationPolicy.setVMOptimizerPolicy(this);
		futureHostUnderUtilisationPolicy.setVmOptimizerPolicy(this);
		futureVmSelectionPolicy.setVmOptimizerPolicy(this);
		futureVmMigrationPolicy.setVmOptimizerPolicy(this);
		migrationStatistics.setVmOptimizerPolicy(this);
	}

	public FutureHostOverSaturationBase getFutureHostOverSaturationPolicy() {
		return futureHostOverSaturationPolicy;
	}

	public void setFutureHostOverSaturationPolicy(FutureHostOverSaturationBase futureHostOverSaturationPolicy) {
		this.futureHostOverSaturationPolicy = futureHostOverSaturationPolicy;
	}

	public FutureHostUnderUtilisationBase getFutureHostUnderUtilisationPolicy() {
		return futureHostUnderUtilisationPolicy;
	}

	public void setFutureHostUnderUtilisationPolicy(FutureHostUnderUtilisationBase futureHostUnderUtilisationPolicy) {
		this.futureHostUnderUtilisationPolicy = futureHostUnderUtilisationPolicy;
	}

	public FutureVmSelectionBase getFutureVmSelectionPolicy() {
		return futureVmSelectionPolicy;
	}

	public void setFutureVmSelectionPolicy(FutureVmSelectionBase futureVmSelectionPolicy) {
		this.futureVmSelectionPolicy = futureVmSelectionPolicy;
	}

	public FutureVmMigrationPolicyBase getFutureVmMigrationPolicy() {
		return futureVmMigrationPolicy;
	}

	public void setFutureVmMigrationPolicy(FutureVmMigrationPolicyBase futureVmMigrationPolicy) {
		this.futureVmMigrationPolicy = futureVmMigrationPolicy;
	}

	public MigrationStatistics getMigrationStatistics() {
		return migrationStatistics;
	}

	public void setMigrationStatistics(MigrationStatistics migrationStatistics) {
		this.migrationStatistics = migrationStatistics;
	}
	
	@Override
	public void setAllocationPolicy(PowerVmAllocationPolicyMigrationAbstract allocationPolicy) {
		// Not necessary
		//vmSelectionPolicy.setAllocationPolicy(allocationPolicy);
	}
	
	@Override
	public PowerVmAllocationPolicyMigrationAbstract getAllocationPolicy() {
		//return vmSelectionPolicy.getAllocationPolicy();
		return this;
	}
	
	@Override
	public void setPowerDatacenter(PowerDatacenter powerDatacenter) {
		//vmSelectionPolicy.setPowerDatacenter(powerDatacenter);
		this.futureVmSelectionPolicy.setPowerDatacenter(powerDatacenter);
	}
	
	@Override
	public PowerDatacenter getPowerDatacenter() {
		//return vmSelectionPolicy.getPowerDatacenter();
		return this.futureVmSelectionPolicy.getPowerDatacenter();
	}
	
	public void setHostList(List<? extends Host> hostList) {
		super.setHostList(hostList);
		if(hostList != null) {
			this.futureHosts = this.getFutureHosts();
			this.futureVms = this.getInitialFutureVms();
		}
	}
	
	@Override
	public boolean allocateHostForVm(Vm vm) {
		FutureVm futureVm = this.futureVms.get(vm.getNetworkId());
		return this.futureVmMigrationPolicy.allocateHostForVm(futureVm);
	}
	
	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		return this.futureVmMigrationPolicy.allocateHostForVm(
				this.futureVms.get(vm.getNetworkId()), this.futureHosts.get(host.getNetworkId()));
	}
	
	public HashMap<Integer, FutureHost> getFutureHosts() {
		ExperimentConfiguration ec = StaticResources.getCE();

		SignalProcessingMethod signalProcessing = SignalProcessingMethodFactory.getSignalProcessingMethod(ec.getHostSignalProcessing());
		signalProcessing.setParams(ec.getHostSignalProcessingParams());
		ForecastingTechnique forecastingTechnique = ForecastingTechniqueFactory.getForecastingTechnique(ec.getHostForecastingTechnique());
		forecastingTechnique.setParams(ec.getHostForecastingTechniqueParams());
		FutureTechnique futureTechnique = new FutureTechnique(signalProcessing, forecastingTechnique, ec.getHostForecastingResume());
		
		List<Host> hostList = getHostList();
		HashMap<Integer, FutureHost> futureHostMap = new HashMap<>();		
		for(Host host : hostList) {
			PowerHostUtilizationHistory powerHost = (PowerHostUtilizationHistory) host;
			FutureHost futureHost = new FutureHost(powerHost, futureTechnique);
			futureHost.addCurrentVms();
			futureHostMap.put(powerHost.getNetworkId(), futureHost);
		}
		
		return futureHostMap;
	}
	
	public HashMap<Integer, FutureVm> getInitialFutureVms() {
		HashMap<Integer, FutureVm> futureVms = new HashMap<>();
		List<Vm> vmList = GlobalRunner.vmList;

		FutureTechnique futureForeTechnique = new FutureTechnique(
				SignalProcessingMethodFactory.getSignalProcessingMethod(SignalProcessingMethodFactory.none),
				ForecastingTechniqueFactory.getForecastingTechnique(ForecastingTechniqueFactory.last));
		FutureTechnique futureSmoothTechnique = new FutureTechnique(
				SignalProcessingMethodFactory.getSignalProcessingMethod(SignalProcessingMethodFactory.none),
				ForecastingTechniqueFactory.getForecastingTechnique(ForecastingTechniqueFactory.last));
		
		for(Vm vm : vmList) {
			PowerVm powerVm = (PowerVm)vm;
			FutureVm futureVm = new FutureVm(powerVm, futureForeTechnique, futureSmoothTechnique);
			futureVms.put(futureVm.getCurrentVm().getNetworkId(), futureVm);
		}
		
		return futureVms;
	}
	
	public HashMap<Integer, FutureVm> getFutureVms(HashMap<Integer, FutureHost> futureHosts) {
		HashMap<Integer, FutureVm> futureVms = new HashMap<>();

		for(FutureHost futureHost : futureHosts.values()) {
			futureVms.putAll(futureHost.getFutureVms());
		}
		
		return futureVms;
	}
}
