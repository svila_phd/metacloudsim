package svila.vmOptimizerFix;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;

public abstract class HostUnderUtilisationPolicy {
	
	protected VMOptimizerPolicy vmOptimizerPolicy;
	
	public VMOptimizerPolicy getVmOptimizerPolicy() {
		return vmOptimizerPolicy;
	}

	public void setVmOptimizerPolicy(VMOptimizerPolicy vmOptimizerPolicy) {
		this.vmOptimizerPolicy = vmOptimizerPolicy;
	}

	public abstract List<Map<String, Object>> getMigrationMapFromUnderUtilizedHosts(
			List<PowerHostUtilizationHistory> overUtilizedHosts);
	protected abstract PowerHost getUnderUtilizedHost(Set<? extends Host> excludedHosts);
	public abstract List<? extends Vm> getVmsToMigrateFromUnderUtilizedHost(PowerHost host);
	
}
