package svila.vmOptimizerFix;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public abstract class VMMigrationPolicy {
	
	VMOptimizerPolicy vmOptimizerPolicy;
	
	public VMOptimizerPolicy getVmOptimizerPolicy() {
		return vmOptimizerPolicy;
	}

	public void setVmOptimizerPolicy(VMOptimizerPolicy vmOptimizerPolicy) {
		this.vmOptimizerPolicy = vmOptimizerPolicy;
	}

	public abstract PowerHost findHostForVm(Vm vm);
	
	public abstract PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts);

	public abstract List<Map<String, Object>> getNewVmPlacement(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts);
	
	public abstract List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts);

	public abstract boolean allocateHostForVm(Vm vm);

	public abstract boolean allocateHostForVm(Vm vm, Host host);
}
