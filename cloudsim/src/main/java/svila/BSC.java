package svila;

import org.cloudbus.cloudsim.power.models.PowerModel;
import org.cloudbus.cloudsim.power.models.PowerModelSpecPowerHpProLiantMl110G4Xeon3040;
import org.cloudbus.cloudsim.power.models.PowerModelSpecPowerHpProLiantMl110G5Xeon3075;

/**
 *
 * @author Sergi
 */
public class BSC {    
    public static int NUMBER_OF_VMS = 2;
    public static int NUMBER_OF_HOSTS = 2;
    public static long CLOUDLET_UTILIZATION_SEED = -1;
    
    public static boolean ENABLE_OUTPUT = true;
    public static boolean OUTPUT_CSV    = false;

    //public final static double SCHEDULING_INTERVAL = 300;
    //public final static double SIMULATION_LIMIT = 24 * 60 * 60;
    public static double SCHEDULING_INTERVAL = 100;
    public static double SIMULATION_LIMIT = 1000;

    public static int CLOUDLET_LENGTH	= 2500 * (int) SIMULATION_LIMIT;
    public static int CLOUDLET_PES	= 1;
    
    public static int VM_TYPES	= 4;
    public static int[] VM_MIPS	= { 2500, 2000, 1000, 500 };
    public static int[] VM_PES	= { 1, 1, 1, 1 };
    public static int[] VM_RAM	= { 870,  1740, 1740, 613 };
    public static int VM_BW		= 100000;
    public static int VM_SIZE		= 2500;
        
    public static int HOST_TYPES	 = 2;
    public static int[] HOST_MIPS	 = { 500, 2660 };
    public static int[] HOST_PES	 = { 1, 1 };
    public static int[] HOST_RAM	 = { 4096, 4096 };
    public static int HOST_BW		 = 1000000; 
    public static int HOST_STORAGE = 1000000;

    public static PowerModel[] HOST_POWER = {
	new PowerModelSpecPowerHpProLiantMl110G4Xeon3040(),
	new PowerModelSpecPowerHpProLiantMl110G5Xeon3075()
    };
    
    public static double LOWER_THRESHOLD = 0.3;
    public static double UPPER_THRESHOLD = 0.7;
    
    public static String CLOUDLET1PATH = "C:\\Users\\Sergi\\Documents\\cloudsim4.0\\modules\\cloudsim\\target\\classes\\workload\\basic\\cloudlet1.txt";
    public static String CLOUDLET2PATH = "C:\\Users\\Sergi\\Documents\\cloudsim4.0\\modules\\cloudsim\\target\\classes\\workload\\basic\\cloudlet2.txt";
}
