package svila.WorkloadGenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONObject;

import svila.staticAllocation.SWFJob;
import svila.staticAllocation.SWFWorkload;
import svila.staticAllocation.StaticExperimentation;

public class Main {
	
	static String outputFolder = "CEA-Curie";
	static String workloadType = "CEA-Curie-2011-2.1-cln.swf";
	static int maxNumberOfTasks = 200;
	static int numberOfOutputs = 30;
	
	static int maxNumberOfCores = 16;
	//static int minRequestedTime = 0;
	//static int maxRequestedTime = Integer.MAX_VALUE;
	static int minRequestedTime = 60;
	static int maxRequestedTime = 60 * 60;
	static int minSubmitTime = 0;
	static int maxSubmitTime = Integer.MAX_VALUE;
	
	
	public static void main(String[] args) {
		Path baseWorkloadPath = Paths.get(getBasePath());
		Path inputWorkloadTypePath = Paths.get(baseWorkloadPath.toString(), "originalWorkloads", workloadType);
		Path inputWorkloadsPath = Paths.get(baseWorkloadPath.toString(), "input", outputFolder);

		WorkloadGenerator wg = new WorkloadGenerator(
				inputWorkloadTypePath.toString(),
				inputWorkloadsPath.toString(),
				maxNumberOfTasks,
				numberOfOutputs,
				maxNumberOfCores,
				minRequestedTime,
				maxRequestedTime,
				minSubmitTime,
				maxSubmitTime);
		
		wg.createFolderWithSplitWorkloads();		
	}
	
	public static String getBasePath() {
		File file = new File(StaticExperimentation.class.getResource("/data.json").getFile());
		JSONObject jsonFile = null;
		String basePath = "";
		try {
			jsonFile = new JSONObject(new String(Files.readAllBytes(file.toPath()),"UTF-8"));
			basePath = jsonFile.getString("basepath");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return basePath;
	}
}
