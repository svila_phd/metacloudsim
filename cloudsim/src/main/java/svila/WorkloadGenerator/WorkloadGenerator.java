package svila.WorkloadGenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import svila.staticAllocation.SWFJob;

public class WorkloadGenerator {
	
	String inputWorkloadTypePath;
	String outputFolder;
	String workloadType;
	int maxNumberOfTasks;
	int numberOfOutputs;
	
	int maxNumberOfCores;
	int minRequestedTime;
	int maxRequestedTime;
	int minSubmitTime;
	int maxSubmitTime;
	
	int JOBNUMBER = 0, SUBMIT_TIME = 1, WAIT_TIME = 2, RUN_TIME = 3,	NUM_ALLOCATED_PROCESSORS = 4,
		    AVERAGE_CPU_TIME_USED = 5, USED_MEMORY = 6, NUM_REQUESTED_PROCESSORS = 7,
		    REQUESTED_TIME = 8, REQUESTED_MEMORY = 9;
	
	public WorkloadGenerator(String inputWorkloadTypePath, String outputFolder, int maxNumberOfTasks,
			int numberOfOutputs, int maxNumberOfCores, int minRequestedTime, int maxRequestedTime,
			int minSubmitTime, int maxSubmitTime) {
		this.inputWorkloadTypePath = inputWorkloadTypePath;
		this.outputFolder = outputFolder;
		this.maxNumberOfTasks = maxNumberOfTasks;
		this.numberOfOutputs = numberOfOutputs;
		this.maxNumberOfCores = maxNumberOfCores;
		this.minRequestedTime = minRequestedTime;
		this.maxRequestedTime = maxRequestedTime;
		this.minSubmitTime = minSubmitTime;
		this.maxSubmitTime = maxSubmitTime;
		
	}
	
	public List<String> obtainValidLines() {
		BufferedReader br = null;
		FileReader fr = null;
		
		List<String> validLines = new LinkedList<>();
		
		try{
			fr = new FileReader(inputWorkloadTypePath);
			br = new BufferedReader(fr);
			String currentLine;
			String[] splitLine;
			
			while((currentLine = br.readLine()) != null) {
				if(currentLine.charAt(0) == ';') {
					continue;
				}
				splitLine = currentLine.trim().split("\\s+");
				if(isValidLine(splitLine)) {
					validLines.add(currentLine);
				}
			}
			System.out.println(validLines.size() + " valid lines");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}
		}
		return validLines;
	}
	
	public boolean isValidLine(String[] splitLine) {
		int submitTime = Integer.valueOf(splitLine[SUBMIT_TIME]);
		int numberOfCores = Integer.valueOf(splitLine[NUM_REQUESTED_PROCESSORS]);
		int requestedTime = Integer.valueOf(splitLine[REQUESTED_TIME]);
		return numberOfCores >= 1 && numberOfCores <= maxNumberOfCores &&
			   submitTime >= minSubmitTime && submitTime <= maxSubmitTime &&
			   requestedTime >= minRequestedTime && requestedTime <= maxRequestedTime;
	}
	
	public void createFolderWithSplitWorkloads() {
		List<String> validLines = obtainValidLines();
		int currentLine = 0;
		SimpleDateFormat sdfDate = new SimpleDateFormat("_dd_MM_yyyy_HH_mm_ss");
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
		String folderName = outputFolder + strDate;
		System.out.println("Creating workload folder" + folderName);
		new File(folderName).mkdirs();
		for(int i=0; i<numberOfOutputs; i++) {
			List<String> currentLines = new ArrayList<>();
			for(int j=0; j<maxNumberOfTasks; j++) {
				currentLines.add(validLines.get(currentLine));
				currentLine++;
			}
			
			createFileWithLines(Paths.get(folderName, i + ".swf").toString(), currentLines);
		}
		System.out.println("Workload folder created correctly!");
	}
	
	public void createFileWithLines(String filepath, List<String> lines) {
		try {
			System.out.println("Creating workload " + filepath);
			File fout = new File(filepath);
			FileOutputStream fos = new FileOutputStream(fout);
		 
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
		 
			for(String line : lines) {
				bw.write(line);
				bw.newLine();
			}
		 
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
