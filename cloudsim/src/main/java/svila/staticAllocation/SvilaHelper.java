package svila.staticAllocation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerDynamicWorkload;
import org.cloudbus.cloudsim.CloudletSchedulerSpaceShared;
import org.cloudbus.cloudsim.CloudletSchedulerTimeShared;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelNull;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicySimple;
import org.cloudbus.cloudsim.VmSchedulerSpaceShared;
import org.cloudbus.cloudsim.VmSchedulerTimeShared;
import org.cloudbus.cloudsim.VmSchedulerTimeSharedOverSubscription;
import org.cloudbus.cloudsim.examples.power.Constants;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.power.models.PowerModel;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;
import org.yaml.snakeyaml.Yaml;

import svila.AsyncDatacenterBroker;
import svila.GridCloudBridge.FromCloudSimToGridSim;

public class SvilaHelper {
	public static Datacenter createDatacenterFromVMs(String name, String vmInputFile) {
		List<Host> hostList = createHostListFromVMs(vmInputFile);

        String arch = "x86";      // system architecture
        String os = "Linux";          // operating system
        String vmm = "Xen";
        double time_zone = 10.0;         // time zone this resource located
        double cost = 3.0;              // the cost of using processing in this resource
        double costPerMem = 0.05;		// the cost of using memory in this resource
        double costPerStorage = 0.001;	// the cost of using storage in this resource
        double costPerBw = 0.0;			// the cost of using bw in this resource
        LinkedList<Storage> storageList = new LinkedList<Storage>();	//we are not adding SAN devices by now

        DatacenterCharacteristics characteristics = new DatacenterCharacteristics(
                arch, os, vmm, hostList, time_zone, cost, costPerMem, costPerStorage, costPerBw);


        // 6. Finally, we need to create a PowerDatacenter object.
        Datacenter datacenter = null;
        try {
            datacenter = new Datacenter(name, characteristics, new SvilaVmAllocationPolicy(hostList), storageList, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return datacenter;
	}
	
	public static Datacenter createDatacenter(String name, String hostInputFile){
		List<Host> hostList = createHostList(hostInputFile);

        String arch = "x86";      // system architecture
        String os = "Linux";          // operating system
        String vmm = "Xen";
        double time_zone = 10.0;         // time zone this resource located
        double cost = 3.0;              // the cost of using processing in this resource
        double costPerMem = 0.05;		// the cost of using memory in this resource
        double costPerStorage = 0.001;	// the cost of using storage in this resource
        double costPerBw = 0.0;			// the cost of using bw in this resource
        LinkedList<Storage> storageList = new LinkedList<Storage>();	//we are not adding SAN devices by now

        DatacenterCharacteristics characteristics = new DatacenterCharacteristics(
                arch, os, vmm, hostList, time_zone, cost, costPerMem, costPerStorage, costPerBw);


        // 6. Finally, we need to create a PowerDatacenter object.
        Datacenter datacenter = null;
        try {
            datacenter = new Datacenter(name, characteristics, new SvilaVmAllocationPolicy(hostList), storageList, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return datacenter;
	}
	
	public static ArrayList<Vm> createVmList(int brokerId, String inputFileName) {
		ArrayList<Vm> vmlist = new ArrayList<Vm>();
		InputStream input;
	
		try {
			input = new FileInputStream(new File(inputFileName));
		
		    Yaml yaml = new Yaml();
		    List<Map<String,Object>> vms = (List<Map<String,Object>>)yaml.load(input);
		    int VMIndex = 0;
		
			for(Map<String,Object> vmwrap: vms)
			{	
				Map<String,Object> vm = (Map<String,Object>)vmwrap.get("VM");
				
				int NUMBER = (int)vm.get("Number");
			    int VM_MIPS = ((ArrayList<Integer>)vm.get("CpuCapacity")).get(0);
				int VM_PES = (int)vm.get("Cores");
				int VM_RAM = (int)vm.get("RAM");
				//int VM_BW = (int)vm.get("BandWidth");
				int VM_BW = 0;
				int VM_SIZE = (int)vm.get("DiskCapacity");
				
				for(int i=0; i<NUMBER; i++) {
					Vm newVm = new Vm(
							VMIndex,
							brokerId,
							VM_MIPS,
							VM_PES,
							VM_RAM,
							VM_BW,
							VM_SIZE,
							"Xen",
							new CloudletSchedulerTimeShared());
					newVm.energyFullPerCore = (double)vm.get("EnergyFullPerCore");
					newVm.energyIdlePerCore = (double)vm.get("EnergyIdlePerCore");
					vmlist.add(newVm);
							
					VMIndex++;
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vmlist;
	}

	public static List<Host> createHostListFromVMs(String vmInputFile) {
		List<Vm> vms = createVmList(0, vmInputFile);
		List<Host> hosts = new ArrayList<>();
		for(Vm vm : vms) {
			hosts.add(createHostFromVM(vm));
		}
		return hosts;
	}
	
	public static Host createHostFromVM(Vm vm) {

		List<Pe> peList = new ArrayList<Pe>();
		for (int j = 0; j < vm.getNumberOfPes(); j++) {
			peList.add(new Pe(j, new PeProvisionerSimple(vm.getMips())));
		}

		Host newHost = new Host(
					vm.getId(),
    				new RamProvisionerSimple(10000),
    				new BwProvisionerSimple(10000),
    				1000000,
    				peList,
    				new VmSchedulerTimeShared(peList)
    	);
		
		newHost.setEnergyIdle(vm.energyIdlePerCore * vm.getNumberOfPes());
		newHost.setEnergyFull(vm.energyFullPerCore * vm.getNumberOfPes());
		return newHost;
	}
	
	public static List<Host> createHostList(String inputFileName) {
	
		List<Host> hostList = new ArrayList<Host>();
	    InputStream input;
		try {
			input = new FileInputStream(new File(inputFileName));
		    Yaml yaml = new Yaml();
		    List<Map<String,Object>> hosts = (List<Map<String,Object>>)yaml.load(input);
	
			int hostID = 0;
			
			for(Map<String,Object> hostwrap: hosts)
			{	
				Map<String,Object> host = (Map<String,Object>)hostwrap.get("Host");
				
				int NUMBER = (int)host.get("Number");
			    int HOST_MIPS = ((ArrayList<Integer>)host.get("CpuCapacity")).get(0);
				int HOST_PES = (int)host.get("Cores");
				int HOST_RAM = (int)host.get("RAM");
				int HOST_BW = (int)host.get("BandWidth");
				int HOST_STORAGE = (int)host.get("DiskCapacity");
				double HOST_ENERGY_IDLE = (double)host.get("EnergyIdle");
				double HOST_ENERGY_FULL = (double)host.get("EnergyFull");
				
				//FromCloudSimToGridSim.baseCores = HOST_PES;
				//FromCloudSimToGridSim.energyIdle = HOST_ENERGY_IDLE;
				//FromCloudSimToGridSim.energyFull = HOST_ENERGY_FULL;
				
				for(int i=0; i<NUMBER; i++) {
					List<Pe> peList = new ArrayList<Pe>();
					for (int j = 0; j < HOST_PES; j++) {
						peList.add(new Pe(j, new PeProvisionerSimple(HOST_MIPS)));
					}
		
					Host newHost = new Host(
							hostID,
		    				new RamProvisionerSimple(HOST_RAM),
		    				new BwProvisionerSimple(HOST_BW),
		    				HOST_STORAGE,
		    				peList,
		    				new VmSchedulerTimeShared(peList)
		    			);
					hostID++;
					newHost.setEnergyIdle(HOST_ENERGY_IDLE);
					newHost.setEnergyFull(HOST_ENERGY_FULL);

					hostList.add(newHost);
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hostList;
	}
	
	public static AsyncDatacenterBroker createBroker(){

		AsyncDatacenterBroker broker = null;
        try {
		broker = new AsyncDatacenterBroker("Broker");
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}
    	return broker;
    }
	
	public static void printCloudletList(List<Cloudlet> list) {
        int size = list.size();
        Cloudlet cloudlet;

        String indent = "    ";
        Log.printLine();
        Log.printLine("========== OUTPUT ==========");
        Log.printLine("Cloudlet ID" + indent + "STATUS" + indent +
                "Data center ID" + indent + "VM ID" + indent + "Time" + indent + "Start Time" + indent + "Finish Time");

        DecimalFormat dft = new DecimalFormat("###.##");
        for (int i = 0; i < size; i++) {
            cloudlet = list.get(i);
            Log.print(indent + cloudlet.getCloudletId() + indent + indent);

            if (cloudlet.getStatus() == Cloudlet.SUCCESS){
                Log.print("SUCCESS");

            	Log.printLine( indent + indent + cloudlet.getResourceId() + indent + indent + indent + indent + cloudlet.getVmId() +
                     indent + indent + dft.format(cloudlet.getActualCPUTime()) + indent + indent + dft.format(cloudlet.getExecStartTime())+
                         indent + indent + dft.format(cloudlet.getFinishTime()));
            }
        }

    }
}
