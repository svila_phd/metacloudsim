package svila.staticAllocation;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.core.CloudSim;
import org.json.JSONObject;

public class SimulationResult {
	
	double makespan;
	double energy;
	
	DatacenterBroker broker;
	
	public SimulationResult(DatacenterBroker broker, double finishTime, double energy) {
		this.broker = broker;
		this.makespan = finishTime;
		this.energy = energy;
	}
	
	public JSONObject getJSONObject() {
		JSONObject jo = new JSONObject();
		jo.put("makespan", this.makespan);
		jo.put("energy", this.energy);
		return jo;
	}
	
	public String toString() {
		return "Makespan: " + makespan + " Energy: " + energy;
	}
}
