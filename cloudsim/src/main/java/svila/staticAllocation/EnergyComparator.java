package svila.staticAllocation;

import java.util.Comparator;

public class EnergyComparator implements Comparator<SimulationResult> {

	@Override
	public int compare(SimulationResult arg0, SimulationResult arg1) {
		if(arg0.energy>arg1.energy)  
			return 1;  
		else if(arg0.energy<arg1.energy)  
			return -1;  
		else  
			return 0;  
	}

}