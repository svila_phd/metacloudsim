package svila.staticAllocation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.json.JSONArray;
import org.json.JSONObject;

import com.cedarsoftware.util.io.JsonReader;

import svila.AsyncDatacenterBroker;
import svila.SvilaPrinter;
import svila.CustomCloudSimulator.CloudManager;
import svila.CustomCloudSimulator.CloudManagerHelper;
import svila.staticAllocation.schedulingTechniques.BasicAlgorithm;
import svila.staticAllocation.schedulingTechniques.CBSAdapted;
import svila.staticAllocation.schedulingTechniques.EnhancedMaxMin;
import svila.staticAllocation.schedulingTechniques.HillClimbingAdapted;
import svila.staticAllocation.schedulingTechniques.BestFitAdapted;
import svila.staticAllocation.schedulingTechniques.MOGAAdapted;
import svila.staticAllocation.schedulingTechniques.MaxMin;
import svila.staticAllocation.schedulingTechniques.MinMinAdapted;
import svila.staticAllocation.schedulingTechniques.PSOFGAAdapted;
import svila.staticAllocation.schedulingTechniques.PSOSA;
import svila.staticAllocation.schedulingTechniques.RandomTechnique;
import svila.staticAllocation.schedulingTechniques.RoundRobin;
import svila.staticAllocation.schedulingTechniques.StaticTaskSchedulingTechnique;
import svila.staticAllocation.schedulingTechniques.extra.JMetalGwasfga;
import svila.staticAllocation.schedulingTechniques.extra.JMetalGwasfgaCloud;
import svila.staticAllocation.schedulingTechniques.extra.JMetalMombi;
import svila.staticAllocation.schedulingTechniques.extra.JMetalNGSAIII;
import svila.staticAllocation.schedulingTechniques.extra.JMetalRandomSearch;

public class StaticExperimentation {
	
	static boolean debug = false;
	static Path basePath;
	static List<Path> experimentConfigurationsPaths;
	static JSONObject ecObject;
	public static Map<Vm, Host> VMToHostMap;
	
	public static void main(String[] args) {  	
		init();
		executeExperiments();
	}
	
	public static void executeExperiments() {
		for(Path currentExperiment : experimentConfigurationsPaths) {
			setECObject(currentExperiment);
			executeFolders();
		}
	}
	
	public static void init() {
		basePath = Paths.get(getBasePath());
    	experimentConfigurationsPaths = getExperimentConfigurationFilenames("experimentNames");
	}
	
	public static void setECObject(Path experimentPath) {
		ecObject = null;
    	try {
    		ecObject = new JSONObject(new String(Files.readAllBytes(experimentPath),"UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void debugExperiment() {
    	/*if(debug) {
    		Experiment ei;
        	ei = new Experiment();
    		ei.setWorkloadInputFile("C:\\Users\\Sergi\\workspace2\\cloudsim4.0\\modules\\cloudsim\\src\\main\\resources\\debugSWF\\svilaDebugSWF.swf");
    		ei.setHostInputFile("C:\\Users\\Sergi\\workspace2\\cloudsim4.0\\modules\\cloudsim\\src\\main\\resources\\debugMachines\\hosts.txt");
    		ei.setVmInputFile("C:\\Users\\Sergi\\workspace2\\cloudsim4.0\\modules\\cloudsim\\src\\main\\resources\\debugMachines\\vms.txt");
    		//ei.setResultFile();
    		ei.setWorkloadName("Test");
    		JSONObject techniqueJson = new JSONObject();
    		techniqueJson.put("name", "random");
    		ei.setTechnique(getTechnique(techniqueJson));
    		ei.getTechnique().setExperimentInfo(ei);
    		//ei.setIteration(i);
    		ei.launchExperiment();
    		System.exit(0);
    	}*/
	}
	
	public static void executeFolders() {
		String hostInputFile = Paths.get(basePath.toString(), ecObject.getString("machinesFolder"), "hosts.txt").toString();
    	String vmInputFile = Paths.get(basePath.toString(), ecObject.getString("machinesFolder"), "vms.txt").toString();
    	int iterations = ecObject.getInt("iterations");
    	JSONArray folders = ecObject.getJSONArray("workloadFolder");

    	for(int i=0; i<folders.length(); i++) {
        	Path workloadInputFolderPath = Paths.get(basePath.toString(), "input", folders.getString(i));
    		String workloadInputFolder = workloadInputFolderPath.toString();
    		File dir = new File(Paths.get(basePath.toString(), "toPlot", ecObject.getString("outputFolderName")).toString());
    		if(!dir.exists()) {
    			dir.mkdir();
    		}
    		Path outputPath = Paths.get(basePath.toString(), "toPlot", ecObject.getString("outputFolderName"), Paths.get(workloadInputFolder).getFileName().toString());

    		executeExperimentFolder(iterations, basePath.toString(), hostInputFile, vmInputFile, outputPath.toString(), workloadInputFolder, ecObject, true);
    	}
	}
	
	public static String getBasePath() {
		File file = new File(StaticExperimentation.class.getResource("/data.json").getFile());
		JSONObject jsonFile = null;
		String basePath = "";
		try {
			jsonFile = new JSONObject(new String(Files.readAllBytes(file.toPath()),"UTF-8"));
			basePath = jsonFile.getString("basepath");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return basePath;
	}
	
	public static List<Path> getExperimentConfigurationFilenames(String experimentNames) {
		File file = new File(StaticExperimentation.class.getResource("/data.json").getFile());
		JSONObject jsonFile = null;
		String basePath = "";
		List<Path> experimentConfigurationFilenames = new ArrayList<>();
		try {
			jsonFile = new JSONObject(new String(Files.readAllBytes(file.toPath()),"UTF-8"));
			basePath = jsonFile.getString("basepath");
			JSONArray names = jsonFile.getJSONArray(experimentNames);
			for(int i=0; i<names.length(); i++) {
				experimentConfigurationFilenames.add(Paths.get(basePath, "experimentConfigurations", names.getString(i)));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return experimentConfigurationFilenames;
	}
	
	public static boolean deleteDir(File dir) {
	      if (dir.isDirectory()) {
	         String[] children = dir.list();
	         for (int i = 0; i < children.length; i++) {
	            boolean success = deleteDir (new File(dir, children[i]));
	            
	            if (!success) {
	               return false;
	            }
	         }
	      }
	      return dir.delete();
	   }
	
	public static void executeExperimentFolder(int iterations, String basePath, String hostInputFile, String vmInputFile, String outputPath, String workloadInputFolder, JSONObject experimentConfiguration, boolean deleteFolder) {
		long startTime = System.nanoTime();
		int numExperiments = 0;
		Path workloadInputFolderPath = Paths.get(workloadInputFolder);
		Path output = Paths.get(outputPath);
		if(deleteFolder) {
			deleteDir(output.toFile());
		}
		
		VMToHostMap = getVMAllocation(hostInputFile, vmInputFile);
		//CloudManager cm = CloudManager.getInstance();
		//cm.reset();
		//CloudManagerHelper.addHosts(SvilaHelper.createHostList(hostInputFile));
		//CloudManagerHelper.mapVMsToHosts(StaticExperimentation.VMToHostMap);
		//cm.executeAllEvents();
		
		
		
		new File(output.toString()).mkdir();
		File workloadFolder = new File(workloadInputFolder);
		Experiment ei;
		Log.setDisabled(!experimentConfiguration.getBoolean("verbose"));
		SvilaPrinter.enable(experimentConfiguration.getBoolean("verbose"));
		
		File[] workloadFiles = workloadFolder.listFiles();
		List<StaticTaskSchedulingTechnique> techniqueList = getTechniques(experimentConfiguration);
		
		int calculatedNumExperiments = workloadFiles.length * techniqueList.size() * iterations;
		
		for(File specificWorkloadFile : workloadFiles) {
			String workloadFilename = Paths.get(specificWorkloadFile.getName()).getFileName().toString().replaceFirst("[.][^.]+$", "");
			Path specificWorkloadOutput = Paths.get(outputPath.toString(), workloadFilename);
			new File(specificWorkloadOutput.toString()).mkdir();
			int currentWorkloadExperimentNum = 0;
			
			for(StaticTaskSchedulingTechnique stst : techniqueList) {
				Path techniqueOutput = Paths.get(specificWorkloadOutput.toString(), stst.getTechniqueName());
				new File(techniqueOutput.toString()).mkdir();
				int currentTechniqueExperimentNum = 0;
				for(int i=0; i<iterations; i++) {
					Path resultFile = Paths.get(techniqueOutput.toString(), String.valueOf(i));
					ei = new Experiment();
					ei.setHostInputFile(hostInputFile);
					ei.setWorkloadInputFile(specificWorkloadFile.toString());
					ei.setVmInputFile(vmInputFile);
					ei.setResultFile(resultFile.toString());
					ei.setWorkloadType(workloadInputFolderPath.getFileName().toString());
					ei.setWorkloadName(workloadFilename);
					ei.setTechnique(stst);
					ei.getTechnique().setExperimentInfo(ei);
					ei.setIteration(i);
					
					stst.setTunning(experimentConfiguration.getBoolean("tunning"));
					
					System.out.println(workloadFolder.getName() + " " + workloadFilename +
							" " + stst.getTechniqueName() + " " + i);
					
					ei.launchExperiment();
					ei.export();
					
					numExperiments++;
					currentTechniqueExperimentNum++;
					currentWorkloadExperimentNum++;

					System.out.println("Experiment " + numExperiments + " of " + calculatedNumExperiments +
							". Current workload: " + currentWorkloadExperimentNum + " of " + (iterations * techniqueList.size())  +
							". Current technique: " + currentTechniqueExperimentNum + " of " + iterations);
				}
			}
		}

		long stopTime = System.nanoTime();
	    long elapsedTime = stopTime - startTime;
		System.out.println();
		System.out.println("-----------------------------");
		System.out.println("-----Experiment finished-----");
		System.out.println("-----------------------------");
		System.out.println("Number of experiments: " + numExperiments);
	    System.out.println("Time: " + elapsedTime / 1000000000.0);
		System.out.println();
	}
	
	private static void executeExperiment(Experiment experimentInfo) {
		int num_user = 1;   // number of cloud users
    	Calendar calendar = Calendar.getInstance();
    	boolean trace_flag = false;  // mean trace events
		
    	CloudSim.init(num_user, calendar, trace_flag);
    	
		@SuppressWarnings("unused")
		Datacenter datacenter = SvilaHelper.createDatacenterFromVMs("Datacenter", experimentInfo.getVmInputFile());
		
		AsyncDatacenterBroker broker = SvilaHelper.createBroker();
    	int brokerId = broker.getId();
    	
    	experimentInfo.setDatacenterBroker(broker);

    	List<Vm> vmList = SvilaHelper.createVmList(brokerId, experimentInfo.getVmInputFile());
    	broker.submitVmList(vmList);

    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(experimentInfo.getWorkloadInputFile(), true, brokerId);
    	
    	List<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();
    	broker.submitCloudletList(cloudletList);
    	
    	StaticTaskSchedulingTechnique technique = experimentInfo.getTechnique();
    	technique.setEnvironment(broker, cloudletList, vmList, datacenter.getHostList());
    	technique.setExperimentInfo(experimentInfo);
    	broker.setTechnique(technique);
    	
    	// Sixth step: Starts the simulation
    	double finishTime = CloudSim.startSimulation();

    	// Final step: Print results when simulation is over
    	List<Cloudlet> cloudletReceivedList = broker.getCloudletReceivedList();
    	HashMap<Vm, Host> vmHostMap = broker.vmHostMap;
    	HashMap<Integer, Vm> vmIdMap = broker.vmIdMap;
    	
    	CloudSim.stopSimulation();
    	
    	//calculateResult(finishTime, vmHostMap, vmIdMap, cloudletReceivedList);
    	
    	printCloudletList(broker.getCloudletReceivedList());
    	
    	System.out.println(experimentInfo);
	}
	
	public static Map<Vm, Host> getVMAllocation(String hostInputFile, String vmInputFile) {
    	int num_user = 1;   // number of cloud users
    	Calendar calendar = Calendar.getInstance();
    	boolean trace_flag = false;  // mean trace events
		
    	CloudSim.init(num_user, calendar, trace_flag);
    	
		@SuppressWarnings("unused")
		Datacenter datacenter = SvilaHelper.createDatacenterFromVMs("Datacenter", vmInputFile);
		
		AsyncDatacenterBroker broker = SvilaHelper.createBroker();
    	int brokerId = broker.getId();
    	broker.setExternalSchedule(true);
    	List<Vm> vmList = SvilaHelper.createVmList(brokerId, vmInputFile);
    	broker.submitVmList(vmList);
    	
    	double finishTime = CloudSim.startSimulation();

    	CloudSim.stopSimulation();
    	
    	return broker.vmHostMap;
	}
	
	private static List<StaticTaskSchedulingTechnique> getTechniques(JSONObject jo) {
		List<StaticTaskSchedulingTechnique> techniques = new ArrayList<>();
		JSONArray ja = jo.getJSONArray("algorithms");
		for(int i=0; i<ja.length(); i++) {
			JSONObject techniqueJson = ja.getJSONObject(i);
			StaticTaskSchedulingTechnique stst = getTechnique(techniqueJson);
			techniques.add(stst);
		}
		return techniques;
	}
	
	public static StaticTaskSchedulingTechnique getTechnique(JSONObject techniqueJson) {
		StaticTaskSchedulingTechnique stst = null;
		String name = techniqueJson.getString("name");
		switch(name){
		case "random":
			stst = new RandomTechnique();
			break;
		case "round-robin":
			stst = new RoundRobin();
			break;
		case "psosa":
			stst = new PSOSA();
			stst.setExtraFields(techniqueJson);
			break;
		case "basic":
			stst = new BasicAlgorithm();
			break;
		case "moga":
			stst = new MOGAAdapted();
			break;
		case "hill":
			stst = new HillClimbingAdapted();
			break;
		case "best-fit":
			stst = new BestFitAdapted();
			break;
		case "cbs":
			stst = new CBSAdapted();
			break;
		case "min-min":
			stst = new MinMinAdapted();
			break;
		case "max-min":
			stst = new MaxMin();
			break;
		case "enhanced-max-min":
			stst = new EnhancedMaxMin();
			break;
		case "psofga":
			stst = new PSOFGAAdapted();
			break;
		case "hybrid":
			//stst = new HybridTechnique();
			break;
		case "jmetal-random-search":
			stst = new JMetalRandomSearch();
			break;
		case "jmetal-gwasfga":
			stst = new JMetalGwasfga();
			stst.setExtraFields(techniqueJson);
			break;
		case "jmetal-mombi":
			stst = new JMetalMombi();
			break;
		case "jmetal-ngsaiii":
			stst = new JMetalNGSAIII();
			break;
		default:
			System.out.println("Technique " + name + " doesn't exist");
			System.exit(-1);
			break;
		}
		return stst;
	}
	
	public static void executeExperiment(String hostInputFile, String vmInputFile, String workloadInputFile) {
		// Second step: Create Datacenters
    	//Datacenters are the resource providers in CloudSim. We need at list one of them to run a CloudSim simulation
    	@SuppressWarnings("unused")
		Datacenter datacenter = SvilaHelper.createDatacenterFromVMs("Datacenter", vmInputFile);

    	//Third step: Create Broker
    	DatacenterBroker broker = SvilaHelper.createBroker();
    	int brokerId = broker.getId();

    	//Fourth step: Create one virtual machine
    	List<Vm> vmList = SvilaHelper.createVmList(brokerId, vmInputFile);

    	//submit vm list to the broker
    	broker.submitVmList(vmList);

    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(workloadInputFile, true, brokerId);
    	
    	List<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();
    	broker.submitCloudletList(cloudletList);

    	// Sixth step: Starts the simulation
    	CloudSim.startSimulation();

    	// Final step: Print results when simulation is over
    	List<Cloudlet> newList = broker.getCloudletReceivedList();

    	CloudSim.stopSimulation();	

    	SvilaHelper.printCloudletList(newList);

    	Log.printLine("Static Experimentation finished!");
    	
    	StaticAllocationJSONOutput.setEnvironment(datacenter, datacenter.getHostList(), vmList, broker.getCloudletReceivedList());
    	StaticAllocationJSONOutput.showJSON();
    	
    	PostExperimentCalculations pec = new PostExperimentCalculations(datacenter, broker.getCloudletReceivedList(), vmList, datacenter.getHostList());
    	pec.showStatistics();
    	
    	System.out.println();
		System.out.println("------------------------------");
		System.out.println("---Experimentation finished---");
		System.out.println("------------------------------");
		System.out.println();
	}
	
	/**
	 * Prints the Cloudlet objects.
	 *
	 * @param list list of Cloudlets
	 */
	private static void printCloudletList(List<Cloudlet> list) {
		int size = list.size();
		Cloudlet cloudlet;

		String indent = "    ";
		Log.printLine();
		Log.printLine("========== OUTPUT ==========");
		Log.printLine("Cloudlet ID" + indent + "STATUS" + indent
				+ "Data center ID" + indent + "VM ID" + indent + "Time" + indent
				+ "Start Time" + indent + "Finish Time");

		DecimalFormat dft = new DecimalFormat("###.##");
		for (int i = 0; i < size; i++) {
			cloudlet = list.get(i);
			Log.print(indent + cloudlet.getCloudletId() + indent + indent);

			if (cloudlet.getCloudletStatus() == Cloudlet.SUCCESS) {
				Log.print("SUCCESS");

				Log.printLine(indent + indent + cloudlet.getResourceId()
						+ indent + indent + indent + cloudlet.getVmId()
						+ indent + indent
						+ dft.format(cloudlet.getActualCPUTime()) + indent
						+ indent + dft.format(cloudlet.getExecStartTime())
						+ indent + indent
						+ dft.format(cloudlet.getFinishTime()));
			}
		}
	}
}
