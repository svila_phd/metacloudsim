package svila.staticAllocation;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Vm;

public class StaticAllocation {
	private Cloudlet task;
	private Vm vm;
	private double startTime, endTime, executionTime;
	
	public StaticAllocation(Cloudlet task, Vm vm, double startTime) {
		this.task = task;
		this.vm = vm;
		this.executionTime = task.getCloudletLength() / vm.getMips();
		this.startTime = startTime;
		this.endTime = startTime + this.executionTime;
	}
	
	public static boolean canAllocCloudletIntoVm(Cloudlet task, Vm vm) {
		return task.getNumberOfPes() < vm.getNumberOfPes();
	}
	
	public static double getExecutionTime(Cloudlet task, Vm vm) {
		return task.getCloudletLength() / vm.getMips();
	}
}
