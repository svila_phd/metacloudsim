package svila.staticAllocation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SWFWorkload {
	
	private List<SWFJob> jobList;
	
	public SWFWorkload(List<SWFJob> jobList) {
		this.jobList= jobList;
	}
	
	public List<SWFJob> getJobList() {
		return jobList;
	}
	
	public int getMinTimestamp() {
		return jobList.get(0).getSubmitTime();
	}
	
	public int getMaxTimestamp() {
		return jobList.get(jobList.size()-1).getSubmitTime();
	}
	
	public int getTotalTime() {
		return getMaxTimestamp() - getMinTimestamp() + 1;
	}
	
	public SWFWorkload getJobsInRange(int start, int end) {
		List<SWFJob> rangedJobList = new ArrayList<>();
		Iterator<SWFJob> iterator = jobList.iterator();
		SWFJob currentJob;
		while(iterator.hasNext()) {
			currentJob = iterator.next();
			if(currentJob.getSubmitTime() >= end) {
				break;
			}
			
			if(currentJob.getSubmitTime() >= start && currentJob.getSubmitTime() < end ) {
				rangedJobList.add(currentJob);
			}
			
		}
		
		return new SWFWorkload(rangedJobList);
	}
}
