package svila.staticAllocation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;

public class SWFAnalyzer {
	
	private SWFWorkload workload;
	private int brokerId;
	private boolean batchedTasks = true;
	
	public SWFAnalyzer(String file, boolean batchedTasks) {
		this.batchedTasks = batchedTasks;
		readSWFFile(file);
		this.brokerId = 1;
	}
	
	public SWFAnalyzer(String file, boolean batchedTasks, int brokerId) {
		this.batchedTasks = batchedTasks;
		readSWFFile(file);
		this.brokerId = brokerId;
	}
	
	public void setUserId(int userId) {
		this.brokerId = userId;
	}
	
	public void readSWFFile(String filepath) {
		BufferedReader br = null;
		FileReader fr = null;
		
		try{
			fr = new FileReader(filepath);
			br = new BufferedReader(fr);
			String currentLine;
			String[] splitLine;
			List<SWFJob> jobList = new ArrayList<>();
			
			while((currentLine = br.readLine()) != null) {
				if(currentLine.charAt(0) == ';') {
					continue;
				}
				splitLine = currentLine.trim().split("\\s+");
				int submitTime = batchedTasks ? 0 : Integer.parseInt(splitLine[1]);
				jobList.add(new SWFJob(
									Integer.parseInt(splitLine[0]),
									submitTime,
									Integer.parseInt(splitLine[3]),
									Integer.parseInt(splitLine[7]),
									Integer.parseInt(splitLine[8]),
									Integer.parseInt(splitLine[9])
								)
							);
			}
			workload = new SWFWorkload(jobList);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}
		}
	}
	
	public ArrayList<Cloudlet> getAllCloudlets() {
		ArrayList<Cloudlet> cloudletList = new ArrayList<>();
		int cloudletId = 0;
		UtilizationModel utilizationModel = new UtilizationModelFull();
		for(SWFJob job : workload.getJobList()) {
			Cloudlet cloudlet = new Cloudlet(cloudletId, job.getRequestedTime() * 1000, job.getRequestedProcessors(), 1, 1, utilizationModel, utilizationModel, utilizationModel);
        	cloudlet.setUserId(brokerId);
            cloudlet.setExecStartTime(job.getSubmitTime());
            cloudletList.add(cloudlet);
            
            cloudletId++;
		}
		return cloudletList;
	}
	
	public void plotWorkloadInRangeWithInterval(int start, int end, int interval) {
		
	}
}
