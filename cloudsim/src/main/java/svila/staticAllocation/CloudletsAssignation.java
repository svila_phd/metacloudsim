package svila.staticAllocation;

import java.util.HashMap;
import java.util.Map;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Vm;

public class CloudletsAssignation {
	public Map<Integer, Integer> cloudletVMMap;
	
	public CloudletsAssignation() {
		cloudletVMMap = new HashMap<>();
	}
	
	public CloudletsAssignation(CloudletsAssignation ca) {
		cloudletVMMap = new HashMap<>(ca.cloudletVMMap);
	}
	
	public void linkCloudletWithVM(Cloudlet c, Vm vm) {
		cloudletVMMap.put(c.getCloudletId(), vm.getId());
	}
	
	public void linkCloudletWithVM(int cloudletId, int vmId) {
		cloudletVMMap.put(cloudletId, vmId);
	}
	
	public int getVMId(Cloudlet c) {
		return cloudletVMMap.get(c.getCloudletId());
	}
	
	public int getVMId(int cloudletId) {
		return cloudletVMMap.get(cloudletId);
	}
}
