package svila.staticAllocation;

import java.util.Comparator;

public class MakespanComparator implements Comparator<SimulationResult> {

	public MakespanComparator() {
		
	}
	
	@Override
	public int compare(SimulationResult arg0, SimulationResult arg1) {
		if(arg0.makespan>arg1.makespan)  
			return 1;  
		else if(arg0.makespan<arg1.makespan)  
			return -1;  
		else  
			return 0;  
	}

}
