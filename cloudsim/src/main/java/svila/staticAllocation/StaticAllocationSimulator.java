package svila.staticAllocation;

import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

public class StaticAllocationSimulator {
	
	List<Cloudlet> cloudlets;
	List<Vm> vms;
	List<Host> hosts;
	
	List<Cloudlet> pendingTasks;
	List<StaticAllocation> assignedTasks;
	
	List<Vm> currentAvailableVms;
	List<Vm> partiallyAvailableVms;
	List<Vm> fullVms;
	
	List<StaticAllocation> scheduledAllocations;
	List<StaticAllocation> runningAllocations;
	List<StaticAllocation> finishedAllocations;
	
	public StaticAllocationSimulator(List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		this.cloudlets = cloudlets;
		this.vms = vms;
		this.hosts = hosts;
	}
	
	public void runSimulation() {
		/*pendingTasks = generateTasks();
		while(!pendingTasks.isEmpty()) {
			if(newAssignationIsAvailable()) {
				decideBestAssignation();
			} else {
				getFirstFinishAllocationTime();
				finalizeAllocations();
				
			}
		}*/
	}

}
