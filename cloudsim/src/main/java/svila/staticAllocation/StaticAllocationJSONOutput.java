package svila.staticAllocation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletScheduler;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.ResCloudlet;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerHost;
import org.json.JSONArray;
import org.json.JSONObject;

import com.cedarsoftware.util.io.JsonWriter;

import svila.JSONOutput;

public class StaticAllocationJSONOutput {
    private static JSONObject root;
    private static JSONArray array;
    private static Datacenter datacenter;
    
    private static String EXPERIMENT = "Experiment";
    private static String TIME = "Time";
    private static String HOSTS = "Hosts";
    private static String VMS = "VMs";
    private static String CLOUDLETS = "Cloudlets";
    private static String ID = "Id";
    
    public static void init() {
        root = new JSONObject();
        //array = new JSONArray();
        //root.put(EXPERIMENT, array);
    }
    
    public static void setEnvironment(Datacenter datacenter, List<Host> hostList, List<Vm> vmList, List<Cloudlet> cloudletList) {
        if(root == null) {
            return;
        }
        
        StaticAllocationJSONOutput.datacenter = datacenter;
        
        JSONObject joEnv = new JSONObject();

        JSONArray hostArray = new JSONArray(),
                  vmArray = new JSONArray(),
                  cloudletArray = new JSONArray();
        joEnv.put(HOSTS, hostArray);
        joEnv.put(VMS, vmArray);
        joEnv.put(CLOUDLETS, cloudletArray);
        
        for(Host host : hostList) {
            JSONObject joHost = new JSONObject();
            joHost.put(ID, host.getId());
            joHost.put("NUM_PE", host.getNumberOfPes());
            joHost.put("MIPS", host.getTotalMips());
            hostArray.put(joHost);
        }
        
        for(Vm vm : vmList) {
            JSONObject joVM = new JSONObject();
            joVM.put(ID, vm.getId());
            joVM.put("NUM_PE", vm.getNumberOfPes());
            joVM.put("MIPS", vm.getMips());
            vmArray.put(joVM);
        }
        
        DecimalFormat dft = new DecimalFormat("###.##");
        for(Cloudlet cloudlet : cloudletList) {
            JSONObject joCloudlet = new JSONObject();
            joCloudlet.put(ID, cloudlet.getCloudletId());
            joCloudlet.put("AssignedVM", cloudlet.getVmId());
            joCloudlet.put("SubmissionTime", dft.format(cloudlet.getSubmissionTime()));
            joCloudlet.put("CPUTime", dft.format(cloudlet.getActualCPUTime()));
            joCloudlet.put("StartTime", dft.format(cloudlet.getExecStartTime()));
            joCloudlet.put("FinishTime", dft.format(cloudlet.getFinishTime()));
            cloudletArray.put(joCloudlet);
        }
        
        root.put("Environment", joEnv);
    }
    
    public static JSONObject getJSON() {
        return root;
    }
    
    public static void showJSON() {
        System.out.println(root);
    }
    
    public static void writeJSON(String path) {
        
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(path));
            bw.write(JsonWriter.formatJson(root.toString()));
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(JSONOutput.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
