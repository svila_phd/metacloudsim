package svila.staticAllocation;

import org.cloudbus.cloudsim.Vm;

public class VmStatus {
	Vm vm;
	int currentAvailablePEs;
	double lastTimeUpdate;
	
	public VmStatus(Vm vm) {
		this.vm = vm;
		this.lastTimeUpdate = 0;
	}
	
	public Vm getVm() {
		return vm;
	}

	public int getCurrentAvailablePEs() {
		return currentAvailablePEs;
	}

	public double getLastTimeUpdate() {
		return lastTimeUpdate;
	}

	public int freePEs(int pe, double lastTimeUpdate) {
		this.currentAvailablePEs += pe;
		this.lastTimeUpdate = lastTimeUpdate;
		return getCurrentAvailablePEs();
	}
	
	public int usePEs(int pe, double lastTimeUpdate) {
		this.currentAvailablePEs -= pe;
		this.lastTimeUpdate = lastTimeUpdate;
		return getCurrentAvailablePEs();
	}
}
