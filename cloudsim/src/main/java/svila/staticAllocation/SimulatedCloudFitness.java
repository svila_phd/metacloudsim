package svila.staticAllocation;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

public class SimulatedCloudFitness {
	
	static HashMap<Integer, Cloudlet> cloudletMap;
	static HashMap<Integer, Vm> vmMap;
	static HashMap<Integer, Host> hostMap;
	
	static HashMap<Vm, Host> vmToHost;
	
	public static void setVMtoHostAssignation() {
		vmToHost = new HashMap<>();
	}
	
	public static double getMakespan(CloudletsAssignation ca) {
		double makespan = 0.0;
		
		Iterator<Integer> it = ca.cloudletVMMap.keySet().iterator();
		
		while(it.hasNext()) {
			Cloudlet c = cloudletMap.get(it.next());
			// Agrupar els cloudlets per VM
			// Sumar la duració de tots els cloudlets
			// Dividir per la potència de la VM
			// Cal tenir en compte que s'ha d'utilitzar com a molt
			// tants cores com tingui el cloudlet, pero si un cloudlet
			// té més cores que la VM, només es poden fer anar els cores
			// de la VM
		}
		
		return makespan;
	}
	
	public static double getEnergy(CloudletsAssignation ca) {
		double energy = 0.0;
		
		return energy;
	}
}
