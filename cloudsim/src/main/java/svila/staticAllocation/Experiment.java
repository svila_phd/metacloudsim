package svila.staticAllocation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.json.JSONObject;

import com.cedarsoftware.util.io.JsonWriter;

import svila.AsyncDatacenterBroker;
import svila.JSONOutput;
import svila.staticAllocation.schedulingTechniques.StaticTaskSchedulingTechnique;

public class Experiment {
	
	private DatacenterBroker datacenterBroker;
	private SimulationResult simulationResult;
	private String workloadType, workloadName;
	private String hostInputFile, vmInputFile, workloadInputFile, resultFile;
	private int iteration;
	private StaticTaskSchedulingTechnique technique;
	
	public Experiment() {

	}
	
	public void launchExperiment() {
		technique.launch();
		System.out.println(toString());
	}
	
	public void launchStandaloneExperiment() {
		int num_user = 1;   // number of cloud users
    	Calendar calendar = Calendar.getInstance();
    	boolean trace_flag = false;  // mean trace events
		
    	CloudSim.init(num_user, calendar, trace_flag);
    	
		@SuppressWarnings("unused")
		Datacenter datacenter = SvilaHelper.createDatacenterFromVMs("Datacenter", getVmInputFile());
		
		AsyncDatacenterBroker broker = SvilaHelper.createBroker();
    	int brokerId = broker.getId();
    	
    	setDatacenterBroker(broker);

    	List<Vm> vmList = SvilaHelper.createVmList(brokerId, getVmInputFile());
    	broker.submitVmList(vmList);

    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(getWorkloadInputFile(), true, brokerId);
    	
    	List<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();
    	broker.submitCloudletList(cloudletList);
    	
    	StaticTaskSchedulingTechnique technique = getTechnique();
    	technique.setEnvironment(broker, cloudletList, vmList, datacenter.getHostList());
    	technique.setExperimentInfo(this);
    	broker.setTechnique(technique);
    	
    	// Sixth step: Starts the simulation
    	double finishTime = CloudSim.startSimulation();
    	finishTime = broker.finishTime;
    	// Final step: Print results when simulation is over
    	List<Cloudlet> cloudletReceivedList = broker.getCloudletReceivedList();
    	HashMap<Vm, Host> vmHostMap = broker.vmHostMap;
    	HashMap<Integer, Vm> vmIdMap = broker.vmIdMap;

    	CloudSim.stopSimulation();
    	
    	calculateResult(finishTime, vmHostMap, vmIdMap, cloudletReceivedList);
    	
    	printCloudletList(cloudletReceivedList);
    	
    	/*
    	System.out.println("VM Size: " + vmList.size());
    	for(Entry<Vm, Host> entry : vmHostMap.entrySet()) {
    		System.out.println(entry.getKey().getId() + " " + entry.getValue().getId());
    	}
    	
    	System.out.println("Hosts: " + datacenter.getHostList().size());
    	for(Host h : datacenter.getHostList()) {
    		System.out.println(h.getVmList().size());
    	}
    	*/
	}
	
	public DatacenterBroker getDatacenterBroker() {
		return datacenterBroker;
	}

	public void setDatacenterBroker(DatacenterBroker datacenterBroker) {
		this.datacenterBroker = datacenterBroker;
	}
	
	public String getWorkloadType() {
		return workloadType;
	}

	public void setWorkloadType(String workloadType) {
		this.workloadType = workloadType;
	}

	public String getWorkloadName() {
		return workloadName;
	}

	public void setWorkloadName(String workloadName) {
		this.workloadName = workloadName;
	}

	public StaticTaskSchedulingTechnique getTechnique() {
		return technique;
	}

	public void setTechnique(StaticTaskSchedulingTechnique technique) {
		this.technique = technique;
	}

	public int getIteration() {
		return iteration;
	}

	public void setIteration(int iteration) {
		this.iteration = iteration;
	}

	public String getHostInputFile() {
		return hostInputFile;
	}

	public void setHostInputFile(String hostInputFile) {
		this.hostInputFile = hostInputFile;
	}

	public String getVmInputFile() {
		return vmInputFile;
	}

	public void setVmInputFile(String vmInputFile) {
		this.vmInputFile = vmInputFile;
	}
	
	public String getResultFile() {
		return resultFile;
	}

	public void setResultFile(String resultFile) {
		this.resultFile = resultFile;
	}

	public String getWorkloadInputFile() {
		return workloadInputFile;
	}

	public void setWorkloadInputFile(String workloadInputFile) {
		this.workloadInputFile = workloadInputFile;
	}

	public void setSimulationResult(SimulationResult simulationResult) {
		this.simulationResult = simulationResult;
	}

	public void calculateResult(double finishTime, HashMap<Vm, Host> vmHostMap, HashMap<Integer, Vm> vmIdMap, List<Cloudlet> cloudletList) {
		simulationResult = new SimulationResult(datacenterBroker, finishTime, calculateEnergy(finishTime, vmHostMap, vmIdMap, cloudletList));
		
	}
	
	public SimulationResult getSimulationResult() {
		return simulationResult;
	}
	
	private double calculateEnergy(double finishTime, HashMap<Vm, Host> vmHostMap, HashMap<Integer, Vm> vmIdMap, List<Cloudlet> cloudletList) {
		//HashMap<Host, List<Cloudlet>> groupedCloudletsByHost = groupCloudletsByHost(vmHostMap, vmIdMap, cloudletList);
		//return calculateEnergyForEachHost(finishTime, lastCloudletMap); // Old formula

		
		HashMap<Vm, List<Cloudlet>> groupCloudletsByVm = groupCloudletsByVm(vmIdMap, cloudletList);
		
		List<VMLastInformation> vmLastInformationList = getLastCloudletTimeForEachVM(groupCloudletsByVm, vmHostMap);
		return calculateEnergyForEachVM(finishTime, vmLastInformationList);
	}
	
	private double calculateEnergyForEachHost(double finishTime, HashMap<Host, Cloudlet> lastCloudletMap) {
		double energy = 0.0;
		
		for(Entry<Host, Cloudlet> entry : lastCloudletMap.entrySet()) {
			Host host = entry.getKey();
			double hostFinishTime = entry.getValue().getFinishTime();
			double hostEnergy = hostFinishTime * host.getEnergyFull()/3600.0 +
					  (finishTime - hostFinishTime) * host.getEnergyIdle()/3600.0;
			energy += hostEnergy;
		}
		System.out.println("Total energy: " + energy);
		return energy;
	}
	
	private HashMap<Host, Cloudlet> FindLastCloudletForEachHost(HashMap<Host, List<Cloudlet>> groupedCloudletsByHost) {
		HashMap<Host, Cloudlet> lastCloudlets = new HashMap<>();
		
		for(Entry<Host, List<Cloudlet>> hostGroup : groupedCloudletsByHost.entrySet()) {
			Host host = hostGroup.getKey();
			double lastTick = 0.0;
			Cloudlet lastCloudlet = null;
			for(Cloudlet c : hostGroup.getValue()) {
				if(c.getFinishTime() > lastTick) {
					lastTick = c.getFinishTime();
					lastCloudlet = c;
				}
			}
			lastCloudlets.put(host, lastCloudlet);
		}
		
		return lastCloudlets;
	}
	
	private HashMap<Vm, List<Cloudlet>> groupCloudletsByVm(HashMap<Integer, Vm> vmIdMap, List<Cloudlet> cloudletList) {
		HashMap<Vm, List<Cloudlet>> groupedCloudlets = new HashMap<>();
		
		for(Cloudlet c : cloudletList) {
			Vm vm = vmIdMap.get(c.getVmId());
			List<Cloudlet> l;
			if(groupedCloudlets.containsKey(vm)) {
				l = groupedCloudlets.get(vm);
				l.add(c);
			} else {
				l = new ArrayList<>();
				l.add(c);
				groupedCloudlets.put(vm, l);
			}
			
		}
		return groupedCloudlets;
	}
	
	private HashMap<Host, List<Vm>> groupVmsByHost(HashMap<Vm, Host> vmHostMap) {
		HashMap<Host, List<Vm>> groupedVms = new HashMap<>();

		for(Entry<Vm, Host> entry : vmHostMap.entrySet()) {
			List<Vm> vmList;
			if(groupedVms.containsKey(entry.getValue())) {
				vmList = groupedVms.get(entry.getValue());
				vmList.add(entry.getKey());
			} else {
				vmList = new ArrayList<>();
				vmList.add(entry.getKey());
				groupedVms.put(entry.getValue(), vmList);
			}
			
		}
		
		return groupedVms;
	}
	
	private HashMap<Host, List<Cloudlet>> groupCloudletsByHost(HashMap<Vm, Host> vmHostMap, HashMap<Integer, Vm> vmIdMap, List<Cloudlet> cloudletList) {
		HashMap<Host, List<Cloudlet>> groupedCloudlets = new HashMap<>();

		for(Cloudlet c : cloudletList) {
			Host host = vmHostMap.get(vmIdMap.get(c.getVmId()));
			List<Cloudlet> cloudletsHostList;
			if(groupedCloudlets.containsKey(host)) {
				cloudletsHostList = groupedCloudlets.get(host);
				cloudletsHostList.add(c);
			} else {
				cloudletsHostList = new ArrayList<>();
				cloudletsHostList.add(c);
				groupedCloudlets.put(host, cloudletsHostList);
			}
			
		}
		
		return groupedCloudlets;
	}
	
	private double calculateEnergyForEachVM(double finishTime, List<VMLastInformation> VMLastInformationList) {
		double energy = 0.0;
		
		for(VMLastInformation vmInfo : VMLastInformationList) {
			int numCoresOfVM = vmInfo.vm.getNumberOfPes();
			int numCloudlets = vmInfo.runningCloudlets;
			int workingCores = Math.min(numCoresOfVM, numCloudlets);
			int nonWorkingCores = numCoresOfVM - workingCores;
			double lastTime = vmInfo.lastTime;
			double energyRunningPerCore = vmInfo.host.getEnergyFull() / vmInfo.host.getNumberOfPes();
			double energyIdlePerCore = vmInfo.host.getEnergyIdle() / vmInfo.host.getNumberOfPes();
			double currentRunningEnergy = lastTime * workingCores * energyRunningPerCore / 3600.0;
			double currentIdleEnergy = ((finishTime - lastTime) * workingCores * energyIdlePerCore +
										nonWorkingCores * finishTime * energyIdlePerCore) / 3600.0;
			energy += currentRunningEnergy + currentIdleEnergy;
		}
		
		return energy;
	}
	
	private List<VMLastInformation> getLastCloudletTimeForEachVM(HashMap<Vm, List<Cloudlet>> groupedCloudletsByVM, HashMap<Vm, Host> vmHostMap) {
		List<VMLastInformation> vmLastInformationList = new ArrayList<>();
		
		Iterator<Entry<Vm, List<Cloudlet>>> it = groupedCloudletsByVM.entrySet().iterator();
		
		while(it.hasNext()) {
			Entry<Vm, List<Cloudlet>> entry = it.next();
			Vm vm = entry.getKey();
			int cloudletsRunning = entry.getValue().size();
			Host host = vmHostMap.get(vm);
			VMLastInformation currentInfo = new VMLastInformation(vm, host, FindLastCloudletTime(entry.getValue()), cloudletsRunning);
			vmLastInformationList.add(currentInfo);
		}
		
		return vmLastInformationList;
	}
	
	private double FindLastCloudletTime(List<Cloudlet> cloudletList) {
		double lastTick = 0.0;
		for(Cloudlet c : cloudletList) {
			if(c.getFinishTime() > lastTick) {
				lastTick = c.getFinishTime();
			}
		}
		
		return lastTick;
	}
	
	public String toString() {
		return "Workload: " + workloadName + " Technique: " + technique.getTechniqueName() +
				" Iteration: " + iteration + " Execution time: " +
				technique.getExecutionTime() + " Results: " + simulationResult.toString();
	}
	
	public void export() {
		try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(resultFile + ".json"));
            JSONObject srjo = simulationResult.getJSONObject();
            JSONObject jo = new JSONObject();
            jo.put("workloadType", workloadType);
            jo.put("workload", workloadName);
            jo.put("technique", technique.getTechniqueName());
            jo.put("iteration", iteration);
            jo.put("executionTime", technique.getExecutionTime());
            jo.put("makespan", srjo.getDouble("makespan"));
            jo.put("energy", srjo.getDouble("energy"));
            jo.put("extraFields", technique.getExtraFields());
            System.out.println(technique.getExtraFields().toString());
            
            bw.write(JsonWriter.formatJson(jo.toString()));
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(JSONOutput.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
	
	private void printCloudletList(List<Cloudlet> list) {
		int size = list.size();
		Cloudlet cloudlet;

		String indent = "    ";
		Log.printLine();
		Log.printLine("========== OUTPUT ==========");
		Log.printLine("Cloudlet ID" + indent + "STATUS" + indent
				+ "Data center ID" + indent + "VM ID" + indent + "Time" + indent
				+ "Start Time" + indent + "Finish Time");

		DecimalFormat dft = new DecimalFormat("###.##");
		for (int i = 0; i < size; i++) {
			cloudlet = list.get(i);
			Log.print(indent + cloudlet.getCloudletId() + indent + indent);

			if (cloudlet.getCloudletStatus() == Cloudlet.SUCCESS) {
				Log.print("SUCCESS");

				Log.printLine(indent + indent + cloudlet.getResourceId()
						+ indent + indent + indent + cloudlet.getVmId()
						+ indent + indent
						+ dft.format(cloudlet.getActualCPUTime()) + indent
						+ indent + dft.format(cloudlet.getExecStartTime())
						+ indent + indent
						+ dft.format(cloudlet.getFinishTime()));
			}
		}
	}
	
	private class VMLastInformation {
		public Vm vm;
		public Host host;
		public double lastTime;
		public int runningCloudlets;
		
		public VMLastInformation(Vm vm, Host host, double lastTime, int runningCloudlets) {
			this.vm = vm;
			this.host = host;
			this.lastTime = lastTime;
			this.runningCloudlets = runningCloudlets;
		}
	}
}
