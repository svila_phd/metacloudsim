package svila.staticAllocation;

public class SWFJob {
	
	private int jobNumber;
	private int submitTime;
	private int runTime;
	private int requestedProcessors;
	private int requestedTime;
	private int requestedMemory;
	
	public SWFJob(int jobNumber, int submitTime, int runTime, int requestedProcessors, int requestedTime,
			int requestedMemory) {
		this.jobNumber = jobNumber;
		this.submitTime = submitTime;
		this.runTime = runTime;
		this.requestedProcessors = requestedProcessors;
		this.requestedTime = requestedTime;
		this.requestedMemory = requestedMemory;
	}

	public int getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(int jobNumber) {
		this.jobNumber = jobNumber;
	}

	public int getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(int submitTime) {
		this.submitTime = submitTime;
	}

	public int getRunTime() {
		return runTime;
	}

	public void setRunTime(int runTime) {
		this.runTime = runTime;
	}

	public int getRequestedProcessors() {
		return requestedProcessors;
	}

	public void setRequestedProcessors(int requestedProcessors) {
		this.requestedProcessors = requestedProcessors;
	}

	public int getRequestedTime() {
		return requestedTime;
	}

	public void setRequestedTime(int requestedTime) {
		this.requestedTime = requestedTime;
	}

	public int getRequestedMemory() {
		return requestedMemory;
	}

	public void setRequestedMemory(int requestedMemory) {
		this.requestedMemory = requestedMemory;
	}
	
}
