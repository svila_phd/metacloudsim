package svila.staticAllocation;

import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

import svila.SvilaPrinter;

public class PostExperimentCalculations {
	
	protected Datacenter datacenter;
	protected List<Cloudlet> cloudlets;
	protected List<Vm> vms;
	protected List<Host> hosts;
	
	public PostExperimentCalculations(Datacenter datacenter, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		this.datacenter = datacenter;
		this.cloudlets = cloudlets;
		this.vms = vms;
		this.hosts = hosts;
	}
	
	public void showStatistics() {
		SvilaPrinter.print("Makespan: " + getMakespan());
	}
	
	public double getMakespan() {
		double makespan = 0;
		for(Cloudlet c : cloudlets) {
			if(c.getFinishTime() > makespan) {
				makespan = c.getFinishTime();
			}
		}
		
		return makespan;
	}
}
