package svila.staticAllocation;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.json.JSONArray;

public class ExperimentGenerator {
	
	public static void main(String[] args) {  	
		StaticExperimentation.init();
    	
		String hostInputFile = Paths.get(StaticExperimentation.basePath.toString(), StaticExperimentation.ecObject.getString("machinesFolder"), "hosts.txt").toString();
    	String vmInputFile = Paths.get(StaticExperimentation.basePath.toString(), StaticExperimentation.ecObject.getString("machinesFolder"), "vms.txt").toString();
    	int iterations = StaticExperimentation.ecObject.getInt("iterations");
    	JSONArray folders = StaticExperimentation.ecObject.getJSONArray("workloadFolder");
    	StaticExperimentation.experimentConfigurationsPath = Paths.get(StaticExperimentation.getExperimentConfigurationFilename("tunningExperimentName"));
    	StaticExperimentation.setECObject();

    	while(true) {
    		try {
    			int i=0;
                Path workloadInputFolderPath = Paths.get(StaticExperimentation.basePath.toString(), "input", folders.getString(i));
            	String workloadInputFolder = workloadInputFolderPath.toString();
        		Path outputPath = Paths.get(StaticExperimentation.basePath.toString(), "toPlotTunning", Paths.get(workloadInputFolder).getFileName().toString());

            	StaticExperimentation.executeExperimentFolder(iterations, StaticExperimentation.basePath.toString(), hostInputFile, vmInputFile, outputPath.toString(), workloadInputFolder, StaticExperimentation.ecObject, false);
    		} catch(Exception e) {
    			
    		}
    		
    	}
	}

	
}
