package svila.staticAllocation.schedulingTechniques;

import java.util.List;
import java.util.Random;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.json.JSONObject;

import ut.CloudLetPSOScheduling;

public class PSOSA  extends StaticTaskSchedulingTechnique{
	CloudLetPSOScheduling pso;
	
	public PSOSA() {
		this.techniqueName = "PSOSA";
	}
	
	public PSOSA(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		pso = new CloudLetPSOScheduling(10, 10, 1.49445, 1.49445);
		this.techniqueName = "PSOSA";
	}
	
	public PSOSA(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts, JSONObject extraFields) {
		super(broker, cloudlets, vms, hosts, extraFields);
		pso = new CloudLetPSOScheduling(
				extraFields.getInt("particles"),
				extraFields.getInt("iterations"),
				extraFields.getDouble("localWeight"),
				extraFields.getDouble("globalWeight")
			); 
		this.techniqueName = "PSOSA";
	}
	
	@Override
	public void setEnvironment(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		this.broker = broker;
		this.cloudlets = cloudlets;
		this.vms = vms;
		this.hosts = hosts;
		
		pso = new CloudLetPSOScheduling(
				extraFields.getInt("particles"),
				extraFields.getInt("iterations"),
				extraFields.getDouble("localWeight"),
				extraFields.getDouble("globalWeight")
			);
	}

	@Override
	public void execute() {
		//will retrieve the solution by PSO as an array of integers
		//where every element in the array is the VM-ID
		//and array index is the cloudlet number in the list
		int[] newPostitionsOnVMs = pso.getScheduledCloudLets(cloudlets, vms);
		
		for(int i = 0 ; i < cloudlets.size(); i++){
			cloudlets.get(i).setVmId(newPostitionsOnVMs[i]);
		}
	}

	@Override
	public void startTechnique() {
		ei.launchStandaloneExperiment();
	}
}
