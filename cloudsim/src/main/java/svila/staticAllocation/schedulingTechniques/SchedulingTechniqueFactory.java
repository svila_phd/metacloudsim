package svila.staticAllocation.schedulingTechniques;

import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.json.JSONObject;

public class SchedulingTechniqueFactory {
	
	DatacenterBroker broker;
	List<Cloudlet> cloudlets;
	List<Vm> vms;
	List<Host> hosts;
	
	public SchedulingTechniqueFactory(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		this.broker = broker;
		this.cloudlets = cloudlets;
		this.vms = vms;
		this.hosts = hosts;
	}
	
	public StaticTaskSchedulingTechnique getSchedulingTechnique(JSONObject schedulingTechniqueConfiguration) {
		String techniqueName = schedulingTechniqueConfiguration.getString("name");
		
		switch(techniqueName) {
			case "random":
				return new RandomTechnique(broker, cloudlets, vms, hosts);
			case "round-robin":
				return new RoundRobin(broker, cloudlets, vms, hosts);
			case "psosa":
				PSOSA psosa = new PSOSA(broker, cloudlets, vms, hosts);
				psosa.setExtraFields(schedulingTechniqueConfiguration);
				return psosa;
			default:
				System.out.println("SchedulingTechniqueFactory: technique name " + techniqueName + " doesn't exist");
				System.exit(-1);
				return null;
		}
	}
}
