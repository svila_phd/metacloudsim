package svila.staticAllocation.schedulingTechniques;

import java.util.List;
import java.util.Random;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

public class RandomTechnique extends StaticTaskSchedulingTechnique {
	
	public RandomTechnique() {
		this.techniqueName = "Random";
	}
	
	public RandomTechnique(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		this.techniqueName = "Random";
	}

	@Override
	public void execute() {
		Random r = new Random();
		
		for(Cloudlet cloudlet : cloudlets) {
			broker.bindCloudletToVm(cloudlet.getCloudletId(), vms.get(r.nextInt(vms.size())).getId());
		}
	}

	@Override
	public void startTechnique() {
		ei.launchStandaloneExperiment();
	}
}
