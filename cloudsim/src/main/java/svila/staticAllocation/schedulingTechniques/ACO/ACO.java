package svila.staticAllocation.schedulingTechniques.ACO;

import java.util.ArrayList;
import java.util.List;

public class ACO {
	
	List<List<ACONodeVM>> tabuLists;
	int m;
	int currentIterationT;
	int tMax;
	
	public ACO(int m, int tMax) {
		this.m = m;
		this.tMax = tMax;
		this.tabuLists = createTabuLists(m);
	}
	
	private List<List<ACONodeVM>> createTabuLists(int numAnts) {
		List<List<ACONodeVM>> newTabuLists = new ArrayList<>();
		
		for(int i=0; i<numAnts; i++) {
			newTabuLists.add(new ArrayList<ACONodeVM>());
		}
		
		return newTabuLists;
	}
	
	private void clearTabuLists() {
		
	}
	
	public ACOSolution ACOalgorithm() {
		this.currentIterationT = 1;
		ACOSolution currentOptimalSolution = null;

		//Inicialitzar tots els edge
		
		for(this.currentIterationT = 1 ;
				this.currentIterationT < tMax;
				this.currentIterationT++) {
			
		}
		
		
		
		// Colocar cada formiga en una VM aleatoriament
		
		for(int k=0; k<m; k++) {
			// Ficar la VM inicial de la formiga k a la llista tabu k
			// Realitzar el tour de la formiga
		}
		
		for(int k=0; k<m; k++) {
			// Calcular la longitud del tour de la formiga k
			// Guardar millor solució si existeix
		}
		
		// Aplicar la feromona local per cada aresta (i,j)
		
		// Actualitzar la feromona global
		
		this.currentIterationT++;
		
		if(this.currentIterationT < tMax) {
			// Buidar totes les llistes tabu
		} else {
			return currentOptimalSolution;
		}
	}
}
