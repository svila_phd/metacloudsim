package svila.staticAllocation.schedulingTechniques.ACO;

public class ACOEdge {
	
	public ACONodeVM vmNode;
	public ACONodeTask taskNode;
	public ACOLocalPheromone localPheronome;
	
	public ACOEdge(ACONodeVM vmNode, ACONodeTask taskNode) {
		this.vmNode = vmNode;
		this.taskNode = taskNode;
	}
}
