package svila.staticAllocation.schedulingTechniques.ACO;

import org.cloudbus.cloudsim.Vm;

public class ACONodeVM extends ACONode {
	
	public Vm vm;
	
	public ACONodeVM(Vm vm) {
		this.vm = vm;
	}
}
