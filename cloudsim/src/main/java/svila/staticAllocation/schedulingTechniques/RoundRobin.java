package svila.staticAllocation.schedulingTechniques;

import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

public class RoundRobin extends StaticTaskSchedulingTechnique {
	
	public RoundRobin() {
		this.techniqueName = "Round Robin";
	}
	
	public RoundRobin(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		this.techniqueName = "Round Robin";
	}

	@Override
	public void execute() {
		int vmPosition = 0;
		for(Cloudlet cloudlet : cloudlets) {
			broker.bindCloudletToVm(cloudlet.getCloudletId(), vms.get(vmPosition).getId());
			
			vmPosition++;
			if(vmPosition==vms.size()) {
				vmPosition = 0;
			}
		}
	}

	@Override
	public void startTechnique() {
		ei.launchStandaloneExperiment();
	}

}
