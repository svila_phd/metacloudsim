package svila.staticAllocation.schedulingTechniques;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

import geneticsimulator.Cluster;
import geneticsimulator.Job;
import gridsim.MachineList;
import svila.GridCloudBridge.FromCloudSimToGridSim;
import svila.staticAllocation.CloudletsAssignation;
import svila.staticAllocation.SWFAnalyzer;
import svila.staticAllocation.SvilaHelper;

public class HillClimbingAdapted extends StaticTaskSchedulingTechnique {
	CloudletsAssignation bestAssignation;
	
	public HillClimbingAdapted() {
		this.techniqueName = "Hill Climbing";
	}
	
	public HillClimbingAdapted(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		this.techniqueName = "Hill Climbing";
	}

	@Override
	public void execute() {
		Iterator<Entry<Integer, Integer>> iterator = bestAssignation.cloudletVMMap.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry<Integer, Integer> entry = iterator.next();
			broker.bindCloudletToVm(entry.getKey(), entry.getValue());
		}
		
	}

	@Override
	public void startTechnique() {
		//ei.launchStandaloneExperiment();
		ArrayList<Vm> vmList = SvilaHelper.createVmList(0, ei.getVmInputFile());
    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(ei.getWorkloadInputFile(), true, 0);
    	ArrayList<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();
		
		ArrayList<Job> processingJobs = FromCloudSimToGridSim.getJobs(cloudletList);
		MachineList machinesList = FromCloudSimToGridSim.getMachineList(vmList);
		
		try {
			bestAssignation = GridSimManager.launchSimulator(GridSimManager.POLICY_HILL, null, machinesList, processingJobs);		
			ei.launchStandaloneExperiment();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
