package svila.staticAllocation.schedulingTechniques;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;

import svila.AsyncDatacenterBroker;
import svila.staticAllocation.CloudletsAssignation;
import svila.staticAllocation.Experiment;
import svila.staticAllocation.MakespanComparator;
import svila.staticAllocation.SWFAnalyzer;
import svila.staticAllocation.SimulationResult;
import svila.staticAllocation.SvilaHelper;

public class BasicAlgorithm extends StaticTaskSchedulingTechnique {
	CloudletsAssignation bestAssignation;
	CloudletsAssignation currentAssignation;
	
	public BasicAlgorithm() {
		this.techniqueName = "Basic";
	}
	
	public BasicAlgorithm(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		this.techniqueName = "Basic";
	}

	@Override
	public void execute() {
		Iterator<Entry<Integer, Integer>> iterator = currentAssignation.cloudletVMMap.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry<Integer, Integer> entry = iterator.next();
			broker.bindCloudletToVm(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public void startTechnique() {
		System.out.println("Basic executed");
		bestAssignation = null;
		
		SimulationResult bestResult = null;
		MakespanComparator makespanComparator = new MakespanComparator();
    	List<Vm> vmList = SvilaHelper.createVmList(0, ei.getVmInputFile());
    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(ei.getWorkloadInputFile(), true, 0);
    	List<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();
		
		for(int i=0; i<3; i++) {
			System.out.println("Iteration " + i);
			Random r = new Random();
			currentAssignation = new CloudletsAssignation();
			for(Cloudlet cloudlet : cloudletList) {
				int cloudletId = cloudlet.getCloudletId();
				Vm vm = vmList.get(r.nextInt(vmList.size()));
				currentAssignation.linkCloudletWithVM(cloudlet, vm);
			}
			ei.launchStandaloneExperiment();
			if(bestResult == null ||
					makespanComparator.compare(ei.getSimulationResult(), bestResult) > 0) {
				bestAssignation = currentAssignation;
				bestResult = ei.getSimulationResult();
				System.out.println("New best");
			} else {
				System.out.println("Maintain");
			}
		}		
	}
}
