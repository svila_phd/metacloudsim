package svila.staticAllocation.schedulingTechniques;

import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.json.JSONObject;

import svila.SvilaPrinter;
import svila.staticAllocation.Experiment;

public abstract class StaticTaskSchedulingTechnique {
	
	protected DatacenterBroker broker;
	protected List<Cloudlet> cloudlets;
	protected List<Vm> vms;
	protected List<Host> hosts;
	protected String techniqueName;
	protected String exportTechniqueName;
	protected double executionTime;
	protected JSONObject extraFields;
	protected Experiment ei;
	protected boolean tunning;
	
	public StaticTaskSchedulingTechnique() {
		this.tunning = false;
	}
	
	public StaticTaskSchedulingTechnique(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		this.broker = broker;
		this.cloudlets = cloudlets;
		this.vms = vms;
		this.hosts = hosts;
		this.tunning = false;
	}
	
	public StaticTaskSchedulingTechnique(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts, JSONObject extraFields) {
		this.broker = broker;
		this.cloudlets = cloudlets;
		this.vms = vms;
		this.hosts = hosts;
		this.extraFields = extraFields;
		this.tunning = false;
	}
	
	public void setEnvironment(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		this.broker = broker;
		this.cloudlets = cloudlets;
		this.vms = vms;
		this.hosts = hosts;
	}
	
	public void setTunning(boolean tunning) {
		this.tunning = tunning;
	}
	
	public abstract void startTechnique();
	
	public void launch() {
		long startTime = System.nanoTime();
		startTechnique();
		long stopTime = System.nanoTime();
	    long elapsedTime = stopTime - startTime;
	    executionTime = elapsedTime / 1000000000.0;
	    System.out.println(executionTime);
	    SvilaPrinter.print(String.valueOf(executionTime));
	}
	
	public abstract void execute();
	
	public String getTechniqueName() {
		return techniqueName;
	}
	
	public double getExecutionTime() {
		return executionTime;
	}
	
	public void setExtraFields(JSONObject extraFields) {
		this.extraFields = extraFields;
	}
	
	public JSONObject getExtraFields() {
		if(extraFields == null) {
			return new JSONObject();
		}
		return extraFields;
	}
	
	public void setExperimentInfo(Experiment ei) {
		this.ei = ei;
	}
	
	public Experiment getExperimentInfo() {
		return ei;
	}
	
	public void randomizeParameters() {

	}
}
