package svila.staticAllocation.schedulingTechniques;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

public class MaxMin extends StaticTaskSchedulingTechnique {
	public class Resource {
		Vm vm;
		long assignedLength;
		List<Cloudlet> assignedCloudlets;
		
		public Resource(Vm vm) {
			this.vm = vm;
			this.assignedLength = 0L;
			this.assignedCloudlets = new ArrayList<>();
		}
		
		public void addCloudlet(Cloudlet cloudlet) {
			assignedCloudlets.add(cloudlet);
			assignedLength += cloudlet.getCloudletLength();
		}
		
		public double getCompletionTimeForCloudlet(Cloudlet cloudlet) {
			return cloudlet.getCloudletLength() / vm.getMips();
		}
		
		public double getAccumulatedCompletionTimeForCloudlet(Cloudlet cloudlet) {
			return getTotalCompletionTime() + cloudlet.getCloudletLength() / vm.getMips();
		}
		
		public double getTotalCompletionTime() {
			return assignedLength / vm.getMips();
		}
		
		public List<Cloudlet> getCloudlets() {
			return cloudlets;
		}
		
		public Vm getVm() {
			return vm;
		}
	}
	
	public MaxMin() {
		this.techniqueName = "MaxMin";
	}
	
	public MaxMin(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		this.techniqueName = "MaxMin";
	}

	@Override
	public void execute() {		
		List<Cloudlet> pendingCloudlets = new LinkedList<Cloudlet>();
		pendingCloudlets.addAll(cloudlets);
		
		List<Resource> resources = new ArrayList<>();
		
		for(Vm vm : vms) {
			resources.add(new Resource(vm));
		}
		
		Collections.sort(pendingCloudlets, new Comparator<Cloudlet>() {
			public int compare(Cloudlet o1, Cloudlet o2) {
				if(o1.getCloudletLength() == o2.getCloudletLength())
		             return 0;
		         return o1.getCloudletLength() > o2.getCloudletLength() ? -1 : 1;
			}
		});
		
		Cloudlet largestCloudlet = pendingCloudlets.get(0);
		pendingCloudlets.remove(0);
		Resource bestResource = getResourceWithMaximumAccumulatedCompletionTime(resources, largestCloudlet);
		bestResource.addCloudlet(largestCloudlet);
		
		// Minimum completion time
		// Minimum execution time
		// ?????
		
		while(!pendingCloudlets.isEmpty()) {
			largestCloudlet = pendingCloudlets.get(0);
			pendingCloudlets.remove(0);
			bestResource = getResourceWithMinimumAccumulatedCompletionTime(resources, largestCloudlet);
			bestResource.addCloudlet(largestCloudlet);
		}
		
		for(Resource r : resources) {
			for(Cloudlet c : r.getCloudlets()) {
				broker.bindCloudletToVm(c.getCloudletId(), r.getVm().getId());
			}
		}
	}
	
	private Resource getResourceWithMinimumAccumulatedCompletionTime(List<Resource> resources, Cloudlet cloudlet) {
		Resource selectedResource = null;
		double bestCompletionTime = Double.MAX_VALUE;
		for(Resource r : resources) {
			double currentCompletionTime = r.getAccumulatedCompletionTimeForCloudlet(cloudlet);
			if(currentCompletionTime < bestCompletionTime) {
				selectedResource = r;
				bestCompletionTime = currentCompletionTime;
			}
		}
		
		return selectedResource;
	}
	
	private Resource getResourceWithMaximumAccumulatedCompletionTime(List<Resource> resources, Cloudlet cloudlet) {
		Resource selectedResource = null;
		double bestCompletionTime = 0;
		for(Resource r : resources) {
			double currentCompletionTime = r.getAccumulatedCompletionTimeForCloudlet(cloudlet);
			if(currentCompletionTime > bestCompletionTime) {
				selectedResource = r;
				bestCompletionTime = currentCompletionTime;
			}
		}
		
		return selectedResource;
	}

	@Override
	public void startTechnique() {
		ei.launchStandaloneExperiment();
	}
}
