package svila.staticAllocation.schedulingTechniques;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.SolutionListEvaluator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;
import org.uma.jmetal.util.fileoutput.SolutionSetOutput;
import org.uma.jmetal.util.fileoutput.impl.DefaultFileOutputContext;

import genetic.JobCrossover;
import genetic.JobData;
import genetic.JobSwapMutation;
import genetic.NSGALog;
import genetic.SchedulingProblemMultiObjective;
import genetic.SchedulingSolution;
import geneticsimulator.BadAllocationException;
import geneticsimulator.Cluster;
import geneticsimulator.Job;
import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;
import gridsim.Machine;
import gridsim.ResGridlet;
import simulator.EnergyMachine;
import simulator.ResJob;
import svila.GridCloudBridge.FromCloudSimToGridSim;
import svila.staticAllocation.CloudletsAssignation;
import svila.staticAllocation.MakespanComparator;
import svila.staticAllocation.SWFAnalyzer;
import svila.staticAllocation.SimulationResult;
import svila.staticAllocation.SvilaHelper;

public class MOGAAdapted extends StaticTaskSchedulingTechnique {
	CloudletsAssignation bestAssignation;
	
	private int generations=40;
	private int population=70;
	private int probability_cx=80;
	private int probability_mu=30;
	private String evaluationMetric="m";
	private String output ="./";
	private String idProva="0";
	
	public MOGAAdapted() {
		this.techniqueName = "GAVM";
	}
	
	public MOGAAdapted(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		this.techniqueName = "GAVM";
	}
	
	@Override
	public void startTechnique() {		
		SchedulingSolution solution = obtainSolution();
		
		SetCloudletsToVms(solution);
		
		ei.launchStandaloneExperiment();
		
		/*SimulationResult bestResult = null;
		MakespanComparator makespanComparator = new MakespanComparator();
    	List<Vm> vmList = SvilaHelper.createVmList(0, ei.getVmInputFile());
    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(ei.getWorkloadInputFile(), true, 0);
    	List<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();
		
		for(int i=0; i<3; i++) {
			System.out.println("Iteration " + i);
			Random r = new Random();
			currentAssignation = new CloudletsAssignation();
			for(Cloudlet cloudlet : cloudletList) {
				int cloudletId = cloudlet.getCloudletId();
				Vm vm = vmList.get(r.nextInt(vmList.size()));
				currentAssignation.linkCloudletWithVM(cloudlet, vm);
			}
			ei.launchStandaloneExperiment();
			if(bestResult == null ||
					makespanComparator.compare(ei.getSimulationResult(), bestResult) > 0) {
				bestAssignation = currentAssignation;
				bestResult = ei.getSimulationResult();
				System.out.println("New best");
			} else {
				System.out.println("Maintain");
			}
		}*/	
		
	}
	
	private void SetCloudletsToVms(SchedulingSolution solution) {
		SchedulingProblemMultiObjective problem = new SchedulingProblemMultiObjective(JobList.instance(),MultiCluster.instance());
		problem.evaluate(solution);
		HashMap<Integer, Integer> allocations = MultiCluster.instance().jobAllocations;
		
		bestAssignation = new CloudletsAssignation();
		for(Entry<Integer, Integer> entry : allocations.entrySet()) {
			bestAssignation.linkCloudletWithVM(entry.getKey(), entry.getValue());
			//System.out.println(entry.getKey() + ": " + entry.getValue());
		}
	}

	@Override
	public void execute() {
		Iterator<Entry<Integer, Integer>> iterator = bestAssignation.cloudletVMMap.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry<Integer, Integer> entry = iterator.next();
			broker.bindCloudletToVm(entry.getKey(), entry.getValue());
		}
		
	}
	
	private SchedulingSolution obtainSolution() {
		ArrayList<Vm> vmList = SvilaHelper.createVmList(0, ei.getVmInputFile());
    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(ei.getWorkloadInputFile(), true, 0);
    	ArrayList<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();
		
		ArrayList<Job> processingJobs = FromCloudSimToGridSim.getJobs(cloudletList);
		ArrayList<Cluster>multi = FromCloudSimToGridSim.getClusters(vmList);

		JobList.setInstance(processingJobs);
		try {
			MultiCluster.setInstance(multi, null);
		} catch (BadAllocationException e1) {
			e1.printStackTrace();
			System.exit(1);
		}    	
		
		//Execute the GA
		 SchedulingProblemMultiObjective problem;
		 Algorithm<List<SchedulingSolution>> algorithm;
		 CrossoverOperator<SchedulingSolution> crossover;
		 MutationOperator<SchedulingSolution> mutation;
		 SelectionOperator<List<SchedulingSolution>, SchedulingSolution> selection;
		 SolutionListEvaluator<SchedulingSolution> evaluator;
		 evaluator = new SequentialSolutionListEvaluator<SchedulingSolution>();	
		 problem = new SchedulingProblemMultiObjective(JobList.instance(),MultiCluster.instance());
		 crossover = new JobCrossover() ;
		 mutation = new JobSwapMutation();
		 selection = new BinaryTournamentSelection<SchedulingSolution>(new RankingAndCrowdingDistanceComparator<SchedulingSolution>());
		 algorithm = (Algorithm<List<SchedulingSolution>>) new NSGALog(problem,this.generations,this.population,crossover,mutation,selection, evaluator,this.output+"ConvergenceE"+this.evaluationMetric+idProva+".csv");
		 AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
		 .execute();
		 List<SchedulingSolution> population = algorithm.getResult() ;
		 long computingTime = algorithmRunner.getComputingTime() ;
		 ArrayList<SchedulingSolution> solutions = new ArrayList<SchedulingSolution>();
		 solutions.add(population.get(0));
 
		 return population.get(0);
		 
		 /*MultiCluster mc=MultiCluster.instance();
		 for(int i=result.getNumberOfVariables()-1;i>=0;i--){
			 JobData jd=result.getVariableValue(i);
			 for(ResJob rj : listToOrder){
				 if(jd.job.getId()==rj.getGridletID()){
					 int[] allocPreferences=new int[mc.size()];
					 for(int j=0;j<mc.size();j++){
						 allocPreferences[j]=(int) ((1-jd.forbidenNodes[j])*super.resource_.getMachineList().getMachineInPos(j).getNumPE());
					 }
					 rj.setAllocationPreferences(allocPreferences);
					 rj.preAllcoate();
					 this.gridletQueueList_.addFirst(rj);
					 break;
				 }
			  }
		 }	*/
	}

	/*private void orderWaitingQueue() {

			//We have jobs in the queue and we want to order and preAllocate them.
			ArrayList<ResJob> listToOrder=new ArrayList<ResJob>();
			ArrayList<Job> processingJobs=new ArrayList<Job>();
			ArrayList<Cluster>multi=new ArrayList<Cluster>();
			
			
			// LECTURA DELS JOBS
			// TODO
			
			
			while(!this.gridletQueueList_.isEmpty()&& listToOrder.size()<this.packageSize){
				//Random r=new Random();
				//ResJob rj=(ResJob)this.gridletQueueList_.remove(r.nextInt(gridletQueueList_.size()));
				ResJob rj= (ResJob)this.gridletQueueList_.remove();
				listToOrder.add(rj);
				processingJobs.add(new Job(rj.getGridletID(),rj.getNumPE(),rj.getGridletLength(),rj.getSigma(),rj.getPPBW()));
			}
			
			// LECTURA DE LES VMS
			// TODO
					
			for(Machine m :super.resource_.getMachineList()){
				EnergyMachine em=(EnergyMachine)m;
				multi.add(new Cluster(m.getMachineID(),m.getNumPE(),(m.getMIPSRating()/m.getNumPE()),em.getMaxBW(),em.getEnergyIdle(),em.getEnergyComputing(),em.getEnergyStart(),em.getEnergyStop()));
			}

			JobList.setInstance(processingJobs);
			try {
				MultiCluster.setInstance(multi, inExec);
			} catch (BadAllocationException e1) {
				e1.printStackTrace();
				System.exit(1);
			}    	
			
			//Execute the GA
			 SchedulingProblemMultiObjective problem;
			 Algorithm<List<SchedulingSolution>> algorithm;
			 CrossoverOperator<SchedulingSolution> crossover;
			 MutationOperator<SchedulingSolution> mutation;
			 SelectionOperator<List<SchedulingSolution>, SchedulingSolution> selection;
			 SolutionListEvaluator<SchedulingSolution> evaluator;
			 evaluator = new SequentialSolutionListEvaluator<SchedulingSolution>();	
			 problem = new SchedulingProblemMultiObjective(JobList.instance(),MultiCluster.instance());
			 crossover = new JobCrossover() ;
			 mutation = new JobSwapMutation();
			 selection = new BinaryTournamentSelection<SchedulingSolution>(new RankingAndCrowdingDistanceComparator<SchedulingSolution>());
			 algorithm = (Algorithm<List<SchedulingSolution>>) new NSGALog(problem,this.generations,this.population,crossover,mutation,selection, evaluator,this.output+"ConvergenceE"+this.evaluationMetric+idProva+".csv");
			 AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
			 .execute();
			 List<SchedulingSolution> population = algorithm.getResult() ;
			 long computingTime = algorithmRunner.getComputingTime() ;
			 ArrayList<SchedulingSolution> solutions = new ArrayList<SchedulingSolution>();
			 solutions.add(population.get(0));
 
			 new SolutionSetOutput.Printer(solutions)
			 .setSeparator("\t")
			 .setVarFileOutputContext(new DefaultFileOutputContext(this.output+"Var"+this.evaluationMetric+idProva+".csv"))
			 .setFunFileOutputContext(new DefaultFileOutputContext(this.output+"Fun"+this.evaluationMetric+idProva+".csv"))
			 .print();
			 SchedulingSolution result=null;
			 double mk=Double.MAX_VALUE;
			 for(SchedulingSolution p :solutions){
				 int objective;
				 if(this.evaluationMetric=="m"){
					 objective=0;
				 }else{
					 objective=1;
				 }
				 if(p.getObjective(objective)<mk){
					 result=p;
					 mk=p.getObjective(objective);
				 }
			 }

			 JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");
			 JMetalLogger.logger.info("Objectives values have been written to file FUN.tsv");
			 JMetalLogger.logger.info("Variables values have been written to file VAR.tsv");
	}*/
}
