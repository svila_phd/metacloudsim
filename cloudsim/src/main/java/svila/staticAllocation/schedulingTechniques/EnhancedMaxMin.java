package svila.staticAllocation.schedulingTechniques;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

import svila.staticAllocation.schedulingTechniques.MaxMin.Resource;

public class EnhancedMaxMin extends StaticTaskSchedulingTechnique {
	public class Resource {
		Vm vm;
		long assignedLength;
		List<Cloudlet> assignedCloudlets;
		
		public Resource(Vm vm) {
			this.vm = vm;
			this.assignedLength = 0L;
			this.assignedCloudlets = new ArrayList<>();
		}
		
		public void addCloudlet(Cloudlet cloudlet) {
			assignedCloudlets.add(cloudlet);
			assignedLength += cloudlet.getCloudletLength();
		}
		
		public double getCompletionTimeForCloudlet(Cloudlet cloudlet) {
			return cloudlet.getCloudletLength() / vm.getMips();
		}
		
		public double getAccumulatedCompletionTimeForCloudlet(Cloudlet cloudlet) {
			return getTotalCompletionTime() + cloudlet.getCloudletLength() / vm.getMips();
		}
		
		public double getTotalCompletionTime() {
			return assignedLength / vm.getMips();
		}
		
		public List<Cloudlet> getCloudlets() {
			return cloudlets;
		}
		
		public Vm getVm() {
			return vm;
		}
	}
	
	public EnhancedMaxMin() {
		this.techniqueName = "EnhancedMaxMin";
	}
	
	public EnhancedMaxMin(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		this.techniqueName = "EnhancedMaxMin";
	}

	@Override
	public void execute() {		
		List<Cloudlet> pendingCloudlets = new LinkedList<Cloudlet>();
		pendingCloudlets.addAll(cloudlets);
		
		List<Resource> resources = new ArrayList<>();
		
		for(Vm vm : vms) {
			resources.add(new Resource(vm));
		}
		
		Collections.sort(pendingCloudlets, new Comparator<Cloudlet>() {
			public int compare(Cloudlet o1, Cloudlet o2) {
				if(o1.getCloudletLength() == o2.getCloudletLength())
		             return 0;
		         return o1.getCloudletLength() > o2.getCloudletLength() ? -1 : 1;
			}
		});
		
		Cloudlet nearAverageCloudlet = getNearAverageCloudlet(pendingCloudlets);
		pendingCloudlets.remove(nearAverageCloudlet);
		Resource bestResource = getResourceWithMaximumAccumulatedCompletionTime(resources, nearAverageCloudlet);
		bestResource.addCloudlet(nearAverageCloudlet);
		
		// Minimum completion time
		// Minimum execution time
		// ?????
		
		Cloudlet largestCloudlet;
		
		while(!pendingCloudlets.isEmpty()) {
			largestCloudlet = pendingCloudlets.get(0);
			pendingCloudlets.remove(0);
			bestResource = getResourceWithMinimumAccumulatedCompletionTime(resources, largestCloudlet);
			bestResource.addCloudlet(largestCloudlet);
		}
		
		for(Resource r : resources) {
			for(Cloudlet c : r.getCloudlets()) {
				broker.bindCloudletToVm(c.getCloudletId(), r.getVm().getId());
			}
		}
	}
	
	private Cloudlet getNearAverageCloudlet(List<Cloudlet> cloudlets) {
		Cloudlet averageCloudlet = null;
		double averageLength = getAverage(cloudlets);
		
		for(Cloudlet c : cloudlets) {
			if(c.getCloudletLength() < averageLength) {
				return averageCloudlet;
			}
			averageCloudlet = c;
		}
		
		return averageCloudlet;
	}
	
	private Resource getResourceWithMinimumAccumulatedCompletionTime(List<Resource> resources, Cloudlet cloudlet) {
		Resource selectedResource = null;
		double bestCompletionTime = Double.MAX_VALUE;
		for(Resource r : resources) {
			double currentCompletionTime = r.getAccumulatedCompletionTimeForCloudlet(cloudlet);
			if(currentCompletionTime < bestCompletionTime) {
				selectedResource = r;
				bestCompletionTime = currentCompletionTime;
			}
		}
		
		return selectedResource;
	}
	
	private Resource getResourceWithMaximumAccumulatedCompletionTime(List<Resource> resources, Cloudlet cloudlet) {
		Resource selectedResource = null;
		double bestCompletionTime = 0;
		for(Resource r : resources) {
			double currentCompletionTime = r.getAccumulatedCompletionTimeForCloudlet(cloudlet);
			if(currentCompletionTime > bestCompletionTime) {
				selectedResource = r;
				bestCompletionTime = currentCompletionTime;
			}
		}
		
		return selectedResource;
	}
	
	private double getAverage(List<Cloudlet> cloudlets) {
		long total = 0;
		for(Cloudlet c : cloudlets) {
			total += c.getCloudletLength();
		}
		
		return total / cloudlets.size();
	}

	@Override
	public void startTechnique() {
		ei.launchStandaloneExperiment();
	}
}
