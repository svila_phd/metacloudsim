package svila.staticAllocation.schedulingTechniques.extra;

import java.util.ArrayList;
import java.util.List;

import org.uma.jmetal54.operator.CrossoverOperator;
import org.uma.jmetal54.util.pseudorandom.JMetalRandom;

import jmetal54.JobData;
import jmetal54.SchedulingSolution;

public class CloudAllocCrossover implements CrossoverOperator<CloudSchedulingSolution> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4347370808154130430L;
	private JMetalRandom randomGenerator ;

	  /** Constructor */
	  public CloudAllocCrossover() {
	    randomGenerator = JMetalRandom.getInstance() ;
	  }

	public List<CloudSchedulingSolution> execute(List<CloudSchedulingSolution> source) {
		CloudSchedulingSolution parent1 = source.get(0);
		CloudSchedulingSolution parent2 = source.get(1);
		CloudSchedulingSolution child1 = new CloudSchedulingSolution(parent1);
		CloudSchedulingSolution child2 = new CloudSchedulingSolution(parent2);
		ArrayList<CloudSchedulingSolution> solutions = new ArrayList<CloudSchedulingSolution>();
		double cross=randomGenerator.nextDouble(-0.2, 1.2);
		
		for(int i=0;i<parent1.getNumberOfVariables();i++){
			CloudData parent1Data=parent1.getVariableValue(i);
			for(int j=0;j<parent2.getNumberOfVariables();j++){
				CloudData parent2Data=parent2.getVariableValue(j);
				if(parent1Data.equals(parent2Data)){
					CloudData child1Data=child1.getVariableValue(i);
					CloudData child2Data=child2.getVariableValue(j);
					for(int fn=0;fn<parent1Data.forbiddenCores.length;fn++){
						child1Data.forbiddenCores[fn]=cross*parent1Data.forbiddenCores[fn]+(1-cross)*parent2Data.forbiddenCores[fn];
						child2Data.forbiddenCores[fn]=cross*parent2Data.forbiddenCores[fn]+(1-cross)*parent1Data.forbiddenCores[fn];
					}
					child1.setVariableValue(i,child1Data);
					child2.setVariableValue(j,child2Data);
				}
			}
		}
	
		solutions.add(child1);
		solutions.add(child2);
		return solutions;
	}

	@Override
	public int getNumberOfRequiredParents() {
		return 2;
	}

	@Override
	public int getNumberOfGeneratedChildren() {
		return 2;
	}
}
