package svila.staticAllocation.schedulingTechniques.extra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.uma.jmetal54.algorithm.Algorithm;
import org.uma.jmetal54.algorithm.multiobjective.gwasfga.GWASFGA;
import org.uma.jmetal54.algorithm.multiobjective.mombi.MOMBI;
import org.uma.jmetal54.operator.CrossoverOperator;
import org.uma.jmetal54.operator.MutationOperator;
import org.uma.jmetal54.operator.SelectionOperator;
import org.uma.jmetal54.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal54.util.AlgorithmRunner;
import org.uma.jmetal54.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal54.util.evaluator.SolutionListEvaluator;
import org.uma.jmetal54.util.evaluator.impl.SequentialSolutionListEvaluator;

import jmetal54.JobCrossover;
import jmetal54.JobSwapMutation;
import jmetal54.SchedulingProblemMultiObjective;
import jmetal54.SchedulingSolution;
import geneticsimulator.BadAllocationException;
import geneticsimulator.Cluster;
import geneticsimulator.Job;
import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;
import svila.GridCloudBridge.FromCloudSimToGridSim;
import svila.staticAllocation.CloudletsAssignation;
import svila.staticAllocation.SWFAnalyzer;
import svila.staticAllocation.SvilaHelper;
import svila.staticAllocation.schedulingTechniques.StaticTaskSchedulingTechnique;

public class JMetalMombi extends StaticTaskSchedulingTechnique {
	CloudletsAssignation bestAssignation;
	
	public JMetalMombi() {
		this.techniqueName = "Mombi";
	}
	
	public JMetalMombi(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		this.techniqueName = "Mombi";
	}
	
	@Override
	public void startTechnique() {		
		SchedulingSolution solution = obtainSolution();
		
		SetCloudletsToVms(solution);
		
		ei.launchStandaloneExperiment();
		
	}
	
	private void SetCloudletsToVms(SchedulingSolution solution) {
		
		// MODIFICAR
		SchedulingProblemMultiObjective problem = new SchedulingProblemMultiObjective(JobList.instance(),MultiCluster.instance());
		problem.evaluate(solution);
		HashMap<Integer, Integer> allocations = MultiCluster.instance().jobAllocations;
		
		bestAssignation = new CloudletsAssignation();
		for(Entry<Integer, Integer> entry : allocations.entrySet()) {
			bestAssignation.linkCloudletWithVM(entry.getKey(), entry.getValue());
			//System.out.println(entry.getKey() + ": " + entry.getValue());
		}
	}

	@Override
	public void execute() {
		Iterator<Entry<Integer, Integer>> iterator = bestAssignation.cloudletVMMap.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry<Integer, Integer> entry = iterator.next();
			broker.bindCloudletToVm(entry.getKey(), entry.getValue());
		}
		
	}
	
	private SchedulingSolution obtainSolution() {

		ArrayList<Vm> vmList = SvilaHelper.createVmList(0, ei.getVmInputFile());
    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(ei.getWorkloadInputFile(), true, 0);
    	ArrayList<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();
		
		ArrayList<Job> processingJobs = FromCloudSimToGridSim.getJobs(cloudletList);
		ArrayList<Cluster>multi = FromCloudSimToGridSim.getClusters(vmList);

		JobList.setInstance(processingJobs);
		try {
			MultiCluster.setInstance(multi, null);
		} catch (BadAllocationException e1) {
			e1.printStackTrace();
			System.exit(1);
		}    	
		
		int maxIterations = 100;

		 Algorithm<List<SchedulingSolution>> algorithm = null;
		 SchedulingProblemMultiObjective problem;
		 problem = new SchedulingProblemMultiObjective(JobList.instance(),MultiCluster.instance());

		 
		 CrossoverOperator<SchedulingSolution> crossover;
		 MutationOperator<SchedulingSolution> mutation;
		 SelectionOperator<List<SchedulingSolution>, SchedulingSolution> selection;
		 SolutionListEvaluator<SchedulingSolution> evaluator;
		 evaluator = new SequentialSolutionListEvaluator<SchedulingSolution>();	
		 crossover = new JobCrossover() ;
		 mutation = new JobSwapMutation();
		 selection = new BinaryTournamentSelection<SchedulingSolution>(new RankingAndCrowdingDistanceComparator<SchedulingSolution>());;
		 

		 //algorithm = (Algorithm<List<SchedulingSolution>>) new NSGALog(problem,this.generations,this.population,crossover,mutation,selection, evaluator,this.output+"ConvergenceE"+this.evaluationMetric+idProva+".csv");
		 
		 algorithm  = new MOMBI<SchedulingSolution>(problem, maxIterations, crossover,
	    		 mutation, selection, evaluator,
				 "");
		 
		 AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
		 .execute();
		 List<SchedulingSolution> population = algorithm.getResult() ;
		 long computingTime = algorithmRunner.getComputingTime() ;
		 ArrayList<SchedulingSolution> solutions = new ArrayList<SchedulingSolution>();
		 solutions.add(population.get(0));
 
		 return population.get(0);
		 
	
	}
}
