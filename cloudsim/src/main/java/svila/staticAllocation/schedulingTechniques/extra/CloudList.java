package svila.staticAllocation.schedulingTechniques.extra;

import java.util.ArrayList;
import java.util.List;

import svila.CustomCloudSimulator.CloudLet;

public class CloudList {
	private static boolean seting=false;
	private static CloudList instance;
	private List<CloudLet> cloudList;
	
	
	/** Set an instance of all jobs to allocate
	 * 
	 * @param jobs the jobs to allocate
	 */
	public static void setInstance(List<CloudLet>cloudlets){
		instance=new CloudList(cloudlets);
		seting=true;
	}
	/** Obtain the list of all jobs
	 * 
	 * @return The current list of jobs
	 * @throws IllegalStateException
	 */
	public static CloudList instance() throws IllegalStateException{
		if(seting==false){
			throw new IllegalStateException();
		}
		return instance;
	}
	
	/** Create the list of jobs**/
	private CloudList(List<CloudLet> cloudlets){
		cloudList=new ArrayList<CloudLet>();
		cloudList.addAll(cloudlets);
	}
	/** Obtain the number of jobs in the queue
	 * 
	 * @return the number of jobs in the queue
	 */
	public int size(){
		return cloudList.size();
	}
	/** Get the job in the specified position
	 * 
	 * @param pos The position to obtain the job
	 * @return the Job at the specified position
	 */
	public CloudLet get(int pos){
		return cloudList.get(pos);
	}
	/** (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String r=new String("");
		for(CloudLet j:cloudList){
			r+=j+"|";
		}
		return r;
	}
	/** Resets all the jobs in the list
	 * 
	 */
	// Not necessary
	public void reset(){
		for(CloudLet j:cloudList){
			//j.reset();
		}
	}
}
