package svila.staticAllocation.schedulingTechniques.extra;

import java.util.ArrayList;
import java.util.List;

import org.uma.jmetal54.algorithm.multiobjective.gwasfga.GWASFGA;
import org.uma.jmetal54.operator.CrossoverOperator;
import org.uma.jmetal54.operator.MutationOperator;
import org.uma.jmetal54.operator.SelectionOperator;
import org.uma.jmetal54.problem.Problem;
import org.uma.jmetal54.solution.Solution;
import org.uma.jmetal54.util.evaluator.SolutionListEvaluator;

public class GWASFGALog<S extends Solution<?>> extends GWASFGA<S> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Double> solutions;
	private boolean showDebug;
	private boolean saveSolutions;
	
	public GWASFGALog(Problem<S> problem, int populationSize, int maxIterations, CrossoverOperator<S> crossoverOperator,
			MutationOperator<S> mutationOperator, SelectionOperator<List<S>, S> selectionOperator, SolutionListEvaluator<S> evaluator,
			List<Double> solutions) {
		super(problem, populationSize, maxIterations, crossoverOperator, mutationOperator, selectionOperator, evaluator);
		this.solutions = solutions;
		this.showDebug = false;
		this.saveSolutions = false;
	}
	
	public void setShowDebug(boolean showDebug) {
		this.showDebug = showDebug;
	}

	public void setSaveSolutions(boolean saveSolutions) {
		this.saveSolutions = saveSolutions;
	}
	
	@Override
	protected void updateProgress() {
		this.iterations+=1;
		
		if(showDebug) {
			printDebugResult();
		}
		
		if(saveSolutions) {
			saveSolution();
		}
	}
	
	private double getBigestM() {
		List<S> l=this.getResult();
		double bigestm=l.get(0).getObjective(1);
		for(S ss:l){
			if(bigestm>ss.getObjective(0)){
				bigestm=ss.getObjective(0);
			}
		}
		double bigeste=l.get(0).getObjective(1);
		for(S ss:l){
			if(bigeste>ss.getObjective(1)){
				bigeste=ss.getObjective(1);
			}
		}
		return bigestm;
	}
	
	private void printDebugResult() {
		System.out.println(getBigestM());
	}
	
	private void saveSolution() {
		solutions.add(getBigestM());
	}
	
	public List<Double> getSolutions() {
		return solutions;
	}
}
