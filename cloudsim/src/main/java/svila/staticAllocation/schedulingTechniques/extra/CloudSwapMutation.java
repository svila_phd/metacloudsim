package svila.staticAllocation.schedulingTechniques.extra;

import org.uma.jmetal54.util.pseudorandom.JMetalRandom;

public class CloudSwapMutation implements org.uma.jmetal54.operator.MutationOperator<CloudSchedulingSolution> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 532840358177210562L;
	double probability;
	int numberOfMutations;
	
	/**
	 * 
	 */

	private JMetalRandom randomGenerator ;

	  /** Constructor */
	public CloudSwapMutation() {
		randomGenerator = JMetalRandom.getInstance();
	    this.probability = 5;
	    this.numberOfMutations = 1;
	}
	
	public CloudSwapMutation(double probability, int numberOfMutations) {
	    randomGenerator = JMetalRandom.getInstance();
	    this.probability = probability;
	    this.numberOfMutations = numberOfMutations;
	}
	
	public CloudSchedulingSolution execute(CloudSchedulingSolution source) {
		int prob1 = randomGenerator.nextInt(0, 100);
		
		CloudSchedulingSolution solution = new CloudSchedulingSolution(source);
		
		for(int mutation=0; mutation<this.numberOfMutations; mutation++) {
			if(prob1<probability){
				solution = new CloudSchedulingSolution(source);
				int ptype = randomGenerator.nextInt(0, 100);
				if(ptype<50){
					int s1=randomGenerator.nextInt(0, source.getNumberOfVariables()-1);
					int s2=randomGenerator.nextInt(0, source.getNumberOfVariables()-1);
					CloudData temp = solution.getVariableValue(s1);
					solution.setVariableValue(s1, source.getVariableValue(s2));
					solution.setVariableValue(s2, temp);
					solution.repairSolution();
				}else{
					for(int i=0;i<source.getNumberOfVariables();i++){
						int pmut=randomGenerator.nextInt(0, 100);
						if(pmut>50){
							CloudData job=solution.getVariableValue(i);
							for(int j=0;j<job.forbiddenCores.length;j++){
								job.forbiddenCores[j]=randomGenerator.nextDouble(0, 1);
							}	
							solution.setVariableValue(i, job);
						}
					}
					solution.repairSolution();
				}
			}
		}
		
		return solution;
	}
}
