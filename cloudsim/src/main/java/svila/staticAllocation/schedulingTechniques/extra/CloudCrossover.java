package svila.staticAllocation.schedulingTechniques.extra;

import java.util.List;

import org.uma.jmetal54.operator.CrossoverOperator;
import org.uma.jmetal54.util.pseudorandom.JMetalRandom;

public class CloudCrossover implements org.uma.jmetal54.operator.CrossoverOperator<CloudSchedulingSolution> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1097737654968429287L;
	private JMetalRandom randomGenerator ;
	double probability;

	  /** Constructor */
	  public CloudCrossover() {
	    randomGenerator = JMetalRandom.getInstance() ;
	    this.probability = 50;
	  }
	  
	  public CloudCrossover(double probability) {
		    randomGenerator = JMetalRandom.getInstance() ;
		    this.probability = probability;
		  }

	public List<CloudSchedulingSolution> execute(List<CloudSchedulingSolution> source) {
		
		int prov = randomGenerator.nextInt(0, 100);
		CrossoverOperator<CloudSchedulingSolution>crossover;
		if(prov<probability){
			crossover=new CloudAllocCrossover();
		}else{
			crossover = new CloudOrderCrossover();
		}
		List<CloudSchedulingSolution> solutions=crossover.execute(source);
		for(CloudSchedulingSolution s : solutions){
			s.repairSolution();
		}
		return solutions;
		
	}

	@Override
	public int getNumberOfRequiredParents() {
		return 2;
	}

	@Override
	public int getNumberOfGeneratedChildren() {
		return 2;
	}
}
