package svila.staticAllocation.schedulingTechniques.extra;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.uma.jmetal54.solution.PermutationSolution;
import org.uma.jmetal54.solution.Solution;
import org.uma.jmetal54.solution.impl.AbstractGenericSolution;

import geneticsimulator.Job;
import jmetal54.JobData;
import jmetal54.SchedulingProblem;
import jmetal54.SchedulingSolution;
import svila.CustomCloudSimulator.CloudLet;
import svila.CustomCloudSimulator.CloudManager;
import svila.CustomCloudSimulator.CloudVM;

public class CloudSchedulingSolution extends AbstractGenericSolution<CloudData,SchedulingProblemMultiObjectiveCloudNative> implements PermutationSolution<CloudData> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1566646965472012015L;
	Random r = new Random();

	public CloudSchedulingSolution(SchedulingProblemMultiObjectiveCloudNative problem) {
		super(problem);

	    //overallConstraintViolationDegree = 0.0 ;
	    //numberOfViolatedConstraints = 0 ;
	    List<CloudData> randomSequence = new ArrayList<CloudData>(problem.getPermutationLength());

	    for (int j = 0; j < problem.getPermutationLength(); j++) {
	    	randomSequence.add(initializeCloudData(problem.getCloudList().get(j)));
	    }

	    java.util.Collections.shuffle(randomSequence);

	    for (int i = 0; i < getNumberOfVariables(); i++) {
	      setVariableValue(i, randomSequence.get(i)) ;
	    }
	    repairSolution();
	}
	
	public CloudSchedulingSolution(SchedulingProblemMultiObjectiveCloudNative problem,boolean diff) {
		super(problem);

	    //overallConstraintViolationDegree = 0.0 ;
	    //numberOfViolatedConstraints = 0 ;
	    List<CloudData> randomSequence = new ArrayList<CloudData>(problem.getPermutationLength());

	    for (int j = 0; j < problem.getPermutationLength(); j++) {
	    	randomSequence.add(initializeCloudData(problem.getCloudList().get(j)));
	    }

	    for (int i = 0; i < getNumberOfVariables(); i++) {
	      setVariableValue(i, randomSequence.get(i)) ;
	    }
	    repairSolution();
	}
	
	public CloudSchedulingSolution(CloudSchedulingSolution sol){
		super(sol.problem);
		//overallConstraintViolationDegree = 0.0 ;
	    //numberOfViolatedConstraints = 0 ;
	    for (int i = 0; i < getNumberOfVariables(); i++) {
	    	CloudData prev=sol.getVariableValue(i);
	    	setVariableValue(i, new CloudData(prev)) ;
		}
	    repairSolution();
	}

	public String getVariableValueString(int index) {
		CloudData variable=this.getVariableValue(index);
		String solution="Cloudlet "+variable.cloudlet.getId()+" {";
		for(int i=0;i<variable.forbiddenCores.length;i++){
			solution+=variable.forbiddenCores[i]+",";
		}
		return solution+"}";
	}

	public Solution<CloudData> copy() {
		return new CloudSchedulingSolution(this);
	}
	
	public CloudData initializeCloudData(CloudLet cloudlet){
		CloudData n=new CloudData(cloudlet,problem.getVMListLength());
		for(int i=0;i<n.forbiddenCores.length;i++){
			n.forbiddenCores[i]=this.randomGenerator.nextDouble(0d, 1d);
		}
		return n;
	}
	
	public void checkRepair() {
		List<CloudVM> vms = CloudManager.getInstance().getVMs();
		boolean valid = true;
		for(int i=0; i<this.getNumberOfVariables(); i++) {
			CloudData cd = this.getVariableValue(i);
			for(int j=0; j<vms.size(); j++) {
				if(!vms.get(j).canCloudletFitVMWithRestrictions(cd.cloudlet, cd.forbiddenCores[j])) {
					valid = false;
					break;
				}
			}
		}
		System.out.println("Repair is correct: " + valid);
	}
	
	public void repairSolution(){
		// Revisar que almenys hi ha una VM on encabir cada cloudlet
		// Si no n'hi ha cap, alliberar un 10% i tornar a intentar
		
		List<CloudVM> vms = CloudManager.getInstance().getVMs();
		int numValid = 0;
		for(int i=0; i<this.getNumberOfVariables(); i++) {
			CloudData cd = this.getVariableValue(i);
			boolean valid = false;
			while(!valid) {
				for(int c=0;c<cd.forbiddenCores.length;c++){
					if(cd.forbiddenCores[c]>1){cd.forbiddenCores[c]=1;}
					if(cd.forbiddenCores[c]<0){cd.forbiddenCores[c]=0;}
				}
				
				for(int j=0; j<vms.size(); j++) {
					if(vms.get(j).canCloudletFitVMWithRestrictions(cd.cloudlet, cd.forbiddenCores[j])) {
						//System.out.println("j: " + j + " Cloudlet " + cd.cloudlet.getId() + " cores: " + cd.cloudlet.getRequiredCores() + " VM: " + vms.get(j).getId() + " cores: "+ vms.get(j).getRequiredCores() + " forbidden: " + cd.forbiddenCores[j]);
						valid = true;
						numValid++;
						break;
					}
				}
			
				if(!valid) {
					for(int k=0; k<cd.forbiddenCores.length; k++) {
						if(cd.forbiddenCores[k] >= 0.0) {
							cd.forbiddenCores[k]-=0.1;
						}
					}
				}
			}
		}
		//System.out.println(numValid);
		//checkRepair();
		//for(int u=0; u<vms.size(); u++) {
		//	System.out.println(u + " " + vms.get(u).getId());
		//}
		/*for(int i=0;i<this.getNumberOfVariables();i++){
			JobData j=this.getVariableValue(i);
			boolean valid=false;
			while(!valid){
				int availableNodes=0;
				for(int c=0;c<j.forbidenVMs.length;c++){
					if(j.forbidenNodes[c]>1){j.forbidenNodes[c]=1;}
					if(j.forbidenNodes[c]<0){j.forbidenNodes[c]=0;}
				}
				for(int c=0;c<j.forbidenNodes.length;c++){
					int numNodesc=this.problem.mc.getClusterInPos(c).getMaxResources();
					availableNodes+=(int) (numNodesc*(1-j.forbidenNodes[c]));
				}
				if(availableNodes>=j.job.getRequirements()){
					valid=true;
				}else{
					ArrayList<Integer>cid = new ArrayList<Integer>();
					for(int c=0;c<j.forbidenNodes.length;c++){
						if(j.forbidenNodes[c]>0){
							cid.add(c);
						}
					}
					int ctemp=0;
					if(cid.size()>0){
						ctemp=r.nextInt(cid.size());
					}
					j.forbidenNodes[cid.get(ctemp)]-=0.1;
				}
			}
		}*/
	}
}
