package svila.staticAllocation.schedulingTechniques.extra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.multiobjective.randomsearch.RandomSearch;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.SolutionListEvaluator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;

import genetic.JobCrossover;
import genetic.JobSwapMutation;
import genetic.SchedulingProblemMultiObjective;
import genetic.SchedulingSolution;
import geneticsimulator.BadAllocationException;
import geneticsimulator.Cluster;
import geneticsimulator.Job;
import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;
import svila.GridCloudBridge.FromCloudSimToGridSim;
import svila.staticAllocation.CloudletsAssignation;
import svila.staticAllocation.SWFAnalyzer;
import svila.staticAllocation.SvilaHelper;
import svila.staticAllocation.schedulingTechniques.StaticTaskSchedulingTechnique;

public class JMetalDifferentialEvolution extends StaticTaskSchedulingTechnique {
	CloudletsAssignation bestAssignation;
	
	public JMetalDifferentialEvolution() {
		this.techniqueName = "jMetalDE";
	}
	
	public JMetalDifferentialEvolution(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		this.techniqueName = "jMetalDE";
	}
	
	@Override
	public void startTechnique() {		
		SchedulingSolution solution = obtainSolution();
		
		SetCloudletsToVms(solution);
		
		ei.launchStandaloneExperiment();
		
	}
	
	private void SetCloudletsToVms(SchedulingSolution solution) {
		
		// MODIFICAR
		SchedulingProblemMultiObjective problem = new SchedulingProblemMultiObjective(JobList.instance(),MultiCluster.instance());
		problem.evaluate(solution);
		HashMap<Integer, Integer> allocations = MultiCluster.instance().jobAllocations;
		
		bestAssignation = new CloudletsAssignation();
		for(Entry<Integer, Integer> entry : allocations.entrySet()) {
			bestAssignation.linkCloudletWithVM(entry.getKey(), entry.getValue());
			//System.out.println(entry.getKey() + ": " + entry.getValue());
		}
	}

	@Override
	public void execute() {
		Iterator<Entry<Integer, Integer>> iterator = bestAssignation.cloudletVMMap.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry<Integer, Integer> entry = iterator.next();
			broker.bindCloudletToVm(entry.getKey(), entry.getValue());
		}
		
	}
	
	private SchedulingSolution obtainSolution() {
		/*bestAssignation = null;
		
		MakespanComparator makespanComparator = new MakespanComparator();
    	List<Vm> vmList = SvilaHelper.createVmList(0, ei.getVmInputFile());
    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(ei.getWorkloadInputFile(), true, 0);
    	List<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();*/
		
		// MODIFICAR
		ArrayList<Vm> vmList = SvilaHelper.createVmList(0, ei.getVmInputFile());
    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(ei.getWorkloadInputFile(), true, 0);
    	ArrayList<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();
		
		ArrayList<Job> processingJobs = FromCloudSimToGridSim.getJobs(cloudletList);
		ArrayList<Cluster>multi = FromCloudSimToGridSim.getClusters(vmList);

		JobList.setInstance(processingJobs);
		try {
			MultiCluster.setInstance(multi, null);
		} catch (BadAllocationException e1) {
			e1.printStackTrace();
			System.exit(1);
		}    	
		
		
		 Algorithm<List<SchedulingSolution>> algorithm;
		 SchedulingProblemMultiObjective problem;
		 problem = new SchedulingProblemMultiObjective(JobList.instance(),MultiCluster.instance());

		 
		 CrossoverOperator<SchedulingSolution> crossover;
		 MutationOperator<SchedulingSolution> mutation;
		 SelectionOperator<List<SchedulingSolution>, SchedulingSolution> selection;
		 SolutionListEvaluator<SchedulingSolution> evaluator;
		 evaluator = new SequentialSolutionListEvaluator<SchedulingSolution>();	
		 crossover = new JobCrossover() ;
		 mutation = new JobSwapMutation();
		 selection = new BinaryTournamentSelection<SchedulingSolution>(new RankingAndCrowdingDistanceComparator<SchedulingSolution>());;

		 int numIterations = 100;

		 algorithm = new RandomSearch<SchedulingSolution>(problem, numIterations);
		 //algorithm = (Algorithm<List<SchedulingSolution>>) new NSGALog(problem,this.generations,this.population,crossover,mutation,selection, evaluator,this.output+"ConvergenceE"+this.evaluationMetric+idProva+".csv");
		 AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
		 .execute();
		 List<SchedulingSolution> population = algorithm.getResult() ;
		 long computingTime = algorithmRunner.getComputingTime() ;
		 ArrayList<SchedulingSolution> solutions = new ArrayList<SchedulingSolution>();
		 solutions.add(population.get(0));
 
		 return population.get(0);
		 
	
	}
}
