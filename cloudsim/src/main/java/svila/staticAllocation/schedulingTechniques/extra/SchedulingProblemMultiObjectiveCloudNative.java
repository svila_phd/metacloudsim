package svila.staticAllocation.schedulingTechniques.extra;

import java.util.List;

import org.uma.jmetal54.problem.impl.AbstractGenericProblem;

import svila.CustomCloudSimulator.CloudManager;
import svila.CustomCloudSimulator.CloudVM;
import svila.CustomCloudSimulator.TimeSharedResults;
import svila.CustomCloudSimulator.events.CalculateTimeSharedResults;
import svila.CustomCloudSimulator.events.GetBestBlacklistVM;
import svila.CustomCloudSimulator.events.SetCloudletToVM;

public class SchedulingProblemMultiObjectiveCloudNative extends AbstractGenericProblem<CloudSchedulingSolution> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8770322370894034355L;
	protected CloudList cloudList;
	protected List<CloudVM> vms;
	
	public SchedulingProblemMultiObjectiveCloudNative(CloudList cloudList, List<CloudVM> vms){
		this.setName("cloudSchedulingProblem");
		this.setNumberOfObjectives(2);
		this.setNumberOfVariables(cloudList.size());
		this.cloudList = cloudList;
		this.vms = vms;
	}
	
	public CloudSchedulingSolution createSolution() {
		return new CloudSchedulingSolution(this);
	}
	public CloudSchedulingSolution createSolutionOrdered(){
		return new CloudSchedulingSolution(this,true);
	}

	public int getPermutationLength() {
		return cloudList.size();
	}
	
	public int getVMListLength() {
		return vms.size();
	}
	
	public CloudList getCloudList(){
		return cloudList;
	}

	@Override
	public void evaluate(CloudSchedulingSolution solution) {
		// TODO Auto-generated method stub
		/*try{
		for(int i=0;i<getNumberOfVariables();i++){
			JobData j = solution.getVariableValue(i);
			Integer[] alloc=mc.getMESDAllocForbiden(j.job, j.forbidenNodes);
			while(!mc.sheduleWithAllocation(j.job,alloc)){
				mc.execute();
				alloc=mc.getMESDAllocForbiden(j.job, j.forbidenNodes);
			}
		}
		solution.setObjective(0,mc.finishExecution());
		solution.setObjective(1,mc.getEnergyConsumition());
	}catch(Exception e){
		e.printStackTrace();
		System.exit(0);
	}*/
		CloudManager cm = CloudManager.getInstance();
		try {
			for(int i=0; i<getNumberOfVariables(); i++) {
				CloudData cd = solution.getVariableValue(i);
				//System.out.println(cd.toString());
				GetBestBlacklistVM bestBlacklistEvent = new GetBestBlacklistVM(cd);
				bestBlacklistEvent.sendAndExecute();
				//System.out.println(cd.cloudlet.getId() + " " + bestBlacklistEvent.bestVM.getId() + " " + bestBlacklistEvent.bestVM.getTimeSharedMakespan());
				new SetCloudletToVM(cd.cloudlet, bestBlacklistEvent.bestVM).sendAndExecute();
			}
			List<CloudVM> cloudVMs = CloudManager.getInstance().getVMs();
			
			/*for(CloudVM v : cloudVMs) {
				System.out.println(v.getCloudlets().size());
			}*/
			
			CalculateTimeSharedResults ctsr = new CalculateTimeSharedResults();
			//ctsr.sendAndExecute();
			TimeSharedResults tsr = ctsr.getResults();
			tsr.setExport(false);
			tsr.generateResults();
			
			// ARREGLAR
			if(tsr.makespan == 0.0) {
				solution.setObjective(0, Double.MAX_VALUE);
				solution.setObjective(1, Double.MAX_VALUE);
			} else {
				//System.out.println("Makespan: " + tsr.makespan);
				//System.out.println("Energy: " + tsr.energy);
				solution.setObjective(0, tsr.makespan);
				solution.setObjective(1, tsr.energy);
			}
			/*System.out.println("Makespan: " + tsr.makespan);
			System.out.println("Energy: " + tsr.energy);
			solution.setObjective(0, tsr.makespan);
			solution.setObjective(1, tsr.energy);
			cm.clearCloudlets();*/
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

}
