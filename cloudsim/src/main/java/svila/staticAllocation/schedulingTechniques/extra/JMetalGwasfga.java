package svila.staticAllocation.schedulingTechniques.extra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.UUID;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.json.JSONArray;
import org.json.JSONObject;
import org.uma.jmetal54.algorithm.Algorithm;
import org.uma.jmetal54.algorithm.multiobjective.gwasfga.GWASFGA;
import org.uma.jmetal54.operator.CrossoverOperator;
import org.uma.jmetal54.operator.MutationOperator;
import org.uma.jmetal54.operator.SelectionOperator;
import org.uma.jmetal54.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal54.util.AlgorithmRunner;
import org.uma.jmetal54.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal54.util.evaluator.impl.SequentialSolutionListEvaluator;

import jmetal54.JobCrossover;
import jmetal54.JobSwapMutation;
import jmetal54.SchedulingProblemMultiObjective;
import jmetal54.SchedulingSolution;
import geneticsimulator.BadAllocationException;
import geneticsimulator.Cluster;
import geneticsimulator.Job;
import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;
import svila.CustomCloudSimulator.CloudLet;
import svila.CustomCloudSimulator.CloudManager;
import svila.CustomCloudSimulator.CloudManagerHelper;
import svila.CustomCloudSimulator.CloudVM;
import svila.GridCloudBridge.FromCloudSimToGridSim;
import svila.staticAllocation.CloudletsAssignation;
import svila.staticAllocation.SWFAnalyzer;
import svila.staticAllocation.StaticExperimentation;
import svila.staticAllocation.SvilaHelper;
import svila.staticAllocation.schedulingTechniques.StaticTaskSchedulingTechnique;

public class JMetalGwasfga extends StaticTaskSchedulingTechnique {
	CloudletsAssignation bestAssignation;

	double crossoverProbability;
    double mutationProbability;
    double annealingReduction;
    int numberOfMutations;
    int populationSize;
    int maxIterations;

	public JMetalGwasfga() {
		this.techniqueName = "GWASFGA";
		this.exportTechniqueName = "GWASFGA";
	}
	
	public JMetalGwasfga(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		this.techniqueName = "GWASFGA";
	}
	
	@Override
	public void startTechnique() {		
		SchedulingSolution solution = obtainSolution();
		
		setCloudletsToVms(solution);
		//setCloudletsToVmsNative(solution);
		
		ei.launchStandaloneExperiment();
		
	}
	
	private void setCloudletsToVms(SchedulingSolution solution) {
		
		SchedulingProblemMultiObjective problem = new SchedulingProblemMultiObjective(JobList.instance(),MultiCluster.instance());
		problem.evaluate(solution);
		HashMap<Integer, Integer> allocations = MultiCluster.instance().jobAllocations;
		
		bestAssignation = new CloudletsAssignation();
		for(Entry<Integer, Integer> entry : allocations.entrySet()) {
			bestAssignation.linkCloudletWithVM(entry.getKey(), entry.getValue());
			//System.out.println(entry.getKey() + ": " + entry.getValue());
		}
	}
	
	private void setCloudletsToVmsNative(SchedulingSolution solution) {
		SchedulingProblemMultiObjective problem = new SchedulingProblemMultiObjective(JobList.instance(), MultiCluster.instance());
		problem.evaluate(solution);		
		bestAssignation = new CloudletsAssignation();
		
		for(CloudVM cvm : CloudManager.getInstance().getVMs()) {
			for(CloudLet cl : cvm.getCloudlets()) {
				bestAssignation.linkCloudletWithVM(cl.getId(), cvm.getId());
			}
		}
	}

	@Override
	public void execute() {
		Iterator<Entry<Integer, Integer>> iterator = bestAssignation.cloudletVMMap.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry<Integer, Integer> entry = iterator.next();
			broker.bindCloudletToVm(entry.getKey(), entry.getValue());
		}
		
	}
	
	private SchedulingSolution obtainSolution() {

		ArrayList<Vm> vmList = SvilaHelper.createVmList(0, ei.getVmInputFile());
    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(ei.getWorkloadInputFile(), true, 0);
    	ArrayList<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();
		
    	CloudList.setInstance(CloudManagerHelper.convertToCloudLets(cloudletList));
    	ArrayList<Job> processingJobs = FromCloudSimToGridSim.getJobs(cloudletList);
		ArrayList<Cluster>multi = FromCloudSimToGridSim.getClusters(vmList);

		JobList.setInstance(processingJobs);

		try {
			MultiCluster.setInstance(multi, null);
		} catch (BadAllocationException e1) {
			e1.printStackTrace();
			System.exit(1);
		}
		
		 Algorithm<List<SchedulingSolution>> algorithm;
		 SchedulingProblemMultiObjective problem;
		 problem = new SchedulingProblemMultiObjective(JobList.instance(),MultiCluster.instance());

		 
		 
		CrossoverOperator<SchedulingSolution> crossover;
	    MutationOperator<SchedulingSolution> mutation;
	    SelectionOperator<List<SchedulingSolution>, SchedulingSolution> selection;
	    List<Double> referencePoint = null;
			
	    referencePoint = new ArrayList<>();
	    referencePoint.add(0.0);
	    referencePoint.add(0.0);
	  
	    //double crossoverProbability = 0.9 ;
	    //double crossoverDistributionIndex = 20.0 ;
	    
	    if(tunning) {
	    	randomizeParameters();
	    } else {
	    	configureParameters();
	    }
	    storeFinalParameters();
	    
	    
		crossover = new JobCrossover(crossoverProbability) ;
	  
	    //double mutationProbability = 1.0 / problem.getNumberOfVariables() ;
	    //double mutationDistributionIndex = 20.0 ;
		mutation = new JobSwapMutation(mutationProbability, numberOfMutations); // Uses custom mutation instead of PolynomialMutation
		
	    selection = new BinaryTournamentSelection<SchedulingSolution>(new RankingAndCrowdingDistanceComparator<SchedulingSolution>());
	  
	    // Resultats finals
	    //int populationSize = 20;
	    //int maxIterations = 10;
	    
	    
	    //int populationSize = 10;
	    //int maxIterations = 10;
	    boolean showDebug = true;
	    boolean saveSolutions = true;
	    
	    
	    
	    // population 10, 20, 30, 40, 50, 60, 70 80, 90, 100
	    
	    List<Double> iterationSolutions = new ArrayList<>();
	    
	    GWASFGALog<SchedulingSolution> gwasfgalog = new GWASFGALog<>(
	            problem,
	            populationSize,
	            maxIterations,
	            crossover,
	            mutation,
	            selection,
	            new SequentialSolutionListEvaluator<SchedulingSolution>(),
	            iterationSolutions);
	    
	    gwasfgalog.setShowDebug(showDebug);
	    gwasfgalog.setSaveSolutions(saveSolutions);
	    
	    algorithm = (Algorithm<List<SchedulingSolution>>) gwasfgalog;
		 
		 AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
		 .execute();
		 List<SchedulingSolution> population = algorithm.getResult() ;
		 long computingTime = algorithmRunner.getComputingTime() ;
		 ArrayList<SchedulingSolution> solutions = new ArrayList<SchedulingSolution>();
		 solutions.add(population.get(0));
		 		 
		 if(saveSolutions) {
			 JSONArray solutionsArray = new JSONArray();
			 solutionsArray.put(iterationSolutions);
			 this.getExtraFields().put("iterationSolutions", solutionsArray);
		 }
 
		 return population.get(0);
	}
	
	private void configureParameters() {
		crossoverProbability = getValueFromJson(extraFields, "crossoverProbability", 50.0);
		mutationProbability = getValueFromJson(extraFields, "mutationProbability", 5.0);
		annealingReduction = getValueFromJson(extraFields, "annealingReduction", 0.95);
		numberOfMutations = getValueFromJson(extraFields, "numberOfMutations", 1);
		populationSize = getValueFromJson(extraFields, "population", 50);
		maxIterations = getValueFromJson(extraFields, "generations", 40);
	}
	
	private void storeFinalParameters() {
		JSONObject finalParameters = new JSONObject();
		finalParameters.put("crossoverProbability", crossoverProbability);
		finalParameters.put("mutationProbability", mutationProbability);
		finalParameters.put("annealingReduction", annealingReduction);
		finalParameters.put("numberOfMutations", numberOfMutations);
		finalParameters.put("population", populationSize);
		finalParameters.put("generations", maxIterations);

		extraFields.put("finalParameters", finalParameters);
	}
	
	private int getValueFromJson(JSONObject jsonObject, String field, int defaultValue) {
		if(jsonObject.getInt(field) >= 0) {
		    return jsonObject.getInt(field);
	    } else {
	    	return defaultValue;
	    }
	}
	
	private double getValueFromJson(JSONObject jsonObject, String field, double defaultValue) {
		if(jsonObject.getInt(field) >= 0.0) {
		    return jsonObject.getInt(field);
	    } else {
	    	return defaultValue;
	    }
	}
	
	@Override
	public void randomizeParameters() {
		JSONObject auto = extraFields.getJSONObject("auto");
		populationSize = getIntRangedValue(auto.getJSONObject("populationRange"));
		maxIterations = getIntRangedValue(auto.getJSONObject("generationsRange"));
		crossoverProbability = getIntRangedValue(auto.getJSONObject("crossoverRange"));
		mutationProbability = getIntRangedValue(auto.getJSONObject("mutationRange"));
		numberOfMutations = getIntRangedValue(auto.getJSONObject("numberOfMutationsRange"));
		annealingReduction = getDoubleRangedValue(auto.getJSONObject("annealingReductionRange"));
	
		this.techniqueName = this.exportTechniqueName + "_" + UUID.randomUUID();
	}
	
	private int getIntRangedValue(JSONObject jsonObject) {
		return getIntRangedValue(jsonObject.getInt("min"), jsonObject.getInt("max"));
	}
	
	private int getIntRangedValue(int min, int max) {
		Random r = new Random();
		return r.nextInt( (max-min) + 1) + min;
	}
	
	private double getDoubleRangedValue(JSONObject jsonObject) {
		return getDoubleRangedValue(jsonObject.getInt("min"), jsonObject.getInt("max"));
	}
	
	private double getDoubleRangedValue(double min, double max) {
		Random r = new Random();
		return min + (max-min) * r.nextDouble();
	}
}
