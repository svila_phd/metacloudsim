package svila.staticAllocation.schedulingTechniques.extra;

import geneticsimulator.Job;
import jmetal54.JobData;
import svila.CustomCloudSimulator.CloudLet;

public class CloudData {
	public CloudLet cloudlet;
	public double[] forbiddenCores;

	public CloudData(CloudLet cloudlet, int numberOfVMs){
		this.cloudlet=cloudlet;
		this.forbiddenCores=new double[numberOfVMs];
	}
	public CloudData(CloudData cloudData){
		this.cloudlet=new CloudLet(cloudData.cloudlet);
		this.forbiddenCores=new double[cloudData.forbiddenCores.length];
		for(int i=0;i<this.forbiddenCores.length;i++){
			this.forbiddenCores[i]=cloudData.forbiddenCores[i];
		}
	}
	
	boolean equals(CloudData cloudData2){
		return this.cloudlet.getId()==cloudData2.cloudlet.getId();
	}
	
	public String toString(){
		String ret=cloudlet.getId()+" {";
		for(int i=0;i<this.forbiddenCores.length;i++){
			ret += i + ": " + this.forbiddenCores[i];
			if(i<this.forbiddenCores.length-1){
				ret +=",";
			}
		}
		ret +="}";
		return ret;
	}
}
