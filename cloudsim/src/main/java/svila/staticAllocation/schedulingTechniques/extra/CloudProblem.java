package svila.staticAllocation.schedulingTechniques.extra;

import java.util.ArrayList;
import java.util.List;

import org.uma.jmetal.problem.impl.AbstractIntegerProblem;
import org.uma.jmetal.solution.IntegerSolution;

public class CloudProblem extends AbstractIntegerProblem {
	public CloudProblem(int variables, int minValue, int maxValue) {
		super();
		this.setName("cloudProblem");
		this.setNumberOfVariables(variables);
		this.setNumberOfObjectives(2);
		
		List<Integer> minList = new ArrayList<>();
		List<Integer> maxList = new ArrayList<>();
		for(int i=0; i<this.getNumberOfVariables(); i++) {
			minList.add(minValue);
			maxList.add(maxValue);
		}
		this.setLowerLimit(minList);
		this.setUpperLimit(maxList);
	}
	
	@Override
	public void evaluate(IntegerSolution solution) {
		
		double makespan, energy;
		
		
		
		int sum = 0;
		
		for(int i=0; i<solution.getNumberOfVariables(); i++) {
			sum += solution.getVariableValue(i);
		}
		
		solution.setObjective(0, -sum);
	}
}
