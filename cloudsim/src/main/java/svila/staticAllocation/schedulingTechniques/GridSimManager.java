package svila.staticAllocation.schedulingTechniques;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import eduni.simjava.Sim_system;
import geneticsimulator.Job;
import gridsim.GridSim;
import gridsim.Gridlet;
import gridsim.MachineList;
import gridsim.net.FIFOScheduler;
import gridsim.net.RIPRouter;
import gridsim.net.Router;
import simulator.CommPolicy;
import simulator.CommWorkload;
import simulator.MCEnvironment;
import simulator.SettingsLoader;
import simulator.SimulatorLog;
import svila.staticAllocation.CloudletsAssignation;
import shedulers.CBSPolicy;
import shedulers.EAAllocPolicy;
import shedulers.EGAPolicy;
import shedulers.ERAPolicy;
import shedulers.FCFSPolicy;
//import shedulers.GAOrderMESDEAPolicy;
//import shedulers.GAOrderMESDPolicy;
//import shedulers.GAOrderPolicy;
import shedulers.GRASPMESDPolicy;
import shedulers.HillClimbingPolicy;
import shedulers.JPRPolicy;
import shedulers.MESDPolicy;
import shedulers.MOGAPolicy;
import shedulers.PSOMOPolicy;
import shedulers.PSOPolicy;
import shedulers.energy.CBSEPolicy;
import shedulers.energy.JPREPolicy;
import shedulers.energy.METLEPolicy;
import shedulers.energy.MinMinPolicy;

public class GridSimManager {
	
	public static int numProcs = 348;
	
	public static String POLICY_FCFS = "fcfs";
	public static String POLICY_CBS = "cbs";
	public static String POLICY_JPR = "jpr";
	
	public static String POLICY_MESD = "mesd"; // NO
	public static String POLICY_GAM = "gaMESD"; // NO
	public static String POLICY_GAMEA = "gaMESDEA"; // NO
	public static String POLICY_GA = "ga"; // NO
	public static String POLICY_GRASP = "grasp"; // NO
	public static String POLICY_HILL = "hill";
	public static String POLICY_EA = "ea"; // NO
	public static String POLICY_ERA="era"; // NO
	public static String POLICY_MOGA="moga";
	
	public static String POLICY_EGA="ega"; // NO
	public static String POLICY_MINMIN="minmin";
	public static String POLICY_JPREA = "jpre";
	public static String POLICY_CBSE = "cbse";
	public static String POLICY_METLE = "metle"; // NO
	public static String POLICY_PSO = "pso";
	
	static SimulatorLog log;
	
	public static CloudletsAssignation launchSimulator(String policyName, String[] policyModificator, MachineList machinesList, List<Job> jobs) throws Exception {
		long start = System.currentTimeMillis();

		Calendar calendar = Calendar.getInstance();

		
		// Initialize the GridSim package
		System.out.println("Initializing GridSim package");
		// 1 is the number of users
		System.out.println("START -------------------------------------------");
		GridSim.unsetGIS();		
		GridSim.init(1, calendar, true);
		GridSim.disableDebugMode();

		// ------------------------------------------------
		// CHECK, VALIDATE, AND CREATE FOLDERS
		// ------------------------------------------------
		// check input folder
		/*String projectPath = "GridSimProjectPath";
		File file = new File(projectPath);
		if (!file.isDirectory() && !file.exists()) {
			System.out.println("Input folder does not exist or is not accessible.");
			System.out.println("Exit.");
			System.exit(0);
		}
		// adjust '/' termination
		if (projectPath.charAt(projectPath.length() - 1) != '/') {
			projectPath = projectPath + "/";
		}*/

		// ------------------------------------------------
		// SETTINGS LOADER
		// ------------------------------------------------
		/*SettingsLoader settings = new SettingsLoader(projectPath);
		if (settings.readFile() == false) {
			System.out.println("\nMCGridSim abruptly exited due to I/O errors.\n");
			System.exit(1);
		}*/

		// ------------------------------------------------
		//	MULTICLUSTER ENVIRONMENT LOADER ANAD ALLOCATION POLICY
		//------------------------------------------------
		String ClusterName="MultiC";
		CommPolicy policy=null;
		
		String idProva = "0";
		
		if(policyName.equals(POLICY_FCFS)){
			policy = new FCFSPolicy(ClusterName,"POLICY_FCFS");
		}else if(policyName.equals(POLICY_CBS)){
			policy=new CBSPolicy(ClusterName,"POLICY_CBS");
		}else if(policyName.equals(POLICY_JPR)){
			policy=new JPRPolicy(ClusterName,"POLICY_JPR");
		}else if(policyName.equals(POLICY_JPREA)){
			policy=new JPREPolicy(ClusterName,"POLICY_JPREA");
		}else if(policyName.equals(POLICY_MESD)){
			policy=new MESDPolicy(ClusterName,"POLICY_MESD",policyModificator);
		}else if(policyName.equals(POLICY_GRASP)){
			policy=new GRASPMESDPolicy(ClusterName,"POLICY_GRASP",policyModificator);
		}/*else if(policyName.equals(POLICY_GA)){
			policy=new GAOrderPolicy(ClusterName,"POLICY_GA",policyModificator);
		}else if(policyName.equals(POLICY_GAM)){
			policy=new GAOrderMESDPolicy(ClusterName,"POLICY_GAM",policyModificator);
		}else if(policyName.equals(POLICY_GAMEA)){
			policy=new GAOrderMESDEAPolicy(ClusterName,"POLICY_GAMEA",policyModificator);
		}*/else if(policyName.equals(POLICY_HILL)){
			policy=new HillClimbingPolicy(ClusterName,"POLICY_HILL",policyModificator);
		}else if(policyName.equals(POLICY_EA)){
			policy=new EAAllocPolicy(ClusterName,"POLICY_EA");
		}else if(policyName.equals(POLICY_METLE)){
			policy=new METLEPolicy(ClusterName,"POLICY_EB",policyModificator);
		}else if(policyName.equals(POLICY_ERA)){
			policy=new ERAPolicy(ClusterName,"POLICY_ERA");
		}else if(policyName.equals(POLICY_MOGA)){
			policy=new MOGAPolicy(ClusterName,"POLICY_MOGA",policyModificator,/*projectPath*/"" + "output/",idProva);
		}else if(policyName.equals(POLICY_EGA)){
			policy=new EGAPolicy(ClusterName,"POLICY_EGA",policyModificator,/*projectPath*/"" + "output/",idProva);
		}else if(policyName.equals(POLICY_MINMIN)){
			policy=new MinMinPolicy(ClusterName,"POLICY_MINMIN",policyModificator);
		}else if(policyName.equals(POLICY_CBSE)){
			policy=new CBSEPolicy(ClusterName,"POLICY_CBSE");
		}else if(policyName.equals(POLICY_PSO)){
			policy=new PSOMOPolicy(ClusterName,"POLICY_PSO",policyModificator,/*projectPath*/"" +"output/",idProva);
		}else{
			System.out.println("No allocation policy specified. Exit.");
			System.exit(1);
		}
		System.out.println("Allocation policy: " + policy);
		
		log = new SimulatorLog();
		String envpath=/*projectPath*/"" + "input/";/*+ settings.getSettingString("environment-file")*/;
		
		MCEnvironment cluster=MCEnvironment.CreateCustomCluster(ClusterName,policy, envpath, log, policyModificator, machinesList);

		
		String workpath=/*projectPath*/"" + "input/";/*+ settings.getSettingString("workload-file");*/
		double baud_rate = 1000; // bits/sec
		double propDelay = 10;   // propagation delay in millisecond
		int mtu = 1500;          // max. transmission unit in byte
		
		CommWorkload workload = null;
		
		try {
			workload = new CommWorkload("Workload",baud_rate,propDelay,mtu,workpath,cluster.get_name(), jobs);
			} catch (Exception e1) {
			e1.printStackTrace();
			System.exit(1);
		}
		Router rC = new RIPRouter("routerC", false);
		FIFOScheduler userSched = new FIFOScheduler("UserSched_0");
		rC.attachHost(workload, userSched);
		FIFOScheduler resSched = new FIFOScheduler("MultiClusterSched_0");
		rC.attachHost(cluster, resSched);
		
		GridSim.startGridSimulation();
		//workload.printGridletList(false);
		System.out.println("FINISH -------------------------------------------");
		double end=System.currentTimeMillis();
		double simtime=(end-start)/1000;
		//System.out.println("Simulation time : "+simtime);
		//System.out.println("Makespan: "+workload.getMakespan());
		//System.out.println("Energy: "+ cluster.getEnergyConsumed() );
		
		GridSim.stopGridSimulation();
		GridSim.unsetGIS();
		Sim_system.setInComplete(true);
		
		CloudletsAssignation ca = new CloudletsAssignation();
		
		for(Gridlet gridlet : workload.list_) {
			int id = gridlet.getGridletID()-1;
			if(id < 0) {
				continue;
			}
			int machineId = getMoreAllocatedMachine((simulator.Job)gridlet);
			ca.linkCloudletWithVM(id, machineId);
		}
		
		return ca;
	}
	
	private static int getMoreAllocatedMachine(simulator.Job gridlet) {
		int bestCluster=0, moreNodes = 0;
		HashMap<Integer, Integer> countAllocations = new HashMap<>();
		for(int[] alloc : gridlet.getAllocation()) {
			int machineId = alloc[0];
			if(countAllocations.containsKey(machineId)) {
				int numTimesAllocated = countAllocations.get(machineId)+1;
				if(numTimesAllocated > moreNodes) {
					bestCluster = machineId;
					moreNodes = numTimesAllocated;
				}
				countAllocations.put(machineId, numTimesAllocated);
			} else {
				countAllocations.put(machineId, 1);
				if(1 > moreNodes) {
					bestCluster = machineId;
					moreNodes = 1;
				}
			}
		}
		
		return bestCluster;
	}
}
