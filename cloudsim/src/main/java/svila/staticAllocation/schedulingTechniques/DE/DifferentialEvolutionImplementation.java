package svila.staticAllocation.schedulingTechniques.DE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.SolutionListEvaluator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;

import genetic.JobCrossover;
import genetic.JobSwapMutation;
import genetic.NSGALog;
import genetic.SchedulingProblemMultiObjective;
import genetic.SchedulingSolution;
import geneticsimulator.Job;
import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;
import gridsim.MachineList;
import svila.GridCloudBridge.FromCloudSimToGridSim;
import svila.staticAllocation.CloudletsAssignation;
import svila.staticAllocation.SWFAnalyzer;
import svila.staticAllocation.SvilaHelper;
import svila.staticAllocation.schedulingTechniques.GridSimManager;
import svila.staticAllocation.schedulingTechniques.StaticTaskSchedulingTechnique;

public class DifferentialEvolutionImplementation extends StaticTaskSchedulingTechnique {

	CloudletsAssignation bestAssignation;
	
	public DifferentialEvolutionImplementation() {
		this.techniqueName = "DE";
	}
	
	public DifferentialEvolutionImplementation(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		this.techniqueName = "DE";
	}

	@Override
	public void execute() {
		Iterator<Entry<Integer, Integer>> iterator = bestAssignation.cloudletVMMap.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry<Integer, Integer> entry = iterator.next();
			broker.bindCloudletToVm(entry.getKey(), entry.getValue());
		}
		
	}

	@Override
	public void startTechnique() {
		ArrayList<Vm> vmList = SvilaHelper.createVmList(0, ei.getVmInputFile());
    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(ei.getWorkloadInputFile(), true, 0);
    	ArrayList<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();
		
		ArrayList<Job> processingJobs = FromCloudSimToGridSim.getJobs(cloudletList);
		MachineList machinesList = FromCloudSimToGridSim.getMachineList(vmList);
		
		try {
			 
			
			
			 SchedulingProblemMultiObjective problem;
			 Algorithm<List<SchedulingSolution>> algorithm;
			 CrossoverOperator<SchedulingSolution> crossover;
			 MutationOperator<SchedulingSolution> mutation;
			 SelectionOperator<List<SchedulingSolution>, SchedulingSolution> selection;
			 SolutionListEvaluator<SchedulingSolution> evaluator;
			 evaluator = new SequentialSolutionListEvaluator<SchedulingSolution>();	
			 problem = new SchedulingProblemMultiObjective(JobList.instance(),MultiCluster.instance());
			 crossover = new JobCrossover() ;
			 mutation = new JobSwapMutation();
			 selection = new BinaryTournamentSelection<SchedulingSolution>(new RankingAndCrowdingDistanceComparator<SchedulingSolution>());
			 algorithm = (Algorithm<List<SchedulingSolution>>) new NSGALog(problem,this.generations,this.population,crossover,mutation,selection, evaluator,this.output+"ConvergenceE"+this.evaluationMetric+idProva+".csv");
			 AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
			 .execute();
			 List<SchedulingSolution> population = algorithm.getResult() ;
			 long computingTime = algorithmRunner.getComputingTime() ;
			 ArrayList<SchedulingSolution> solutions = new ArrayList<SchedulingSolution>();
			 solutions.add(population.get(0));
	 
			 //return population.get(0);
			
			
			//bestAssignation = GridSimManager.launchSimulator(GridSimManager.POLICY_PSO, null, machinesList, processingJobs);		
			//ei.launchStandaloneExperiment();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
