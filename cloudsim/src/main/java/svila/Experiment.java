/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package svila;

import java.util.List;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

/**
 *
 * @author Sergi
 */
public abstract class Experiment {
    
    public CustomConfiguration cc;
    
    public Experiment(CustomConfiguration cc) {
        this.cc = cc;
    }
    
    abstract List<Cloudlet> createCustomCloudletList(int brokerId);
    abstract void addCloudlet(int id, String cloudletPath, int brokerId, List<Cloudlet> list);
    abstract List<Cloudlet> createCustomCloudletList(int brokerId, int cloudletsNumber);
    abstract List<Vm> createCustomVmList(int brokerId, int vmsNumber);
    abstract void addPowerVm(int id, int brokerId, int vmType, List<Vm> vms);
    abstract List<PowerHost> createCustomHostList();
    abstract void addPowerHostUtilizationHistory(int id, int hostType, List<PowerHost> hostList);
}
