package svila;

public class SvilaPrinter {
	
	static boolean enabled = true;
	
	public static void enable(boolean enabled) {
		SvilaPrinter.enabled = enabled;
	}
	
	public static void enablePrint() {
		enabled = true;
	}
	
	public static void disablePrint() {
		enabled = false;
	}
	
	public static void print(String text) {
		if(enabled) {
			System.out.println(text);	
		}
	}
	
	public static void print(double text) {
		if(enabled) {
			System.out.println(text);	
		}
	}
	
	public static void print(int text) {
		if(enabled) {
			System.out.println(text);	
		}
	}
}
