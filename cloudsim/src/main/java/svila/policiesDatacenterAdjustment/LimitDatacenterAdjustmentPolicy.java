package svila.policiesDatacenterAdjustment;

import java.util.List;

import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public class LimitDatacenterAdjustmentPolicy extends AbstractDatacenterAdjustmentPolicy {
	
	int historicLength;
	double ratio;
	
	public LimitDatacenterAdjustmentPolicy(FutureVMOptimizerPolicy vmOptimizer, List<Float> params) {
		super(vmOptimizer);
		historicLength = Math.round(params.get(0));
		ratio = params.get(1);
		
		if(historicLength == -1) {
			historicLength = 6;
		}
		
		if(ratio == -1.0) {
			ratio = 1.1;
		}
	}

	@Override
	public void adjust() {
		LimitManager lm = new LimitManager(this.vmOptimizer.futureHostClassified,  this.vmOptimizer.datacenterAnalyzer, historicLength, ratio);
		lm.moveLevelOverRequestedLevel();
		lm.blockHosts();
		
		
		
		/*HashMap<String, Set<FutureHost>> futureHostClassified = this.vmOptimizer.futureHostClassified;
		
		Set<FutureHost> overloaded = futureHostClassified.get("overloaded");
		Set<FutureHost> powerOff = futureHostClassified.get("poweroff");
		Set<FutureHost> valid = futureHostClassified.get("active");
		Set<FutureHost> blocked = futureHostClassified.get("blocked");*/
		
		// all: total MIPS
		// all - blocked: current available MIPS
		// all - blocked - off: current active MIPS

		// requested MIPS * capacityRatio: limit, value must be under current available MIPS
		
		// if limit > current active MIPS:
		//		unlock host until limit is over current active MIPS
		//		start with blocked hosts, then poweroff
		// if limit < current active MIPS:
		//		lock hosts until limit is just over current active MIPS
		//		start with poweroff hosts, then active
		
		// datacenter history
		
		
		/*JSONObject limitData = new JSONObject();
		List<Double> datacenterMIPSRequested = table.doubleColumn(DatacenterStats.REQUESTED_MIPS).asList();
		limitData.put("historic", datacenterMIPSRequested);
		int historyLength = datacenterMIPSRequested.size();
		for(String hostType : futureHostClassified.keySet()) {
			Set<FutureHost> currentHosts = futureHostClassified.get(hostType);
			for(FutureHost currentHost : currentHosts) {
				double totalHostMIPS = currentHost.getCurrentHost().getTotalMips();
				JSONObject host = new JSONObject();
				host.put("id", currentHost.getCurrentHost().getId());
				host.put("netId", currentHost.getCurrentHost().getId());
				host.put("totalMIPS", totalHostMIPS);
				host.put("state", hostType);
			}
		}
		
		Map<String, List<FutureHost>> hostLevelMap = new HashMap<>();
		
		double oversaturatedSum;
		double activeSum;
		double offSum;
		double blockedSum;
		
		double oversaturatedLevel = oversaturatedSum;
		double activeLevel = oversaturatedLevel + activeSum;
		double offLevel = activeLevel + offSum;
		double blockedLevel = offSum + blockedSum;
		
		limitData.put("oversaturatedSum", oversaturatedSum);
		limitData.put("activeSum", activeSum);
		limitData.put("offSum", offSum);
		limitData.put("blockedSum", blockedSum);

		limitData.put("oversaturatedLevel", oversaturatedLevel);
		limitData.put("activeLevel", activeLevel);
		limitData.put("offLevel", offLevel);
		limitData.put("blockedLevel", blockedLevel);
		*/

	}
}
