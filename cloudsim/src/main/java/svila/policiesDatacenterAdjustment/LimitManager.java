package svila.policiesDatacenterAdjustment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import svila.SvilaPrinter;
import svila.metricsLogic.DatacenterAnalyzer;
import svila.metricsLogic.DatacenterStats;
import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.StaticResources;
import svila.policiesVmOptimizer.future.FutureHost;
import tech.tablesaw.api.Table;

public class LimitManager {
	
	HashMap<String, Set<FutureHost>> futureHostClassified;
	HashMap<FutureHost, String> originalHostStates;
	HashMap<FutureHost, String> newHostStates;
	HashMap<String, Set<FutureHost>> levelMap;
	HashMap<String, Set<FutureHost>> blockedMap;
	DatacenterAnalyzer datacenterAnalyzer;
	double requestedLevel;
	int historicLength;
	int maxBlockedHosts;
	double ratio;
	
	public LimitManager(HashMap<String, Set<FutureHost>> futureHostClassified,
						DatacenterAnalyzer datacenterAnalyzer,
						int historicLength, double ratio) {
		this.futureHostClassified = futureHostClassified;
		this.datacenterAnalyzer = datacenterAnalyzer;
		this.historicLength = historicLength;
		this.ratio = ratio;
		initMaps();
		requestedLevel = getRequestedLevel();
	}
	
	private void initMaps() {
		levelMap = new HashMap<>();
		originalHostStates = new HashMap<>();
		newHostStates = new HashMap<>();
		newHostStates = new HashMap<>();
		blockedMap = new HashMap<>();
		String[] levels = new String[] {"overloaded", "active", "poweroff", "blocked"};
		for(String level : levels) {
			levelMap.put(level, new HashSet<>());
		}
		String[] blockedStates = new String[] {"active", "poweroff"};
		for(String state : blockedStates) {
			blockedMap.put(state, new HashSet<>());
		}
		for(String hostType : futureHostClassified.keySet()) {
			if(hostType == "blocked" || hostType == "all" || hostType == "valid") {
				continue;
			}
			Set<FutureHost> currentHosts = futureHostClassified.get(hostType);
			for(FutureHost currentHost : currentHosts) {
				levelMap.get(hostType).add(currentHost);
				originalHostStates.put(currentHost, hostType);
				newHostStates.put(currentHost, hostType);
			}
		}
		
		maxBlockedHosts = futureHostClassified.get("active").size() + futureHostClassified.get("poweroff").size();
		
		Set<FutureHost> currentHosts = futureHostClassified.get("blocked");
		for(FutureHost currentHost : currentHosts) {
			blockHost(currentHost);
		}
	}
	
	private double getRequestedLevel() {
		DatacenterStats dcs = datacenterAnalyzer.datacenterStats.getSlice(Math.max(datacenterAnalyzer.index - historicLength, 0), datacenterAnalyzer.index, 0);
		Table table = dcs.getTable();
		return table.doubleColumn(DatacenterStats.REQUESTED_MIPS).percentile(95.0) * ratio;
	}
	
	public double getAvailableMIPS() {
		return getLevel("poweroff");
	}
	
	private double getSum(String type) {
		double sum = 0.0;
		for(FutureHost host : levelMap.get(type)) {
			sum += host.getCurrentHost().getTotalMips();
		}
		return sum;
	}
	
	private double getLevel(String level) {
		double value = 0.0;
		switch(level) {
		case "blocked":
			value += getSum("blocked");
		case "poweroff":
			value += getSum("poweroff");
		case "active":
			value += getSum("active");
		case "overloaded":
			value += getSum("overloaded");
		}
		return value;
	}
	
	private double getCurrentMips() {
		return getLevel("poweroff");
	}
	
	private String getCurrentZone(double requestedLevel) {
		String[] levels = new String[] {"overloaded", "active", "poweroff", "blocked"};
		for(String level : levels) {
			if(requestedLevel < getLevel(level)) {
				return level;
			}
		}
		return "blocked";
	}
	
	private boolean blockHost(FutureHost host) {
		String currentState = newHostStates.get(host);
		if(currentState == "blocked" || currentState == "overloaded") {
			return false;
		}
		
		this.levelMap.get(currentState).remove(host);
		this.levelMap.get("blocked").add(host);
		this.blockedMap.get(currentState).add(host);
		newHostStates.put(host, "blocked");
		return true;
	}
	
	private boolean unlockHost(FutureHost host) {
		String currentState = newHostStates.get(host);
		String originalState = originalHostStates.get(host);
		if(currentState != "blocked") {
			return false;
		}
		
		this.levelMap.get("blocked").remove(host);
		this.levelMap.get(originalState).add(host);
		this.blockedMap.get(originalState).remove(host);
		newHostStates.put(host, originalState);
		return true;
	}
	
	private FutureHost getHostToUnlock() {
		String[] states = new String[] {"active", "poweroff"};
		for(String state : states) {
			Set<FutureHost> currentBlockedHosts = blockedMap.get(state);
			if(currentBlockedHosts.size() == 0) {
				continue;
			}
			
			return currentBlockedHosts.iterator().next();
		}
		return null;
	}
	
	private FutureHost getHostToBlock() {
		String[] states = new String[] {"poweroff", "active"};
		for(String state : states) {
			Set<FutureHost> currentHosts = levelMap.get(state);
			if(currentHosts.size() == 0) {
				continue;
			}
			
			return currentHosts.iterator().next();
		}
		return null;
	}
	
	private boolean blockOneHost() {
		return blockHost(getHostToBlock());
	}
	
	private boolean unlockOneHost() {
		return unlockHost(getHostToUnlock());
	}
	
	private class AdjustmentStep {
		
		int step;
		double requestedLevel;
		double currentLevel;
		int blockedHosts;
		
		public AdjustmentStep(int step, double requestedLevel, double currentLevel, int blockedHosts) {
			this.step = step;
			this.requestedLevel = requestedLevel;
			this.currentLevel = currentLevel;
			this.blockedHosts = blockedHosts;
		}
		
		public String toString() {
			return "step: " + step + 
					" requestedLevel: " + requestedLevel +
					" currentLevel: " + currentLevel + 
					" blockedHosts: " + blockedHosts;
		}
		
		public String getLine() {
			return step + "," + requestedLevel + "," + currentLevel +
				   "," + blockedHosts;
		}
	}
	
	public void moveLevelOverRequestedLevel() {
		boolean stop = false;
		double requestedLevel = getRequestedLevel();
		double currentInitialLevel = getCurrentMips();
		double currentLevel = currentInitialLevel;
		int steps = 0;
		int numHosts = originalHostStates.size();
		boolean isOver = currentLevel > requestedLevel;
		boolean isLastOver = isOver;
		
		List<AdjustmentStep> adjustmentSteps = new ArrayList<>();
		
		while(!stop) {
			adjustmentSteps.add(new AdjustmentStep(steps, requestedLevel, currentLevel, levelMap.get("blocked").size()));
			isOver = currentLevel > requestedLevel;
			
			if(isOver) {
				if(isLastOver) { // block
					if(maxBlockedHosts == this.levelMap.get("blocked").size()) { // check how many hosts can be blocked, if no more, stop
						stop = true;
					} else { // else: block
						blockOneHost();
					}
				} else { // Final condition reached
					stop = true;
				}
			} else {
				if(this.levelMap.get("blocked").isEmpty()) { // No more hosts to unlock
					stop = true;
				} else { // unlock
					unlockOneHost();
				}
			}
			
			isLastOver = isOver;
			if(steps > numHosts) {
				stop = true;
			}
			steps++;
			currentLevel = getCurrentMips();
		}
	
		adjustmentSteps.add(new AdjustmentStep(steps, requestedLevel, currentLevel, levelMap.get("blocked").size()));
		
		//showStats(adjustmentSteps);
		
		if(!stop) {
			System.out.println("Check stop false");
		}
	}
	
	private void showStats(List<AdjustmentStep> adjustmentSteps) {
		for(AdjustmentStep as : adjustmentSteps) {
			System.out.println(as);
		}
	}
	
	public void blockHosts() {
		this.levelMap.get("blocked").forEach((k) -> k.setBlocked(true));
		futureHostClassified.get("valid").clear();
		futureHostClassified.get("blocked").clear();
		futureHostClassified.get("active").clear();
		ExperimentConfiguration ec = StaticResources.getCE();

		for(Entry<FutureHost, String> entry : newHostStates.entrySet()) {
			FutureHost host = entry.getKey();
			String oldState = originalHostStates.get(host);
			String newState = entry.getValue();
			if(oldState != newState) {
				futureHostClassified.get(oldState).remove(host);
				futureHostClassified.get(newState).add(host);
			}
			
			if(host.isBlocked()) {
				futureHostClassified.get("blocked").add(host);
				continue;
			}
			
			if(newState == "active") {
				futureHostClassified.get("active").add(host);
				futureHostClassified.get("valid").add(host);
			}
			
			if(newState == "poweroff" && !ec.isOnlyInitialHosts()) {
				futureHostClassified.get("valid").add(host);
			}
		}
	}
	
	public void checkLimit() {
		double requestedLevel = getRequestedLevel();
		double currentLevel = getCurrentMips();
		SvilaPrinter.print("requestedLevel: " + requestedLevel);
		SvilaPrinter.print("currentLevel: " + currentLevel);
	}
}
