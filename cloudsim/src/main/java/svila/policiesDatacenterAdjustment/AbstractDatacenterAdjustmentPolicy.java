package svila.policiesDatacenterAdjustment;

import java.util.HashMap;

import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;
import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public abstract class AbstractDatacenterAdjustmentPolicy {
	FutureVMOptimizerPolicy vmOptimizer;
	
	public AbstractDatacenterAdjustmentPolicy(FutureVMOptimizerPolicy vmOptimizer) {
		this.vmOptimizer = vmOptimizer;
	}
	
	public abstract void adjust();
	
	public void addVMsFromBlockedHosts(HashMap<Integer, FutureVm> vmsToMigrate) {
		for(FutureHost futureHost : vmOptimizer.futureHosts.values()) {
			if(futureHost.isBlocked()) {
				for(FutureVm futureVm : futureHost.getFutureVms().values()) {
					futureHost.popVm(futureVm);
					vmsToMigrate.put(futureVm.getCurrentVm().getNetworkId(), futureVm);
				}
			}
		}
	}
}
