package svila.policiesDatacenterAdjustment;

import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public class NoDatacenterAdjustmentPolicy extends AbstractDatacenterAdjustmentPolicy {

	public NoDatacenterAdjustmentPolicy(FutureVMOptimizerPolicy vmOptimizer) {
		super(vmOptimizer);
	}

	@Override
	public void adjust() {}

}
