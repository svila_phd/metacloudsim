package svila.CustomCloudSimulator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cedarsoftware.util.io.JsonWriter;

import svila.JSONOutput;

public class TimeSharedResults {
	public double makespan, energy;
	public List<CloudLet> cloudlets;
	public List<CloudVM> vms;
	public List<CloudHost> hosts;
	public HashMap<CloudHost, HashMap<CloudVM, Double>> results;
	public boolean export;
	JSONObject root;
	
	private static final String TIMESTAMP = "timestamp";
	private static final String DATE = "date";
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
	
	//private static int val, val2;
	
	public TimeSharedResults(List<CloudLet> cloudlets, List<CloudVM> vms, List<CloudHost> hosts) {
		this.cloudlets = cloudlets;
		this.vms = vms;
		this.hosts = hosts;
		this.results = new HashMap<CloudHost, HashMap<CloudVM, Double>>();
		this.export = true;
	}
	
	public void setExport(boolean export) {
		this.export = export;
	}
	
	public void generateJSON() {
		root = new JSONObject();
		long millis = System.currentTimeMillis();
		root.put(TIMESTAMP, millis);
		root.put(DATE, sdf.format(millis));
		
		generateAssignation();
		generateResults();
	}
	
	private void generateAssignation() {
		JSONObject assignation = new JSONObject();
		root.put("assignation", assignation);
		
		JSONArray hostsArray = new JSONArray();
		
		for(CloudHost host : hosts) {
			JSONObject hostObject = new JSONObject();
			hostObject.put("id", host.getId());
			hostObject.put("numCores", host.getTotalCores());
			hostObject.put("MIPS", host.getTotalCPU());
			hostObject.put("energyRunning", host.getEnergyRunning());
			hostObject.put("energyIdle", host.getEnergyIdle());
			JSONArray vmsArray = new JSONArray();
			PriorityQueueIterator<CloudVM> vmIt = host.getVMIterator();
			while(vmIt.hasNext()) {
				CloudVM vm = vmIt.next();
				JSONObject vmObject = new JSONObject();
				vmObject.put("id", vm.getId());
				vmObject.put("numCores", vm.getTotalCores());
				vmObject.put("MIPS", vm.getTotalCPU());
				vmsArray.put(vmObject);
			}
			hostObject.put("vms", vmsArray);
			hostsArray.put(hostObject);
		}
		
		assignation.put("hosts", hostsArray);
	}
	
	public void generateResults() {
		JSONObject results = new JSONObject();
		if(root == null) {
			root = new JSONObject();
		}
		root.put("results", results);

		double currentMakespan = 0.0;
		
		for(CloudHost host : hosts) {
			PriorityQueueIterator<CloudVM> vmIt = host.getVMIterator();
			while(vmIt.hasNext()) {
				CloudVM vm = vmIt.next();
				double vmTime = vm.getTimeSharedMakespan();
				if(currentMakespan < vmTime) {
					currentMakespan = vmTime;
				}
			}
		}
		
		makespan = currentMakespan;
		
		
		JSONArray hostsArray = new JSONArray();
		List<Double> vmTimes = new ArrayList<>();
		List<Double> vmEnergies = new ArrayList<>();
		
		for(CloudHost host : hosts) {
			double vmTimeRunning = 0.0, vmTimeIdle = 0.0, vmEnergyRunning = 0.0, vmEnergyIdle = 0.0;
			double hostTimeRunning = 0.0, hostTimeIdle = 0.0, hostEnergyRunning = 0.0, hostEnergyIdle = 0.0;
			double energyConsumptionRunning = 0.0, energyConsumptionIdle = 0.0;

			JSONObject hostObject = new JSONObject();
			JSONArray vmArray = new JSONArray();
			PriorityQueueIterator<CloudVM> vmIt = host.getVMIterator();
			while(vmIt.hasNext()) {
				CloudVM vm = vmIt.next();
				JSONObject vmObject = new JSONObject();
				vmTimeRunning = vm.getTimeSharedMakespan();
				vmTimeIdle = makespan - vmTimeRunning;
				energyConsumptionRunning = vm.getAssignedHost().getEnergyRunning();
				energyConsumptionIdle = vm.getAssignedHost().getEnergyIdle();
				PriorityQueueIterator<CloudCore> coreIt = vm.getCoreIterator();
				
				JSONArray coreArray = new JSONArray();
				while(coreIt.hasNext()) {
					JSONObject coreObject = new JSONObject();
					CloudCore cc = coreIt.next();
					coreObject.put("id", cc.getNum());
					Iterator<CloudLet> cloudletIt = vm.cloudletList.iterator();
					JSONArray cloudletArray = new JSONArray();
					
					while(cloudletIt.hasNext()) {
						CloudLet c = cloudletIt.next();
						if(export) {
							cloudletArray.put(c.getId());
						}
					}
					
					double coreEnergyRunning = energyConsumptionRunning * vmTimeRunning;
					double coreEnergyIdle = energyConsumptionIdle * vmTimeIdle;
					
					if(export) {
						coreObject.put("cloudlets", cloudletArray);
						coreObject.put("timeRunning", vmTimeRunning);
						coreObject.put("timeIdle", vmTimeIdle);
						coreObject.put("energyRunning", coreEnergyRunning);
						coreObject.put("energyIdle", coreEnergyIdle);
						coreArray.put(coreObject);
					}
					
					
					vmEnergyRunning += coreEnergyRunning;
					vmEnergyIdle += coreEnergyIdle;
				}
				if(export) {
					vmObject.put("cores", coreArray);
					vmArray.put(vmObject);
					
					vmObject.put("id", vm.getId());
					vmObject.put("timeRunning", vmTimeRunning);
					vmObject.put("timeIdle", vmTimeIdle);
					vmObject.put("energyRunning", vmEnergyRunning);
					vmObject.put("energyIdle", vmEnergyIdle);
					vmObject.put("energy", vmEnergyRunning + vmEnergyIdle);
					vmObject.put("utilization", vmTimeRunning / makespan);
				}
				
				
				vmTimes.add(vmTimeRunning);
				vmEnergies.add(vmEnergyRunning + vmEnergyIdle);
				
				if(export) {
					hostObject.put("vms", vmArray);
				}
				
				hostEnergyRunning += vmEnergyRunning;
				hostEnergyIdle += vmEnergyIdle;
				hostTimeRunning = Math.max(hostTimeRunning, vmTimeRunning);
			}
			
			hostTimeIdle = makespan - hostTimeRunning;
			
			if(export) {
				hostObject.put("id", host.getId());
				hostObject.put("timeRunning", hostTimeRunning);
				hostObject.put("timeIdle", hostTimeIdle);
				hostObject.put("energyRunning", hostEnergyRunning);
				hostObject.put("energyIdle", hostEnergyIdle);
				hostObject.put("energy", hostEnergyRunning + hostEnergyIdle);
				hostObject.put("utilization", hostTimeRunning / makespan);
			}
			
			
			energy += hostEnergyRunning + hostEnergyIdle;
			hostsArray.put(hostObject);
		}
		
		if(export) {
			results.put("makespan", makespan);
			results.put("energy", energy);
			results.put("standardDeviationTime", getStdDev(vmTimes));
			results.put("standardDeviationEnergy", getStdDev(vmEnergies));
			results.put("hosts", hostsArray);
		}
		
		//System.out.println("Val: " + val);
		//val++;
		//if(makespan == 0) {
			//System.out.println("Val2: " + val2);
			//val2++;
			/*System.out.println("=====");
			for(CloudVM cvm : vms) {
				System.out.println(cvm.cloudletList.size());
			}
			System.out.println("CL: " + vms.get(0).cloudletList.size());
			System.out.println("T: " + vms.get(0).getTimeSharedMakespan());
			System.out.println("=====");
			for(CloudLet cl : cloudlets) {
				System.out.println(cl.getId() + " " + cl.assignedVM);
			}*/
		//}
	}

	
	//https://stackoverflow.com/questions/7988486/how-do-you-calculate-the-variance-median-and-standard-deviation-in-c-or-java
	private double getMean(List<Double> values) {
        double sum = 0.0;
        for(double a : values)
            sum += a;
        return sum/values.size();
    }

    private double getVariance(List<Double> values) {
        double mean = getMean(values);
        double temp = 0;
        for(double a : values)
            temp += (a-mean)*(a-mean);
        return temp/(values.size()-1);
    }

    private double getStdDev(List<Double> values) {
        return Math.sqrt(getVariance(values));
    }
	
	public void exportJSON(String path) {
		checkRoot();
		
		try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(path));
            bw.write(JsonWriter.formatJson(root.toString()));
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(JSONOutput.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
	
	private void checkRoot() {
		if(root == null) {
			generateJSON();
		}
	}
	
	public void showJSON() {
		checkRoot();
		System.out.println(root.toString(4));
	}
}
