package svila.CustomCloudSimulator;

import java.util.Comparator;

public class CloudLetAllocationComparator implements Comparator<CloudLetAllocation> {

	@Override
	public int compare(CloudLetAllocation o1, CloudLetAllocation o2) {
		if(o1.getStartTime() > o2.getStartTime()) {
			return 1;
		} else if(o1.getStartTime() < o2.getStartTime()) {
			return -1;
		}
		return 0;
	}

}
