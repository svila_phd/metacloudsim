package svila.CustomCloudSimulator.events;

import svila.CustomCloudSimulator.CloudManager;
import svila.CustomCloudSimulator.CloudMessage;

public class StartSimulation extends CloudEvent {

	public StartSimulation() {
		
	}
	
	@Override
	public void execute() {
		CloudManager.getInstance().setActive(true);
		CloudManager.getInstance().sendMessage(CloudMessage.Messages.SIMULATION_STARTED);
	}

	@Override
	public String getInfo() {
		return "Start simulation";
	}
}
