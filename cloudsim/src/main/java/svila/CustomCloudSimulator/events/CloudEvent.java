package svila.CustomCloudSimulator.events;

import java.util.Comparator;

import svila.CustomCloudSimulator.CloudElement;
import svila.CustomCloudSimulator.CloudManager;
import svila.CustomCloudSimulator.CloudMessage;
import svila.CustomCloudSimulator.CloudMessage.Messages;

public abstract class CloudEvent {
	protected double startTime, duration;
	protected CloudElement ce1;
	protected CloudElement ce2;
	protected int id;
	
	public CloudEvent() {
		this.id = CloudManager.getInstance().generateEventId();
		this.startTime = -1;
	}
	
	public CloudEvent(CloudElement ce1) {
		this.id = CloudManager.getInstance().generateEventId();
		this.startTime = -1;
		this.ce1 = ce1;
	}
	
	public CloudEvent(double startTime, CloudElement ce1) {
		this.id = CloudManager.getInstance().generateEventId();
		this.startTime = startTime;
		this.ce1 = ce1;
	}
	
	public CloudEvent(double startTime, CloudElement ce1, CloudElement ce2) {
		this.id = CloudManager.getInstance().generateEventId();
		this.startTime = startTime;
		this.ce1 = ce1;
		this.ce2 = ce2;
	}
	
	public int getId() {
		return id;
	}
	
	public double getStartTime() {
		return this.startTime;
	}
	
	public void setDuration(double duration) {
		this.duration = duration;
	}
	
	public double getDuration() {
		return this.duration;
	}
	
	public double getFinishTime() {
		return getStartTime() + getDuration();
	}
	
	public abstract void execute();
	
	public String toString() {
		return "(" + id + ") " + getInfo();
	}
	
	public abstract String getInfo();
	
	public void sendToCloudManager() {
		CloudManager.getInstance().addEvent(this);
	}
	
	public void sendAndExecute() {
		CloudManager.getInstance().executeEvent(this);
	}
}
