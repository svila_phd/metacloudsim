package svila.CustomCloudSimulator.events;

import svila.CustomCloudSimulator.CloudElement;
import svila.CustomCloudSimulator.CloudLet;
import svila.CustomCloudSimulator.CloudVM;

public class SetCloudletToVM extends CloudEvent {

	CloudLet c;
	CloudVM vm;
	
	public SetCloudletToVM(double startTime, CloudLet c, CloudVM vm) {
		super(startTime, c, vm);
		this.c = c;
		this.vm = vm;
	}
	
	public SetCloudletToVM(CloudLet c, CloudVM vm) {
		super(c);
		this.c = c;
		this.vm = vm;
	}

	@Override
	public void execute() {
		vm.addCloudlet(c);
	}

	@Override
	public String getInfo() {
		return "Added " + c.getName() + " to " + vm.getName();
	}

}
