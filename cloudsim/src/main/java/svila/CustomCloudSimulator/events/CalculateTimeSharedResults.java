package svila.CustomCloudSimulator.events;

import java.util.HashMap;
import java.util.Map;

import svila.CustomCloudSimulator.CloudHost;
import svila.CustomCloudSimulator.CloudManager;
import svila.CustomCloudSimulator.CloudVM;
import svila.CustomCloudSimulator.PriorityQueueIterator;
import svila.CustomCloudSimulator.TimeSharedResults;

public class CalculateTimeSharedResults extends CloudEvent {

	TimeSharedResults results;
	
	public CalculateTimeSharedResults() {
		super();
		CloudManager cm = CloudManager.getInstance();
		results = new TimeSharedResults(cm.getCloudlets(), cm.getVMs(), cm.getHosts());
	}
	
	public CalculateTimeSharedResults(String path) {
		super();
		CloudManager cm = CloudManager.getInstance();
		results = new TimeSharedResults(cm.getCloudlets(), cm.getVMs(), cm.getHosts());
	}
	
	@Override
	public void execute() {
		Map<CloudHost, Double> hostTimes = new HashMap<>();
		Map<CloudVM, Double> VMTimes = new HashMap<>();
		PriorityQueueIterator<CloudHost> it = CloudManager.getInstance().getDatacenter().getHostIterator();
		while(it.hasNext()) {
			CloudHost ch = it.next();
			ch.recalculateVMs();
			hostTimes.put(ch, ch.getTimeSharedMakespan());
			//System.out.println(ch.getName() + " " + ch.getTimeSharedMakespan());
			PriorityQueueIterator<CloudVM> itVM = ch.getVMIterator();
			while(itVM.hasNext()) {
				CloudVM vm = itVM.next();
				VMTimes.put(vm, vm.getTimeSharedMakespan());
				//System.out.println(vm.getName() + " " + vm.getTimeSharedMakespan());
			}
		}
	}

	public TimeSharedResults getResults() {
		return results;
	}
	
	@Override
	public String getInfo() {
		return null;
	}
	
}
