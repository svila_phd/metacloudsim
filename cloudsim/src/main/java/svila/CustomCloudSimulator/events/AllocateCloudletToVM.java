package svila.CustomCloudSimulator.events;

import svila.CustomCloudSimulator.CloudLet;
import svila.CustomCloudSimulator.CloudVM;

public class AllocateCloudletToVM extends CloudEvent {

	CloudLet cl;
	CloudVM vm;
	
	public AllocateCloudletToVM(CloudLet cl, CloudVM vm) {
		this.cl = cl;
		this.vm = vm;
	}
	
	@Override
	public void execute() {
		vm.addAllocation(cl);
	}

	@Override
	public String getInfo() {
		return "Allocate " + cl + " to " + vm;
	}

}
