package svila.CustomCloudSimulator.events;

import svila.CustomCloudSimulator.CloudDatacenter;
import svila.CustomCloudSimulator.CloudHost;
import svila.CustomCloudSimulator.CloudManager;

public class CreateDatacenter extends CloudEvent {
	CloudDatacenter datacenter;
	
	public CreateDatacenter(CloudDatacenter cd) {
		super(cd);
		datacenter = cd;
	}
	
	public CreateDatacenter(double startTime, CloudDatacenter cd) {
		super(startTime, cd);
		datacenter = cd;
	}
	
	public void execute() {
		
	}
	
	public String getInfo() {
		return "";
	}
}
