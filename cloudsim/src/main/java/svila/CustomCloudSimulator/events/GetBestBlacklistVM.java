package svila.CustomCloudSimulator.events;

import java.util.ArrayList;
import java.util.List;

import svila.CustomCloudSimulator.CloudManager;
import svila.CustomCloudSimulator.CloudVM;
import svila.staticAllocation.schedulingTechniques.extra.CloudData;

public class GetBestBlacklistVM extends CloudEvent {

	public CloudData cd;
	public CloudVM bestVM;
	
	public GetBestBlacklistVM(CloudData cd) {
		this.cd = cd;
	}
	
	@Override
	public void execute() {
		List<CloudVM> vms = CloudManager.getInstance().getVMs();
		List<CloudVM> selectedVMs = new ArrayList<>();
		
		for(CloudVM cvm : vms) {
			//System.out.println("Cloudlet " + cd.cloudlet.getId() + " cores: " + cd.cloudlet.getRequiredCores() + " VM: " + cvm.getId() + " cores: " + cvm.getRequiredCores() + " forbidden: " + cd.forbiddenCores[cvm.getId()]);
			//System.out.println(cvm.getTotalCores() - (cvm.getTotalCores() * cd.forbiddenCores[cvm.getId()]));
			//System.out.println(cd.cloudlet.getRequiredCores() + " - " + "(" + cvm.getRequiredCores() + " - " + "(" + cvm.getRequiredCores() + " * " + cd.forbiddenCores[cvm.getId()] + "))");
			//System.out.println(cvm.canCloudletFitVMWithRestrictions(cd.cloudlet, cd.forbiddenCores[cvm.getId()]));
			if(cvm.canCloudletFitVMWithRestrictions(cd.cloudlet, cd.forbiddenCores[cvm.getId()])) {
				selectedVMs.add(cvm);
			}
		}
		
		CloudVM mostFreeVM = null;
		double bestFinishTime = Double.MAX_VALUE;
		//System.out.println("Cloudlet " + cd.cloudlet.getId() + " cores: " + cd.cloudlet.getRequiredCores() + " Size: " + selectedVMs.size());
		for(CloudVM cvm : selectedVMs) {
			double currentTime = cvm.getTimeSharedMakespan();
			//System.out.println("currentTime: " + currentTime);
			if(currentTime < bestFinishTime) {
				bestFinishTime = currentTime;
				mostFreeVM = cvm;
			}
		}
		
		bestVM = mostFreeVM;
	}

	@Override
	public String getInfo() {
		return "Get best VM";
	}
}
