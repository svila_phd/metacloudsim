package svila.CustomCloudSimulator.events;

import svila.CustomCloudSimulator.CloudManager;
import svila.CustomCloudSimulator.CloudMessage;

public class FinishSimulation extends CloudEvent {

	public FinishSimulation() {
		
	}
	
	@Override
	public void execute() {
		CloudManager.getInstance().setActive(false);
		CloudManager.getInstance().sendMessage(CloudMessage.Messages.SIMULATION_FINISHED);
	}

	@Override
	public String getInfo() {
		return "Finish simulation";
	}

}
