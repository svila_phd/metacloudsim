package svila.CustomCloudSimulator.events;

import svila.CustomCloudSimulator.CloudHost;
import svila.CustomCloudSimulator.CloudMessage;
import svila.CustomCloudSimulator.CloudVM;

public class SetVMToHost extends CloudEvent {
	
	CloudHost host;
	CloudVM vm;
	
	public SetVMToHost(double startTime, CloudVM vm, CloudHost host) {
		super(startTime, host, vm);
		this.host = host;
		this.vm = vm;
	}

	@Override
	public void execute() {
		host.addVM(vm);
	}

	@Override
	public String getInfo() {
		return "Add " + vm.getName() + " to " + host.getName();
	}

}
