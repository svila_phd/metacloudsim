package svila.CustomCloudSimulator.events;

import svila.CustomCloudSimulator.CloudElement;
import svila.CustomCloudSimulator.CloudLet;

public class RunCloudlet extends CloudEvent {

	CloudLet cloudlet;
	
	public RunCloudlet(double startTime, CloudLet cloudlet) {
		super(startTime, cloudlet);
	}
	
	public void execute() {
		
	}

	@Override
	public String getInfo() {
		return "Run cloudlet";
	}

}
