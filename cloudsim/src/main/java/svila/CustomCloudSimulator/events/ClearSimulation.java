package svila.CustomCloudSimulator.events;

import svila.CustomCloudSimulator.CloudManager;
import svila.CustomCloudSimulator.CloudMessage;

public class ClearSimulation extends CloudEvent {

	public ClearSimulation() {
		
	}
	
	@Override
	public void execute() {
		CloudManager.getInstance().reset();
		CloudManager.getInstance().sendMessage(CloudMessage.Messages.SIMULATION_CLEARED);
	}

	@Override
	public String getInfo() {
		return "Simulation clear";
	}

}
