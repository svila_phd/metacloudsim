package svila.CustomCloudSimulator.events;

import svila.CustomCloudSimulator.CloudElement;
import svila.CustomCloudSimulator.CloudHost;
import svila.CustomCloudSimulator.CloudManager;

public class CreateHost extends CloudEvent {
	CloudHost host;
	
	public CreateHost(CloudHost ch) {
		super(ch);
		host = ch;
	}
	
	public CreateHost(double startTime, CloudHost ce1) {
		super(startTime, ce1);
		host = ce1;
	}
	
	public void execute() {
		CloudManager.getInstance().getDatacenter().addHost(host);
	}
	
	public String getInfo() {
		return "Create " + host.getName();
	}
}
