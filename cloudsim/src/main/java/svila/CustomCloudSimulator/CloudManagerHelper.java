package svila.CustomCloudSimulator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

import svila.CustomCloudSimulator.events.CreateHost;
import svila.CustomCloudSimulator.events.SetVMToHost;

public class CloudManagerHelper {
	
	private static Map<Integer, CloudHost> cloudHostMap;
	
	public static void addHosts(List<Host> hosts) {
		cloudHostMap = new HashMap<>();
		for(Host h : hosts) {
			CloudHost ch = convertToCloudHost(h);
			new CreateHost(ch).sendToCloudManager();
			cloudHostMap.put(ch.id, ch);
		}
	}
	
	public static CloudHost convertToCloudHost(Host host) {
		CloudHost ch = CloudManager.getInstance().createHost(host.getTotalMips(), host.getNumberOfPes());
		ch.setEnergyIdle(host.getEnergyIdle());
		ch.setEnergyRunning(host.getEnergyFull());
		ch.id = host.getId();
		return ch;
	}
	
	public static CloudVM convertToCloudVM(Vm vm) {
		CloudVM cvm = CloudManager.getInstance().createVM(vm.getMips(), vm.getNumberOfPes());
		cvm.id = vm.getId();
		return cvm;
	}
	
	public static CloudLet convertToCloudLet(Cloudlet c) {
		CloudLet cl = CloudManager.getInstance().createCloudlet(c.getCloudletLength(), c.getNumberOfPes());
		cl.id = c.getCloudletId();
		return cl;
	}
	
	public static List<CloudLet> convertToCloudLets(List<Cloudlet> cloudletList) {
		List<CloudLet> cloudLets = new ArrayList<>();
		for(Cloudlet c : cloudletList) {
			CloudLet cl = CloudManager.getInstance().createCloudlet(c.getCloudletLength(), c.getNumberOfPes());
			cl.id = c.getCloudletId();
			cloudLets.add(cl);
		}
		
		return cloudLets;
	}
	
	public static void mapVMsToHosts(Map<Vm, Host> map) {
		List<Entry<Vm, Host>> entries = new ArrayList<>();
		for(Entry<Vm, Host> entry : map.entrySet()) {
			entries.add(entry);
		}
		
		// Es necessita ordenar les VMs per a que coincideixi l'index
		Collections.sort(entries, new Comparator<Entry<Vm, Host>>(){
		     public int compare(Entry<Vm, Host> o1, Entry<Vm, Host> o2){
		         if(o1.getKey().getId() == o2.getKey().getId())
		             return 0;
		         return o1.getKey().getId() < o2.getKey().getId() ? -1 : 1;
		     }
		});
		
		for(Entry<Vm, Host> entry : entries) {
			Vm vm = entry.getKey();
			Host host = entry.getValue();
			CloudVM cvm = convertToCloudVM(vm);
			//System.out.println(cvm.getId());
			new SetVMToHost(-1, cvm, cloudHostMap.get(host.getId())).sendToCloudManager();
		}
	}
}
