package svila.CustomCloudSimulator;

import java.util.Map;

public class CloudLetAllocation {
	double startTime;
	CloudLet cloudlet;
	Map<CloudLetTask, CloudCore> tasksToCoresMap;
	
	CloudLetAllocation previous;
	CloudLetAllocation next;
	
	public CloudLetAllocation(double startTime, CloudLet cloudlet) {
		this.startTime = startTime;
		this.cloudlet = cloudlet;
	}
	
	public void addTaskToCore(CloudLetTask cloudletTask, CloudCore core) {
		tasksToCoresMap.put(cloudletTask, core);
	}

	public double getStartTime() {
		return startTime;
	}

	public void setStartTime(double startTime) {
		this.startTime = startTime;
	}

	public CloudLet getCloudlet() {
		return cloudlet;
	}

	public void setCloudlet(CloudLet cloudlet) {
		this.cloudlet = cloudlet;
	}

	public Map<CloudLetTask, CloudCore> getTasksToCoresMap() {
		return tasksToCoresMap;
	}

	public void setTasksToCoresMap(Map<CloudLetTask, CloudCore> tasksToCoresMap) {
		this.tasksToCoresMap = tasksToCoresMap;
	}
	
	public void execute() {
		
	}
	
	public void redo() {
		
	}
}
