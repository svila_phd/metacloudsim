package svila.CustomCloudSimulator;

import java.util.Comparator;

import svila.CustomCloudSimulator.events.CloudEvent;

public class CloudEventComparator implements Comparator<CloudEvent> {

	@Override
	public int compare(CloudEvent o1, CloudEvent o2) {
		if(o1.getFinishTime() == o2.getFinishTime()) {
			if(o1.getId() > o2.getId()) {
				return 1;
			} else if(o1.getId() < o2.getId()) {
				return -1;
			}
			return 0;
		}
		
		if(o1.getFinishTime() > o2.getFinishTime()) {
			return 1;
		} else if(o1.getFinishTime() < o2.getFinishTime()) {
			return -1;
		}
		return 0;
	}


}
