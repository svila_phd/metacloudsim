package svila.CustomCloudSimulator;

import java.util.Comparator;

public class CloudHostComparator implements Comparator<CloudHost> {

	@Override
	public int compare(CloudHost c1, CloudHost c2) {
		if(c1.getLastTick() > c2.getLastTick()) {
			return 1;
		} else if(c1.getLastTick() < c2.getLastTick()) {
			return -1;
		}
		return 0;
	}
}
