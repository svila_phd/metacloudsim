package svila.CustomCloudSimulator;

import svila.CustomCloudSimulator.events.AllocateCloudletToVM;
import svila.CustomCloudSimulator.events.CalculateTimeSharedResults;
import svila.CustomCloudSimulator.events.CreateHost;
import svila.CustomCloudSimulator.events.FinishSimulation;
import svila.CustomCloudSimulator.events.SetCloudletToVM;
import svila.CustomCloudSimulator.events.SetVMToHost;
import svila.CustomCloudSimulator.events.StartSimulation;

public class CloudMain {
	public static void main(String[] args) {
		example1();
		
		//example2();
	}
	
	
	
	public static void example1() {
		CloudManager cm = new CloudManager();
		cm.setVerbose(false);
		cm.setStopIfError(false);
		
		new StartSimulation().sendToCloudManager();
		
		CloudHost ch1 = cm.createHost(1000, 8);
		CloudHost ch2 = cm.createHost(2000, 2);
		
		ch1.setEnergyIdle(7);
		ch1.setEnergyRunning(10);
		ch2.setEnergyIdle(3);
		ch2.setEnergyRunning(5);

		new CreateHost(ch1).sendToCloudManager();
		new CreateHost(ch2).sendToCloudManager();

		CloudVM vm0 = cm.createVM(1000, 4);
		CloudVM vm1 = cm.createVM(1000, 4);
		CloudVM vm2 = cm.createVM(2000, 2);
		new SetVMToHost(-1, vm0, ch1).sendToCloudManager();
		new SetVMToHost(-1, vm1, ch1).sendToCloudManager();
		new SetVMToHost(-1, vm2, ch2).sendToCloudManager();
		
		CloudLet c1 = cm.createCloudlet(10000, 4);
		CloudLet c2 = cm.createCloudlet(5000, 2);
		CloudLet c3 = cm.createCloudlet(4000, 4);
		CloudLet c4 = cm.createCloudlet(20000, 4);
		CloudLet c5 = cm.createCloudlet(100000, 2);
				
		new SetCloudletToVM(c1, vm0).sendToCloudManager();
		new SetCloudletToVM(c2, vm0).sendToCloudManager();
		new SetCloudletToVM(c3, vm1).sendToCloudManager();
		new SetCloudletToVM(c4, vm1).sendToCloudManager();
		new SetCloudletToVM(c5, vm2).sendToCloudManager();

		CalculateTimeSharedResults ctsr = new CalculateTimeSharedResults();
		ctsr.getResults().setExport(false);
		ctsr.getResults().generateResults();
		//ctsr.sendToCloudManager();
		//new SetCloudletToVM
		//new SetCloudletToHost
		//new SetCloudletToAvailableHost
		
		
		//cm.showPendingEvents();
		/*cm.showNextEvent();
		cm.executeNextEvent();
		cm.showNextEvent();
		cm.executeNextEvent();
		cm.showNextEvent();
		cm.executeNextEvent();
		cm.showNextEvent();
		cm.executeNextEvent();
		cm.showNextEvent();
		*/
		
		new FinishSimulation().sendToCloudManager();
		
		cm.executeAllEvents();
		
		//cm.executeNextEvent();
		
		//cm.showPendingEvents();

		cm.showLogs();
		
		//ctsr.getResults().exportJSON("C:\\Users\\Sergi\\Documents\\test.json");
		ctsr.getResults().showJSON();
		cm.showErrorLogs();
	}

	public static void example2() {
		CloudManager cm = new CloudManager();
		cm.setVerbose(false);
		cm.setStopIfError(false);
		
		new StartSimulation().sendToCloudManager();
		
		CloudHost ch0 = cm.createHost(1000, 8);
		ch0.setEnergyIdle(7);
		ch0.setEnergyRunning(10);
		
		new CreateHost(ch0).sendToCloudManager();
		
		CloudVM vm0 = cm.createVM(1000, 4);
		new SetVMToHost(-1, vm0, ch0).sendToCloudManager();
		CloudLet c0 = cm.createCloudlet(10000, 4);
		new AllocateCloudletToVM(c0, vm0).sendToCloudManager();

		new FinishSimulation().sendToCloudManager();
	}
}
