package svila.CustomCloudSimulator;

import java.util.Comparator;

public class CloudVMComparator implements Comparator<CloudVM> {

	@Override
	public int compare(CloudVM c1, CloudVM c2) {
		if(c1.getLastTick() > c2.getLastTick()) {
			return 1;
		} else if(c1.getLastTick() < c2.getLastTick()) {
			return -1;
		}
		return 0;
	}
}
