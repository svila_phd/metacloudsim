package svila.CustomCloudSimulator;

import java.util.ArrayList;
import java.util.List;

public class CloudCore {
	int num;
	double MIPS;
	CloudHost host;
	CloudVM vm;
	List<CloudLetTask> cloudletTaskList;
	double lastTick;
	
	public CloudCore(CloudHost host, int num, double MIPS) {
		this.host = host;
		this.num = num;
		this.MIPS = MIPS;
		this.cloudletTaskList = new ArrayList<>();
	}
	
	public int getNum() {
		return num;
	}
	
	public double getMIPS() {
		return MIPS;
	}

	public CloudHost getHost() {
		return host;
	}

	public CloudVM getVm() {
		return vm;
	}

	public void setVm(CloudVM vm) {
		this.vm = vm;
	}
	
	public void addCloudLetTask(CloudLetTask cloudletTask) {
		cloudletTaskList.add(cloudletTask);
		lastTick += cloudletTask.getTotalMIPS()/MIPS;
	}
	
	public double getLastTick() {
		return this.lastTick;
	}
}
