package svila.CustomCloudSimulator;

import java.util.ArrayList;
import java.util.List;

public class CloudMessageManager {
	
	boolean verbose;
	int messageIndex;
	List<String> logs;
	List<String> errorLogs;
	CloudManager cm;
	
	public CloudMessageManager(CloudManager cm) {
		this.cm = cm;
		this.logs = new ArrayList<>();
		this.errorLogs = new ArrayList<>();
		this.messageIndex = 0;
	}
	
	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}
	
	public void sendMessage(CloudMessage.Messages message) {
		sendMessage(null, null, message);
	}
	
	public void sendMessage(CloudElement ce1, CloudMessage.Messages message) {
		sendMessage(ce1, null, message);
	}
	
	public void sendMessage(CloudElement ce1, CloudElement ce2, CloudMessage.Messages message) {
		String log = "#" + messageIndex + " " + getFormattedTime() + " ";
		messageIndex++;
		boolean isError = false;
		switch(message) {
		case HOST_CREATED:
			log += ce1.getName() + " " + "created ";
			break;
		case NOT_ENOUGH_MIPS:
			log += ce1.getName() + " (free MIPS: " + ce1.getFreeCPU() + ")" + " can't allocate " + ce2.getName() + " (required MIPS: " + ce2.getRequiredCPU() + ")";
			isError = true;
			break;
		case NOT_ENOUGH_CORES:
			log += ce1.getName() + " (free cores: " + ce1.getFreeCores() + ")" + " can't allocate " + ce2.getName() + " (required cores: " + ce2.getRequiredCores() + ")";
			isError = true;
			break;
		case VM_ALLOCATION_TO_HOST:
			log += ce1.getName() + " allocated to " + ce2.getName();
			break;
		case TRY_ALLOCATE_VM:
			log += "Try to allocate " + ce1.getName() + " into " + ce2.getName();
			break;
		case TRY_ALLOCATE_HOST:
			log += "Try to allocate " + ce1.getName() + " into datacenter";
			break;
		case TRY_ALLOCATE_CLOUDLET:
			log += "Try to allocate " + ce1.getName() + " into " + ce2.getName();
			break;
		case CLOUDLET_ALLOCATION_TO_VM:
			log += ce1.getName() + " allocated to " + ce2.getName();
			break;
		case SIMULATION_FINISHED:
			log += "Simulation finished";
			break;
		case SIMULATION_CLEARED:
			log += "Simulation cleared";
			break;
		case SIMULATION_STARTED:
			log += "Simulation started";
			break;
		default:
			log += "Message not found";
			isError = true;
			break;
		}
		
		addLog(log, isError);
	}
	
	private void addLog(String log, boolean isError) {
		logs.add(log);
		
		if(isError) {
			errorLogs.add(log);
		}
		
		if(verbose) {
			if(isError) {
				System.err.println(log);
			} else {
				System.out.println(log);
			}
		}
		
	}
	
	public String getFormattedTime() {
		return String.format("[%.2f]", cm.getTime());
	}
	
	public void showLogs() {
		System.out.println("\n\n-------- LOGS --------");
		for(String log : this.logs) {
			System.out.println(log);
		}
		System.out.println("----------------------\n");
	}
	
	public void showErrorLogs() {
		System.err.println("\n\n----- ERROR LOGS -----");
		for(String log : this.errorLogs) {
			System.err.println(log);
		}
		System.err.println("----------------------\n");
	}
	
	public void showMessage(String message) {
		if(verbose) {
			System.out.println(message);
		}
	}
}
