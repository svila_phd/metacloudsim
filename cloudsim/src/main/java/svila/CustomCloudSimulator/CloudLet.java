package svila.CustomCloudSimulator;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class CloudLet extends CloudElement {
	
	PriorityQueue<CloudLetTask> tasksQueue;
	CloudVM assignedVM;

	public CloudLet(int id, double totalCPU, int totalCores) {
		super(id, totalCPU, totalCores);
		this.totalCPU = totalCPU;
		tasksQueue = new PriorityQueue<>(4, new CloudLetTaskComparator());
		for(int i=0; i<totalCores; i++) {
			tasksQueue.add(new CloudLetTask(this, i, totalCPU));
		}
	}
	
	public CloudLet(CloudLet c) {
		super(c.id, c.totalCPU, c.totalCores);
		this.totalCPU = c.totalCPU;
		tasksQueue = new PriorityQueue<>(4, new CloudLetTaskComparator());
		for(int i=0; i<totalCores; i++) {
			tasksQueue.add(new CloudLetTask(this, i, totalCPU));
		}
	}
	
	public void setAssignedVM(CloudVM assignedVM) {
		this.assignedVM = assignedVM;
	}
	
	public PriorityQueue<CloudLetTask> getTasks() {
		return tasksQueue;
	}
	
	public String getType() {
		return "Cloudlet";
	}

	@Override
	public List<String> display() {
		List<String> info = new ArrayList<>();
		info.add(this.getName() + ": Length: " + this.getTotalCPU() + " Cores: " + tasksQueue.size());
		return info;
	}

}
