package svila.CustomCloudSimulator;

public class CloudLetTask {
	CloudLet cloudlet;
	int id;
	double totalMIPS, pendingMIPS;
	boolean completed;
	
	public CloudLetTask(CloudLet cloudlet, int id, double totalMIPS) {
		this.cloudlet = cloudlet;
		this.id = id;
		this.totalMIPS = totalMIPS;
		this.pendingMIPS = totalMIPS;
		this.completed = false;
	}
	
	public double getTotalMIPS() {
		return totalMIPS;
	}
	
	public double getPendingMIPS() {
		return pendingMIPS;
	}
	
	public void calculeMIPS(double MIPS) {
		pendingMIPS = pendingMIPS - MIPS;
		
		if(pendingMIPS <= 0.0) {
			completed = true;
		}
	}
	
	public boolean isCompleted() {
		return completed;
	}
}
