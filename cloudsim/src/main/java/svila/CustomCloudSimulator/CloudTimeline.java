package svila.CustomCloudSimulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.TreeMap;

import svila.CustomCloudSimulator.events.CloudEvent;

public class CloudTimeline implements Iterable {
	// IMPLEMENTAR ITERADOR
	
	PriorityQueue<CloudEvent> eventQueue;
	
	
	public CloudTimeline() {
		this.eventQueue = new PriorityQueue<CloudEvent>(11, new CloudEventComparator());
	}
	
	public int getNumPendingElements() {
		return eventQueue.size();
	}
	
	public void addEvent(CloudEvent ce) {
		eventQueue.add(ce);
		/*List<CloudEvent> tempList = null;
		double key = ce.getStartTime();
		if (events.containsKey(key)) {
		   tempList = events.get(key);
		   if(tempList == null)
		      tempList = new ArrayList();
		      tempList.add(ce);
		} else {
		   tempList = new ArrayList();
		   tempList.add(ce);
		}
		events.put(key,tempList);
		*/
	}

	@Override
	public Iterator<CloudEvent> iterator() {
		return eventQueue.iterator();
	}
		   
}
