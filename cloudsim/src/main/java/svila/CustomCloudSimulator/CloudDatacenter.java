package svila.CustomCloudSimulator;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class CloudDatacenter extends CloudElement {
	
	PriorityQueue<CloudHost> hostQueue;
	
	public CloudDatacenter(int id) {
		super(id, Double.MAX_VALUE, Integer.MAX_VALUE);
		this.totalCPU = Double.MAX_VALUE;
		this.totalCores = Integer.MAX_VALUE;
		hostQueue = new PriorityQueue<CloudHost>(11, new CloudHostComparator());
	}
	
	public PriorityQueueIterator<CloudHost> getHostIterator() {
		return new PriorityQueueIterator<CloudHost>(hostQueue);
	}
	
	public boolean addHost(CloudHost cloudHost) {
		CloudManager.getInstance().sendMessage(cloudHost, CloudMessage.Messages.TRY_ALLOCATE_HOST);
		if(cloudHost.requiredCores < this.getTotalCores()) {
			if(cloudHost.requiredCores * cloudHost.getRequiredCPU() <
			   this.getTotalCores() * this.getTotalCPU()) {
				hostQueue.add(cloudHost);
				CloudManager.getInstance().sendMessage(cloudHost, CloudMessage.Messages.HOST_CREATED);
				return true;
			}
			CloudManager.getInstance().sendMessage(this, cloudHost, CloudMessage.Messages.NOT_ENOUGH_CORES);
			return false;
		}
		CloudManager.getInstance().sendMessage(this, cloudHost, CloudMessage.Messages.NOT_ENOUGH_MIPS);
		return false;
	}
	
	public String getType() {
		return "Datacenter";
	}
	
	public List<String> display() {
		List<String> info = new ArrayList<>();
		info.add(getName() + "has " + hostQueue.size());
		return info;
	}
}
