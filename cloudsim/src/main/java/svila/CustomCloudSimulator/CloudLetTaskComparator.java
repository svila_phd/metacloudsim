package svila.CustomCloudSimulator;

import java.util.Comparator;

import svila.CustomCloudSimulator.events.CloudEvent;

public class CloudLetTaskComparator implements Comparator<CloudLetTask> {

	@Override
	public int compare(CloudLetTask o1, CloudLetTask o2) {
		if(o1.getPendingMIPS() > o2.getPendingMIPS()) {
			return 1;
		} else if(o1.getPendingMIPS() < o2.getPendingMIPS()) {
			return -1;
		}
		return 0;
	}
}
