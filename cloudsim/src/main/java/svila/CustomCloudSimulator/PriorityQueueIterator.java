package svila.CustomCloudSimulator;

import java.util.Iterator;
import java.util.PriorityQueue;

public class PriorityQueueIterator<T> implements Iterator<T> {
	final PriorityQueue<T> pq;
	
	public PriorityQueueIterator(PriorityQueue <T> source) {
		pq = new PriorityQueue<T>(source);
	}
	
	@Override
	public boolean hasNext() {
		return pq.peek() != null;
	}
	
	@Override
	public T next() {
		return pq.poll();
	}
	
	@Override
	public void remove() {
		throw new UnsupportedOperationException("");
	}
}
