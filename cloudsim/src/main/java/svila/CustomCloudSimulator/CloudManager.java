package svila.CustomCloudSimulator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import svila.CustomCloudSimulator.events.CloudEvent;

public class CloudManager {
	
	CloudTimeline pendingEvents;
	CloudTimeline timelineStart;
	CloudTimeline timelineEnd;
	double time;
	private CloudDatacenter mainElement;
	int cloudletIndex, vmIndex, hostIndex, eventIndex;
	List<CloudHost> hosts;
	List<CloudVM> vms;
	List<CloudLet> cloudlets;
	boolean isActive, stopIfError;
	List<String> logs;
	List<String> errorLogs;
	private CloudMessageManager cloudMessageManager;
	
	private static CloudManager singleton;
	
	public static CloudManager getInstance() {
		if(singleton == null) {
			new CloudManager();
		}
		return singleton;
	}
	
	public CloudManager() {
		singleton = this;
		init();
	}
	
	public void init() {
		cloudletIndex = 0; vmIndex = 0; hostIndex = 0;
		pendingEvents = new CloudTimeline();
		timelineStart = new CloudTimeline();
		timelineEnd = new CloudTimeline();
		this.mainElement = new CloudDatacenter(0);
		this.hosts = new ArrayList<>();
		this.vms = new ArrayList<>();
		this.cloudlets = new ArrayList<>();
		this.isActive = true;
		this.stopIfError = true;
		this.logs = new ArrayList<>();
		this.errorLogs = new ArrayList<>();
		cloudMessageManager = new CloudMessageManager(this);
	}
	
	public void reset() {
		init();
	}
	
	public void clearCloudlets() {
		for(CloudVM cvm : vms) {
			cvm.clearCloudlets();
		}
	}
	
	public void setVerbose(boolean verbose) {
		cloudMessageManager.setVerbose(verbose);
	}
	
	public void setStopIfError(boolean stopIfError) {
		this.stopIfError = stopIfError;
	}
	
	/*public boolean addCloudElementToAnotherCloudElement(
											CloudElement ceLittle,
											CloudElement ceBig) {
		ceBig.addCloudElement(ceLittle);
	}*/
	
	public void addEvent(CloudEvent cloudEvent) {
		pendingEvents.addEvent(cloudEvent);
	}
	
	public boolean executeNextEvent() {
		CloudEvent nextCloudEvent = pendingEvents.eventQueue.poll();
		if(nextCloudEvent == null) {
			return false;
		}
		executeEvent(nextCloudEvent);
		return true;
	}
	
	public void showPendingEvents() {
		System.out.println("Pending events: " + pendingEvents.getNumPendingElements());
		PriorityQueueIterator<CloudEvent> it = new PriorityQueueIterator<>(pendingEvents.eventQueue);
		int i = 1;
		while(it.hasNext()) {
			CloudEvent nextCloudEvent = it.next();
			System.out.println(i + ": " + nextCloudEvent.getInfo() + " [" + nextCloudEvent.getId() + "]");
			i++;
		}
	}
	
	public void showNextEvent() {
		CloudEvent ce = pendingEvents.eventQueue.peek();
		if(ce!=null) {
			System.out.println(ce + " [" + ce.getId() + "]");
		} else {
			System.out.println("No more events");
		}
		
	}
	
	public void executeEvent(CloudEvent nextCloudEvent) {
		timelineStart.addEvent(nextCloudEvent);
		timelineEnd.addEvent(nextCloudEvent);
		nextCloudEvent.execute();
		time += nextCloudEvent.getDuration();
	}
	
	public void executeAllEvents() {
		while(executeNextEvent()) {
			
		}
	}
	
	public CloudDatacenter getDatacenter() {
		return this.mainElement;
	}

	public CloudHost createHost(double totalCPU, int totalCores) {
		CloudHost ch = new CloudHost(hostIndex, totalCPU, totalCores);
		hostIndex++;
		hosts.add(ch);
		return ch;
	}
	
	public CloudVM createVM(double totalCPU, int totalCores) {
		CloudVM cvm = new CloudVM(vmIndex, totalCPU, totalCores);
		vmIndex++;
		vms.add(cvm);
		return cvm;
	}
	
	public CloudLet createCloudlet(double totalCPU, int totalCores) {
		CloudLet cl = new CloudLet(cloudletIndex, totalCPU, totalCores);
		cloudletIndex++;
		cloudlets.add(cl);
		return cl;
	}
	
	public List<CloudHost> getHosts() {
		return hosts;
	}
	
	public List<CloudVM> getVMs() {
		return vms;
	}
	
	public List<CloudLet> getCloudlets() {
		return cloudlets;
	}
	
	public int generateEventId() {
		int id = eventIndex;
		eventIndex++;
		return id;
	}
	
	public void sendMessage(CloudMessage.Messages message) {
		sendMessage(null, null, message);
	}
	
	public void sendMessage(CloudElement ce1, CloudMessage.Messages message) {
		sendMessage(ce1, null, message);
	}
	
	public void sendMessage(CloudElement ce1, CloudElement ce2, CloudMessage.Messages message) {
		cloudMessageManager.sendMessage(ce1, ce2, message);
	}

	public void showLogs() {
		cloudMessageManager.showLogs();
	}
	
	public void showErrorLogs() {
		cloudMessageManager.showErrorLogs();
	}
	
	public boolean executionWithErrors() {
		return !cloudMessageManager.errorLogs.isEmpty();
	}
	
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	public String getName() {
		return "Manager";
	}

	public double getTime() {
		return time;
	}
	
	public String getFormattedTime() {
		return String.format("[%.2f]", getTime());
	}
	
	public void showMessage(String message) {
		cloudMessageManager.showMessage(message);
	}
}
