package svila.CustomCloudSimulator;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class CloudHost extends CloudElement {
	
	PriorityQueue<CloudCore> coreQueue;
	PriorityQueue<CloudVM> VMQueue;
	double energyRunning, energyIdle;

	public CloudHost(int id, double totalCPU, int totalCores) {
		super(id, totalCPU, totalCores);
		this.totalCPU = totalCPU;
		this.totalCores = totalCores;
		this.coreQueue = new PriorityQueue<>(11, new CloudCoreComparator());
		for(int i=0; i<totalCores; i++) {
			coreQueue.add(new CloudCore(this, i, totalCPU));
		}
		this.VMQueue = new PriorityQueue<>(11, new CloudVMComparator());
		this.energyRunning = 0.0;
		this.energyIdle = 0.0;
	}
	
	public boolean addVM(CloudVM cloudVM) {
		CloudManager.getInstance().sendMessage(cloudVM, this, CloudMessage.Messages.TRY_ALLOCATE_VM);
		if(cloudVM.getRequiredCPU() <= this.getTotalCPU()) {
			if(cloudVM.getRequiredCores() <= this.getFreeCores()) {
				this.usedCores += cloudVM.getRequiredCores();
				VMQueue.add(cloudVM);
				
				cloudVM.setAssignedHost(this, getRequiredCores(cloudVM.getRequiredCores()));
				cloudVM.setTotalCPU(cloudVM.getRequiredCPU());
				cloudVM.setTotalCores(cloudVM.getRequiredCores());
				cloudVM.setUsedCores(cloudVM.getRequiredCores());
				CloudManager.getInstance().sendMessage(cloudVM, this, CloudMessage.Messages.VM_ALLOCATION_TO_HOST);
				return true;
			}
			CloudManager.getInstance().sendMessage(cloudVM, this, CloudMessage.Messages.NOT_ENOUGH_CORES);
			return false;
		}
		CloudManager.getInstance().sendMessage(cloudVM, this, CloudMessage.Messages.NOT_ENOUGH_MIPS);
		return false;
	}
	
	private List<CloudCore> getRequiredCores(int requiredCores) {
		List<CloudCore> cores = new ArrayList<>();
		PriorityQueueIterator<CloudCore> coreIt = getCoreIterator();
		int addedCores = 0;
		while(coreIt.hasNext()) {
			CloudCore core = coreIt.next();
			if(addedCores < requiredCores && core.getVm() == null) {
				cores.add(core);
				addedCores++;
			}
		}
		
		if(addedCores != requiredCores) {
			System.err.println("Error: not enough cores (" + requiredCores + ")");
			System.exit(-1);
		}
		
		return cores;
	}
	
	public PriorityQueueIterator<CloudVM> getVMIterator() {
		return new PriorityQueueIterator<CloudVM>(VMQueue);
	}
	
	public PriorityQueueIterator<CloudCore> getCoreIterator() {
		return new PriorityQueueIterator<CloudCore>(coreQueue);
	}
	
	public double getTimeSharedMakespan() {
		PriorityQueueIterator<CloudVM> it = getVMIterator();
		CloudVM lastVM = null;
		while(it.hasNext()) {
			lastVM = it.next();
		}
		
		if(lastVM == null) {
			return 0.0;
		}
		
		return lastVM.getTimeSharedMakespan();
	}
	
	public void recalculateVMs() {
		PriorityQueue<CloudVM> temp = new PriorityQueue<>(11, new CloudVMComparator());
		
		while(!VMQueue.isEmpty()) {
			temp.add(VMQueue.poll());
		}
		
		VMQueue = temp;
	}
	
	public CloudVM getLessOccupiedVM() {
		return VMQueue.peek();
	}
	
	public void setEnergyRunning(double energyRunning) {
		this.energyRunning = energyRunning;
	}
	
	public void setEnergyIdle(double energyIdle) {
		this.energyIdle = energyIdle;
	}
	
	public double getEnergyRunning() {
		return this.energyRunning;
	}
	
	public double getEnergyIdle() {
		return this.energyIdle;
	}
	
	public double getLastTick() {
		if(VMQueue.isEmpty()) {
			return 0.0;
		}
		return VMQueue.peek().getLastTick();
	}

	public PriorityQueue<CloudVM> getVMs() {
		return VMQueue;
	}
	
	public List<CloudVM> getFreeVMs() {
		return null;
	}
	
	public List<CloudVM> getPartiallyFreeHosts() {
		return null;
	}
	
	public List<CloudVM> getHostsWithAtLeast(double CPU, int cores) {
		return null;
	}
	
	public String getType() {
		return "Host";
	}



	@Override
	public List<String> display() {
		List<String> info = new ArrayList<>();
		info.add(this.getName() + " has " + VMQueue.size() + " VMs");
		return info;
	}
}
