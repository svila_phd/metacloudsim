package svila.CustomCloudSimulator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import svila.CustomCloudSimulator.events.CloudEvent;

public abstract class CloudElement {
	
	protected int id;
	protected double totalCPU, usedCPU, requiredCPU;
	protected int totalCores, usedCores, requiredCores;
	protected Map<Integer, CloudElement> cloudElementMap;
	protected Map<Integer, CloudElement> pendingCloudElements;
	protected Map<Integer, CloudElement> completedCloudElements;
	protected Map<Integer, CloudElement> inProgressCloudElements;
	
	
	public CloudElement(int id, double requiredCPU, int requiredCores) {
		this.id = id;
		this.requiredCPU = requiredCPU;
		this.requiredCores = requiredCores;
		this.cloudElementMap = new HashMap<>();
		this.pendingCloudElements = new HashMap<>();
		this.inProgressCloudElements = new HashMap<>();
	}
	
	public int getId() {
		return id;
	}
	
	public double getRequiredCPU() {
		return requiredCPU;
	}
	
	public double getTotalCPU() {
		return totalCPU;
	}
	
	public double getUsedCPU() {
		return usedCPU;
	}
	
	public double getFreeCPU() {
		return totalCPU - usedCPU;
	}
	
	public int getRequiredCores() {
		return requiredCores;
	}
	
	public int getTotalCores() {
		return totalCores;
	}
	
	public int getUsedCores() {
		return usedCores;
	}
	
	public int getFreeCores() {
		return totalCores - usedCores;
	}
	
	public void setTotalCores(int totalCores) {
		this.totalCores = totalCores;
	}
	
	public void setUsedCores(int usedCores) {
		this.usedCores = usedCores;
	}
	
	public void setTotalCPU(double CPU) {
		this.totalCPU = CPU;
	}
	
	public boolean canFitCloudElement(CloudElement ce) {
		return this.getFreeCores() >= ce.getTotalCores();
	}
	
	public void addCloudElement(CloudElement ce) {
		this.cloudElementMap.put(ce.getId(), ce);
	}
	
	public boolean removeCloudElement(CloudElement ce) {
		return removeCloudElement(ce.getId());
	}
	
	public boolean removeCloudElement(int id) {
		return this.cloudElementMap.remove(id) != null;
	}
	
	public CloudEvent runOnce() {
		return null;
	}
	
	public CloudEvent run() {
		return null;
	}
	
	public String getName() {
		return getType() + " " + getId();
	}
	
	public abstract String getType();
	
	public abstract List<String> display();
	public void show() {
		for(String line : display()) {
			System.out.println(line);
		}
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
