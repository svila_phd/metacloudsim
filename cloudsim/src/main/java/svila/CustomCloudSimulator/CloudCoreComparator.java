package svila.CustomCloudSimulator;

import java.util.Comparator;

public class CloudCoreComparator implements Comparator<CloudCore> {

	@Override
	public int compare(CloudCore o1, CloudCore o2) {
		if(o1.getLastTick() > o2.getLastTick()) {
			return 1;
		} else if(o1.getLastTick() < o2.getLastTick()) {
			return -1;
		}
		return 0;
	}

}
