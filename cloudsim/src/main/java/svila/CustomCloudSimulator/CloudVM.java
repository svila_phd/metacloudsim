package svila.CustomCloudSimulator;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class CloudVM extends CloudElement {
	
	PriorityQueue<CloudCore> coreQueue;
	List<CloudLet> cloudletList;
	List<CloudLet> pendingCloudlets;
	List<CloudLet> completedCloudlets;
	PriorityQueue<CloudLetAllocation> scheduledAllocations;
	PriorityQueue<CloudLetFreeAllocation> freeAllocations;
	CloudHost assignedHost;
	double cachedTimeSharedMakespan;
	boolean cached;

	public CloudVM(int id, double totalCPU, int totalCores) {
		super(id, totalCPU, totalCores);
		coreQueue = new PriorityQueue<>(11, new CloudCoreComparator());
		cloudletList = new ArrayList<>();
		pendingCloudlets = new ArrayList<>();
		completedCloudlets = new ArrayList<>();
		scheduledAllocations = new PriorityQueue<>(11, new CloudLetAllocationComparator());
		this.cached = false;
	}

	public void addCloudlet(CloudLet cloudlet) {
		CloudManager.getInstance().sendMessage(cloudlet, this, CloudMessage.Messages.TRY_ALLOCATE_CLOUDLET);
		if(cloudlet.getRequiredCores() <= this.getRequiredCores()) {
			cloudletList.add(cloudlet);
			pendingCloudlets.add(cloudlet);
			cached = false;
			//System.out.println("ok " + getTimeSharedMakespan());
			CloudManager.getInstance().sendMessage(cloudlet, this, CloudMessage.Messages.CLOUDLET_ALLOCATION_TO_VM);
		} else {
			//System.out.println("error " + cloudlet.getRequiredCores() + " " + this.getRequiredCores());
			CloudManager.getInstance().sendMessage(this, cloudlet, CloudMessage.Messages.NOT_ENOUGH_CORES);
		}
	}
	
	public void clearCloudlets() {
		cloudletList.clear();
		pendingCloudlets.clear();
		completedCloudlets.clear();
		cached = false;
	}
	
	public List<CloudLet> getCloudlets() {
		return cloudletList;
	}
	
	public void addAllocation(CloudLet cloudlet) {
		//CloudLetAllocation ca = new CloudLetAllocation()
	}
	
	public PriorityQueueIterator<CloudCore> getCoreIterator() {
		return new PriorityQueueIterator<CloudCore>(coreQueue);
	} 
	
	public void setAssignedHost(CloudHost assignedHost, List<CloudCore> assignedCores) {
		this.assignedHost = assignedHost;
		for(CloudCore cc : assignedCores) {
			coreQueue.add(cc);
		}
	}
	
	public CloudHost getAssignedHost() {
		return assignedHost;
	}
	
	public CloudCore getLessOccupiedCore() {
		return coreQueue.peek();
	}
	
	public double getLastTick() {
		return getTimeSharedMakespan();
	}

	public String getType() {
		return "VM";
	}
	
	public boolean canCloudletFitVMWithRestrictions(CloudLet c, double forbiddenCores) {
		return (this.getRequiredCores() - (this.getRequiredCores() * forbiddenCores)) >= c.getRequiredCores();
	}
	
	public double getTimeSharedMakespan() {
		
		if(cloudletList.isEmpty()) {
			return 0.0;
		}
		
		if(cached) {
			return cachedTimeSharedMakespan;
		}
		
		double cloudletMIPS = getTotalMIPSFromCloudlets();
		double coreMIPS = requiredCPU;
		int cloudletCores = getTotalCoresFromCloudlets();
		int VMCores = requiredCores;
		
		if(coreMIPS < VMCores) {
			coreMIPS = VMCores;
		}
		cachedTimeSharedMakespan = (cloudletMIPS * cloudletCores) / (coreMIPS * VMCores);
		cached = true;
		return cachedTimeSharedMakespan;
	}
	
	private double getTotalMIPSFromCloudlets() {
		double mips = 0.0;
		for(CloudLet c : cloudletList) {
			mips += c.getRequiredCPU();
		}
		
		return mips;
	}
	
	private int getTotalCoresFromCloudlets() {
		int cores = 0;
		for(CloudLet c : cloudletList) {
			cores += c.getRequiredCores();
		}
		return cores;
	}

	@Override
	public List<String> display() {
		List<String> info = new ArrayList<>();
		info.add(getName() + " requires " + this.getRequiredCores() + " cores");
		info.add(getName() + " has " + this.getTotalCores() + " total cores");
		info.add(getName() + " has " + cloudletList.size() + " cloudlets");
		return info;
	}
}
