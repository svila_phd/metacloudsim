package svila.Brokers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;
import org.cloudbus.cloudsim.lists.VmList;
import svila.AsyncDatacenterBroker;
import svila.mesd.MESD;

/**
 *
 * @author Sergi
 */
public class MESDBroker extends AsyncDatacenterBroker {
    
    MESD mesd;
    HashMap<Integer, Vm> vmMap;

    public MESDBroker(String name) throws Exception {
        super(name);
    }

    @Override
    protected void submitCloudlets() {
        vmMap = new HashMap<>();
        for(Vm vm : vmList) {
            vmMap.put(vm.getId(), vm);
        }
        
        mesd = MESD.getAdaptedMESDFromVm(vmList, cloudletList);
        mesd.mainAlgorithm();
        List<Cloudlet> successfullySubmitted = new ArrayList<>();
        
        for (Cloudlet cloudlet : getCloudletList()) {
            Vm vm;
            vm  = vmMap.get(Integer.parseInt(mesd.getHostIdFromCloudletId(String.valueOf(cloudlet.getCloudletId()))));
            Log.printConcatLine(vm.getId());
            if (!Log.isDisabled()) {
                Log.printConcatLine(CloudSim.clock(), ": ", getName(), ": Sending cloudlet ",
                        cloudlet.getCloudletId(), " to VM #", vm.getId());
            }

            cloudlet.setVmId(vm.getId());
            cloudlet.setExecStartTime(mesd.getCloudletStartTime(String.valueOf(cloudlet.getCloudletId())));

            System.out.println("Cloudlet " + cloudlet.getCloudletId() + " Start time: " + cloudlet.getExecStartTime());
            send(getVmsToDatacentersMap().get(vm.getId()), cloudlet.getExecStartTime(), CloudSimTags.CLOUDLET_SUBMIT, cloudlet);
            cloudletsSubmitted++;
            getCloudletSubmittedList().add(cloudlet);
            successfullySubmitted.add(cloudlet);
        }

        // remove submitted cloudlets from waiting list
        getCloudletList().removeAll(successfullySubmitted);
    }
}
