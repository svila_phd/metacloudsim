package svila.Brokers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;
import svila.AsyncDatacenterBroker;
import svila.mesd.MESD;

/**
 *
 * @author Sergi
 */
public class RandomBroker extends DatacenterBroker {

    Random random;
    
    public RandomBroker(String name) throws Exception {
        super(name);
        this.random = new Random();
    }
    
    public void setRandomSeed(long seed) {
        this.random.setSeed(seed);
    }

    @Override
    protected void submitCloudlets() {


        List<Cloudlet> successfullySubmitted = new ArrayList<>();
        for (Cloudlet cloudlet : getCloudletList()) {
            Vm vm;

            vm = vmList.get(random.nextInt(vmList.size()));

            if (!Log.isDisabled()) {
                Log.printConcatLine(CloudSim.clock(), ": ", getName(), ": Sending cloudlet ",
                        cloudlet.getCloudletId(), " to VM #", vm.getId());
            }

            cloudlet.setVmId(vm.getId());

            sendNow(getVmsToDatacentersMap().get(vm.getId()), CloudSimTags.CLOUDLET_SUBMIT, cloudlet);
            cloudletsSubmitted++;
            getCloudletSubmittedList().add(cloudlet);
            successfullySubmitted.add(cloudlet);
        }

        // remove submitted cloudlets from waiting list
        getCloudletList().removeAll(successfullySubmitted);
    }
}
