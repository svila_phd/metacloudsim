package svila.Brokers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;

/**
 *
 * @author Sergi
 */
public class BinPackingBroker extends DatacenterBroker {
    
    HashMap<Integer, Vm> vmMap;
    HashMap<Integer, Cloudlet> cloudletMap;

    public BinPackingBroker(String name) throws Exception {
        super(name);
        vmMap = new HashMap<>();
        for(Vm vm : vmList) {
            vmMap.put(vm.getId(), vm);
        }
        cloudletMap = new HashMap<>();
        for(Cloudlet cloudlet : cloudletList) {
            cloudletMap.put(cloudlet.getCloudletId(), cloudlet);
        }
    }

    @Override
    protected void submitCloudlets() {        


        List<Cloudlet> successfullySubmitted = new ArrayList<>();

        int[] vmAssignation = initialAssignation();
        int[] bestVmAssignation = vmAssignation;
        double bestTime = Double.MAX_VALUE;
        
        long numIterations = (long) Math.pow(getCloudletList().size(), vmList.size());
        System.out.println("Num iterations: " + numIterations);
        double currentTime;
        for(int i=0; i<numIterations; i++) {
            nextAssignation(vmAssignation);
            currentTime = obtainTime(vmAssignation);
            if(currentTime > bestTime) {
                bestVmAssignation = vmAssignation;
                bestTime = currentTime;
            }
        }
        
        int i=0;
        for (Cloudlet cloudlet : getCloudletList()) {
            Vm vm;
            vm = vmList.get(vmAssignation[i]);
            Log.printConcatLine(vm.getId());
            if (!Log.isDisabled()) {
                Log.printConcatLine(CloudSim.clock(), ": ", getName(), ": Sending cloudlet ",
                        cloudlet.getCloudletId(), " to VM #", vm.getId());
            }

            cloudlet.setVmId(vm.getId());

            System.out.println("Cloudlet " + cloudlet.getCloudletId() + " Start time: " + cloudlet.getExecStartTime());
            send(getVmsToDatacentersMap().get(vm.getId()), cloudlet.getExecStartTime(), CloudSimTags.CLOUDLET_SUBMIT, cloudlet);
            cloudletsSubmitted++;
            getCloudletSubmittedList().add(cloudlet);
            successfullySubmitted.add(cloudlet);
        }

        // remove submitted cloudlets from waiting list
        getCloudletList().removeAll(successfullySubmitted);
    }
    
    private int[] initialAssignation() {
        int[] array = new int[cloudletList.size()];
        for(int i=0; i<array.length; i++) {
            array[i] = 0;
        }
        
        return array;
    }
    
    private void nextAssignation(int[] vmAssignation) {
        vmAssignation[0]++;
        for(int i=0; i<vmAssignation.length-1; i++) {
            if(vmAssignation[i] > vmList.size()) {
                vmAssignation[i] = 0;
                vmAssignation[i+1]++;
                break;
            }
        }
    }
    
    /* TODO */
    private double obtainTime(int[] vmAssignation) {
        return 0.0;
        /*double[] mipsPerVm = new double[vmList.size()];
        for(int i=0; i<vmAssignation; i++) {
            mipsPerVm[vmAssignation[i]] += cloudletMap.get(i);
        }*/
    }
    
    public void printAssignation(int[] vmAssignation) {
        
    }
}
