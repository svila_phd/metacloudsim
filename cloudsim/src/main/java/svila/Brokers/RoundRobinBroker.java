package svila.Brokers;

import org.cloudbus.cloudsim.DatacenterBroker;

/**
 *
 * @author Sergi
 */
public class RoundRobinBroker extends DatacenterBroker {

    // The same implementation as DatacenterBroker, it does RR be itself
    
    public RoundRobinBroker(String name) throws Exception {
        super(name);
    }

}
