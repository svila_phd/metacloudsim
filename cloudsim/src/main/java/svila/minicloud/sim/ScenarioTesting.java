package svila.minicloud.sim;

import java.util.ArrayList;
import java.util.List;

import svila.planetlabNetwork.Interaction;
import svila.planetlabNetwork.MockInteraction;

public class ScenarioTesting {
	public static void main(String[] args) {
		Test1();
		//Test2();
	}
	
	public static void Test1() {		
		MCHost host1 = new MCHost(0, 600.0, 0.0);
		MCHost host2 = new MCHost(1, 600.0, 0.0);
		MCHost host3 = new MCHost(2, 600.0, 0.0);
		
		MCVM vm3 = new MCVM(3, 300.0, 200.0);
		MCVM vm4 = new MCVM(4, 300.0, 100.0);
		MCVM vm5 = new MCVM(5, 300.0, 300.0);
		MCVM vm6 = new MCVM(6, 300.0, 100.0);
		MCVM vm7 = new MCVM(7, 300.0, 50.0);
		MCVM vm8 = new MCVM(8, 300.0, 200.0);
		
		host1.addVm(vm3);
		host1.addVm(vm4);
		host2.addVm(vm5);
		host2.addVm(vm6);
		host2.addVm(vm7);
		host3.addVm(vm8);
		
		List<MCHost> hosts = new ArrayList<>();
		hosts.add(host1);
		hosts.add(host2);
		hosts.add(host3);
		
		List<Interaction> interactions = new ArrayList<>();
		interactions.add(new MockInteraction(3, 4));
		interactions.add(new MockInteraction(5, 6));
		interactions.add(new MockInteraction(5, 7));
		
		Scenario scenario = new Scenario(hosts);
		scenario.setInteractions(interactions);
		
		Scenario scenario2 = scenario.getCopy();
		Scenario scenario3 = scenario.getCopy();
		
		System.out.println(scenario.equals(scenario2));

		
		System.out.println("Requested MIPS: " + scenario.getRequestedMips());
		System.out.println("Allocated MIPS: " + scenario.getAllocatedMips());
		System.out.println("Non allocated MIPS: " + scenario.getNonAllocatedMips());
		System.out.println("---");
		
		scenario.migrateAllVms(host3, host2);
		System.out.println(scenario.equals(scenario2));

		
		System.out.println("Requested MIPS: " + scenario.getRequestedMips());
		System.out.println("Allocated MIPS: " + scenario.getAllocatedMips());
		System.out.println("Non allocated MIPS: " + scenario.getNonAllocatedMips());
		System.out.println("---");

		scenario.migrateAllVms(host2, host1);
		
		System.out.println("Requested MIPS: " + scenario.getRequestedMips());
		System.out.println("Allocated MIPS: " + scenario.getAllocatedMips());
		System.out.println("Non allocated MIPS: " + scenario.getNonAllocatedMips());
		System.out.println("---");
		System.out.println("---");

		System.out.println("Requested MIPS: " + scenario2.getRequestedMips());
		System.out.println("Allocated MIPS: " + scenario2.getAllocatedMips());
		System.out.println("Non allocated MIPS: " + scenario2.getNonAllocatedMips());
		System.out.println("---");
		
		scenario2.migrateAllVms(host2.netId, host1.netId);
		//scenario2.migrateAllVms(host3.netId, host1.netId);
		
		System.out.println("Requested MIPS: " + scenario2.getRequestedMips());
		System.out.println("Allocated MIPS: " + scenario2.getAllocatedMips());
		System.out.println("Non allocated MIPS: " + scenario2.getNonAllocatedMips());
		System.out.println("---");
		
		scenario.migrateAllVms(2, 0);
		scenario.migrateAllVms(1, 0);
		
		scenario.rollbackAllMigrations();
		
		System.out.println(scenario.equals(scenario3));
		
		scenario.printResume();

	}
	
	public static void Test2() {		
		MCHost host1 = new MCHost(0, 600.0, 0.0);
		MCHost host2 = new MCHost(1, 600.0, 0.0);
		MCHost host3 = new MCHost(2, 600.0, 0.0);
		
		MCVM vm3 = new MCVM(3, 300.0, 200.0);
		MCVM vm4 = new MCVM(4, 300.0, 100.0);
		MCVM vm5 = new MCVM(5, 300.0, 300.0);
		MCVM vm6 = new MCVM(6, 300.0, 100.0);
		MCVM vm7 = new MCVM(7, 300.0, 50.0);
		MCVM vm8 = new MCVM(8, 300.0, 200.0);
		
		host1.addVm(vm3);
		host1.addVm(vm4);
		host2.addVm(vm5);
		host2.addVm(vm6);
		host2.addVm(vm7);
		host3.addVm(vm8);
		
		List<MCHost> hosts = new ArrayList<>();
		hosts.add(host1);
		hosts.add(host2);
		hosts.add(host3);
		
		List<Interaction> interactions = new ArrayList<>();
		interactions.add(new MockInteraction(3, 4));
		interactions.add(new MockInteraction(5, 6));
		interactions.add(new MockInteraction(5, 7));
		
		Scenario scenario = new Scenario(hosts);
		scenario.setInteractions(interactions);
		
		Scenario scenario2 = scenario.getCopy();
		
		scenario.migrateVm(8, 0);
		
		scenario.printMigrations();
		
		scenario.rollbackLastMigration();
		
		System.out.println(scenario.equals(scenario2));

		scenario.printResume();

	}
}
