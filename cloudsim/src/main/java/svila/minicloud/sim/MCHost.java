package svila.minicloud.sim;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.models.PowerModel;
import org.cloudbus.cloudsim.power.models.PowerModelLinear;

import svila.metricsLogic.HostStats;
import svila.minicloud.real.RealCloudScenario;

public class MCHost implements Cloneable {
	
	int netId;
	public double cpuPerc;
	double totalMips;
	double usedMips;
	Map<Integer, MCVM> vmMap;
	boolean requireUpdate;
	double energy;
	PowerModel powerModel;
	double totalRam;
	double usedRam;
	int step;
	double time;
	HostStats hostStats;
	HostStats slicedHostStats;
	
	// inVmsMap
	
	public MCHost(int netId, double totalMips, double usedMips) {
		this.netId = netId;
		this.totalMips = totalMips;
		this.usedMips = usedMips;
		this.requireUpdate = true;
		this.vmMap = new HashMap<>();
		this.powerModel = new PowerModelLinear(100, 70);
	}
	
	public MCHost(PowerHostUtilizationHistory host) {
		this.netId = host.getNetworkId();
		this.totalMips = host.getTotalMips();
		this.vmMap = new HashMap<>();
		for(Vm vm : host.getVmList()) {
			this.vmMap.put(vm.getNetworkId(), new MCVM(vm));
		}
		this.requireUpdate = true;
		this.powerModel = host.getPowerModel();
	}
	
	public MCHost(HostStats hostStats, int firstStep, int lastStep, int forecastingSteps) {
		this.netId = hostStats.host.getNetworkId();
		this.hostStats = hostStats;
		this.slicedHostStats = this.hostStats.getSlice(firstStep, lastStep, forecastingSteps);
		this.totalMips = hostStats.host.getTotalMips();
	}
	
	private MCHost() {
		this.vmMap = new HashMap<>();
		this.requireUpdate = true;
	}
	
	public void activateRequireUpdate() {
		this.requireUpdate = true;
	}
	
	public void deactivateRequireUpdate() {
		this.requireUpdate = false;
	}
	
	public int getNetId() {
		return this.netId;
	}
	
	public void addVm(MCVM vm) {
		vmMap.put(vm.getNetId(), vm);
		vm.setHost(this);
		requireUpdate = true;
	}
	
	public void addVms(List<MCVM> vms) {
		for(MCVM vm : vms) {
			addVm(vm);
		}
	}
	
	public void removeVm(MCVM vm) {
		vmMap.remove(vm.getNetId());
		requireUpdate = true;
	}
	
	public double getEnergy() {
		return this.powerModel.getPower(getRealUtilization());
	}
	
	public double getRealUtilization() {
		checkUpdate();
		return Math.min(usedMips, totalMips)/totalMips;
	}
	
	public double getUtilization() {
		checkUpdate();
		return usedMips/totalMips;
	}
	
	public double getTotalMips() {
		return totalMips;
	}
	
	public boolean checkUpdate() {
		boolean requireBefore= requireUpdate;
		if(requireUpdate) {
			calculateUsedMips();
		}
		return requireBefore && !requireUpdate;
	}

	public double getUsedMips() {
		checkUpdate();
		return usedMips;
	}
	
	public double getAllocatedMips() {
		checkUpdate();
		return Math.min(totalMips, getUsedMips());
	}
	
	public double getNonAllocatedMips() {
		checkUpdate();
		return Math.max(0, getUsedMips() - totalMips);
	}

	public Map<Integer, MCVM> getVmMap() {
		return vmMap;
	}

	void calculateUsedMips() {
		this.usedMips = 0.0;
		for(MCVM vm : vmMap.values()) {
			this.usedMips += vm.getUsedMips();
		}
		requireUpdate = false;
	}
	
	public void setStep(int step) {
		this.step = step;
		this.time = 300.0 + this.step * 300.0 + 0.1;
		this.activateRequireUpdate();
	}
	
	public void loadFromStats() {
		loadFrom(this.hostStats);
	}
	
	public void loadFromSlicedStats() {
		loadFrom(this.slicedHostStats);
	}
	
	private void loadFrom(HostStats currentHostStats) {
		this.cpuPerc = currentHostStats.getCPUPerc(this.step);
		
		this.vmMap = new HashMap<>();
	}
	
	public void saveToSlicedStats() {
		this.slicedHostStats.putPerc(this.step, this.cpuPerc);
	}
	
	public void loadFromRealScenario(RealCloudScenario realCloudScenario) {
		this.cpuPerc = realCloudScenario.getHostByNetId(this.netId).getUtilizationOfCpu();
	
		this.deactivateRequireUpdate();
	}
	
	/*
	public Map<Integer, Set<MCVM>> getSlicedVmMap(int firstStep, int lastStep) {
		Map<Integer, Set<MCVM>> slicedVmMap = new HashMap<>();
		for(Integer currentStep = firstStep; currentStep <= lastStep; currentStep++) {
			slicedVmMap.put(currentStep, this.hostStats.getVms(currentStep));
		}
		return slicedVmMap;
	}
	*/
	
	public MCHost clone() {
		MCHost copy = new MCHost();
		copy.netId = this.netId;
		copy.requireUpdate = this.requireUpdate;
		copy.totalMips = this.totalMips;
		copy.usedMips = this.usedMips;
		for(MCVM vm : this.vmMap.values()) {
			MCVM vmCopy = vm.cloneWithoutHost();
			vmCopy.setHost(copy);
			copy.vmMap.put(vm.netId, vmCopy);
		}
		
		return copy;
	}
	
	@Override
	public String toString() {
		String str = "";
		str += "Host " + this.netId + " [";
		Collection<MCVM> vms = this.vmMap.values();
		int i=0;
		for(MCVM vm : vms) {
			str += vm.netId;
			if(i < vms.size()-1) {
				str += ", ";
			}
			i++;
		}
		str += "]";
		return str;
	}
	
	@Override  
	public boolean equals(Object obj)   
	{
		if(obj == null) {  
			return false;
		}
		if(obj == this) {
			return true;
		}
		
		MCHost other = (MCHost) obj;
		
		this.checkUpdate();
		other.checkUpdate();
		
		if(this.netId != other.netId || this.usedMips != other.usedMips ||
		   this.totalMips != other.totalMips) {
			return false;
		}
		
		if(this.vmMap.size() != other.vmMap.size()) {
			return false;
		}
		
		Set<Integer> otherVmNetIds = other.vmMap.keySet();
		for(Integer vmNetId : this.vmMap.keySet()) {
			if(!otherVmNetIds.contains(vmNetId)) {
				return false;
			}
		}
		
		return true;
	}
}
