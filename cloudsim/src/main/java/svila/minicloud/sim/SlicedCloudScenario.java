package svila.minicloud.sim;

public class SlicedCloudScenario {
	int firstStep, lastStep, forecastingSteps;
	
	public SlicedCloudScenario(int firstStep, int lastStep, int forecastingSteps) {
		this.firstStep = firstStep;
		this.lastStep = lastStep;
		this.forecastingSteps = forecastingSteps;
	}
}
