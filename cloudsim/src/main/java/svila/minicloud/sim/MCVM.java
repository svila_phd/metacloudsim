package svila.minicloud.sim;

import java.util.HashMap;
import java.util.Map;

import org.cloudbus.cloudsim.Vm;

import svila.metricsLogic.VmStats;
import svila.minicloud.real.RealCloudScenario;

public class MCVM implements Cloneable {
	int netId;
	double totalMips;
	double usedMips;
	double cpuPerc;
	double ram;
	MCHost host;
	Map<Integer, MCHost> hostMap;
	int step;
	double time;
	VmStats vmStats;
	VmStats slicedVmStats;
	
	public MCVM(int netId, double totalMips) {
		this.netId = netId;
		this.totalMips = totalMips;
		this.usedMips = 0.0;
		this.host = null;
		this.hostMap = new HashMap<>();
	}
	
	public MCVM(int netId, double totalMips, double usedMips) {
		this.netId = netId;
		this.totalMips = totalMips;
		this.usedMips = usedMips;
		this.host = null;
		this.hostMap = new HashMap<>();
	}
	
	public MCVM(Vm vm) {
		this.netId = vm.getNetworkId();
		this.totalMips = vm.getMips();
		for(Double requestedMips : vm.getCurrentRequestedMips()) {
			this.usedMips += requestedMips;
		}
	}
	
	public MCVM(VmStats vmStats, Map<Integer, MCHost> hostMap, int firstStep, int lastStep, int forecastingSteps) {
		this.hostMap = hostMap;
		this.vmStats = vmStats;
		this.slicedVmStats = this.vmStats.getSlice(firstStep, lastStep, forecastingSteps);
		this.netId = vmStats.vm.getNetworkId();
		this.totalMips = this.vmStats.vm.getMips();
	}
	
	public void setStep(int step) {
		this.step = step;
	}
	
	public void loadFromStats() {
		this.loadFrom(this.vmStats);
	}
	
	public void loadFromSlicedStats() {
		this.loadFrom(this.slicedVmStats);
	}
	
	private void loadFrom(VmStats currentVmStats) {
		this.cpuPerc = currentVmStats.getPerc(this.step);
		this.setUsedMips(this.cpuPerc * this.totalMips);
		this.host = this.hostMap.get(currentVmStats.getHostNetId(this.step));
		this.host.addVm(this);
	}
	
	public void saveToSlicedStats() {
		this.slicedVmStats.putCPU(this.step, this.cpuPerc);
		this.slicedVmStats.putHostNetId(this.step, this.host.getNetId());
	}
	
	public void loadFromRealScenario(RealCloudScenario realCloudScenario) {
		this.cpuPerc = realCloudScenario.getVmByNetId(this.netId).getCloudletScheduler().getTotalUtilizationOfCpu(this.time);
		this.setUsedMips(this.cpuPerc * this.totalMips);
		int hostNetId = realCloudScenario.getVmByNetId(this.netId).getHost().getNetworkId();
		this.host = this.hostMap.get(hostNetId);
	}
	
	private MCVM() {
		
	}
	
	public MCVM cloneWithoutHost() {
		MCVM copy = new MCVM();
		copy.netId = this.netId;
		copy.totalMips = this.totalMips;
		copy.usedMips = this.usedMips;
		return copy;
	}
	
	public int getNetId() {
		return netId;
	}

	public double getTotalMips() {
		return totalMips;
	}

	public double getUsedMips() {
		return usedMips;
	}
	
	public void setUsedMips(double usedMips) {
		this.usedMips = usedMips;
	}
	
	public MCHost getHost() {
		return host;
	}
	
	public void setHost(MCHost host) {
		this.host = host;
	}
	
	public double getAdjustedUsedMips() {
		return 0.0;
	}
	
	@Override
	public String toString() {
		return "VM " + this.netId + " [" + this.host.netId + "]";
	}
	
	@Override  
	public boolean equals(Object obj)   
	{
		if(obj == null) {  
			return false;
		}
		if(obj == this) {
			return true;
		}
		
		MCVM other = (MCVM) obj;
		
		return this.netId == other.netId && this.host.netId == other.host.netId
			&& this.totalMips == other.totalMips && this.usedMips == other.usedMips;		
	}
}
