package svila.minicloud.sim;

public class ScenarioStats {
	
	Scenario scenario;
	int start, end;
	
	public ScenarioStats(Scenario scenario) {
		this.scenario = scenario;
	}
	
	public void setInterval(int start, int end) {
		this.start = start;
		this.end = end;
	}
	
	public double getTotalAllocatedMIPS() {
		double totalAllocatedMIPS = 0.0;
		for(int currentStep=start; currentStep<=end; currentStep++) {
			scenario.setStep(currentStep);
			scenario.loadCurrentStepFromSlice();
			double currentAllocatedMIPS = scenario.getAllocatedMips();
			totalAllocatedMIPS += currentAllocatedMIPS;
		}
		
		return totalAllocatedMIPS;
	}
}
