package svila.minicloud.sim;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;

import svila.metricsLogic.HostAnalyzer;
import svila.metricsLogic.HostStats;
import svila.metricsLogic.MigrationsTracker;
import svila.metricsLogic.MigrationsTracker.NetIdMigration;
import svila.metricsLogic.MigrationsTracker.TriggeredMigrations;
import svila.metricsLogic.VmAnalyzer;
import svila.metricsLogic.VmStats;
import svila.minicloud.real.RealCloudScenario;
import svila.planetlabNetwork.Interaction;
import svila.planetlabNetwork.TopologyHostDistanceManager;

public class Scenario implements Cloneable {
	public Map<Integer, MCHost> hostMap;
	public Map<Integer, MCVM> vmMap;
	List<PowerHost> realHosts;
	List<Vm> realVms;
	// VM communications
	List<Interaction> interactions;
	// Topology
	
	List<MCMigration> migrations;
	
	int step;
	int[] applicationSteps;
	
	private Scenario() {
		this.hostMap = new HashMap<>();
		this.vmMap = new HashMap<>();
		this.migrations = new ArrayList<>();
	}
	
	public Scenario(List<PowerHostUtilizationHistory> hosts, List<Vm> vms) {
		this.hostMap = new HashMap<>();
		this.vmMap = new HashMap<>();
		this.migrations = new ArrayList<>();
		this.realHosts = (List<PowerHost>)(List<?>) hosts;
		this.realVms = vms;
		
		for(PowerHostUtilizationHistory host : hosts) {
			hostMap.put(host.getNetworkId(), new MCHost(host));
		}
		
		for(MCHost host : hostMap.values()) {
			vmMap.putAll(host.getVmMap());
		}
	}
	
	public Scenario(HostAnalyzer hostAnalyzer, VmAnalyzer vmAnalyzer, int step) {
		this.hostMap = new HashMap<>();
		this.vmMap = new HashMap<>();
		this.migrations = new ArrayList<>();
		
		for(VmStats vmStats : vmAnalyzer.vmStats.values()) {
			MCVM mcVM = new MCVM(vmStats.vm.getNetworkId(),
					vmStats.vm.getMips(), vmStats.getCPU(step));
			vmMap.put(mcVM.getNetId(), mcVM);
		}
		
		for(HostStats hostStats : hostAnalyzer.hostStats.values()) {
			MCHost mcHost = new MCHost(hostStats.host.getNetworkId(),
					hostStats.host.getTotalMips(), hostStats.getCPU(step));
			
			this.hostMap.put(mcHost.netId, mcHost);
			for(Vm vm: hostStats.getVms(step)) {
				mcHost.addVm(vmMap.get(vm.getNetworkId()));
			}
		}
	}
	
	public Scenario(HostAnalyzer hostAnalyzer, VmAnalyzer vmAnalyzer, int firstStep, int lastStep, int forecastingSteps) {
		this.hostMap = new HashMap<>();
		this.vmMap = new HashMap<>();
		this.migrations = new ArrayList<>();
		this.realHosts = new ArrayList<>();
		this.realVms = new ArrayList<>();
		for(HostStats hostStats : hostAnalyzer.hostStats.values()) {
			this.realHosts.add((PowerHost)hostStats.host);
		}
		for(VmStats vmStats :vmAnalyzer.vmStats.values()) {
			this.realVms.add(vmStats.vm);
		}
		
		for(HostStats hostStats : hostAnalyzer.hostStats.values()) {
			MCHost mcHost = new MCHost(hostStats, firstStep, lastStep, forecastingSteps);
			this.hostMap.put(mcHost.netId, mcHost);
		}
		
		for(VmStats vmStats : vmAnalyzer.vmStats.values()) {
			MCVM mcVM = new MCVM(vmStats, this.hostMap, firstStep, lastStep, forecastingSteps);
			vmMap.put(mcVM.getNetId(), mcVM);
		}				
	}
	
	public void applyForwardMigrations(MigrationsTracker migrationsTracker, int firstStep, int lastStep) {
		for(int currentStep : this.applicationSteps) {
			this.loadStep(currentStep);
			TriggeredMigrations currentTriggeredMigrations = migrationsTracker.getTriggeredMigrations(currentStep);
			List<NetIdMigration> migrations = currentTriggeredMigrations.getMigrations();
			List<MCMigration> forwardMigrations = new ArrayList<>();
			for(NetIdMigration migration : migrations) {
				MCMigration mcMigration = getMCMigration(migration);
				forwardMigrations.add(mcMigration);
			}
			this.applyMigrations(forwardMigrations);
		}
	}
	
	private MCMigration getMCMigration(NetIdMigration netIdMigration) {
		return new MCMigration(this.vmMap.get(netIdMigration.vmNetId),
				this.hostMap.get(netIdMigration.originHostNetId),
				this.hostMap.get(netIdMigration.targetHostNetId));
	}
	
	public void loadStep(int step) {
		for(MCHost host : this.hostMap.values()) {
			host.setStep(step);
			host.loadFromSlicedStats();
		}
		
		for(MCVM vm : this.vmMap.values()) {
			vm.setStep(step);
			vm.loadFromSlicedStats();
		}
	}
	
	public void loadCurrentStepFromSlice() {
		this.loadStep(this.step);
	}
	
	public void saveToSlice() {
		for(MCHost host : this.hostMap.values()) {
			host.saveToSlicedStats();
		}
		
		for(MCVM vm : this.vmMap.values()) {
			vm.saveToSlicedStats();
		}
	}
	
	public void loadCurrentStepFromReal() {
		Map<Integer, Integer> initialScenario = getVmHostMapNetIds();
		RealCloudScenario realCloudScenario = new RealCloudScenario(this.realHosts, this.realVms, this.step, initialScenario);
		
		for(MCVM mcvm : this.vmMap.values()) {
			mcvm.loadFromRealScenario(realCloudScenario);
		}
		
		for(MCHost mcHost : this.hostMap.values()) {
			mcHost.loadFromRealScenario(realCloudScenario);
		}
		
		realCloudScenario.rollbackAll();
		
		for(MCHost mcHost : this.hostMap.values()) {
			mcHost.activateRequireUpdate();
		}
	}
	
	public void loadCurrentStepFromOriginal() {
		for(MCVM mcvm : this.vmMap.values()) {
			mcvm.loadFromStats();
		}
		
		for(MCHost mcHost : this.hostMap.values()) {
			mcHost.loadFromStats();
		}
	}
	
	private Map<Integer, Integer> getVmHostMapNetIds() {
		Map<Integer, Integer> initialScenario = new HashMap<>();
		for(MCVM mcvm : this.vmMap.values()) {
			initialScenario.put(mcvm.getNetId(), mcvm.vmStats.getHostNetId(this.step));
		}
		
		return initialScenario;
	}
	
	public void setStep(int step) {
		this.step = step;
	}
	
	public void setApplicationSteps(int[] applicationSteps) {
		this.applicationSteps = applicationSteps;
	}
	
	public void setApplicationSteps(int first, int last) {
		int size = last-first+1;
		this.applicationSteps = new int[size];
		for(int i=0; i<size; i++) {
			this.applicationSteps[i] = first+i;
		}
	}
	
	public void commit() {
		for(int currentStep : this.applicationSteps) {
			this.loadStep(currentStep);
			this.applyMigrations(this.migrations);
		}
	}
	
	// TODO: Create scenarios using arrays or maps
	//		 [5] [4] [7] <- host
	//       VM0 VM1 VM2
	
	public Scenario(List<MCHost> hosts) {
		this.hostMap = new HashMap<>();
		this.vmMap = new HashMap<>();
		this.migrations = new ArrayList<>();
		
		for(MCHost host : hosts) {
			hostMap.put(host.netId, host);
		}
		
		for(MCHost host : hostMap.values()) {
			vmMap.putAll(host.getVmMap());
		}
	}
	
	public double getEnergy() {
		double energy = 0.0;
		for(MCHost host : hostMap.values()) {
			energy += host.getEnergy();
		}
		return energy;
	}
	
	public double getRequestedMips() {
		double requestedMips = 0.0;
		for(MCHost host : hostMap.values()) {
			requestedMips += host.getUsedMips();
		}
		return requestedMips;
	}
	
	public double getAllocatedMips() {
		double allocatedMips = 0.0;
		for(MCHost host : hostMap.values()) {
			allocatedMips += host.getAllocatedMips();
		}
		return allocatedMips;
	}
	
	public double getNonAllocatedMips() {
		double nonAllocatedMips = 0.0;
		for(MCHost host : hostMap.values()) {
			nonAllocatedMips += host.getNonAllocatedMips();
		}
		return nonAllocatedMips;
	}
	
	public void migrateAllVms(int originHostNetId, int destinationHostNetId) {
		migrateAllVms(hostMap.get(originHostNetId),
					  hostMap.get(destinationHostNetId));
	}
	
	public void migrateAllVms(MCHost origin, MCHost destination) {
		List<MCVM> vmsToMigrate = new ArrayList<>();
		for(MCVM vm : origin.getVmMap().values()) {
			vmsToMigrate.add(vm);
		}
		for(MCVM vm : vmsToMigrate) {
			migrateVm(vm, destination);
		}
	}
	
	public void migrateVm(int vmNetId, int hostNetId) {
		migrateVm(vmMap.get(vmNetId),
				  hostMap.get(hostNetId));
	}
	
	public boolean migrateVm(MCVM vm, MCHost destinationHost) {
		return migrateVm(vm, destinationHost, false);
	}
	
	public void migrateVmAndSaveMigration(int vmNetId, int hostNetId) {
		migrateVmAndSaveMigration(vmMap.get(vmNetId),
				  				  hostMap.get(hostNetId));
	}
	
	public boolean migrateVmAndSaveMigration(MCVM vm, MCHost destinationHost) {
		return migrateVm(vm, destinationHost, true);
	}
	
	public boolean migrateVm(MCVM vm, MCHost destinationHost, boolean storeMigration) {
		MCHost originHost = vm.getHost();
		
		if(originHost.netId == destinationHost.netId) {
			return false;
		}
		
		if(storeMigration) {
			this.migrations.add(new MCMigration(vm, originHost, destinationHost));
		}
		
		vm.getHost().removeVm(vm);
		destinationHost.addVm(vm);
		
		return true;
	}
	
	public void rollbackLastMigration() {
		MCMigration lastMigration = this.migrations.remove(this.migrations.size()-1);
		lastMigration.destinationHost.removeVm(lastMigration.vm);
		lastMigration.originHost.addVm(lastMigration.vm);
	}
	
	public void rollbackAllMigrations() {
		while(!migrations.isEmpty()) {
			this.rollbackLastMigration();
		}
	}
	
	public boolean applyMigrations(List<MCMigration> migrations) {
		int count = 0;
		for(MCMigration migration : migrations) {
			if(migration.getVm().getHost().netId == migration.getOriginHost().netId) {
				migrateVm(migration.getVm().netId, migration.getDestinationHost().netId);
				count++;
			} else {
				break;
			}
		}
		
		if(count != migrations.size()) {
			while(count > 0) {
				rollbackLastMigration();
				count--;
			}
		}
		
		return count == migrations.size();
	}
	
	public void simulateMigration(MCVM vm, MCHost host) {
		// VM adjustments
		
		// Origin host
		
		
		// Destination host
	}
	
	public Scenario getCopy() {
		Scenario copy = new Scenario();
		
		for(MCHost host : this.hostMap.values()) {
			copy.hostMap.put(host.netId, host.clone());
		}
		
		for(MCHost host : copy.hostMap.values()) {
			copy.vmMap.putAll(host.getVmMap());
		}
		
		for(MCMigration migration : migrations) {
			copy.migrations.add(
					new MCMigration(
							copy.vmMap.get(migration.getVm().getNetId()),
							copy.hostMap.get(migration.originHost.netId),
							copy.hostMap.get(migration.destinationHost.netId)
					)
			);
		}
		
		return copy;
	}
	
	public void setInteractions(List<Interaction> interactions) {
		this.interactions = interactions;
	}
	
	public List<Double> getCommunicationDistances() {
		List<Double> distances = new ArrayList<>();
		double distance;
		for(Interaction interaction : interactions) {
			if(!vmMap.containsKey(interaction.getSourceId()) ||
					!vmMap.containsKey(interaction.getEndId())) {
				continue;
			}
			distance = TopologyHostDistanceManager.
					getShortestDistanceBetweenTwoHostNetworkIds(
							vmMap.get(interaction.getSourceId()).host.netId,
							vmMap.get(interaction.getEndId()).host.netId
					);
			distances.add(new Double(distance));
		}
		Collections.sort(distances);
		return distances;
	}
	
	public double getAverageCommunicationDistance() {
		return getCommunicationDistances().stream().mapToDouble(d -> d).average().orElse(0.0);
	}
	
	public int getNumCommunicationsInSameHost() {
		int count = 0;
		for(Interaction interaction : interactions) {
			if(!vmMap.containsKey(interaction.getSourceId()) ||
					!vmMap.containsKey(interaction.getEndId())) {
				continue;
			}
			if(vmMap.get(interaction.getSourceId()).host.netId == vmMap.get(interaction.getEndId()).host.netId) {
				count++;
			}
		}
		return count;
	}
	
	public double getPercNumCommunicationsInSameHost() {
		double num = getNumCommunicationsInSameHost();
		return num / interactions.size();
	}
	
	public List<MCMigration> getMigrations() {
		return this.migrations;
	}
	
	public void printResume() {
		System.out.println("Requested MIPS: " + this.getRequestedMips());
		System.out.println("Allocated MIPS: " + this.getAllocatedMips());
		System.out.println("Communication distance average: " + this.getAverageCommunicationDistance());
		System.out.println("% communications in same host: " + this.getPercNumCommunicationsInSameHost() * 100 + "%");
		System.out.println("Energy: " + this.getEnergy());

	}
	
	public void printMigrations() {
		for(MCMigration migration : migrations) {
			System.out.println(migration);
		}
	}
	
	@Override  
	public boolean equals(Object obj)   
	{
		if(obj == null) {  
			return false;
		}
		if(obj == this) {
			return true;
		}
		
		Scenario other = (Scenario) obj;
				

		if(this.vmMap.size() != other.vmMap.size() ||
		   this.hostMap.size() != other.hostMap.size()) {
			return false;
		}
		
		for(Integer value : this.vmMap.keySet()) {
			if(!other.vmMap.containsKey(value)) {
				return false;
			}
		}
		
		for(Integer value : this.hostMap.keySet()) {
			if(!other.hostMap.containsKey(value)) {
				return false;
			}
		}
		
		for(Integer hostNetId : this.hostMap.keySet()) {
			if(!this.hostMap.get(hostNetId).equals(other.hostMap.get(hostNetId))) {
				return false;
			}
		}
				
		for(Integer vmNetId : this.vmMap.keySet()) {
			if(!this.vmMap.get(vmNetId).equals(other.vmMap.get(vmNetId))) {
				return false;
			}
		}
		
		return true;
	}
	
	
	public boolean equalsWithMigrations(Object obj) {
		if(!this.equals(obj)) {
			return false;
		}
		
		Scenario other = (Scenario) obj;
		
		if(this.migrations.size() != other.migrations.size()) {
			return false;
		}
		
		for(int i=0; i<this.migrations.size(); i++) {
			if(!this.migrations.get(i).equals(other.migrations.get(i))) {
				return false;
			}
		}
		
		return true;
	}
}
