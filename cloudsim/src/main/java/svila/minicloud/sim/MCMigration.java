package svila.minicloud.sim;

public class MCMigration implements Cloneable {
	
	MCHost originHost;
	MCHost destinationHost;
	MCVM vm;
	
	public MCMigration(MCVM vm, MCHost originHost, MCHost destinationHost) {
		this.vm = vm;
		this.originHost = originHost;
		this.destinationHost = destinationHost;
	}

	public MCHost getOriginHost() {
		return originHost;
	}

	public MCHost getDestinationHost() {
		return destinationHost;
	}

	public MCVM getVm() {
		return vm;
	}
	
	@Override
	public String toString() {
		return vm.netId + " [" + originHost.netId + "] -> [" + destinationHost.netId + "]";
	}
	
	@Override  
	public boolean equals(Object obj)   
	{
		if(obj == null) {  
			return false;
		}
		if(obj == this) {
			return true;
		}
	
		MCMigration other = (MCMigration) obj;
		
		if(!this.originHost.equals(other.originHost) ||
		   !this.destinationHost.equals(other.destinationHost) ||
		   !this.vm.equals(other.vm)) {
			return false;
		}
		
		return true;
	}
}
