package svila.minicloud.real;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public class MoveOneVmToHostAction extends Action {
	
	public MoveOneVmToHostAction(PowerHost targetHost, Vm vm) {
		super();
		this.addMovement((PowerHost) vm.getHost(), targetHost, vm);
	}
}
