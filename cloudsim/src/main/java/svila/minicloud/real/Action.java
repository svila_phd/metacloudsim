package svila.minicloud.real;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public abstract class Action {
	List<Movement> movements;
	
	protected Action() {
		this.movements = new ArrayList<>();
	}
	
	public void addMovement(Movement movement) {
		this.movements.add(movement);
	}
	
	public void addMovement(PowerHost originHost, PowerHost targetHost, Vm vm) {
		this.movements.add(new Movement(originHost, targetHost, vm));
	}
	
	public void apply(Map<Vm, PowerHost> vmHostMapResult) {
		for(Movement movement : this.movements) {
			if(movement.originHost != null) {
				movement.originHost.vmDestroy(movement.vm);
				movement.targetHost.vmCreate(movement.vm);
				vmHostMapResult.put(movement.vm, movement.targetHost);
			}
		}
	}

	public void rollback(Map<Vm, PowerHost> vmHostMapResult) {
		for(Movement movement : this.movements) {
			if(movement.targetHost != null) {
				movement.targetHost.vmDestroy(movement.vm);
			}
			if(movement.originHost != null) {
				movement.originHost.vmCreate(movement.vm);
			}
			vmHostMapResult.put(movement.vm, movement.originHost);
		}
	}
}