package svila.minicloud.real;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Stack;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.metricsLogic.MigrationsTracker;

public class RealCloudScenario {
	Map<Integer, PowerHost> hostsMap;
	Map<Integer, Vm> vmsMap;
	// originalScenario
	Map<Vm, PowerHost> vmHostMapOriginal;
	// movements
	Queue<Action> pendingActions;
	Stack<Action> appliedActions;
	// resultingScenario
	Map<Vm, PowerHost> vmHostMapResult;
	int step;
	double time;
	
	public RealCloudScenario(List<PowerHost> hosts, List<Vm> vms) {
		this.hostsMap = hosts.stream().collect(Collectors.toMap(PowerHost::getNetworkId, host -> host));
		this.vmsMap = vms.stream().collect(Collectors.toMap(Vm::getNetworkId, vm -> vm));
		this.vmHostMapOriginal = new HashMap<>();
		for(Vm vm : vms) {
			this.vmHostMapOriginal.put(vm, (PowerHost) vm.getHost());
		}
		this.vmHostMapResult = new HashMap<>(this.vmHostMapOriginal);
		this.pendingActions = new LinkedList<>();
		this.appliedActions = new Stack<>();
	}
	
	public RealCloudScenario(List<PowerHost> hosts, List<Vm> vms, int step, Map<Integer, Integer> initialMap) {
		this.pendingActions = new LinkedList<>();
		this.appliedActions = new Stack<>();
		
		this.hostsMap = hosts.stream().collect(Collectors.toMap(PowerHost::getNetworkId, host -> host));
		this.vmsMap = vms.stream().collect(Collectors.toMap(Vm::getNetworkId, vm -> vm));
		
		setStep(step);
		setInitialScenario(initialMap);
		
		this.vmHostMapOriginal = new HashMap<>();
		for(Vm vm : vms) {
			this.vmHostMapOriginal.put(vm, (PowerHost) vm.getHost());
		}
		this.vmHostMapResult = new HashMap<>(this.vmHostMapOriginal);
	}
	
	private void setInitialScenario(Map<Integer, Integer> initialMap) {
		Map<Vm, PowerHost> migrationMap = new HashMap<>();
		for(Entry<Integer, Integer> entry : initialMap.entrySet()) {
			Vm vm = this.getVmByNetId(entry.getKey());
			int targetHostNetId = entry.getValue();
			if(vm.getHost().getNetworkId() != targetHostNetId) {
				migrationMap.put(vm, this.getHostByNetId(targetHostNetId));
			}
		}
		MoveVmsToMultipleHostsAction migrations = new MoveVmsToMultipleHostsAction(migrationMap);
		this.addPendingAction(migrations);
		this.applyPendingActions();
	}
	
	public void setStep(int step) {
		this.step = step;
		this.time = this.step * 300 + 0.1;
	}
	
	public void loadStep() {
		// Apply migrations
		
		for(Vm vm : this.vmsMap.values()) {
			vm.updateVmProcessing(this.time, vm.getHost().getVmScheduler()
					.getAllocatedMipsForVm(vm));
		}
	}
	
	public Vm getVmByNetId(int vmNetId) {
		return this.vmsMap.get(vmNetId);
	}
	
	public PowerHost getHostByNetId(int hostNetId) {
		return this.hostsMap.get(hostNetId);
	}
	
	public List<Pair<Integer, Integer>> getIntegerPairPositionsList() {
		List<Pair<Integer, Integer>> pairList = new ArrayList<>();
		for(Entry<Vm, PowerHost> entry : vmHostMapResult.entrySet()) {
			pairList.add(Pair.of(entry.getKey().getNetworkId(), entry.getValue().getNetworkId()));
		}
		return pairList;
	}
	
	public Map<Integer, Integer> getIntegerPositionsMap() {
		Map<Integer, Integer> posMap = new HashMap<>();
		for(Entry<Vm, PowerHost> entry : vmHostMapResult.entrySet()) {
			posMap.put(entry.getKey().getNetworkId(), entry.getValue().getNetworkId());
		}
		return posMap;
	}
	
	public Map<Vm, PowerHost> getVmHostMapResult() {
		return vmHostMapResult;
	}
	
	public Map<Vm, PowerHost> getVmHostMapOriginal() {
		return vmHostMapOriginal;
	}

	public void addPendingAction(Action action) {
		this.pendingActions.add(action);
	}
	
	public void applyPendingActions() {
		while(!this.pendingActions.isEmpty()) {
			Action currentAction = this.pendingActions.poll();
			currentAction.apply(vmHostMapResult);
			this.appliedActions.add(currentAction);
		}
	}
	
	public void rollbackLastAction() {
		if(!this.appliedActions.isEmpty()) {
			Action currentAction = this.appliedActions.pop();
			currentAction.rollback(vmHostMapResult);
		}
	}
	
	public void rollbackAll() {
		for(Entry<Vm, PowerHost> entry : this.vmHostMapOriginal.entrySet()) {
			Vm vm = entry.getKey();
			PowerHost originalHost = entry.getValue();
			PowerHost currentHost = (PowerHost) vm.getHost();
			if(originalHost != currentHost) {
				if(currentHost != null) {
					currentHost.vmDestroy(vm);
				}
				if(originalHost != null) {
					originalHost.vmCreate(vm);
				}
			}
		}
		vmHostMapResult = new HashMap<>(vmHostMapOriginal);
		this.pendingActions.clear();
		this.appliedActions.clear();
	}
	
	private Map<Vm, PowerHost> getMigrationMap(Map<Vm, PowerHost> mapBefore, Map<Vm, PowerHost> mapAfter) {
		Map<Vm, PowerHost> migrationMap = new HashMap<>();
		for(Entry<Vm, PowerHost> entry : mapAfter.entrySet()) {
			Vm vm = entry.getKey();
			PowerHost targetHost = entry.getValue();
			PowerHost originHost = mapBefore.get(vm);
			
			if(originHost != targetHost) {
				migrationMap.put(vm, targetHost);
			}
		}		
		return migrationMap;
	}
	
	public Map<Vm, PowerHost> getMigrationMap() {
		return this.getMigrationMap(vmHostMapOriginal, vmHostMapResult);
	}
}
