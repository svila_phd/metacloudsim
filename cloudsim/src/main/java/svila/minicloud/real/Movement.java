package svila.minicloud.real;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public class Movement {
	PowerHost originHost;
	PowerHost targetHost;
	Vm vm;
	
	public Movement(PowerHost originHost, PowerHost targetHost, Vm vm) {
		this.originHost = originHost;
		this.targetHost = targetHost;
		this.vm = vm;
	}
}
