package svila.minicloud.real;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public class MoveAllVmsFromHostToHostAction extends Action {

	public MoveAllVmsFromHostToHostAction(PowerHost originHost, PowerHost targetHost) {
		super();
		for(Vm vm : originHost.getVmList()) {
			this.addMovement(originHost, targetHost, vm);
		}
	}
}