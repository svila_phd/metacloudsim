package svila.minicloud.real;

import java.util.List;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public class MoveVmsToHostAction extends Action {
	
	public MoveVmsToHostAction(PowerHost targetHost, List<Vm> vms) {
		super();
		for(Vm vm : vms) {
			this.addMovement((PowerHost) vm.getHost(), targetHost, vm);
		}
	}
}