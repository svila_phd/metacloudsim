package svila.minicloud.real;

import java.util.Map;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public class MoveVmsToMultipleHostsAction extends Action {
	
	public MoveVmsToMultipleHostsAction(Map<Vm, PowerHost> vmHostMap) {
		super();
		for(Entry<Vm, PowerHost> entry : vmHostMap.entrySet()) {
			Vm vm = entry.getKey();
			PowerHost originHost = (PowerHost)vm.getHost();
			PowerHost targetHost = entry.getValue();
			this.addMovement(originHost, targetHost, vm);
		}
	}
}
