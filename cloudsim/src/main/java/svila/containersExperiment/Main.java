package svila.containersExperiment;

import org.cloudbus.cloudsim.examples.container.ContainerOverbooking;
import org.cloudbus.cloudsim.examples.container.RunnerInitiator;

/**
 *
 * @author Sergi
 */
public class Main {
    public static void main(String[] args) {
        
        
        /* Host
            37274 MIPS per core
            Type 1 - 4 cores, 64 GB RAM, 86 W power idle, 117 W power max, 234 population
            Type 2 - 8 cores, 128 GB RAM, 93 W power idle, 135 W power max, 233 population
            Type 3 - 16 cores, 256 GB RAM, 66 W power idle, 247 W power max, 233 population
        
            VM
            18636 MIPS per core
            Type 1 - 2 cores, 1 GB RAM, 256 population
            Type 2 - 4 cores, 2 GB RAM, 256 population
            Type 3 - 1 cores, 4 GB RAM, 256 population
            Type 4 - 8 cores, 8 GB RAM, 256 population
        
            Container (1 core)
            Type 1 - 4658 MIPS, 128 GB RAM, 1666 population
            Type 2 - 9320 MIPS, 256 GB RAM, 1667 population
            Type 3 - 18636 MIPS, 512 GB RAM, 1667 population
        */
        
        boolean enableOutput = true;
        boolean outputToFile = false;
        /**
         * Getting the path of the planet lab workload that is included in the
         * cloudSim Package
         */
        String inputFolder = ContainerOverbooking.class.getClassLoader().getResource("workload/planetlab").getPath();
        /**
         * The output folder for the logs. The log files would be located in
         * this folder.
         */
        String outputFolder = "~/Results";
        /**
         * The allocation policy for VMs.
         */
        String vmAllocationPolicy = "MSThreshold-Under_0.80_0.70"; // DVFS policy without VM migrations
        /**
         * The selection policy for containers where a container migration is
         * triggered.
         */
//           String containerSelectionPolicy = "MaxUsage";
        String containerSelectionPolicy = "Cor";
        /**
         * The allocation policy used for allocating containers to VMs.
         */

//          String containerAllocationPolicy= "MostFull";
        String containerAllocationPolicy = "FirstFit";
        /**
         * The host selection policy determines which hosts should be selected
         * as the migration destination.
         */
        String hostSelectionPolicy = "FirstFit";
        /**
         * The VM Selection Policy is used for selecting VMs to migrate when a
         * host status is determined as "Overloaded"
         */
        String vmSelectionPolicy = "VmMaxC";
        /**
         * The container overbooking factor is used for overbooking resources of
         * the VM. In this specific case the overbooking is performed on CPU
         * only.
         */

        int OverBookingFactor = 80;
        int runTime = 0;

        new RunnerInitiator(
                enableOutput,
                outputToFile,
                inputFolder,
                outputFolder,
                vmAllocationPolicy,
                containerAllocationPolicy,
                vmSelectionPolicy,
                containerSelectionPolicy,
                hostSelectionPolicy,
                OverBookingFactor, Integer.toString(runTime), outputFolder);
        
    }
}
