package svila;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerDynamicWorkload;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.UtilizationModelNull;
import org.cloudbus.cloudsim.UtilizationModelPlanetLabInMemory;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmSchedulerTimeSharedOverSubscription;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;

/**
 *
 * @author Sergi
 */
public class Experiment2 extends Experiment {
    public Experiment2() {
        super(new CCE2());
    }
   
    public Experiment2(CustomConfiguration cc) {
        super(cc);
    }
    
    @Override
    public List<Cloudlet> createCustomCloudletList(int brokerId) {
        List<Cloudlet> list = new ArrayList<>();
            addCloudlet(0, cc.getCloudletFilepath(0), brokerId, list);
            addCloudlet(1, cc.getCloudletFilepath(1), brokerId, list);
            addCloudlet(2, cc.getCloudletFilepath(2), brokerId, list);
            return list;
    }

    @Override
    public void addCloudlet(int id, String cloudletPath, int brokerId, List<Cloudlet> list) {
        try {
            Cloudlet cloudlet = new Cloudlet(
                    id,
                    cc.getCloudletLength(),
                    cc.getCloudletPEs(),
                    300,
                    300,
                    //utilizationModelCpu
                    //new UtilizationModelStochastic(),
                    new UtilizationModelPlanetLabInMemory(
                            cloudletPath,
                            cc.getSchedulingInterval(), 11),
                    //utilizationModelNull,
                    //utilizationModelRam
                    new UtilizationModelNull(),
                    //utilizationModelBw
                    new UtilizationModelNull()
            );
            cloudlet.setUserId(brokerId);
            cloudlet.setVmId(id);
            list.add(cloudlet);
        } catch (NumberFormatException ex) {
            Logger.getLogger(Experiment1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Experiment1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List<Cloudlet> createCustomCloudletList(int brokerId, int cloudletsNumber) {
        List<Cloudlet> list = new ArrayList<>();
        addCloudlet(0, cc.getCloudletFilepath(0), brokerId, list);
        addCloudlet(1, cc.getCloudletFilepath(1), brokerId, list);
        addCloudlet(2, cc.getCloudletFilepath(2), brokerId, list);
        return list;
    }

    @Override
    public List<Vm> createCustomVmList(int brokerId, int vmsNumber) {
        List<Vm> vms = new ArrayList<>();
        addPowerVm(0, brokerId, 3, vms);
        addPowerVm(1, brokerId, 3, vms);
        addPowerVm(2, brokerId, 3, vms);
        return vms;
    }

    @Override
    public void addPowerVm(int id, int brokerId, int vmType, List<Vm> vms) {
        vms.add(new PowerVm(
					id,
					brokerId,
					cc.getVM_MIPS(vmType),
					cc.getVM_PES(vmType),
					cc.getVM_RAM(vmType),
					cc.getVM_BW(),
					cc.getVM_Size(),
					1,
					"Xen",
					new CloudletSchedulerDynamicWorkload(cc.getVM_MIPS(vmType), cc.getVM_PES(vmType)),
					cc.getSchedulingInterval()));
    }

    @Override
    public List<PowerHost> createCustomHostList() {
        List<PowerHost> hostList = new ArrayList<>();
        addPowerHostUtilizationHistory(0, 0, hostList);
        addPowerHostUtilizationHistory(1, 0, hostList);
        addPowerHostUtilizationHistory(2, 0, hostList);
        return hostList;
    }

    @Override
    public void addPowerHostUtilizationHistory(int id, int hostType, List<PowerHost> hostList) {
        List<Pe> peList = new ArrayList<>();
	for (int j = 0; j < cc.getHost_PES(hostType); j++) {
            peList.add(new Pe(j, new PeProvisionerSimple(cc.getHost_MIPS(hostType))));
	}
            
        hostList.add(new PowerHostUtilizationHistory(
					id,
					new RamProvisionerSimple(cc.getHost_RAM(hostType)),
					new BwProvisionerSimple(cc.getHost_BW()),
					cc.getHost_Storage(),
					peList,
					new VmSchedulerTimeSharedOverSubscription(peList),
					cc.getHostPower(hostType)));
    }
}
