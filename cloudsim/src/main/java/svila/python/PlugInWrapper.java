package svila.python;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class PlugInWrapper {
	
	PlugIn plugin;
	
	public PlugInWrapper(PlugIn plugin) {
		this.plugin = plugin;
	}
	
	public Map<String, Float[]> getBollinger(Float[] values, Float winma, Float alpha) {
		Float[] all = this.plugin.getBollinger(values, winma, alpha);
		Float[] lower = Arrays.copyOfRange(all, 0, all.length/2);
		Float[] upper = Arrays.copyOfRange(all, all.length/2, all.length);
		Map<String, Float[]> map = new HashMap<>();
		map.put("upper", upper);
		map.put("lower", lower);
		return map;
	}
	
	public void showPythonPath() {
		System.out.println(this.plugin.getPythonPath());
	}
	
	//public Float[] getAutoArima(Float[] values) {
		//this.plugin.getAutoArima(values);
	//}

}
