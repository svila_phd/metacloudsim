package svila.python;

import svila.python.SignalProcessingWrapper.SeasonalityData;

public class ForecastingWrapper {
	ForecastingPlugIn plugin;
	
	public ForecastingWrapper(ForecastingPlugIn plugin) {
		this.plugin = plugin;
	}
	
	public Float[] getAutoArima(Float[] values, int periodsForecasted, SeasonalityData seasonalityData) {
		return this.plugin.python_getAutoArima(values, periodsForecasted, seasonalityData.isSeasonal, seasonalityData.seasonalityValue);
	}
	
	public Float[] getSimpleExpSmoothing(Float[] values, int periodsForecasted) {
		return this.plugin.python_getSimpleExpSmoothing(values, periodsForecasted);
	}
	
	public Float[] getHoltExpSmoothing(Float[] values, int periodsForecasted, float smoothingLevelValue, float smoothingTrendValue) {
		return this.plugin.python_getHoltExpSmoothing(values, periodsForecasted, smoothingLevelValue, smoothingTrendValue);
	}
	
	public Float[] getHoltWintersExpSmoothing(Float[] values, int periodsForecasted, SeasonalityData seasonalityData,
			Float smoothing_level_value, Float smoothing_trend_value, Float smoothing_seasonal_value, Float damping_trend_value) {
		return this.plugin.python_getHoltWintersExpSmoothing(values, periodsForecasted, seasonalityData.isSeasonal, seasonalityData.seasonalityValue, 
				smoothing_level_value, smoothing_trend_value, smoothing_seasonal_value, damping_trend_value);
	}
	
	public Float[] getNeuralProphet(Float[] values, int periodsForecasted) {
		return this.plugin.python_getNeuralProphet(values, periodsForecasted);
	}
	
	public Float[] getFacebookProphet(Float[] values, int periodsForecasted) {
		return this.plugin.python_getFacebookProphet(values, periodsForecasted);
	}
	
	public Float[] getKNN(Float[] values, int periodsForecasted, int n_neighbors) {
		return this.plugin.python_getKNN(values, periodsForecasted, n_neighbors);
	}
	
	public Float[] getPercentile(Float[] values, int periodsForecasted, float perc) {
		return this.plugin.python_getPercentile(values, periodsForecasted, perc);
	}
}
