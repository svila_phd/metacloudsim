package svila.python;

import org.jpy.PyLib;
import org.jpy.PyModule;
import org.jpy.PyObject;

import svila.planetlabNetwork.StaticResources;

public class PythonBridge {
	
	public static boolean alreadyInit = false;
	
	//public static PlugInWrapper plugInWrapper;
	public static SignalProcessingWrapper signalProcessingWrapper;
	public static ForecastingWrapper forecastingWrapper;
	
	public static void init() {
		if(alreadyInit) {
			return;
		}
		alreadyInit = true;
		// Also check src/main/resources/jpyconfig.properties
		System.setProperty("jpy.jpyLib", StaticResources.jpyLib);
		System.setProperty("jpy.jdlLib", StaticResources.jdlLib);
		PyLib.startPython();
		PyModule.extendSysPath(StaticResources.pymodule, true);

		
		/*PyModule plugInModule = PyModule.importModule("pythonBinding");
		PyObject plugInClass = plugInModule.getAttribute("PythonBinding");
		PlugIn plugIn = plugInClass.createProxy(PlugIn.class);
		plugInWrapper = new PlugInWrapper(plugIn);
		*/
		
		PyModule signalProcessingPlugInModule = PyModule.importModule("signalProcessing");
		PyObject signalProcessingPlugInClass = signalProcessingPlugInModule.getAttribute("SignalProcessing");
		SignalProcessingPlugIn signalProcessingPlugIn = signalProcessingPlugInClass.createProxy(SignalProcessingPlugIn.class);
		signalProcessingWrapper = new SignalProcessingWrapper(signalProcessingPlugIn);
		
		PyModule forecastingPlugInModule = PyModule.importModule("forecastingTechniques");
		PyObject forecastingPlugInClass = forecastingPlugInModule.getAttribute("ForecastingTechniques");
		ForecastingPlugIn forecastingPlugIn = forecastingPlugInClass.createProxy(ForecastingPlugIn.class);
		forecastingWrapper = new ForecastingWrapper(forecastingPlugIn);
		
		System.out.println("Python loaded");
	}
}
