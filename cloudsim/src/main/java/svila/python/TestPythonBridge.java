package svila.python;

import org.apache.commons.lang3.tuple.Pair;

import svila.python.SignalProcessingWrapper.SeasonalityData;

public class TestPythonBridge {

	public static void main(String[] args) {
		
		PythonBridge.init();
		
		Float[] floatArray = {1.0f, 2.0f, 3.0f, 1.0f, 2.0f, 3.0f, 1.0f, 2.0f, 3.0f,
				1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,
				1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,
				1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,
				1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f};
		Float[] result = PythonBridge.signalProcessingWrapper.getInterPeaks(floatArray, 3);
		for(int i=0; i<result.length; i++) {
			System.out.print(result[i] + " ");
		}
		System.out.println("");
		SeasonalityData seasonalData = PythonBridge.signalProcessingWrapper.getSeasonality(floatArray);
		System.out.println(seasonalData);
		
		//Float[] forecasting = PythonBridge.forecastingWrapper.getAutoArima(floatArray, 6, seasonalData.getLeft(), seasonalData.getRight());
		//for(int i=0; i<forecasting.length; i++) {
		//	System.out.print(forecasting[i] + " ");
		//}
		System.out.println("PercForecasting");
		Float[] forecasting = PythonBridge.forecastingWrapper.getPercentile(floatArray, 6, 0.95f);
		for(int i=0; i<forecasting.length; i++) {
			System.out.print(forecasting[i] + " ");
		}
	}
}
