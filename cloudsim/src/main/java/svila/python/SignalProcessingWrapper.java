package svila.python;

import org.apache.commons.lang3.tuple.Pair;

public class SignalProcessingWrapper {
	SignalProcessingPlugIn plugin;
	
	public SignalProcessingWrapper(SignalProcessingPlugIn plugin) {
		this.plugin = plugin;
	}
	
	public Float[] getBollinger(Float[] values, int winma, float alpha) {
		return this.plugin.python_getBollinger(values, winma, alpha);
	}
	
	public Float[] getInterPeaks(Float[] values, int rolling, float zScoreThreshold) {
		return this.plugin.python_getInterPeaks(values, rolling, zScoreThreshold);
	}
	
	public Float[] getInterBollinger(Float[] values, int winma, float alpha, int rolling, float zScoreThreshold) {
		return this.plugin.python_getInterBollinger(values, winma, alpha, rolling, zScoreThreshold);
	}
	
	public class SeasonalityData {
		public boolean isSeasonal;
		public int seasonalityValue;
		
		public SeasonalityData(boolean isSeasonal, int seasonalityValue) {
			this.isSeasonal = isSeasonal;
			this.seasonalityValue = seasonalityValue;
		}
	}
	
	public SeasonalityData getSeasonality(Float[] values) {
		Integer[] result = this.plugin.python_getSeasonality(values);
		return new SeasonalityData(result[0] != 0, result[1]);
	}
}
