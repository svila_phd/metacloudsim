package svila.python;

public interface ForecastingPlugIn {
	public Float[] python_getAutoArima(Float[] values, int periodsForecasted, boolean isSeasonal, int seasonalityValue);
	public Float[] python_getSimpleExpSmoothing(Float[] values, int periodsForecasted);
	public Float[] python_getHoltExpSmoothing(Float[] values, int periodsForecasted, float smoothingLevelValue, float smoothingTrendValue);
	public Float[] python_getHoltWintersExpSmoothing(Float[] values, int periodsForecasted, boolean isSeasonal, int seasonalityValue,
			Float smoothing_level_value, Float smoothing_trend_value, Float smoothing_seasonal_value, Float damping_trend_value);
	public Float[] python_getNeuralProphet(Float[] values, int periodsForecasted);
	public Float[] python_getFacebookProphet(Float[] values, int periodsForecasted);
	public Float[] python_getKNN(Float[] values, int periodsForecasted, int n_neighbors);
	public Float[] python_getPercentile(Float[] values, int periodsForecasted, float perc);
}
