package svila.python;

public interface PlugIn {
	Float[] process(Float[] values);
	String[] getWorkloads();
	Float[] getBollingerUpper(Float[] values, Float winma, Float alpha);
	Float[] getBollinger(Float[] values, Float winma, Float alpha);
	
	Float[] getAutoArima(Float[] values);
	
	String getPythonPath();
}
