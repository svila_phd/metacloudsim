package svila.python;

public interface SignalProcessingPlugIn {
	public Float[] python_getBollinger(Float[] values, int winma, float alpha);
	public Float[] python_getInterPeaks(Float[] values, int rolling, float zScoreThreshold);
	public Float[] python_getInterBollinger(Float[] values, int winma, float alpha, int rolling, float zScoreThreshold);
	public Integer[] python_getSeasonality(Float[] values);
}
