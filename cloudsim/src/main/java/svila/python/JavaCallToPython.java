package svila.python;

import org.jpy.PyLib;
import org.jpy.PyModule;

import java.util.List;
import java.util.Map;

import org.jpy.PyDictWrapper;
import org.jpy.PyInputMode;
import org.jpy.PyObject;

public class JavaCallToPython {
	public static void main(String[] args) {
		
		System.setProperty("jpy.jpyLib", "C:\\Users\\Sergi\\Documents\\jpy\\build\\lib.win-amd64-3.8\\jpy.cp38-win_amd64.pyd");
		System.setProperty("jpy.jdlLib", "C:\\Users\\Sergi\\Documents\\jpy\\build\\lib.win-amd64-3.8\\jdl.cp38-win_amd64.pyd");
		PyLib.startPython();
		//PyObject.executeCode("print('Hello!')", PyInputMode.STATEMENT);
		PyObject.executeCode("import pandas as pd", PyInputMode.STATEMENT);
		PyObject.executeCode("import numpy as np",  PyInputMode.STATEMENT);
		PyObject.executeCode("pd.DataFrame()", PyInputMode.STATEMENT);
		PyModule.extendSysPath("C:/Users/Sergi/Documents/workspaceCloud2019/cloudsim_base/cloudsim/src/main/python", true);
		//PyObject.executeCode("import sys", PyInputMode.STATEMENT);
		//PyObject.executeCode("sys.path.append('C:/Users/Sergi/Documents/workspaceCloud2019/cloudsim_base/cloudsim/src/main/python')", PyInputMode.STATEMENT);
		//PyObject.executeCode("from pythonExampleBasic import basicFunction", PyInputMode.STATEMENT);

		//PyObject.executeCode("print(basicFunction(5))", PyInputMode.STATEMENT);
		PyModule module = PyModule.importModule("pythonExampleBasic");
		System.out.println(module.callMethod("basicFunction", 5));
		
		PyModule plugInModule = PyModule.importModule("pythonBinding");
		PyObject plugInClass = plugInModule.getAttribute("PythonBinding");
		//PyObject plugInObj = plugInClass.
		PlugIn plugIn = plugInClass.createProxy(PlugIn.class);
		
		PlugInWrapper plugInWrapper = new PlugInWrapper(plugIn);
		
		Float[] floatArray = {1.0f, 2.0f, 3.0f};
		Float[] result = plugIn.process(floatArray);
		System.out.println(result[0]);
		System.out.println(result[1]);
		String[] workloads = plugIn.getWorkloads();
		for(String w : workloads) {
			System.out.println(w);
		}
		Float[] floatArrayForBollinger = {1.0f, 2.0f, 3.0f, 1.0f, 2.0f, 3.0f, 1.0f, 2.0f, 3.0f,
				1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,
				1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,
				1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,
				1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f,1.0f, 2.0f, 3.0f};
		
		Map<String, Float[]> allBollinger = plugInWrapper.getBollinger(floatArrayForBollinger, 10f, 2f);
		System.out.println(allBollinger.keySet().toString());
		Float[] upperData = allBollinger.get("upper");
		for(int i=0; i < upperData.length; i++) {
			System.out.print(upperData[i] + " ");
		}		
		System.out.println("---");
		
		Float[] bollingerUpperResult = plugIn.getBollingerUpper(floatArrayForBollinger, 10f, 2f);
		for(int i=0; i < bollingerUpperResult.length; i++) {
			System.out.print(bollingerUpperResult[i] + " ");
		}
		
		
		
		//System.out.println(bollingerResult.keySet());
		//System.out.println(bollingerResult.get("lower"));
				
		
		PyLib.stopPython();
		
		/*
		PythonInterpreter interpreter = new PythonInterpreter();
		//interpreter.exec("import sys\nsys.path.append('pathToModules if they are not there by default')\nimport yourModule");
		interpreter.exec("import sys\nsys.path.append('C:\\Users\\Sergi\\Documents\\workspaceCloud2019\\cloudsim_base\\cloudsim\\src\\main\\python')\nfrom pythonExampleBasic import basicFunction");
		
		// execute a function that takes a string and returns a string
		PyObject someFunc = interpreter.get("basicFunction");
		PyObject result = someFunc.__call__();
		Integer realResult = (Integer) result.__tojava__(Integer.class);
		System.out.println(realResult);
		*/
	}
}
