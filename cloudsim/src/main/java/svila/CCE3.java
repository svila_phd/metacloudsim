package svila;

/**
 *
 * @author Sergi
 */
public class CCE3 extends CustomConfiguration {
public CCE3() {
        numVMs = 2;
        numHosts = 2;
        cloudletUtilizationSeed = -1;

        enableOutput = false;
        outputCSV    = false;

        //schedulingInterval = 300;
        //simulationLimit = 24 * 60 * 60;
        schedulingInterval = 100;
        simulationLimit = 1000;

        cloudletLength	= 2500 * (int) simulationLimit;
        numCloudletPEs	= 1;

        vmTypes	= 4;
        vmMips	= new int[] { 2500, 2000, 1000, 500 };
        vmPEs	= new int[] { 1, 1, 1, 1 };
        vmRAM	= new int[] { 870,  1740, 1740, 613 };
        vmBW		= 100000;
        vmSize		= 2500;

        numHostTypes	 = 2;
        hostMIPS	 = new int[] { 2500, 2000, 1000, 500 };
        hostPEs	 = new int[] { 1, 1, 1, 1 };
        hostRAM	 = new int[] { 4096, 4096, 4096, 4096 };
        hostBW		 = 1000000; 
        hostStorage = 1000000;

        lowerThreshold = 0.3;
        upperThreshold = 0.7;
        
        cloudlet1path = "C:\\Users\\Sergi\\Documents\\cloudsim4.0\\modules\\cloudsim\\target\\classes\\workload\\basic\\exp3_c1.txt";
        cloudlet2path = "C:\\Users\\Sergi\\Documents\\cloudsim4.0\\modules\\cloudsim\\target\\classes\\workload\\basic\\exp3_c2.txt";
        String cloudlet3path = "C:\\Users\\Sergi\\Documents\\cloudsim4.0\\modules\\cloudsim\\target\\classes\\workload\\basic\\exp3_c3.txt";
        cloudletFilepaths = new String[] {
                            cloudlet1path,
                            cloudlet2path,
                            cloudlet3path
        };
        }
}
