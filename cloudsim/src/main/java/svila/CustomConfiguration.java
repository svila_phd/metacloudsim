package svila;

import org.cloudbus.cloudsim.power.models.PowerModel;
import org.cloudbus.cloudsim.power.models.PowerModelSpecPowerHpProLiantMl110G4Xeon3040;
import org.cloudbus.cloudsim.power.models.PowerModelSpecPowerHpProLiantMl110G5Xeon3075;

/**
 *
 * @author Sergi
 */
public class CustomConfiguration {
    
    protected int numVMs = 2;
    protected int numHosts = 2;
    protected long cloudletUtilizationSeed = -1;
    
    protected boolean enableOutput = true;
    protected boolean outputCSV    = false;

    //protected double SCHEDULING_INTERVAL = 300;
    //protected double SIMULATION_LIMIT = 24 * 60 * 60;
    protected double schedulingInterval = 100;
    protected double simulationLimit = 1000;

    protected int cloudletLength	= 2500 * (int) simulationLimit;
    protected int numCloudletPEs	= 1;
    
    protected int vmTypes	= 4;
    protected int[] vmMips	= { 2500, 2000, 1000, 500 };
    protected int[] vmPEs	= { 1, 1, 1, 1 };
    protected int[] vmRAM	= { 870,  1740, 1740, 613 };
    protected int vmBW		= 100000;
    protected int vmSize		= 2500;
        
    protected int numHostTypes	 = 2;
    protected int[] hostMIPS	 = { 500, 2660 };
    protected int[] hostPEs	 = { 1, 1 };
    protected int[] hostRAM	 = { 4096, 4096 };
    protected int hostBW		 = 1000000; 
    protected int hostStorage = 1000000;
    
    protected PowerModel[] hostPower = {
	new PowerModelSpecPowerHpProLiantMl110G4Xeon3040(),
	new PowerModelSpecPowerHpProLiantMl110G5Xeon3075()
    };
    
    protected double THRESHOLD = 0.7;
    protected double lowerThreshold = 0.3;
    protected double upperThreshold = 0.7;
    
    protected String cloudlet1path = "C:\\Users\\Sergi\\Documents\\cloudsim4.0\\modules\\cloudsim\\target\\classes\\workload\\basic\\cloudlet1.txt";
    protected String cloudlet2path = "C:\\Users\\Sergi\\Documents\\cloudsim4.0\\modules\\cloudsim\\target\\classes\\workload\\basic\\cloudlet2.txt";
    
    protected String[] cloudletFilepaths = {
                            cloudlet1path,
                            cloudlet2path
    };
    
    public CustomConfiguration() {
        
    }
    
    public int getNumVms() {
        return numVMs;
    }
    
    public int getNumHosts() {
        return numHosts;
    }
    
    public long getCloudletUtilizationSeed() {
        return cloudletUtilizationSeed;
    }
    
    public boolean isEnableOutput() {
        return enableOutput;
    }
    
    public boolean isOutputCSV() {
        return outputCSV;
    }
    
    public double getSchedulingInterval() {
        return schedulingInterval;
    }
    
    public double getSimulationLimit() {
        return simulationLimit;
    }

    public int getCloudletLength() {
        return cloudletLength;
    }
    
    public int getCloudletPEs() {
        return numCloudletPEs;
    }
    
    public int getNumVMTypes() {
        return vmTypes;
    }
    
    public int getVM_MIPS(int i) {
        return vmMips[i];
    }
    
    public int getVM_PES(int i) {
        return vmPEs[i];
    }
    
    public int getVM_RAM(int i) {
        return vmRAM[i];
    }
    
    public int getVM_BW() {
        return vmBW;
    }
    
    public int getVM_Size() {
        return vmSize;
    }

    public int getNumHostTypes() {
        return numHostTypes;
    }
    
    public int getHost_MIPS(int i) {
        return hostMIPS[i];
    }
    
    public int getHost_PES(int i) {
        return hostPEs[i];
    }
    
    public int getHost_RAM(int i) {
        return hostRAM[i];
    }
    
    public int getHost_BW() {
        return hostBW;
    }
    
    public int getHost_Storage() {
        return hostStorage;
    }
    
    public PowerModel getHostPower(int i) {
        return hostPower[i];
    }
    
    public double getThreshold() {
        return THRESHOLD;
    }
    
    public double getLowerThreshold() {
        return lowerThreshold;
    }
    
    public double getUpperThreshold() {
        return upperThreshold;
    }
    
    public String getCloudletFilepath(int i) {
        return cloudletFilepaths[i];
    }
}
