package svila;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.power.PowerHost;

/**
 *
 * @author Sergi
 */
public class OneToOneVmAllocationPolicy extends VmAllocationPolicy {

    /** The map map where each key is a VM id and
         * each value is the host where the VM is placed. */
	private final Map<String, Host> vmTable = new HashMap<String, Host>();
    
    HashMap<Integer, Host> hostMap;
    
    public OneToOneVmAllocationPolicy(List<Host> list) {
        super(list);
        hostMap = new HashMap<>();
        for(Host host : list) {
            hostMap.put(host.getId(), host);
        }
    }

    @Override
    public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
	public boolean allocateHostForVm(Vm vm) {
		return allocateHostForVm(vm, findHostForVm(vm));
	}

    public Host findHostForVm(Vm vm) {
        return hostMap.get(vm.getId());
    }
        
    @Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		if (host == null) {
			Log.formatLine("%.2f: No suitable host found for VM #" + vm.getId() + "\n", CloudSim.clock());
			return false;
		}
		if (host.vmCreate(vm)) { // if vm has been succesfully created in the host
			getVmTable().put(vm.getUid(), host);
			Log.formatLine(
					"%.2f: VM #" + vm.getId() + " has been allocated to the host #" + host.getId(),
					CloudSim.clock());
			return true;
		}
		Log.formatLine(
				"%.2f: Creation of VM #" + vm.getId() + " on the host #" + host.getId() + " failed\n",
				CloudSim.clock());
		return false;
	}

    public Map<String, Host> getVmTable() {
	return vmTable;
    }
        
    @Override
	public void deallocateHostForVm(Vm vm) {
		Host host = getVmTable().remove(vm.getUid());
		if (host != null) {
			host.vmDestroy(vm);
		}
	}

    @Override
    public Host getHost(Vm vm) {
	return getVmTable().get(vm.getUid());
    }

    @Override
    public Host getHost(int vmId, int userId) {
	return getVmTable().get(Vm.getUid(userId, vmId));
    }

}
