package svila.GridCloudBridge;

import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Vm;

import geneticsimulator.Cluster;
import geneticsimulator.Job;
import gridsim.Machine;
import gridsim.MachineList;
import simulator.EnergyMachine;

public class FromCloudSimToGridSim {
	
	public static int baseCores;
	public static double energyIdle;
	public static double energyFull;
	
	public static Cluster getCluster(Vm vm) {
		/*return new Cluster(vm.getId(),
						   vm.getNumberOfPes(),
						   vm.getMips(),
						   1.0,
						   energyIdle * baseCores / vm.getNumberOfPes(),
						   energyFull * baseCores / vm.getNumberOfPes(),
						   energyIdle * baseCores / vm.getNumberOfPes(),
						   energyIdle * baseCores / vm.getNumberOfPes()
					);*/
		return new Cluster(vm.getId(),
		   vm.getNumberOfPes(),
		   vm.getMips(),
		   1.0,
		   vm.energyIdlePerCore *  vm.getNumberOfPes(),
		   vm.energyFullPerCore *  vm.getNumberOfPes(),
		   vm.energyIdlePerCore *  vm.getNumberOfPes(),
		   vm.energyFullPerCore *  vm.getNumberOfPes()
	);
	}
	
	public static EnergyMachine getEnergyMachine(Vm vm) {
		return new EnergyMachine(vm.getId(),
				vm.getNumberOfPes(),
				(int)vm.getMips(),
				1.0,
				vm.energyIdlePerCore *  vm.getNumberOfPes(),
				vm.energyFullPerCore *  vm.getNumberOfPes(),
				vm.energyIdlePerCore *  vm.getNumberOfPes(),
				vm.energyFullPerCore *  vm.getNumberOfPes());
	}
	
	public static MachineList getMachineList(List<Vm> vms) {
		MachineList machineList = new MachineList();
		ArrayList<Cluster> clusters = new ArrayList<>();
		for(Vm vm : vms) {
			machineList.add(getEnergyMachine(vm));
			clusters.add(getCluster(vm));
		}
		
		return machineList;
	}
	
	public static ArrayList<Cluster> getClusters(List<Vm> vms) {
		ArrayList<Cluster> clusters = new ArrayList<>();
		for(Vm vm : vms) {
			clusters.add(getCluster(vm));
		}
		
		return clusters;
	}
	
	public static Job getJob(Cloudlet cloudlet) {
		return new Job(cloudlet.getCloudletId(),
					   cloudlet.getNumberOfPes(),
					   cloudlet.getCloudletLength() / 1000.0,
					   1,
					   1.0
				);
	}
	
	public static ArrayList<Job> getJobs(List<Cloudlet> cloudlets) {
		ArrayList<Job> jobs = new ArrayList<>();
		for(Cloudlet cloudlet : cloudlets) {
			jobs.add(getJob(cloudlet));
		}
		
		return jobs;
	}
}
