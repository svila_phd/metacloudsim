package svila.GridCloudBridge;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerTimeShared;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.Vm;

import geneticsimulator.Cluster;
import geneticsimulator.Job;

public class FromGridSimToCloudSim {
	public static Cloudlet getCloudlet(Job job) {
		return new Cloudlet(job.getId(),
							(int)job.getTimeBase() * 1000,
							job.getRequirements(),
							1,
							1,
							new UtilizationModelFull(),
							new UtilizationModelFull(),
							new UtilizationModelFull()
						);
	}
	
	public static Vm getVM(Cluster cluster, int brokerId) {
		return new Vm(cluster.getId(),
					  brokerId,
					  cluster.getMIPS(),
					  cluster.getMaxResources(),
					  1, 1, 1, "Xen",
					  new CloudletSchedulerTimeShared()
				);
	}
}
