package svila;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerTimeShared;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.UtilizationModelNull;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmSchedulerTimeShared;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;
import svila.Brokers.MESDBroker;
import svila.Brokers.RandomBroker;
import svila.Brokers.RoundRobinBroker;

/**
 *
 * @author Sergi
 */
public class AlgorithmComparator {
    static DatacenterBroker broker;
    static List<Cloudlet> cloudletList;
    static List<Vm> vmList;
    static List<Host> hostList;
    static Random random;
    
    static int numVms = 5;
    static int numCloudlets = 1000;
    static int minMipsVM = 100, maxMipsVM = 2000;
    static int minMipsCloudlet = 500, maxMipsCloudlet = 100000;
    static int randomSeed = 0;
    
    public static void main(String[] args) throws IOException {
        random = new Random(randomSeed);
        
        /*boolean enableOutput = true;
        boolean outputToFile = false;
	String inputFolder = PlanetLabExecuter.class.getClassLoader().getResource("workload/planetlab").getPath();
	String outputFolder = "output";
	String workload = "20110303"; // PlanetLab workload
        */
        
        //broker = createRandomBroker();
        //broker = createRoundRobinBroker();

        launchExperiment("mesd");
        System.in.read();
        launchExperiment("rr");
        System.in.read();
        launchExperiment("random");
    }
    
    public static void launchExperiment(String brokerType) {
        try {
            int num_user = 1;
            Calendar calendar = Calendar.getInstance();
            boolean trace_flag = false;

            CloudSim.init(num_user, calendar, trace_flag);
            
            vmList = new ArrayList<Vm>();
            
            broker = createMESDBroker();
            //broker = createRandomBroker();
            
            Datacenter datacenter0 = createDatacenter("Datacenter_0");

            // submit vm list to the broker
            broker.submitVmList(vmList);

            // Fifth step: Create one Cloudlet
            cloudletList = new ArrayList<Cloudlet>();

            createRandomCloudlets();

            // submit cloudlet list to the broker
            broker.submitCloudletList(cloudletList);

            // Sixth step: Starts the simulation
            CloudSim.startSimulation();

            CloudSim.stopSimulation();

            //Final step: Print results when simulation is over
            List<Cloudlet> newList = broker.getCloudletReceivedList();
            printCloudletList(newList);
            Log.printConcatLine("Finished experiment with " + brokerType +" broker ");
	} catch (Exception e) {
            e.printStackTrace();
            Log.printLine("Unwanted errors happen");
	}
    }
    
    private static DatacenterBroker createBroker(String brokerType) {
        switch(brokerType) {
            case "mesd":
                return createMESDBroker();
            case "rr":
                return createRoundRobinBroker();
            case "random":
                return createRandomBroker();
        }
        Log.printConcatLine("Invalid broker type " + brokerType);
        System.exit(-1);
        return null;
    }
    
    private static DatacenterBroker createMESDBroker() {
		DatacenterBroker broker = null;
		try {
			broker = new MESDBroker("Broker");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
        return broker;
    }

    private static DatacenterBroker createRandomBroker() {
        RandomBroker broker = null;
        try {
            broker = new RandomBroker("Broker");
            broker.setRandomSeed(1);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return broker;
    }
    
    private static DatacenterBroker createRoundRobinBroker() {
        DatacenterBroker broker = null;
        try {
            broker = new RoundRobinBroker("Broker");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return broker;
    }

    private static Datacenter createDatacenter(String name) {

		// Here are the steps needed to create a PowerDatacenter:
		// 1. We need to create a list to store
		// our machine
		List<Host> hostList = new ArrayList<Host>();

		// 2. A Machine contains one or more PEs or CPUs/Cores.
		// In this example, it will have only one core.
		List<Pe> peList = new ArrayList<Pe>();
                
                int ram = 1000000; // host memory (MB)
		long storage = 1000000; // host storage
		int bw = 1000000;
                
                for(int i=0; i<numVms; i++) {
                    int mips = random.nextInt(maxMipsVM-minMipsVM) + minMipsVM;
                    // Bug: Segons els MIPS del PE, la allocation pot fallar tot i haver espai aparentment
                    peList.add(new Pe(i, new PeProvisionerSimple(1000000))); // need to store Pe id and MIPS Rating
                    hostList.add(
			new Host(
				i,
				new RamProvisionerSimple(ram),
				new BwProvisionerSimple(bw),
				storage,
				peList,
				new VmSchedulerTimeShared(peList)
			)
                    );
                    
                    Vm vm = new Vm(i, broker.getId(), mips, 1, 0, 0, 1, "Xen", new CloudletSchedulerTimeShared());
                    vmList.add(vm);
                    
                    System.out.println(i + " " + mips);
                }
                

		String arch = "x86"; // system architecture
		String os = "Linux"; // operating system
		String vmm = "Xen";
		double time_zone = 10.0; // time zone this resource located
		double cost = 3.0; // the cost of using processing in this resource
		double costPerMem = 0.05; // the cost of using memory in this resource
		double costPerStorage = 0.001; // the cost of using storage in this
										// resource
		double costPerBw = 0.0; // the cost of using bw in this resource
		LinkedList<Storage> storageList = new LinkedList<Storage>(); // we are not adding SAN
													// devices by now

		DatacenterCharacteristics characteristics = new DatacenterCharacteristics(
				arch, os, vmm, hostList, time_zone, cost, costPerMem,
				costPerStorage, costPerBw);

		// 6. Finally, we need to create a PowerDatacenter object.
		Datacenter datacenter = null;
		try {                
                        
			datacenter = new Datacenter(name, characteristics, new OneToOneVmAllocationPolicy(hostList), storageList, 0);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return datacenter;
	}
    
    public static void createRandomCloudlets() {
        int pesNumber = 1;
        long fileSize = 300;
        long outputSize = 300;
        UtilizationModel utilizationModelFull = new UtilizationModelFull();
        UtilizationModel utilizationModelNull = new UtilizationModelNull();

        for(int i=0; i<numCloudlets; i++) {
            long length = random.nextInt(maxMipsCloudlet-minMipsCloudlet) + minMipsCloudlet;
            Cloudlet cloudlet = 
                new Cloudlet(i, length, pesNumber, fileSize, 
                            outputSize, utilizationModelFull, utilizationModelNull, utilizationModelNull);
            cloudlet.setUserId(broker.getId());
            cloudletList.add(cloudlet);
        }
    }
    
    private static void printCloudletList(List<Cloudlet> list) {
        int size = list.size();
        Cloudlet cloudlet;

        String indent = "    ";
        Log.printLine();
        Log.printLine("========== OUTPUT ==========");
        Log.printLine("Cloudlet ID" + indent + "STATUS" + indent
                + "Data center ID" + indent + "VM ID" + indent + "Time" + indent
                + "Start Time" + indent + "Finish Time");

        DecimalFormat dft = new DecimalFormat("###.##");
        for (int i = 0; i < size; i++) {
            cloudlet = list.get(i);
            Log.print(indent + cloudlet.getCloudletId() + indent + indent);

            if (cloudlet.getCloudletStatus() == Cloudlet.SUCCESS) {
                Log.print("SUCCESS");

                Log.printLine(indent + indent + cloudlet.getResourceId()
                        + indent + indent + indent + cloudlet.getVmId()
                        + indent + indent
                        + dft.format(cloudlet.getActualCPUTime()) + indent
                        + indent + dft.format(cloudlet.getExecStartTime())
                        + indent + indent
                        + dft.format(cloudlet.getFinishTime()));
            }
        }
    }
}
