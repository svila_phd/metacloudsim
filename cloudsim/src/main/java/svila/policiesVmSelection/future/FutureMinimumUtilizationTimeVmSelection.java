package svila.policiesVmSelection.future;

import java.util.List;

import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;

public class FutureMinimumUtilizationTimeVmSelection extends FutureVmSelectionBase {

	@Override
	public FutureVm getVmToMigrate(FutureHost host) {
		List<FutureVm> migratableVms = getMigratableVms(host);
		if (migratableVms.isEmpty()) {
			return null;
		}
		FutureVm vmToMigrate = null;
		double minMetric = Double.MAX_VALUE;
		for (FutureVm vm : migratableVms) {
			if (vm.getCurrentVm().isInMigration()) {
				continue;
			}
			//Original calc: double metric = vm.getTotalUtilizationOfCpuMips(CloudSim.clock()) / vm.getMips();
			double metric = vm.getSmoothingMIPS() / vm.getCurrentVm().getMips();
			if (metric < minMetric) {
				minMetric = metric;
				vmToMigrate = vm;
			}
		}
		return vmToMigrate;
	}
}
