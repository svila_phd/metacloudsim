package svila.policiesVmSelection.future;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;

import svila.policiesHostOverSaturation.future.OverSaturationData;
import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;
import svila.vmOptimizerFix.VMOptimizerPolicy;
import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public abstract class FutureVmSelectionBase {
	
	public enum Mode {FIRST_CHECK, CHANGE_CHECK};
	
	Mode mode;
	FutureVMOptimizerPolicy vmOptimizerPolicy;
	private PowerDatacenter powerDatacenter;

	public FutureVmSelectionBase() {
		this.mode = Mode.FIRST_CHECK;
	}
	
	public FutureVmSelectionBase(Mode mode) {
		this.mode = mode;
	}
	
	public Mode getMode() {
		return mode;
	}
	
	public void setMode(String mode) {
		switch(mode) {
			case "change":
				this.mode = Mode.CHANGE_CHECK;
			case "first":
			default:
				this.mode = Mode.FIRST_CHECK;
		}
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	public VMOptimizerPolicy getVmOptimizerPolicy() {
		return vmOptimizerPolicy;
	}

	public void setVmOptimizerPolicy(FutureVMOptimizerPolicy vmOptimizerPolicy) {
		this.vmOptimizerPolicy = vmOptimizerPolicy;
	}
	
	public abstract FutureVm getVmToMigrate(FutureHost host);
	
	// The usable VMs are the VMs from the future list
	// The list of VMs from the current host is ever the same, the futureVMs has the changes
	protected List<FutureVm> getMigratableVms(FutureHost host) {
		List<FutureVm> migratableVms = new ArrayList<>();
		for (FutureVm vm : host.getFutureVms().values()) {
			if (!vm.getCurrentVm().isInMigration()) {
				migratableVms.add(vm);
			}
		}
		return migratableVms;
	}
	
	public HashMap<Integer, FutureVm> getVmsToMigrateFromHosts(Set<FutureHost> hosts) {
		HashMap<Integer, FutureVm> vmsToMigrate = new HashMap<>();
		for(FutureHost host : hosts) {
			/*if(host.getCurrentHost().getId() == 14 && CloudSim.clock() >= 55200.0 && CloudSim.clock() < 59000.0) {
				System.out.println("");
			}*/
			List<FutureVm> currentVms = getVmsToMigrate(host);
			for(FutureVm vm : currentVms) {
				vmsToMigrate.put(vm.getCurrentVm().getNetworkId(), vm);
			}
		}
		return vmsToMigrate;
	}
	
	public List<FutureVm> getVmsToMigrate(FutureHost host) {
		switch(mode) {
			case CHANGE_CHECK:
				return getVmsToMigrateWithChangeCheck(host);
			case FIRST_CHECK:
			default:
				return getVmsToMigrateWithFirstCheck(host);
		}
	}
	
	public List<FutureVm> getVmsToMigrateWithFirstCheck(FutureHost host) {
		OverSaturationData osd = this.vmOptimizerPolicy.futureHostOverSaturationPolicy.getOverSaturationData(host);
		List<FutureVm> vmsToMigrate = new ArrayList<>();
		double hostMips = osd.futureUtilization * host.getCurrentHost().getTotalMips();
		double limitMips = osd.upperThreshold * host.getCurrentHost().getTotalMips();

		while(hostMips > limitMips) {
			FutureVm futureVm = getVmToMigrate(host);
			if(futureVm == null) {
				break;
			}
			host.popVm(futureVm);
			double vmMips = futureVm.getForecastedMIPS();
			hostMips -= vmMips;
			vmsToMigrate.add(futureVm);
		}

		return vmsToMigrate;
	}
	
	public List<FutureVm> getVmsToMigrateWithChangeCheck(FutureHost host) {
		OverSaturationData osd = this.vmOptimizerPolicy.futureHostOverSaturationPolicy.getOverSaturationData(host);
		List<FutureVm> vmsToMigrate = new ArrayList<>();
		while(osd.existUpperThresholdSaturation) {
			FutureVm futureVm = getVmToMigrate(host);
			if(futureVm == null) {
				break;
			}
			host.popVm(futureVm);
			osd = this.vmOptimizerPolicy.futureHostOverSaturationPolicy.getOverSaturationData(host);
		}
		return vmsToMigrate;
	}

	public PowerDatacenter getPowerDatacenter() {
		return powerDatacenter;
	}

	public void setPowerDatacenter(PowerDatacenter powerDatacenter) {
		this.powerDatacenter = powerDatacenter;
	}
}
