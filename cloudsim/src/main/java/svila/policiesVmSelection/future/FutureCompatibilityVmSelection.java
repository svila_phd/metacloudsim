package svila.policiesVmSelection.future;

import org.cloudbus.cloudsim.Vm;

import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;
import svila.vmOptimizerFix.VMSelectionPolicy;
import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public class FutureCompatibilityVmSelection extends FutureVmSelectionBase {

	VMSelectionPolicy vmSelectionPolicy;
	
	public FutureCompatibilityVmSelection(VMSelectionPolicy vmSelectionPolicy) {
		this.vmSelectionPolicy = vmSelectionPolicy;
	}
	
	@Override
	public FutureVm getVmToMigrate(FutureHost host) {
		// host: currentHosts VMs - FutureVms out + FutureVms in
		
		Vm vm = this.vmSelectionPolicy.getVmToMigrate(host.getCurrentHost());
		FutureVm futureVm = host.getFutureVms().get(vm.getNetworkId());
		
		// host: currentHost VMs + FutureVms out - FutureVms in
		
		return futureVm;
	}

	public void setVmOptimizerPolicy(FutureVMOptimizerPolicy vmOptimizerPolicy) {
		this.vmOptimizerPolicy = vmOptimizerPolicy;
		this.vmSelectionPolicy.setAllocationPolicy(vmOptimizerPolicy);
	}
}
