package svila.policiesVmSelection.future;

import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;

public class FutureNoVmSelection extends FutureVmSelectionBase {
	@Override
	public FutureVm getVmToMigrate(final FutureHost host) {
		return null;
	}
}
