package svila.policiesVmSelection.future;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVm;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.Graph;
import svila.planetlabNetwork.Node;
import svila.planetlabNetwork.PowerVmData;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;
import svila.planetlabNetwork.correlation.Correlation;
import svila.planetlabNetwork.correlation.CorrelationValue;
import svila.planetlabNetwork.correlation.NetworkBandwidthValues;
import svila.planetlabNetwork.results.CorrelationsData;
import svila.planetlabNetwork.results.CorrelationsResults;
import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;
import svila.vmOptimizerFix.VMSelectionPolicy;

public abstract class FutureVmSelectionPolicyCorrelation extends FutureVmSelectionBase {
	private FutureVmSelectionBase fallbackPolicy;
	protected Correlation correlation;
	double thresholdHeuristic;

	public FutureVmSelectionPolicyCorrelation(Correlation correlation, final FutureVmSelectionBase fallbackPolicy) {
		super();
		this.correlation = correlation;
		setFallbackPolicy(fallbackPolicy);
		
		checkTunning();
	}
	
	protected void checkTunning() {
		ExperimentConfiguration ec = StaticResources.getCE();
		if(ec.isTunning()) {
			this.thresholdHeuristic = ec.getTunningValue();
		}
	}

	public FutureVmSelectionBase getFallbackPolicy() {
		return fallbackPolicy;
	}

	public double getThresholdHeuristic() {
		return thresholdHeuristic;
	}

	public void setThresholdHeuristic(double thresholdHeuristic) {
		this.thresholdHeuristic = thresholdHeuristic;
	}

	public void setFallbackPolicy(final FutureVmSelectionBase fallbackPolicy) {
		this.fallbackPolicy = fallbackPolicy;
	}

	protected void checkDatacenter() {
		if (this.getPowerDatacenter() == null) {
			this.setPowerDatacenter(this.vmOptimizerPolicy.getPowerDatacenter());
		}
	}

	public void printCorrelationDecision(Map<Integer, CorrelationValue> cpuPearsonValues,
			Map<Integer, CorrelationValue> networkPearsonValues, HashMap<PowerVm, Double> heuristicValues,
			PowerVm selectedVm) {
		DecimalFormat decimalFormat = new DecimalFormat("0.00");
		DecimalFormat idFormat = new DecimalFormat("00");

		for (Entry<Integer, CorrelationValue> entry : cpuPearsonValues.entrySet()) {
			PowerVm vm = entry.getValue().vm;
			Double cpuValue = entry.getValue().correlationValue;
			CorrelationValue networkPearson = networkPearsonValues.get(entry.getKey());
			Double networkValue = networkPearson.correlationValue;
			Double heuristicValue = heuristicValues.get(vm);
			String s = "VM: id: " + idFormat.format(vm.getId()) + " netId: " + idFormat.format(vm.getNetworkId())
					+ " CPU correlation value: " + decimalFormat.format(cpuValue) + " Network correlation value: "
					+ decimalFormat.format(networkValue) + " Heuristic value: " + decimalFormat.format(heuristicValue);
			if (vm == selectedVm) {
				s += " Chosen VM";
			}
			System.out.println(s);
		}
		System.out.println();
	}
	
	private Double getSum(List<Double> list) {
		Double sum = 0.0;
		for(Double d : list) {
			sum += d;
		}
		return sum;
	}

	protected Map<Integer, CorrelationValue> getCpuValues(FutureHost futureHost, List<PowerVm> migratableVms) {
		List<Snapshot> snapshots = getPowerDatacenter().snapshots;
		Map<Integer, CorrelationValue> cpuValues = new HashMap<>();
		int numHistory = StaticResources.getCE().getMigrationInterval();
		List<Double> hostHistoryMips = new ArrayList<>();
		PowerHost powerHost = futureHost.getCurrentHost();
		for (int i = 1; i <= numHistory; i++) {
			Snapshot snapshot = snapshots.get(snapshots.size() - i);
			Double hostMips = snapshot.getPowerHostDataMap().get(powerHost.getId()).getRequestedMips();
			hostHistoryMips.add(hostMips);
		}
		Double hostCpuSum = getSum(hostHistoryMips);
		for (FutureVm futureVm : futureHost.getFutureVms().values()) {
			PowerVm vm = futureVm.getCurrentVm();
			List<Double> currentVmCpuValues = new ArrayList<>();
			for (int i = 1; i <= numHistory; i++) {
				Snapshot snapshot = snapshots.get(snapshots.size() - i);
				PowerVmData powerVmData = snapshot.getPowerHostDataMap().get(powerHost.getId()).getVmsData()
						.get(vm.getId());
				Double requestedMips = powerVmData.getRequestedMips();
				currentVmCpuValues.add(requestedMips);
			}
			Double vmCpuSum = getSum(currentVmCpuValues);

			CorrelationValue cv = new CorrelationValue((PowerVm) vm, correlation.correlation(currentVmCpuValues, hostHistoryMips));
			cv.setSumValues(vmCpuSum, hostCpuSum);
			
			if(Double.isNaN(cv.correlationValue)) {
				cv.correlationValue = 0.0;
			}
			cpuValues.put(vm.getNetworkId(), cv);
		}

		return cpuValues;
	}

	protected Map<Integer, CorrelationValue> getNetworkValues(FutureHost futureHost, List<PowerVm> migratableVms) {
		List<NetworkBandwidthValues> nbv = new ArrayList<>();
		List<Snapshot> snapshots = getPowerDatacenter().snapshots;
		int numHistory = StaticResources.getCE().getMigrationInterval();

		Map<Integer, PowerVm> vmIdsInHost = new HashMap<>();
		for (FutureVm vm : futureHost.getFutureVms().values()) {
			vmIdsInHost.put(vm.getCurrentVm().getNetworkId(), vm.getCurrentVm());
		}

		for (int i = 1; i <= numHistory; i++) {
			NetworkBandwidthValues currentNBV = new NetworkBandwidthValues();
			Snapshot snapshot = snapshots.get(snapshots.size() - i);
			Graph interactions = snapshot.getLogicalNetwork();
			for (PowerVm vm : migratableVms) {
				if (interactions.getNodeById(vm.getNetworkId()) == null) { // If the VM has no interactions
					currentNBV.addRelation(vm, vm, 0f);
					continue;
				}
				Node vmNode = interactions.getNodeById(vm.getNetworkId());
				Map<Node, Float> adjacentNodes = vmNode.getAdjacentNodes();
				for (Entry<Node, Float> entry : adjacentNodes.entrySet()) {
					Node otherNode = entry.getKey();
					float bandwidth = entry.getValue() / 2; // Due to this process i made twice
					if (vmIdsInHost.containsKey(vmNode.getId()) && vmIdsInHost.containsKey(otherNode.getId())) {
						currentNBV.addRelation(vm, vmIdsInHost.get(otherNode.getId()), bandwidth);
					} else {
						currentNBV.addRelation(vm, vm, 0f);
					}
				}
			}
			nbv.add(currentNBV);
		}

		List<Double> hostBandwidthHistory = new ArrayList<>();
		for (NetworkBandwidthValues currentNBV : nbv) {
			hostBandwidthHistory.add(currentNBV.getInternalCommunicationsBandwith().doubleValue());
		}
		Double hostNetworkSum = getSum(hostBandwidthHistory);
		Map<Integer, CorrelationValue> networkValues = new HashMap<>();

		for (PowerVm vm : migratableVms) {
			List<Double> vmBandwidthHistory = new ArrayList<>();
			for (NetworkBandwidthValues currentNBV : nbv) {
				if(currentNBV.getRelationMap().get(vm) == null) {
					vmBandwidthHistory.add(0.0);
				} else {
					vmBandwidthHistory.add(currentNBV.getRelationMap().get(vm).doubleValue());
				}
			}
			Double vmNetworkSum = getSum(vmBandwidthHistory);

			CorrelationValue cv = new CorrelationValue(vm, correlation.correlation(vmBandwidthHistory, hostBandwidthHistory));
			cv.setSumValues(vmNetworkSum, hostNetworkSum);
			if(Double.isNaN(cv.correlationValue)) {
				cv.correlationValue = 0.0;
			}
			networkValues.put(vm.getNetworkId(), cv);
		}

		return networkValues;
	}

	protected void addCorrelationsData(Map<Integer, CorrelationValue> cpuPearsonValues,
			Map<Integer, CorrelationValue> networkPearsonValues,
			HashMap<PowerVm, Double> heuristicValues, PowerVm migratedVm) {
		for(Entry<PowerVm,Double> entry : heuristicValues.entrySet()) {
			PowerVm currentVm = entry.getKey();
			int vmNetId = currentVm.getNetworkId();
			int hostNetId = currentVm.getHost().getNetworkId();
			int vmId = currentVm.getId();
			int hostId = currentVm.getHost().getId();
			double cpuCorr = cpuPearsonValues.get(vmNetId).correlationValue;
			double netCorr = networkPearsonValues.get(vmNetId).correlationValue;
			double hVal = entry.getValue();
			int snapshotNum = this.getPowerDatacenter().snapshots.size()-1;
			int migratedVmId = migratedVm==null ? -1 : migratedVm.getId();
			CorrelationsData cd = new CorrelationsData(snapshotNum, hostId, vmId,
					hostNetId, vmNetId, cpuCorr, netCorr, hVal, migratedVmId);
			CorrelationsResults.getInstance().add(cd);
		}
	}

	public abstract FutureVm getVmToMigrate(FutureHost host);
}
