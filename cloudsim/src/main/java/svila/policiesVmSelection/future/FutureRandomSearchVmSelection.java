package svila.policiesVmSelection.future;

import java.util.List;
import java.util.Random;

import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;

public class FutureRandomSearchVmSelection extends FutureVmSelectionBase {
	@Override
	public FutureVm getVmToMigrate(FutureHost host) {
		List<FutureVm> migratableVms = getMigratableVms(host);
		if (migratableVms.isEmpty()) {
			return null;
		}
		int index = (new Random()).nextInt(migratableVms.size());
		return migratableVms.get(index);
	}
}
