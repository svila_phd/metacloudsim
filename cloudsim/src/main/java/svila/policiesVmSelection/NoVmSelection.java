package svila.policiesVmSelection;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.vmOptimizerFix.VMSelectionPolicy;

public class NoVmSelection extends VMSelectionPolicy {
	@Override
	public Vm getVmToMigrate(final PowerHost host) {
		return null;
	}
}
