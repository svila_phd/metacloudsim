package svila.policiesVmSelection;

import java.util.List;
import java.util.Random;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVm;

import svila.vmOptimizerFix.VMSelectionPolicy;

public class RandomSearchVmSelection extends VMSelectionPolicy {

	@Override
	public Vm getVmToMigrate(PowerHost host) {
		List<PowerVm> migratableVms = getMigratableVms(host);
		if (migratableVms.isEmpty()) {
			return null;
		}
		int index = (new Random()).nextInt(migratableVms.size());
		return migratableVms.get(index);
	}

}
