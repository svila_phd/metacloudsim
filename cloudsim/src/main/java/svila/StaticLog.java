package svila;

import java.util.List;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletScheduler;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.ResCloudlet;
import org.cloudbus.cloudsim.Vm;

/**
 *
 * @author Sergi
 */
public class StaticLog {
    public final static boolean enabledCustomLog = false;
    
    public static void showHostInfo(List<? extends Host> hosts) {
        if(!enabledCustomLog) {
            return;
        }
        
        double usedMIPS;
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        for(Host host : hosts) {
            int currentHostId = host.getId();
            usedMIPS = host.getTotalMips()-host.getAvailableMips();
            Log.formatLine(
						"[Host #%d] %.2f / %d MIPS (%.2f%%)",
						currentHostId,
						usedMIPS,
						host.getTotalMips(),
                                                usedMIPS/host.getTotalMips()*100);
            for(Vm vm : host.getVmList()) {
                if(vm.isInMigration()) {
                    int newHostId = vm.getHost().getId();
                    if(currentHostId == newHostId) {
                        Log.formatLine("    [VM #%d] Migrating to this host",
                                vm.getId(),
                                vm.getHost().getId());
                    } else {
                        Log.formatLine("    [VM #%d] Migrating to Host #%d",
                                vm.getId(),
                                vm.getHost().getId());
                    }
                } else {
                    Log.formatLine("    [VM #%d] ",
                                vm.getId());
                }
                
                CloudletScheduler cloudletScheduler = vm.getCloudletScheduler();
                for(ResCloudlet resCloudlet : cloudletScheduler.getCloudletExecList()) {
                    Cloudlet cloudlet = resCloudlet.getCloudlet();
                    Log.formatLine("        [Cloudlet #%d] %d",
                                   cloudlet.getCloudletId(), cloudlet.getStatus());
                }
            }
        }
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }
}
