package svila.planetlabNetwork.Executers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import svila.planetlabNetwork.StaticResources;

public class FolderReceiver {
	public static void main(String[] args) {
		StaticResources.init();
		
		String experimentName = "minPRTesting";
		
		
		
		
		
		String remoteOutputPath = "$HOME/cloud/networkExperiments/output/" + experimentName;
		
		Path localOutputPath = Paths.get(StaticResources.baseFolder,
				"output",
				experimentName);
		
		execute(localOutputPath, remoteOutputPath);
	}
	
	public static void execute(Path localOutputPath, String remoteOutputPath) {
		System.out.println("Obtaining folder from cluster: " + remoteOutputPath);
		System.out.println("To copy to the local folder: " + localOutputPath);
		
//		try {
//			Files.createDirectories(localOutputPath);
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
		SSHConnector sshc = new SSHConnector();
		try {
			sshc.connect("svila", "maracasdoctorat", "maracas.udl.net", 22);
			
			String remoteOutputParent = Paths.get(remoteOutputPath).getParent().toString().replace('\\', '/');
			String localFilename = localOutputPath.toFile().getName();
			
			//sshc.executeCommand("zip -j -r " + remoteOutputPath + ".zip" + " " + remoteOutputPath);
			//System.out.println("Compressed remote file: " + remoteOutputPath + ".zip");
			System.out.println("Compressed remote file: " + localFilename + ".zip");

			sshc.executeCommand("cd " + remoteOutputParent + " && " + "zip -r " +
								localFilename + ".zip" + " " + localFilename +
								" && cd -");

			//System.out.println("Trying to copy the next files:");
			//String filesToCopyRaw = sshc.executeCommand("ls " + remoteOutputPath);
			//new File(localOutputPath.toString()).mkdirs();
			//String[] filenames = filesToCopyRaw.split("\n");
			//for(String remoteFilename : filenames) {
				//System.out.println("File: " + remoteOutputPath + "/" + remoteFilename);
			
			copyRemoteToLocal(sshc.session, remoteOutputParent + "/", localOutputPath.getParent().toString().replace('\\', '/')+"/", localFilename+".zip");
			//}
			System.out.println("Unzip of file: " + localOutputPath.toString() + ".zip");
			//unzip(new File(localOutputPath.toString() + ".zip"),
			//	  new File(localOutputPath.getParent().toString()));
			unzip(new File(localOutputPath.toString() + ".zip").toString(),
				  new File(localOutputPath.getParent().toString()).toString());
			
			sshc.executeCommand("rm " + remoteOutputPath + ".zip");
			sshc.disconnect();
			System.out.println("Session disconnected. All files transfered.");
			removeCompressedFile(new File(localOutputPath.toString() + ".zip").toPath());
		} catch (IllegalAccessException | JSchException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void removeCompressedFile(Path path) {
		try {
			Files.deleteIfExists(path);
			System.out.println("Removed compressed file: " + path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void unzip(String source, String destination) {
		try {
		    ZipFile zipFile = new ZipFile(source);
		    zipFile.extractAll(destination);
		} catch (ZipException e) {
		    e.printStackTrace();
		}
	}
	
	// https://www.baeldung.com/java-compress-and-uncompress
	/*
	private static void unzip(File file, File outputFolder) {
        byte[] buffer = new byte[4096]; // Original 1024
        try {
	        ZipInputStream zis = new ZipInputStream(new FileInputStream(file));
	        ZipEntry zipEntry = zis.getNextEntry();
	        
	        while (zipEntry != null) {
	            File newFile = newFile(outputFolder, zipEntry);
	            System.out.println(newFile);
	            FileOutputStream fos = new FileOutputStream(newFile);
	            int len;
	            while ((len = zis.read(buffer)) > 0) {
	                fos.write(buffer, 0, len);
	            }
	            fos.close();
	            zipEntry = zis.getNextEntry();
	        }
	        zis.closeEntry();
	
				zis.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());
        
        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();
        
        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }
        
        return destFile;
    }
    */
	
	private static void copyRemoteToLocal(Session session, String from, String to, String fileName) throws JSchException, IOException {
        from = from + File.separator + fileName;
        String prefix = null;

        if (new File(to).isDirectory()) {
            prefix = to + File.separator;
        }

        // exec 'scp -f rfile' remotely
        String command = "scp -f " + from;
        Channel channel = session.openChannel("exec");
        ((ChannelExec) channel).setCommand(command);

        // get I/O streams for remote scp
        OutputStream out = channel.getOutputStream();
        InputStream in = channel.getInputStream();

        channel.connect();

        byte[] buf = new byte[1024];

        // send '\0'
        buf[0] = 0;
        out.write(buf, 0, 1);
        out.flush();

        while (true) {
            int c = checkAck(in);
            if (c != 'C') {
                break;
            }

            // read '0644 '
            in.read(buf, 0, 5);

            long filesize = 0L;
            while (true) {
                if (in.read(buf, 0, 1) < 0) {
                    // error
                    break;
                }
                if (buf[0] == ' ') break;
                filesize = filesize * 10L + (long) (buf[0] - '0');
            }

            String file = null;
            for (int i = 0; ; i++) {
                in.read(buf, i, 1);
                if (buf[i] == (byte) 0x0a) {
                    file = new String(buf, 0, i);
                    break;
                }
            }

            System.out.println("file-size=" + filesize + ", file=" + file);

            // send '\0'
            buf[0] = 0;
            out.write(buf, 0, 1);
            out.flush();

            // read a content of lfile
            FileOutputStream fos = new FileOutputStream(prefix == null ? to : prefix + file);
            int foo;
            while (true) {
                if (buf.length < filesize) foo = buf.length;
                else foo = (int) filesize;
                foo = in.read(buf, 0, foo);
                if (foo < 0) {
                    // error
                    break;
                }
                fos.write(buf, 0, foo);
                filesize -= foo;
                if (filesize == 0L) break;
            }

            if (checkAck(in) != 0) {
                System.exit(0);
            }

            // send '\0'
            buf[0] = 0;
            out.write(buf, 0, 1);
            out.flush();

            try {
                if (fos != null) fos.close();
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        channel.disconnect();
        //session.disconnect();
    }
	
	public static int checkAck(InputStream in) throws IOException {
        int b = in.read();
        // b may be 0 for success,
        //          1 for error,
        //          2 for fatal error,
        //         -1
        if (b == 0) return b;
        if (b == -1) return b;

        if (b == 1 || b == 2) {
            StringBuffer sb = new StringBuffer();
            int c;
            do {
                c = in.read();
                sb.append((char) c);
            }
            while (c != '\n');
            if (b == 1) { // error
                System.out.print(sb.toString());
            }
            if (b == 2) { // fatal error
                System.out.print(sb.toString());
            }
        }
        return b;
    }
}
