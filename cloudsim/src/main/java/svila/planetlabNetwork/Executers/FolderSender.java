package svila.planetlabNetwork.Executers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import svila.planetlabNetwork.StaticResources;

public class FolderSender {
	public static void main(String[] args) {
		StaticResources.init();

		String experimentName = "testingBFDWastageSendTest";

		String remoteOutputPath = "$HOME/cloud/networkExperiments/output/" + experimentName + ".zip";
		
		Path localOutputPath = Paths.get(StaticResources.baseFolder,
				"output",
				experimentName);
		compress(localOutputPath);
		Path compressedPath = Paths.get(StaticResources.baseFolder,
				"output",
				experimentName + ".zip");
		execute(compressedPath, remoteOutputPath);
		removeCompressedFile(compressedPath);
	}
	
	public static void removeCompressedFile(Path path) {
		try {
			Files.deleteIfExists(path);
			System.out.println("Removed compressed file: " + path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// https://www.baeldung.com/java-compress-and-uncompress
	public static void compress(Path path) {
		try {
			FileOutputStream fos = new FileOutputStream(path.toString()+".zip");
			ZipOutputStream zipOut = new ZipOutputStream(fos);
			File fileToZip = path.toFile();
			zipFile(fileToZip, fileToZip.getName(), zipOut);
			zipOut.close();
	        fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private static void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
        if (fileToZip.isHidden()) {
            return;
        }
        if (fileToZip.isDirectory()) {
            if (fileName.endsWith("/")) {
                zipOut.putNextEntry(new ZipEntry(fileName));
                zipOut.closeEntry();
            } else {
                zipOut.putNextEntry(new ZipEntry(fileName + "/"));
                zipOut.closeEntry();
            }
            File[] children = fileToZip.listFiles();
            for (File childFile : children) {
                zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
            }
            return;
        }
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zipOut.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        fis.close();
    }
	
	public static void execute(Path localOutputPath, String remoteOutputPath) {
		/*
		 * String remoteOutputPath = "$HOME/cloud/networkExperiments/output/" + experimentName;
		
		Path localOutputPath = Paths.get(StaticResources.baseFolder,
				"output",
				experimentName);
		*/
		System.out.println("Copying folder from local: " + localOutputPath);
		System.out.println("To copy to the remote folder: " + remoteOutputPath);

		String remoteOutputParent = Paths.get(remoteOutputPath).getParent().toString().replace('\\', '/');
		
		SSHConnector sshc = new SSHConnector();
		try {
			sshc.connect("svila", "maracasdoctorat", "maracas.udl.net", 22);
			System.out.println("Trying to copy the next files:");
			
			sshc.executeCommand("mkfile -p" + remoteOutputPath);
			
			System.out.println("File: " + localOutputPath);
			String localFilename = localOutputPath.toFile().getName();
			copyLocalToRemote(sshc.session, localOutputPath.getParent().toString().replace('\\', '/'), remoteOutputPath, localFilename);
			sshc.executeCommand("unzip -o " + remoteOutputPath + " -d " + remoteOutputParent);
			System.out.println("Unzipped remote file");
			sshc.executeCommand("rm " + remoteOutputPath);
			System.out.println("Removed remote file: " + remoteOutputPath);
			sshc.disconnect();
			System.out.println("Session disconnected. All files transfered.");
		} catch (IllegalAccessException | JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private static void copyLocalToRemote(Session session, String from, String to, String fileName) throws JSchException, IOException {
        boolean ptimestamp = true;
        from = from + "/" + fileName;
        System.out.println(from);

        // exec 'scp -t rfile' remotely
        String command = "scp " + (ptimestamp ? "-p" : "") + " -t " + to;
        Channel channel = session.openChannel("exec");
        ((ChannelExec) channel).setCommand(command);

        // get I/O streams for remote scp
        OutputStream out = channel.getOutputStream();
        InputStream in = channel.getInputStream();

        channel.connect();

        if (checkAck(in) != 0) {
            System.exit(0);
        }

        File _lfile = new File(from);

        if (ptimestamp) {
            command = "T" + (_lfile.lastModified() / 1000) + " 0";
            // The access time should be sent here,
            // but it is not accessible with JavaAPI ;-<
            command += (" " + (_lfile.lastModified() / 1000) + " 0\n");
            out.write(command.getBytes());
            out.flush();
            if (checkAck(in) != 0) {
                System.exit(0);
            }
        }

        // send "C0644 filesize filename", where filename should not include '/'
        long filesize = _lfile.length();
        command = "C0644 " + filesize + " ";
        if (from.lastIndexOf('/') > 0) {
            command += from.substring(from.lastIndexOf('/') + 1);
        } else {
            command += from;
        }

        command += "\n";
        out.write(command.getBytes());
        out.flush();

        if (checkAck(in) != 0) {
            System.exit(0);
        }

        // send a content of lfile
        FileInputStream fis = new FileInputStream(from);
        byte[] buf = new byte[1024];
        while (true) {
            int len = fis.read(buf, 0, buf.length);
            if (len <= 0) break;
            out.write(buf, 0, len); //out.flush();
        }

        // send '\0'
        buf[0] = 0;
        out.write(buf, 0, 1);
        out.flush();

        if (checkAck(in) != 0) {
            System.exit(0);
        }
        out.close();

        try {
            if (fis != null) fis.close();
        } catch (Exception ex) {
            System.out.println(ex);
        }

        channel.disconnect();
        //session.disconnect();
    }
	
	public static int checkAck(InputStream in) throws IOException {
        int b = in.read();
        // b may be 0 for success,
        //          1 for error,
        //          2 for fatal error,
        //         -1
        if (b == 0) return b;
        if (b == -1) return b;

        if (b == 1 || b == 2) {
            StringBuffer sb = new StringBuffer();
            int c;
            do {
                c = in.read();
                sb.append((char) c);
            }
            while (c != '\n');
            if (b == 1) { // error
                System.out.print(sb.toString());
            }
            if (b == 2) { // fatal error
                System.out.print(sb.toString());
            }
        }
        return b;
    }
}
