package svila.planetlabNetwork.Executers;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.jcraft.jsch.JSchException;

import svila.planetlabNetwork.StaticResources;

public class QsubExecuter {
	public static void main(String[] args) {
	
		String testbedName = "minPRTesting";
		
		execute(testbedName);
	}
	
	public static void execute(String testbedName) {
		StaticResources.init();
		QsubGenerator.execute(testbedName);
		
		sendGeneratedExperiments(testbedName);
		sendQsubFiles(testbedName);
		executeQsubExecuter(testbedName);
	}
	
	public static void sendGeneratedExperiments(String testbedName) {
		String remoteGeneratedExperimentsTestbedFolder = "/home/svila/cloud/networkExperiments/generatedExperiments/"+
							testbedName + ".zip";

		Path localGeneratedExperimentsTestbedFolder = Paths.get(StaticResources.baseFolder,
				StaticResources.generatedExperimentsFolderName,
				testbedName);
		Path compressedPath = Paths.get(StaticResources.baseFolder,
				StaticResources.generatedExperimentsFolderName,
				testbedName + ".zip");
		
		FolderSender.compress(localGeneratedExperimentsTestbedFolder);
		FolderSender.execute(compressedPath, remoteGeneratedExperimentsTestbedFolder);
		FolderSender.removeCompressedFile(compressedPath);
	}
	
	public static void sendQsubFiles(String testbedName) {
		String remoteQsubFolder = "/home/svila/cloud/networkExperiments/qsubScripts/" + testbedName + ".zip";
		Path localQsubFolder = Paths.get(StaticResources.baseFolder,
				StaticResources.qsubFolderName,
				testbedName);
		Path compressedPath = Paths.get(StaticResources.baseFolder,
				StaticResources.qsubFolderName,
				testbedName + ".zip");
		
		FolderSender.compress(localQsubFolder);
		FolderSender.execute(compressedPath, remoteQsubFolder);
		FolderSender.removeCompressedFile(compressedPath);
	}
	
	public static void executeQsubExecuter(String testbedName) {

		String qsubExecuterFile = "/home/svila/cloud/networkExperiments/qsubScripts/" + testbedName + "/" + testbedName + "_qsub_executer" + ".sh";
		SSHConnector sshc = new SSHConnector();
		try {
			sshc.connect(SSHConnector.USER, SSHConnector.PASSWD, "maracas.udl.net", 22);
			System.out.println("Queuing qsub files");
			sshc.executeCommand("chmod +x " + qsubExecuterFile);
			System.out.println(sshc.executeCommand(qsubExecuterFile));
			sshc.disconnect();
		} catch (IllegalAccessException | JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
