package svila.planetlabNetwork.Executers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

import org.cloudbus.cloudsim.VmSchedulerTimeShared;
import org.json.JSONException;
import org.json.JSONObject;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.GlobalRunner;
import svila.planetlabNetwork.StaticResources;
import svila.planetlabNetwork.results.StatusFile;
import svila.python.PythonBridge;

public class ConsoleExecuter {
	public static void main(String[] args) {	
		if(args[0].equals("help")) {
			showHelp();
		}
		
		String mode = args[0];
		String path = args[1];
		
		StaticResources.init();		
		PythonBridge.init();
		
		switch(mode) {
		case "fullpath":
			externalStandaloneExperiment(path);
			break;
		case "folder":
			folderExperiment(args);
			break;
		case "experiment":
			StaticResources.loadExperiment(path);
			break;
		case "traffic":
			generateTraffic(path);
			break;
		case "topology":
			generateTopology(path);
			break;
		case "testbed":
			generateTestbed(path);
			break;
		case "qsub":
			generateQsub(path);
			break;
		case "gen-pool":
			ExperimentPoolGenerator.generatePool(args);
			break;
		case "check-pool":
			ExperimentPoolCompletionChecker.check(path);
			break;
		case "exec-pool":
			ExperimentPoolExecuter.execute(args);
			break;
		default:
			showHelp();
			break;
		}
	}
	
	public static void generateQsub(String path) {
		QsubGenerator.execute(path);
	}
	
	public static void generateTestbed(String path) {
		StaticResources.init();

		Path inputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.testbedFolderName,
				path + ".json");
		Path outputFolderPath = Paths.get(StaticResources.baseFolder,
				StaticResources.generatedExperimentsFolderName,
				path);
		
		StaticResources.purgeDirectory(outputFolderPath.toFile());

		System.out.println("Generating experiments from file " + inputPath);
		System.out.println("Storing experiments in folder " + outputFolderPath);

		JSONObject[] results = ExperimentConfigurationGenerator.getJSONCombinations(inputPath);
		ExperimentConfigurationGenerator.writeJSONFiles(results, outputFolderPath);
	}
	
	public static void generateTraffic(String path) {
		Path trafficPath = Paths.get(StaticResources.baseFolder,
									StaticResources.trafficGeneratorConfigurationsFolderName,
									path+".json");
		File file = new File(trafficPath.toString());
		try {
			JSONObject trafficJson = new JSONObject(new String(Files.readAllBytes(file.toPath()),"UTF-8"));
			TrafficGenerator.executeFromJSONObject(trafficJson);
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void generateTopology(String path) {
		Path generateTopologyPath = Paths.get(StaticResources.baseFolder,
				StaticResources.topologiesConfigurationsFolderName,
				path+".json");
		File file = new File(generateTopologyPath.toString());
		try {
		JSONObject topologyJson = new JSONObject(new String(Files.readAllBytes(file.toPath()),"UTF-8"));
		TopologyExporter.exportFromJSONObject(topologyJson);
		} catch (JSONException | IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
	}
	
	public static void externalStandaloneExperiment(String absolutePath) {
		StaticResources.loadFullFolderExperiment(absolutePath);
		ExperimentConfiguration ec = StaticResources.getCE();		
		runExperiment(ec);
	}
	
	public static void standaloneExperiment(String experimentName) {
		StaticResources.loadExperiment(experimentName);
		ExperimentConfiguration ec = StaticResources.getCE();		
		runExperiment(ec);
	}
	
	/*public static void folderExperiment(String folderName) {
		Path folderPath = Paths.get(StaticResources.baseFolder,
				StaticResources.generatedExperimentsFolderName,
				folderName);
		for(File file : folderPath.toFile().listFiles()) {
			StaticResources.loadExperiment(Paths.get(StaticResources.generatedExperimentsFolderName, folderName).toString(),
					file.getName().replaceFirst("[.][^.]+$", ""));
			ExperimentConfiguration ec = StaticResources.getCE();
			runExperiment(ec);
		}
	}*/
	
	public static void folderExperiment(String[] args) {
		
		String[] folders = Arrays.copyOfRange(args, 1, args.length);

		boolean generateExperiments = true;
		int numFiles = getNumFiles(folders, generateExperiments);
		long seconds = 0;
		int numExperimentsCompleted = 0;
		StatusFile statusFile = new StatusFile();
		//StaticResources.pressAnyKeyToContinue();
		
		for(String folder : folders) {
			if(generateExperiments) {
				ExperimentConfigurationGenerator.execute(folder);
			}
			
			Path folderPath = Paths.get(StaticResources.baseFolder,
										StaticResources.generatedExperimentsFolderName,
										folder);
			statusFile.generate(folder);
			for(File file : folderPath.toFile().listFiles()) {
				if(file.getName().endsWith(".json")) {
					VmSchedulerTimeShared.clearMips();
					StaticResources.loadExperiment(Paths.get(StaticResources.generatedExperimentsFolderName, folder).toString(),
							file.getName().replaceFirst("[.][^.]+$", ""));
					ExperimentConfiguration ec = StaticResources.getCE();
					System.out.println(ec);
					statusFile.setCurrentExperiment(ec);
					Instant startTime = Instant.now();
					try {
						runExperiment(ec);
					} catch (Exception e) {
						statusFile.sendError(e);
					} finally {
						if(!statusFile.isError()) {
							statusFile.sendOk();
						}
					}
					System.gc();
					numExperimentsCompleted++;
					Duration experimentTime = Duration.between(startTime, Instant.now());
					String.format("%02d:%02d", experimentTime.toMinutes(), experimentTime.getSeconds()%60);
					seconds += experimentTime.getSeconds();
					long expectedTime = seconds*numFiles/numExperimentsCompleted;
					long remainingTime = expectedTime - seconds;
	
					System.out.println(String.format("%02d:%02d:%02d/%02d:%02d:%02d (remaining: %02d:%02d:%02d)", seconds/3600, seconds/60%60, seconds%60,
										expectedTime/3600, expectedTime/60%60, expectedTime%60,
										remainingTime/3600, remainingTime/60%60, remainingTime%60
										));
					System.out.println(numExperimentsCompleted + "/" + numFiles);
					System.out.println("mipsNotAllocated: " + VmSchedulerTimeShared.mipsOver);
					System.out.printf("mipsSum: %f\n", VmSchedulerTimeShared.mipsSum);
					System.out.println("mipsOver: " + VmSchedulerTimeShared.mipsOver);
					System.out.printf("Ratio: %f%%\n", VmSchedulerTimeShared.mipsOver/VmSchedulerTimeShared.mipsSum*100);
				}
			}
			
			statusFile.close();
		}
	}

	private static int getNumFiles(String[] folders, boolean generateExperiments) {
		int numExperiments = 0;
		for(String folder : folders) {
			if(generateExperiments) {
				ExperimentConfigurationGenerator.execute(folder);
			}
			
			Path folderPath = Paths.get(StaticResources.baseFolder,
										StaticResources.generatedExperimentsFolderName,
										folder);
			for(File file : folderPath.toFile().listFiles()) {
				if(file.getName().endsWith(".json")) {
					numExperiments++;
				}
			}
		}
		return numExperiments;
	}
	
	public static void runExperiment(ExperimentConfiguration ec) {
		System.out.println(ec);
		new GlobalRunner(
	            ec
	    );
	}
	public static void showHelp() {
		System.out.println("Configure launcher.json with the desired working directory baseFolder");
		System.out.println("Options:");
		System.out.println("\tfullpath <absolute path>");
		System.out.println("\tfolder <folder name in generatedExperiments folder>");
		System.out.println("\texperiment <file name in experiments folder>");
		System.out.println("\ttopology <file name in topologiesConfigurations folder>");
		System.out.println("\ttraffic <file name in trafficGeneratorConfigurations folder>");
		System.out.println("\tqsub <file name in testbed folder>");
		System.out.println("\tgen-pool <pool name> <list of testbeds>");
		System.out.println("\tcheck-pool <pool name>");
		System.out.println("\texec-pool <pool name> <list of type folders>");
		System.exit(0);
	}

}
