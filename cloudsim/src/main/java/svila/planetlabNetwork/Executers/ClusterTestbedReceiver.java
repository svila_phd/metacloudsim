package svila.planetlabNetwork.Executers;

import java.nio.file.Path;
import java.nio.file.Paths;

import svila.planetlabNetwork.StaticResources;

public class ClusterTestbedReceiver {
	public static void main(String[] args) {
		
		String testbedName = "final";
		
		execute(testbedName);
	}
	
	public static void execute(String testbedName) {
		StaticResources.init();
		String remoteOutputPath = "/home/svila/cloud/networkExperiments/output/" + testbedName;
		
		Path localOutputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				testbedName);
		
		FolderReceiver.execute(localOutputPath, remoteOutputPath);
	}
}
