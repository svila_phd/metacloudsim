package svila.planetlabNetwork.Executers;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONException;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Predicate;

import static com.jayway.jsonpath.Criteria.where;
import static com.jayway.jsonpath.Filter.filter;

import svila.planetlabNetwork.StaticResources;

public class ExperimentFilter {
	public static void main(String[] args) {
		
		String inputFilterFolder = StaticResources.generatedExperimentsFolderName;

		String[] testbeds = {"europar_tune_v4_fnss"};
		String outputTestbed = "europar_wpsp_mdgiqr_0.65_(from_europar_tune_v4_fnss)";
		//Predicate filter = filter(where("selectionPolicy").is("pearson"));
		//Predicate filter = filter(where("migrationInterval").is("12").and("simulationTimeLimit").is(86400.0));
		//Predicate filter = filter(where("selectionPolicy").is("psp2"));
		//Predicate filter = filter(where("selectionPolicy").in(Arrays.asList("psp2")).and("allocationPolicy").in(Arrays.asList("mdgiqr")).and("tunningValue").is("0.7"));
		//Predicate filter = filter(where("allocationPolicy").is("iqr").and("selectionPolicy").in("mc", "mmt", "mu", "rs", "nm"));
		//Predicate filter = filter(where("migrationInterval").is("5"));
		Predicate filter = filter(where("tunningValue").is("0.65"));

		execute(testbeds, inputFilterFolder, outputTestbed, filter);
	}
	
	public static void showOptions(String[] testbeds) {
		
	}
	
	public static void execute(String[] testbeds, String inputFilterFolder, String outputTestbed, Predicate filter) {
		StaticResources.init();
		
		List<Path> inputPaths = new ArrayList<>();
		int experimentsCopied =0, filesCopied = 0;
		
		for(String testbed : testbeds) {
			inputPaths.add(Paths.get(StaticResources.baseFolder,
					inputFilterFolder,
					testbed));
		}
		
		Path newTestbedPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				outputTestbed);
		newTestbedPath.toFile().mkdirs();
		
		for(Path inputPath : inputPaths) {
			System.out.println("Checking folder: " + inputPath);
			Path outputTestbedPath = Paths.get(StaticResources.baseFolder,
					StaticResources.rootOutputFolderName,
					inputPath.toFile().getName());
			for(File file : inputPath.toFile().listFiles()) {
				System.out.println("Filtering file: " + file);
				try {
					List<Object> o = JsonPath.parse(file).read("$[?]", filter);
					if(o.size() == 1) {
						experimentsCopied++;
						String filenameCode = FilenameUtils.removeExtension(file.getName().toString());
						filenameCode.substring(filenameCode.length() - 5);
						System.out.println("Copying files with code: " + filenameCode);
						int currentFilesCopied = copyDataToOutput(filenameCode,
								outputTestbedPath,
								newTestbedPath);
						filesCopied += currentFilesCopied;
					}
				} catch (JSONException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		System.out.println(experimentsCopied + " experiments copied. " + filesCopied + " files copied.");
	}
	
	private static int copyDataToOutput(String filenameCode, Path sourceFolder, Path outputPath) {
		FilenameFilter fileFilter = new FileDataFilter(filenameCode);
		System.out.println("Filtering from: " + sourceFolder);
		File[] filteredFiles = sourceFolder.toFile().listFiles(fileFilter);
		
		for(File filteredFile : filteredFiles) {
			try {
				System.out.println("Copying file: " + filteredFile);
				System.out.println("To: " + Paths.get(outputPath.toString(), filteredFile.getName()).toFile());
				FileUtils.copyFile(filteredFile, Paths.get(outputPath.toString(), filteredFile.getName()).toFile());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return filteredFiles.length;
	}
	
	private static class FileDataFilter implements FilenameFilter {
		private String pattern;
		
		public FileDataFilter(String pattern) {
			this.pattern = pattern;
		}

		@Override
		public boolean accept(File dir, String name) {
			return name.contains(pattern);
		}
	}
}
