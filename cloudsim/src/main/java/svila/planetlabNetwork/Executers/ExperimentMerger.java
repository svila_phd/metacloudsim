package svila.planetlabNetwork.Executers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import svila.planetlabNetwork.StaticResources;

public class ExperimentMerger {
	public static void main(String[] args) {
		StaticResources.init();
		FilenameFilter filenameFilter = new FilenameFilter() { // all, json
		    @Override
		    public boolean accept(File dir, String name) {
		        return name.toLowerCase().endsWith(".json");
		    }
		};
		String outputExperiment = "Future_pl";
		String[] experiments = {"Future_pl_03_reference",
								"Future_pl_06_reference",
								"Future_pl_09_reference",
								"Future_pl_22_reference",
								"Future_pl_25_reference",
								"Future_pl_03_improvements",
								"Future_pl_06_improvements",
								"Future_pl_09_improvements",
								"Future_pl_22_improvements",
								"Future_pl_25_improvements",
								};
		List<Path> inputPaths = new ArrayList<>();
		for(String experiment : experiments) {
			inputPaths.add(Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				experiment));
		}
		
		Path outputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				outputExperiment);
		outputPath.toFile().mkdirs();
		copyFiles(inputPaths, outputPath, filenameFilter);
		
		System.out.println("Merge completed");
	}
	
	private static void copyFiles(List<Path> inputPaths, Path outputPath, FilenameFilter filenameFilter) {
		for(Path path : inputPaths) {
			for(File currentInputFile : path.toFile().listFiles(filenameFilter)) {
				if(!currentInputFile.isFile()) {
					continue;
				}
				File targetFile = new File(outputPath.toFile(), currentInputFile.getName());
				copyFileUsingStream(currentInputFile, targetFile);
			}
		}
	}

    private static void deleteFiles(File fSource) {
        if(fSource.exists()) {
            try {
                FileUtils.forceDelete(fSource);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void copyFileUsingStream(File source, File dest) {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } catch (Exception ex) {
            System.out.println("Unable to copy file:" + ex.getMessage());
        } finally {
            try {
                is.close();
                os.close();
            } catch (Exception ex) {
            }
        }
    }
}
