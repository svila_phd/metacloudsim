package svila.planetlabNetwork.Executers;

import java.util.ArrayList;
import java.util.List;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.GlobalRunner;
import svila.planetlabNetwork.Snapshot;

public class SlicedSimulation {
	double startTime;
	double endTime;
	List<Snapshot> snapshots;
	List<Snapshot> currentSnapshots;
	ExperimentConfiguration ec;
	
	public SlicedSimulation(double startTime, double endTime, List<Snapshot> snapshots, ExperimentConfiguration ec) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.snapshots = new ArrayList<>(snapshots);
		this.currentSnapshots = new ArrayList<>();
		this.ec = ec;
	}
	
	public void runSimulation() {
		new GlobalRunner(
                ec
        );
	}
	
	
}
