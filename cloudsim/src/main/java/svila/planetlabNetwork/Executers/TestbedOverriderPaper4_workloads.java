package svila.planetlabNetwork.Executers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import svila.planetlabNetwork.StaticResources;

public class TestbedOverriderPaper4_workloads {
	public static void main(String[] args) {
		StaticResources.init();

		String testbed = "paper4_test3_alibaba_30_wbf";

		Path inputPath = Paths.get(StaticResources.baseFolder, StaticResources.testbedFolderName, testbed + ".json");

		Path outputPath = Paths.get(StaticResources.baseFolder, StaticResources.testbedFolderName,
				testbed + "_o" + ".json");

		JSONObject testbedConfiguration = null;
		try {
			testbedConfiguration = new JSONObject(new String(Files.readAllBytes(inputPath), "UTF-8"));
			testbedConfiguration.put("name",
					new JSONArray().put(testbedConfiguration.getJSONArray("name").get(0) + "_o"));
			testbedConfiguration.put("experimentOutputFolder",
					new JSONArray().put(testbedConfiguration.getJSONArray("experimentOutputFolder").get(0) + "_o"));
		} catch (JSONException | IOException e) {
			e.printStackTrace();
		}

		JSONArray overrideArray = TestbedOverriderPaper4_workloads.getOverride_paper4_test3_alibaba_30_wbf();

		testbedConfiguration.put("override", overrideArray);

		try {
			File currentOutputFile = new File(outputPath.toString());
			BufferedWriter writer = new BufferedWriter(new FileWriter(currentOutputFile));
			testbedConfiguration.write(writer, 4, 0);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String content;
		try {
			content = new String(Files.readAllBytes(outputPath));
			content = content.replaceAll("\"-_-", "");
			content = content.replaceAll("-_-\"", "");
			Files.write(outputPath, content.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public static JSONArray getOverride_workloads() {
		JSONArray overrideArray = new JSONArray();
		
		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);
		
		
		for (int window = 6; window <= 13; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.7); alpha
					.compareTo(BigDecimal.valueOf(0.5)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			
			overrideArray.put(jsonObject);
		}
		}

		
		
		JSONArray facebookProphetConf = new JSONArray();
		facebookProphetConf.put("fbProphet");
		
		// Only facebookProphet
		JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
		jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
		jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
		overrideArray.put(jsonObjectOnlyFPVmFore);

		overrideArray.put(jsonObjectOnlyFPVmFore);
		
		for (int window = 6; window <= 13; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.7); alpha
					.compareTo(BigDecimal.valueOf(0.5)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", facebookProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			
			overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}
	
	public static JSONArray getOverride_workloads_general() {
		JSONArray overrideArray = new JSONArray();
		
		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);
		
		
		for (int window = 12; window <= 12; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha
					.compareTo(BigDecimal.valueOf(0.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			
			overrideArray.put(jsonObject);
		}
		}

		
		
		JSONArray facebookProphetConf = new JSONArray();
		facebookProphetConf.put("fbProphet");
		
		// Only facebookProphet
		JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
		jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
		jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
		overrideArray.put(jsonObjectOnlyFPVmFore);

		overrideArray.put(jsonObjectOnlyFPVmFore);
		
		for (int window = 15; window <= 15; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.2); alpha
					.compareTo(BigDecimal.valueOf(-0.2)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", facebookProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			
			overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}
	
	public static JSONArray getOverride_test_allw_best_values() {
		JSONArray overrideArray = new JSONArray();
		
		JSONObject jsonObjectNoMigrations = new JSONObject();
		JSONArray noMigrationsConf = new JSONArray();
		jsonObjectNoMigrations.put("selectionPolicy", "nm");
		overrideArray.put(jsonObjectNoMigrations);

		
		
		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);
		
		
		for (int window = 12; window <= 12; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();
	
				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}
		
		for (int window = 13; window <= 13; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksbollingerConf = new JSONArray();
				currentInterpeaksbollingerConf.put("interpeaksbollinger");
				currentInterpeaksbollingerConf.put(window);
				currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
				currentInterpeaksbollingerConf.put(4);
				currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		
		
		JSONArray facebookProphetConf = new JSONArray();
		facebookProphetConf.put("fbProphet");
		
		// Only facebookProphet
		JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
		jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
		jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
		overrideArray.put(jsonObjectOnlyFPVmFore);

		overrideArray.put(jsonObjectOnlyFPVmFore);
		
		for (int window = 15; window <= 15; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", facebookProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			
			overrideArray.put(jsonObject);
			}
		}
		
		for (int window = 9; window <= 9; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksbollingerConf = new JSONArray();
				currentInterpeaksbollingerConf.put("interpeaksbollinger");
				currentInterpeaksbollingerConf.put(window);
				currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
				currentInterpeaksbollingerConf.put(4);
				currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}
	
	public static JSONArray getOverride_test_allw_pre() {
		JSONArray overrideArray = new JSONArray();
		
		JSONObject jsonObjectNoMigrations = new JSONObject();
		JSONArray noMigrationsConf = new JSONArray();
		jsonObjectNoMigrations.put("selectionPolicy", "nm");
		overrideArray.put(jsonObjectNoMigrations);

		
		
		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);
		
		
		for (int window = 12; window <= 12; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha
					.compareTo(BigDecimal.valueOf(0.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();
	
				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}
		
		for (int window = 13; window <= 13; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.2); alpha
					.compareTo(BigDecimal.valueOf(0.2)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksbollingerConf = new JSONArray();
				currentInterpeaksbollingerConf.put("interpeaksbollinger");
				currentInterpeaksbollingerConf.put(window);
				currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
				currentInterpeaksbollingerConf.put(4);
				currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		
		
		JSONArray facebookProphetConf = new JSONArray();
		facebookProphetConf.put("fbProphet");
		
		// Only facebookProphet
		JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
		jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
		jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
		overrideArray.put(jsonObjectOnlyFPVmFore);

		overrideArray.put(jsonObjectOnlyFPVmFore);
		
		for (int window = 15; window <= 15; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.2); alpha
					.compareTo(BigDecimal.valueOf(-0.2)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", facebookProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			
			overrideArray.put(jsonObject);
			}
		}
		
		for (int window = 9; window <= 9; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.2); alpha
					.compareTo(BigDecimal.valueOf(0.2)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksbollingerConf = new JSONArray();
				currentInterpeaksbollingerConf.put("interpeaksbollinger");
				currentInterpeaksbollingerConf.put(window);
				currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
				currentInterpeaksbollingerConf.put(4);
				currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}
	
	public static JSONArray getOverride_test_bitbrains_9_tuning() {
		JSONArray overrideArray = new JSONArray();
		
		JSONObject jsonObjectNoMigrations = new JSONObject();
		JSONArray noMigrationsConf = new JSONArray();
		jsonObjectNoMigrations.put("selectionPolicy", "nm");
		overrideArray.put(jsonObjectNoMigrations);

		
		
		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);
		
		
		for (int window = 6; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();
	
				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}
		
		for (int window = 6; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksbollingerConf = new JSONArray();
				currentInterpeaksbollingerConf.put("interpeaksbollinger");
				currentInterpeaksbollingerConf.put(window);
				currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
				currentInterpeaksbollingerConf.put(4);
				currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		
		
		JSONArray facebookProphetConf = new JSONArray();
		facebookProphetConf.put("fbProphet");
		
		// Only facebookProphet
		JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
		jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
		jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
		overrideArray.put(jsonObjectOnlyFPVmFore);

		overrideArray.put(jsonObjectOnlyFPVmFore);
		
		for (int window = 6; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", facebookProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			
			overrideArray.put(jsonObject);
			}
		}
		
		for (int window = 6; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksbollingerConf = new JSONArray();
				currentInterpeaksbollingerConf.put("interpeaksbollinger");
				currentInterpeaksbollingerConf.put(window);
				currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
				currentInterpeaksbollingerConf.put(4);
				currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}
	
	public static JSONArray getOverride_test_google_tuning() {
		JSONArray overrideArray = new JSONArray();
		
		JSONObject jsonObjectNoMigrations = new JSONObject();
		JSONArray noMigrationsConf = new JSONArray();
		jsonObjectNoMigrations.put("selectionPolicy", "nm");
		overrideArray.put(jsonObjectNoMigrations);

		
		
		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);
		
		
		for (int window = 6; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();
	
				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}
		
		for (int window = 6; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksbollingerConf = new JSONArray();
				currentInterpeaksbollingerConf.put("interpeaksbollinger");
				currentInterpeaksbollingerConf.put(window);
				currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
				currentInterpeaksbollingerConf.put(4);
				currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		
		
		JSONArray facebookProphetConf = new JSONArray();
		facebookProphetConf.put("fbProphet");
		
		// Only facebookProphet
		JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
		jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
		jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
		overrideArray.put(jsonObjectOnlyFPVmFore);

		overrideArray.put(jsonObjectOnlyFPVmFore);
		
		for (int window = 6; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", facebookProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			
			overrideArray.put(jsonObject);
			}
		}
		
		for (int window = 6; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksbollingerConf = new JSONArray();
				currentInterpeaksbollingerConf.put("interpeaksbollinger");
				currentInterpeaksbollingerConf.put(window);
				currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
				currentInterpeaksbollingerConf.put(4);
				currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}
	
	public static JSONArray getOverride_test_materna_tuning() {
		JSONArray overrideArray = new JSONArray();
		
		JSONObject jsonObjectNoMigrations = new JSONObject();
		JSONArray noMigrationsConf = new JSONArray();
		jsonObjectNoMigrations.put("selectionPolicy", "nm");
		overrideArray.put(jsonObjectNoMigrations);

		
		
		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);
		
		
		for (int window = 6; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();
	
				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}
		
		for (int window = 6; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksbollingerConf = new JSONArray();
				currentInterpeaksbollingerConf.put("interpeaksbollinger");
				currentInterpeaksbollingerConf.put(window);
				currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
				currentInterpeaksbollingerConf.put(4);
				currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		
		
		JSONArray facebookProphetConf = new JSONArray();
		facebookProphetConf.put("fbProphet");
		
		// Only facebookProphet
		JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
		jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
		jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
		overrideArray.put(jsonObjectOnlyFPVmFore);

		overrideArray.put(jsonObjectOnlyFPVmFore);
		
		for (int window = 6; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", facebookProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			
			overrideArray.put(jsonObject);
			}
		}
		
		for (int window = 6; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksbollingerConf = new JSONArray();
				currentInterpeaksbollingerConf.put("interpeaksbollinger");
				currentInterpeaksbollingerConf.put(window);
				currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
				currentInterpeaksbollingerConf.put(4);
				currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test_planetlab_tuning_autoarima() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("autoArima");
		
		// Bollinger + neuralProphet
		for (int window = 4; window <= 15; window = window + 3) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				
				
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				overrideArray.put(jsonObject);
			}
		}
				
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);

		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test_new_workload_general() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				"planetlab/planetlab_20110303_100_mostDiff",
				"materna/materna_trace1_valid_0_288_100_mostDiff",
				"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				"bitbrains/fastStorage_2013-8_100_vms_short",
				"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				"azure/azure_azure2019_traces_100_mostDiff"
				};
		Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		vmDistributions.add(planetlab);
		vmDistributions.add(materna_trace1);
		vmDistributions.add(bitbrains_rnd8);
		vmDistributions.add(bitbrains_rnd9);
		vmDistributions.add(bitbrains_fast);
		vmDistributions.add(alibaba);
		vmDistributions.add(azure);

		String[] vmsType = new String[] {
				"vms4",
				"vms4_materna",
				"vms4",
				"vms4",
				"vms4_materna",
				"vms4",
				"vms4_materna"
		};
		
		
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			overrideArray.put(jsonObjectBase);
			
			JSONArray neuralProphetConf = new JSONArray();
			neuralProphetConf.put("neuralProphet");
			
			// Bollinger + neuralProphet
			for (int window = 3; window <= 18; window = window + 1) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentBollingerConf = new JSONArray();
					currentBollingerConf.put("bollinger");
					currentBollingerConf.put(window);
					currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
					
					
					JSONObject jsonObject = new JSONObject();

					jsonObject.put("hostSignalProcessing", currentBollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

					jsonObject.put("hostForecastingTechnique", neuralProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);

					overrideArray.put(jsonObject);
				}
			}
					
			// Only neuralProphet
			JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
			jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
			jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			jsonObjectOnlyNPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyNPVmFore.put("vms", vmsType[i]);

			overrideArray.put(jsonObjectOnlyNPVmFore);
		}
		


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test_new_workload_general_wpsp() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				"planetlab/planetlab_20110303_100_mostDiff",
				"materna/materna_trace1_valid_0_288_100_mostDiff",
				"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				"bitbrains/fastStorage_2013-8_100_vms_short",
				"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				"azure/azure_azure2019_traces_100_mostDiff"
				};
		Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		vmDistributions.add(planetlab);
		vmDistributions.add(materna_trace1);
		vmDistributions.add(bitbrains_rnd8);
		vmDistributions.add(bitbrains_rnd9);
		vmDistributions.add(bitbrains_fast);
		vmDistributions.add(alibaba);
		vmDistributions.add(azure);

		String[] vmsType = new String[] {
				"vms4",
				"vms4_materna",
				"vms4",
				"vms4",
				"vms4_materna",
				"vms4",
				"vms4_materna"
		};
		
		
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			overrideArray.put(jsonObjectBase);
			
			JSONArray neuralProphetConf = new JSONArray();
			neuralProphetConf.put("neuralProphet");
			
			// Bollinger + neuralProphet
			for (int window = 3; window <= 18; window = window + 1) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentBollingerConf = new JSONArray();
					currentBollingerConf.put("bollinger");
					currentBollingerConf.put(window);
					currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
					
					
					JSONObject jsonObject = new JSONObject();

					jsonObject.put("hostSignalProcessing", currentBollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

					jsonObject.put("hostForecastingTechnique", neuralProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);

					overrideArray.put(jsonObject);
				}
			}
					
			// Only neuralProphet
			JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
			jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
			jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			jsonObjectOnlyNPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyNPVmFore.put("vms", vmsType[i]);

			overrideArray.put(jsonObjectOnlyNPVmFore);
		}
		


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test2_main_workload_mu_wpsp_fix() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				"planetlab/planetlab_20110303_100_mostDiff",
				"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				"azure/azure_azure2019_traces_100_mostDiff"
				};
		Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		vmDistributions.add(planetlab);
		vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		vmDistributions.add(alibaba);
		vmDistributions.add(azure);

		String[] vmsType = new String[] {
				"vms4",
				"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				"vms4",
				"vms4_materna"
		};
		
		
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			overrideArray.put(jsonObjectBase);
			
			JSONArray neuralProphetConf = new JSONArray();
			neuralProphetConf.put("neuralProphet");
			
			// Bollinger + neuralProphet
			for (int window = 12; window <= 12; window = window + 1) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentBollingerConf = new JSONArray();
					currentBollingerConf.put("bollinger");
					currentBollingerConf.put(window);
					currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
					
					
					JSONObject jsonObject = new JSONObject();

					jsonObject.put("hostSignalProcessing", currentBollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

					jsonObject.put("hostForecastingTechnique", neuralProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);

					overrideArray.put(jsonObject);
				}
			}
			
			// interpeaksBollinger + neuralProphet
			for (int window = 13; window <= 13; window++) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", neuralProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					
					overrideArray.put(jsonObject);
				}
			}
					
			// Only neuralProphet
			JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
			jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
			jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			jsonObjectOnlyNPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyNPVmFore.put("vms", vmsType[i]);

			overrideArray.put(jsonObjectOnlyNPVmFore);
			
			
			
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
			jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			overrideArray.put(jsonObjectOnlyFPVmFore);

			overrideArray.put(jsonObjectOnlyFPVmFore);
			
			// Bollinger + facebookProphet
			for (int window = 15; window <= 15; window++) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				jsonObject.put("workloadTrace", workloadTraces[i]);
				jsonObject.put("vms", vmsType[i]);
				
				overrideArray.put(jsonObject);
				}
			}
			
			// interpeaksBollinger + facebookProphet
			for (int window = 9; window <= 9; window++) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					
					overrideArray.put(jsonObject);
				}
			}
		}
		


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test2_main_workload_mu_wpsp_only_autoarima() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				"planetlab/planetlab_20110303_100_mostDiff",
				"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				"azure/azure_azure2019_traces_100_mostDiff"
				};
		Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		vmDistributions.add(planetlab);
		vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		vmDistributions.add(alibaba);
		vmDistributions.add(azure);

		String[] vmsType = new String[] {
				"vms4",
				"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				"vms4",
				"vms4_materna"
		};
		
		
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			overrideArray.put(jsonObjectBase);
			
			JSONArray autoarimaConf = new JSONArray();
			autoarimaConf.put("autoArima");
			

			// Only neuralProphet
			JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
			jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", autoarimaConf);
			jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", autoarimaConf);
			jsonObjectOnlyNPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyNPVmFore.put("vms", vmsType[i]);

			overrideArray.put(jsonObjectOnlyNPVmFore);
		}
		


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				"planetlab/planetlab_20110303_100_mostDiff",
				"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				"azure/azure_azure2019_traces_100_mostDiff"
				};
		Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		vmDistributions.add(planetlab);
		vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		vmDistributions.add(alibaba);
		vmDistributions.add(azure);

		String[] vmsType = new String[] {
				"vms4",
				"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				"vms4",
				"vms4_materna"
		};
		
		
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			overrideArray.put(jsonObjectBase);
			
			JSONArray neuralProphetConf = new JSONArray();
			neuralProphetConf.put("neuralProphet");
			
			// Bollinger + neuralProphet
			for (int window = 12; window <= 12; window = window + 1) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentBollingerConf = new JSONArray();
					currentBollingerConf.put("bollinger");
					currentBollingerConf.put(window);
					currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
					
					
					JSONObject jsonObject = new JSONObject();

					jsonObject.put("hostSignalProcessing", currentBollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

					jsonObject.put("hostForecastingTechnique", neuralProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);

					overrideArray.put(jsonObject);
				}
			}
			
			// interpeaksBollinger + neuralProphet
			for (int window = 13; window <= 13; window++) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", neuralProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					
					overrideArray.put(jsonObject);
				}
			}
					
			// Only neuralProphet
			JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
			jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
			jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			jsonObjectOnlyNPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyNPVmFore.put("vms", vmsType[i]);

			overrideArray.put(jsonObjectOnlyNPVmFore);
			
			
			
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
			jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyFPVmFore.put("vms", vmsType[i]);
			overrideArray.put(jsonObjectOnlyFPVmFore);
			
			// Bollinger + facebookProphet
			for (int window = 15; window <= 15; window++) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				jsonObject.put("workloadTrace", workloadTraces[i]);
				jsonObject.put("vms", vmsType[i]);
				
				overrideArray.put(jsonObject);
				}
			}
			
			// interpeaksBollinger + facebookProphet
			for (int window = 9; window <= 9; window++) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					
					overrideArray.put(jsonObject);
				}
			}
		}
		


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_materna_tuning() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				//"planetlab/planetlab_20110303_100_mostDiff",
				"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				//"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				//"azure/azure_azure2019_traces_100_mostDiff"
				};
		//Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		//Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		//Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		//vmDistributions.add(planetlab);
		vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		//vmDistributions.add(alibaba);
		//vmDistributions.add(azure);

		String[] vmsType = new String[] {
				//"vms4",
				"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				//"vms4",
				//"vms4_materna"
		};
		
		
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			overrideArray.put(jsonObjectBase);
			
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
			jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyFPVmFore.put("vms", vmsType[i]);
			overrideArray.put(jsonObjectOnlyFPVmFore);
			
			// Bollinger + facebookProphet
			for (int window = 6; window <= 25; window=window+2) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				jsonObject.put("workloadTrace", workloadTraces[i]);
				jsonObject.put("vms", vmsType[i]);
				
				overrideArray.put(jsonObject);
				}
			}
			
			// interpeaksBollinger + facebookProphet
			for (int window = 6; window <= 25; window=window+2) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					
					overrideArray.put(jsonObject);
				}
			}
		}
		


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_materna_tuning_hindex() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				//"planetlab/planetlab_20110303_100_mostDiff",
				"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				//"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				//"azure/azure_azure2019_traces_100_mostDiff"
				};
		//Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		//Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		//Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		//vmDistributions.add(planetlab);
		vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		//vmDistributions.add(alibaba);
		//vmDistributions.add(azure);

		String[] vmsType = new String[] {
				//"vms4",
				"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				//"vms4",
				//"vms4_materna"
		};
		
		String[] refVMSelection = new String[] {"mc", "rs", "mmt", "mu"};
		for(int i=0; i<workloadTraces.length; i++) {
			for(int j=0; j<refVMSelection.length; j++) {
				JSONObject jsonObjectBase = new JSONObject();
				jsonObjectBase.put("workloadTrace", workloadTraces[i]);
				jsonObjectBase.put("vms", vmsType[i]);
				jsonObjectBase.put("selectionPolicy", refVMSelection[j]);
				overrideArray.put(jsonObjectBase);
			}
		}
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.5); tunningValue
				.compareTo(BigDecimal.valueOf(0.9)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.1))) {
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			jsonObjectBase.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectBase);
			
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
			jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyFPVmFore.put("vms", vmsType[i]);
			jsonObjectOnlyFPVmFore.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectOnlyFPVmFore);
			
			// Bollinger + facebookProphet
			for (int window = 12; window <= 25; window=window+2) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				jsonObject.put("workloadTrace", workloadTraces[i]);
				jsonObject.put("vms", vmsType[i]);
				jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
				
				overrideArray.put(jsonObject);
				}
			}
			
			// interpeaksBollinger + facebookProphet
			for (int window = 12; window <= 25; window=window+2) {
				for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
		}
		}


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_materna_tuning_hindex2() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				//"planetlab/planetlab_20110303_100_mostDiff",
				"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				//"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				//"azure/azure_azure2019_traces_100_mostDiff"
				};
		//Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		//Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		//Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		//vmDistributions.add(planetlab);
		vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		//vmDistributions.add(alibaba);
		//vmDistributions.add(azure);

		String[] vmsType = new String[] {
				//"vms4",
				"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				//"vms4",
				//"vms4_materna"
		};
		
		String[] refVMSelection = new String[] {"mc", "rs", "mmt", "mu"};
		for(int i=0; i<workloadTraces.length; i++) {
			for(int j=0; j<refVMSelection.length; j++) {
				JSONObject jsonObjectBase = new JSONObject();
				jsonObjectBase.put("workloadTrace", workloadTraces[i]);
				jsonObjectBase.put("vms", vmsType[i]);
				jsonObjectBase.put("selectionPolicy", refVMSelection[j]);
				overrideArray.put(jsonObjectBase);
			}
		}
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.8); tunningValue
				.compareTo(BigDecimal.valueOf(1.2)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.1))) {
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			jsonObjectBase.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectBase);
			
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
			jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyFPVmFore.put("vms", vmsType[i]);
			jsonObjectOnlyFPVmFore.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectOnlyFPVmFore);
		
			
			// interpeaksBollinger + facebookProphet
			for (int window = 3; window <= 14; window=window+1) {
				for (BigDecimal alpha = BigDecimal.valueOf(0.3); alpha
						.compareTo(BigDecimal.valueOf(1.3)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
		}
		}


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_materna_tuning_30() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				//"planetlab/planetlab_20110303_100_mostDiff",
				"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				//"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				//"azure/azure_azure2019_traces_100_mostDiff"
				};
		//Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		//Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		//Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		//vmDistributions.add(planetlab);
		vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		//vmDistributions.add(alibaba);
		//vmDistributions.add(azure);

		String[] vmsType = new String[] {
				//"vms4",
				"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				//"vms4",
				//"vms4_materna"
		};
		
		String[] refVMSelection = new String[] {"mc", "rs", "mmt", "mu"};
		for(int i=0; i<workloadTraces.length; i++) {
			for(int j=0; j<refVMSelection.length; j++) {
				JSONObject jsonObjectBase = new JSONObject();
				jsonObjectBase.put("workloadTrace", workloadTraces[i]);
				jsonObjectBase.put("vms", vmsType[i]);
				jsonObjectBase.put("selectionPolicy", refVMSelection[j]);
				overrideArray.put(jsonObjectBase);
			}
		}
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.9); tunningValue
				.compareTo(BigDecimal.valueOf(0.9)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.1))) {
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			jsonObjectBase.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectBase);
			
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
			jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyFPVmFore.put("vms", vmsType[i]);
			jsonObjectOnlyFPVmFore.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectOnlyFPVmFore);
		
			
			// interpeaksBollinger + facebookProphet
			for (int window = 9; window <= 9; window=window+1) {
				for (BigDecimal alpha = BigDecimal.valueOf(1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
		}
		}


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_alibaba_azure() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				//"planetlab/planetlab_20110303_100_mostDiff",
				//"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				"azure/azure_azure2019_traces_100_mostDiff"
				};
		//Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		//Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		//vmDistributions.add(planetlab);
		//vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		vmDistributions.add(alibaba);
		vmDistributions.add(azure);

		String[] vmsType = new String[] {
				//"vms4",
				//"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				"vms4",
				"vms4_materna"
		};
		
		String[] refVMSelection = new String[] {"mc", "rs", "mmt", "mu"};
		for(int i=0; i<workloadTraces.length; i++) {
			for(int j=0; j<refVMSelection.length; j++) {
				JSONObject jsonObjectBase = new JSONObject();
				jsonObjectBase.put("workloadTrace", workloadTraces[i]);
				jsonObjectBase.put("vms", vmsType[i]);
				jsonObjectBase.put("selectionPolicy", refVMSelection[j]);
				overrideArray.put(jsonObjectBase);
			}
		}
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.7); tunningValue
				.compareTo(BigDecimal.valueOf(1.2)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.1))) {
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			jsonObjectBase.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectBase);
			
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
			jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyFPVmFore.put("vms", vmsType[i]);
			jsonObjectOnlyFPVmFore.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectOnlyFPVmFore);
		
			
			// interpeaksBollinger + facebookProphet
			for (int window = 3; window <= 18; window=window+1) {
				for (BigDecimal alpha = BigDecimal.valueOf(0.5); alpha
						.compareTo(BigDecimal.valueOf(1.3)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
		}
		}


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_alibaba_30() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				//"planetlab/planetlab_20110303_100_mostDiff",
				//"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				//"azure/azure_azure2019_traces_100_mostDiff"
				};
		//Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		//Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		//Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		//vmDistributions.add(planetlab);
		//vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		vmDistributions.add(alibaba);
		//vmDistributions.add(azure);

		String[] vmsType = new String[] {
				//"vms4",
				//"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				"vms4",
				//"vms4_materna"
		};
		
		String[] refVMSelection = new String[] {"mc", "rs", "mmt", "mu"};
		for(int i=0; i<workloadTraces.length; i++) {
			for(int j=0; j<refVMSelection.length; j++) {
				JSONObject jsonObjectBase = new JSONObject();
				jsonObjectBase.put("workloadTrace", workloadTraces[i]);
				jsonObjectBase.put("vms", vmsType[i]);
				jsonObjectBase.put("selectionPolicy", refVMSelection[j]);
				overrideArray.put(jsonObjectBase);
			}
		}
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.9); tunningValue
				.compareTo(BigDecimal.valueOf(0.9)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.1))) {
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			jsonObjectBase.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectBase);
			
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
			jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyFPVmFore.put("vms", vmsType[i]);
			jsonObjectOnlyFPVmFore.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectOnlyFPVmFore);
		
			
			// interpeaksBollinger + facebookProphet
			for (int window = 9; window <= 9; window=window+1) {
				for (BigDecimal alpha = BigDecimal.valueOf(1.1); alpha
						.compareTo(BigDecimal.valueOf(1.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
		}
		}


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_azure_30() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				//"planetlab/planetlab_20110303_100_mostDiff",
				//"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				//"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				"azure/azure_azure2019_traces_100_mostDiff"
				};
		//Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		//Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		//Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		//vmDistributions.add(planetlab);
		//vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		//vmDistributions.add(alibaba);
		vmDistributions.add(azure);

		String[] vmsType = new String[] {
				//"vms4",
				//"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				//"vms4",
				"vms4_materna"
		};
		
		String[] refVMSelection = new String[] {"mc", "rs", "mmt", "mu"};
		for(int i=0; i<workloadTraces.length; i++) {
			for(int j=0; j<refVMSelection.length; j++) {
				JSONObject jsonObjectBase = new JSONObject();
				jsonObjectBase.put("workloadTrace", workloadTraces[i]);
				jsonObjectBase.put("vms", vmsType[i]);
				jsonObjectBase.put("selectionPolicy", refVMSelection[j]);
				overrideArray.put(jsonObjectBase);
			}
		}
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.9); tunningValue
				.compareTo(BigDecimal.valueOf(0.9)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.1))) {
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			jsonObjectBase.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectBase);
			
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
			jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyFPVmFore.put("vms", vmsType[i]);
			jsonObjectOnlyFPVmFore.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectOnlyFPVmFore);
		
			
			// interpeaksBollinger + facebookProphet
			for (int window = 8; window <= 8; window=window+1) {
				for (BigDecimal alpha = BigDecimal.valueOf(1.1); alpha
						.compareTo(BigDecimal.valueOf(1.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
		}
		}


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_azure_30_2() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				//"planetlab/planetlab_20110303_100_mostDiff",
				//"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				//"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				"azure/azure_azure2019_traces_100_mostDiff"
				};
		//Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		//Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		//Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		//vmDistributions.add(planetlab);
		//vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		//vmDistributions.add(alibaba);
		vmDistributions.add(azure);

		String[] vmsType = new String[] {
				//"vms4",
				//"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				//"vms4",
				"vms4_materna"
		};
		
		String[] refVMSelection = new String[] {"mc", "rs", "mmt", "mu"};
		for(int i=0; i<workloadTraces.length; i++) {
			for(int j=0; j<refVMSelection.length; j++) {
				JSONObject jsonObjectBase = new JSONObject();
				jsonObjectBase.put("workloadTrace", workloadTraces[i]);
				jsonObjectBase.put("vms", vmsType[i]);
				jsonObjectBase.put("selectionPolicy", refVMSelection[j]);
				overrideArray.put(jsonObjectBase);
			}
		}
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.9); tunningValue
				.compareTo(BigDecimal.valueOf(0.9)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.1))) {
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			jsonObjectBase.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectBase);
			
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
			jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyFPVmFore.put("vms", vmsType[i]);
			jsonObjectOnlyFPVmFore.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectOnlyFPVmFore);
		
			
			// interpeaksBollinger + facebookProphet
			for (int window = 7; window <= 9; window=window+1) {
				for (BigDecimal alpha = BigDecimal.valueOf(1.1); alpha
						.compareTo(BigDecimal.valueOf(1.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
		}
		}


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_planetlab_10traces_30() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				"planetlab/planetlab_20110303_100_mostDiff",
				"planetlab/planetlab_20110306_117_mostDiff",
				"planetlab/planetlab_20110309_123_mostDiff",
				"planetlab/planetlab_20110322_122_mostDiff",
				"planetlab/planetlab_20110325_105_mostDiff",
				"planetlab/planetlab_20110403_100_mostDiff",
				"planetlab/planetlab_20110409_110_mostDiff",
				"planetlab/planetlab_20110411_106_mostDiff",
				"planetlab/planetlab_20110412_110_mostDiff",
				"planetlab/planetlab_20110420_140_mostDiff"
				};
		Integer[] planetlab303      = new Integer[] {1,2,3,4,5};
		Integer[] planetlab306      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab309      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab322      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab325      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab403      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab409      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab411      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab412      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab420      = new Integer[] {1,3,4,5,3};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		vmDistributions.add(planetlab303);
		vmDistributions.add(planetlab306);
		vmDistributions.add(planetlab309);
		vmDistributions.add(planetlab322);
		vmDistributions.add(planetlab325);
		vmDistributions.add(planetlab403);
		vmDistributions.add(planetlab409);
		vmDistributions.add(planetlab411);
		vmDistributions.add(planetlab412);
		vmDistributions.add(planetlab420);

		String[] vmsType = new String[] {
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
		};
		
		String[] refVMSelection = new String[] {"mc", "rs", "mmt", "mu"};
		for(int i=0; i<workloadTraces.length; i++) {
			for(int j=0; j<refVMSelection.length; j++) {
				JSONObject jsonObjectBase = new JSONObject();
				jsonObjectBase.put("workloadTrace", workloadTraces[i]);
				jsonObjectBase.put("vms", vmsType[i]);
				jsonObjectBase.put("selectionPolicy", refVMSelection[j]);
				overrideArray.put(jsonObjectBase);
			}
		}
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.9); tunningValue
				.compareTo(BigDecimal.valueOf(0.9)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.1))) {
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			jsonObjectBase.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectBase);
			
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
			jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyFPVmFore.put("vms", vmsType[i]);
			jsonObjectOnlyFPVmFore.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectOnlyFPVmFore);
		
			
			// interpeaksBollinger + facebookProphet
			for (int window = 9; window <= 9; window=window+1) {
				for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha
						.compareTo(BigDecimal.valueOf(0.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
		}
		}


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_planetlab_10traces_30_wpsp() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				"planetlab/planetlab_20110303_100_mostDiff",
				"planetlab/planetlab_20110306_117_mostDiff",
				"planetlab/planetlab_20110309_123_mostDiff",
				"planetlab/planetlab_20110322_122_mostDiff",
				"planetlab/planetlab_20110325_105_mostDiff",
				"planetlab/planetlab_20110403_100_mostDiff",
				"planetlab/planetlab_20110409_110_mostDiff",
				"planetlab/planetlab_20110411_106_mostDiff",
				"planetlab/planetlab_20110412_110_mostDiff",
				"planetlab/planetlab_20110420_140_mostDiff"
				};
		Integer[] planetlab303      = new Integer[] {1,2,3,4,5};
		Integer[] planetlab306      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab309      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab322      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab325      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab403      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab409      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab411      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab412      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab420      = new Integer[] {1,3,4,5,3};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		vmDistributions.add(planetlab303);
		vmDistributions.add(planetlab306);
		vmDistributions.add(planetlab309);
		vmDistributions.add(planetlab322);
		vmDistributions.add(planetlab325);
		vmDistributions.add(planetlab403);
		vmDistributions.add(planetlab409);
		vmDistributions.add(planetlab411);
		vmDistributions.add(planetlab412);
		vmDistributions.add(planetlab420);

		String[] vmsType = new String[] {
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
		};
		
		String[] refVMSelection = new String[] {"mc", "rs", "mmt", "mu"};
		for(int i=0; i<workloadTraces.length; i++) {
			for(int j=0; j<refVMSelection.length; j++) {
				JSONObject jsonObjectBase = new JSONObject();
				jsonObjectBase.put("workloadTrace", workloadTraces[i]);
				jsonObjectBase.put("vms", vmsType[i]);
				jsonObjectBase.put("selectionPolicy", refVMSelection[j]);
				overrideArray.put(jsonObjectBase);
			}
		}
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.65); tunningValue
				.compareTo(BigDecimal.valueOf(0.85)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.05))) {
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			jsonObjectBase.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectBase);
			
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
			jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			jsonObjectOnlyFPVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyFPVmFore.put("vms", vmsType[i]);
			jsonObjectOnlyFPVmFore.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectOnlyFPVmFore);
		
			
			// interpeaksBollinger + facebookProphet
			for (int window = 9; window <= 9; window=window+1) {
				for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha
						.compareTo(BigDecimal.valueOf(0.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentInterpeaksbollingerConf = new JSONArray();
					currentInterpeaksbollingerConf.put("interpeaksbollinger");
					currentInterpeaksbollingerConf.put(window);
					currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
					currentInterpeaksbollingerConf.put(4);
					currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
		}
		}


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_planetlab_10traces_30_wpsp_arima() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				"planetlab/planetlab_20110303_100_mostDiff",
				"planetlab/planetlab_20110306_117_mostDiff",
				"planetlab/planetlab_20110309_123_mostDiff",
				"planetlab/planetlab_20110322_122_mostDiff",
				"planetlab/planetlab_20110325_105_mostDiff",
				"planetlab/planetlab_20110403_100_mostDiff",
				"planetlab/planetlab_20110409_110_mostDiff",
				"planetlab/planetlab_20110411_106_mostDiff",
				"planetlab/planetlab_20110412_110_mostDiff",
				"planetlab/planetlab_20110420_140_mostDiff"
				};
		Integer[] planetlab303      = new Integer[] {1,2,3,4,5};
		Integer[] planetlab306      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab309      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab322      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab325      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab403      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab409      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab411      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab412      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab420      = new Integer[] {1,3,4,5,3};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		vmDistributions.add(planetlab303);
		vmDistributions.add(planetlab306);
		vmDistributions.add(planetlab309);
		vmDistributions.add(planetlab322);
		vmDistributions.add(planetlab325);
		vmDistributions.add(planetlab403);
		vmDistributions.add(planetlab409);
		vmDistributions.add(planetlab411);
		vmDistributions.add(planetlab412);
		vmDistributions.add(planetlab420);

		String[] vmsType = new String[] {
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
		};
		
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.65); tunningValue
				.compareTo(BigDecimal.valueOf(0.65)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.05))) {
		for(int i=0; i<workloadTraces.length; i++) {
			JSONObject jsonObjectBase = new JSONObject();
			jsonObjectBase.put("workloadTrace", workloadTraces[i]);
			jsonObjectBase.put("vms", vmsType[i]);
			jsonObjectBase.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectBase);
			
			
			JSONArray autoarimaConf = new JSONArray();
			autoarimaConf.put("autoArima");
			
			// Only facebookProphet
			JSONObject jsonObjectOnlyArimaVmFore = new JSONObject();
			jsonObjectOnlyArimaVmFore.put("hostForecastingTechnique", autoarimaConf);
			jsonObjectOnlyArimaVmFore.put("vmForecastingTechniqueAccurate", autoarimaConf);
			jsonObjectOnlyArimaVmFore.put("workloadTrace", workloadTraces[i]);
			jsonObjectOnlyArimaVmFore.put("vms", vmsType[i]);
			jsonObjectOnlyArimaVmFore.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
			overrideArray.put(jsonObjectOnlyArimaVmFore);
		}
		}


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_planetlab_10traces_30_wbf() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				"planetlab/planetlab_20110303_100_mostDiff",
				"planetlab/planetlab_20110306_117_mostDiff",
				"planetlab/planetlab_20110309_123_mostDiff",
				"planetlab/planetlab_20110322_122_mostDiff",
				"planetlab/planetlab_20110325_105_mostDiff",
				"planetlab/planetlab_20110403_100_mostDiff",
				"planetlab/planetlab_20110409_110_mostDiff",
				"planetlab/planetlab_20110411_106_mostDiff",
				"planetlab/planetlab_20110412_110_mostDiff",
				"planetlab/planetlab_20110420_140_mostDiff"
				};
		Integer[] planetlab303      = new Integer[] {1,2,3,4,5};
		Integer[] planetlab306      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab309      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab322      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab325      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab403      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab409      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab411      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab412      = new Integer[] {1,3,4,5,3};
		Integer[] planetlab420      = new Integer[] {1,3,4,5,3};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		vmDistributions.add(planetlab303);
		vmDistributions.add(planetlab306);
		vmDistributions.add(planetlab309);
		vmDistributions.add(planetlab322);
		vmDistributions.add(planetlab325);
		vmDistributions.add(planetlab403);
		vmDistributions.add(planetlab409);
		vmDistributions.add(planetlab411);
		vmDistributions.add(planetlab412);
		vmDistributions.add(planetlab420);

		String[] vmsType = new String[] {
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
				"vms4",
		};
		
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.65); tunningValue
				.compareTo(BigDecimal.valueOf(0.65)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.05))) {
		for(int i=0; i<workloadTraces.length; i++) {	
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Bollinger + facebookProphet
			for (int window = 9; window <= 9; window=window+1) {
				for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha
						.compareTo(BigDecimal.valueOf(0.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentBollingerConf = new JSONArray();
					currentBollingerConf.put("bollinger");
					currentBollingerConf.put(window);
					currentBollingerConf.put("-_-" + alpha.toString() + "-_-");

					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentBollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
			
		}
		}


		return overrideArray;
	}
	
	
	public static JSONArray getOverride_paper4_test3_azure_30_wbf() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				//"planetlab/planetlab_20110303_100_mostDiff",
				//"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				//"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				"azure/azure_azure2019_traces_100_mostDiff"
				};
		//Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		//Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		//Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		//vmDistributions.add(planetlab);
		//vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		//vmDistributions.add(alibaba);
		vmDistributions.add(azure);

		String[] vmsType = new String[] {
				//"vms4",
				//"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				//"vms4",
				"vms4_materna"
		};
		
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.9); tunningValue
				.compareTo(BigDecimal.valueOf(0.9)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.1))) {
		for(int i=0; i<workloadTraces.length; i++) {
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Bollinger + facebookProphet
			for (int window = 8; window <= 8; window=window+1) {
				for (BigDecimal alpha = BigDecimal.valueOf(1.1); alpha
						.compareTo(BigDecimal.valueOf(1.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentBollingerConf = new JSONArray();
					currentBollingerConf.put("bollinger");
					currentBollingerConf.put(window);
					currentBollingerConf.put("-_-" + alpha.toString() + "-_-");

					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentBollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
		}
		}


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_alibaba_30_wbf() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				//"planetlab/planetlab_20110303_100_mostDiff",
				//"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				//"azure/azure_azure2019_traces_100_mostDiff"
				};
		//Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		//Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		//Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		//vmDistributions.add(planetlab);
		//vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		vmDistributions.add(alibaba);
		//vmDistributions.add(azure);

		String[] vmsType = new String[] {
				//"vms4",
				//"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				"vms4",
				//"vms4_materna"
		};
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.9); tunningValue
				.compareTo(BigDecimal.valueOf(0.9)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.1))) {
		for(int i=0; i<workloadTraces.length; i++) {
			
			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Bollinger + facebookProphet
			for (int window = 9; window <= 9; window=window+1) {
				for (BigDecimal alpha = BigDecimal.valueOf(1.1); alpha
						.compareTo(BigDecimal.valueOf(1.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentBollingerConf = new JSONArray();
					currentBollingerConf.put("bollinger");
					currentBollingerConf.put(window);
					currentBollingerConf.put("-_-" + alpha.toString() + "-_-");

					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentBollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
		}
		}


		return overrideArray;
	}
	
	public static JSONArray getOverride_paper4_test3_materna_30_wbf() {
		JSONArray overrideArray = new JSONArray();
		
		String[] workloadTraces = new String[] {
				//"planetlab/planetlab_20110303_100_mostDiff",
				"materna/materna_trace1_valid_0_288_100_mostDiff",
				//"bitbrains/bitbrains_rnd_2013-8_100_mostDiff_short",
				//"bitbrains/bitbrains_rnd_2013-9_valid_0_576_original_100_mostDiff",
				//"bitbrains/fastStorage_2013-8_100_vms_short",
				//"alibaba2018/alibaba2018_fixed_first_500_100_mostDiff",
				//"azure/azure_azure2019_traces_100_mostDiff"
				};
		//Integer[] planetlab      = new Integer[] {1,2,3,4,5};
		Integer[] materna_trace1 = new Integer[] {2,4,5,4,3};
		//Integer[] bitbrains_rnd8 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_rnd9 = new Integer[] {1,3,3,4,5};
		//Integer[] bitbrains_fast = new Integer[] {3,6,4,3,2};
		//Integer[] alibaba        = new Integer[] {1,2,3,4,5};
		//Integer[] azure          = new Integer[] {1,4,5,3,2};

		ArrayList<Integer[]> vmDistributions = new ArrayList<>();
		//vmDistributions.add(planetlab);
		vmDistributions.add(materna_trace1);
		//vmDistributions.add(bitbrains_rnd8);
		//vmDistributions.add(bitbrains_rnd9);
		//vmDistributions.add(bitbrains_fast);
		//vmDistributions.add(alibaba);
		//vmDistributions.add(azure);

		String[] vmsType = new String[] {
				//"vms4",
				"vms4_materna",
				//"vms4",
				//"vms4",
				//"vms4_materna",
				//"vms4",
				//"vms4_materna"
		};
		
		for (BigDecimal tunningValue = BigDecimal.valueOf(0.9); tunningValue
				.compareTo(BigDecimal.valueOf(0.9)) <= 0; tunningValue = tunningValue.add(BigDecimal.valueOf(0.1))) {
		for(int i=0; i<workloadTraces.length; i++) {

			JSONArray facebookProphetConf = new JSONArray();
			facebookProphetConf.put("fbProphet");
			
			// Bollinger + facebookProphet
			for (int window = 9; window <= 9; window=window+1) {
				for (BigDecimal alpha = BigDecimal.valueOf(1.0); alpha
						.compareTo(BigDecimal.valueOf(1.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentBollingerConf = new JSONArray();
					currentBollingerConf.put("bollinger");
					currentBollingerConf.put(window);
					currentBollingerConf.put("-_-" + alpha.toString() + "-_-");

					JSONObject jsonObject = new JSONObject();
					jsonObject.put("hostSignalProcessing", currentBollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
					
					jsonObject.put("hostForecastingTechnique", facebookProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
					jsonObject.put("workloadTrace", workloadTraces[i]);
					jsonObject.put("vms", vmsType[i]);
					jsonObject.put("tunningValue", "-_-" + tunningValue.toString() + "-_-");
					
					overrideArray.put(jsonObject);
				}
			}
		}
		}


		return overrideArray;
	}

}
