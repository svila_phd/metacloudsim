package svila.planetlabNetwork.Executers;

import java.nio.file.Paths;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.GlobalRunner;
import svila.planetlabNetwork.StaticResources;
import svila.planetlabNetwork.results.StatusFile;
import svila.python.PythonBridge;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.cloudbus.cloudsim.VmSchedulerTimeShared;

public class ExperimentFolderExecuter {
	public static void main(String[] args) {
		StaticResources.init();
	
		PythonBridge.init();
		
		//String folder = "final_jp_iqr_cn_19_2";
		//String folder = "clusteredHosts";
		
		String[] folders = {"Future_100_VMs_no_migrations"};
		boolean generateExperiments = true;
		
		// testingCPUCalculatorRollingFixing
		int numFiles = getNumFiles(folders, generateExperiments);
		long seconds = 0;
		int numExperimentsCompleted = 0;
		StatusFile statusFile = new StatusFile();
		//StaticResources.pressAnyKeyToContinue();
		
		for(String folder : folders) {
			if(generateExperiments) {
				ExperimentConfigurationGenerator.execute(folder);
			}
			
			Path folderPath = Paths.get(StaticResources.baseFolder,
										StaticResources.generatedExperimentsFolderName,
										folder);
			statusFile.generate(folder);
			for(File file : folderPath.toFile().listFiles()) {
				if(file.getName().endsWith(".json")) {
					VmSchedulerTimeShared.clearMips();
					StaticResources.loadExperiment(Paths.get(StaticResources.generatedExperimentsFolderName, folder).toString(),
							file.getName().replaceFirst("[.][^.]+$", ""));
					ExperimentConfiguration ec = StaticResources.getCE();
					System.out.println(ec);
					statusFile.setCurrentExperiment(ec);
					Instant startTime = Instant.now();
					try {
						runExperiment(ec);
					} catch (Exception e) {
						statusFile.sendError(e);
					} finally {
						if(!statusFile.isError()) {
							statusFile.sendOk();
						}
					}
					System.gc();
					numExperimentsCompleted++;
					Duration experimentTime = Duration.between(startTime, Instant.now());
					String.format("%02d:%02d", experimentTime.toMinutes(), experimentTime.getSeconds()%60);
					seconds += experimentTime.getSeconds();
					long expectedTime = seconds*numFiles/numExperimentsCompleted;
					long remainingTime = expectedTime - seconds;

					System.out.println(String.format("%02d:%02d:%02d/%02d:%02d:%02d (remaining: %02d:%02d:%02d)", seconds/3600, seconds/60%60, seconds%60,
										expectedTime/3600, expectedTime/60%60, expectedTime%60,
										remainingTime/3600, remainingTime/60%60, remainingTime%60
										));
					System.out.println(numExperimentsCompleted + "/" + numFiles);
					System.out.println("mipsNotAllocated: " + VmSchedulerTimeShared.mipsOver);
					System.out.printf("mipsSum: %f\n", VmSchedulerTimeShared.mipsSum);
					System.out.println("mipsOver: " + VmSchedulerTimeShared.mipsOver);
					System.out.printf("Ratio: %f%%\n", VmSchedulerTimeShared.mipsOver/VmSchedulerTimeShared.mipsSum*100);
				}
			}
			
			statusFile.close();
		}
	}
	
	private static int getNumFiles(String[] folders, boolean generateExperiments) {
		int numExperiments = 0;
		for(String folder : folders) {
			if(generateExperiments) {
				ExperimentConfigurationGenerator.execute(folder);
			}
			
			Path folderPath = Paths.get(StaticResources.baseFolder,
										StaticResources.generatedExperimentsFolderName,
										folder);
			for(File file : folderPath.toFile().listFiles()) {
				if(file.getName().endsWith(".json")) {
					numExperiments++;
				}
			}
		}
		return numExperiments;
	}
	
	public static void runExperiment(ExperimentConfiguration ec) {
		new GlobalRunner(
                ec
        );
	}
}
