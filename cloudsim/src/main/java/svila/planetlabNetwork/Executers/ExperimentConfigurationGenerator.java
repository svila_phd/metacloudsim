package svila.planetlabNetwork.Executers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.nio.file.Path;

import org.jgrapht.alg.util.Pair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import svila.planetlabNetwork.StaticResources;

public class ExperimentConfigurationGenerator {
	
	public static void main(String[] args) {
		String testbedName = "pearsonDev";
		execute(testbedName);
    }
	
	public static void execute(String testbedName) {
		StaticResources.init();
		
		Path inputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.testbedFolderName,
				testbedName + ".json");
		Path outputFolderPath = Paths.get(StaticResources.baseFolder,
				StaticResources.generatedExperimentsFolderName,
				testbedName);

		JSONObject[] results = getJSONCombinations(inputPath);
		
		StaticResources.purgeDirectory(new File(outputFolderPath.toString()));
		
		writeJSONFiles(results, outputFolderPath);
	}
	
	public static void writeJSONFiles(JSONObject[] results, Path outputFolderPath) {
		File folder = new File(outputFolderPath.toString());
		folder.mkdir();
		for(int i=0; i<results.length; i++) {
			String experimentCode = String.format("%05d", i);
			File currentOutputFile = new File(Paths.get(folder.toString(), results[i].get("name") + "_" + experimentCode+".json").toString());
			try {
				BufferedWriter writer = new BufferedWriter(new FileWriter(currentOutputFile));
				results[i].put("name", results[i].get("name") + "_" + experimentCode);
				results[i].write(writer, 4, 4);
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
	}
	
	public static JSONObject[] getJSONCombinations(Path inputPath){
		JSONObject experimentBase = null;
		try {
			experimentBase = new JSONObject(new String(Files.readAllBytes(inputPath),"UTF-8"));
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JSONArray overrideArray = experimentBase.getJSONArray("override");
		experimentBase.remove("override");
		
		Set<String> keySet = experimentBase.keySet();
		
		String[] keyArray = keySet.toArray(new String[keySet.size()]);
		Object[][] matrix = new Object[keySet.size()][];
		for(int i=0; i<keySet.size(); i++) {
			JSONArray elementsArray = experimentBase.getJSONArray(keyArray[i]);
			Object[] objectsArray = new Object[elementsArray.length()];
			for(int j=0; j<elementsArray.length(); j++) {
				objectsArray[j] = elementsArray.get(j);
			}
			matrix[i] = objectsArray;
		}
		JSONObject[] combinations = getCombinations(keyArray, matrix);
		
		List<JSONObject> finalCombinations = new ArrayList<>();
		
		Collections.addAll(finalCombinations, combinations);
		
		for(JSONObject combination : combinations) {
			for(int i=0; i<overrideArray.length(); i++) {
				JSONObject newCombination = new JSONObject(combination, JSONObject.getNames(combination));
				JSONObject overrideData = overrideArray.getJSONObject(i);
				for(String key : overrideData.keySet()) {
					newCombination.put(key, overrideData.get(key));
				}
				finalCombinations.add(newCombination);
			}
		}
		
		
		return finalCombinations.toArray(new JSONObject[0]);
	}

    private static JSONObject[] getCombinations(String[] names, Object[][] sets) {

      int[] counters = new int[sets.length];
        int count = 1;   
        int count2 = 0;

        for (int i = 0; i < sets.length; i++) {
          count *= sets[i].length;
        }

        JSONObject[] combinations = new JSONObject[count];

        do{
           combinations[count2++] = getCombinationString(counters, names, sets);
        } while(increment(counters, sets));

        return combinations;
    }

    private static JSONObject getCombinationString(int[] counters, String[] names, Object[][] sets) {
      JSONObject jobject = new JSONObject();
      for(int i = 0; i<counters.length;i++) {
    	  jobject.put(names[i], sets[i][counters[i]]);
      }
      
      return jobject;

    }

    private static boolean increment(int[] counters, Object[][] sets) {
        for(int i=counters.length-1;i>=0;i--) {
            if(counters[i] < sets[i].length-1) {
                counters[i]++;
                return true;
            } else {
                counters[i] = 0;
            }
        }
        return false;
    }
}
