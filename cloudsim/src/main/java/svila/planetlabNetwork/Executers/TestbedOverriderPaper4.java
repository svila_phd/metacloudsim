package svila.planetlabNetwork.Executers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import svila.planetlabNetwork.StaticResources;

public class TestbedOverriderPaper4 {
	public static void main(String[] args) {
		StaticResources.init();

		String testbed = "paper4_test_facebook_neural_bollinger_interpeaksbollinger_no_balance";

		Path inputPath = Paths.get(StaticResources.baseFolder, StaticResources.testbedFolderName, testbed + ".json");

		Path outputPath = Paths.get(StaticResources.baseFolder, StaticResources.testbedFolderName,
				testbed + "_o" + ".json");

		JSONObject testbedConfiguration = null;
		try {
			testbedConfiguration = new JSONObject(new String(Files.readAllBytes(inputPath), "UTF-8"));
			testbedConfiguration.put("name",
					new JSONArray().put(testbedConfiguration.getJSONArray("name").get(0) + "_o"));
			testbedConfiguration.put("experimentOutputFolder",
					new JSONArray().put(testbedConfiguration.getJSONArray("experimentOutputFolder").get(0) + "_o"));
		} catch (JSONException | IOException e) {
			e.printStackTrace();
		}

		JSONArray overrideArray = TestbedOverriderPaper4.getOverride_facebook_neural_bollinger_interpeaksbollinger();

		testbedConfiguration.put("override", overrideArray);

		try {
			File currentOutputFile = new File(outputPath.toString());
			BufferedWriter writer = new BufferedWriter(new FileWriter(currentOutputFile));
			testbedConfiguration.write(writer, 4, 0);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String content;
		try {
			content = new String(Files.readAllBytes(outputPath));
			content = content.replaceAll("\"-_-", "");
			content = content.replaceAll("-_-\"", "");
			Files.write(outputPath, content.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static JSONArray getOverride() {
		JSONArray overrideArray = new JSONArray();
		for (int window = 3; window <= 15; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(2)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostForecastingTechnique", new JSONArray().put("neuralProphet"));
				jsonObject.put("vmForecastingTechniqueAccurate", new JSONArray().put("neuralProphet"));
				JSONArray bollingerConf = new JSONArray();
				bollingerConf.put("bollinger");
				bollingerConf.put(window);
				bollingerConf.put("-_-" + alpha.toString() + "-_-");
				jsonObject.put("hostSignalProcessing", bollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", bollingerConf);
				jsonObject.put("vmSignalProcessingBasic", bollingerConf);
				overrideArray.put(jsonObject);
			}
		}
		return overrideArray;
	}

	public static JSONArray getOverrideOnlyBollinger() {
		JSONArray overrideArray = new JSONArray();
		for (int window = 3; window <= 15; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(2)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONObject jsonObject = new JSONObject();
				JSONArray bollingerConf = new JSONArray();
				bollingerConf.put("bollinger");
				bollingerConf.put(window);
				bollingerConf.put("-_-" + alpha.toString() + "-_-");
				jsonObject.put("hostSignalProcessing", bollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", bollingerConf);
				jsonObject.put("vmSignalProcessingBasic", bollingerConf);
				overrideArray.put(jsonObject);
			}
		}
		return overrideArray;
	}

	public static JSONArray getOverrideAllPlanetlab() {
		JSONArray overrideArray = new JSONArray();

		String[] planetlabTraces = new String[] { "planetlab_20110303_100_mostDiff", "planetlab_20110306_117_mostDiff",
				"planetlab_20110309_123_mostDiff", "planetlab_20110322_122_mostDiff", "planetlab_20110325_105_mostDiff",
				"planetlab_20110403_90_mostDiff", "planetlab_20110409_110_mostDiff", "planetlab_20110411_106_mostDiff",
				"planetlab_20110412_110_mostDiff", "planetlab_20110420_140_mostDiff" };
		Integer[] numVmsList = new Integer[] { 100, 117, 123, 122, 105, 90, 110, 106, 110, 140 };
		JSONArray bollingerConf = new JSONArray();
		bollingerConf.put("bollinger");
		bollingerConf.put(6);
		bollingerConf.put("-_-" + "0.5" + "-_-");

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Reference for other workloads
		// i=1, avoiding first workload
		for (int i = 1; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);
			overrideArray.put(jsonObject);
		}

		// Bollinger + neuralProphet
		for (int i = 0; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);

			jsonObject.put("hostSignalProcessing", bollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", bollingerConf);
			jsonObject.put("vmSignalProcessingBasic", bollingerConf);

			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			overrideArray.put(jsonObject);
		}

		// Only bollinger
		for (int i = 0; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);

			jsonObject.put("hostSignalProcessing", bollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", bollingerConf);
			jsonObject.put("vmSignalProcessingBasic", bollingerConf);
			overrideArray.put(jsonObject);
		}

		// Only neuralProphet
		for (int i = 0; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);

			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			overrideArray.put(jsonObject);
		}

		return overrideArray;
	}

	public static JSONArray getOverrideAllPlanetlabBestParams() {
		JSONArray overrideArray = new JSONArray();

		String[] planetlabTraces = new String[] { "planetlab_20110303_100_mostDiff", "planetlab_20110306_117_mostDiff",
				"planetlab_20110309_123_mostDiff", "planetlab_20110322_122_mostDiff", "planetlab_20110325_105_mostDiff",
				"planetlab_20110403_90_mostDiff", "planetlab_20110409_110_mostDiff", "planetlab_20110411_106_mostDiff",
				"planetlab_20110412_110_mostDiff", "planetlab_20110420_140_mostDiff" };
		Integer[] numVmsList = new Integer[] { 100, 117, 123, 122, 105, 90, 110, 106, 110, 140 };
		JSONArray bollingerConf = new JSONArray();
		bollingerConf.put("bollinger");
		bollingerConf.put(6);
		bollingerConf.put("-_-" + "0.5" + "-_-");

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Reference for other workloads
		// i=1, avoiding first workload

		for (int i = 1; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);
			overrideArray.put(jsonObject);
		}

		// Bollinger + neuralProphet
		/*
		 * for(int i=0; i<planetlabTraces.length; i++) { for(int window=3; window<=15;
		 * window++) { for(BigDecimal alpha = BigDecimal.valueOf(0.1);
		 * alpha.compareTo(BigDecimal.valueOf(2)) < 0; alpha =
		 * alpha.add(BigDecimal.valueOf(0.1))) { JSONArray currentBollingerConf = new
		 * JSONArray(); currentBollingerConf.put("bollinger");
		 * currentBollingerConf.put(window); currentBollingerConf.put("-_-" +
		 * alpha.toString() + "-_-"); String planetlabTrace = planetlabTraces[i];
		 * Integer numVms = numVmsList[i]; JSONObject jsonObject = new JSONObject();
		 * jsonObject.put("workloadTrace", planetlabTrace);
		 * jsonObject.put("numCloudlets", numVms);
		 * 
		 * jsonObject.put("hostSignalProcessing", currentBollingerConf);
		 * jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
		 * jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
		 * 
		 * jsonObject.put("hostForecastingTechnique", neuralProphetConf);
		 * jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		 * overrideArray.put(jsonObject); } } }
		 */

		// Only bollinger

		for (int i = 0; i < planetlabTraces.length; i++) {
			for (int window = 3; window <= 15; window++) {
				for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha
						.compareTo(BigDecimal.valueOf(2)) < 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentBollingerConf = new JSONArray();
					currentBollingerConf.put("bollinger");
					currentBollingerConf.put(window);
					currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
					String planetlabTrace = planetlabTraces[i];
					Integer numVms = numVmsList[i];
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("workloadTrace", planetlabTrace);
					jsonObject.put("numCloudlets", numVms);

					jsonObject.put("hostSignalProcessing", currentBollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
					overrideArray.put(jsonObject);
				}
			}
		}

		// Only neuralProphet
		for (int i = 0; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);

			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			overrideArray.put(jsonObject);
		}

		return overrideArray;
	}

	public static JSONArray getOverrideBB_NN() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Reference for other workloads
		// i=1, avoiding first workload

		// Bollinger + neuralProphet
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet, nn only vm selection
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				// jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet, nn only host saturation
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				// jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only bollinger
		for (int window = 3; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only neuralProphet
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("hostForecastingTechnique", neuralProphetConf);
		jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObject);

		return overrideArray;
	}

	public static JSONArray getOverride_Fix() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Reference for other workloads
		// i=1, avoiding first workload

		// Bollinger + neuralProphet
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet, nn only vm selection
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				// jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet, nn only host saturation
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				// jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only bollinger
		for (int window = 3; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only interpeaks
		for (int window = 3; window <= 6; window++) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("interpeaks");
			currentBollingerConf.put(window);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			overrideArray.put(jsonObject);
		}

		// Interpeaks + neuralProphet
		for (int window = 3; window <= 6; window++) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("interpeaks");
			currentBollingerConf.put(window);
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			overrideArray.put(jsonObject);
		}

		// Only neuralProphet
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("hostForecastingTechnique", neuralProphetConf);
		jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObject);

		return overrideArray;
	}

	public static JSONArray getOverride_Fix3() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Reference for other workloads
		// i=1, avoiding first workload

		// Bollinger + neuralProphet
		for (int window = 6; window <= 6; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.5);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			overrideArray.put(jsonObject);
		}

		// Only bollinger
		for (int window = 6; window <= 6; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.5);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			overrideArray.put(jsonObject);
		}

		// Only neuralProphet
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("hostForecastingTechnique", neuralProphetConf);
		jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObject);

		return overrideArray;
	}

	public static JSONArray getOverride_interpeaks_test() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Only bollinger
		for (int window = 5; window <= 5; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.5);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			overrideArray.put(jsonObject);
		}

		// Only interpeaks
		for (int window = 3; window <= 3; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(2.0); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}

	public static JSONArray getOverride_interpeaks_test_plus() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Only bollinger
		for (int window = 5; window <= 5; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.5);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			overrideArray.put(jsonObject);
		}

		// Only interpeaks
		for (int window = 3; window <= 7; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(0.1); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}

	public static JSONArray getOverride_pre_data() {
		JSONArray overrideArray = new JSONArray();

		// Only bollinger
		for (int window = 12; window <= 12; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.1);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			overrideArray.put(jsonObject);
		}

		// Only interpeaks
		for (int window = 3; window <= 7; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(0.1); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}

	public static JSONArray getOverride_pre_data_autoarima() {
		JSONArray overrideArray = new JSONArray();

		// Only bollinger
		for (int window = 12; window <= 12; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.1);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			overrideArray.put(jsonObject);
		}

		// Only interpeaks
		for (int window = 3; window <= 7; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(0.1); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}
	
	public static JSONArray getOverride_test_wpsp() {
		JSONArray overrideArray = new JSONArray();
		
		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);
		
		
		for (int window = 3; window <= 25; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.7); alpha
					.compareTo(BigDecimal.valueOf(1.2)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			
			overrideArray.put(jsonObject);
		}
		}
		
		for (int window = 3; window <= 7; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(-0.6); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}
		
		
		
		JSONArray facebookProphetConf = new JSONArray();
		facebookProphetConf.put("fbProphet");
		
		// Only facebookProphet
		JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
		jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
		jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
		overrideArray.put(jsonObjectOnlyFPVmFore);

		overrideArray.put(jsonObjectOnlyFPVmFore);
		
		for (int window = 3; window <= 25; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.7); alpha
					.compareTo(BigDecimal.valueOf(1.2)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", facebookProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			
			overrideArray.put(jsonObject);
			}
		}
		
		for (int window = 3; window <= 7; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(0.1); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}
	
	
	public static JSONArray getOverride_facebook_bollinger_tunning() {
		JSONArray overrideArray = new JSONArray();

		JSONArray facebookProphetConf = new JSONArray();
		facebookProphetConf.put("fbProphet");
		
		// Bollinger + neuralProphet
		for (int window = 3; window <= 25; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.6); alpha
					.compareTo(BigDecimal.valueOf(1.5)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);

				overrideArray.put(jsonObject);
			}
		}
				
		// Only neuralProphet
		JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
		jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
		jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
		overrideArray.put(jsonObjectOnlyFPVmFore);

		return overrideArray;
	}
	
	public static JSONArray getOverride_facebook_neural_prophet() {
		JSONArray overrideArray = new JSONArray();

		/*
		// Only bollinger
		for (int window = 12; window <= 12; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.1);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			overrideArray.put(jsonObject);
		}

		// Only interpeaks
		for (int window = 3; window <= 7; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(0.1); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				overrideArray.put(jsonObject);
			}
		}
		*/
		
		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);
		
		
		for (int window = 12; window <= 12; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.1);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			
			overrideArray.put(jsonObject);
		}
		
		for (int window = 3; window <= 7; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(0.1); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}
		
		
		
		JSONArray facebookProphetConf = new JSONArray();
		facebookProphetConf.put("fbProphet");
		
		// Only facebookProphet
		JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
		jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
		jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
		overrideArray.put(jsonObjectOnlyFPVmFore);

		overrideArray.put(jsonObjectOnlyFPVmFore);
		
		for (int window = 12; window <= 12; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.1);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", facebookProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			
			overrideArray.put(jsonObject);
		}
		
		for (int window = 3; window <= 7; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(0.1); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}
	
	public static JSONArray getOverride_facebook_neural_bollinger_interpeaks() {
		JSONArray overrideArray = new JSONArray();

		/*
		// Only bollinger
		for (int window = 12; window <= 12; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.1);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			overrideArray.put(jsonObject);
		}

		// Only interpeaks
		for (int window = 3; window <= 7; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(0.1); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				overrideArray.put(jsonObject);
			}
		}
		*/
		
		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);
		
		
		for (int window = 12; window <= 12; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.1);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			
			overrideArray.put(jsonObject);
		}
		
		for (int window = 3; window <= 25; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(-1); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}
		
		
		
		JSONArray facebookProphetConf = new JSONArray();
		facebookProphetConf.put("fbProphet");
		
		// Only facebookProphet
		JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
		jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
		jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
		overrideArray.put(jsonObjectOnlyFPVmFore);

		overrideArray.put(jsonObjectOnlyFPVmFore);
		
		for (int window = 15; window <= 15; window++) {
			BigDecimal alpha = BigDecimal.valueOf(-0.2);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", facebookProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			
			overrideArray.put(jsonObject);
		}
		
		for (int window = 3; window <= 25; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(-1); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}
	
	public static JSONArray getOverride_facebook_neural_bollinger_interpeaksbollinger() {
		JSONArray overrideArray = new JSONArray();

		/*
		// Only bollinger
		for (int window = 12; window <= 12; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.1);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			overrideArray.put(jsonObject);
		}

		// Only interpeaks
		for (int window = 3; window <= 7; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(0.1); zScore
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				overrideArray.put(jsonObject);
			}
		}
		*/
		
		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);
		
		
		for (int window = 12; window <= 12; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.1);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			
			overrideArray.put(jsonObject);
		}
		
		for (int window = 3; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.5)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksbollingerConf = new JSONArray();
				currentInterpeaksbollingerConf.put("interpeaksbollinger");
				currentInterpeaksbollingerConf.put(window);
				currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
				currentInterpeaksbollingerConf.put(4);
				currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
				
				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}
		
		
		
		JSONArray facebookProphetConf = new JSONArray();
		facebookProphetConf.put("fbProphet");
		
		// Only facebookProphet
		JSONObject jsonObjectOnlyFPVmFore = new JSONObject();
		jsonObjectOnlyFPVmFore.put("hostForecastingTechnique", facebookProphetConf);
		jsonObjectOnlyFPVmFore.put("vmForecastingTechniqueAccurate", facebookProphetConf);
		overrideArray.put(jsonObjectOnlyFPVmFore);

		overrideArray.put(jsonObjectOnlyFPVmFore);
		
		for (int window = 15; window <= 15; window++) {
			BigDecimal alpha = BigDecimal.valueOf(-0.2);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			
			jsonObject.put("hostForecastingTechnique", facebookProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
			
			overrideArray.put(jsonObject);
		}
		
		for (int window = 3; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-1.0); alpha
					.compareTo(BigDecimal.valueOf(1.5)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksbollingerConf = new JSONArray();
				currentInterpeaksbollingerConf.put("interpeaksbollinger");
				currentInterpeaksbollingerConf.put(window);
				currentInterpeaksbollingerConf.put("-_-" + alpha.toString() + "-_-");
				currentInterpeaksbollingerConf.put(4);
				currentInterpeaksbollingerConf.put("-_-" + "1.0" + "-_-");
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksbollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksbollingerConf);
				
				jsonObject.put("hostForecastingTechnique", facebookProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", facebookProphetConf);
				
				overrideArray.put(jsonObject);
			}
		}

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}
}
