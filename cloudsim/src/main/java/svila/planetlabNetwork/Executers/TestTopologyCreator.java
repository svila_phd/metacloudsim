package svila.planetlabNetwork.Executers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import svila.planetlabNetwork.DeprecatedInteractionsCreator;
import svila.planetlabNetwork.Graph;
import svila.planetlabNetwork.Node;
import svila.planetlabNetwork.TopologyCreator;

public class TestTopologyCreator {
	public static void main(String[] argv) {
		List<Integer> interconnectedLevels = new ArrayList<>(
				Arrays.asList(3,3)
				);
		List<Integer> treeLevels = new ArrayList<>(
				Arrays.asList(2, 5)
				);
		TopologyCreator tc = new TopologyCreator(interconnectedLevels,
													treeLevels,
													0.6f,
													0,
													1000.0f,
													100.0f,
													new ArrayList<Pair<Integer, Integer>>());
		tc.createTopology();
		Graph g = tc.getGraph();
		System.out.println(g);
		System.out.println(tc.getCentralNodes().size());
		System.out.println(tc.getLeafNodes().size());
		
		float internalClusterRate = 0.3f;
		float externalClusterRate = 0.05f;
		
		List<Node> leafList = new ArrayList<>();
		leafList.addAll(tc.getLeafNodes().values());
		DeprecatedInteractionsCreator ic = new DeprecatedInteractionsCreator(leafList);
		ic.createInteractions(internalClusterRate, externalClusterRate);
		List<Pair<Integer, Integer>> newLinks = ic.getPairLinks();
		for(Pair<Integer, Integer> link : newLinks) {
			g.linkNodes(link.getLeft(), link.getRight(), 1.0f);
			System.out.println(link.getLeft() + " - " + link.getRight());
		}
		
		System.out.println(g.getAdjacencyMatrix());
	}
}
