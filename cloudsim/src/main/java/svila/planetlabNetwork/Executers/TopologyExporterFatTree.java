package svila.planetlabNetwork.Executers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONArray;
import org.json.JSONObject;

import svila.planetlabNetwork.FatTreeTopologyCreator;
import svila.planetlabNetwork.Graph;
import svila.planetlabNetwork.Link;
import svila.planetlabNetwork.Node;
import svila.planetlabNetwork.StaticResources;
import svila.planetlabNetwork.TopologyCreator;

public class TopologyExporterFatTree {	
	public static void main(String[] argv) {
		StaticResources.init();
		String baseFolder = StaticResources.baseFolder;
		String topologiesFolderName = StaticResources.topologiesFolderName;
		
		int k 					  = 4;
		int aggregationGroupsPerK = 2;
		int aggregationSwitchs 	  = 4;
		int hostsPerSwitch		  = 2;
		int numHosts = k * aggregationGroupsPerK * aggregationSwitchs * hostsPerSwitch;
		
		float defaultWeightInterconnected = 1000.0f;
		float defaultWeightTree = 100.0f;
		
		String topologyName = "fatTreeTopology" + "_k-" + k + "_g-" + aggregationGroupsPerK +
				"_s-" + aggregationSwitchs + "_hs-" + hostsPerSwitch + "_h-" + numHosts;
		
		String outputFilePath = Paths.get(baseFolder, topologiesFolderName, topologyName + ".json").toString();
		System.out.println("Output file: " + outputFilePath);

		TopologyCreator tc = new FatTreeTopologyCreator(k, aggregationGroupsPerK,
				aggregationSwitchs, hostsPerSwitch, defaultWeightInterconnected,
				defaultWeightTree);
		tc.createTopology();
		exportGraph(tc, outputFilePath);
		//System.out.println(tc.getGraph().getAdjacencyMatrix());
		System.out.println("Is graph valid: " + tc.getGraph().check());
	}
	
	public static void exportFromJSONObject(JSONObject jsonObject) {
		JSONArray interconnectedLevelsArray = jsonObject.getJSONArray("interconnectedLevels");
		List<Integer> interconnectedLevels = new ArrayList<>();
		for(int i=0; i<interconnectedLevelsArray.length(); i++) {
			interconnectedLevels.add(interconnectedLevelsArray.getInt(i));
		}
		
		JSONArray treeLevelsArray = jsonObject.getJSONArray("treeLevels");
		List<Integer> treeLevels = new ArrayList<>();
		for(int i=0; i<treeLevelsArray.length(); i++) {
			treeLevels.add(treeLevelsArray.getInt(i));
		}

		float degreeOfInterconnection = jsonObject.getBigDecimal("degreeOfInterconnection").floatValue();
		long randomSeed = jsonObject.getLong("randomSeed");
		float defaultWeightInterconnected = jsonObject.getBigDecimal("defaultWeightInterconnected").floatValue();;
		float defaultWeightTree = jsonObject.getBigDecimal("defaultWeightTree").floatValue();
		
		String outputFilePath = Paths.get(StaticResources.baseFolder,
				StaticResources.topologiesFolderName,
				jsonObject.getString("name") + ".json").toString();
		
		TopologyCreator tc = new TopologyCreator(interconnectedLevels,
				treeLevels,
				degreeOfInterconnection,
				randomSeed,
				defaultWeightInterconnected,
				defaultWeightTree,
				null);
		tc.createTopology();

		exportGraph(tc, outputFilePath);
	}
	
	public static void exportGraph(TopologyCreator tc, String path) {
		Graph g = tc.getGraph();
		JSONObject root = new JSONObject();
		JSONArray nodes = new JSONArray();
		JSONArray links = new JSONArray();
		JSONObject info = new JSONObject();
		
		root.put("nodes", nodes);
		root.put("links", links);
		root.put("info", info);
		
		info.put("numLinks", g.getLinks().size());
		info.put("numNodes", g.getNodes().size());
		info.put("numHosts", tc.getLeafNodes().size());
		info.put("numSwitchs", g.getNodes().size() - tc.getLeafNodes().size());
		info.put("defaultWeightInterconnected", tc.getDefaultWeightInterconnected());
		info.put("defaultWeightTree", tc.getDefaultWeightTree());
		
		for(Node n : g.getNodes()) {
			JSONObject nodeObject = new JSONObject();
			nodeObject.put("id", n.getId());
			nodeObject.put("type", n.getType());
			
			nodes.put(nodeObject);
		}
		
		for(Link l : g.getLinks()) {
			JSONObject linkObject = new JSONObject();
			linkObject.put("source", l.getSourceNode().getId());
			linkObject.put("end", l.getEndNode().getId());
			linkObject.put("weight", l.getWeight());
			linkObject.put("linkType", l.getLinkType());
			
			links.put(linkObject);
		}
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(path));
			root.write(writer, 4, 0);
			writer.close();
			System.out.println("Graph exported");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
