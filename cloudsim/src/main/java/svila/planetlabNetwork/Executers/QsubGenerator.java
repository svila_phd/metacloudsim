package svila.planetlabNetwork.Executers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import svila.planetlabNetwork.StaticResources;

public class QsubGenerator {
	public static void main(String[] args) {
		StaticResources.init();
		String testbedName = "testingBFDWastage";
		
		
		execute(testbedName);
	}
	
	public static void execute(String testbedName) {
		
		Path experimentsPath = Paths.get(StaticResources.baseFolder,
				StaticResources.generatedExperimentsFolderName,
				testbedName);
		Path outputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.qsubFolderName,
				testbedName);
		
		StaticResources.purgeDirectory(outputPath.toFile());

		ConsoleExecuter.generateTestbed(testbedName);
		
		System.out.println("Generating qsub files from: " + experimentsPath);
		System.out.println("To: " + outputPath);

		outputPath.toFile().mkdirs();
		String qsubClusterPath = "/home/svila/cloud/networkExperiments/qsubScripts/" + testbedName;
		String experimentClusterPath = "/home/svila/cloud/networkExperiments/generatedExperiments/" + testbedName;
		
		List<String> qsubFilePaths = new ArrayList<String>();
		
		int i = 0;
		int lastFile = experimentsPath.toFile().listFiles().length - 1;
		for(File file : experimentsPath.toFile().listFiles()) {
			System.out.println("Generating qsub file from file: " + file);
			String filename = file.getName().substring(0, file.getName().indexOf('.'));
			Path outputQsubFilePath = Paths.get(outputPath.toString(), filename + ".sh");
			String experimentName = file.getName();
			qsubFilePaths.add("/home/svila/cloud/networkExperiments/qsubScripts/" + testbedName + "/" + filename + ".sh");
			boolean sendEmail = i == lastFile;
			generateQsubFile(testbedName, experimentName, qsubClusterPath, experimentClusterPath, outputQsubFilePath, sendEmail);
			i++;
		}
		
		Path qsubExecuterFilePath = Paths.get(outputPath.toString(), testbedName + "_qsub_executer" + ".sh");
		
		System.out.println("Generating qsub executer: " + qsubExecuterFilePath);
		generateQsubExecuterFile(qsubExecuterFilePath, qsubFilePaths);
	}
	
	private static void generateQsubFile(String testbedName, String experimentName, String qsubClusterPath, String experimentClusterPath, Path outputFilePath, boolean sendEmail) {
		try {			
			System.out.println("Generating file: " + outputFilePath);
			BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath.toString()));
			writer.write("#!/bin/bash\n");
			writer.write("#$ -S /bin/bash\n");
			writer.write("#$ -N " + experimentName + "\n");
			writer.write("#$ -V\n");
			if(sendEmail) {
				writer.write("#$ -M svila@diei.udl.cat\n");
				writer.write("#$ -m ea\n");
			}
			writer.write("#$ -o /home/svila/cloud/qsubOutput\n");
			writer.write("#$ -e /home/svila/cloud/qsubError\n");
			writer.write("#$ -wd /home/svila/cloud/\n");
			writer.write("#$ -pe smp 2\n");
			
			writer.write("java -jar /home/svila/cloud/netcloud.jar " + "fullpath " + experimentClusterPath + "/" + experimentName + "\n");

			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private static void generateQsubExecuterFile(Path filePath, List<String> qsubFilePaths) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(filePath.toString()));
			for(String qsubFilePath : qsubFilePaths) {
				writer.write("qsub " + qsubFilePath.toString() + "\n");
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
