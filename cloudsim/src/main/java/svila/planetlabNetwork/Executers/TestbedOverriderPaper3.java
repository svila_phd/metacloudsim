package svila.planetlabNetwork.Executers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import svila.planetlabNetwork.StaticResources;

public class TestbedOverriderPaper3 {
	public static void main(String[] args) {
		StaticResources.init();

		String testbed = "paper3_def_test_vm_window_alpha";

		Path inputPath = Paths.get(StaticResources.baseFolder, StaticResources.testbedFolderName, testbed + ".json");

		Path outputPath = Paths.get(StaticResources.baseFolder, StaticResources.testbedFolderName,
				testbed + "_o" + ".json");

		JSONObject testbedConfiguration = null;
		try {
			testbedConfiguration = new JSONObject(new String(Files.readAllBytes(inputPath), "UTF-8"));
			testbedConfiguration.put("name",
					new JSONArray().put(testbedConfiguration.getJSONArray("name").get(0) + "_o"));
			testbedConfiguration.put("experimentOutputFolder",
					new JSONArray().put(testbedConfiguration.getJSONArray("experimentOutputFolder").get(0) + "_o"));
		} catch (JSONException | IOException e) {
			e.printStackTrace();
		}

		JSONArray overrideArray = TestbedOverriderPaper3.getOverride_def_multiple_general();

		testbedConfiguration.put("override", overrideArray);

		try {
			File currentOutputFile = new File(outputPath.toString());
			BufferedWriter writer = new BufferedWriter(new FileWriter(currentOutputFile));
			testbedConfiguration.write(writer, 4, 0);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String content;
		try {
			content = new String(Files.readAllBytes(outputPath));
			content = content.replaceAll("\"-_-", "");
			content = content.replaceAll("-_-\"", "");
			Files.write(outputPath, content.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static JSONArray getOverride() {
		JSONArray overrideArray = new JSONArray();
		for (int window = 3; window <= 15; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(2)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostForecastingTechnique", new JSONArray().put("neuralProphet"));
				jsonObject.put("vmForecastingTechniqueAccurate", new JSONArray().put("neuralProphet"));
				JSONArray bollingerConf = new JSONArray();
				bollingerConf.put("bollinger");
				bollingerConf.put(window);
				bollingerConf.put("-_-" + alpha.toString() + "-_-");
				jsonObject.put("hostSignalProcessing", bollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", bollingerConf);
				jsonObject.put("vmSignalProcessingBasic", bollingerConf);
				overrideArray.put(jsonObject);
			}
		}
		return overrideArray;
	}

	public static JSONArray getOverrideOnlyBollinger() {
		JSONArray overrideArray = new JSONArray();
		for (int window = 3; window <= 15; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(2)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONObject jsonObject = new JSONObject();
				JSONArray bollingerConf = new JSONArray();
				bollingerConf.put("bollinger");
				bollingerConf.put(window);
				bollingerConf.put("-_-" + alpha.toString() + "-_-");
				jsonObject.put("hostSignalProcessing", bollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", bollingerConf);
				jsonObject.put("vmSignalProcessingBasic", bollingerConf);
				overrideArray.put(jsonObject);
			}
		}
		return overrideArray;
	}

	public static JSONArray getOverrideAllPlanetlab() {
		JSONArray overrideArray = new JSONArray();

		String[] planetlabTraces = new String[] { "planetlab_20110303_100_mostDiff", "planetlab_20110306_117_mostDiff",
				"planetlab_20110309_123_mostDiff", "planetlab_20110322_122_mostDiff", "planetlab_20110325_105_mostDiff",
				"planetlab_20110403_90_mostDiff", "planetlab_20110409_110_mostDiff", "planetlab_20110411_106_mostDiff",
				"planetlab_20110412_110_mostDiff", "planetlab_20110420_140_mostDiff" };
		Integer[] numVmsList = new Integer[] { 100, 117, 123, 122, 105, 90, 110, 106, 110, 140 };
		JSONArray bollingerConf = new JSONArray();
		bollingerConf.put("bollinger");
		bollingerConf.put(6);
		bollingerConf.put("-_-" + "0.5" + "-_-");

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Reference for other workloads
		// i=1, avoiding first workload
		for (int i = 1; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);
			overrideArray.put(jsonObject);
		}

		// Bollinger + neuralProphet
		for (int i = 0; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);

			jsonObject.put("hostSignalProcessing", bollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", bollingerConf);
			jsonObject.put("vmSignalProcessingBasic", bollingerConf);

			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			overrideArray.put(jsonObject);
		}

		// Only bollinger
		for (int i = 0; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);

			jsonObject.put("hostSignalProcessing", bollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", bollingerConf);
			jsonObject.put("vmSignalProcessingBasic", bollingerConf);
			overrideArray.put(jsonObject);
		}

		// Only neuralProphet
		for (int i = 0; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);

			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			overrideArray.put(jsonObject);
		}

		return overrideArray;
	}

	public static JSONArray getOverrideAllPlanetlabBestParams() {
		JSONArray overrideArray = new JSONArray();

		String[] planetlabTraces = new String[] { "planetlab_20110303_100_mostDiff", "planetlab_20110306_117_mostDiff",
				"planetlab_20110309_123_mostDiff", "planetlab_20110322_122_mostDiff", "planetlab_20110325_105_mostDiff",
				"planetlab_20110403_90_mostDiff", "planetlab_20110409_110_mostDiff", "planetlab_20110411_106_mostDiff",
				"planetlab_20110412_110_mostDiff", "planetlab_20110420_140_mostDiff" };
		Integer[] numVmsList = new Integer[] { 100, 117, 123, 122, 105, 90, 110, 106, 110, 140 };
		JSONArray bollingerConf = new JSONArray();
		bollingerConf.put("bollinger");
		bollingerConf.put(6);
		bollingerConf.put("-_-" + "0.5" + "-_-");

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Reference for other workloads
		// i=1, avoiding first workload

		for (int i = 1; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);
			overrideArray.put(jsonObject);
		}

		// Bollinger + neuralProphet
		/*
		 * for(int i=0; i<planetlabTraces.length; i++) { for(int window=3; window<=15;
		 * window++) { for(BigDecimal alpha = BigDecimal.valueOf(0.1);
		 * alpha.compareTo(BigDecimal.valueOf(2)) < 0; alpha =
		 * alpha.add(BigDecimal.valueOf(0.1))) { JSONArray currentBollingerConf = new
		 * JSONArray(); currentBollingerConf.put("bollinger");
		 * currentBollingerConf.put(window); currentBollingerConf.put("-_-" +
		 * alpha.toString() + "-_-"); String planetlabTrace = planetlabTraces[i];
		 * Integer numVms = numVmsList[i]; JSONObject jsonObject = new JSONObject();
		 * jsonObject.put("workloadTrace", planetlabTrace);
		 * jsonObject.put("numCloudlets", numVms);
		 * 
		 * jsonObject.put("hostSignalProcessing", currentBollingerConf);
		 * jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
		 * jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
		 * 
		 * jsonObject.put("hostForecastingTechnique", neuralProphetConf);
		 * jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		 * overrideArray.put(jsonObject); } } }
		 */

		// Only bollinger

		for (int i = 0; i < planetlabTraces.length; i++) {
			for (int window = 3; window <= 15; window++) {
				for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha
						.compareTo(BigDecimal.valueOf(2)) < 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONArray currentBollingerConf = new JSONArray();
					currentBollingerConf.put("bollinger");
					currentBollingerConf.put(window);
					currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
					String planetlabTrace = planetlabTraces[i];
					Integer numVms = numVmsList[i];
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("workloadTrace", planetlabTrace);
					jsonObject.put("numCloudlets", numVms);

					jsonObject.put("hostSignalProcessing", currentBollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
					overrideArray.put(jsonObject);
				}
			}
		}

		// Only neuralProphet
		for (int i = 0; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);

			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			overrideArray.put(jsonObject);
		}

		return overrideArray;
	}

	public static JSONArray getOverrideBB_NN() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Reference for other workloads
		// i=1, avoiding first workload

		// Bollinger + neuralProphet
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet, nn only vm selection
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				// jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet, nn only host saturation
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				// jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only bollinger
		for (int window = 3; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only neuralProphet
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("hostForecastingTechnique", neuralProphetConf);
		jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObject);

		return overrideArray;
	}

	public static JSONArray getOverride_Fix() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Reference for other workloads
		// i=1, avoiding first workload

		// Bollinger + neuralProphet
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet, nn only vm selection
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				// jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet, nn only host saturation
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				// jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only bollinger
		for (int window = 3; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only interpeaks
		for (int window = 3; window <= 6; window++) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("interpeaks");
			currentBollingerConf.put(window);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			overrideArray.put(jsonObject);
		}

		// Interpeaks + neuralProphet
		for (int window = 3; window <= 6; window++) {
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("interpeaks");
			currentBollingerConf.put(window);
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			overrideArray.put(jsonObject);
		}

		// Only neuralProphet
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("hostForecastingTechnique", neuralProphetConf);
		jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObject);

		return overrideArray;
	}

	public static JSONArray getOverride_Fix3() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Reference for other workloads
		// i=1, avoiding first workload

		// Bollinger + neuralProphet
		for (int window = 6; window <= 6; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.5);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

			jsonObject.put("hostForecastingTechnique", neuralProphetConf);
			jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			overrideArray.put(jsonObject);
		}

		// Only bollinger
		for (int window = 6; window <= 6; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.5);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			overrideArray.put(jsonObject);
		}

		// Only neuralProphet
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("hostForecastingTechnique", neuralProphetConf);
		jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObject);

		return overrideArray;
	}

	public static JSONArray getOverride_interpeaks() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Only bollinger
		for (int window = 6; window <= 6; window++) {
			BigDecimal alpha = BigDecimal.valueOf(0.5);
			JSONArray currentBollingerConf = new JSONArray();
			currentBollingerConf.put("bollinger");
			currentBollingerConf.put(window);
			currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
			JSONObject jsonObject = new JSONObject();

			jsonObject.put("hostSignalProcessing", currentBollingerConf);
			jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
			jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
			overrideArray.put(jsonObject);
		}

		// Bollinger + neuralProphet
		for (int window = 4; window <= 6; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(0.6)) < 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only interpeaks
		for (int window = 3; window <= 6; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(1.5); zScore
					.compareTo(BigDecimal.valueOf(2.5)) < 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);
				overrideArray.put(jsonObject);
			}
		}

		// Interpeaks + neuralProphet
		for (int window = 3; window <= 6; window++) {
			for (BigDecimal zScore = BigDecimal.valueOf(1.5); zScore
					.compareTo(BigDecimal.valueOf(2.5)) < 0; zScore = zScore.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentInterpeaksConf = new JSONArray();
				currentInterpeaksConf.put("interpeaks");
				currentInterpeaksConf.put(window);
				currentInterpeaksConf.put(zScore);
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingAccurate", currentInterpeaksConf);
				jsonObject.put("vmSignalProcessingBasic", currentInterpeaksConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only neuralProphet
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("hostForecastingTechnique", neuralProphetConf);
		jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObject);

		System.out.println("Override: " + overrideArray.length());
		return overrideArray;
	}

	public static JSONArray getOverride_final() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Bollinger + neuralProphet
		for (int window = 3; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(2)) <= 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet only host
		for (int window = 3; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(2)) <= 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet only vm
		for (int window = 3; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha.compareTo(BigDecimal.valueOf(2)) <= 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only neuralProphet
		JSONObject jsonObjectOnlyNP = new JSONObject();
		jsonObjectOnlyNP.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNP.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNP);

		// Only neuralProphet host forecasting
		JSONObject jsonObjectOnlyNPHostFore = new JSONObject();
		jsonObjectOnlyNPHostFore.put("hostForecastingTechnique", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPHostFore);

		// Only neuralProphet vm forecasting
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		return overrideArray;
	}

	public static JSONArray getOverride_final_neg() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Bollinger + neuralProphet
		for (int window = 3; window <= 20; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-2.0); alpha.compareTo(BigDecimal.valueOf(2)) <= 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet
		for (int window = 3; window <= 6; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-2.0); alpha.compareTo(BigDecimal.valueOf(2)) <= 0; alpha = alpha
					.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				JSONArray hostForecastingResume = new JSONArray();
				JSONArray vmForecastingResumeAccurate = new JSONArray();
				hostForecastingResume.put("mean");
				vmForecastingResumeAccurate.put("mean");

				jsonObject.put("hostForecastingResume", "mean");
				jsonObject.put("vmForecastingResumeAccurate", "mean");

				overrideArray.put(jsonObject);
			}
		}

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmForeMean = new JSONObject();
		jsonObjectOnlyNPVmForeMean.put("vmForecastingTechniqueAccurate", neuralProphetConf);

		JSONArray hostForecastingResume = new JSONArray();
		JSONArray vmForecastingResumeAccurate = new JSONArray();
		hostForecastingResume.put("mean");
		vmForecastingResumeAccurate.put("mean");

		jsonObjectOnlyNPVmForeMean.put("hostForecastingResume", "mean");
		jsonObjectOnlyNPVmForeMean.put("vmForecastingResumeAccurate", "mean");

		overrideArray.put(jsonObjectOnlyNPVmForeMean);

		return overrideArray;
	}

	public static JSONArray getOverride_final_multiple04() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Bollinger + neuralProphet
		for (int window = 5; window <= 5; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.4); alpha
					.compareTo(BigDecimal.valueOf(0.4)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.4))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet
		for (int window = 5; window <= 5; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.4); alpha
					.compareTo(BigDecimal.valueOf(0.4)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.4))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				jsonObject.put("hostForecastingResume", "mean");
				jsonObject.put("vmForecastingResumeAccurate", "mean");

				overrideArray.put(jsonObject);
			}
		}

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmForeMean = new JSONObject();
		jsonObjectOnlyNPVmForeMean.put("vmForecastingTechniqueAccurate", neuralProphetConf);

		JSONArray hostForecastingResume = new JSONArray();
		JSONArray vmForecastingResumeAccurate = new JSONArray();
		hostForecastingResume.put("mean");
		vmForecastingResumeAccurate.put("mean");

		jsonObjectOnlyNPVmForeMean.put("hostForecastingResume", "mean");
		jsonObjectOnlyNPVmForeMean.put("vmForecastingResumeAccurate", "mean");

		overrideArray.put(jsonObjectOnlyNPVmForeMean);

		return overrideArray;
	}

	public static JSONArray getOverride_final_decision() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Only bollinger
		for (int window = 3; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-2.0); alpha
					.compareTo(BigDecimal.valueOf(2.0)) < 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet
		for (int window = 3; window <= 20; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-2.0); alpha
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet mean
		for (int window = 3; window <= 20; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-2.0); alpha
					.compareTo(BigDecimal.valueOf(2.0)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				jsonObject.put("hostForecastingResume", "mean");
				jsonObject.put("vmForecastingResumeAccurate", "mean");

				overrideArray.put(jsonObject);
			}
		}

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmForeMean = new JSONObject();
		jsonObjectOnlyNPVmForeMean.put("vmForecastingTechniqueAccurate", neuralProphetConf);

		JSONArray hostForecastingResume = new JSONArray();
		JSONArray vmForecastingResumeAccurate = new JSONArray();
		hostForecastingResume.put("mean");
		vmForecastingResumeAccurate.put("mean");

		jsonObjectOnlyNPVmForeMean.put("hostForecastingResume", "mean");
		jsonObjectOnlyNPVmForeMean.put("vmForecastingResumeAccurate", "mean");

		overrideArray.put(jsonObjectOnlyNPVmForeMean);

		return overrideArray;
	}

	public static JSONArray getOverride_no_network() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Only bollinger
		for (int window = 5; window <= 7; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-5.0); alpha
					.compareTo(BigDecimal.valueOf(0.5)) < 0; alpha = alpha.add(BigDecimal.valueOf(0.5))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet
		for (int window = 5; window <= 7; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.50); alpha
					.compareTo(BigDecimal.valueOf(0.5)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.5))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				overrideArray.put(jsonObject);
			}
		}

		// Bollinger + neuralProphet mean
		for (int window = 5; window <= 7; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.5); alpha
					.compareTo(BigDecimal.valueOf(0.5)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.5))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				jsonObject.put("hostForecastingResume", "mean");
				jsonObject.put("vmForecastingResumeAccurate", "mean");

				overrideArray.put(jsonObject);
			}
		}

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmForeMean = new JSONObject();
		jsonObjectOnlyNPVmForeMean.put("vmForecastingTechniqueAccurate", neuralProphetConf);

		JSONArray hostForecastingResume = new JSONArray();
		JSONArray vmForecastingResumeAccurate = new JSONArray();
		hostForecastingResume.put("mean");
		vmForecastingResumeAccurate.put("mean");

		jsonObjectOnlyNPVmForeMean.put("hostForecastingResume", "mean");
		jsonObjectOnlyNPVmForeMean.put("vmForecastingResumeAccurate", "mean");

		overrideArray.put(jsonObjectOnlyNPVmForeMean);

		return overrideArray;

	}
	
	public static JSONArray getOverride_final_energy_mig() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Bollinger + neuralProphet
		for (int window = 5; window <= 5; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.4); alpha
					.compareTo(BigDecimal.valueOf(0.4)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.4))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				overrideArray.put(jsonObject);
			}
		}
		
		// Bollinger + neuralProphet
				for (int window = 5; window <= 5; window = window + 1) {
					for (BigDecimal alpha = BigDecimal.valueOf(-0.5); alpha
							.compareTo(BigDecimal.valueOf(0.5)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.5))) {
						JSONArray currentBollingerConf = new JSONArray();
						currentBollingerConf.put("bollinger");
						currentBollingerConf.put(window);
						currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
						JSONObject jsonObject = new JSONObject();

						jsonObject.put("hostSignalProcessing", currentBollingerConf);
						jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
						jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

						jsonObject.put("hostForecastingTechnique", neuralProphetConf);
						jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

						jsonObject.put("hostForecastingResume", "mean");
						jsonObject.put("vmForecastingResumeAccurate", "mean");

						overrideArray.put(jsonObject);
					}
				}

		// Bollinger + neuralProphet
		for (int window = 5; window <= 5; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.4); alpha
					.compareTo(BigDecimal.valueOf(0.4)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.4))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				jsonObject.put("hostForecastingResume", "mean");
				jsonObject.put("vmForecastingResumeAccurate", "mean");

				overrideArray.put(jsonObject);
			}
		}
		
		// Bollinger + neuralProphet
				for (int window = 5; window <= 5; window = window + 1) {
					for (BigDecimal alpha = BigDecimal.valueOf(-0.5); alpha
							.compareTo(BigDecimal.valueOf(0.5)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.5))) {
						JSONArray currentBollingerConf = new JSONArray();
						currentBollingerConf.put("bollinger");
						currentBollingerConf.put(window);
						currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
						JSONObject jsonObject = new JSONObject();

						jsonObject.put("hostSignalProcessing", currentBollingerConf);
						jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
						jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

						jsonObject.put("hostForecastingTechnique", neuralProphetConf);
						jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

						jsonObject.put("hostForecastingResume", "mean");
						jsonObject.put("vmForecastingResumeAccurate", "mean");

						overrideArray.put(jsonObject);
					}
				}
				
		// Only bollinger
		for (int window = 5; window <= 7; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.5); alpha
					.compareTo(BigDecimal.valueOf(1.0)) < 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmForeMean = new JSONObject();
		jsonObjectOnlyNPVmForeMean.put("vmForecastingTechniqueAccurate", neuralProphetConf);

		JSONArray hostForecastingResume = new JSONArray();
		JSONArray vmForecastingResumeAccurate = new JSONArray();
		hostForecastingResume.put("mean");
		vmForecastingResumeAccurate.put("mean");

		jsonObjectOnlyNPVmForeMean.put("hostForecastingResume", "mean");
		jsonObjectOnlyNPVmForeMean.put("vmForecastingResumeAccurate", "mean");

		overrideArray.put(jsonObjectOnlyNPVmForeMean);

		return overrideArray;
	}
	
	public static JSONArray getOverride_final_e_m() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Bollinger + neuralProphet
		for (int window = 3; window <= 20; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-2); alpha
					.compareTo(BigDecimal.valueOf(2)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				overrideArray.put(jsonObject);
			}
		}
		
		// Bollinger + neuralProphet
				for (int window = 3; window <= 20; window = window + 1) {
					for (BigDecimal alpha = BigDecimal.valueOf(-2); alpha
							.compareTo(BigDecimal.valueOf(2)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
						JSONArray currentBollingerConf = new JSONArray();
						currentBollingerConf.put("bollinger");
						currentBollingerConf.put(window);
						currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
						JSONObject jsonObject = new JSONObject();

						jsonObject.put("hostSignalProcessing", currentBollingerConf);
						jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
						jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

						jsonObject.put("hostForecastingTechnique", neuralProphetConf);
						jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

						jsonObject.put("hostForecastingResume", "mean");
						jsonObject.put("vmForecastingResumeAccurate", "mean");

						overrideArray.put(jsonObject);
					}
				}
				
		// Only bollinger
		for (int window = 3; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-2.0); alpha
					.compareTo(BigDecimal.valueOf(2.0)) < 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				overrideArray.put(jsonObject);
			}
		}

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmForeMean = new JSONObject();
		jsonObjectOnlyNPVmForeMean.put("vmForecastingTechniqueAccurate", neuralProphetConf);

		JSONArray hostForecastingResume = new JSONArray();
		JSONArray vmForecastingResumeAccurate = new JSONArray();
		hostForecastingResume.put("mean");
		vmForecastingResumeAccurate.put("mean");

		jsonObjectOnlyNPVmForeMean.put("hostForecastingResume", "mean");
		jsonObjectOnlyNPVmForeMean.put("vmForecastingResumeAccurate", "mean");

		overrideArray.put(jsonObjectOnlyNPVmForeMean);

		return overrideArray;
	}
	
	public static JSONArray getOverride_final_e_m_only_neural() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmForeMean = new JSONObject();
		jsonObjectOnlyNPVmForeMean.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmForeMean.put("vmForecastingTechniqueAccurate", neuralProphetConf);

		JSONArray hostForecastingResume = new JSONArray();
		JSONArray vmForecastingResumeAccurate = new JSONArray();
		hostForecastingResume.put("mean");
		vmForecastingResumeAccurate.put("mean");

		jsonObjectOnlyNPVmForeMean.put("hostForecastingResume", "mean");
		jsonObjectOnlyNPVmForeMean.put("vmForecastingResumeAccurate", "mean");

		overrideArray.put(jsonObjectOnlyNPVmForeMean);

		return overrideArray;
	}
	
	public static JSONArray getOverride_final_decision_e_m_multiple_host() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Bollinger + neuralProphet
				for (int window = 5; window <= 5; window = window + 1) {
					for (BigDecimal alpha = BigDecimal.valueOf(0.6); alpha
							.compareTo(BigDecimal.valueOf(0.6)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
						JSONArray currentBollingerConf = new JSONArray();
						currentBollingerConf.put("bollinger");
						currentBollingerConf.put(window);
						currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
						JSONObject jsonObject = new JSONObject();

						jsonObject.put("hostSignalProcessing", currentBollingerConf);
						jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
						jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

						jsonObject.put("hostForecastingTechnique", neuralProphetConf);
						jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

						overrideArray.put(jsonObject);
					}
				}
				
				// Bollinger 
				for (int window = 5; window <= 5; window = window + 1) {
					for (BigDecimal alpha = BigDecimal.valueOf(0.6); alpha
							.compareTo(BigDecimal.valueOf(0.6)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
						JSONArray currentBollingerConf = new JSONArray();
						currentBollingerConf.put("bollinger");
						currentBollingerConf.put(window);
						currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
						JSONObject jsonObject = new JSONObject();

						jsonObject.put("hostSignalProcessing", currentBollingerConf);
						jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
						jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

						//jsonObject.put("hostForecastingTechnique", neuralProphetConf);
						//jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

						overrideArray.put(jsonObject);
					}
				}
				
				// Bollinger + neuralProphet
				for (int window = 19; window <= 19; window = window + 1) {
					for (BigDecimal alpha = BigDecimal.valueOf(-0.1); alpha
							.compareTo(BigDecimal.valueOf(-0.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
						JSONArray currentBollingerConf = new JSONArray();
						currentBollingerConf.put("bollinger");
						currentBollingerConf.put(window);
						currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
						JSONObject jsonObject = new JSONObject();

						jsonObject.put("hostSignalProcessing", currentBollingerConf);
						jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
						jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

						jsonObject.put("hostForecastingTechnique", neuralProphetConf);
						jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

						overrideArray.put(jsonObject);
					}
				}
				
				// Bollinger 
				for (int window = 19; window <= 19; window = window + 1) {
					for (BigDecimal alpha = BigDecimal.valueOf(-0.1); alpha
							.compareTo(BigDecimal.valueOf(-0.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
						JSONArray currentBollingerConf = new JSONArray();
						currentBollingerConf.put("bollinger");
						currentBollingerConf.put(window);
						currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
						JSONObject jsonObject = new JSONObject();

						jsonObject.put("hostSignalProcessing", currentBollingerConf);
						jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
						jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

						//jsonObject.put("hostForecastingTechnique", neuralProphetConf);
						//jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

						overrideArray.put(jsonObject);
					}
				}
				

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		return overrideArray;
	}
	
	public static JSONArray getOverride_def_multiple_host_mean_params() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Bollinger + neuralProphet
		for (int window = 3; window <= 20; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.4); alpha
					.compareTo(BigDecimal.valueOf(2)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				overrideArray.put(jsonObject);
			}
		}
				
		// Only bollinger
		/*for (int window = 3; window <= 20; window++) {
			for (BigDecimal alpha = BigDecimal.valueOf(-2.0); alpha
					.compareTo(BigDecimal.valueOf(2.0)) < 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
				overrideArray.put(jsonObject);
			}
		}*/

		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);

		return overrideArray;
	}
	
	public static JSONArray getOverride_def_multiple_30() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Bollinger + neuralProphet
		for (int window = 5; window <= 6; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.5); alpha
					.compareTo(BigDecimal.valueOf(0.6)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				overrideArray.put(jsonObject);
			}
		}
				
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);

		return overrideArray;
	}

	
	public static JSONArray getOverride_def_500vms() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Bollinger + neuralProphet
		for (int window = 5; window <= 6; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(0.5); alpha
					.compareTo(BigDecimal.valueOf(0.5)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				overrideArray.put(jsonObject);
			}
		}
				
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		return overrideArray;
	}
	
	
	
	public static JSONArray getOverride_def_multiple_5() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Bollinger + neuralProphet
		for (int window = 3; window <= 25; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.5); alpha
					.compareTo(BigDecimal.valueOf(1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				overrideArray.put(jsonObject);
			}
		}
				
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);

		return overrideArray;
	}
	
	public static JSONArray getOverride_def_multiple_general() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Bollinger + neuralProphet
		for (int window = 3; window <= 25; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.6); alpha
					.compareTo(BigDecimal.valueOf(1.5)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConf);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				overrideArray.put(jsonObject);
			}
		}
				
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);

		return overrideArray;
	}
	
	public static JSONArray getOverride_planetlab_all() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		String[] planetlabTraces = new String[] { "planetlab_20110303_100_mostDiff", "planetlab_20110306_117_mostDiff",
				"planetlab_20110309_123_mostDiff", "planetlab_20110322_122_mostDiff", "planetlab_20110325_105_mostDiff",
				"planetlab_20110403_90_mostDiff", "planetlab_20110409_110_mostDiff", "planetlab_20110411_106_mostDiff",
				"planetlab_20110412_110_mostDiff", "planetlab_20110420_140_mostDiff" };
		Integer[] numVmsList = new Integer[] { 100, 117, 123, 122, 105, 90, 110, 106, 110, 140 };
		
		
		for (int i = 1; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];
		
		
			// Bollinger + neuralProphet
			for (int window = 12; window <= 12; window = window + 1) {
				for (BigDecimal alpha = BigDecimal.valueOf(0.1); alpha
						.compareTo(BigDecimal.valueOf(0.1)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("workloadTrace", planetlabTrace);
					jsonObject.put("numCloudlets", numVms);
					
					JSONArray currentBollingerConf = new JSONArray();
					currentBollingerConf.put("bollinger");
					currentBollingerConf.put(window);
					currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
	
					jsonObject.put("hostSignalProcessing", currentBollingerConf);
					jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
					jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);
	
					jsonObject.put("hostForecastingTechnique", neuralProphetConf);
					jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);
	
					overrideArray.put(jsonObject);
				}
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("workloadTrace", planetlabTrace);
			jsonObject.put("numCloudlets", numVms);
					
			// Only neuralProphet
			JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
			jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
			jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
			overrideArray.put(jsonObjectOnlyNPVmFore);
	
			overrideArray.put(jsonObjectOnlyNPVmFore);
		}

		System.out.println(overrideArray.length());
		return overrideArray;
	}
	
	public static JSONArray getOverride_planetlab_all_bis2() {
		JSONArray overrideArray = new JSONArray();

		String[] planetlabTraces = new String[] { "planetlab_20110303_100_mostDiff", "planetlab_20110306_117_mostDiff",
				"planetlab_20110309_123_mostDiff", "planetlab_20110322_122_mostDiff", "planetlab_20110325_105_mostDiff",
				"planetlab_20110403_90_mostDiff", "planetlab_20110409_110_mostDiff", "planetlab_20110411_106_mostDiff",
				"planetlab_20110412_110_mostDiff", "planetlab_20110420_140_mostDiff" };
		Integer[] numVmsList = new Integer[] { 100, 117, 123, 122, 105, 90, 110, 106, 110, 140 };
		
		
		for (int i = 0; i < planetlabTraces.length; i++) {
			String planetlabTrace = planetlabTraces[i];
			Integer numVms = numVmsList[i];

			for(int randomSeed=0; randomSeed<30; randomSeed++) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("workloadTrace", planetlabTrace);
				jsonObject.put("numCloudlets", numVms);
				jsonObject.put("randomSeed", randomSeed);
				
				// ref
				//overrideArray.put(jsonObject);
				
				
				// Only Neural Prophet
				JSONObject jsonObjectNP = new JSONObject();
				jsonObjectNP.put("workloadTrace", planetlabTrace);
				jsonObjectNP.put("numCloudlets", numVms);
				jsonObjectNP.put("randomSeed", randomSeed);
				
				JSONArray neuralProphetConf = new JSONArray();
				neuralProphetConf.put("neuralProphet");
				
				jsonObjectNP.put("hostForecastingTechnique", neuralProphetConf);
				jsonObjectNP.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				//overrideArray.put(jsonObjectNP);
				
				// Neural Prophet + Bollinger Bands
				JSONObject jsonObjectNPBB = new JSONObject();
				jsonObjectNPBB.put("workloadTrace", planetlabTrace);
				jsonObjectNPBB.put("numCloudlets", numVms);
				jsonObjectNPBB.put("randomSeed", randomSeed);
				jsonObjectNPBB.put("hostForecastingTechnique", neuralProphetConf);
				jsonObjectNPBB.put("vmForecastingTechniqueAccurate", neuralProphetConf);
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(12);
				currentBollingerConf.put("-_-" + "0.1" + "-_-");
				jsonObjectNPBB.put("hostSignalProcessing", currentBollingerConf);
				jsonObjectNPBB.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObjectNPBB.put("vmSignalProcessingBasic", currentBollingerConf);
				overrideArray.put(jsonObjectNPBB);
			}
		}

		System.out.println(overrideArray.length());
		return overrideArray;
	}
	
	public static JSONArray getOverride_def_test_vm_window_alpha() {
		JSONArray overrideArray = new JSONArray();

		JSONArray neuralProphetConf = new JSONArray();
		neuralProphetConf.put("neuralProphet");
		
		// Bollinger + neuralProphet
		for (int window = 3; window <= 25; window = window + 1) {
			for (BigDecimal alpha = BigDecimal.valueOf(-0.6); alpha
					.compareTo(BigDecimal.valueOf(1.5)) <= 0; alpha = alpha.add(BigDecimal.valueOf(0.1))) {
				JSONArray currentBollingerConf = new JSONArray();
				currentBollingerConf.put("bollinger");
				currentBollingerConf.put(window);
				currentBollingerConf.put("-_-" + alpha.toString() + "-_-");
				
				JSONArray currentBollingerConfHost = new JSONArray();
				currentBollingerConfHost.put("bollinger");
				currentBollingerConfHost.put(12);
				currentBollingerConfHost.put("-_-" + "0.1" + "-_-");
				
				JSONObject jsonObject = new JSONObject();

				jsonObject.put("hostSignalProcessing", currentBollingerConfHost);
				jsonObject.put("vmSignalProcessingAccurate", currentBollingerConf);
				jsonObject.put("vmSignalProcessingBasic", currentBollingerConf);

				jsonObject.put("hostForecastingTechnique", neuralProphetConf);
				jsonObject.put("vmForecastingTechniqueAccurate", neuralProphetConf);

				overrideArray.put(jsonObject);
			}
		}
				
		// Only neuralProphet
		JSONObject jsonObjectOnlyNPVmFore = new JSONObject();
		jsonObjectOnlyNPVmFore.put("hostForecastingTechnique", neuralProphetConf);
		jsonObjectOnlyNPVmFore.put("vmForecastingTechniqueAccurate", neuralProphetConf);
		overrideArray.put(jsonObjectOnlyNPVmFore);

		overrideArray.put(jsonObjectOnlyNPVmFore);

		return overrideArray;
	}
}
