package svila.planetlabNetwork.Executers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;

import org.cloudbus.cloudsim.VmSchedulerTimeShared;

import com.google.common.io.Files;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.GlobalRunner;
import svila.planetlabNetwork.StaticResources;
import svila.planetlabNetwork.results.StatusFile;
import svila.python.PythonBridge;

public class ExperimentPoolExecuter {
	public static void main(String[] args) {

		String poolName = "pool_paper4_test3_planetlab_10traces_30_wpsp_arima_o";
		String[] foldersToExecute = {"pending"}; // {"cancelled", "pending", "error"};
		
		execute(poolName, foldersToExecute);
	}
	
	public static void execute(String[] args) {
		String poolName = args[1];
		String[] foldersToExecute = Arrays.copyOfRange(args, 2, args.length);
		
		execute(poolName, foldersToExecute);

	}
	
	public static void execute(String poolName, String[] foldersToExecute) {
		StaticResources.init();
		
		PythonBridge.init();
		
		System.out.println("Pool: " + poolName);
		System.out.print("Folder types:");
		for(String folder : foldersToExecute) {
			System.out.print(" " + folder);
		}
		System.out.println();
	
		StatusFile statusFile = new StatusFile();
		
		statusFile.generate(poolName);
		for(String folderType : foldersToExecute) {
			Path inputPath = Paths.get(StaticResources.baseFolder,
					StaticResources.poolFolderName,
					poolName, folderType);
			File[] currentFiles = inputPath.toFile().listFiles();
			while(currentFiles.length > 0) {
				File file = currentFiles[0];
				if(file.getName().endsWith(".json")) {
					VmSchedulerTimeShared.clearMips();
					file = moveFileToRunningFolder(file, poolName);
					StaticResources.loadExperiment(
							Paths.get(StaticResources.poolFolderName, poolName, "running").toString(),
							file.getName().replaceFirst("[.][^.]+$", ""));
					
					ExperimentConfiguration ec = StaticResources.getCE();
					statusFile.setCurrentExperiment(ec);

					System.out.println(ec);
					try {
						runExperiment(ec);
					} catch (Exception e) {
						moveFileToErrorFolder(file, poolName, e);
						statusFile.sendError(e);
					} finally {
						if(!statusFile.isError()) {
							moveFileToCompletedFolder(file, poolName);
							statusFile.sendOk();
						}
					}
					
					System.gc();
					Path poolPath = Paths.get(StaticResources.baseFolder,
							StaticResources.poolFolderName,
							poolName);
					ExperimentPoolCompletionChecker.showFolderStats(poolPath);
					ExperimentPoolCompletionChecker.showCompletion(poolPath);
				}
				
				currentFiles = inputPath.toFile().listFiles();
			}
		}
	}
	
	public static void moveFileToErrorFolder(File file, String poolName, Exception e) {
		try {
			Files.move(file, Paths.get(StaticResources.baseFolder, StaticResources.poolFolderName,
					poolName, "error", file.getName()).toFile());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public static void moveFileToCompletedFolder(File file, String poolName) {
		try {
			File newFile = Paths.get(StaticResources.baseFolder, StaticResources.poolFolderName,
					poolName, "completed", file.getName()).toFile();
			Files.move(file, newFile);
			newFile.setLastModified(new Date().getTime());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public static File moveFileToRunningFolder(File file, String poolName) {
		try {
			File newFile = Paths.get(StaticResources.baseFolder, StaticResources.poolFolderName,
					poolName, "running", file.getName()).toFile();
			Files.move(file, newFile);
			return newFile;
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(-1);
			return null;
		}
	}

	public static void runExperiment(ExperimentConfiguration ec) {
		new GlobalRunner(
                ec
        );
	}

}
