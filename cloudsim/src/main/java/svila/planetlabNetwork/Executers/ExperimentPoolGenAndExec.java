package svila.planetlabNetwork.Executers;

public class ExperimentPoolGenAndExec {
	public static void main(String[] args) {
		String poolName = "pool_paper4_test3_planetlab_10traces_30_wbf_o";
		String[] folders = {"paper4_test3_planetlab_10traces_30_wbf_o"};
		

		ExperimentPoolGenerator.generatePool(poolName, folders);
		String[] foldersToExecute = {"pending"}; // {"cancelled", "pending", "error"};
		ExperimentPoolExecuter.execute(poolName, foldersToExecute);
	}
}
