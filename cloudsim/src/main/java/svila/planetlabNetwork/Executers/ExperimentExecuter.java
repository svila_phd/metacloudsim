package svila.planetlabNetwork.Executers;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.GlobalRunner;
import svila.planetlabNetwork.StaticResources;
import svila.python.PythonBridge;

public class ExperimentExecuter {
	public static void main(String[] args) {
		StaticResources.init();
		PythonBridge.init();
		StaticResources.loadExperiment("Future_100_VMs_V2_00000");
		ExperimentConfiguration ec = StaticResources.getCE();		
		
		System.out.println(ec);
		runExperiment(ec);
	}
	
	public static void runExperiment(ExperimentConfiguration ec) {
		new GlobalRunner(
                ec
        );
	}
}
