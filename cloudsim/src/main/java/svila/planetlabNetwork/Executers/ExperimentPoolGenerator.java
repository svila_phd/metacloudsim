package svila.planetlabNetwork.Executers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.json.JSONObject;

import svila.planetlabNetwork.StaticResources;

public class ExperimentPoolGenerator {
	public static void main(String[] args) {
		String poolName = "pool_paper4_test3_azure_materna_alibaba_30_wbf_o"; //"pool_paper4_pre_data2_autoarima_o";
		String[] folders = {"paper4_test3_azure_30_wbf_o", "paper4_test3_materna_30_wbf_o", "paper4_test3_alibaba_30_wbf_o"};
							//"paper4_pre_data2_autoarima_o"};
							//"paper3_def_multiple_sel_tecs_o",};
							//"paper3_def_multiple_1_5_o"};
							//"paper3_def_multiple_sat_tecs_o"};
				/*,
				"paper3_final_decision_e_m_multiple_select_o",
				"paper3_final_decision_e_m_multiple_alloc_o",
				"paper3_final_decision_e_m_multiple_30"};*/
		
		generatePool(poolName, folders);
	}
	
	public static void generatePool(String[] args) {
		StaticResources.init();
		
		String poolName = args[1];
		String[] folders = Arrays.copyOfRange(args, 2, args.length);
		
		generatePool(poolName, folders);
	}
	
	public static void generatePool(String poolName, String[] folders) {
		StaticResources.init();
		
		Path outputFolderPath = Paths.get(StaticResources.baseFolder,
				StaticResources.poolFolderName,
				poolName);
		
		StaticResources.purgeDirectory(new File(outputFolderPath.toString()));
		
		createFolders(outputFolderPath);
		
		for(String testBedName : folders) {
			generateExperimentFiles(poolName, testBedName);
		}
		
		System.out.println("Pool generated at " + outputFolderPath);
	}
	
	public static void generateExperimentFiles(String poolName, String testbedName) {		
		Path inputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.testbedFolderName,
				testbedName + ".json");
		Path outputFolderPath = Paths.get(StaticResources.baseFolder,
				StaticResources.poolFolderName,
				poolName, "pending");

		JSONObject[] results = ExperimentConfigurationGenerator.getJSONCombinations(inputPath);
		System.out.println(results.length);
		ExperimentConfigurationGenerator.writeJSONFiles(results, outputFolderPath);
	}
	
	private static void createFolders(Path outputFolderPath) {
		String[] paths = {"pending", "cancelled", "running", "completed", "error"};
		for(String path : paths) {
			Path currentPath = Paths.get(outputFolderPath.toString(), path);
			try {
				Files.createDirectories(currentPath);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
