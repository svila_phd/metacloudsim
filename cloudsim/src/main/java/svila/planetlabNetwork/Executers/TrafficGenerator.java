package svila.planetlabNetwork.Executers;

import java.nio.file.Paths;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

import svila.planetlabNetwork.StaticResources;
import svila.planetlabNetwork.generators.DistributionGenerator;
import svila.planetlabNetwork.generators.RandomDistribution;

public class TrafficGenerator {
	public static void main(String[] args) {
		StaticResources.init();
		
		String interactionsSetName = "dispersed_30_70";
		int numberOfInteractions = 1000;
		int trafficLength = 288;
		int minValue = 30;
		int maxValue = 70;
		
		execute(interactionsSetName, numberOfInteractions, trafficLength, minValue, maxValue);
	}
	
	public static void execute(String interactionsSetName,
								int numberOfInteractions,
								int trafficLength,
								int minValue,
								int maxValue) {
		
		Path outputFolder = Paths.get(StaticResources.baseFolder,
				StaticResources.interactionsFolderName,
				interactionsSetName);
		
		DistributionGenerator distributionGenerator = new RandomDistribution(trafficLength, 0, minValue, maxValue);
		TrafficGenerator ig = new TrafficGenerator(outputFolder,
				interactionsSetName,
				numberOfInteractions,
				distributionGenerator);
		ig.generateFiles();
	}
	
	public static void executeFromJSONObject(JSONObject jsonObject) {
		String interactionsSetName = jsonObject.getString("name");
		int numberOfInteractions = jsonObject.getInt("numberOfInteractions");
		int trafficLength = jsonObject.getInt("trafficLength");
		int minValue = jsonObject.getInt("minValue");
		int maxValue = jsonObject.getInt("maxValue");
		
		execute(interactionsSetName, numberOfInteractions, trafficLength, minValue, maxValue);
	}
	
	Path outputFolder;
	String interactionsSetName;
	int numberOfInteractions;
	DistributionGenerator dg;
	
	public TrafficGenerator(Path outputFolder,
								String interactionsSetName,
								int numberOfInteractions,
								DistributionGenerator dg) {
		this.outputFolder = outputFolder;
		this.interactionsSetName = interactionsSetName;
		this.numberOfInteractions = numberOfInteractions;
		this.dg = dg;
	}
	
	public void generateFiles() {
		System.out.println("Generating files: " + outputFolder.toString());
		new File(outputFolder.toString()).mkdir();
		for(int i=0; i<this.numberOfInteractions; i++) {
			int[] values = dg.obtainValues();
			File outputFile = Paths.get(outputFolder.toString(),
					interactionsSetName+"_"+String.format("%05d", i)+".txt").toFile();
			System.out.println("Generating file: " + outputFile.toString());
			try {
				BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
				for(int j=0; j<values.length; j++) {
					writer.write(values[j] + "\n");
				}
			writer.close();
			System.out.println("Completed file: " + outputFile.toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
		System.out.println("Completed files: " + outputFolder.toString());
	}
}