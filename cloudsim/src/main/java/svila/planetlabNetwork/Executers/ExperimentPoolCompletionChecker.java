package svila.planetlabNetwork.Executers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

import org.apache.commons.io.comparator.LastModifiedFileComparator;

import svila.planetlabNetwork.StaticResources;

public class ExperimentPoolCompletionChecker {
	public static void main(String[] args) {
		
		String poolName = "test";
		
		check(poolName);
	}
	
	public static void check(String poolName) {
		StaticResources.init();
		
		Path outputFolderPath = Paths.get(StaticResources.baseFolder,
				StaticResources.poolFolderName,
				poolName);
		
		showFolderStats(outputFolderPath);
		showCompletion(outputFolderPath);
	}
	
	public static void showFolderStats(Path outputFolderPath) {
		String[] paths = {"pending", "cancelled", "running", "completed", "error"};
		
		for(String folderType : paths) {
			Path currentPath = Paths.get(outputFolderPath.toString(), folderType);
			System.out.println(folderType + ": " + currentPath.toFile().listFiles().length +  " files");
		}
		System.out.println();
	}
	
	public static void showCompletion(Path outputFolderPath) {
		Path completedPath = Paths.get(outputFolderPath.toString(), "completed");
		Path pendingPath = Paths.get(outputFolderPath.toString(), "pending");
		int numCompletedExperiments = completedPath.toFile().listFiles().length;
		
		if(numCompletedExperiments == 0) {
			return;
		}
		
		int numPendingExperiments = pendingPath.toFile().listFiles().length;
		int total = numCompletedExperiments + numPendingExperiments;
		float completion = ((float)numCompletedExperiments) / total * 100.0f;
		System.out.println("% completion: " + completion);
		completedPath.toFile().lastModified();
		
		File[] completedFiles = completedPath.toFile().listFiles();
		Arrays.sort(completedFiles, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		File lastFile = completedFiles[0];
		File firstFile = completedFiles[completedFiles.length-1];
		try {
			BasicFileAttributes lastAttr = Files.readAttributes(lastFile.toPath(), BasicFileAttributes.class);
			Instant lastTime = lastAttr.lastModifiedTime().toInstant();
			BasicFileAttributes firstAttr = Files.readAttributes(firstFile.toPath(), BasicFileAttributes.class);
			Instant firstTime = firstAttr.lastModifiedTime().toInstant();
			Duration poolTime = Duration.between(firstTime, lastTime);
			long currentTime = poolTime.getSeconds();
			long expectedTime = currentTime*total/numCompletedExperiments;
			long remainingTime = expectedTime - currentTime;
			System.out.println(String.format("%02d:%02d:%02d/%02d:%02d:%02d (remaining: %02d:%02d:%02d)", currentTime/3600, currentTime/60%60, currentTime%60,
					expectedTime/3600, expectedTime/60%60, expectedTime%60,
					remainingTime/3600, remainingTime/60%60, remainingTime%60
					));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
