package svila.planetlabNetwork.Executers;

import java.nio.file.Path;
import java.nio.file.Paths;

import svila.planetlabNetwork.Graph;
import svila.planetlabNetwork.StaticResources;
import svila.planetlabNetwork.TopologyReader;

public class TopologyChecker {
	public static void main(String[] argv) {
		StaticResources.init();
		
		String topologyName = "smallTopology";
		
		Path topologyPath = Paths.get(StaticResources.baseFolder,
				StaticResources.topologiesFolderName,
				topologyName + ".json");
				
		TopologyReader tr = new TopologyReader(topologyPath);
		Graph g = tr.getTopology();
		System.out.println("Is graph valid: " + g.check());
	}
}
