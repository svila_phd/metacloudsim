package svila.planetlabNetwork.Executers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.StaticResources;

public class ExperimentFolderIndividualScriptExecuter {
	public static void main(String[] args) {
		StaticResources.init();
		
		String folder = "ccgrid_all_v4_fnss";
		execute(folder);
	}
	
	public static void execute(String testbedName) {
		Path experimentsPath = Paths.get(StaticResources.baseFolder,
				StaticResources.generatedExperimentsFolderName,
				testbedName);
		Path outputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.indiviudalExperimentScriptsName,
				testbedName);
		StaticResources.purgeDirectory(outputPath.toFile());
		ConsoleExecuter.generateTestbed(testbedName);
		outputPath.toFile().mkdirs();
		File[] files = experimentsPath.toFile().listFiles();
		List<String> experimentFiles = new ArrayList<String>();
		for(File file : files) {
			experimentFiles.add(file.getAbsolutePath());
		}
		Path executerFilePath = Paths.get(outputPath.toString(), testbedName + "_executer" + ".ps1");
		generateExecuterFile(executerFilePath, experimentFiles);
	}
	
	private static void generateExecuterFile(Path filePath, List<String> experimentFiles) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(filePath.toString()));
			//String jarPath = Paths.get(StaticResources.baseFolder, "netcloud.jar").toString();
			for(String qsubFilePath : experimentFiles) {
				writer.write("java -Xmx5000m -jar " + "metacloud.jar" + " fullpath " + qsubFilePath.toString() + "\n");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
