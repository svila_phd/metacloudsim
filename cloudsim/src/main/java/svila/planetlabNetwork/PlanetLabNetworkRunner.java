package svila.planetlabNetwork;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerDynamicWorkload;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.NetworkTopology;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.VmSchedulerTimeSharedOverSubscription;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.examples.power.Constants;
import org.cloudbus.cloudsim.examples.power.Helper;
import org.cloudbus.cloudsim.examples.power.RunnerAbstract;
import org.cloudbus.cloudsim.examples.power.planetlab.PlanetLabConstants;
import org.cloudbus.cloudsim.examples.power.planetlab.PlanetLabHelper;
import org.cloudbus.cloudsim.network.TopologicalGraph;
import org.cloudbus.cloudsim.network.TopologicalLink;
import org.cloudbus.cloudsim.network.TopologicalNode;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;

import svila.JSONOutput;

public class PlanetLabNetworkRunner extends RunnerAbstract {
	
	Map<Vm, Host> vmToHostMap;
	/**
	 * Instantiates a new planet lab runner.
	 * 
	 * @param enableOutput the enable output
	 * @param outputToFile the output to file
	 * @param inputFolder the input folder
	 * @param outputFolder the output folder
	 * @param workload the workload
	 * @param vmAllocationPolicy the vm allocation policy
	 * @param vmSelectionPolicy the vm selection policy
	 * @param parameter the parameter
	 */
	public PlanetLabNetworkRunner(
			boolean enableOutput,
			boolean outputToFile,
			String inputFolder,
			String outputFolder,
			String workload,
			String vmAllocationPolicy,
			String vmSelectionPolicy,
			String parameter,
			int migrationInterval) {
		super(
				enableOutput,
				outputToFile,
				inputFolder,
				outputFolder,
				workload,
				vmAllocationPolicy,
				vmSelectionPolicy,
				parameter,
				migrationInterval);
	}
	
	public PlanetLabNetworkRunner(
			ExperimentConfiguration ec) {
		super(ec);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cloudbus.cloudsim.examples.power.RunnerAbstract#init(java.lang.String)
	 */
	@Override
	protected void init(String inputFolder) {
		try {
			CloudSim.init(1, Calendar.getInstance(), false);
			int numCloudlets = PlanetLabNetworkConstants.NUMBER_OF_CLOUDLETS;
			broker = Helper.createBroker();
			cloudletList = WorkloadNetworkHelper.createCloudletListWorkload(broker.getId(), inputFolder, numCloudlets);
			for(Cloudlet c : cloudletList) {
				System.out.println(c.getCloudletId() + " " + c.getVmId());
			}
			hostList = new ArrayList<>();
			vmList = new ArrayList<>();
			vmToHostMap = new HashMap<>();
			createEnvironment();
		} catch (Exception e) {
			e.printStackTrace();
			Log.printLine("The simulation has been terminated due to an unexpected error");
			System.exit(0);
		}
	}
	
	protected void init(ExperimentConfiguration ec) {
		try {
			CloudSim.init(1, Calendar.getInstance(), false);
			int numCloudlets = ec.getNumCloudlets();
			broker = Helper.createBroker();
			String planetlabInputPath = Paths.get(StaticResources.baseFolder, StaticResources.workloadsFolderName, ec.getWorkload(), ec.getWorkloadTrace()).toString();
			cloudletList = WorkloadNetworkHelper.createCloudletListWorkload(broker.getId(), planetlabInputPath, numCloudlets);
			for(Cloudlet c : cloudletList) {
				System.out.println(c.getCloudletId() + " " + c.getVmId());
			}
			hostList = new ArrayList<>();
			vmList = new ArrayList<>();
			vmToHostMap = new HashMap<>();
			loadEnvironment();
		} catch (Exception e) {
			e.printStackTrace();
			Log.printLine("The simulation has been terminated due to an unexpected error");
			System.exit(0);
		}
	}
	
	/**
	 * Starts the simulation.
	 * 
	 * @param experimentName the experiment name
	 * @param outputFolder the output folder
	 * @param vmAllocationPolicy the vm allocation policy
	 */
	@Override
	protected void start(String experimentName, String outputFolder, VmAllocationPolicy vmAllocationPolicy) {
		System.out.println("Starting " + experimentName);
		
		try {
			PowerDatacenter datacenter = (PowerDatacenter) Helper.createDatacenter(
					"Datacenter",
					PowerDatacenter.class,
					hostList,
					vmAllocationPolicy);
			
			vmAllocationPolicy.setPowerDatacenter(datacenter);

			datacenter.setDisableMigrations(false);
			datacenter.setMigrationDelayer(new MigrationDelayer(0, migrationInterval));

			broker.submitVmList(vmList);
			broker.submitCloudletList(cloudletList);
                        
			JSONOutput.setEnvironment(datacenter, hostList, vmList, cloudletList);
                        
			CloudSim.terminateSimulation(PlanetLabNetworkConstants.SIMULATION_LIMIT);
			double lastClock = CloudSim.startSimulation();

			List<Cloudlet> newList = broker.getCloudletReceivedList();
			Log.printLine("Received " + newList.size() + " cloudlets");

			CloudSim.stopSimulation();

			Helper.printResults(
					datacenter,
					vmList,
					lastClock,
					experimentName,
					Constants.OUTPUT_CSV,
					outputFolder);
			
			Log.enable();
			List<Cloudlet> cList = broker.getCloudletReceivedList();
			printCloudletList(cList);

		} catch (Exception e) {
			e.printStackTrace();
			Log.printLine("The simulation has been terminated due to an unexpected error");
			System.exit(0);
		}

		Log.printLine("Finished " + experimentName);
	}
	
	public static DatacenterBroker getDatacenterBroker() {
		return broker;
	}
	
	private PowerHost createPowerHost(int hostId, int hostType) {
		List<Pe> peList;
		peList = new ArrayList<Pe>();

		for (int j = 0; j < PlanetLabNetworkConstants.HOST_PES[hostType]; j++) {
			peList.add(new Pe(j, new PeProvisionerSimple(PlanetLabNetworkConstants.HOST_MIPS[hostType])));
		}
		return new PowerHostUtilizationHistory(
				hostId,
					new RamProvisionerSimple(PlanetLabNetworkConstants.HOST_RAM[hostType]),
					new BwProvisionerSimple(PlanetLabNetworkConstants.HOST_BW),
					PlanetLabNetworkConstants.HOST_STORAGE,
					peList,
					new VmSchedulerTimeSharedOverSubscription(peList),
					PlanetLabNetworkConstants.HOST_POWER[hostType]);
	}
	
	private PowerVm createPowerVm(int vmId, int vmType) {
		return new PowerVm(
				vmId,
				broker.getId(),
				PlanetLabNetworkConstants.VM_MIPS[vmType],
				PlanetLabNetworkConstants.VM_PES[vmType],
				PlanetLabNetworkConstants.VM_RAM[vmType],
				PlanetLabNetworkConstants.VM_BW,
				PlanetLabNetworkConstants.VM_SIZE,
				1,
				"Xen",
				new CloudletSchedulerDynamicWorkload(PlanetLabNetworkConstants.VM_MIPS[vmType], Constants.VM_PES[vmType]),
				PlanetLabNetworkConstants.SCHEDULING_INTERVAL);
	}
	
	protected void loadEnvironment() {
		/*loadTopology();
		loadHosts();
		loadVms();
		loadCloudlets();
		
		broker.setFullTopology(totalGraph);
		broker.createSubTopologiesFromFullTopology();
		broker.createBaseTopologyFromFullTopology();
		
		String interactionsFile = Paths.get(StaticResources.baseFolder;
		broker.setSimulatedInteractionsManager(new SimulatedInteractionsManager(interactionsFile));
	*/
	}
	
	protected void createEnvironment() {	

		PowerHost host0 = createPowerHost(0, 0);
		hostList.add(host0);
		PowerHost host1 = createPowerHost(1, 0);
		hostList.add(host1);
		PowerHost host2 = createPowerHost(2, 0);
		hostList.add(host2);
		

		PowerVm vm0 = createPowerVm(0, 0);
		vmList.add(vm0);
		PowerVm vm1 = createPowerVm(1, 0);
		vmList.add(vm1);
		PowerVm vm2 = createPowerVm(2, 0);
		vmList.add(vm2);
		
		for(Vm vm : vmList) {
			vm.hasPreAssignedHost = true;
			vm.preAssignedHost = host0;
		}
		
		// Network configuration
		Node node_switch0 = new Node(0);
		node_switch0.setType(Node.TYPE.CENTRAL);
		Node node_switch1 = new Node(1);
		node_switch1.setType(Node.TYPE.CENTRAL);
		Node node_host0 = new Node(2);
		node_host0.setType(Node.TYPE.LEAF);
		host0.setNetworkId(2);
		Node node_host1 = new Node(3);
		node_host1.setType(Node.TYPE.LEAF);
		host1.setNetworkId(3);
		Node node_host2 = new Node(4);
		node_host2.setType(Node.TYPE.LEAF);
		host2.setNetworkId(4);
		Node node_vm0 = new Node(5);
		node_vm0.setType(Node.TYPE.VM);
		vm0.setNetworkId(5);
		Node node_vm1 = new Node(6);
		node_vm1.setType(Node.TYPE.VM);
		vm1.setNetworkId(6);
		Node node_vm2 = new Node(7);
		node_vm2.setType(Node.TYPE.VM);
		vm2.setNetworkId(7);
		
		Graph totalGraph = new Graph();
		totalGraph.addNode(node_switch0);
		totalGraph.addNode(node_switch1);
		totalGraph.addNode(node_host0);
		totalGraph.addNode(node_host1);
		totalGraph.addNode(node_host2);
		totalGraph.addNode(node_vm0);
		totalGraph.addNode(node_vm1);
		totalGraph.addNode(node_vm2);
		
		float main_link_bw = 100.0f;
		float host_link_bw = 10.0f;
		float vm_link_bw = 10.0f;
		
		totalGraph.linkNodes(node_switch0, node_switch1, main_link_bw);
		totalGraph.linkNodes(node_host0, node_switch0, host_link_bw);
		//totalGraph.linkNodes(node_vm0, node_host0, vm_link_bw);
		totalGraph.linkNodes(node_switch0, node_host1, host_link_bw);
		//totalGraph.linkNodes(node_host1, node_vm1, host_link_bw);
		totalGraph.linkNodes(node_switch1, node_host2, host_link_bw);
		//totalGraph.linkNodes(node_host2, node_vm2, vm_link_bw);
		
		/*Graph centralGraph = new Graph();
		Node bg_switch0 = node_switch0.getCleanNodeCopy();
		Node bg_switch1 = node_switch1.getCleanNodeCopy();
		centralGraph.addNode(bg_switch0);
		centralGraph.addNode(bg_switch1);
		centralGraph.linkNodes(bg_switch0, bg_switch1, main_link_bw);*/
		
		broker.setFullTopology(totalGraph);
		broker.createSubTopologiesFromFullTopology();
		broker.createBaseTopologyFromFullTopology();
		
		String interactionsFile = Paths.get("C:\\Users\\Sergi\\Desktop\\cloudsim\\target\\classes\\workload\\network\\custom\\network.txt").toString();

		//broker.setSimulatedInteractionsManager(new SimulatedInteractionsManager(interactionsFile));
	}
}
