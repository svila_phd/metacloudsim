package svila.planetlabNetwork;

public class MockInteraction extends Interaction {
	public MockInteraction(int sourceId, int endId) {
		super();
		this.sourceId = sourceId;
		this.endId = endId;
	}
	
	@Override
	public float getUtilization(double time) {
		return 1.0f;
	}
	
	@Override
	public float getWeight(double time) {
		return 1.0f;
	}
}
