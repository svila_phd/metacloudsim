package svila.planetlabNetwork;

import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public class NetworkTopologyAssembler {
	
	Graph topologyWithVMs;
	Graph fullTopologyWithVMsLinked;
	float vmHostLinkWeight;
	
	public static void main(String[] args) {
		
	}
	
	public NetworkTopologyAssembler(Graph topologyWithVMs, float vmHostLinkWeight) {
		this.topologyWithVMs = topologyWithVMs;
		this.vmHostLinkWeight = vmHostLinkWeight;
	}
	
	public Graph getFullTopologyWithVMsLinked() {
		return this.fullTopologyWithVMsLinked;
	}
	
	public void completeTopologyWithAllocatedVMs(List<Vm> vms) {
		fullTopologyWithVMsLinked = topologyWithVMs.copy();
		
		for(Vm vm : vms) {
			int hostId;
			if(vm.hasPreAssignedHost) {
				hostId = vm.preAssignedHost.getNetworkId();
			} else {
				hostId = vm.getHost().getNetworkId();
			}
			int vmId = vm.getNetworkId();
			fullTopologyWithVMsLinked.linkNodes(vmId, hostId, vmHostLinkWeight);
		}
	}
	
	public void assignHostToVms(List<PowerHost> hosts, List<Vm> vms) {
		ExperimentConfiguration ec = StaticResources.getCE();
		float hostAllocationUtilization = ec.getHostAllocationUtilization();
		int usedHosts = (int) (hosts.size() * hostAllocationUtilization);
		
		for(int i=0; i<vms.size(); i++) {
			Vm currentVm = vms.get(i);
			PowerHost ph = hosts.get(i % usedHosts);
			currentVm.hasPreAssignedHost = true;
			currentVm.preAssignedHost = ph;
			//System.out.println("Preassignation: " + currentVm.toString() + " with " + ph);
		}
	}
}
