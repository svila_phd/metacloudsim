package svila.planetlabNetwork;

public class NetworkIdGenerator {
	static int currentId = -1;
	
	public static int getCurrentId() {
		return currentId;
	}
	
	public static void generateNextId() {
		currentId++;
	}
	
	public static int getNext() {
		generateNextId();
		return getCurrentId();
	}
	
	public static void reset() {
		currentId = 0;
	}
}
