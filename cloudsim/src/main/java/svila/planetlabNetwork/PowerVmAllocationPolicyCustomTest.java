package svila.planetlabNetwork;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.container.hostSelectionPolicies.HostSelectionPolicy;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;
import org.cloudbus.cloudsim.util.ExecutionTimeMeasurer;

public class PowerVmAllocationPolicyCustomTest extends
PowerVmAllocationPolicyMigrationAbstract {
	
	List<? extends Host> secondaryHostList;
	boolean isHostAlreadyOverused;
	Map<Integer, PowerHost> hostNetworkIds;
	Map<Integer, Integer> vmHostInitialAssignations;
	int numberOfAssignations;

	public PowerVmAllocationPolicyCustomTest(List<? extends Host> hostList, PowerVmSelectionPolicy vmSelectionPolicy) {
		super(hostList, vmSelectionPolicy);
		vmSelectionPolicy.setAllocationPolicy(this);
		secondaryHostList = hostList;
		isHostAlreadyOverused = false;
		this.numberOfAssignations = 0;
		hostNetworkIds = new HashMap<>();
		for(Host h : hostList) {
			hostNetworkIds.put(h.getNetworkId(), (PowerHost)h);
		}
		vmHostInitialAssignations = new HashMap<>();
		vmHostInitialAssignations.put(5, 2);
		vmHostInitialAssignations.put(6, 2);
		vmHostInitialAssignations.put(7, 2);
		//vmHostInitialAssignations.put(8, 2);
		//vmHostInitialAssignations.put(9, 3);
		//vmHostInitialAssignations.put(10, 4);
	}
	
	/*@Override
	public boolean allocateHostForVm(Vm vm) {
		System.out.println("allocateHostForVm");
		System.out.println("VM " + vm.getId());
		
		PowerHost ph = null;
		for (PowerHost host : this.<PowerHost> getHostList()) {
			if(host.getId() == vm.getId()) {
				ph = host;
				break;
			}
		}
		getVmTable().put(vm.getUid(), ph);
		System.out.println("Host " + ph.getId());
		return true;
	}*/
	
	/*@Override
	public PowerHost findHostForVm(Vm vm) {
		System.out.println("findHostForVm");
		System.out.println(vm.getId());

		for(Host h : secondaryHostList) {
			if(h.getId() == vm.getId()) {
				return (PowerHost) h;
			}
		}
		return null;
	}*/
	
	public PowerHost findHostForVm(Vm vm) {
		PowerHost selectedHost;
		if(numberOfAssignations < PlanetLabNetworkConstants.NUMBER_OF_CLOUDLETS) {
			System.out.println("Initial assignation for VM: id: " + vm.getId() + " netId: " + vm.getNetworkId());
			selectedHost = initialAssignation(vm);
		} else {
			selectedHost = migrationAssignation(vm);
		}
		numberOfAssignations++;
		
		return selectedHost;
	}
	
	private PowerHost initialAssignation(Vm vm) {
		int hostNetworkId = vmHostInitialAssignations.get(vm.getNetworkId());
		PowerHost selectedHost = hostNetworkIds.get(hostNetworkId);
		return selectedHost;
	}
	
	private PowerHost migrationAssignation(Vm vm) {
		int hostNetworkId = vmHostInitialAssignations.get(vm.getNetworkId());
		PowerHost selectedHost = hostNetworkIds.get(hostNetworkId);
		return selectedHost;
	}
	
	@Override
	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
		System.out.println("optimizeAllocation");
		ExecutionTimeMeasurer.start("optimizeAllocationTotal");

		ExecutionTimeMeasurer.start("optimizeAllocationHostSelection");
		List<PowerHostUtilizationHistory> overUtilizedHosts = getOverUtilizedHosts();
		getExecutionTimeHistoryHostSelection().add(
				ExecutionTimeMeasurer.end("optimizeAllocationHostSelection"));

		printOverUtilizedHosts(overUtilizedHosts);

		saveAllocation();
		
		ExecutionTimeMeasurer.start("optimizeAllocationVmSelection");
		List<? extends Vm> vmsToMigrate = getVmsToMigrateFromHosts(overUtilizedHosts);
		getExecutionTimeHistoryVmSelection().add(ExecutionTimeMeasurer.end("optimizeAllocationVmSelection"));

		Log.printLine("Reallocation of VMs from the over-utilized hosts:");
		ExecutionTimeMeasurer.start("optimizeAllocationVmReallocation");
		List<Map<String, Object>> migrationMap = getNewVmPlacement(vmsToMigrate, new HashSet<Host>(
				overUtilizedHosts));
		getExecutionTimeHistoryVmReallocation().add(
				ExecutionTimeMeasurer.end("optimizeAllocationVmReallocation"));
		Log.printLine();

		migrationMap.addAll(getMigrationMapFromUnderUtilizedHosts(overUtilizedHosts));

		restoreAllocation();

		getExecutionTimeHistoryTotal().add(ExecutionTimeMeasurer.end("optimizeAllocationTotal"));

		
		showMigrationMap(migrationMap);
		return migrationMap;
	}
	
	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
		System.out.println("findHostForVm excluded");
		System.out.println("VM " + vm.getId());
		
		for(Host h : secondaryHostList) {
			if(h.getId() == vm.getId()) {
				System.out.println("Host " + h.getId());
				return (PowerHost) h;
			}
		}
		return null;
	}

	@Override
	public boolean isHostOverUtilized(PowerHost host) {
		System.out.println("isHostOverUtilized host " + host.getId() + " " + host.getUtilizationOfCpu());
		return host.getUtilizationOfCpu() > 0.7;
		/*if(isHostAlreadyOverused) {
			System.out.println("false");
			return false;
		}
		if(host.getId() == 2) {
			System.out.println("true");
			isHostAlreadyOverused = true;
			return true;
		}
		
		System.out.println("false");
		return false;*/
	}

	/*@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		System.out.println("allocateHostForVm");
		if (host == null) {
			Log.formatLine("%.2f: No suitable host found for VM #" + vm.getId() + "\n", CloudSim.clock());
			return false;
		}
		if (host.vmCreate(vm)) { // if vm has been succesfully created in the host
			getVmTable().put(vm.getUid(), host);
			Log.formatLine(
					"%.2f: VM #" + vm.getId() + " has been allocated to the host #" + host.getId(),
					CloudSim.clock());
			return true;
		}
		Log.formatLine(
				"%.2f: Creation of VM #" + vm.getId() + " on the host #" + host.getId() + " failed\n",
				CloudSim.clock());
		return false;
	}*/
	
}
