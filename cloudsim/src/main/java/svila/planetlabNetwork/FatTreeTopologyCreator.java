package svila.planetlabNetwork;

import java.util.HashMap;
import java.util.List;

public class FatTreeTopologyCreator extends TopologyCreator {
	private int k;
	private int aggregationGroupsPerK;
	private int aggregationSwitchs;
	private int hostsPerSwitch;
	
	public FatTreeTopologyCreator(int k,
			int aggregationGroupsPerK,
			int aggregationSwitchs,
			int hostsPerSwitch,
			float defaultWeightInterconnected,
			float defaultWeightTree) {
		this.k = k;
		this.aggregationGroupsPerK = aggregationGroupsPerK;
		this.aggregationSwitchs = aggregationSwitchs;
		this.hostsPerSwitch = hostsPerSwitch;
		this.setDefaultWeightInterconnected(defaultWeightInterconnected);
		this.setDefaultWeightTree(defaultWeightTree);
		this.leafNodeMap = new HashMap<>();
	}
	
	private void setTypeToNodes(List<Node> nodes, Node.TYPE nodeType) {
		for(Node node : nodes) {
			node.setType(nodeType);
		}
	}
	
	@Override
	public void createTopology() {
		Graph graph = getGraph();
		
		// Core
		List<Node> coreNodes = createNodes(k);
		setTypeToNodes(coreNodes, Node.TYPE.CENTRAL);
		graph.addNodes(coreNodes);
		
		// Aggregation
		List<Node> aggregationNodes = createNodes(k * aggregationGroupsPerK * aggregationSwitchs);
		setTypeToNodes(aggregationNodes, Node.TYPE.TREE);
		graph.addNodes(aggregationNodes);
		
		// Core to Aggregation
		for(int i=0; i<coreNodes.size(); i++) {
			//int groupOffset = i / aggregationSwitchs;
			int groupOffset = i / (k/aggregationSwitchs);
			for(int j=groupOffset; j<aggregationNodes.size(); j+=aggregationSwitchs) {
				System.out.println(i+"-"+j);
				graph.linkNodes(coreNodes.get(i), aggregationNodes.get(j),
						getDefaultWeightInterconnected(),
						Link.LinkType.defaultWeightInterconnected);
			}
		}
		
		// Edge
		List<Node> edgeNodes = createNodes(k * aggregationGroupsPerK * aggregationSwitchs);
		setTypeToNodes(edgeNodes, Node.TYPE.TREE);
		graph.addNodes(edgeNodes);
		
		// Aggregation to Edge
		for(int i=0; i<aggregationNodes.size(); i++) {
			int aggregationGroup = i / aggregationSwitchs;
			for(int j=0; j<edgeNodes.size();j++) {
				int edgeGroup = j / aggregationSwitchs;
				if(aggregationGroup == edgeGroup) {
					graph.linkNodes(aggregationNodes.get(i), edgeNodes.get(j),
							getDefaultWeightInterconnected(),
							Link.LinkType.defaultWeightInterconnected);
				}
			}
		}
		
		// Hosts
		// Edge to Hosts
		for(int i=0; i<edgeNodes.size(); i++) {
			List<Node> hostNodes = createNodes(hostsPerSwitch);
			setTypeToNodes(hostNodes, Node.TYPE.LEAF);
			graph.addNodes(hostNodes);
			
			for(Node node : hostNodes) {
				graph.linkNodes(edgeNodes.get(i), node,
						getDefaultWeightTree(),
						Link.LinkType.defaultWeight);
				leafNodeMap.put(node.getId(), node);
			}
		}
	}
	
	
}
