package svila.planetlabNetwork;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.VmSchedulerTimeSharedOverSubscription;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;
import org.yaml.snakeyaml.Yaml;

public class HostLoader {
	
	List<PowerHostTemplate> hostTemplatesList;
	
	public static void main(String[] args) {
		StaticResources.init();
		String hostsFilename = "testHosts";
		
		HostLoader hl = new HostLoader();
		Path hostFilePath = Paths.get(StaticResources.baseFolder,
									StaticResources.hostsFolderName,
									hostsFilename + ".txt");
		hl.loadHostTemplatesList(new File(hostFilePath.toString()));
		
		List<PowerHostTemplate> hostTemplates = hl.getHostTemplates();
		PowerHostUtilizationHistory phuh = hostTemplates.get(0).generatePowerHost(0);
	}
	
	public HostLoader() {
		
	}
	
	public List<PowerHostTemplate> getHostTemplates() {
		return hostTemplatesList;
	}
	
	public void loadHostTemplatesList(File yamlFile) {
		hostTemplatesList = new ArrayList<>();
	    InputStream input;
		try {
			input = new FileInputStream(yamlFile);
		    Yaml yaml = new Yaml();
		    List<Map<String,Object>> hosts = (List<Map<String,Object>>)yaml.load(input);
			
			for(Map<String,Object> hostwrap: hosts)
			{	
				Map<String,Object> host = (Map<String,Object>)hostwrap.get("Host");
				
				int HOST_MIPS = ((ArrayList<Integer>)host.get("CpuCapacity")).get(0);
				int HOST_PES = (int)host.get("Cores");
				int HOST_RAM = (int)host.get("RAM");
				//int VM_BW = (int)vm.get("BandWidth");
				//int HOST_BW = 1000000;
				int HOST_BW = (int)host.get("BandWidth");
				int HOST_SIZE = (int)host.get("DiskCapacity");
				List<Double> HOST_POWER_MODEL = (ArrayList<Double>)host.get("PowerModel");
				// Sergi
				double ENERGY_ON = (double)host.get("EnergyOn");
				double ENERGY_OFF = (double)host.get("EnergyOff");

				PowerHostTemplate pht = new PowerHostTemplate();
				pht.setMips(HOST_MIPS);
				pht.setPes(HOST_PES);
				pht.setRam(HOST_RAM);
				pht.setBw(HOST_BW);
				pht.setSize(HOST_SIZE);
				double[] powerArray = new double[HOST_POWER_MODEL.size()];
				for(int i=0; i<HOST_POWER_MODEL.size(); i++) {
					powerArray[i] = HOST_POWER_MODEL.get(i);
				}
				pht.setPower(powerArray);
				
				pht.setEnergyOn(ENERGY_ON);
				pht.setEnergyOff(ENERGY_OFF);
				
				hostTemplatesList.add(pht);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
