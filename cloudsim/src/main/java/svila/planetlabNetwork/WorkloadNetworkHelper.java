package svila.planetlabNetwork;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.UtilizationModelNull;
import org.cloudbus.cloudsim.UtilizationModelPlanetLabInMemory;
import org.cloudbus.cloudsim.examples.power.Constants;

import svila.SvilaPrinter;
import svila.workloads.UtilizationModelFactory;

public class WorkloadNetworkHelper {
	/**
	 * Creates the cloudlet list planet lab.
	 * 
	 * @param brokerId the broker id
	 * @param inputFolderName the input folder name
	 * @return the list
	 * @throws FileNotFoundException the file not found exception
	 */
	public static List<Cloudlet> createCloudletListWorkload(int brokerId, String inputFolderName, int cloudlets)
			throws FileNotFoundException {
		List<Cloudlet> list = new ArrayList<Cloudlet>();

		long fileSize = 300;
		long outputSize = 300;
		UtilizationModel utilizationModelNull = new UtilizationModelNull();
		UtilizationModel utilizationModelFull = new UtilizationModelFull();
		//UtilizationModel utilizationModelLinearReverse = new UtilizationModelLinearReverse();

		// TODO: Implementar RAM dinàmica per BitBrains i Materna
		// TODO: Poder assignar una VM a un host tot i no tenir suficient RAM

		File inputFolder = new File(inputFolderName);
		File[] files = inputFolder.listFiles();

		// SERGI: Added to guarantee same file alphabetical order
		Arrays.sort(files);
		
		UtilizationModelFactory utilizationModelFactory = new UtilizationModelFactory();
		String workloadType = StaticResources.getCE().getWorkload();
		
		if(cloudlets == -1) {
			cloudlets = files.length;
			StaticResources.getCE().setNumCloudlets(cloudlets);
		}
		
		// Cloudlet configuration. Suggestion: create a new system to create random cloudlets
		for (int i = 0; i < Math.min(cloudlets, files.length); i++) {
			Cloudlet cloudlet = null;
			try {
                            //System.out.println(files[i].getAbsolutePath());
				SvilaPrinter.print("Cloudlet " + i + " " + files[i].getAbsolutePath());
				cloudlet = new Cloudlet(
						i,
						PlanetLabNetworkConstants.CLOUDLET_LENGTH,
						PlanetLabNetworkConstants.CLOUDLET_PES,
						fileSize,
						outputSize,
						utilizationModelFactory.getUtilitzationModel(
								workloadType,
								files[i].getAbsolutePath(),
								PlanetLabNetworkConstants.SCHEDULING_INTERVAL,
								StaticResources.getCE().getDataStartStep(),
								289
						),
						utilizationModelFull,
						utilizationModelNull);
				//if(true) {
				cloudlet.setSourceFilepath(files[i].getAbsolutePath());
				//	cloudlet.loadSourceData();
				//}
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
			cloudlet.setUserId(brokerId);
			cloudlet.setVmId(i);
			list.add(cloudlet);
		}

		return list;
	}
}
