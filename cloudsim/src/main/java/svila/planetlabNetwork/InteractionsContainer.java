package svila.planetlabNetwork;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Vm;

public class InteractionsContainer {
	
	List<Interaction> interactions;
	Set<Integer> usedIds;
	
	public InteractionsContainer(List<Interaction> interactions) {
		this.interactions = interactions;
		this.usedIds = obtainUsedNodes();
	}
	
	private Set<Integer> obtainUsedNodes() {
		Set<Integer> ids = new HashSet<>();
		
		for(Interaction interaction : interactions) {
			ids.add(interaction.getSourceId());
			ids.add(interaction.getEndId());
		}
		
		return ids;
	}
	
	public Graph getLogicalGraphForTime(double time, List<Vm> vmList) {
		Graph g = new Graph();
		
		boolean networkEnabled = StaticResources.getCE().isNetwork();
		if(!networkEnabled) {
			return g;
		} 
		
		for(Integer id : usedIds) {
			Node n = new Node(id);
			n.setType(Node.TYPE.VM);
			g.addNode(n);
		}
		
		Set<Integer> switchedOffVms = new HashSet<>();
		for(Vm vm : vmList) {
			if(vm.getTotalUtilizationOfCpu(time) == 0.0) {
				switchedOffVms.add(vm.getNetworkId());
			}
		}
		
		for(Interaction interaction : interactions) {
			if(switchedOffVms.contains(interaction.getSourceId()) ||
				switchedOffVms.contains(interaction.getEndId()) ) {
				continue;
			}
			g.linkNodes(interaction.getSourceId(), interaction.getEndId(), interaction.getWeight(time));
		}
		
		return g;
	}
	
	public void addInteractionsToVms(List<Vm> vmList) {
		Map<Integer, Vm> vmMap = new HashMap<>();
		for(Vm vm : vmList) {
			vmMap.put(vm.getNetworkId(), vm);
		}
		
		for(Interaction interaction : interactions) {
			vmMap.get(interaction.getSourceId()).addVmInteraction(interaction);
			vmMap.get(interaction.getEndId()).addVmInteraction(interaction);
		}
	}
	
	public List<Interaction> getInteractions() {
		return this.interactions;
	}
}
