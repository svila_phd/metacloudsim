package svila.planetlabNetwork;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import svila.planetlabNetwork.Link.LinkType;


public class TopologyReader {
	Path topologyPath;
	Graph graph;
	
	public static void main(String[] args) {
		StaticResources.init();
		
		String topologyName = "debugTopology";
		
		Path topologyPath = Paths.get(StaticResources.baseFolder,
				StaticResources.topologiesFolderName,
				topologyName + ".json");
				
		TopologyReader tr = new TopologyReader(topologyPath);
		Graph g = tr.getTopology();
		System.out.println(g);
		System.out.println(g.getAdjacencyMatrix());
	}
	
	public TopologyReader(Path topologyPath) {
		this.topologyPath = topologyPath;
	}
	
	public void readTopology() {
		File topologyFile = new File(this.topologyPath.toString());
		try {
			JSONObject topologyJson = new JSONObject(new String(Files.readAllBytes(topologyFile.toPath()),"UTF-8"));
			JSONArray nodes = topologyJson.getJSONArray("nodes");
			JSONArray links = topologyJson.getJSONArray("links");
			this.graph = generateGraph(nodes, links);
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private float getWeight(JSONObject linkJson) {
		float weight = 0f;
		ExperimentConfiguration ec = StaticResources.getCE();
		if(ec != null && ec.isOverrideLinkWeight()) {
			LinkType linkType = LinkType.valueOf(linkJson.getString("linkType"));
			switch(linkType) {
			case defaultWeight:
				weight = ec.getDefaultWeight();
				break;
			case defaultWeightInterconnected:
				weight = ec.getDefaultWeightInterconnected();
				break;
			default:
				weight = linkJson.getInt("weight");
				break;
			}
		} else {
			weight = linkJson.getInt("weight");
		}
		return weight;
	}
	
	private Graph generateGraph(JSONArray nodes, JSONArray links) {
		Graph g = new Graph();
		
		for(int i=0; i<nodes.length(); i++) {
			JSONObject nodeJson = nodes.getJSONObject(i);
			int id = nodeJson.getInt("id");
			Node.TYPE type = Node.TYPE.valueOf(nodeJson.getString("type"));
			Node node = new Node(id);
			node.setType(type);
			g.addNode(node);
		}
		
		for(int i=0; i<links.length(); i++) {
			JSONObject linkJson = links.getJSONObject(i);
			int sourceId = linkJson.getInt("source");
			int endId = linkJson.getInt("end");
			float weight = getWeight(linkJson);
			
			g.linkNodesButOneLink(sourceId, endId, weight, Link.LinkType.valueOf(linkJson.getString("linkType")));
		}
		
		return g;
	}
	
	public Graph getTopology() {
		if(graph == null) {
			readTopology();
		}
		
		return graph;
	}
}
