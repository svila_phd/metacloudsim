package svila.planetlabNetwork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Node {
    
	private int id; 
	
    private String name;
    private int level;
    
    public enum TYPE {CENTRAL, TREE, LEAF, VM, NONE};
    private Node.TYPE type = TYPE.NONE;
     
    private List<Node> shortestPath = new LinkedList<>();
     
    private Float distance = Float.MAX_VALUE;
     
    Map<Node, Float> adjacentNodes = new HashMap<>();
 
    public Node.TYPE getType() {
    	return type;
    }
    
    public void setType(Node.TYPE type) {
    	this.type = type;
    }
    
    public void addDestination(Node destination, Float distance) {
        adjacentNodes.put(destination, distance);
    }
    
    public void addDoubleDestination(Node destination, Float distance) {
        adjacentNodes.put(destination, distance);
        destination.adjacentNodes.put(this, distance);
    }
    
    public Node(int id) {
    	this.id = id;
    	this.name = String.valueOf(id);
    }
  
    public Node(String name) {
        this.name = name;
    }
    
    public Node(String name, int id) {
        this.name = name;
        this.id = id;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public List<Node> getShortestPath() {
		return shortestPath;
	}
	
	public List<Node> getShortestPathCopy() {
		return new ArrayList<Node>(shortestPath);
	}

	public void setShortestPath(List<Node> shortestPath) {
		this.shortestPath = shortestPath;
	}

	public Float getDistance() {
		return distance;
	}

	public void setDistance(Float distance) {
		this.distance = distance;
	}

	public Map<Node, Float> getAdjacentNodes() {
		return adjacentNodes;
	}

	public void setAdjacentNodes(Map<Node, Float> adjacentNodes) {
		this.adjacentNodes = adjacentNodes;
	}
     
    public Node getCleanNodeCopy() {
    	Node n = new Node(this.getName());
    	n.id = this.id;
    	n.level = this.level;
    	n.type = this.type;
    	
    	return n;
    }
	
	@Override
	public String toString() {
		return id + " (" + type + ")";
	}
}
