package svila.planetlabNetwork;

import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerDynamicWorkload;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.VmSchedulerTimeSharedOverSubscription;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.examples.power.Constants;
import org.cloudbus.cloudsim.examples.power.Helper;
import org.cloudbus.cloudsim.examples.power.RunnerAbstract;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;

import svila.JSONOutput;
import svila.SvilaPrinter;
import svila.planetlabNetwork.results.InitialScenarioExporter;

public class GlobalRunner extends RunnerAbstract {
	Map<Vm, Host> vmToHostMap;
	/**
	 * Instantiates a new planet lab runner.
	 * 
	 * @param enableOutput the enable output
	 * @param outputToFile the output to file
	 * @param inputFolder the input folder
	 * @param outputFolder the output folder
	 * @param workload the workload
	 * @param vmAllocationPolicy the vm allocation policy
	 * @param vmSelectionPolicy the vm selection policy
	 * @param parameter the parameter
	 */
	public GlobalRunner(
			boolean enableOutput,
			boolean outputToFile,
			String inputFolder,
			String outputFolder,
			String workload,
			String vmAllocationPolicy,
			String vmSelectionPolicy,
			String parameter,
			int migrationInterval) {
		super(
				enableOutput,
				outputToFile,
				inputFolder,
				outputFolder,
				workload,
				vmAllocationPolicy,
				vmSelectionPolicy,
				parameter,
				migrationInterval);
	}
	
	public GlobalRunner(
			ExperimentConfiguration ec) {
		super(ec);
	}
	
	@Override
	protected void init(String inputFolder) {
		// Call the custom init due it is required to call using init(String)
		init(StaticResources.getCE());
	}

	protected void init(ExperimentConfiguration ec) {
		try {
			CloudSim.init(1, Calendar.getInstance(), false);
			SvilaPrinter.enable(StaticResources.getCE().isPrinter());
			int numCloudlets = ec.getNumCloudlets();
			broker = Helper.createBroker();

			String workloadInputPath = Paths.get(StaticResources.baseFolder, StaticResources.workloadsFolderName, ec.getWorkload(), ec.getWorkloadTrace()).toString();
			cloudletList = WorkloadNetworkHelper.createCloudletListWorkload(broker.getId(), workloadInputPath, numCloudlets);			
			
			// TODO: Set initial RAM
			//VMLoadBalancer.modifyCloudlets(cloudletList);
			
			hostList = new ArrayList<>();
			vmList = new ArrayList<>();
			vmToHostMap = new HashMap<>();
			loadEnvironment();
			//exportInitialEnvironment();
		} catch (Exception e) {
			e.printStackTrace();
			Log.printLine("The simulation has been terminated due to an unexpected error");
			System.exit(0);
		}
	}
	
	private void exportInitialEnvironment() {
		InitialScenarioExporter scenarioExporter = new InitialScenarioExporter(cloudletList, vmList);
		scenarioExporter.export();
		System.exit(0);
	}
	
	/**
	 * Starts the simulation.
	 * 
	 * @param experimentName the experiment name
	 * @param outputFolder the output folder
	 * @param vmAllocationPolicy the vm allocation policy
	 */
	@Override
	protected void start(String experimentName, String outputFolder, VmAllocationPolicy vmAllocationPolicy) {
		System.out.println("Starting " + experimentName);
		
		try {
			PowerDatacenter datacenter = (PowerDatacenter) Helper.createDatacenter(
					"Datacenter",
					PowerDatacenter.class,
					hostList,
					vmAllocationPolicy);
			
			vmAllocationPolicy.setPowerDatacenter(datacenter);

			datacenter.setDisableMigrations(false);
			datacenter.setMigrationDelayer(new MigrationDelayer(migrationInterval, migrationInterval));

			VMLoadBalancer.init(broker, vmList, cloudletList, StaticResources.getCE().getNumCloudlets());
			
			// Planificar totes els VMs amb els seus temps corresponents
//			Map<Integer, Double> timeMap = new HashMap<>();
//			List<Vm> currentVms = VMLoadBalancer.getNextVms();
//			while(currentVms.size() > 0) {
//				for(int i=0; i<currentVms.size(); i++) {
//					
//				}
//				VMLoadBalancer.loadNext();
//				currentVms = VMLoadBalancer.getNextVms();
//			}
			
			//broker.submitVmList(vmList);
			//broker.submitCloudletList(cloudletList);
			
			broker.submitVmList(VMLoadBalancer.getNextVms());
			broker.submitCloudletList(VMLoadBalancer.getNextCloudlets());
			
			//broker.submitVmList(vmList);
			//broker.submitCloudletList(cloudletList);
			
			//GlobalPowerBroker globalBroker = new GlobalPowerBroker("GlobalPowerBroker", 600.0, vmList, cloudletList);

			datacenter.setDatacenterBroker(broker);
                        
			JSONOutput.setEnvironment(datacenter, hostList, vmList, cloudletList);
                        
			CloudSim.terminateSimulation(StaticResources.getCE().getSimulationTimeLimit());
			
			long startTime = System.nanoTime();
			double lastClock = CloudSim.startSimulation();

			List<Cloudlet> newList = broker.getCloudletReceivedList();
			Log.printLine("Received " + newList.size() + " cloudlets");

			CloudSim.stopSimulation();
			long endTime = System.nanoTime();

			Helper.printResults(
					datacenter,
					vmList,
					lastClock,
					experimentName,
					Constants.OUTPUT_CSV,
					outputFolder);
			
			Log.enable();
			List<Cloudlet> cList = broker.getCloudletReceivedList();
			printCloudletList(cList);
			
			exportResults(startTime, endTime, lastClock);

			DecisionLogger.close();
		} catch (Exception e) {
			e.printStackTrace();
			//Log.printLine("The simulation has been terminated due to an unexpected error");
			//System.exit(0);
			String errorMessage = "GlobalRunner: The simulation has been terminated due to an unexpected error";
            StaticResources.throwRuntimeException(new RuntimeException(errorMessage), errorMessage);
		}

		Log.printLine("Finished " + experimentName);
	}
	
	public static DatacenterBroker getDatacenterBroker() {
		return broker;
	}
	
	private Graph loadTopology() {
		Path topologyPath = Paths.get(StaticResources.baseFolder,
										StaticResources.topologiesFolderName,
					StaticResources.getCE().getTopology() + ".json");
		TopologyReader tr = new TopologyReader(topologyPath);
		tr.readTopology();
		Graph topology = tr.getTopology();
		return topology;
	}
	
	private void addHostsFromTemplate(List<PowerHost> hosts, int num, PowerHostTemplate template) {
		for(int i=0; i<num; i++) {
			PowerHost ph = template.generatePowerHost(-1);
			hosts.add(ph);
		}
	}
	
	private void addVmsFromTemplate(List<Vm> vms, int num, PowerVMTemplate template) {
		for(int i=0; i<num; i++) {
			PowerVm pv = template.generatePowerVM(-1, StaticResources.userId);
			vms.add(pv);
		}
	}
	
	private List<PowerHost> loadHosts(Graph topologyWithoutVms) {
		List<PowerHost> hosts = new ArrayList<>();
		ExperimentConfiguration ec = StaticResources.getCE();
		List<Node> leafNodes = new ArrayList<>();
		for(Node node : topologyWithoutVms.getNodes()) {
			if(node.getType().equals(Node.TYPE.LEAF)) {
				leafNodes.add(node);
			}
		}
		
		Collections.sort(leafNodes, new Comparator<Node>() {
			  @Override
			  public int compare(Node n1, Node n2) {
				  if(n1.getId() == n2.getId()) {
					  return 0;  
				  } else if(n1.getId() > n2.getId()) {  
					  return 1;  
				  } else {
					  return -1;  
				  }
			  }
			});
		
		int numHosts = leafNodes.size();
		float totalDistribution = 0;
		
		Path hostsPath = Paths.get(StaticResources.baseFolder,
									StaticResources.hostsFolderName,
									ec.getHosts()+".txt");
		HostLoader hl = new HostLoader();
		hl.loadHostTemplatesList(new File(hostsPath.toString()));
		
		List<PowerHostTemplate> hostTemplates = hl.getHostTemplates();
		List<Integer> distributions = ec.getHostsDistribution();
		for(int i=0; i<distributions.size(); i++) {
			totalDistribution += distributions.get(i);
		}
		
		for(int i=0; i<distributions.size()-1; i++) {
			int currentNumHosts = (int)((distributions.get(i) / totalDistribution) * numHosts);
			addHostsFromTemplate(hosts, currentNumHosts, hostTemplates.get(i));
		}
		addHostsFromTemplate(hosts, numHosts-hosts.size(), hostTemplates.get(hostTemplates.size()-1));
		
		// Shuffle the order to obtain a random distribution of types
		Collections.shuffle(hosts, new Random(ec.getRandomSeed()));
		
		
		// Adding the networkId into the hosts
		for(int i=0; i<hosts.size(); i++) {
			PowerHost ph = hosts.get(i);
			Node networkNode = leafNodes.get(i);
			ph.setNetworkId(networkNode.getId());
			ph.setId(i);
		}
		
		// Adding HostVMlLinkWeight
		for(int i=0; i<hosts.size(); i++) {
			PowerHost ph = hosts.get(i);
			ph.setHostVmBandwith(ec.getDefaultWeight());
		}
		
		return hosts;
	}
	
	private List<Vm> loadVms() {
		ExperimentConfiguration ec = StaticResources.getCE();
		int numVms = ec.getNumCloudlets();
		List<Vm> vms = new ArrayList<>();
		
		Path vmsPath = Paths.get(StaticResources.baseFolder,
				StaticResources.vmsFolderName,
				ec.getVms()+".txt");
		VMLoader vl = new VMLoader();
		vl.loadVmTemplates(new File(vmsPath.toString()));
		List<PowerVMTemplate> vmTemplates = vl.getVmTemplates();
		
		float totalDistribution = 0;
		List<Integer> distributions = ec.getVmsDistribution();
		for(int i=0; i<distributions.size(); i++) {
			totalDistribution += distributions.get(i);
		}
		
		for(int i=0; i<distributions.size()-1; i++) {
			int currentNumVms = (int)((distributions.get(i) / totalDistribution) * numVms);
			addVmsFromTemplate(vms, currentNumVms, vmTemplates.get(i));
		}
		addVmsFromTemplate(vms, numVms-vms.size(), vmTemplates.get(vmTemplates.size()-1));
		
		// Shuffle the order to obtain a random distribution of types
		Collections.shuffle(vms, new Random(ec.getRandomSeed()));
		
		for(int i=0; i<vms.size(); i++) {
			vms.get(i).setId(i);
		}
		
		return vms;
	}
	
	private List<String> getInteractionFullPaths() {
		List<String> fullPaths = new ArrayList<>();
		ExperimentConfiguration ec = StaticResources.getCE();
		List<String> filenames = ec.getInteractions();
		
		for(int i=0; i<filenames.size(); i++) {
			Path currentPath = Paths.get(StaticResources.baseFolder,
										StaticResources.interactionsFolderName,
										filenames.get(i));
			fullPaths.add(currentPath.toString());
		}
		
		return fullPaths;
	}

	private List<Node> generateVMNodes(int numNodesInBaseGraph, List<Vm> vms) {
		List<Node> nodes = new ArrayList<>();
		int currentId = numNodesInBaseGraph;
		for(Vm vm : vms) {
			Node node = new Node(currentId);
			node.setType(Node.TYPE.VM);
			vm.setNetworkId(currentId);
			currentId++;
			nodes.add(node);
		}
		
		return nodes;
	}
	
	private double adjustLoad() {
		double totalRequestedMIPS = 0.0;
		double maxTime = 86400.0;
		double deltaTime = 300.0;
		
		Instant firstTime = Instant.now();
		
		for(int i=0; i<vmList.size(); i++) {
			double vmCapacity = vmList.get(i).getMips();
			double cloudletRequested = 0.0;
			Cloudlet currentCloudlet = cloudletList.get(i);
			for(double time=0; time < maxTime; time+=deltaTime) {
				double requestedPeriod = vmCapacity * currentCloudlet.getUtilizationOfCpu(time) * deltaTime;
				cloudletRequested += requestedPeriod;
			}
			totalRequestedMIPS += cloudletRequested;
		}
		Instant secondTime = Instant.now();
		Duration diffTime = Duration.between(firstTime, secondTime);
		double currentTime = diffTime.getNano()/1000000000.0;
		SvilaPrinter.print("Time: " + currentTime);
		SvilaPrinter.print("Calculated total requested: " + totalRequestedMIPS);
		
		double hostsTotalMIPS = 0.0;
		for(int i=0; i<hostList.size(); i++) {
			double hostCapacity = hostList.get(i).getTotalMips();
			double hostTotalMIPS = hostCapacity * maxTime;
			hostsTotalMIPS += hostTotalMIPS;
		}
		SvilaPrinter.print("Calculated hosts capacity: " + hostsTotalMIPS);
		double ratio_VMs_hosts = totalRequestedMIPS/hostsTotalMIPS;
		SvilaPrinter.print("Ratio VMs/hosts: " + (ratio_VMs_hosts * 100));
		return ratio_VMs_hosts;
	}
	
	private void showCPUStats() {
		double hostMips = 0.0;
		double vmMips = 0.0;
		double hostRam = 0.0;
		double vmRam = 0.0;
		for(PowerHost host : hostList) {
			hostMips += host.getTotalMips();
			hostRam += host.getRam();
		}
		
		for(Vm vm : vmList) {
			vmMips += vm.getMips();
			vmRam += vm.getRam();
		}		
		
		System.out.println("Host MIPS: " + hostMips);
		System.out.println("VM MIPS: " + vmMips);
		System.out.println("Ratio MIPS VMs/Hosts: " + (vmMips/hostMips));
		System.out.println("Host RAM: " + hostRam);
		System.out.println("VM RAM: " + vmRam);
		System.out.println("Ratio RAM VMs/Hosts: " + (vmRam/hostRam));
		System.out.println("Num Hosts: " + hostList.size());
		System.out.println("Num vms: " + vmList.size());
		System.out.println("Ratio VMs/Hosts: " + ((float)vmList.size()/hostList.size()));
		
		Map<Integer, Integer> countMipsVmMap = new HashMap<>();
		for(Vm vm : vmList) {
			Integer currentMips = (int) vm.getMips();
			if(countMipsVmMap.containsKey(currentMips)) {
				countMipsVmMap.put(currentMips, countMipsVmMap.get(currentMips) + 1);
			} else {
				countMipsVmMap.put(currentMips, 1);
			}
		}
		
		for(Entry<Integer, Integer> entry : countMipsVmMap.entrySet()) {
			System.out.println(entry.getValue() + " VMs with " + entry.getKey() + " MIPS");
		}
		
		Map<Integer, Integer> countMipsHostMap = new HashMap<>();
		for(PowerHost host : hostList) {
			Integer currentMips = (int) host.getTotalMips();
			if(countMipsHostMap.containsKey(currentMips)) {
				countMipsHostMap.put(currentMips, countMipsHostMap.get(currentMips) + 1);
			} else {
				countMipsHostMap.put(currentMips, 1);
			}
		}
		
		for(Entry<Integer, Integer> entry : countMipsHostMap.entrySet()) {
			System.out.println(entry.getValue() + " hosts with " + entry.getKey() + " MIPS");
		}
	}
	
	protected void loadEnvironment() {
		ExperimentConfiguration ec = StaticResources.getCE();
		Graph topologyWithoutVms = loadTopology();
		
		TopologyHostDistanceManager.loadPhysicalGraph(topologyWithoutVms);
		
		DecisionLogger.setFilename(StaticResources.decisionLogger);
		DecisionLogger.out("test");
		DecisionLogger.separator();
		
		hostList = loadHosts(topologyWithoutVms);
		
		TopologyHostDistanceManager.loadHosts(hostList);
		TopologyHostDistanceManager.calculateDistances();
		TopologyHostDistanceManager.calculateHostDistances();
		//TopologyHostDistanceManager.show();
		vmList = loadVms();
		

		
		float initialMips = hostList.get(0).getTotalMips();
		if(ec.isAdjustLoadRatio()) {
			double ratio = adjustLoad();
			double minRatio = ec.getMinRatio();
			double maxRatio = ec.getMaxRatio();
			double offsetRatio = ec.getOffsetRatio();
			while(ratio < minRatio || ratio > maxRatio) {
				double multiplier = ratio < minRatio ? 1-offsetRatio : 1+offsetRatio;
				for(PowerHost host : hostList) {
					for(Pe pe : host.getPeList()) {
						pe.setMips(pe.getMips() * multiplier);
					}
				}
				ratio = adjustLoad();
			}
		}
		float finalMips = hostList.get(0).getTotalMips();
		float ratioMips = finalMips / initialMips;
		SvilaPrinter.print("Ratio MIPS: " + ratioMips + " From " + initialMips + " to " + finalMips);
		
		
		/*double initialMips = vmList.get(0).getMips();
		if(ec.isAdjustLoadRatio()) {
			double ratio = adjustLoad();
			SvilaPrinter.print("Initial ratio: " + ratio);
			double minRatio = ec.getMinRatio();
			double maxRatio = ec.getMaxRatio();
			double offsetRatio = ec.getOffsetRatio();
			while(ratio < minRatio || ratio > maxRatio) {
				double multiplier = ratio < minRatio ? 1+offsetRatio : 1-offsetRatio;
				for(Vm vm : vmList) {
					vm.setMips(vm.getMips() * multiplier);
				}
				ratio = adjustLoad();
			}
		}
		double finalMips = vmList.get(0).getMips();
		double ratioMips = finalMips / initialMips;
		SvilaPrinter.print("Ratio MIPS: " + ratioMips + " From " + initialMips + " to " + finalMips);
		for(int i=0; i<vmList.size(); i++) {
			Vm currentVm = vmList.get(i);
			Cloudlet currentCloudlet = cloudletList.get(i);
			SvilaPrinter.print(currentVm.getId() + ": " + currentVm.getMips());
			SvilaPrinter.print("\t300: " + currentVm.getMips() * currentCloudlet.getUtilizationOfCpu(300));
			SvilaPrinter.print("\t40000: " + currentVm.getMips() * currentCloudlet.getUtilizationOfCpu(40000));
			SvilaPrinter.print("\t80000: " + currentVm.getMips() * currentCloudlet.getUtilizationOfCpu(80000));
		}*/
		
		ec.setUsedHostRatio(ratioMips);
		showCPUStats();
		
		List<Node> vmNodes = generateVMNodes(
				topologyWithoutVms.getNodes().size(),
				vmList);
		
		Graph fullTopology = topologyWithoutVms.copy();
		for(Node node : vmNodes) {
			fullTopology.addNode(node);
		}
		
		broker.setFullTopology(fullTopology);
		broker.createSubTopologiesFromFullTopology();
		broker.createBaseTopologyFromFullTopology();
		
		
		float logicLinkWeight = Float.valueOf(String.valueOf(ec.getLogicLinkWeight()));
		//float VMHostlinkWeight = Float.valueOf(String.valueOf(ec.defaultWeight));
		float VMHostlinkWeight = Float.MAX_VALUE; // VM-host links have no limit
		NetworkTopologyAssembler nta = new NetworkTopologyAssembler(fullTopology, VMHostlinkWeight);
		nta.assignHostToVms(hostList, vmList);
		nta.completeTopologyWithAllocatedVMs(vmList);
		Graph fullTopologyWithVMsLinked = nta.getFullTopologyWithVMsLinked();
		
		InteractionsCreator interactionsCreator = new InteractionsCreator(
				fullTopologyWithVMsLinked,
				vmList,
				getInteractionFullPaths(),
				ec.getInteractionsDistribution(),
				ec.getRandomSeed(),
				logicLinkWeight,
				ec.getInternalClusterRate(),
				ec.getExternalClusterRate()
				);
		interactionsCreator.generateInteractions();
		List<Interaction> interactions = interactionsCreator.getInteractions();
		InteractionsContainer interactionsContainer = new InteractionsContainer(interactions);
		interactionsContainer.addInteractionsToVms(vmList);
		broker.setInteractionsContainer(interactionsContainer);
	}

}
