package svila.planetlabNetwork.DistributionGenerators;

import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Vm;

public class AllAtStart extends VMDistributionGenerator {

	public AllAtStart(List<Vm> vms, List<Cloudlet> cloudlets, int numSteps) {
		super(vms, cloudlets, numSteps);
	}

	@Override
	public void preCalculateDistribution() {
		this.vmsInEachStep.get(0).addAll(this.vms);
		this.cloudletsInEachStep.get(0).addAll(this.cloudlets);
	}
}
