package svila.planetlabNetwork.DistributionGenerators;

import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Vm;

public abstract class VMDistributionGenerator {
	List<Vm> vms;
	List<Cloudlet> cloudlets;
	int numSteps;
	int currentStep;
	List<List<Vm>> vmsInEachStep;
	List<List<Cloudlet>> cloudletsInEachStep;
	
	public VMDistributionGenerator(List<Vm> vms, List<Cloudlet> cloudlets, int numSteps) {
		this.vms = vms;
		this.cloudlets = cloudlets;
		this.numSteps = numSteps;
		this.vmsInEachStep = new ArrayList<>();
		this.cloudletsInEachStep = new ArrayList<>();
		this.initList(this.vmsInEachStep);
		this.initList(this.cloudletsInEachStep);
	}
	
	private <T> void initList(List<List<T>> list) {
		for(int i=0; i<this.numSteps; i++) {
			list.add(new ArrayList<T>());
		}
	}
	
	public abstract void preCalculateDistribution();
	
	public List<Vm> getVmsAtStep(int numStep) {
		return this.vmsInEachStep.get(numStep);
	}
	
	public List<Cloudlet> getCloudletsAtStep(int numStep) {
		return this.cloudletsInEachStep.get(numStep);
	}
}
