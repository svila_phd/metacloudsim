package svila.planetlabNetwork;

import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.CloudletSchedulerDynamicWorkload;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.VmSchedulerTimeSharedOverSubscription;
import org.cloudbus.cloudsim.examples.power.Constants;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;

public class PowerHostTemplate {
	int mips;
	int pes;
	int ram;
	int bw;
	int size;
	double[] power;
	CustomPowerModel customPowerModel;
	//Sergi
	double energyOn;
	double energyOff;
	
	public PowerHostTemplate() {
		
	}
	
	public PowerHostUtilizationHistory generatePowerHost(int hostId) {
		List<Pe> peList;
		peList = new ArrayList<Pe>();

		for (int j = 0; j < this.pes; j++) {
			peList.add(new Pe(j, new PeProvisionerSimple(this.mips)));
		}
		return new PowerHostUtilizationHistory(
					hostId,
					new RamProvisionerSimple(this.ram),
					new BwProvisionerSimple(this.bw),
					this.size,
					peList,
					new VmSchedulerTimeSharedOverSubscription(peList),
					customPowerModel, this.energyOn, this.energyOff);
	}
	
	public double[] getPower() {
		return power;
	}

	public void setPower(double[] power) {
		this.power = power;
		customPowerModel = new CustomPowerModel(this.power);
	}

	public int getMips() {
		return mips;
	}

	public void setMips(int mips) {
		this.mips = mips;
	}

	public int getPes() {
		return pes;
	}

	public void setPes(int pes) {
		this.pes = pes;
	}

	public int getRam() {
		return ram;
	}

	public void setRam(int ram) {
		this.ram = ram;
	}

	public int getBw() {
		return bw;
	}

	public void setBw(int bw) {
		this.bw = bw;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public double getEnergyOn() {
		return energyOn;
	}

	public void setEnergyOn(double energyOn) {
		this.energyOn = energyOn;
	}

	public double getEnergyOff() {
		return energyOff;
	}

	public void setEnergyOff(double energyOff) {
		this.energyOff = energyOff;
	}
	
	
}
