package svila.planetlabNetwork;

import org.cloudbus.cloudsim.CloudletSchedulerDynamicWorkload;
import org.cloudbus.cloudsim.power.PowerVm;

public class PowerVMTemplate {
	int mips;
	int pes;
	int ram;
	int bw;
	int size;
	
	public PowerVMTemplate() {
		
	}
	
	public PowerVm generatePowerVM(int id, int brokerId) {
		return new PowerVm(
				id,
				brokerId,
				mips,
				pes,
				ram,
				bw,
				size,
				1,
				"Xen",
				new CloudletSchedulerDynamicWorkload(
						mips,
						pes),
				PlanetLabNetworkConstants.SCHEDULING_INTERVAL);
	}

	public int getMips() {
		return mips;
	}

	public void setMips(int mips) {
		this.mips = mips;
	}

	public int getPes() {
		return pes;
	}

	public void setPes(int pes) {
		this.pes = pes;
	}

	public int getRam() {
		return ram;
	}

	public void setRam(int ram) {
		this.ram = ram;
	}

	public int getBw() {
		return bw;
	}

	public void setBw(int bw) {
		this.bw = bw;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	
}
