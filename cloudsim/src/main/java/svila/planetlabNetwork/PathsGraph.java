package svila.planetlabNetwork;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PathsGraph {
	Graph graph;
	Set<Node> nodes;
	Set<Path> paths;
	Map<Integer, Set<Path>> nodePathsMap;
	Map<String, Set<Path>> linkPathsMap;
	
	public PathsGraph() {
		this.graph = new Graph();
		this.nodes = new HashSet<>();
		this.paths = new HashSet<>();
		this.nodePathsMap = new HashMap<>();
		this.linkPathsMap = new HashMap<>();
	}
	
	public PathsGraph(Graph graph, Set<Path> paths) {
		this.graph = graph.copy();
		this.paths = paths;
		this.nodes = generateLinkedNodes();
		this.nodePathsMap = generateNodePathsMap();
		this.linkPathsMap = generateLinkPathsMap();
	}
	
	public PathsGraph(Graph graph, Set<Path> paths, Set<Node> nodes) {
		this.graph = graph.copy();
		this.paths = paths;
		this.nodes = nodes;
		this.nodePathsMap = generateNodePathsMap();
		this.linkPathsMap = generateLinkPathsMap();
	}
	
	private Set<Node> generateLinkedNodes() {
		Set<Node> usedNodes = new HashSet<>();
		
		for(Path path : paths) {
			usedNodes.add(path.getNode1());
			usedNodes.add(path.getNode2());
		}
		
		return usedNodes;
	}
	
	public PathsGraph getCopyUsingLogicalGraph(Graph logicalGraph) {
		PathsGraph copyPathsGraph = this.getCopy();
		Graph copyGraph = copyPathsGraph.getGraph();
		copyGraph.clearLinkWeights();
		for(Path path : this.getPaths()) {
			float newWeight = logicalGraph.getWeightFromLink(path.getNode1(), path.getNode2());
			for(Link link : path.getLinks()) {
				Link newLink = new Link(
						link.getSourceNode(),
						link.getEndNode(),
						newWeight,
						link.getLinkType());
				copyGraph.addWeightLink(newLink);
			}
		}
		
		return copyPathsGraph;
	}
	
	public PathsGraph getCopy() {
		Graph copyGraph = graph.copy();
		Set<Node> copyNodes = new HashSet<>();
		for(Node node : nodes) {
			copyNodes.add(node.getCleanNodeCopy());
		}
		Set<Path> copyPaths = new HashSet<>();
		for(Path path : paths) {
			copyPaths.add(path.copy());
		}
		
		return new PathsGraph(copyGraph, copyPaths, copyNodes);
	}
	
	private <V, K> void addElementToHashMapSet(Map<V,Set<K>> map, V key, K value) {
		if(!map.containsKey(key)) {
			map.put(key, new HashSet<>());
		}
		
		map.get(key).add(value);
	}
	
	
	private Map<Integer, Set<Path>> generateNodePathsMap() {
		Map<Integer, Set<Path>> map = new HashMap<>();
		for(Path path : paths) {
			addElementToHashMapSet(map, path.getNode1().getId(), path);
			addElementToHashMapSet(map, path.getNode2().getId(), path);
		}
		
		return map;
	}
	
	private Map<String, Set<Path>> generateLinkPathsMap() {
		Map<String, Set<Path>> map = new HashMap<>();
		
		for(Path path : paths) {
			for(Link link : path.getLinks()) {
				addElementToHashMapSet(map, link.getName(), path);
			}
		}
		
		return map;
	}

	public Graph getGraph() {
		return graph;
	}

	public Set<Path> getPaths() {
		return paths;
	}
	
	public Set<Path> getPathsForLink(Link link) {
		return linkPathsMap.get(link.getName());
	}
	
	public Set<Path> getPathsForNode(Node node) {
		return nodePathsMap.get(node.getId());
	}
	
	public Path getPathForNodes(Node node1, Node node2) {
		Set<Path> paths1 = new HashSet<>(nodePathsMap.get(node1.getId()));
		Set<Path> paths2 = new HashSet<>(nodePathsMap.get(node2.getId()));
		paths1.retainAll(paths2);
		if(paths1.size() == 1) {
			return paths1.iterator().next();
		} else {
			return null;
		}
	}
}
