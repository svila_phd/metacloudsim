package svila.planetlabNetwork;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.examples.power.planetlab.PlanetLabRunner;

import svila.JSONOutput;
import svila.PlanetLabExperiment;

public class PlanetLabNetworkExecuter {
	public static void main(String[] args) throws IOException {	
	// Pesquisar sobre arquitectures de servidors en xarxa
		
	//ShortestPathCalculator spc = new ShortestPathCalculator();
	//spc.test5();
	//System.exit(0);
		
        boolean enableOutput = true;
        boolean outputToFile = false;

    String inputFolder = Paths.get("C:\\Users\\Sergi\\Desktop\\cloudsim\\target\\classes\\workload\\planetlab").toString();
    //String inputFolder = PlanetLabExecuter.class.getClassLoader().getResource("workload/planetlab").getPath();
	String outputFolder = "output";
	//String workload = "20110303"; // PlanetLab workload
    String workload = "custom";
    int migrationInterval = 1;
	List<PlanetLabExperiment> experiments = getExperiments();

        experiments.forEach((experiment) -> {
            JSONOutput.init();
            System.out.println("Running experiment " + experiment);
            new PlanetLabNetworkRunner(
                    enableOutput,
                    outputToFile,
                    inputFolder,
                    outputFolder,
                    workload,
                    experiment.getVmAllocationPolicy(),
                    experiment.getVmSelectionPolicy(),
                    experiment.getParameter(),
                    migrationInterval
            );
        });
    }
    
    public static List<PlanetLabExperiment> getExperiments() {
        List<PlanetLabExperiment> experiments = new ArrayList<>();
        //experiments.add(new PlanetLabExperiment("test", "test", "1.5"));
        //experiments.add(new PlanetLabExperiment("test", "pearson", "1.5"));
        //experiments.add(new PlanetLabExperiment("iqr", "mc", "1.5"));
        experiments.add(new PlanetLabExperiment("lr", "pearson", "1.5"));

        return experiments;
    }
}
