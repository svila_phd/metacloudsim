package svila.planetlabNetwork;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Interaction {
	int sourceId;
	int endId;
	float maxWeight;
	String inputPath;
	/** The scheduling interval. */
	double schedulingInterval;

	/** The data (5 min * 288 = 24 hours). */
	float[] data; 
	
	public Interaction(int sourceId,
						int endId,
						float maxWeight,
						String inputPath,
						double schedulingInterval) {
		this.data = new float[289];
		setSchedulingInterval(schedulingInterval);
		this.sourceId = sourceId;
		this.endId = endId;
		this.maxWeight = maxWeight;
		this.inputPath = inputPath;
		this.schedulingInterval = schedulingInterval;
		loadData();
	}
	
	public Interaction() {
		
	}
	
	private void loadData() {
		try {
			BufferedReader input;
			input = new BufferedReader(new FileReader(inputPath));
			int n = data.length;
			for (int i = 0; i < n - 1; i++) {
				data[i] = Integer.valueOf(input.readLine()) / 100.0f;
			}
			data[n - 1] = data[n - 2];
			input.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public float getUtilization(double time) {
		if (time % getSchedulingInterval() == 0) {
			return data[(int) time / (int) getSchedulingInterval()];
		}
		int time1 = (int) Math.floor(time / getSchedulingInterval());
		int time2 = (int) Math.ceil(time / getSchedulingInterval());
		float utilization1 = data[time1];
		float utilization2 = data[time2];
		float delta = (utilization2 - utilization1) / ((time2 - time1) * (float)getSchedulingInterval());
		float utilization = (float) (utilization1 + delta * (time - time1 * getSchedulingInterval()));
		return utilization;
	}
	
	public float getWeight(double time) {
		return getUtilization(time) * this.maxWeight;
	}
	
	/**
	 * Sets the scheduling interval.
	 * 
	 * @param schedulingInterval the new scheduling interval
	 */
	public void setSchedulingInterval(double schedulingInterval) {
		this.schedulingInterval = schedulingInterval;
	}

	/**
	 * Gets the scheduling interval.
	 * 
	 * @return the scheduling interval
	 */
	public double getSchedulingInterval() {
		return schedulingInterval;
	}
	
	public float[] getData(){
		return data;
	}

	public int getSourceId() {
		return sourceId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	public int getEndId() {
		return endId;
	}

	public void setEndId(int endId) {
		this.endId = endId;
	}

	public float getMaxWeight() {
		return maxWeight;
	}

	public void setMaxWeight(float maxWeight) {
		this.maxWeight = maxWeight;
	}

	public String getInputPath() {
		return inputPath;
	}

	public void setInputPath(String inputPath) {
		this.inputPath = inputPath;
	}
	
	@Override
	public String toString() {
		return sourceId + "-" + endId + " weight: " + maxWeight; 
	}
}
