package svila.planetlabNetwork;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Graph {
	 
    private Set<Node> nodes = new HashSet<>();
    private Set<Link> links = new HashSet<>();
    
    private Map<String, Node> nodeMap = new HashMap<>();
    private Map<Integer, Node> nodeIdMap = new HashMap<>();
    private Map<String, Link> linkMap = new HashMap<>();
    
    public void addNode(Node node) {
        nodes.add(node);
        nodeMap.put(node.getName(), node);
        nodeIdMap.put(node.getId(), node);
    }
    
    public void addNodes(List<Node> nodes) {
    	for(Node node : nodes) {
    		addNode(node);
    	}
    }

	public Set<Node> getNodes() {
		return nodes;
	}

	public void setNodes(Set<Node> nodes) {
		this.nodes = nodes;
	}
	
	public void addLink(Link link) {
		links.add(link);
		linkMap.put(link.getSourceNode().getName() + "-" + link.getEndNode().getName(), link);
	}
	
	public void clearLinkWeights() {
		for(Link link : linkMap.values()) {
			link.clearWeight();
		}
	}
	
	public Graph generateSubTopology(Node.TYPE type) {
		Graph subGraph = new Graph();
		for(Node node : this.getNodes()) {
			if(node.getType() == type) {
				subGraph.addNode(node.getCleanNodeCopy());
			}
		}
		
		for(Link link : this.getLinks()) {
			if(link.getSourceNode().getType() == type &&
					link.getEndNode().getType() == type) {
				subGraph.linkNodes(link.getSourceNode().getId(), link.getEndNode().getId(), link.getWeight());
			}
		}
		return subGraph;
	}
	
	public Graph generateGraphWithClearedLinks() {
		Graph newGraph = this.copy();
		newGraph.clearLinkWeights();
		return newGraph;
	}
	
	public Graph generateSubstractionGraph(Graph graph) {
		Graph substractionGraph = this.copy();
		for(Link link : graph.getLinks()) {
			substractionGraph.getLinkFromNodes(link.getSourceNode(), link.getEndNode())
				.substractWeight(link.getWeight());
			
		}
		return substractionGraph;
	}
	
	public Graph generateNewGraphWithSelectedTypes(Set<Node.TYPE> typeSet) {
		Graph newGraph = new Graph();
		
		for(Node node : this.getNodes()) {
			if(typeSet.contains(node.getType())) {
				newGraph.addNode(node.getCleanNodeCopy());
			}
		}
		
		for(Link link : this.getLinks()) {
			if(typeSet.contains(link.getSourceNode().getType()) &&
					typeSet.contains(link.getEndNode().getType())) {
				newGraph.linkNodesButOneLink(newGraph.getNodeById(link.getSourceNode().getId()),
						newGraph.getNodeById(link.getEndNode().getId()),
						link.getWeight());
			}
		}
		
		return newGraph;
	}
	
	public void addWeightLink(Link link) {
		Link l = this.getLinkFromNodes(link.getSourceNode(), link.getEndNode());
		l.addWeight(link.getWeight());
	}
	
	public void addWeightLinkToBothSidesLink(Link link) {
		Link l1 = this.getLinkFromNodes(link.getSourceNode(), link.getEndNode());
		Link l2 = this.getLinkFromNodes(link.getEndNode(), link.getSourceNode());
		l1.addWeight(link.getWeight());
		l2.addWeight(link.getWeight());
	}
	
	public void linkNodes(int id1, int id2, float weight) {
		linkNodes(getNodeById(id1), getNodeById(id2), weight);
	}

	public void linkNodes(int id1, int id2, float weight, Link.LinkType linkType) {
		linkNodes(getNodeById(id1), getNodeById(id2), weight, linkType);
	}
	
	public void linkNodes(Node node1, Node node2, float weight) {
		Link link1 = new Link(node1, node2, weight);
		Link link2 = new Link(node2, node1, weight);
		addLink(link1);
		addLink(link2);
		node1.addDoubleDestination(node2, weight);
	}
	
	public void linkNodes(Node node1, Node node2, float weight, Link.LinkType linkType) {
		Link link1 = new Link(node1, node2, weight, linkType);
		Link link2 = new Link(node2, node1, weight, linkType);
		addLink(link1);
		addLink(link2);
		node1.addDoubleDestination(node2, weight);
	}
	
	public void linkNodesButOneLink(Node node1, Node node2, float weight) {
		Link link1 = new Link(node1, node2, weight);
		addLink(link1);
		node1.addDestination(node2, weight);
	}
	
	public void linkNodesButOneLink(Node node1, Node node2, float weight, Link.LinkType linkType) {
		Link link1 = new Link(node1, node2, weight, linkType);
		addLink(link1);
		node1.addDestination(node2, weight);
	}
	
	public void linkNodesButOneLink(int id1, int id2, float weight, Link.LinkType linkType) {
		Node node1 = getNodeById(id1);
		Node node2 = getNodeById(id2);
		
		Link link1 = new Link(node1, node2, weight, linkType);
		addLink(link1);
		node1.addDestination(node2, weight);
	}
	
	public Set<Link> getLinks() {
		return links;
	}
	
	public void setLinks(Set<Link> links) {
		this.links = links;
	}
	
	public Node getNodeByName(String name) {
		return this.nodeMap.get(name);
	}
	
	public Node getNodeById(int id) {
		return this.nodeIdMap.get(id);
	}
	
	public Link getLinkFromNodes(Node sourceNode, Node destinationNode) {
		return linkMap.get(sourceNode.getName() + "-" + destinationNode.getName());
	}
	
	public Float getWeightFromLink(Node sourceNode, Node destinationNode) {
		Link link = getLinkFromNodes(sourceNode, destinationNode);
		if(link == null) {
			return 0.0f;
		}
		return link.getWeight();
	}
	
	public void removeLink(int sourceNodeId, int destinationNodeId) {
		Node sourceNode = this.getNodeById(sourceNodeId);
		Node destinationNode = this.getNodeById(destinationNodeId);
		Link link = this.getLinkFromNodes(sourceNode, destinationNode);
		this.links.remove(link);
		this.linkMap.remove(link.getName());
	}
	
	public void removeBothLink(int sourceNodeId, int destinationNodeId) {
		removeLink(sourceNodeId, destinationNodeId);
		removeLink(destinationNodeId, sourceNodeId);
	}
	
	public Map<String, Link> getLinkMap() {
		return this.linkMap;
	}
	
	public Graph copy() {
		Graph copyGraph = new Graph();
		
		for(Node oldNode : nodes) {
			copyGraph.addNode(oldNode.getCleanNodeCopy());
		}
		
		for(Link oldLink : links) {
			Node sourceNode = copyGraph.getNodeById(oldLink.getSourceNode().getId());
			Node endNode = copyGraph.getNodeById(oldLink.getEndNode().getId());
			copyGraph.linkNodesButOneLink(sourceNode, endNode, oldLink.getWeight(), oldLink.getLinkType());			
		}
		
		return copyGraph;
	}
	
	public String toString() {
		String s = "";
		
		s += "Number of nodes: " + nodes.size() +
				"\nList of nodes:\n";
		for(Node n : nodes) {
			s += n + "\n";
		}
		s += "Number of links: " + links.size() +
				"\nList of links:\n";
		for(Link l : links) {
			s += l + "\n";
		}
		
		return s;
	}
	
	public String getAdjacencyMatrix() {
		String s = "";
		int[][] matrix = new int[nodes.size()][nodes.size()];
		for(Link l : links) {
			matrix[l.getSourceNode().getId()][l.getEndNode().getId()] = 1;
			matrix[l.getEndNode().getId()][l.getSourceNode().getId()] = 1;

		}
		
		for(int i=0; i<matrix.length; i++) {
			for(int j=0; j<matrix.length; j++) {
				s += matrix[i][j] +  " ";
			}
			s += "\n";
		}
		
		return s;
	}
	
	public boolean check() {
		boolean isCorrect = true;
		
		if(getLinks().size() != linkMap.values().size()) {
			System.err.println("The number of internal links is not equal");
			return false;
		}
		
		for(Link l : getLinks()) {
			Node n1 = l.getSourceNode();
			Node n2 = l.getEndNode();
			if(getLinkFromNodes(n2, n1) == null) {
				isCorrect = false;
				System.err.println("Link " + n2.getId() + "-" + n1.getId() + " does not exist");
			}
		}
		
		Link[] linksArray = links.toArray(new Link[links.size()]);
		for(int i=0; i<linksArray.length; i++) {
			Link link1 = linksArray[i];
			for(int j=i+1; j<linksArray.length; j++) {
				Link link2 = linksArray[j];
				if(link1.getSourceNode().getId() == link2.getSourceNode().getId() &&
						link1.getEndNode().getId() == link2.getEndNode().getId()) {
					System.err.println("Duplicated link: " + link1.getName());
					isCorrect = false;
				}
					
			}
		}
		
		return isCorrect;
	}
	
	/*public void exportToFilePath(String path) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(path));
			
			
			
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
}
