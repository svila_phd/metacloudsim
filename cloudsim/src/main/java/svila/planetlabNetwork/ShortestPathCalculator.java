package svila.planetlabNetwork;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.network.TopologicalGraph;
import org.cloudbus.cloudsim.network.TopologicalLink;
import org.cloudbus.cloudsim.network.TopologicalNode;

public class ShortestPathCalculator {
	
	/*
	 * Translate the original data to nodes using toString names
	 * Iterate the links to translate
	 * Execute algorithm for each vm pair related
	 * Obtain the list of nodes used
	 * Obtain the links used from the pair of nodes
	 * Create a new map with the aggregate value for each vm pair
	 * 
	 * 
	 * Make slides about the work done and the pending tasks
	 */
	
	TopologicalGraph tg;
	
	public static void resetDistances(Graph g) {
		for(Node n : g.getNodes()) {
			n.setDistance(Float.MAX_VALUE);
			n.getShortestPath().clear();
		}
	}
	
	public static void setAllAdjacentDistancesTo(Graph g, float value) {
		for(Node n : g.getNodes()) {
			for(Node n2 : n.getAdjacentNodes().keySet()) {
				n.getAdjacentNodes().replace(n2, value);
			}
		}
	}
	
	public static Graph calculateShortestPathFromSource(Graph graph, Node source) {
	    source.setDistance(0.0f);
	 
	    Set<Node> settledNodes = new HashSet<>();
	    Set<Node> unsettledNodes = new HashSet<>();
	 
	    unsettledNodes.add(source);
	 
	    while (unsettledNodes.size() != 0) {
	        Node currentNode = getLowestDistanceNode(unsettledNodes);
	        unsettledNodes.remove(currentNode);
	        for (Entry < Node, Float> adjacencyPair: 
	          currentNode.getAdjacentNodes().entrySet()) {
	            Node adjacentNode = adjacencyPair.getKey();
	            Float edgeWeight = adjacencyPair.getValue();
	            if (!settledNodes.contains(adjacentNode)) {
	                calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
	                unsettledNodes.add(adjacentNode);
	            }
	        }
	        settledNodes.add(currentNode);
	    }
	    return graph;
	}
	
	public ShortestPathCalculator(TopologicalGraph tg) {
		this.tg = tg;
	}
	
	public ShortestPathCalculator() {
		
	}
	
	public static TopologicalGraph fromGraphToTopologicalGraph(Graph g) {
		TopologicalGraph tg = new TopologicalGraph();
		Set<Node> graphNodes = g.getNodes();
		for(Node n : graphNodes) {
			TopologicalNode tn = new TopologicalNode(n.getId());
			tg.addNode(tn);
		}
		
		Set<Link> graphLinks = g.getLinks();
		for(Link l : graphLinks) {
			TopologicalLink tl = new TopologicalLink(l.getSourceNode().getId(),
					l.getEndNode().getId(), 0.0f, l.getWeight());
			tg.addLink(tl);
		}
		return tg;
	}
	
	public Graph getAgregatedBandWidthMap(Graph physicalNetwork, Graph vmLinksMap) {
		//System.out.println("Graph");
		//System.out.println(graph);
		//System.out.println("End graph");

		Map<Link, List<Node>> pathGraphs = new HashMap<>();
		Set<Node> nodeIds = new HashSet<>();
		//Iterator<Link> itLink = vmLinksMap.getLinks().iterator();
		
		
		List<Graph> routes = new ArrayList<>();
		
		for(Link l : vmLinksMap.getLinks()) {
		//while(itLink.hasNext()) {
		//	Link l = itLink.next();
			nodeIds.add(physicalNetwork.getNodeById(l.getSourceNode().getId()));
			Node origin = physicalNetwork.getNodeById(l.getSourceNode().getId());
			Node destination = physicalNetwork.getNodeById(l.getEndNode().getId());
			//System.out.println("Obtaining shortest paths for origin node: " + origin.getId());
			resetDistances(physicalNetwork);
			calculateShortestPathFromSource(physicalNetwork, origin);
			//System.out.println("Obtaining path for destination node: " + destination.getId());
			List<Node> shortestPath = destination.getShortestPathCopy();
			shortestPath.add(destination);
			pathGraphs.put(l, shortestPath);
			
			//System.out.println("Shortest path (" + shortestPath.size() + ")");
			//for(Node n : shortestPath) {
			//	System.out.println(n);
			//}
			//System.out.println("End shortest path");
			
			List<Link> usedLinks = getUsedLinks(physicalNetwork, shortestPath);
			
			/*System.out.println("Used links");
			for(Link ll : usedLinks) {
				System.out.println(ll);
			}
			System.out.println(l.getSourceNode().getId() + " - " + l.getEndNode().getId());
			*/
			Graph route = getRoutedGraph(usedLinks, l.getWeight());
			routes.add(route);
		}

// ORIGINAL
//		while(itLink.hasNext()) {
//			Link l = itLink.next();
//			nodeIds.add(physicalNetwork.getNodeById(l.getSourceNode().getId()));
//			Node origin = physicalNetwork.getNodeById(l.getSourceNode().getId());
//			Node destination = physicalNetwork.getNodeById(l.getEndNode().getId());
//			//System.out.println("Obtaining shortest paths for origin node: " + origin.getId());
//			resetDistances(physicalNetwork);
//			calculateShortestPathFromSource(physicalNetwork, origin);
//			//System.out.println("Obtaining path for destination node: " + destination.getId());
//			List<Node> shortestPath = destination.getShortestPathCopy();
//			shortestPath.add(destination);
//			pathGraphs.put(l, shortestPath);
//			
//			//System.out.println("Shortest path (" + shortestPath.size() + ")");
//			//for(Node n : shortestPath) {
//			//	System.out.println(n);
//			//}
//			//System.out.println("End shortest path");
//			
//			List<Link> usedLinks = getUsedLinks(physicalNetwork, shortestPath);
//			
//			/*System.out.println("Used links");
//			for(Link ll : usedLinks) {
//				System.out.println(ll);
//			}
//			System.out.println(l.getSourceNode().getId() + " - " + l.getEndNode().getId());
//			*/
//			Graph route = getRoutedGraph(usedLinks, l.getWeight());
//			routes.add(route);
//		}		
		
		Graph resultingGraph = combineRoutes(routes);
		return resultingGraph;
	}
	
	private List<Link> getLinksCopyWithNewWeight(List<Link> links, float weight) {
		List<Link> newLinks = new ArrayList<>();
		for(Link link : links) {
			Link newLink = link.copy();
			newLink.setWeight(weight);
			newLinks.add(newLink);
		}
		
		return newLinks;
	}
	
	public PathsGraph getAgregatedBandWidthMap2(Graph physicalNetwork, Graph vmLinksMap) {
		Graph agregatedNetwork = physicalNetwork.copy();
		resetDistances(agregatedNetwork);
		setAllAdjacentDistancesTo(agregatedNetwork, 0.0f);
		agregatedNetwork.clearLinkWeights();

		Map<Link, List<Node>> pathGraphs = new HashMap<>();
		Set<Node> nodeIds = new HashSet<>();
		Iterator<Link> itLink = vmLinksMap.getLinks().iterator();
		
		Set<Path> paths = new HashSet<>();
		List<Link> usedLinks = new ArrayList<>();
		while(itLink.hasNext()) {
			Link l = itLink.next();
			nodeIds.add(physicalNetwork.getNodeById(l.getSourceNode().getId()));
			Node origin = physicalNetwork.getNodeById(l.getSourceNode().getId());
			Node destination = physicalNetwork.getNodeById(l.getEndNode().getId());
			
			
			//System.out.println("Obtaining shortest paths for origin node: " + origin.getId());
			resetDistances(physicalNetwork);
			calculateShortestPathFromSource(physicalNetwork, origin);
			//System.out.println("Obtaining path for destination node: " + destination.getId());
			List<Node> shortestPath = destination.getShortestPathCopy();
			shortestPath.add(destination);
			pathGraphs.put(l, shortestPath);
			
			List<Link> currentUsedLinks = getUsedLinks(physicalNetwork, shortestPath);
			List<Link> currentUsedLinksWithLogicWeights = getLinksCopyWithNewWeight(currentUsedLinks, l.getWeight());
			/*System.out.println("Used links");
			for(Link ll : currentUsedLinks) {
				System.out.println(ll);
			}
			System.out.println(l.getSourceNode().getId() + " - " + l.getEndNode().getId());
			*/
			
			Path path = new Path(origin, destination, currentUsedLinksWithLogicWeights);
			paths.add(path);
			
			usedLinks.addAll(currentUsedLinksWithLogicWeights);
			
			for(Link link : currentUsedLinksWithLogicWeights) {
				agregatedNetwork.addWeightLink(link);
			}
		}
		
		PathsGraph pathsGraph = new PathsGraph(agregatedNetwork, paths);

		return pathsGraph;
	}
	
	/*public Graph getAgregatedBandWidthMapUsingPreviousAssignation(Graph physicalNetwork,
			Graph vmLinksMap,
			Map<Vm, Host> lastMigrationMap,
			Map<Vm, Host> currentMigrationMap) {
		// Separar el vincle entre VMs que ja estaven prèviament de les que han canviat
		// Generar les mateixes connexions de les connexions ja existents
		// Llençar dijkstra per la resta de noves connexions
		Set<Vm> vmSet = new HashSet<>();
		vmSet.addAll(lastMigrationMap.keySet());
		vmSet.addAll(currentMigrationMap.keySet());
		
		for(Vm vm : vmSet) {
			if(lastMigrationMap.containsKey(vm) && currentMigrationMap.containsKey(vm)) {
				
				
			}
		}
		/////////////////////////////////
		
		return null;
	}*/

	
	private Graph combineRoutes(List<Graph> routes) {
		Graph combinedGraph = new Graph();
		Set<Node> usedNodes = new HashSet<>();
		
		for(Graph g : routes) {
			for(Node n : g.getNodes()) {
				usedNodes.add(n.getCleanNodeCopy());
			}
		}
		
		for(Node n : usedNodes) {
			combinedGraph.addNode(n);
		}
		
		for(Graph g : routes) {
			for(Link l : g.getLinks()) {
				Link combinedLink = combinedGraph.getLinkFromNodes(l.getSourceNode(), l.getEndNode());
				if(combinedLink == null) {
					combinedGraph.addLink(new Link(l.getSourceNode().getCleanNodeCopy(),
							l.getEndNode().getCleanNodeCopy(), l.getWeight()));
				} else {
					combinedLink.addWeight(l.getWeight());
				}
			}
		}
		
		return combinedGraph;
	}
	
	private List<Link> getLinksToCentralNode(Graph g, Node node) {
		List<Link> links = new ArrayList<>();

		Node currentNode = node;
		Node lastNode = null;
		while(currentNode.getType() != Node.TYPE.CENTRAL) {
			for(Node nextNode : currentNode.getAdjacentNodes().keySet()) {
				if(nextNode != lastNode) {
					links.add(g.getLinkFromNodes(currentNode, nextNode));
					lastNode = currentNode;
					currentNode = nextNode;
					break;
				}
			}
		}
		
		return links;
	}
	
	private List<Link> getShortestPathBetweenCentralNodes(Graph fullGraph, Graph centralGraph, Node centralNode1, Node centralNode2) {
		resetDistances(centralGraph);
		calculateShortestPathFromSource(centralGraph, centralNode1);
		List<Node> path = centralNode2.getShortestPathCopy();
		path.add(centralNode2);
		return getUsedLinks(fullGraph, path);
	}
	
	public List<Link> getShortestPathBetweenTwoVmNodes(Graph physicalGraph, Graph centralGraph, Node node1, Node node2) {		
		List<Link> linksToCentralNode1 = getLinksToCentralNode(physicalGraph, node1);
		List<Link> linksToCentralNode2 = getLinksToCentralNode(physicalGraph, node2);

		Node centralNode1 = linksToCentralNode1.get((linksToCentralNode1.size()-1)).getEndNode();
		Node centralNode2 = linksToCentralNode2.get((linksToCentralNode2.size()-1)).getEndNode();
		
		List<Link> fullPath = new ArrayList<>();
		fullPath.addAll(linksToCentralNode1);
		
		if(centralNode1 != centralNode2) {
			List<Link> linksBetweenCentralNodes = getShortestPathBetweenCentralNodes(physicalGraph, centralGraph, centralNode1, centralNode2);
			fullPath.addAll(linksBetweenCentralNodes);
		}
		
		List<Link> path2reversed = new ArrayList<>();
		path2reversed.addAll(linksToCentralNode2);
		Collections.reverse(path2reversed);
		fullPath.addAll(path2reversed);
		
		return fullPath;
	}
	
	private List<Link> getUsedLinks(Graph graph, List<Node> shortestPath) {
		List<Link> links = new ArrayList<>();
		Iterator<Node> it = shortestPath.iterator();
		Node first = it.next();
		Node second;
		while(it.hasNext()) {
			second = it.next();
			Link link = graph.getLinkFromNodes(first, second);
			Link link2 = graph.getLinkFromNodes(second, first);
			links.add(link);
			links.add(link2);
			first = second;
		}
		return links;
	}
	
	private Graph getRoutedGraph(List<Link> usedLinks, Float weightPath) {
		Graph routedGraph = new Graph();
		Set<Node> nodeSet = new HashSet<>();
		//System.out.println("Size: " + usedLinks.size());
		for(Link link : usedLinks) {
			//System.out.println(link);
			nodeSet.add(link.getSourceNode().getCleanNodeCopy());
			nodeSet.add(link.getEndNode().getCleanNodeCopy());
			Link newLink = new Link(link.getSourceNode().getCleanNodeCopy(),
					link.getEndNode().getCleanNodeCopy(), weightPath);
			routedGraph.addLink(newLink);
		}
		
		for(Node n : nodeSet) {
			routedGraph.addNode(n);
		}
		
		return routedGraph;
	}
	
	public static Graph translateFromTopologicalGraphToGraph(TopologicalGraph tg) {		
		Graph g = new Graph();
		Iterator<TopologicalNode> itNode = tg.getNodeIterator();
		while(itNode.hasNext()) {
			TopologicalNode tn = itNode.next();
			Node n = new Node(tn.getNodeLabel());
			n.setId(tn.getNodeID());
			g.addNode(n);
		}
		
		Iterator<TopologicalLink> itLink = tg.getLinkIterator();
		while(itLink.hasNext()) {
			TopologicalLink tl = itLink.next();
			Node origin = g.getNodeById(tl.getSrcNodeID());
			Node destination = g.getNodeById(tl.getDestNodeID());
			Float weight = tl.getLinkBw();
			g.linkNodes(origin,  destination, weight);
		}
		
		return g;
	}
	
	public void test() {
		Graph g = new Graph();
		
		// 8 nodes
		Node vm0 = new Node("vm0", 0);
		Node vm1 = new Node("vm1", 1);
		Node vm2 = new Node("vm2", 2);
		Node host0 = new Node("host0", 3);
		Node host1 = new Node("host1", 4);
		Node host2 = new Node("host2", 5);
		Node switch0 = new Node("switch0", 6);
		Node switch1 = new Node("switch1", 7);
		
		// 7 double links = 14 links
		vm0.addDoubleDestination(host0, 10.0f);
		vm1.addDoubleDestination(host1, 10.0f);
		vm2.addDoubleDestination(host2, 10.0f);
		host0.addDoubleDestination(switch0, 20.0f);
		switch0.addDoubleDestination(switch1, 30.0f);
		host1.addDoubleDestination(switch0, 10.0f);
		host2.addDoubleDestination(switch1, 10.0f);

		//host2.addDoubleDestination(switch0, 50.0);
		
		g.addNode(vm0);
		g.addNode(vm1);
		g.addNode(vm2);
		g.addNode(host0);
		g.addNode(host1);
		g.addNode(host2);
		g.addNode(switch0);
		g.addNode(switch1);
		
		Graph resultingGraph = calculateShortestPathFromSource(g, vm2);

		int i = 1;
		//for(Node n : vm0.getShortestPath()) {
		//	System.out.println(i + " " + n);
		//}
		//System.out.println("Distance: " + vm0.getDistance());
	}
	
	public void test2() {
		Graph g = new Graph();
		Node switch0 = new Node("0", 0);
		Node switch1 = new Node("1", 1);
		Node host0 = new Node("2", 2);
		Node host1 = new Node("3", 3);
		Node host2 = new Node("4", 4);
		Node vm0 = new Node("5", 5);
		Node vm1 = new Node("6", 6);
		Node vm2 = new Node("7", 7);
		Node vm3 = new Node("8", 8);
		Node vm4 = new Node("9", 9);
		Node vm5 = new Node("10", 10);
		g.addNode(switch0);
		g.addNode(switch1);
		g.addNode(host0);
		g.addNode(host1);
		g.addNode(host2);
		g.addNode(vm0);
		g.addNode(vm1);
		g.addNode(vm2);
		g.addNode(vm3);
		g.addNode(vm4);
		g.addNode(vm5);
		
		g.linkNodes(vm0, host0, 1.0f);
		g.linkNodes(vm1, host0, 1.0f);
		g.linkNodes(vm2, host0, 1.0f);
		g.linkNodes(vm3, host0, 1.0f);
		g.linkNodes(vm4, host1, 1.0f);
		g.linkNodes(vm5, host2, 1.0f);
		g.linkNodes(host0, switch0, 1.0f);
		g.linkNodes(host1, switch0, 1.0f);
		g.linkNodes(host2, switch1, 1.0f);
		g.linkNodes(switch0, switch1, 1.0f);
		
		String interactionsFile = Paths.get("C:\\Users\\Sergi\\Desktop\\Desktop\\cloudsim\\target\\classes\\workload\\network\\custom\\network.txt").toString();
		SimulatedInteractionsManager sim = new SimulatedInteractionsManager(interactionsFile);
		sim.readNextSimulatedInteractions();
		Graph logicalGraph = sim.getCurrentLogicalGraph();
		System.out.println(g);
		System.out.println(logicalGraph);
		Graph resultGraph = getAgregatedBandWidthMap(g, logicalGraph);
	}
	
	public void test3() {
		Graph g = new Graph();
		Node host0 = new Node("2", 2);
		Node vm0 = new Node("5", 5);
		Node vm1 = new Node("6", 6);
		Node vm2 = new Node("7", 7);
		Node vm3 = new Node("8", 8);
		g.addNode(host0);
		g.addNode(vm0);
		g.addNode(vm1);
		g.addNode(vm2);
		g.addNode(vm3);
		
		g.linkNodes(vm0, host0, 1.0f);
		g.linkNodes(vm1, host0, 1.0f);
		g.linkNodes(vm2, host0, 1.0f);
		g.linkNodes(vm3, host0, 1.0f);
		
		String interactionsFile = Paths.get("C:\\Users\\Sergi\\Desktop\\cloudsim\\target\\classes\\workload\\network\\custom\\network.txt").toString();
		SimulatedInteractionsManager sim = new SimulatedInteractionsManager(interactionsFile);
		sim.readNextSimulatedInteractions();
		Graph logicalGraph = sim.getCurrentLogicalGraph();
		System.out.println(g);
		System.out.println(logicalGraph);
		//Graph resultGraph = getAgregatedBandWidthMap(g, logicalGraph);
		
		calculateShortestPathFromSource(g, vm0);
		vm1.getShortestPathCopy();
	}
	
	public void test4() {
		Graph g = new Graph();
		Node n0 = new Node("0", 0);
		Node n1 = new Node("1", 1);
		Node n2 = new Node("2", 2);
		Node n3 = new Node("3", 3);
		Node n4 = new Node("4", 4);
		Node n5 = new Node("5", 5);
		Node n6 = new Node("6", 6);
		Node n7 = new Node("7", 7);
		Node n8 = new Node("8", 8);
		Node n9 = new Node("9", 9);
		g.addNode(n0);
		g.addNode(n1);
		g.addNode(n2);
		g.addNode(n3);
		g.addNode(n4);
		g.addNode(n5);
		g.addNode(n6);
		g.addNode(n7);
		g.addNode(n8);
		g.addNode(n9);
		
		g.linkNodes(n0, n3, 1.0f);
		g.linkNodes(n1, n3, 1.0f);
		g.linkNodes(n2, n3, 1.0f);
		g.linkNodes(n0, n5, 1.0f);
		g.linkNodes(n3, n4, 1.0f);
		g.linkNodes(n4, n5, 1.0f);
		g.linkNodes(n4, n6, 1.0f);
		g.linkNodes(n4, n9, 1.0f);
		g.linkNodes(n5, n6, 1.0f);
		g.linkNodes(n5, n7, 1.0f);
		g.linkNodes(n4, n8, 1.0f);
		g.linkNodes(n7, n8, 1.0f);
		
		calculateShortestPathFromSource(g, n0);
		List<Node> path = n8.getShortestPathCopy();
		for(Node n : path) {
			System.out.println(n.getId());
		}
		resetDistances(g);
		System.out.println();
		calculateShortestPathFromSource(g, n9);
		path = n1.getShortestPathCopy();
		for(Node n : path) {
			System.out.println(n.getId());
		}
	}
	
	public void test5() {
		Graph g = new Graph();
		Node node00 = new Node(0); Node node01 = new Node(1); Node node02 = new Node(2);
		Node node10 = new Node(3); Node node11 = new Node(4); Node node12 = new Node(5);
		Node node20 = new Node(6); Node node21 = new Node(7); Node node22 = new Node(8);
		
		g.linkNodes(node00, node01, 1.0f);
		g.linkNodes(node01, node02, 0.0f);
		g.linkNodes(node10, node11, 0.0f);
		g.linkNodes(node11, node12, 0.0f);
		g.linkNodes(node20, node21, 0.0f);
		g.linkNodes(node21, node22, 0.0f);
		
		g.linkNodes(node00, node10, 0.0f);
		g.linkNodes(node10, node20, 0.0f);
		g.linkNodes(node01, node11, 0.0f);
		g.linkNodes(node11, node21, 1.0f);
		g.linkNodes(node02, node12, 0.0f);
		g.linkNodes(node12, node22, 0.0f);

		calculateShortestPathFromSource(g, node00);
		List<Node> path = node01.getShortestPathCopy();
		for(Node n : path) {
			System.out.println(n.getId());
		}
	}
	
	private static Node getLowestDistanceNode(Set < Node > unsettledNodes) {
	    Node lowestDistanceNode = null;
	    Float lowestDistance = Float.MAX_VALUE;
	    for (Node node: unsettledNodes) {
	    	Float nodeDistance = node.getDistance();
	        if (nodeDistance < lowestDistance) {
	            lowestDistance = nodeDistance;
	            lowestDistanceNode = node;
	        }
	    }
	    return lowestDistanceNode;
	}

	private static void calculateMinimumDistance(Node evaluationNode,
		Float edgeWeight, Node sourceNode) {
		Float sourceDistance = sourceNode.getDistance();
		if (sourceDistance + edgeWeight < evaluationNode.getDistance()) {
			evaluationNode.setDistance(sourceDistance + edgeWeight);
			LinkedList<Node> shortestPath = new LinkedList<>(sourceNode.getShortestPathCopy());
			shortestPath.add(sourceNode);
			evaluationNode.setShortestPath(shortestPath);
		}
	}
}
