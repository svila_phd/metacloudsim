package svila.planetlabNetwork;

import org.cloudbus.cloudsim.util.MathUtil;

public class TestMathUtil {
	public static void main(String[] args) {
		double[] data = new double[] {10,
				21,
				21,
				32,
				33,
				37,
				48,
				48,
				53,
				68,
				70,
				77
};
		System.out.println(MathUtil.iqr(data));
	}
}
