package svila.planetlabNetwork;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.tuple.Pair;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.json.JSONArray;
import org.json.JSONObject;

import svila.planetlabNetwork.Link.LinkType;

public class Snapshot {

	private double snapshotTime;
	private int snapshotNumber;
	private boolean migrationSnapshot;
	private Map<Integer, PowerHostData> powerHostDataMap;
	private Graph physicalNetwork;
	private Graph logicalNetwork;
	private Graph aggregatedBWGraph;
	private Graph remainingBWGraph;
	private PathsGraph pathsGraph;
	private Map<Vm, Host> migrationMap;
	private Map<Vm, Pair<PowerHost, PowerHost>> enhancedMigrationMap;
	private Snapshot lastSnapshot;
	
	public Snapshot(
			double snapshotTime,
			int snapshotNumber,
			boolean migrationSnapshot,
			List<PowerHost> hostList,
			Graph baseNetwork,
			Graph logicalNetwork,
			Snapshot lastSnapshot) {
		this.snapshotTime = snapshotTime;
		this.snapshotNumber = snapshotNumber;
		this.migrationSnapshot = migrationSnapshot;
		this.powerHostDataMap = new HashMap<>();
		this.lastSnapshot = lastSnapshot;
		for(PowerHost host : hostList) {
			this.powerHostDataMap.put(host.getId(), new PowerHostData(host));
		}
		//System.out.println(this.resume());
		
		this.logicalNetwork = logicalNetwork;
		
		if(this.migrationSnapshot || lastSnapshot == null) {
			this.physicalNetwork = recreatePhysicalNetwork(baseNetwork);		
			this.pathsGraph = createAgregatedBWGraph();
		} else {
			this.physicalNetwork = lastSnapshot.getPhysicalNetwork().copy();
			this.pathsGraph = lastSnapshot.getPathsGraph().getCopyUsingLogicalGraph(logicalNetwork);
		}
		this.aggregatedBWGraph = this.pathsGraph.getGraph();
		this.remainingBWGraph = createRemainingBWGraph();
		this.migrationMap = new HashMap<>();
		this.enhancedMigrationMap = new HashMap<>();
	}
	
	public void setMigrationMap(List<Map<String, Object>> migrationMap) {
		for(Map<String, Object> map : migrationMap) {
			Vm vm = (Vm) map.get("vm");
			PowerHost targetHost = (PowerHost) map.get("host");
			PowerHost oldHost = (PowerHost) vm.getHost();
			this.migrationMap.put(vm, targetHost);
			this.enhancedMigrationMap.put(vm, Pair.of(oldHost, targetHost));
		}
	}
	
	public Graph getAggregatedBWGraph() {
		return aggregatedBWGraph;
	}

	public void setAggregatedBWGraph(Graph aggregatedBWGraph) {
		this.aggregatedBWGraph = aggregatedBWGraph;
	}

	public Graph getRemainingBWGraph() {
		return remainingBWGraph;
	}

	public void setRemainingBWGraph(Graph remainingBWGraph) {
		this.remainingBWGraph = remainingBWGraph;
	}

	public PathsGraph getPathsGraph() {
		return pathsGraph;
	}

	public void setPathsGraph(PathsGraph pathsGraph) {
		this.pathsGraph = pathsGraph;
	}

	public Snapshot getLastSnapshot() {
		return lastSnapshot;
	}

	public void setLastSnapshot(Snapshot lastSnapshot) {
		this.lastSnapshot = lastSnapshot;
	}

	public Map<Vm, Host> getMigrationMap() {
		return migrationMap;
	}
	
	public boolean isMigrationSnapshot() {
		return migrationSnapshot;
	}

	public void setMigrationSnapshot(boolean migrationSnapshot) {
		this.migrationSnapshot = migrationSnapshot;
	}

	private Graph recreatePhysicalNetwork(Graph baseNetwork) {
		boolean networkEnabled = StaticResources.getCE().isNetwork();
		if(!networkEnabled) {
			return new Graph();
		} 
		Graph currentPhysicalNetwork = baseNetwork.copy();
		
		for(PowerHostData phd : this.powerHostDataMap.values()) {
			int networkHostId = phd.getHost().getNetworkId();
			for(PowerVmData pvd : phd.getVmsData().values()) {
				int networkVmId = pvd.getVm().getNetworkId();
				Node hostNode = currentPhysicalNetwork.getNodeById(networkHostId);
				Node vmNode = currentPhysicalNetwork.getNodeById(networkVmId);
				currentPhysicalNetwork.linkNodes(hostNode, vmNode, phd.getHost().getHostVmBandwith(), LinkType.hostVm);
			}
		}
		
		return currentPhysicalNetwork;
	}
	
	private Graph createRemainingBWGraph() {
		return this.physicalNetwork.generateSubstractionGraph(aggregatedBWGraph);
	}
	
	private Graph calculateCombinedRoutes() {
		ShortestPathCalculator spc = new ShortestPathCalculator();
		return spc.getAgregatedBandWidthMap(physicalNetwork, logicalNetwork);
	}
	
	private PathsGraph createAgregatedBWGraph() {
		boolean networkEnabled = StaticResources.getCE().isNetwork();
		if(!networkEnabled) {
			return new PathsGraph();
		} 
		ShortestPathCalculator spc = new ShortestPathCalculator();
		return spc.getAgregatedBandWidthMap2(physicalNetwork, logicalNetwork);
	}
	
	private PathsGraph createAgregatedBWGraphFast() {
		// En procés
		// Obtenir l'últim snapshot
		Snapshot lastSnapshot = this.lastSnapshot;
		if(lastSnapshot == null) {
			ShortestPathCalculator spc = new ShortestPathCalculator();
			return spc.getAgregatedBandWidthMap2(physicalNetwork, logicalNetwork);
		}
		
		// Obtenir les interaccions que han canviat
		// De les que no han canviat, crear els mateixos camins
		// De les que han canviat, llençar Dijkstra de nou
		
		ShortestPathCalculator spc = new ShortestPathCalculator();
		return spc.getAgregatedBandWidthMap2(physicalNetwork, logicalNetwork);
	}
	
	public double getSnapshotTime() {
		return snapshotTime;
	}

	public void setSnapshotTime(double snapshotTime) {
		this.snapshotTime = snapshotTime;
	}

	public int getSnapshotNumber() {
		return snapshotNumber;
	}

	public void setSnapshotNumber(int snapshotNumber) {
		this.snapshotNumber = snapshotNumber;
	}

	public Map<Integer, PowerHostData> getPowerHostDataMap() {
		return powerHostDataMap;
	}

	public void setPowerHostDataMap(Map<Integer, PowerHostData> powerHostDataMap) {
		this.powerHostDataMap = powerHostDataMap;
	}

	public Graph getPhysicalNetwork() {
		return physicalNetwork;
	}

	public void setPhysicalNetwork(Graph physicalNetwork) {
		this.physicalNetwork = physicalNetwork;
	}

	public Graph getLogicalNetwork() {
		return logicalNetwork;
	}

	public void setLogicalNetwork(Graph logicalNetwork) {
		this.logicalNetwork = logicalNetwork;
	}

	public Map<Vm, Pair<PowerHost, PowerHost>> getEnhancedMigrationMap() {
		return enhancedMigrationMap;
	}

	public void setEnhancedMigrationMap(Map<Vm, Pair<PowerHost, PowerHost>> enhancedMigrationMap) {
		this.enhancedMigrationMap = enhancedMigrationMap;
	}

	public String info() {
		String s = "";
		for(PowerHostData phd : powerHostDataMap.values()) {
			s += phd.toString() + "\n";
		}
		return s;
	}
	
	public String resume() {
		String s = "";
		for(PowerHostData phd : powerHostDataMap.values()) {
			s += phd.resume() + "\n";
		}
		return s;
	}
	
	public String toString() {
		String s = "\n-----SNAPSHOT " + snapshotNumber + " (" + snapshotTime + ")" + "----- Migrations: " + migrationSnapshot + "\n";
		//s += info();
		s += resume();
		s += "-----END SNAPSHOT-----\n";
		return s;
	}
	
	public JSONObject toJSONObject() {
		JSONObject json = new JSONObject();
		
		json.put("snapshotTime", snapshotTime);
		json.put("snapshotNumber", snapshotNumber);
		json.put("migrationSnapshot", migrationSnapshot);
		JSONArray phdArray = new JSONArray();
		for(PowerHostData phd : powerHostDataMap.values()) {
			phdArray.put(phd.toJSON());
		}
		json.put("powerHostData", phdArray);
		/*JSONArray migrationArray = new JSONArray();
		for(Entry<Vm, Host> entry : migrationMap.entrySet()) {
			JSONObject migrationObject = new JSONObject();
			migrationObject.put("vmId", entry.getKey().getId());
			migrationObject.put("vmNetworkId", entry.getKey().getNetworkId());
			migrationObject.put("hostId", entry.getValue().getId());
			migrationObject.put("hostNetworkId", entry.getValue().getNetworkId());
			migrationArray.put(migrationObject);
		}*/
		JSONArray migrationsArray = new JSONArray();
		json.put("migrations", migrationsArray);
		for(Entry<Vm, Pair<PowerHost, PowerHost>> entry : this.enhancedMigrationMap.entrySet()) {
			JSONObject currentMigration = new JSONObject();
			currentMigration.put("vmId", entry.getKey().getId());
			currentMigration.put("vmNetId", entry.getKey().getNetworkId());
			currentMigration.put("oldHostId", entry.getValue().getLeft().getId());
			currentMigration.put("oldHostNetId", entry.getValue().getLeft().getNetworkId());
			currentMigration.put("newHostId", entry.getValue().getRight().getId());
			currentMigration.put("newHostNetId", entry.getValue().getRight().getNetworkId());
		}
		
		return json;
	}
}
