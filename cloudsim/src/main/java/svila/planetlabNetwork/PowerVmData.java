package svila.planetlabNetwork;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.power.PowerVm;

public class PowerVmData {

	private PowerVm vm;
	private Double requestedMips;
	
	public PowerVmData(PowerVm vm) {
		this.vm = vm;
		// Get last % utilization
		List<Double> utilizationHistory = vm.getUtilizationHistory();
		Double percentageUtilization = utilizationHistory.get(0);
		// Get requested MIPS
		Double mipsUtilization = percentageUtilization * vm.getMips();
		this.requestedMips = mipsUtilization;
	}

	public PowerVm getVm() {
		return vm;
	}

	public Double getRequestedMips() {
		return requestedMips;
	}
	
	public String toString() {
		DecimalFormat df = new DecimalFormat("#0.00"); 
		return "VM " + vm.getId() + " MIPS: " + df.format(requestedMips);
	}
}
