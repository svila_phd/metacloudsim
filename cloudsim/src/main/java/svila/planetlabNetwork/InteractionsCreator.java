package svila.planetlabNetwork;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.cloudbus.cloudsim.Vm;

public class InteractionsCreator {
	
	List<Interaction> interactions;
	Graph fullGraph;
	List<Vm> vms;
	List<String> interactionsFolders;
	List<Integer> distributions;
	Map<Integer, List<Integer>> vmHostMap;
	List<Pair<Integer, Integer>> logicPairLinks;
	long randomSeed;
	float linkWeight;
	float internalClusterRate;
	float externalClusterRate;
	double schedulingInterval = 300.0;
	Random r;

	public static void main(String[] args) {
		
	}
	
	public InteractionsCreator(Graph fullGraph,
								List<Vm> vms,
								List<String> interactionsFolders,
								List<Integer> distributions,
								long randomSeed,
								float linkWeight,
								float internalClusterRate,
								float externalClusterRate) {
		this.fullGraph = fullGraph;
		this.vms = vms;
		this.interactionsFolders = interactionsFolders;
		this.distributions = distributions;
		this.randomSeed = randomSeed < 0 ? System.currentTimeMillis() : randomSeed;
		this.linkWeight = linkWeight;
		this.internalClusterRate = internalClusterRate;
		this.externalClusterRate = externalClusterRate;
		this.interactions = new ArrayList<>();
		r = new Random(this.randomSeed);
	}
	
	public void generateInteractions() {
		generateLogicInteractions();
		generateInteractionsWithDynamicLoad();
	}
	
	public void generateLogicInteractions() {
		obtainClusterVms();
		logicPairLinks = new ArrayList<>();
		
		boolean networkEnabled = StaticResources.getCE().isNetwork();
		if(!networkEnabled) {
			return;
		}
		
		createInternalLinks(internalClusterRate);
		createExternalLinks(externalClusterRate);
	}
	
	public void generateInteractionsWithDynamicLoad() {
		int numInteractions = logicPairLinks.size();
		Iterator<Pair<Integer, Integer>> pairs = logicPairLinks.iterator();
		float totalDistributions = sumDistributions();
		for(int i=0; i<interactionsFolders.size()-1; i++) {
			int currentNumInteractions = (int)((distributions.get(i) / totalDistributions) * numInteractions);
			addInteractions(interactionsFolders.get(i), currentNumInteractions, pairs);
		}
		
		addInteractions(interactionsFolders.get(
				interactionsFolders.size()-1),
				numInteractions-interactions.size(), pairs);
	}
	
	private void addInteractions(String folder, int num, Iterator<Pair<Integer, Integer>> pairIterator) {
		File currentFolder = new File(folder);
		File[] files = currentFolder.listFiles();
		
		Arrays.sort(files);
		
		for(int i=0; i<num; i++) {
			Pair<Integer, Integer> currentPair = pairIterator.next();
			File currentFile = files[i];
			//System.out.println("Interaction " + i + " " + files[i].getAbsolutePath());
			Interaction interaction = new Interaction(currentPair.getLeft(),
													currentPair.getRight(),
													linkWeight,
													currentFile.toString(),
													schedulingInterval);
			interactions.add(interaction);
		}
	}
	
	public List<Interaction> getInteractions() {
		return this.interactions;
	}
	
	private int sumDistributions() {
		int sum = 0;
		
		for(int i=0; i<distributions.size(); i++) {
			sum += distributions.get(i);
		}
		
		return sum;
	}
	
	private void createClusteredLinks(List<Integer> groupOfNodeIds, float ratio) {
		//Random r = new Random(this.randomSeed);

		List<Pair<Integer, Integer>> possiblePairs = new LinkedList<Pair<Integer, Integer>>();
		Set<Pair<Integer, Integer>> appliedPairs = new HashSet<Pair<Integer, Integer>>();
		
		for(int i=0; i<groupOfNodeIds.size(); i++) {
			for(int j=i+1; j<groupOfNodeIds.size(); j++) {
				possiblePairs.add(new ImmutablePair<Integer, Integer>(
						groupOfNodeIds.get(i), groupOfNodeIds.get(j)));
			}
		}
		
		float totalPossiblePairs = groupOfNodeIds.size() * (groupOfNodeIds.size()-1) / 2;
		
		while(appliedPairs.size() / totalPossiblePairs < ratio) {
			int randomIndex = r.nextInt(possiblePairs.size());
			Pair<Integer, Integer> randomPair = possiblePairs.get(randomIndex);
			appliedPairs.add(randomPair);
			possiblePairs.remove(randomPair);
			totalPossiblePairs = groupOfNodeIds.size() * (groupOfNodeIds.size()-1) / 2;
		}
		
		logicPairLinks.addAll(appliedPairs);
	}
	
	private void createInternalLinks(float internalClusterRate) {
		for(List<Integer> groupOfNodeIds : vmHostMap.values()) {
			createClusteredLinks(groupOfNodeIds, internalClusterRate);
		}
	}
	
	private void createExternalLinks(float externalClusterRate) {
		
		List<Pair<Integer, Integer>> possiblePairs = new LinkedList<Pair<Integer, Integer>>();
		Set<Pair<Integer, Integer>> appliedPairs = new HashSet<Pair<Integer, Integer>>();
				
		List<List<Integer>> groupOfNodeIds = new ArrayList<>();
		groupOfNodeIds.addAll(vmHostMap.values());

		for(int i=0; i<groupOfNodeIds.size(); i++) {
			List<Integer> currentGroup = groupOfNodeIds.get(i);
			for(int ii=0; ii<currentGroup.size(); ii++) {
				int currentNodeId = currentGroup.get(ii);
				for(int j=i+1; j<groupOfNodeIds.size(); j++) {
					List<Integer> otherGroup = groupOfNodeIds.get(j);
					for(int jj=0; jj<otherGroup.size(); jj++) {
						int otherNodeId = otherGroup.get(jj);
						possiblePairs.add(new ImmutablePair<Integer, Integer>(currentNodeId, otherNodeId));
					}
				}
			}
		}
		
		float totalPossiblePairs = possiblePairs.size();
		
		while(((float)appliedPairs.size()) / totalPossiblePairs < externalClusterRate) {
			int randomIndex = r.nextInt(possiblePairs.size());
			Pair<Integer, Integer> randomPair = possiblePairs.get(randomIndex);
			appliedPairs.add(randomPair);
			possiblePairs.remove(randomPair);
		}

		logicPairLinks.addAll(appliedPairs);
	}
	
	private void obtainClusterVms() {
		vmHostMap = new LinkedHashMap<>();
		for(Node n : getVmNodes()) {
			int idParent = n.getAdjacentNodes().keySet().iterator().next().getId();
			if(!vmHostMap.containsKey(idParent)) {
				vmHostMap.put(idParent, new ArrayList<>());
			}
			vmHostMap.get(idParent).add(n.getId());
		}
	}
	
	private List<Node> getVmNodes() {
		List<Node> leafNodes = new ArrayList<Node>();
		for(Node n : fullGraph.getNodes()) {
			if(n.getType().equals(Node.TYPE.VM)) {
				leafNodes.add(n);
			}
		}
		Collections.sort(leafNodes, new Comparator<Node>() {
			  @Override
			  public int compare(Node n1, Node n2) {
				  if(n1.getId() == n2.getId()) {
					  return 0;  
				  } else if(n1.getId() > n2.getId()) {  
					  return 1;  
				  } else {
					  return -1;  
				  }
			  }
			});
		return leafNodes;
	}
}
