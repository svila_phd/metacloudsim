package svila.planetlabNetwork;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import svila.planetlabNetwork.Node.TYPE;

public class HostDistancesTest {
	public static void main(String[] args) {		
		//test1();
		//test2();
		
		ShortestPathCalculator spc = new ShortestPathCalculator();
		spc.test2();
	}
	
	public static void test1() {
		Graph g = new Graph();
		
		Node n0 = new Node(0);
		Node n1 = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		Node n4 = new Node(4);
		Node n5 = new Node(5);
		Node n6 = new Node(6);

		g.addNode(n0);
		g.addNode(n1);
		g.addNode(n2);
		g.addNode(n3);
		g.addNode(n4);
		g.addNode(n5);
		g.addNode(n6);
	
		n0.setType(TYPE.LEAF);
		n2.setType(TYPE.LEAF);
		n6.setType(TYPE.LEAF);

		g.linkNodes(0, 1, 1);
		g.linkNodes(0, 4, 1);
		g.linkNodes(1, 2, 1);
		g.linkNodes(1, 3, 1);
		g.linkNodes(2, 4, 1);
		g.linkNodes(3, 4, 1);
		g.linkNodes(2, 5, 1);
		g.linkNodes(5, 6, 1);
		
		TopologyHostDistanceManager.loadPhysicalGraph(g);
		System.out.println(g.toString());
		TopologyHostDistanceManager.calculateDistances();
		TreeMap<Integer, List<Integer>> groupedNodes = TopologyHostDistanceManager.getLeafNodeIdsGroupedByDistance(0);
		groupedNodes.keySet();
	}
	
	public static void test2() {
		Graph g = new Graph();
		
		Node n0 = new Node(0);
		Node n1 = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		Node n4 = new Node(4);
		Node n5 = new Node(5);
		Node n6 = new Node(6);
		Node n7 = new Node(7);
		Node n8 = new Node(8);
		Node n9 = new Node(9);
		Node n10 = new Node(10);
		Node n11 = new Node(11);
		Node n12 = new Node(12);
		Node n13 = new Node(13);
		Node n14 = new Node(14);
		Node n15 = new Node(15);
		Node n16 = new Node(16);
		Node n17 = new Node(17);
		Node n18 = new Node(18);

		g.addNode(n0);
		g.addNode(n1);
		g.addNode(n2);
		g.addNode(n3);
		g.addNode(n4);
		g.addNode(n5);
		g.addNode(n6);
		g.addNode(n7);
		g.addNode(n8);
		g.addNode(n9);
		g.addNode(n10);
		g.addNode(n11);
		g.addNode(n12);
		g.addNode(n13);
		g.addNode(n14);
		g.addNode(n15);
		g.addNode(n16);
		g.addNode(n17);
		g.addNode(n18);
	
		n5.setType(TYPE.LEAF);
		n6.setType(TYPE.LEAF);
		n8.setType(TYPE.LEAF);
		n9.setType(TYPE.LEAF);
		n11.setType(TYPE.LEAF);
		n12.setType(TYPE.LEAF);
		n14.setType(TYPE.LEAF);
		n15.setType(TYPE.LEAF);
		n17.setType(TYPE.LEAF);
		n18.setType(TYPE.LEAF);
		
		g.linkNodes(0, 1, 1);
		g.linkNodes(0, 4, 1);
		g.linkNodes(1, 2, 1);
		g.linkNodes(1, 3, 1);
		g.linkNodes(1, 7, 1);
		g.linkNodes(1, 10, 1);
		g.linkNodes(2, 3, 1);
		g.linkNodes(2, 16, 1);
		g.linkNodes(3, 13, 1);
		g.linkNodes(4, 5, 1);
		g.linkNodes(4, 6, 1);
		g.linkNodes(7, 8, 1);
		g.linkNodes(7, 9, 1);
		g.linkNodes(10, 11, 1);
		g.linkNodes(10, 12, 1);
		g.linkNodes(13, 14, 1);
		g.linkNodes(13, 15, 1);
		g.linkNodes(16, 17, 1);
		g.linkNodes(16, 18, 1);

		
		TopologyHostDistanceManager.loadPhysicalGraph(g);
		System.out.println(g.toString());
		TopologyHostDistanceManager.calculateDistances();
		Map<Integer, TreeMap<Integer, List<Integer>>> groupedDistances = TopologyHostDistanceManager.getAllGroupedDistances();
		System.out.println();
	}
}
