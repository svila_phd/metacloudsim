package svila.planetlabNetwork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.UtilizationModelPlanetLabInMemory;
import org.cloudbus.cloudsim.Vm;

import svila.SvilaPrinter;

public class VMLoadBalancer {
	
	public static List<Vm> vmList;
	public static List<Vm> allVmList;
	public static List<Cloudlet> cloudletList;
	public static List<Cloudlet> allCloudletList;
	static int vmIncrement;
	static DatacenterBroker broker;
	static int start;
	static int end;
	public static Map<Integer, Double> vmMapTime;
	
	public static void init(DatacenterBroker b, List<Vm> list, List<Cloudlet> clist, int increment) {
		broker = b;
		vmList = list;
		cloudletList = clist;
		allVmList = new ArrayList<>(list);
		allCloudletList = new ArrayList<>(clist);
		vmMapTime = new HashMap<>();
		vmIncrement = increment;
		start = 0;
		end = Math.min(allCloudletList.size(), vmIncrement);
		
//		for(Cloudlet c : allCloudletList.subList(allCloudletList.size()/2, allCloudletList.size())) {
//			c.setExecStartTime(10000.0);
//		}
		
//		for(Vm vm : list.subList(0, list.size()/2)) {
//			vmMapTime.put(vm.getId(), 0.0);
//		}
//		
//		for(Vm vm : list.subList(list.size()/2, list.size())) {
//			vmMapTime.put(vm.getId(), 0.0);
//		}
	}
	
	public static void modifyCloudlets(List<Cloudlet> clist) {
		for(Cloudlet c : clist.subList(2, 4)) {
			UtilizationModelPlanetLabInMemory utilizationModel = (UtilizationModelPlanetLabInMemory) c.getUtilizationModelCpu();
			for(int j=0; j<6; j++) {
				utilizationModel.load[j] = 0.0;
			}
		}
	}
	
	public static List<Vm> getNextVms() {
		return start == allVmList.size() ?
				new ArrayList<>() : 
					allVmList.subList(start, end);
	}
	
	public static List<Cloudlet> getNextCloudlets() {
		return start == allCloudletList.size() ?
				new ArrayList<>() : 
				allCloudletList.subList(start, end);
	}
	
	public static void loadNext() {
		start = end;
		end = end + Math.min(allCloudletList.size()-end, vmIncrement);
		SvilaPrinter.print("VMLoadBalancer:" + start + " - " + end);
	}
	
	public static void startVms() {
		for(int i=0; i<vmList.size()-1; i++) {
			vmList.remove(vmList.size()-1);
			cloudletList.remove(cloudletList.size()-1);
		}
	}
	
	public static void addVms() {
		int fixedVmIncrement = Math.min(allVmList.size()-vmList.size(), vmIncrement);
		for(int i=0; i<fixedVmIncrement; i++) {
			vmList.add(allVmList.get(vmList.size()));
			cloudletList.add(allCloudletList.get(cloudletList.size()));
		}
		System.out.println("vmList: " + vmList.size());
		System.out.println("allVmList: " + allVmList.size());
		System.out.println("cloudletList: " + cloudletList.size());
		System.out.println("allCloudletList: " + allCloudletList.size());
	}
}
