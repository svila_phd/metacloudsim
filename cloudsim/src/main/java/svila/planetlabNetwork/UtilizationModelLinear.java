package svila.planetlabNetwork;

import org.cloudbus.cloudsim.UtilizationModel;

public class UtilizationModelLinear implements UtilizationModel {

	double minValue = 0.1;
	double maxValue = 0.9;
	double minTime = 0;
	double maxTime = 86400;
	
	@Override
	public double getUtilization(double time) {
		return (((time - minTime) * (maxValue - minValue)) / (maxTime - minTime)) + minValue;
	}

}
