package svila.planetlabNetwork.generators;

import java.util.Random;

public class RandomDistribution extends DistributionGenerator {
	public RandomDistribution(int length, long randomSeed, int minValue, int maxValue) {
		super(length, randomSeed, minValue, maxValue);
	}
	
	public int[] obtainValues() {
		Random r = new Random(getRandomSeed());
		int[] values = r.ints(length, minValue, maxValue+1).toArray();
		return values;
	}
}
