package svila.planetlabNetwork.generators;

public abstract class DistributionGenerator {
	int length;
	long randomSeed;
	int minValue;
	int maxValue;
	
	public DistributionGenerator(int length, long randomSeed, int minValue, int maxValue) {
		this.length = length;
		this.randomSeed = randomSeed;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
		
	public long getRandomSeed() {
		if(randomSeed < 0) {
			return System.currentTimeMillis();
		}
			
		return randomSeed;
	}
		
	public abstract int[] obtainValues();
}
