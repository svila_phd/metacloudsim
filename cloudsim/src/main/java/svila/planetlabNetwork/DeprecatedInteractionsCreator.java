package svila.planetlabNetwork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class DeprecatedInteractionsCreator {
	
	private List<Node> leafNodes;
	private Map<Integer, List<Integer>> leafsSwitchMap;
	private List<Pair<Integer, Integer>> logicPairLinks;
	
	public DeprecatedInteractionsCreator(List<Node> leafNodes) {
		this.leafNodes = leafNodes;
		this.leafsSwitchMap = new HashMap<>();
		this.logicPairLinks = new ArrayList<>();
	}
	
	public void createInteractions(float internalClusterRate, float externalClusterRate) {
		obtainClusterLeafs();
		createInternalLinks(internalClusterRate);
		createExternalLinks(externalClusterRate);
	}
	
	private void createClusteredLinks(List<Integer> groupOfNodeIds, float ratio) {
		Random r = new Random(System.currentTimeMillis());

		List<Pair<Integer, Integer>> possiblePairs = new LinkedList<Pair<Integer, Integer>>();
		Set<Pair<Integer, Integer>> appliedPairs = new HashSet<Pair<Integer, Integer>>();
		
		for(int i=0; i<groupOfNodeIds.size(); i++) {
			for(int j=i+1; j<groupOfNodeIds.size(); j++) {
				possiblePairs.add(new ImmutablePair<Integer, Integer>(
						groupOfNodeIds.get(i), groupOfNodeIds.get(j)));
			}
		}
		
		float totalPossiblePairs = groupOfNodeIds.size() * (groupOfNodeIds.size()-1) / 2;
		
		while(appliedPairs.size() / totalPossiblePairs < ratio) {
			int randomIndex = r.nextInt(possiblePairs.size());
			Pair<Integer, Integer> randomPair = possiblePairs.get(randomIndex);
			appliedPairs.add(randomPair);
			possiblePairs.remove(randomPair);
			totalPossiblePairs = groupOfNodeIds.size() * (groupOfNodeIds.size()-1) / 2;
		}
		
		logicPairLinks.addAll(appliedPairs);
	}
	
	private void createInternalLinks(float internalClusterRate) {
		for(List<Integer> groupOfNodeIds : leafsSwitchMap.values()) {
			createClusteredLinks(groupOfNodeIds, internalClusterRate);
		}
	}
	
	private void createExternalLinks(float externalClusterRate) {
		Random r = new Random(System.currentTimeMillis());
		List<Pair<Integer, Integer>> possiblePairs = new LinkedList<Pair<Integer, Integer>>();
		Set<Pair<Integer, Integer>> appliedPairs = new HashSet<Pair<Integer, Integer>>();
				
		List<List<Integer>> groupOfNodeIds = new ArrayList<>();
		groupOfNodeIds.addAll(leafsSwitchMap.values());

		for(int i=0; i<groupOfNodeIds.size(); i++) {
			List<Integer> currentGroup = groupOfNodeIds.get(i);
			for(int ii=0; ii<currentGroup.size(); ii++) {
				int currentNodeId = currentGroup.get(ii);
				for(int j=i+1; j<groupOfNodeIds.size(); j++) {
					List<Integer> otherGroup = groupOfNodeIds.get(j);
					for(int jj=0; jj<otherGroup.size(); jj++) {
						int otherNodeId = otherGroup.get(jj);
						possiblePairs.add(new ImmutablePair<Integer, Integer>(currentNodeId, otherNodeId));
					}
				}
			}
		}
		
		float totalPossiblePairs = possiblePairs.size();
		
		while(appliedPairs.size() / totalPossiblePairs < externalClusterRate) {
			int randomIndex = r.nextInt(possiblePairs.size());
			Pair<Integer, Integer> randomPair = possiblePairs.get(randomIndex);
			appliedPairs.add(randomPair);
			possiblePairs.remove(randomPair);
			totalPossiblePairs = groupOfNodeIds.size() * (groupOfNodeIds.size()-1) / 2;
		}

		logicPairLinks.addAll(appliedPairs);
	}
	
	private void obtainClusterLeafs() {
		for(Node n : leafNodes) {
			int idParent = n.getAdjacentNodes().keySet().iterator().next().getId();
			if(!leafsSwitchMap.containsKey(idParent)) {
				leafsSwitchMap.put(idParent, new ArrayList<>());
			}
			leafsSwitchMap.get(idParent).add(n.getId());
		}
	}
	
	public List<Pair<Integer, Integer>> getPairLinks() {
		return logicPairLinks;
	}
}
