package svila.planetlabNetwork;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

public class PowerVmSelectionPolicyNull extends PowerVmSelectionPolicy {

	@Override
	public Vm getVmToMigrate(PowerHost host) {
		return null;
	}

}
