package svila.planetlabNetwork;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import org.json.JSONException;
import org.json.JSONObject;

public class StaticResources {
	static boolean initialized = false;
	static JSONObject launcherObject;
	static ExperimentConfiguration currentEC;
	public static String launcherFilename = "launcher.json";
	public static String interactiveFilename = "interactive.json";
	public static String baseFolder;
	public static String jpyLib, jdlLib, pymodule, decisionLogger;
	
	public final static String experimentsFolderName = "experiments";
	public final static String workloadsFolderName = "workloads";
	public final static String topologiesFolderName = "topologies";
	public final static String hostsFolderName = "hosts";
	public final static String vmsFolderName = "vms";
	public final static String interactionsFolderName = "interactions";
	public final static String testbedFolderName = "testbed";
	public final static String generatedExperimentsFolderName = "generatedExperiments";
	public final static String rootOutputFolderName = "output";
	public final static String trafficGeneratorConfigurationsFolderName = "trafficGeneratorConfigurations";
	public final static String topologiesConfigurationsFolderName = "topologiesConfigurations";
	public final static String qsubFolderName = "qsubScripts";
	public final static String indiviudalExperimentScriptsName = "individualExperimentsScripts";
	public final static String interactiveFolderName = "interactive";
	public final static String poolFolderName = "poolExperiments";

	
	public final static int userId = 2;

	public StaticResources() {
		
	}
	
	public static void init() {
		if(!initialized) {
			loadLauncher();
			initialized = true;
		}
	}
	
	public static void loadLauncher() {
		try {
			File file = new File(launcherFilename);
			launcherObject = new JSONObject(new String(Files.readAllBytes(file.toPath()),"UTF-8"));
			System.out.println("Loaded file: " + file.getAbsolutePath());
			baseFolder = Paths.get(launcherObject.getString("baseFolder")).toString();
			jpyLib = Paths.get(launcherObject.getString("jpyLib")).toString();
			jdlLib = Paths.get(launcherObject.getString("jdlLib")).toString();
			pymodule = Paths.get(launcherObject.getString("pymodule")).toString();
			decisionLogger = Paths.get(launcherObject.getString("decisionLogger")).toString();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void purgeDirectory(File dir) {
		if(!dir.exists()) {
			return;
		}

	    for (File file: dir.listFiles()) {
	        if (file.isDirectory())
	            purgeDirectory(file);
	        file.delete();
	    }
	}
	
	public static void loadExperiment(String experimentName) {
		loadExperiment(experimentsFolderName, experimentName);
	}
	
	public static void loadExperiment(String parentFolder, String experimentName) {
		Path basePath = Paths.get(launcherObject.getString("baseFolder"));
		Path experimentPath = Paths.get(basePath.toString(),
				parentFolder,
				experimentName+".json");
		File file = experimentPath.toFile();
		System.out.println(file.getAbsolutePath());
		JSONObject experimentObject;
		try {
			experimentObject = new JSONObject(new String(Files.readAllBytes(file.toPath()),"UTF-8"));
			currentEC = new ExperimentConfiguration(experimentObject);
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void loadFolderExperiment(String parentFolder, String experimentName) {
		Path basePath = Paths.get(launcherObject.getString("baseFolder"));
		Path experimentPath = Paths.get(basePath.toString(),
				generatedExperimentsFolderName,
				parentFolder,
				experimentName+".json");
		File file = experimentPath.toFile();
		System.out.println(file.getAbsolutePath());
		JSONObject experimentObject;
		try {
			experimentObject = new JSONObject(new String(Files.readAllBytes(file.toPath()),"UTF-8"));
			currentEC = new ExperimentConfiguration(experimentObject);
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void loadFullFolderExperiment(String experimentFullPath) {
		File file = new File(experimentFullPath);
		System.out.println(file.getAbsolutePath());
		JSONObject experimentObject;
		try {
			experimentObject = new JSONObject(new String(Files.readAllBytes(file.toPath()),"UTF-8"));
			currentEC = new ExperimentConfiguration(experimentObject);
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void setCE(ExperimentConfiguration ec) {
		currentEC = ec;
	}
	
	public static ExperimentConfiguration getCE() {
		return currentEC;
	}
	
	public static void pressAnyKeyToContinue() { 
		//System.out.println("Press Enter key to continue...");
		//try {
		//	System.in.read();
		//} catch(Exception e) {}
		Scanner userInput = new Scanner(System.in);
		System.out.println("Press Enter key to continue...");
        String input = userInput.nextLine();
        userInput.close();
	 }
	
	public static void throwRuntimeException(Exception exception, String errorMessage) {
        System.err.println(errorMessage);
        exception.printStackTrace();
        throw new RuntimeException(exception);
	}
}
