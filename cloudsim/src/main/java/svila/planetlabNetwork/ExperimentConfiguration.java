package svila.planetlabNetwork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ExperimentConfiguration {
	
	JSONObject jsonObject;
	
	String name;
	String vmOptimizerPolicy;
	String allocationPolicy;
	String selectionPolicy;
	String selectionMode;
	String hostOverSaturationPolicy;
	String hostUnderUtilisationPolicy;
	String vmMigrationPolicy;
	String techniqueLoader;
	String topology;
	String hosts;
	String vms;
	String workload;
	String workloadTrace;
	String parameter;
	int numCloudlets;
	double utilizationThreshold;
	long randomSeed;
	int migrationInterval;
	double simulationTimeLimit;
	String experimentOutputFolder;
	String logOutputFolder;
	boolean printer;
	boolean enableOutput;
	boolean outputToFile;
	float internalClusterRate;
	float externalClusterRate;
	int logicLinkWeight;
	List<String> interactions;
	List<Integer> interactionsDistribution;
	List<Integer> vmsDistribution;
	List<Integer> hostsDistribution;
	float hostAllocationUtilization;
	boolean outputFullTrace;
	boolean outputCSVTrace;
	boolean outputGraphTrace;
	boolean overrideLinkWeight;
	int defaultWeight;
	int defaultWeightInterconnected;
	Map<String, Double> techniquesConfiguration;
	boolean individualSeeds;
	boolean seedVmShuffle;
	boolean seedHostShuffle;
	boolean seedCreateClusteredLinks;
	boolean seedCreateExternalLinks;
	boolean seedInteractionFiles;
	boolean seedPlanetlabFiles;
	boolean filesInABCOrder;
	boolean isTunning;
	double tunningValue;
	boolean onlyInitialHosts;
	boolean mipsStatsInterpolation;
	int defaultHistoryLength;
	int forecastingHistoryLength;
	int dataStartStep;
	
	String signalProcessing;
	List<Float> signalProcessingParams;
	String forecastingTechnique;
	List<Float> forecastingTechniqueParams;
	
	String hostSignalProcessing;
	List<Float> hostSignalProcessingParams;
	String hostForecastingTechnique;
	List<Float> hostForecastingTechniqueParams;
	String hostForecastingResume;
	
	String vmSignalProcessingBasic;
	List<Float> vmSignalProcessingParamsBasic;
	String vmForecastingTechniqueBasic;
	List<Float> vmForecastingTechniqueParamsBasic;
	String vmForecastingResumeBasic;
	
	String vmSignalProcessingAccurate;
	List<Float> vmSignalProcessingParamsAccurate;
	String vmForecastingTechniqueAccurate;
	List<Float> vmForecastingTechniqueParamsAccurate;
	String vmForecastingResumeAccurate;
	
	String dataSource;
	
	String limitAdjustment;
	List<Float> limitAdjustmentParams;
	
	boolean network;
	
	boolean adjustLoadRatio;
	double minRatio;
	double maxRatio;
	double offsetRatio;
	double usedHostRatio;
	
	public ExperimentConfiguration() {
	}

	public ExperimentConfiguration(JSONObject jsonObject) {
		setName(jsonObject.getString("name"));
		setVmOptimizerPolicy(jsonObject.getString("vmOptimizerPolicy"));
		setAllocationPolicy(jsonObject.getString("allocationPolicy"));
		setSelectionPolicy(jsonObject.getString("selectionPolicy"));
		setHostOverSaturationPolicy(jsonObject.getString("hostOverSaturationPolicy"));
		setHostUnderUtilisationPolicy(jsonObject.getString("hostUnderUtilisationPolicy"));
		setVmMigrationPolicy(jsonObject.getString("vmMigrationPolicy"));
		setTechniqueLoader(jsonObject.getString("techniqueLoader"));
		setTopology(jsonObject.getString("topology"));
		setHosts(jsonObject.getString("hosts"));
		setVms(jsonObject.getString("vms"));
		
		
		setWorkload(jsonObject.getString("workload"));
		if(getWorkload().equals("path")) { // workloadTrace: workload/workloadTrace
			String[] workloadTokens = jsonObject.getString("workloadTrace").split("/");
			setWorkload(workloadTokens[0]);
			setWorkloadTrace(workloadTokens[1]);
		} else {
			setWorkloadTrace(jsonObject.getString("workloadTrace"));
		}
		
		
		setParameter(jsonObject.getString("parameter"));
		setNumCloudlets(jsonObject.getInt("numCloudlets"));
		setUtilizationThreshold(jsonObject.getDouble("utilizationThreshold"));
		setRandomSeed(jsonObject.getLong("randomSeed"));
		if(getRandomSeed() == -1L) {
			setRandomSeed(System.currentTimeMillis());
		}
		setMigrationInterval(jsonObject.getInt("migrationInterval"));
		setSimulationTimeLimit(jsonObject.getDouble("simulationTimeLimit"));
		setExperimentOutputFolder(jsonObject.getString("experimentOutputFolder"));
		setLogOutputFolder(jsonObject.getString("logOutputFolder"));
		setPrinter(jsonObject.getBoolean("printer"));
		setEnableOutput(jsonObject.getBoolean("enableOutput"));
		setOutputToFile(jsonObject.getBoolean("outputToFile"));
		setInternalClusterRate(jsonObject.getBigDecimal("internalClusterRate").floatValue());
		setExternalClusterRate(jsonObject.getBigDecimal("externalClusterRate").floatValue());
		setLogicLinkWeight(jsonObject.getInt("logicLinkWeight"));
		setOutputFullTrace(jsonObject.getBoolean("outputFullTrace"));
		setOutputCSVTrace(jsonObject.getBoolean("outputCSVTrace"));
		setOutputGraphTrace(jsonObject.getBoolean("outputGraphTrace"));
		setOverrideLinkWeight(jsonObject.getBoolean("overrideLinkWeight"));
		setDefaultWeight(jsonObject.getInt("defaultWeight"));
		setDefaultWeightInterconnected(jsonObject.getInt("defaultWeightInterconnected"));
		
		JSONArray interactionsArray = jsonObject.getJSONArray("interactions");
		List<String> interactionsString = new ArrayList<>();
		for(int i=0; i<interactionsArray.length(); i++) {
			interactionsString.add(interactionsArray.getString(i));
		}
		setInteractions(interactionsString);
		
		JSONArray interactionsDistributionArray = jsonObject.getJSONArray("interactionsDistribution");
		List<Integer> interactionsDistributionInt = new ArrayList<>();
		for(int i=0; i<interactionsDistributionArray.length(); i++) {
			interactionsDistributionInt.add(interactionsDistributionArray.getInt(i));
		}
		setInteractionsDistribution(interactionsDistributionInt);
		
		JSONArray vmsDistributionArray = jsonObject.getJSONArray("vmsDistribution");
		List<Integer> vmsDistributionInt = new ArrayList<>();
		for(int i=0; i<vmsDistributionArray.length(); i++) {
			vmsDistributionInt.add(vmsDistributionArray.getInt(i));
		}
		setVmsDistribution(vmsDistributionInt);
		
		JSONArray hostsDistributionArray = jsonObject.getJSONArray("hostsDistribution");
		List<Integer> hostsDistributionInt = new ArrayList<>();
		for(int i=0; i<hostsDistributionArray.length(); i++) {
			hostsDistributionInt.add(hostsDistributionArray.getInt(i));
		}
		setHostsDistribution(hostsDistributionInt);
		
		setHostAllocationUtilization(jsonObject.getBigDecimal("hostAllocationUtilization").floatValue());
		
		JSONObject techConfJson = jsonObject.getJSONObject("techniquesConfiguration");
		
		techniquesConfiguration = new HashMap<>();
		for(String key : techConfJson.keySet()) {
			techniquesConfiguration.put(key, techConfJson.getDouble(key));
		}
		
		setTunning(jsonObject.getBoolean("isTunning"));
		setTunningValue(jsonObject.getDouble("tunningValue"));
		setOnlyInitialHosts(jsonObject.getBoolean("onlyInitialHosts"));
		setMipsStatsInterpolation(jsonObject.getBoolean("mipsStatsInterpolation"));
		
		int dhl;
		try {
			dhl = jsonObject.getInt("defaultHistoryLength");
		} catch(JSONException e) {
			System.out.println(e.getMessage());
			System.out.println("Using default history length: 30");
			dhl = 30;
		}
		setDefaultHistoryLength(dhl);
		
		int fhl;
		try {
			fhl = jsonObject.getInt("forecastingHistoryLength");
		} catch(JSONException e) {
			System.out.println(e.getMessage());
			System.out.println("Using forecasting history length: 30");
			fhl = 30;
		}
		setForecastingHistoryLength(fhl);
		
		setDataStartStep(jsonObject.getInt("dataStartStep"));
		
		setDataSource(jsonObject.getString("dataSource"));

		// V2
		JSONArray signalProcessingArray = jsonObject.getJSONArray("signalProcessing");
		String signalProcessingString = signalProcessingArray.getString(0);
		setSignalProcessing(signalProcessingString);		
		signalProcessingParams = new ArrayList<>();
		for(int i=1; i < signalProcessingArray.length(); i++) {
			signalProcessingParams.add(signalProcessingArray.getBigDecimal(i).floatValue());
		}

		JSONArray forecastingTechniqueArray = jsonObject.getJSONArray("forecastingTechnique");
		String forecastingTechniqueString = forecastingTechniqueArray.getString(0);
		setForecastingTechnique(forecastingTechniqueString);		
		forecastingTechniqueParams = new ArrayList<>();
		for(int i=1; i < forecastingTechniqueArray.length(); i++) {
			forecastingTechniqueParams.add(forecastingTechniqueArray.getBigDecimal(i).floatValue());
		}
		setForecastingTechniqueParams(forecastingTechniqueParams);
		
		// V3
		// Host
		JSONArray hostSignalProcessingArray = jsonObject.getJSONArray("hostSignalProcessing");
		String hostSignalProcessingString = hostSignalProcessingArray.getString(0);
		setHostSignalProcessing(hostSignalProcessingString);		
		hostSignalProcessingParams = new ArrayList<>();
		for(int i=1; i < hostSignalProcessingArray.length(); i++) {
			hostSignalProcessingParams.add(hostSignalProcessingArray.getBigDecimal(i).floatValue());
		}
		
		JSONArray hostForecastingTechniqueArray = jsonObject.getJSONArray("hostForecastingTechnique");
		String hostForecastingTechniqueString = hostForecastingTechniqueArray.getString(0);
		setHostForecastingTechnique(hostForecastingTechniqueString);		
		hostForecastingTechniqueParams = new ArrayList<>();
		for(int i=1; i < hostForecastingTechniqueArray.length(); i++) {
			hostForecastingTechniqueParams.add(hostForecastingTechniqueArray.getBigDecimal(i).floatValue());
		}
		setHostForecastingTechniqueParams(hostForecastingTechniqueParams);
		setHostForecastingResume(jsonObject.getString("hostForecastingResume"));

		// Vm basic

		JSONArray vmSignalProcessingBasicArray = jsonObject.getJSONArray("vmSignalProcessingBasic");
		String vmSignalProcessingBasicString = vmSignalProcessingBasicArray.getString(0);
		setVmSignalProcessingBasic(vmSignalProcessingBasicString);		
		vmSignalProcessingParamsBasic = new ArrayList<>();
		for(int i=1; i < vmSignalProcessingBasicArray.length(); i++) {
			vmSignalProcessingParamsBasic.add(vmSignalProcessingBasicArray.getBigDecimal(i).floatValue());
		}
		
		JSONArray vmForecastingTechniqueBasicArray = jsonObject.getJSONArray("vmForecastingTechniqueBasic");
		String vmForecastingTechniqueBasicString = vmForecastingTechniqueBasicArray.getString(0);
		setVmForecastingTechniqueBasic(vmForecastingTechniqueBasicString);		
		vmForecastingTechniqueParamsBasic = new ArrayList<>();
		for(int i=1; i < vmForecastingTechniqueBasicArray.length(); i++) {
			vmForecastingTechniqueParamsBasic.add(vmForecastingTechniqueBasicArray.getBigDecimal(i).floatValue());
		}
		setVmForecastingTechniqueParamsBasic(vmForecastingTechniqueParamsBasic);
		setVmForecastingResumeBasic(jsonObject.getString("vmForecastingResumeBasic"));
		
		
		// Vm accurate
		
		JSONArray vmSignalProcessingAccurateArray = jsonObject.getJSONArray("vmSignalProcessingAccurate");
		String vmSignalProcessingAccurateString = vmSignalProcessingAccurateArray.getString(0);
		setVmSignalProcessingAccurate(vmSignalProcessingAccurateString);		
		vmSignalProcessingParamsAccurate = new ArrayList<>();
		for(int i=1; i < vmSignalProcessingAccurateArray.length(); i++) {
			vmSignalProcessingParamsAccurate.add(vmSignalProcessingAccurateArray.getBigDecimal(i).floatValue());
		}
		
		JSONArray vmForecastingTechniqueAccurateArray = jsonObject.getJSONArray("vmForecastingTechniqueAccurate");
		String vmForecastingTechniqueAccurateString = vmForecastingTechniqueAccurateArray.getString(0);
		setVmForecastingTechniqueAccurate(vmForecastingTechniqueAccurateString);		
		vmForecastingTechniqueParamsAccurate = new ArrayList<>();
		for(int i=1; i < vmForecastingTechniqueAccurateArray.length(); i++) {
			vmForecastingTechniqueParamsAccurate.add(vmForecastingTechniqueAccurateArray.getBigDecimal(i).floatValue());
		}
		setVmForecastingTechniqueParamsAccurate(vmForecastingTechniqueParamsAccurate);
		setVmForecastingResumeAccurate(jsonObject.getString("vmForecastingResumeAccurate"));
		
		
		setSelectionMode(jsonObject.getString("selectionMode"));
		
		JSONArray limitAdjustmentArray = jsonObject.getJSONArray("limitAdjustment");
		String limitAdjustmentString = limitAdjustmentArray.getString(0);
		setLimitAdjustment(limitAdjustmentString);		
		limitAdjustmentParams = new ArrayList<>();
		for(int i=1; i < limitAdjustmentArray.length(); i++) {
			limitAdjustmentParams.add(limitAdjustmentArray.getBigDecimal(i).floatValue());
		}
		setLimitAdjustmentParams(limitAdjustmentParams);
		
		setNetwork(jsonObject.getBoolean("network"));
		
		if(jsonObject.has("adjustLoadRatio")) {
			setAdjustLoadRatio(jsonObject.getBoolean("adjustLoadRatio"));
			setMinRatio(jsonObject.getDouble("minRatio"));
			setMaxRatio(jsonObject.getDouble("maxRatio"));
			setOffsetRatio(jsonObject.getDouble("offsetRatio"));
		} else {
			setAdjustLoadRatio(false);
			setMinRatio(0.0);
			setMaxRatio(0.0);
			setOffsetRatio(0.0);
			setUsedHostRatio(1.0);
		}
		
	}
	
	public String concatInfo(String baseString, String parameterName, Object o) {
		return baseString.concat(parameterName + ": " + o + "" + "\n");
	}
	
	@Override
	public String toString() {
		String s = "";
		s = concatInfo(s, "Name", getName());
		s = concatInfo(s, "VMOptimizerPolicy", getVmOptimizerPolicy());
		s = concatInfo(s, "AllocationPolicy", getAllocationPolicy());
		s = concatInfo(s, "SelectionPolicy", getSelectionPolicy());
		s = concatInfo(s, "SelectionMode", getSelectionMode());
		s = concatInfo(s, "Host over saturation policy", getHostOverSaturationPolicy());
		s = concatInfo(s, "Host under utilisation policy", getHostUnderUtilisationPolicy());
		s = concatInfo(s, "VM migration policy", getVmMigrationPolicy());
		s = concatInfo(s, "Technique loader", getTechniqueLoader());
		s = concatInfo(s, "Topology", getTopology());
		s = concatInfo(s, "Hosts", getHosts());
		s = concatInfo(s, "Hosts distribution", getHostsDistribution());
		s = concatInfo(s, "VMs", getVms());
		s = concatInfo(s, "VMs distribution", getVmsDistribution());
		s = concatInfo(s, "Workload", getWorkload());
		s = concatInfo(s, "Workload trace", getWorkloadTrace());
		s = concatInfo(s, "Parameter", getParameter());
		s = concatInfo(s, "Num cloudlets", getNumCloudlets());
		s = concatInfo(s, "Utilization threshold", getUtilizationThreshold());
		s = concatInfo(s, "Random seed", getRandomSeed());
		s = concatInfo(s, "Migration interval", migrationInterval);
		s = concatInfo(s, "Simulation time limit", getSimulationTimeLimit());
		s = concatInfo(s, "Logic link weight", getLogicLinkWeight());
		s = concatInfo(s, "Interactions", getInteractions());
		s = concatInfo(s, "Interactions distribution", getInteractionsDistribution());
		s = concatInfo(s, "Internal cluster rate", getInternalClusterRate());
		s = concatInfo(s, "External cluster rate", getExternalClusterRate());
		s = concatInfo(s, "Experiment output folder", getExperimentOutputFolder());
		s = concatInfo(s, "Log output folder", getLogOutputFolder());
		s = concatInfo(s, "Printer", isPrinter());
		s = concatInfo(s, "Enable output", isEnableOutput());
		s = concatInfo(s, "Output to file", isOutputToFile());
		s = concatInfo(s, "Host allocation utilization", getHostAllocationUtilization());
		s = concatInfo(s, "Only initial hosts", isOnlyInitialHosts());
		s = concatInfo(s, "Output full trace", isOutputFullTrace());
		s = concatInfo(s, "Output CSV trace", isOutputCSVTrace());
		s = concatInfo(s, "Output graph trace", isOutputGraphTrace());
		s = concatInfo(s, "Override link weight", isOverrideLinkWeight());
		s = concatInfo(s, "Default weight tree", isOutputCSVTrace());
		s = concatInfo(s, "Output CSV trace", isOutputCSVTrace());
		s = concatInfo(s, "Default weight", getDefaultWeight());
		s = concatInfo(s, "Default weight interconnected", getDefaultWeightInterconnected());
		s = concatInfo(s, "Techniques configuration", getTechniquesConfiguration());
		s = concatInfo(s, "Is tunning", isTunning());
		s = concatInfo(s, "Tunning value", getTunningValue());
		s = concatInfo(s, "Mips stats interpolation", isMipsStatsInterpolation());
		s = concatInfo(s, "Default history length", getDefaultHistoryLength());
		s = concatInfo(s, "Forecasting history length", getForecastingHistoryLength());
		s = concatInfo(s, "Data start step", getDataStartStep());
		s = concatInfo(s, "signalProcessing", getSignalProcessing());
		s = concatInfo(s, "signalProcessingParams", getSignalProcessingParams());
		s = concatInfo(s, "forecastingTechnique", getForecastingTechnique());
		s = concatInfo(s, "forecastingTechniqueParams", getForecastingTechniqueParams());
		
		s = concatInfo(s, "hostSignalProcessing", getHostSignalProcessing());
		s = concatInfo(s, "hostSignalProcessingParams", getHostSignalProcessingParams());
		s = concatInfo(s, "hostForecastingTechnique", getHostForecastingTechnique());
		s = concatInfo(s, "hostForecastingTechniqueParams", getHostForecastingTechniqueParams());
		s = concatInfo(s, "hostForecastingResume", getHostForecastingResume());
		
		s = concatInfo(s, "vmSignalProcessingBasic", getVmSignalProcessingBasic());
		s = concatInfo(s, "vmSignalProcessingParamsBasic", getVmSignalProcessingParamsBasic());
		s = concatInfo(s, "vmForecastingTechniqueBasic", getVmForecastingTechniqueBasic());
		s = concatInfo(s, "vmForecastingTechniqueParamsBasic", getVmForecastingTechniqueParamsBasic());
		s = concatInfo(s, "vmForecastingResumeBasic", getVmForecastingResumeBasic());
		
		s = concatInfo(s, "vmSignalProcessingAccurate", getVmSignalProcessingAccurate());
		s = concatInfo(s, "vmSignalProcessingParamsAccurate", getVmSignalProcessingParamsAccurate());
		s = concatInfo(s, "vmForecastingTechniqueAccurate", getVmForecastingTechniqueAccurate());
		s = concatInfo(s, "vmForecastingTechniqueParamsAccurate", getVmForecastingTechniqueParamsAccurate());
		s = concatInfo(s, "vmForecastingResumeAccurate", getVmForecastingResumeAccurate());
		
		s = concatInfo(s, "Data source", getDataSource());
		
		s = concatInfo(s, "limitAdjustment", getLimitAdjustment());
		s = concatInfo(s, "limitAdjustmentParams", getLimitAdjustmentParams());
		
		s = concatInfo(s, "network", isNetwork());
		
		s = concatInfo(s, "adjustLoadRatio", isAdjustLoadRatio());
		s = concatInfo(s, "minRatio", getMinRatio());
		s = concatInfo(s, "maxRatio", getMaxRatio());
		s = concatInfo(s, "offsetRatio", getOffsetRatio());
		s = concatInfo(s, "usedHostRatio", getUsedHostRatio());
		
		return s;
	}
	
	public void generateJSONObject() {
		jsonObject = new JSONObject();
		jsonObject.put("name", getName());
		jsonObject.put("vmOptimizerPolicy", getVmOptimizerPolicy());
		jsonObject.put("allocationPolicy", getAllocationPolicy());
		jsonObject.put("selectionPolicy", getSelectionPolicy());
		jsonObject.put("selectionMode", getSelectionMode());
		jsonObject.put("hostOverSaturationPolicy", getHostOverSaturationPolicy());
		jsonObject.put("hostUnderUtilisationPolicy", getHostUnderUtilisationPolicy());
		jsonObject.put("vmMigrationPolicy", getVmMigrationPolicy());
		jsonObject.put("techniqueLoader", getTechniqueLoader());
		jsonObject.put("topology", getTopology());
		jsonObject.put("hosts", getHosts());
		jsonObject.put("hostsDistribution", getHostsDistribution());
		jsonObject.put("vms", getVms());
		jsonObject.put("vmsDistribution", getVmsDistribution());
		jsonObject.put("workload", getWorkload());
		jsonObject.put("workloadTrace", getWorkloadTrace());
		jsonObject.put("parameter", getParameter());
		jsonObject.put("numCloudlets", numCloudlets);
		jsonObject.put("utilizationThreshold", getUtilizationThreshold());
		jsonObject.put("randomSeed", getRandomSeed());
		jsonObject.put("migrationInterval", getMigrationInterval());
		jsonObject.put("simulationTimeLimit", getSimulationTimeLimit());
		jsonObject.put("logicLinkWeight", getLogicLinkWeight());
		jsonObject.put("interactions", getInteractions());
		jsonObject.put("interactionsDistribution", getInteractionsDistribution());
		jsonObject.put("internalClusterRate", getInternalClusterRate());
		jsonObject.put("externalClusterRate", getExternalClusterRate());
		jsonObject.put("experimentOutputFolder", getExperimentOutputFolder());
		jsonObject.put("logOutputFolder", getLogOutputFolder());
		jsonObject.put("printer", isPrinter());
		jsonObject.put("enableOutput", isEnableOutput());
		jsonObject.put("outputToFile", isOutputToFile());
		jsonObject.put("hostAllocationUtilization", getHostAllocationUtilization());
		jsonObject.put("onlyInitialHosts", isOnlyInitialHosts());
		jsonObject.put("outputFullTrace", isOutputFullTrace());
		jsonObject.put("outputCSVTrace", isOutputCSVTrace());
		jsonObject.put("outputGraphTrace", isOutputGraphTrace());
		jsonObject.put("overrideLinkWeight", isOverrideLinkWeight());
		jsonObject.put("defaultWeight", getDefaultWeight());
		jsonObject.put("defaultWeightInterconnected", getDefaultWeightInterconnected());
		jsonObject.put("techniquesConfiguration", getTechniquesConfiguration());
		jsonObject.put("isTunning", isTunning());
		jsonObject.put("tunningValue", getTunningValue());
		jsonObject.put("mipsStatsInterpolation", isMipsStatsInterpolation());
		jsonObject.put("defaultHistoryLength", getDefaultHistoryLength());
		jsonObject.put("forecastingHistoryLength", getForecastingHistoryLength());
		jsonObject.put("dataStartStep", getDataStartStep());
		
		jsonObject.put("signalProcessing", getSignalProcessing());
		jsonObject.put("signalProcessingParams", getSignalProcessingParams());
		jsonObject.put("forecastingTechnique", getForecastingTechnique());
		jsonObject.put("forecastingTechniqueParams", getForecastingTechniqueParams());
		
		jsonObject.put("hostSignalProcessing", getHostSignalProcessing());
		jsonObject.put("hostSignalProcessingParams", getHostSignalProcessingParams());
		jsonObject.put("hostForecastingTechnique", getHostForecastingTechnique());
		jsonObject.put("hostForecastingTechniqueParams", getHostForecastingTechniqueParams());
		jsonObject.put("hostForecastingResume", getHostForecastingResume());
		
		jsonObject.put("vmSignalProcessingBasic", getVmSignalProcessingBasic());
		jsonObject.put("vmSignalProcessingParamsBasic", getVmSignalProcessingParamsBasic());
		jsonObject.put("vmForecastingTechniqueBasic", getVmForecastingTechniqueBasic());
		jsonObject.put("vmForecastingTechniqueParamsBasic", getVmForecastingTechniqueParamsBasic());
		jsonObject.put("vmForecastingResumeBasic", getVmForecastingResumeBasic());
		
		jsonObject.put("vmSignalProcessingAccurate", getVmSignalProcessingAccurate());
		jsonObject.put("vmSignalProcessingParamsAccurate", getVmSignalProcessingParamsAccurate());
		jsonObject.put("vmForecastingTechniqueAccurate", getVmForecastingTechniqueAccurate());
		jsonObject.put("vmForecastingTechniqueParamsAccurate", getVmForecastingTechniqueParamsAccurate());
		jsonObject.put("vmForecastingResumeAccurate", getVmForecastingResumeAccurate());
		
		jsonObject.put("dataSource", getDataSource());
		
		jsonObject.put("limitAdjustment", getLimitAdjustment());
		jsonObject.put("limitAdjustmentParams", getLimitAdjustmentParams());
		
		jsonObject.put("network", isNetwork());
		
		jsonObject.put("adjustLoadRatio", isAdjustLoadRatio());
		jsonObject.put("minRatio", getMinRatio());
		jsonObject.put("maxRatio", getMaxRatio());
		jsonObject.put("offsetRatio", getOffsetRatio());
		jsonObject.put("usedHostRatio", getUsedHostRatio());
	}
	
	public JSONObject getJSONObject() {
		if(jsonObject == null) {
			generateJSONObject();
		}
		
		return jsonObject;
	}
	
	public boolean isOutputFullTrace() {
		return outputFullTrace;
	}

	public void setOutputFullTrace(boolean outputFullTrace) {
		this.outputFullTrace = outputFullTrace;
	}

	public float getHostAllocationUtilization() {
		return hostAllocationUtilization;
	}

	public void setHostAllocationUtilization(float hostAllocationUtilization) {
		this.hostAllocationUtilization = hostAllocationUtilization;
	}

	public int getLogicLinkWeight() {
		return logicLinkWeight;
	}

	public void setLogicLinkWeight(int logicLinkWeight) {
		this.logicLinkWeight = logicLinkWeight;
	}

	public List<Integer> getVmsDistribution() {
		return vmsDistribution;
	}

	public void setVmsDistribution(List<Integer> vmsDistribution) {
		this.vmsDistribution = vmsDistribution;
	}

	public List<Integer> getHostsDistribution() {
		return hostsDistribution;
	}

	public void setHostsDistribution(List<Integer> hostsDistribution) {
		this.hostsDistribution = hostsDistribution;
	}

	public float getInternalClusterRate() {
		return internalClusterRate;
	}

	public void setInternalClusterRate(float internalClusterRate) {
		this.internalClusterRate = internalClusterRate;
	}

	public float getExternalClusterRate() {
		return externalClusterRate;
	}

	public void setExternalClusterRate(float externalClusterRate) {
		this.externalClusterRate = externalClusterRate;
	}

	public List<Integer> getInteractionsDistribution() {
		return interactionsDistribution;
	}

	public void setInteractionsDistribution(List<Integer> interactionsDistribution) {
		this.interactionsDistribution = interactionsDistribution;
	}

	public void setInteractions(List<String> interactions) {
		this.interactions = interactions;
	}

	public List<String> getInteractions() {
		return interactions;
	}

	public int getNumCloudlets() {
		return numCloudlets;
	}

	public void setNumCloudlets(int numCloudlets) {
		this.numCloudlets = numCloudlets;
	}

	public boolean isEnableOutput() {
		return enableOutput;
	}

	public void setEnableOutput(boolean enableOutput) {
		this.enableOutput = enableOutput;
	}

	public boolean isOutputToFile() {
		return outputToFile;
	}

	public void setOutputToFile(boolean outputToFile) {
		this.outputToFile = outputToFile;
	}

	public String getLogOutputFolder() {
		return logOutputFolder;
	}
	
	public void setLogOutputFolder(String logOutputFolder) {
		this.logOutputFolder = logOutputFolder;
	}
	
	public double getUtilizationThreshold() {
		return this.utilizationThreshold;
	}
	
	public void setUtilizationThreshold(double utilizationThreshold) {
		this.utilizationThreshold = utilizationThreshold;
	}
	
	public String getParameter() {
		return parameter;
	}
	
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAllocationPolicy() {
		return allocationPolicy;
	}

	public void setAllocationPolicy(String allocationPolicy) {
		this.allocationPolicy = allocationPolicy;
	}

	public String getSelectionPolicy() {
		return selectionPolicy;
	}

	public void setSelectionPolicy(String selectionPolicy) {
		this.selectionPolicy = selectionPolicy;
	}

	public String getTopology() {
		return topology;
	}

	public void setTopology(String topology) {
		this.topology = topology;
	}

	public String getHosts() {
		return hosts;
	}

	public void setHosts(String hosts) {
		this.hosts = hosts;
	}

	public String getVms() {
		return vms;
	}

	public void setVms(String vms) {
		this.vms = vms;
	}

	public long getRandomSeed() {
		return randomSeed;
	}

	public void setRandomSeed(long randomSeed) {
		this.randomSeed = randomSeed;
	}

	public int getMigrationInterval() {
		return migrationInterval;
	}

	public void setMigrationInterval(int migrationInterval) {
		this.migrationInterval = migrationInterval;
	}

	public double getSimulationTimeLimit() {
		return simulationTimeLimit;
	}

	public void setSimulationTimeLimit(double simulationTimeLimit) {
		this.simulationTimeLimit = simulationTimeLimit;
	}

	public String getExperimentOutputFolder() {
		return experimentOutputFolder;
	}

	public void setExperimentOutputFolder(String experimentOutputFolder) {
		this.experimentOutputFolder = experimentOutputFolder;
	}

	public JSONObject getJsonObject() {
		return jsonObject;
	}

	public void setJsonObject(JSONObject jsonObject) {
		this.jsonObject = jsonObject;
	}

	public boolean isOutputCSVTrace() {
		return outputCSVTrace;
	}

	public void setOutputCSVTrace(boolean outputCSVTrace) {
		this.outputCSVTrace = outputCSVTrace;
	}

	public boolean isOverrideLinkWeight() {
		return overrideLinkWeight;
	}

	public void setOverrideLinkWeight(boolean linkWeight) {
		this.overrideLinkWeight = linkWeight;
	}

	public float getDefaultWeight() {
		return defaultWeight;
	}

	public void setDefaultWeight(int defaultWeight) {
		this.defaultWeight = defaultWeight;
	}

	public int getDefaultWeightInterconnected() {
		return defaultWeightInterconnected;
	}

	public void setDefaultWeightInterconnected(int defaultWeightInterconnected) {
		this.defaultWeightInterconnected = defaultWeightInterconnected;
	}

	public Map<String, Double> getTechniquesConfiguration() {
		return techniquesConfiguration;
	}

	public void setTechniquesConfiguration(Map<String, Double> techniquesConfiguration) {
		this.techniquesConfiguration = techniquesConfiguration;
	}

	public boolean isOutputGraphTrace() {
		return outputGraphTrace;
	}

	public void setOutputGraphTrace(boolean outputGraphTrace) {
		this.outputGraphTrace = outputGraphTrace;
	}

	public boolean isTunning() {
		return isTunning;
	}

	public void setTunning(boolean isTunning) {
		this.isTunning = isTunning;
	}

	public double getTunningValue() {
		return tunningValue;
	}

	public void setTunningValue(double tunningValue) {
		this.tunningValue = tunningValue;
	}

	public String getVmOptimizerPolicy() {
		return vmOptimizerPolicy;
	}

	public void setVmOptimizerPolicy(String vmOptimizerPolicy) {
		this.vmOptimizerPolicy = vmOptimizerPolicy;
	}

	public String getHostOverSaturationPolicy() {
		return hostOverSaturationPolicy;
	}

	public void setHostOverSaturationPolicy(String hostOverSaturationPolicy) {
		this.hostOverSaturationPolicy = hostOverSaturationPolicy;
	}

	public String getHostUnderUtilisationPolicy() {
		return hostUnderUtilisationPolicy;
	}

	public void setHostUnderUtilisationPolicy(String hostUnderUtilisationPolicy) {
		this.hostUnderUtilisationPolicy = hostUnderUtilisationPolicy;
	}

	public String getTechniqueLoader() {
		return techniqueLoader;
	}

	public void setTechniqueLoader(String techniqueLoader) {
		this.techniqueLoader = techniqueLoader;
	}

	public String getVmMigrationPolicy() {
		return vmMigrationPolicy;
	}

	public void setVmMigrationPolicy(String vmMigrationPolicy) {
		this.vmMigrationPolicy = vmMigrationPolicy;
	}

	public boolean isOnlyInitialHosts() {
		return onlyInitialHosts;
	}

	public void setOnlyInitialHosts(boolean onlyInitialHosts) {
		this.onlyInitialHosts = onlyInitialHosts;
	}

	public String getWorkload() {
		return workload;
	}

	public void setWorkload(String workload) {
		this.workload = workload;
	}

	public String getWorkloadTrace() {
		return workloadTrace;
	}

	public void setWorkloadTrace(String workloadTrace) {
		this.workloadTrace = workloadTrace;
	}

	public boolean isMipsStatsInterpolation() {
		return mipsStatsInterpolation;
	}

	public void setMipsStatsInterpolation(boolean mipsStatsInterpolation) {
		this.mipsStatsInterpolation = mipsStatsInterpolation;
	}

	public int getDefaultHistoryLength() {
		return defaultHistoryLength;
	}

	public void setDefaultHistoryLength(int defaultHistoryLength) {
		this.defaultHistoryLength = defaultHistoryLength;
	}

	public int getDataStartStep() {
		return dataStartStep;
	}

	public void setDataStartStep(int dataStartStep) {
		this.dataStartStep = dataStartStep;
	}

	public String getSignalProcessing() {
		return signalProcessing;
	}

	public void setSignalProcessing(String signalProcessing) {
		this.signalProcessing = signalProcessing;
	}

	public List<Float> getSignalProcessingParams() {
		return signalProcessingParams;
	}

	public void setSignalProcessingParams(List<Float> signalProcessingParams) {
		this.signalProcessingParams = signalProcessingParams;
	}

	public String getForecastingTechnique() {
		return forecastingTechnique;
	}

	public void setForecastingTechnique(String forecastingTechnique) {
		this.forecastingTechnique = forecastingTechnique;
	}

	public List<Float> getForecastingTechniqueParams() {
		return forecastingTechniqueParams;
	}

	public void setForecastingTechniqueParams(List<Float> forecastingTechniqueParams) {
		this.forecastingTechniqueParams = forecastingTechniqueParams;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public int getForecastingHistoryLength() {
		return forecastingHistoryLength;
	}

	public void setForecastingHistoryLength(int forecastingHistoryLength) {
		this.forecastingHistoryLength = forecastingHistoryLength;
	}

	public boolean isPrinter() {
		return printer;
	}

	public void setPrinter(boolean printer) {
		this.printer = printer;
	}

	public String getHostSignalProcessing() {
		return hostSignalProcessing;
	}

	public void setHostSignalProcessing(String hostSignalProcessing) {
		this.hostSignalProcessing = hostSignalProcessing;
	}

	public List<Float> getHostSignalProcessingParams() {
		return hostSignalProcessingParams;
	}

	public void setHostSignalProcessingParams(List<Float> hostSignalProcessingParams) {
		this.hostSignalProcessingParams = hostSignalProcessingParams;
	}

	public String getHostForecastingTechnique() {
		return hostForecastingTechnique;
	}

	public void setHostForecastingTechnique(String hostForecastingTechnique) {
		this.hostForecastingTechnique = hostForecastingTechnique;
	}

	public List<Float> getHostForecastingTechniqueParams() {
		return hostForecastingTechniqueParams;
	}

	public void setHostForecastingTechniqueParams(List<Float> hostForecastingTechniqueParams) {
		this.hostForecastingTechniqueParams = hostForecastingTechniqueParams;
	}

	public String getVmSignalProcessingBasic() {
		return vmSignalProcessingBasic;
	}

	public void setVmSignalProcessingBasic(String vmSignalProcessingBasic) {
		this.vmSignalProcessingBasic = vmSignalProcessingBasic;
	}

	public List<Float> getVmSignalProcessingParamsBasic() {
		return vmSignalProcessingParamsBasic;
	}

	public void setVmSignalProcessingParamsBasic(List<Float> vmSignalProcessingParamsBasic) {
		this.vmSignalProcessingParamsBasic = vmSignalProcessingParamsBasic;
	}

	public String getVmForecastingTechniqueBasic() {
		return vmForecastingTechniqueBasic;
	}

	public void setVmForecastingTechniqueBasic(String vmForecastingTechniqueBasic) {
		this.vmForecastingTechniqueBasic = vmForecastingTechniqueBasic;
	}

	public List<Float> getVmForecastingTechniqueParamsBasic() {
		return vmForecastingTechniqueParamsBasic;
	}

	public void setVmForecastingTechniqueParamsBasic(List<Float> vmForecastingTechniqueParamsBasic) {
		this.vmForecastingTechniqueParamsBasic = vmForecastingTechniqueParamsBasic;
	}

	public String getVmSignalProcessingAccurate() {
		return vmSignalProcessingAccurate;
	}

	public void setVmSignalProcessingAccurate(String vmSignalProcessingAccurate) {
		this.vmSignalProcessingAccurate = vmSignalProcessingAccurate;
	}

	public List<Float> getVmSignalProcessingParamsAccurate() {
		return vmSignalProcessingParamsAccurate;
	}

	public void setVmSignalProcessingParamsAccurate(List<Float> vmSignalProcessingParamsAccurate) {
		this.vmSignalProcessingParamsAccurate = vmSignalProcessingParamsAccurate;
	}

	public String getVmForecastingTechniqueAccurate() {
		return vmForecastingTechniqueAccurate;
	}

	public void setVmForecastingTechniqueAccurate(String vmForecastingTechniqueAccurate) {
		this.vmForecastingTechniqueAccurate = vmForecastingTechniqueAccurate;
	}

	public List<Float> getVmForecastingTechniqueParamsAccurate() {
		return vmForecastingTechniqueParamsAccurate;
	}

	public void setVmForecastingTechniqueParamsAccurate(List<Float> vmForecastingTechniqueParamsAccurate) {
		this.vmForecastingTechniqueParamsAccurate = vmForecastingTechniqueParamsAccurate;
	}

	public String getSelectionMode() {
		return selectionMode;
	}

	public void setSelectionMode(String selectionMode) {
		this.selectionMode = selectionMode;
	}

	public String getLimitAdjustment() {
		return limitAdjustment;
	}

	public void setLimitAdjustment(String limitAdjustment) {
		this.limitAdjustment = limitAdjustment;
	}

	public List<Float> getLimitAdjustmentParams() {
		return limitAdjustmentParams;
	}

	public void setLimitAdjustmentParams(List<Float> limitAdjustmentParams) {
		this.limitAdjustmentParams = limitAdjustmentParams;
	}

	public String getHostForecastingResume() {
		return hostForecastingResume;
	}

	public void setHostForecastingResume(String hostForecastingResume) {
		this.hostForecastingResume = hostForecastingResume;
	}

	public String getVmForecastingResumeBasic() {
		return vmForecastingResumeBasic;
	}

	public void setVmForecastingResumeBasic(String vmForecastingResumeBasic) {
		this.vmForecastingResumeBasic = vmForecastingResumeBasic;
	}

	public String getVmForecastingResumeAccurate() {
		return vmForecastingResumeAccurate;
	}

	public void setVmForecastingResumeAccurate(String vmForecastingResumeAccurate) {
		this.vmForecastingResumeAccurate = vmForecastingResumeAccurate;
	}

	public boolean isNetwork() {
		return network;
	}

	public void setNetwork(boolean network) {
		this.network = network;
	}

	public boolean isAdjustLoadRatio() {
		return adjustLoadRatio;
	}

	public void setAdjustLoadRatio(boolean adjustLoadRatio) {
		this.adjustLoadRatio = adjustLoadRatio;
	}

	public double getMinRatio() {
		return minRatio;
	}

	public void setMinRatio(double minRatio) {
		this.minRatio = minRatio;
	}

	public double getMaxRatio() {
		return maxRatio;
	}

	public void setMaxRatio(double maxRatio) {
		this.maxRatio = maxRatio;
	}

	public double getOffsetRatio() {
		return offsetRatio;
	}

	public void setOffsetRatio(double offsetRatio) {
		this.offsetRatio = offsetRatio;
	}

	public double getUsedHostRatio() {
		return usedHostRatio;
	}

	public void setUsedHostRatio(double usedHostRatio) {
		this.usedHostRatio = usedHostRatio;
	}
	
	
}
