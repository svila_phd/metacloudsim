package svila.planetlabNetwork.optimizeAllocation;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;
import org.cloudbus.cloudsim.util.ExecutionTimeMeasurer;

public class BaseOptimizeAllocation {
	PowerVmAllocationPolicyMigrationAbstract allocationPolicy;
	UtilizationHostManager overManager;
	VMSelectionManager vmSelectionManager;
	VMMigrationManager vmMigrationManager;
	UtilizationHostManager underManager;
	VMMigrationManager vmUnderManager;

	
	public void fullOptimization() {
		
	}
	
	public List<Map<String, Object>> adaptedOptimizeAllocation(List<? extends Vm> vmList) {
		fullOptimization(); // Hook
		
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationTotal");
		ExecutionTimeMeasurer.start("optimizeAllocationHostSelection");
		
		// Step 1: Get overutilized hosts
		List<PowerHostUtilizationHistory> overUtilizedHosts = allocationPolicy.getOverUtilizedHosts();
		
		
		
		// Statistics
		allocationPolicy.getExecutionTimeHistoryHostSelection().add(
				ExecutionTimeMeasurer.end("optimizeAllocationHostSelection"));

		// Restore point
		allocationPolicy.saveAllocation();
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationVmSelection");
		
		// Step 2: Get VMs to migrate from overutilized hosts
		List<? extends Vm> vmsToMigrate = allocationPolicy.getVmsToMigrateFromHosts(overUtilizedHosts);
		
		// Statistics
		allocationPolicy.getExecutionTimeHistoryVmSelection().add(ExecutionTimeMeasurer.end("optimizeAllocationVmSelection"));
		Log.printLine("Reallocation of VMs from the over-utilized hosts:");
		ExecutionTimeMeasurer.start("optimizeAllocationVmReallocation");
		
		// Step 3: Get to the migrated VMs a new host
		List<Map<String, Object>> migrationMap = allocationPolicy.getNewVmPlacement(vmsToMigrate, new HashSet<Host>(
				overUtilizedHosts));
		
		// Statistics
		allocationPolicy.getExecutionTimeHistoryVmReallocation().add(
				ExecutionTimeMeasurer.end("optimizeAllocationVmReallocation"));
		Log.printLine();

		// Step 4:   Migration map from underutilized hosts
		// Step 4.1: Underutilization host detection
		// Step 4.2: VM migrations
		migrationMap.addAll(allocationPolicy.getMigrationMapFromUnderUtilizedHosts(overUtilizedHosts));

		// Restore the VMs to their original host
		allocationPolicy.restoreAllocation();

		// Statistics
		allocationPolicy.getExecutionTimeHistoryTotal().add(ExecutionTimeMeasurer.end("optimizeAllocationTotal"));

		
		//showMigrationMap(migrationMap);
		// Return map with VM migrations
		return migrationMap;
	}
	
	public List<Map<String, Object>> originalOptimizeAllocation(List<? extends Vm> vmList) {
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationTotal");
		ExecutionTimeMeasurer.start("optimizeAllocationHostSelection");
		
		// Step 1: Get overutilized hosts
		List<PowerHostUtilizationHistory> overUtilizedHosts = allocationPolicy.getOverUtilizedHosts();
		
		// Statistics
		allocationPolicy.getExecutionTimeHistoryHostSelection().add(
				ExecutionTimeMeasurer.end("optimizeAllocationHostSelection"));

		// Restore point
		allocationPolicy.saveAllocation();
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationVmSelection");
		
		// Step 2: Get VMs to migrate from overutilized hosts
		List<? extends Vm> vmsToMigrate = allocationPolicy.getVmsToMigrateFromHosts(overUtilizedHosts);
		
		// Statistics
		allocationPolicy.getExecutionTimeHistoryVmSelection().add(ExecutionTimeMeasurer.end("optimizeAllocationVmSelection"));
		Log.printLine("Reallocation of VMs from the over-utilized hosts:");
		ExecutionTimeMeasurer.start("optimizeAllocationVmReallocation");
		
		// Step 3: Get to the migrated VMs a new host
		List<Map<String, Object>> migrationMap = allocationPolicy.getNewVmPlacement(vmsToMigrate, new HashSet<Host>(
				overUtilizedHosts));
		
		// Statistics
		allocationPolicy.getExecutionTimeHistoryVmReallocation().add(
				ExecutionTimeMeasurer.end("optimizeAllocationVmReallocation"));
		Log.printLine();

		// Step 4: Migration map from underutilized hosts
		migrationMap.addAll(allocationPolicy.getMigrationMapFromUnderUtilizedHosts(overUtilizedHosts));

		// Restore the VMs to their original host
		allocationPolicy.restoreAllocation();

		// Statistics
		allocationPolicy.getExecutionTimeHistoryTotal().add(ExecutionTimeMeasurer.end("optimizeAllocationTotal"));

		
		//showMigrationMap(migrationMap);
		// Return map with VM migrations
		return migrationMap;
	}
}
