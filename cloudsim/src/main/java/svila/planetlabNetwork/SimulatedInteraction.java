package svila.planetlabNetwork;

public class SimulatedInteraction {
	public int numSlot;
	public int vmIdStart;
	public int vmIdEnd;
	public float bandwidth;
	
	public SimulatedInteraction(int numSlot, int vmIdStart, int vmIdEnd, float bandwith) {
		this.numSlot = numSlot;
		this.vmIdStart = vmIdStart;
		this.vmIdEnd = vmIdEnd;
		this.bandwidth = bandwith;
	}
}
