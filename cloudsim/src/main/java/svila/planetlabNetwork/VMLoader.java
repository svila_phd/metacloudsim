package svila.planetlabNetwork;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.power.PowerVm;
import org.yaml.snakeyaml.Yaml;

public class VMLoader {
	
	List<PowerVMTemplate> vmTemplatesList;
	
	public static void main(String[] args) {
		StaticResources.init();
		
		String vmTemplatesFileName = "testVms";
		
		Path vmTemplatesPath = Paths.get(StaticResources.baseFolder,
								StaticResources.vmsFolderName,
								vmTemplatesFileName + ".txt");
		System.out.println(vmTemplatesPath.toString());
		VMLoader vmLoader = new VMLoader();
		vmLoader.loadVmTemplates(new File(vmTemplatesPath.toString()));
		
		List<PowerVMTemplate> vmTemplates = vmLoader.getVmTemplates();
		PowerVm powerVM = vmTemplates.get(0).generatePowerVM(0, 0);
		System.out.println(powerVM.getMips());
	}
	
	public VMLoader() {
		
	}
	
	public List<PowerVMTemplate> getVmTemplates() {
		return vmTemplatesList;
	}
	
	public void loadVmTemplates(File yamlFile) {
		vmTemplatesList = new ArrayList<>();
		InputStream input;
	
		try {
			input = new FileInputStream(yamlFile);
		
		    Yaml yaml = new Yaml();
		    List<Map<String,Object>> vms = (List<Map<String,Object>>)yaml.load(input);
		
			for(Map<String,Object> vmwrap: vms)
			{	
				Map<String,Object> vm = (Map<String,Object>)vmwrap.get("VM");
				
			    int VM_MIPS = ((ArrayList<Integer>)vm.get("CpuCapacity")).get(0);
				int VM_PES = (int)vm.get("Cores");
				int VM_RAM = (int)vm.get("RAM");
				//int VM_BW = (int)vm.get("BandWidth");
				int VM_BW = 0;
				int VM_SIZE = (int)vm.get("DiskCapacity");
				
				PowerVMTemplate pvmt = new PowerVMTemplate();
				pvmt.setMips(VM_MIPS);
				pvmt.setPes(VM_PES);
				pvmt.setRam(VM_RAM);
				pvmt.setBw(VM_BW);
				pvmt.setSize(VM_SIZE);

				vmTemplatesList.add(pvmt);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
