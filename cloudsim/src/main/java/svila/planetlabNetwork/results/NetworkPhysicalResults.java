package svila.planetlabNetwork.results;

import java.util.List;

import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.Link;
import svila.planetlabNetwork.Link.LinkType;
import svila.planetlabNetwork.Snapshot;

public class NetworkPhysicalResults extends NetworkResults {
	private static final long serialVersionUID = 1L;
	
	public static NetworkPhysicalResults instance;
	
	public NetworkPhysicalResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter, int steps) {
		super(name, snapshots, datacenter, steps);
		//if(instance == null) {
		instance = this;
		//}
	}
	
	@Override
	public boolean isValidLink(Link link) {
		return link.getLinkType() != LinkType.hostVm;
	}
}
