package svila.planetlabNetwork.results;

public class MigrationReport extends ExportableExperimentResults{
	
	// Per cada migració observar els pròxims 6 passos
	// Als hosts i a la VM moguda, s'obté quants MIPS s'han executat correctament,
	// quants han sobrat i quants s'han saturat
	
	@Override
	public void exportAll() {
		
	}
	
	@Override
	public void calculateAll() {
		
	}

	@Override
	public void export(int i) {
		
	}

	@Override
	public void calculate(int i) {
		
	}

	
	
}
