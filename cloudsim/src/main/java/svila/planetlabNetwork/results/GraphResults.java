package svila.planetlabNetwork.results;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.json.JSONArray;
import org.json.JSONObject;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.Graph;
import svila.planetlabNetwork.Link;
import svila.planetlabNetwork.Node;
import svila.planetlabNetwork.Path;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;

public class GraphResults extends ExportableExperimentResults {

	public GraphResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super(name, snapshots, datacenter);
	}
	
	public void export(int i) {
		ExperimentConfiguration ec = StaticResources.getCE();

		String outputFolderName = ec.getExperimentOutputFolder();
		java.nio.file.Path outputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				outputFolderName, "graphs", ec.getName());
		File outputFolder = outputPath.toFile();
		outputFolder.mkdirs();

		Snapshot s = snapshots.get(i);
		JSONObject jsonGraph = getSnaphsotGraph(s);
		String snapshotNum = String.format("%05d", s.getSnapshotNumber());
		java.nio.file.Path exportFile = Paths.get(outputPath.toString(), ec.getName()+"_snapshot_" + snapshotNum +".json");
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(exportFile.toString()));
			jsonGraph.write(writer);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void exportAll() {

	}
	
	private JSONObject getSnaphsotGraph(Snapshot s) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("snapshot", s.getSnapshotNumber());
		jsonObject.put("time", s.getSnapshotTime());
		JSONArray nodes = new JSONArray();
		jsonObject.put("nodes", nodes);
		JSONArray edges = new JSONArray();
		jsonObject.put("edges", edges);
		JSONArray paths = new JSONArray();
		jsonObject.put("paths", paths);
		
		Graph g = s.getAggregatedBWGraph();
		for(Node node : g.getNodes()) {
			JSONObject jsonNode = new JSONObject();
			nodes.put(jsonNode);
			jsonNode.put("id", node.getId());
			jsonNode.put("type", node.getType());
		}
		
		for(Link link : g.getLinks()) {
			if(link.getSourceNode().getId() > link.getEndNode().getId()) {
				// Nothing, we avoid half of the links
				continue;
			}
			
			JSONObject jsonLink = new JSONObject();
			edges.put(jsonLink);
			jsonLink.put("source", link.getSourceNode().getId());
			jsonLink.put("target", link.getEndNode().getId());
			jsonLink.put("weight", link.getWeight());
			jsonLink.put("type", link.getLinkType());
		}
		
		for(Path path : s.getPathsGraph().getPaths()) {
			JSONObject jsonPath = new JSONObject();
			paths.put(jsonPath);
			jsonPath.put("source", path.getNode1().getId());
			jsonPath.put("target", path.getNode2().getId());
			JSONArray pathLinks = new JSONArray();
			jsonPath.put("links", pathLinks);
			for(Link link : path.getLinks()) {
				JSONObject jsonLink = new JSONObject();
				jsonLink.put("source", link.getSourceNode().getId());
				jsonLink.put("target", link.getEndNode().getId());
				jsonLink.put("weight", link.getWeight());
				jsonLink.put("type", link.getLinkType());
				pathLinks.put(jsonLink);
			}
		}
		
		return jsonObject;
	}

	@Override
	public void calculate(int i) {
		
	}

}
