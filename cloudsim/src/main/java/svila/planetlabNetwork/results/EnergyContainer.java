package svila.planetlabNetwork.results;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;

public class EnergyContainer {
	public static Map<String, Double> energyMap;
	public static Map<String, Double> energyEnhancedMap;
	
	public static void init(List<Host> hostList) {
		energyMap = new HashMap<>();
		energyEnhancedMap = new HashMap<>();
		energyMap.put("0", 0.0);
		for(Host host : hostList) {
			energyMap.put("0-"+host.getNetworkId(), 0.0);
			energyEnhancedMap.put("0-"+host.getNetworkId(), 0.0);
		}
	}
	
	public static void addHostValue(int step, int hostNetId, double energy) {
		String key = step + "-" + hostNetId;
		energyMap.merge(key, energy, (oldVal, newVal) -> oldVal + newVal);
	}
	
	public static void addSnapshotValue(int step, double energy) {
		energyMap.merge(String.valueOf(step), energy, (oldVal, newVal) -> oldVal + newVal);
	}
	
	public static void addHostValue(double time, int hostNetId, double energy) {
		String key = time + "-" + hostNetId;
		energyEnhancedMap.put(key, energy);
	}
	
	public static void addSnapshotValue(double time, double energy) {
		energyEnhancedMap.put(String.valueOf(time), energy);
	}
	
	public static double getHostValue(int step, int hostNetId) {
		return energyMap.get(step + "-" + hostNetId);
	}
	
	public static double getSnapshotValue(int step) {
		return energyMap.get(String.valueOf(step));
	}
	
	public static double getHostValue(double time, int hostNetId) {
		/*if(time < 3000) {
			System.out.println("Energy container: " + time + "-" + hostNetId);
		}*/
		Double value = energyEnhancedMap.get(time + "-" + hostNetId);
		if(value == null) {
			System.out.println("Null Energy container: " + time + "-" + hostNetId);
			return 0.0;
		}
		return value;
	}
	
	public static double getSnapshotValue(double time) {
		return energyEnhancedMap.get(String.valueOf(time));
	}
}
