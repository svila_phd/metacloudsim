package svila.planetlabNetwork.results;

import java.util.List;

import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.PowerHostData;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;

public class RAMResults extends ExperimentResultsAbs implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	double[] totalAvailableRAM;
	double[] totalAllocatedRAM;
	double[] totalWastedRAM;

	public RAMResults() {
		
	}
	
	public RAMResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter, int steps) {
		super(name, snapshots, datacenter);
		this.totalAvailableRAM = new double[steps];
		this.totalAllocatedRAM = new double[steps];
		this.totalWastedRAM    = new double[steps];
	}
	
	public void calculateAll() {
			
	}
	
	public void calculate(int i) {
		Snapshot currentSnapshot = snapshots.get(i);
		for(PowerHostData phd : currentSnapshot.getPowerHostDataMap().values()) {
			totalAvailableRAM[i] += phd.getHost().getRam();
			totalAllocatedRAM[i] += phd.getHost().getUtilizationOfRam();
			totalWastedRAM[i]    += totalAvailableRAM[i] - totalAllocatedRAM[i];
		}
	}

	public double[] getTotalAvailableRAM() {
		return totalAvailableRAM;
	}

	public void setTotalAvailableRAM(double[] totalAvailableRAM) {
		this.totalAvailableRAM = totalAvailableRAM;
	}

	public double[] getTotalAllocatedRAM() {
		return totalAllocatedRAM;
	}

	public void setTotalAllocatedRAM(double[] totalAllocatedRAM) {
		this.totalAllocatedRAM = totalAllocatedRAM;
	}

	public double[] getTotalWastedRAM() {
		return totalWastedRAM;
	}

	public void setTotalWastedRAM(double[] totalWastedRAM) {
		this.totalWastedRAM = totalWastedRAM;
	}
}
