package svila.planetlabNetwork.results;

import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.Snapshot;

public class ExperimentResults {
	
	ExperimentConfiguration ec;
	List<Snapshot> snapshots;
	PowerDatacenter datacenter;
	//List<Vm> vmList;
	double lastClock;
	long startTime;
	long endTime;
	List<ExportableExperimentResults> results;

	public ExperimentResults() {
		results = new ArrayList<>();
	}
	
	public void loadDefaultExperiments() {
		MainResults mr = new MainResults("main", snapshots, datacenter);
		mr.addResults(new CPUResults("cpuResults", snapshots, datacenter));
		mr.addResults(new HelperResults("cloudsimResults", snapshots, datacenter));
		mr.addResults(new NetworkResults("networkResults", snapshots, datacenter));
		mr.addResults(new NetworkPhysicalResults("networkPhysicalResults", snapshots, datacenter));
		mr.addResults(new SLAResults("slaResults", snapshots, datacenter));
		mr.addResults(new SnapshotResults("snapshotResults", snapshots, datacenter));	
		
		results.add(mr);
		
		TimeResults tr = new TimeResults("timeResults", snapshots, datacenter);
		tr.setStartTime(startTime);
		tr.setEndTime(endTime);
		tr.setLastClock(lastClock);
		mr.addResults(tr);
		
		if(ec.isOutputFullTrace()) {
			results.add(new FullTraceResults("fullTraceResults", snapshots, datacenter));
		}
		if(ec.isOutputCSVTrace()) {
			results.add(new HistoricHostsResults("historicHostsResults", snapshots, datacenter));
			results.add(new HistoricVMResults("historicVMResults", snapshots, datacenter));
			results.add(new MigrationResults("migrationResults", snapshots, datacenter));
		}
		if(ec.isOutputGraphTrace()) {
			results.add(new GraphResults("graphResults", snapshots, datacenter));
		}
		
		CorrelationsResults cr = CorrelationsResults.getInstance();
		if(cr != null) {
			cr.init("CorrelationsResults", snapshots, datacenter);
			results.add(cr);
		}
	}
	
	public void calculateResults() {
		for(ExportableExperimentResults experiment : results) {
			experiment.calculateAll();
			experiment.postCalculate();
		}
	}
	
	public void export() {
		for(ExportableExperimentResults result : results) {
			result.exportAll();
		}
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public ExperimentConfiguration getEc() {
		return ec;
	}

	public void setEc(ExperimentConfiguration ec) {
		this.ec = ec;
	}

	public List<Snapshot> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(List<Snapshot> snapshots) {
		this.snapshots = snapshots;
	}

	public PowerDatacenter getDatacenter() {
		return datacenter;
	}

	public void setDatacenter(PowerDatacenter datacenter) {
		this.datacenter = datacenter;
	}

	/*public List<Vm> getVmList() {
		return vmList;
	}

	public void setVmList(List<Vm> vmList) {
		this.vmList = vmList;
	}*/

	public double getLastClock() {
		return lastClock;
	}

	public void setLastClock(double lastClock) {
		this.lastClock = lastClock;
	}
}
