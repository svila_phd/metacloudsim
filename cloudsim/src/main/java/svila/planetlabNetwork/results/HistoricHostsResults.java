package svila.planetlabNetwork.results;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.HostStateHistoryEntry;
import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;

public class HistoricHostsResults extends ExportableExperimentResults {
	List<HostData> hostDatas;

	public HistoricHostsResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super(name, snapshots, datacenter);
		this.hostDatas = new ArrayList<>();
	}
	
	@Override
	public void calculateAll() {

	}
	
	public void calculate(int i) {
		ExperimentConfiguration ec = StaticResources.getCE();
		if(!ec.isOutputCSVTrace()) {
			return;
		}
		for(Host host : datacenter.getHostList()) {
			HostDynamicWorkload hdw = (HostDynamicWorkload) host;
			HostStateHistoryEntry currentStateHistory = hdw.getStateHistoryMap().get(snapshots.get(i).getSnapshotTime());
			double requestedMips = currentStateHistory.getRequestedMips();
			double allocatedMips = currentStateHistory.getAllocatedMips();
			HostData hostData = new HostData(hdw.getId(), hdw.getNetworkId(), i,
					requestedMips, allocatedMips,
					(double)host.getTotalMips(),
					EnergyContainer.getHostValue(i, hdw.getNetworkId()),
					host.getRam(),
					currentStateHistory.getUsedRam(),
					host.getRam() - currentStateHistory.getUsedRam(),
					currentStateHistory.getNetworkUtilization(),
					currentStateHistory.getNumVms()
					);
			this.hostDatas.add(hostData);
			host.hostDatas.add(hostData);
		}
	}

	@Override
	public void exportAll() {
		ExperimentConfiguration ec = StaticResources.getCE();

		String outputFolderName = ec.getExperimentOutputFolder();
		Path outputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				outputFolderName);
		File outputFolder = outputPath.toFile();
		outputFolder.mkdir();
		
		Path exportFile = Paths.get(outputPath.toString(), ec.getName()+"_hosts"+".csv");
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(exportFile.toString()));
			writer.write(HostData.CSVHeader + "\n");
			for(HostData hostData : this.hostDatas) {
				writer.write(hostData.getCSVLine() + "\n");
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void export(int i) {
		
	}
}
