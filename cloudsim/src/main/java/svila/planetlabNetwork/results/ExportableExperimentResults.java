package svila.planetlabNetwork.results;

import java.util.List;

import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.Snapshot;

public abstract class ExportableExperimentResults extends ExperimentResultsAbs {	
	public ExportableExperimentResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super(name, snapshots, datacenter);
	}
	
	public ExportableExperimentResults() {
		super();
	}

	public abstract void exportAll();
	
	public abstract void export(int i);
}
