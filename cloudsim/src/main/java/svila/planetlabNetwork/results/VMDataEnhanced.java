package svila.planetlabNetwork.results;

public class VMDataEnhanced {
	int VMId;
	int netVMId;
	double time;
	double requestedMIPS;
	double allocatedMIPS;
	double percentageUsed;
	double overusedMIPS;
	double percentageOverused;
	double MIPSCapacity;
	double externalBW;
	int hostId;
	
	public static final String CSVHeader = "id" + "," + "netId" + "," + "time" + "," + "requestedMIPS" + "," + 
			"allocatedMIPS" + "," + "percentageUsage" + "," + "overusedMIPS" + "," + "percentageOverused"
			+ "," + "MIPSCapacity" + "," + "hostId" + "," + "externalBW";
	
	public VMDataEnhanced(int VMId, int netVMId, double time,
			Double requestedMIPS, Double allocatedMIPS, Double MIPSCapacity,
			int hostId, double externalBW) {
		this.VMId = VMId;
		this.netVMId = netVMId;
		this.time = time;
		this.requestedMIPS = requestedMIPS;
		this.allocatedMIPS = allocatedMIPS;
		this.overusedMIPS = Math.max(0, requestedMIPS - allocatedMIPS);
		this.MIPSCapacity = MIPSCapacity;
		this.hostId = hostId;
		if(allocatedMIPS == 0.0) {
			this.percentageUsed = 0.0f;
			this.percentageOverused = 0.0f;
		} else {
			this.percentageUsed = requestedMIPS / allocatedMIPS;
			this.percentageOverused = overusedMIPS / allocatedMIPS;
		}
		this.externalBW = externalBW;
	}
	
	public String getCSVLine() {
		return VMId + "," + netVMId + "," + time + "," + requestedMIPS + "," + 
				allocatedMIPS + "," + percentageUsed + "," + overusedMIPS + "," + percentageOverused
				+ "," + MIPSCapacity + "," + hostId + "," + externalBW;
	}
}
