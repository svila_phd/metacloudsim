package svila.planetlabNetwork.results;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.HostStateHistoryEntry;
import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;

public class HistoricHostsResultsEnhanced extends ExportableExperimentResults {
	List<HostDataEnhanced> hostDatas;

	public HistoricHostsResultsEnhanced(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super(name, snapshots, datacenter);
		this.hostDatas = new ArrayList<>();
	}
	
	@Override
	public void calculateAll() {
		ExperimentConfiguration ec = StaticResources.getCE();
		if(!ec.isOutputCSVTrace()) {
			return;
		}
		for(Host host : datacenter.getHostList()) {
			HostDynamicWorkload hdw = (HostDynamicWorkload) host;
			List<HostStateHistoryEntry> stateHistory = hdw.getStateHistory();
			for(int i=0; i<stateHistory.size(); i++) {
				HostStateHistoryEntry currentStateHistory = stateHistory.get(i);
				double requestedMips = currentStateHistory.getRequestedMips();
				double allocatedMips = currentStateHistory.getAllocatedMips();
				
				/*if(host.getId() == 14 && currentStateHistory.getTime() >= 55200.0 && currentStateHistory.getTime() < 59000.0) {
					double requestedMipsDebug = currentStateHistory.getRequestedMips();
					System.out.println("HistoricHostsResultsEnhanced: " + currentStateHistory.getTime() + " " + requestedMips);
				}*/
				
				HostDataEnhanced hostDataEnhanced = new HostDataEnhanced(hdw.getId(), hdw.getNetworkId(), currentStateHistory.getTime(),
						requestedMips, allocatedMips,
						(double)host.getTotalMips(),
						EnergyContainer.getHostValue(currentStateHistory.getTime(), host.getNetworkId()),
						host.getRam(),
						currentStateHistory.getUsedRam(),
						host.getRam() - currentStateHistory.getUsedRam(),
						currentStateHistory.getNetworkUtilization(),
						currentStateHistory.getNumVms()
						);
				int lastIndex = this.hostDatas.size()-1;
				if(this.hostDatas.size() > 0) {
					HostDataEnhanced last = this.hostDatas.get(lastIndex);
					if(host.getNetworkId() == last.getHostNetId()) {
						hostDataEnhanced.setOffsetTime(last);
					}
				}
				this.hostDatas.add(hostDataEnhanced);
			}
			
		}
	}
	
	public void calculate(int i) {
		
	}

	@Override
	public void exportAll() {
		ExperimentConfiguration ec = StaticResources.getCE();

		String outputFolderName = ec.getExperimentOutputFolder();
		Path outputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				outputFolderName);
		File outputFolder = outputPath.toFile();
		outputFolder.mkdir();
		
		Path exportFile = Paths.get(outputPath.toString(), ec.getName()+"_hosts_enhanced"+".csv");
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(exportFile.toString()));
			writer.write(HostDataEnhanced.CSVHeader + "\n");
			for(HostDataEnhanced hostDataEnhanced : this.hostDatas) {
				writer.write(hostDataEnhanced.getCSVLine() + "\n");
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void export(int i) {
		
	}
}
