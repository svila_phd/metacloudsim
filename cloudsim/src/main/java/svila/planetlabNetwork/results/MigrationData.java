package svila.planetlabNetwork.results;

public class MigrationData {
	
	int snapshot;
	int originHost;
	int destinationHost;
	int vm;
	
	public static final String CSVHeader = "snapshot" + "," + "originHost" + "," + 
			"destinationHost" + "," + "vm";
	
	public MigrationData(int snapshot, int originHost, int destinationHost, int vm) {
		this.snapshot = snapshot;
		this.originHost = originHost;
		this.destinationHost = destinationHost;
		this.vm = vm;
	}
	
	public String getCSVLine() {
		return snapshot + "," + originHost + "," + destinationHost + "," + vm;
	}
}
