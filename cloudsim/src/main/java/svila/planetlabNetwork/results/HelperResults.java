package svila.planetlabNetwork.results;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.HostStateHistoryEntry;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.examples.power.Helper;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.util.MathUtil;
import org.json.JSONArray;
import org.json.JSONObject;

import svila.planetlabNetwork.Snapshot;

public class HelperResults extends ExperimentResultsAbs implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	int numberOfHosts;
	int numberOfVms;
	double energy;
	int numberOfMigrations;
	int numberOfDelayedMigrations;
	Map<String, Double> slaMetrics;
	double slaOverall;
	double slaAverage;
	double slaDegradationDueToMigration;
	double slaTimePerHost;
	double slaTimePerActiveHost;
	double sla;
	List<Double> timeBeforeHostShutdown;
	int numberOfHostShutdowns;
	double meanTimeBeforeHostShutdown;
	double stDevTimeBeforeHostShutdown;
	List<Double> timeBeforeVmMigration;
	double meanTimeBeforeVmMigration;
	double stDevTimeBeforeVmMigration;

	double executionTimeVmSelectionMean;
	double executionTimeVmSelectionStDev;
	double executionTimeHostSelectionMean;
	double executionTimeHostSelectionStDev;
	double executionTimeVmReallocationMean;
	double executionTimeVmReallocationStDev;
	double executionTimeTotalMean;
	double executionTimeTotalStDev;

	double vmTotalAllocatedMips;
	double vmTotalRequestedMips;
	double vmTotalUnderAllocatedDueToMigrationMips;

	// Sergi
	double energyOpenHosts;
	double energyCloseHosts;
	double energyOpenCloseHosts;
	double energyWithExtraHost;

	public HelperResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super(name, snapshots, datacenter);
	}

	public void calculate(int i) {

	}

	@Override
	public void calculateAll() {
		List<Host> hosts = datacenter.getHostList();
		List<Vm> vmList = datacenter.getVmList();
		this.numberOfHosts = hosts.size();
		this.numberOfVms = vmList.size();

		this.energy = datacenter.getPower() / (3600 * 1000);

		this.numberOfMigrations = datacenter.getMigrationCount();
		this.numberOfDelayedMigrations = datacenter.getDelayedMigrationCount();

		this.slaMetrics = Helper.getSlaMetrics(vmList);

		this.slaOverall = this.slaMetrics.get("overall");
		this.slaAverage = this.slaMetrics.get("average");
		this.slaDegradationDueToMigration = this.slaMetrics.get("underallocated_migration");
		this.slaTimePerHost = Helper.getSlaTimePerHost(hosts);
		this.slaTimePerActiveHost = Helper.getSlaTimePerActiveHost(hosts);
		
		this.vmTotalAllocatedMips = this.slaMetrics.get("totalAllocated");
		this.vmTotalRequestedMips = this.slaMetrics.get("totalRequested");
		this.vmTotalUnderAllocatedDueToMigrationMips = this.slaMetrics.get("totalUnderAllocatedDueToMigration");

		this.sla = this.slaTimePerActiveHost * this.slaDegradationDueToMigration;

		//this.timeBeforeHostShutdown = Helper.getTimesBeforeHostShutdown(hosts);
		this.timeBeforeHostShutdown = Helper.getTimesBeforeHostShutdownFixed(hosts); // Sergi
		
		//System.out.println("NumberOfHostShutdowns:" + Helper.getTimesBeforeHostShutdown(hosts).size());
		//System.out.println("NumberOfHostShutdownsFixed:" + Helper.getTimesBeforeHostShutdownFixed(hosts).size());

		this.numberOfHostShutdowns = this.timeBeforeHostShutdown.size();

		this.meanTimeBeforeHostShutdown = Double.NaN;
		this.stDevTimeBeforeHostShutdown = Double.NaN;
		if (!this.timeBeforeHostShutdown.isEmpty()) {
			this.meanTimeBeforeHostShutdown = MathUtil.mean(this.timeBeforeHostShutdown);
			this.stDevTimeBeforeHostShutdown = MathUtil.stDev(this.timeBeforeHostShutdown);
		}

		this.timeBeforeVmMigration = Helper.getTimesBeforeVmMigration(vmList);
		this.meanTimeBeforeVmMigration = Double.NaN;
		this.stDevTimeBeforeVmMigration = Double.NaN;
		if (!this.timeBeforeVmMigration.isEmpty()) {
			this.meanTimeBeforeVmMigration = MathUtil.mean(this.timeBeforeVmMigration);
			this.stDevTimeBeforeVmMigration = MathUtil.stDev(this.timeBeforeVmMigration);
		}

		this.energy = checkNaN(this.energy);
		for(String key : this.slaMetrics.keySet()) {
			this.slaMetrics.put(key, checkNaN(this.slaMetrics.get(key)));
		}
		this.slaOverall = checkNaN(this.slaOverall);
		this.slaAverage = checkNaN(this.slaAverage);
		this.slaDegradationDueToMigration = checkNaN(this.slaDegradationDueToMigration);
		this.slaTimePerHost = checkNaN(this.slaTimePerHost);
		this.slaTimePerActiveHost = checkNaN(this.slaTimePerActiveHost);
		this.sla= checkNaN(this.sla);
		for(int i=0; i<this.timeBeforeHostShutdown.size(); i++) {
			this.timeBeforeHostShutdown.set(i, checkNaN(this.timeBeforeHostShutdown.get(i)));
		}
		this.meanTimeBeforeHostShutdown = checkNaN(this.meanTimeBeforeHostShutdown);
		this.stDevTimeBeforeHostShutdown = checkNaN(this.stDevTimeBeforeHostShutdown);
		for(int i=0; i<this.timeBeforeVmMigration.size(); i++) {
			this.timeBeforeVmMigration.set(i, checkNaN(this.timeBeforeVmMigration.get(i)));
		}
		this.meanTimeBeforeVmMigration = checkNaN(this.meanTimeBeforeVmMigration);
		this.stDevTimeBeforeVmMigration = checkNaN(this.stDevTimeBeforeVmMigration);
		this.executionTimeVmSelectionMean = checkNaN(this.executionTimeVmSelectionMean);
		this.executionTimeVmSelectionStDev = checkNaN(this.executionTimeVmSelectionStDev);
		this.executionTimeHostSelectionMean = checkNaN(this.executionTimeHostSelectionMean);
		this.executionTimeHostSelectionStDev = checkNaN(this.executionTimeHostSelectionStDev);
		this.executionTimeVmReallocationMean = checkNaN(this.executionTimeVmReallocationMean);
		this.executionTimeVmReallocationStDev = checkNaN(this.executionTimeVmReallocationStDev);
		this.executionTimeTotalMean = checkNaN(this.executionTimeTotalMean);
		this.executionTimeTotalStDev = checkNaN(this.executionTimeTotalStDev);
		
		// Sergi
		Map<String, Double> extraEnergy = this.getExtraEnergy(hosts);
		this.energyOpenHosts = extraEnergy.get("energyOffToOn");
		this.energyCloseHosts = extraEnergy.get("energyOnToOff");
		this.energyOpenCloseHosts = this.energyOpenHosts + this.energyCloseHosts;
		this.energyWithExtraHost = this.energy + this.energyOpenCloseHosts;
		
		this.energyOpenHosts = checkNaN(this.energyOpenHosts);
		this.energyCloseHosts = checkNaN(this.energyOpenCloseHosts);
		this.energyOpenCloseHosts = checkNaN(this.energyOpenCloseHosts);
		this.energyWithExtraHost = checkNaN(this.energyWithExtraHost);
	}

	private Map<String, Double> getExtraEnergy(List<Host> hosts) {
		Map<String, Double> extraEnergy = new HashMap<>();
		double energyOffToOn = 0.0;
		double energyOnToOff = 0.0;
		
		for (Host host : hosts) {
			boolean previousIsActive = true;
			int count = 0;
			for (HostStateHistoryEntry entry : ((HostDynamicWorkload) host).getStateHistory()) {
				if(count == 0) {
					previousIsActive = entry.isActive();
				}
				
				if (previousIsActive == true && entry.isActive() == false) { // On to off
					energyOnToOff += ((PowerHost) host).getEnergyOff();
				}
				if (previousIsActive == false && entry.isActive() == true) { // Off to on
					energyOffToOn += ((PowerHost) host).getEnergyOn();
				}
				previousIsActive = entry.isActive();
				count++;
			}
		}
		
		energyOffToOn = energyOffToOn / (3600*1000); // J (or Ws) -> kWh
		energyOnToOff = energyOnToOff / (3600*1000); // J (or Ws) -> kWh

		extraEnergy.put("energyOffToOn", energyOffToOn);
		extraEnergy.put("energyOnToOff", energyOnToOff);
		return extraEnergy;
	}

	public int getNumberOfHosts() {
		return numberOfHosts;
	}

	public void setNumberOfHosts(int numberOfHosts) {
		this.numberOfHosts = numberOfHosts;
	}

	public int getNumberOfVms() {
		return numberOfVms;
	}

	public void setNumberOfVms(int numberOfVms) {
		this.numberOfVms = numberOfVms;
	}

	public double getEnergy() {
		return energy;
	}

	public void setEnergy(double energy) {
		this.energy = energy;
	}

	public int getNumberOfMigrations() {
		return numberOfMigrations;
	}

	public void setNumberOfMigrations(int numberOfMigrations) {
		this.numberOfMigrations = numberOfMigrations;
	}

	public Map<String, Double> getSlaMetrics() {
		return slaMetrics;
	}

	public void setSlaMetrics(Map<String, Double> slaMetrics) {
		this.slaMetrics = slaMetrics;
	}

	public double getSlaOverall() {
		return slaOverall;
	}

	public void setSlaOverall(double slaOverall) {
		this.slaOverall = slaOverall;
	}

	public double getSlaAverage() {
		return slaAverage;
	}

	public void setSlaAverage(double slaAverage) {
		this.slaAverage = slaAverage;
	}

	public double getSlaDegradationDueToMigration() {
		return slaDegradationDueToMigration;
	}

	public void setSlaDegradationDueToMigration(double slaDegradationDueToMigration) {
		this.slaDegradationDueToMigration = slaDegradationDueToMigration;
	}

	public double getSlaTimePerActiveHost() {
		return slaTimePerActiveHost;
	}

	public void setSlaTimePerActiveHost(double slaTimePerActiveHost) {
		this.slaTimePerActiveHost = slaTimePerActiveHost;
	}

	public double getSla() {
		return sla;
	}

	public void setSla(double sla) {
		this.sla = sla;
	}

	public List<Double> getTimeBeforeHostShutdown() {
		return timeBeforeHostShutdown;
	}

	public void setTimeBeforeHostShutdown(List<Double> timeBeforeHostShutdown) {
		this.timeBeforeHostShutdown = timeBeforeHostShutdown;
	}

	public int getNumberOfHostShutdowns() {
		return numberOfHostShutdowns;
	}

	public void setNumberOfHostShutdowns(int numberOfHostShutdowns) {
		this.numberOfHostShutdowns = numberOfHostShutdowns;
	}

	public double getMeanTimeBeforeHostShutdown() {
		return meanTimeBeforeHostShutdown;
	}

	public void setMeanTimeBeforeHostShutdown(double meanTimeBeforeHostShutdown) {
		this.meanTimeBeforeHostShutdown = meanTimeBeforeHostShutdown;
	}

	public double getStDevTimeBeforeHostShutdown() {
		return stDevTimeBeforeHostShutdown;
	}

	public void setStDevTimeBeforeHostShutdown(double stDevTimeBeforeHostShutdown) {
		this.stDevTimeBeforeHostShutdown = stDevTimeBeforeHostShutdown;
	}

	public List<Double> getTimeBeforeVmMigration() {
		return timeBeforeVmMigration;
	}

	public void setTimeBeforeVmMigration(List<Double> timeBeforeVmMigration) {
		this.timeBeforeVmMigration = timeBeforeVmMigration;
	}

	public double getMeanTimeBeforeVmMigration() {
		return meanTimeBeforeVmMigration;
	}

	public void setMeanTimeBeforeVmMigration(double meanTimeBeforeVmMigration) {
		this.meanTimeBeforeVmMigration = meanTimeBeforeVmMigration;
	}

	public double getStDevTimeBeforeVmMigration() {
		return stDevTimeBeforeVmMigration;
	}

	public void setStDevTimeBeforeVmMigration(double stDevTimeBeforeVmMigration) {
		this.stDevTimeBeforeVmMigration = stDevTimeBeforeVmMigration;
	}

	public double getExecutionTimeVmSelectionMean() {
		return executionTimeVmSelectionMean;
	}

	public void setExecutionTimeVmSelectionMean(double executionTimeVmSelectionMean) {
		this.executionTimeVmSelectionMean = executionTimeVmSelectionMean;
	}

	public double getExecutionTimeVmSelectionStDev() {
		return executionTimeVmSelectionStDev;
	}

	public void setExecutionTimeVmSelectionStDev(double executionTimeVmSelectionStDev) {
		this.executionTimeVmSelectionStDev = executionTimeVmSelectionStDev;
	}

	public double getExecutionTimeHostSelectionMean() {
		return executionTimeHostSelectionMean;
	}

	public void setExecutionTimeHostSelectionMean(double executionTimeHostSelectionMean) {
		this.executionTimeHostSelectionMean = executionTimeHostSelectionMean;
	}

	public double getExecutionTimeHostSelectionStDev() {
		return executionTimeHostSelectionStDev;
	}

	public void setExecutionTimeHostSelectionStDev(double executionTimeHostSelectionStDev) {
		this.executionTimeHostSelectionStDev = executionTimeHostSelectionStDev;
	}

	public double getExecutionTimeVmReallocationMean() {
		return executionTimeVmReallocationMean;
	}

	public void setExecutionTimeVmReallocationMean(double executionTimeVmReallocationMean) {
		this.executionTimeVmReallocationMean = executionTimeVmReallocationMean;
	}

	public double getExecutionTimeVmReallocationStDev() {
		return executionTimeVmReallocationStDev;
	}

	public void setExecutionTimeVmReallocationStDev(double executionTimeVmReallocationStDev) {
		this.executionTimeVmReallocationStDev = executionTimeVmReallocationStDev;
	}

	public double getExecutionTimeTotalMean() {
		return executionTimeTotalMean;
	}

	public void setExecutionTimeTotalMean(double executionTimeTotalMean) {
		this.executionTimeTotalMean = executionTimeTotalMean;
	}

	public double getExecutionTimeTotalStDev() {
		return executionTimeTotalStDev;
	}

	public void setExecutionTimeTotalStDev(double executionTimeTotalStDev) {
		this.executionTimeTotalStDev = executionTimeTotalStDev;
	}

	public double getVmTotalAllocatedMips() {
		return vmTotalAllocatedMips;
	}

	public void setVmTotalAllocatedMips(double vmTotalAllocatedMips) {
		this.vmTotalAllocatedMips = vmTotalAllocatedMips;
	}

	public double getVmTotalRequestedMips() {
		return vmTotalRequestedMips;
	}

	public void setVmTotalRequestedMips(double vmTotalRequestedMips) {
		this.vmTotalRequestedMips = vmTotalRequestedMips;
	}

	public double getVmTotalUnderAllocatedDueToMigrationMips() {
		return vmTotalUnderAllocatedDueToMigrationMips;
	}

	public void setVmTotalUnderAllocatedDueToMigrationMips(double vmTotalUnderAllocatedDueToMigrationMips) {
		this.vmTotalUnderAllocatedDueToMigrationMips = vmTotalUnderAllocatedDueToMigrationMips;
	}

	public double getSlaTimePerHost() {
		return slaTimePerHost;
	}

	public void setSlaTimePerHost(double slaTimePerHost) {
		this.slaTimePerHost = slaTimePerHost;
	}

	public double getEnergyOpenHosts() {
		return energyOpenHosts;
	}

	public void setEnergyOpenHosts(double energyOpenHosts) {
		this.energyOpenHosts = energyOpenHosts;
	}

	public double getEnergyCloseHosts() {
		return energyCloseHosts;
	}

	public void setEnergyCloseHosts(double energyCloseHosts) {
		this.energyCloseHosts = energyCloseHosts;
	}

	public double getEnergyWithExtraHost() {
		return energyWithExtraHost;
	}

	public void setEnergyWithExtraHost(double energyWithExtraHost) {
		this.energyWithExtraHost = energyWithExtraHost;
	}

	public double getEnergyOpenCloseHosts() {
		return energyOpenCloseHosts;
	}

	public void setEnergyOpenCloseHosts(double energyOpenCloseHosts) {
		this.energyOpenCloseHosts = energyOpenCloseHosts;
	}

	public int getNumberOfDelayedMigrations() {
		return numberOfDelayedMigrations;
	}

	public void setNumberOfDelayedMigrations(int numberOfDelayedMigrations) {
		this.numberOfDelayedMigrations = numberOfDelayedMigrations;
	}
	

}
