package svila.planetlabNetwork.results;

import java.io.File;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.UtilizationModelPlanetLabInMemory;
import org.cloudbus.cloudsim.Vm;

public class InitialScenarioExporter {
	List<Cloudlet> cloudletList;
	List<Vm> vmList;
	
	public InitialScenarioExporter(List<Cloudlet> cloudletList, List<Vm> vmList) {
		this.cloudletList = cloudletList;
		this.vmList = vmList;
	}
	
	public void export() {
		/*for(Cloudlet cloudlet : cloudletList) {
			UtilizationModelPlanetLabInMemory cpuUtilization = (UtilizationModelPlanetLabInMemory) cloudlet.getUtilizationModelCpu();
			File path = new File(cpuUtilization.getPath());
			System.out.println(cloudlet.getCloudletId() + ": " + path.getParentFile().getName() + "/" + path.getName());
		}*/
		for(Vm vm : vmList) {
			System.out.println(vm.getCloudletScheduler());
		}
	}
}
