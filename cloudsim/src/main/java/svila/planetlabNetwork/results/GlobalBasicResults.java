package svila.planetlabNetwork.results;

public class GlobalBasicResults extends ExperimentResultsAbs implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private float hostsCPU;
	private float VMsCPU;
	
	@Override
	public void calculate(int i) {

	}

	public float getHostsCPU() {
		return hostsCPU;
	}

	public void setHostsCPU(float hostsCPU) {
		this.hostsCPU = hostsCPU;
	}

	public float getVMsCPU() {
		return VMsCPU;
	}

	public void setVMsCPU(float vMsCPU) {
		VMsCPU = vMsCPU;
	}

}
