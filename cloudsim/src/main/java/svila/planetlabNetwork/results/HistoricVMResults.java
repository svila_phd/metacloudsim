package svila.planetlabNetwork.results;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;

public class HistoricVMResults extends ExportableExperimentResults {
	List<VMData> vmDatas;

	public HistoricVMResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super(name, snapshots, datacenter);
		this.vmDatas = new ArrayList<>();
	}
	
	@Override
	public void calculateAll() {

	}
	
	public void calculate(int i) {
		ExperimentConfiguration ec = StaticResources.getCE();
		if(!ec.isOutputCSVTrace()) {
			return;
		}
		for(Vm vm : datacenter.getVmList()) {
			double requestedMips = vm.getStateHistoryMap().get(snapshots.get(i).getSnapshotTime()).getRequestedMips();
			double allocatedMips = vm.getStateHistoryMap().get(snapshots.get(i).getSnapshotTime()).getAllocatedMips();

			VMData vmData = new VMData(vm.getId(), i, requestedMips, allocatedMips, vm.getMips());
			this.vmDatas.add(vmData);
		}
	}

	@Override
	public void exportAll() {
		ExperimentConfiguration ec = StaticResources.getCE();

		String outputFolderName = ec.getExperimentOutputFolder();
		Path outputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				outputFolderName);
		File outputFolder = outputPath.toFile();
		outputFolder.mkdir();
		
		Path exportFile = Paths.get(outputPath.toString(), ec.getName()+"_vms"+".csv");
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(exportFile.toString()));
			writer.write(VMData.CSVHeader + "\n");
			for(VMData vmData : this.vmDatas) {
				writer.write(vmData.getCSVLine() + "\n");
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void export(int i) {
		// TODO Auto-generated method stub
		
	}
}
