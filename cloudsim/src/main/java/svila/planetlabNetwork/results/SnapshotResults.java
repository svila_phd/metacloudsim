package svila.planetlabNetwork.results;

import java.util.List;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.Snapshot;

public class SnapshotResults extends ExperimentResultsAbs implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	boolean[] migrationSnapshots;
	double[] energySnapshots;
	int[] numVms;
	int[] numVmsInDatacenter;
	double[] selectedVMsToMigratePerSnapshot;
	int totalSelectedVMstoMigrate;
	
	public SnapshotResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter, int steps) {
		super(name, snapshots, datacenter);
		this.migrationSnapshots = new boolean[steps];
		this.energySnapshots = new double[steps];
		this.energySnapshots = new double[steps];
		this.numVms = new int[steps];
		this.numVmsInDatacenter = new int[steps];
		this.selectedVMsToMigratePerSnapshot = new double[steps];
		this.totalSelectedVMstoMigrate = 0;
	}
	
	@Override
	public void calculateAll() {
		
	}
	
	public void calculate(int i) {
		this.migrationSnapshots[i] = snapshots.get(i).isMigrationSnapshot();
		this.energySnapshots[i] = EnergyContainer.getSnapshotValue(i) / (3600 * 1000);
		
		if(i == 210) {
			System.out.println(this.datacenter.getVmList().size());
		}
		
		double currentTime = 300.0*i + 0.1;
		for(Vm vm : this.datacenter.getVmList()) {
			if(vm.getTotalUtilizationOfCpu(currentTime) != 0.0) {
				this.numVms[i]++;
			}
		}
		this.numVmsInDatacenter[i] = this.datacenter.getVmList().size();
	}

	public boolean[] getMigrationSnapshots() {
		return migrationSnapshots;
	}

	public void setMigrationSnapshots(boolean[] migrationSnapshots) {
		this.migrationSnapshots = migrationSnapshots;
	}

	public double[] getEnergySnapshots() {
		return energySnapshots;
	}

	public void setEnergySnapshots(double[] energySnapshots) {
		this.energySnapshots = energySnapshots;
	}

	public int[] getNumVms() {
		return numVms;
	}

	public void setNumVms(int[] numVms) {
		this.numVms = numVms;
	}

	public int[] getNumVmsInDatacenter() {
		return numVmsInDatacenter;
	}

	public void setNumVmsInDatacenter(int[] numVmsInDatacenter) {
		this.numVmsInDatacenter = numVmsInDatacenter;
	}
}
