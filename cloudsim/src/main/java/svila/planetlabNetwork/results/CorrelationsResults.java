package svila.planetlabNetwork.results;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;

public class CorrelationsResults extends ExportableExperimentResults {
	static CorrelationsResults instance;
	List<CorrelationsData> correlationsDatas;
	
	public CorrelationsResults() {
		super();
	}
	public CorrelationsResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super(name, snapshots, datacenter);
		this.correlationsDatas = new ArrayList<>();
		CorrelationsResults.instance = this;
	}
	
	public void start() {
		this.correlationsDatas = new ArrayList<>();
		CorrelationsResults.instance = this;
	}
	
	public static CorrelationsResults getInstance() {
		return CorrelationsResults.instance;
	}
	
	public void calculate(int index) {
	}
	
	public void add(CorrelationsData correlationsData) {
		this.correlationsDatas.add(correlationsData);
	}

	@Override
	public void exportAll() {
		ExperimentConfiguration ec = StaticResources.getCE();

		String outputFolderName = ec.getExperimentOutputFolder();
		Path outputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				outputFolderName);
		File outputFolder = outputPath.toFile();
		outputFolder.mkdir();
		
		Path exportFile = Paths.get(outputPath.toString(), ec.getName()+"_correlations"+".csv");
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(exportFile.toString()));
			writer.write(CorrelationsData.CSVHeader + "\n");
			for(CorrelationsData correlationsData : correlationsDatas) {
				writer.write(correlationsData.getCSVLine() + "\n");
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void export(int i) {
	}
}
