package svila.planetlabNetwork.results;

import java.util.List;

import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.Snapshot;

public class ResultsManager {
	public static ExperimentResultsStream experimentsResultsStream;
	
	public static void init(List<Snapshot> snapshots, PowerDatacenter datacenter) {
		experimentsResultsStream = new ExperimentResultsStream(snapshots, datacenter);
		experimentsResultsStream.loadDefaultExperiments();
	}
}
