package svila.planetlabNetwork.results;

import java.util.List;

import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;

public class ExperimentResultsStream extends ExperimentResults {
	int purgeOffset;
	int currentIndex;
	int steps;
	MainResults mr;
	
	public ExperimentResultsStream(List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super();

		this.snapshots = snapshots;
		this.datacenter = datacenter;
		this.ec = StaticResources.getCE();
		this.steps = (int) (this.ec.getSimulationTimeLimit() / 300);
		if(this.ec.getSimulationTimeLimit() % 300 == 0) {
			this.steps = this.steps - 1;
		}
		this.currentIndex = -1;
		this.purgeOffset = 10;
	}
	
	public void addTimeResults(long startTime, long endTime, double lastClock) {
		TimeResults tr = new TimeResults("timeResults", snapshots, datacenter);
		tr.setStartTime(startTime);
		tr.setEndTime(endTime);
		tr.setLastClock(lastClock);
		mr.addResults(tr);
	}
	
	public void addNetworkResults() {
		mr.addResults(new NetworkResults("networkResults", snapshots, datacenter, steps));
		mr.addResults(new NetworkPhysicalResults("networkPhysicalResults", snapshots, datacenter, steps));
	}
	
	@Override
	public void loadDefaultExperiments() {
		mr = new MainResults("main", snapshots, datacenter);
		mr.addResults(new CPUResults("cpuResults", snapshots, datacenter, steps));
		mr.addResults(new RAMResults("ramResults", snapshots, datacenter, steps));
		mr.addResults(new HelperResults("cloudsimResults", snapshots, datacenter));
		mr.addResults(new SLAResults("slaResults", snapshots, datacenter, steps));
		mr.addResults(new SnapshotResults("snapshotResults", snapshots, datacenter, steps));	
		
		results.add(mr);
		
		if(ec.isOutputFullTrace()) {
			results.add(new FullTraceResults("fullTraceResults", snapshots, datacenter));
		}
		
		if(ec.isOutputCSVTrace()) {
			results.add(new HistoricHostsResults("historicHostsResults", snapshots, datacenter));
			results.add(new HistoricHostsResultsEnhanced("historicHostsResultsEnhanced", snapshots, datacenter));
			results.add(new HistoricVMResults("historicVMResults", snapshots, datacenter));
			results.add(new HistoricVMResultsEnhanced("historicVMResultsEnhanced", snapshots, datacenter));
		}
		results.add(new MigrationResults("migrationResults", snapshots, datacenter));

		if(ec.isOutputGraphTrace()) {
			results.add(new GraphResults("graphResults", snapshots, datacenter));
		}
		
		CorrelationsResults cr = CorrelationsResults.getInstance();
		if(cr != null) {
			cr.init("CorrelationsResults", snapshots, datacenter);
			results.add(cr);
		}
	}
	
	public void update() {
		this.currentIndex++;
		//System.out.println("Updating step " + currentIndex);
		this.checkPendingResults();
		this.calculateCurrentResults();
		this.exportCurrentResults();
		this.tryPurgeData();
	}
	
	private void checkPendingResults() {
		if(currentIndex == 0) {
			this.addNetworkResults();
		}
	}
	
	private void calculateCurrentResults() {
		for(ExportableExperimentResults experiment : results) {
			experiment.calculate(currentIndex);
		}
	}
	
	private void exportCurrentResults() {
		for(ExportableExperimentResults experiment : results) {
			experiment.export(currentIndex);
		}
	}
	
	private void tryPurgeData() {
		int purgeIndex = currentIndex - purgeOffset;
		if(purgeIndex >= 0) {
			purgeData(purgeIndex);
		}
	}
	
	private void purgeData(int purgeIndex) {
		Snapshot snapshotToPurge = snapshots.get(purgeIndex);
		snapshotToPurge.setLogicalNetwork(null);
		snapshotToPurge.setPathsGraph(null);
		snapshotToPurge.setPhysicalNetwork(null);
		snapshotToPurge.setRemainingBWGraph(null);
		snapshotToPurge.setAggregatedBWGraph(null);
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
}
