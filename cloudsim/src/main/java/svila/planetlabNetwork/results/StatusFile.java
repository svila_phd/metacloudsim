package svila.planetlabNetwork.results;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import org.apache.commons.lang3.exception.ExceptionUtils;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.StaticResources;

public class StatusFile {
	
	public static StatusFile reference;
	Path folderPath;
	String filename = "status.csv";
	File outputFilepath;
	Writer writer;
	ExperimentConfiguration ec;
	boolean error;
	Instant start;
	
	public StatusFile() {
		if(reference == null) {
			StatusFile.reference = this;
		}
		setFilename();
	}
	
	private void setFilename() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
		filename = "status_" + formatter.format(date) + ".csv";
	}
	
	public void generate(String folderName) {
		File currentPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				folderName).toFile();
		if(!currentPath.exists()) {
			currentPath.mkdir();
		}
		outputFilepath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				folderName, filename).toFile();
		
		//if(outputFilepath.exists()) {
		//	outputFilepath.delete();
		//}

		try {
			writer = new BufferedWriter(new FileWriter(outputFilepath.toString()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setCurrentExperiment(ExperimentConfiguration ec) {
		this.ec = ec;
		this.error = false;
		this.start = Instant.now();
	}
	
	public void writeStatus(String status) {
		try {
			writer.write(String.format("%s,%s,%s,%s\n", start, Instant.now(), ec.getName(), status));
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendError(Exception e) {
		writeStatus('"' + ExceptionUtils.getStackTrace(e) + "\n" + ec.toString() + '"');
		this.error = true;
		e.printStackTrace();
	}
	
	public void sendOk() {
		writeStatus("ok");
		System.out.println("ok");
	}
	
	public boolean isError() {
		return this.error;
	}
	
	public void close() {
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
