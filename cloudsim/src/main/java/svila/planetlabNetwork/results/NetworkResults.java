package svila.planetlabNetwork.results;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang3.tuple.Pair;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerHost;
import org.json.JSONObject;

import svila.planetlabNetwork.Graph;
import svila.planetlabNetwork.Interaction;
import svila.planetlabNetwork.InteractionsContainer;
import svila.planetlabNetwork.Link;
import svila.planetlabNetwork.PlanetLabNetworkRunner;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.TopologyHostDistanceManager;
import svila.planetlabNetwork.VMLoadBalancer;

public class NetworkResults extends ExperimentResultsAbs implements java.io.Serializable {

	public static NetworkResults instance;
	
	private static final long serialVersionUID = 1L;
	int[] numSaturatedLinksPerSnapshot;
	int totalSaturatedLinks;
	float[] saturatedDataPerSnapshot;
	float totalSaturatedData;
	int numLinks;
	int[] numUsedLinksPerSnapshot;
	int[] numPathsPerSnapshot;
	int[] numOverusedPathsPerSnapshot;
	float[] BWUsedPerSnapshot;
	float totalBWUsed;
	int[] numMigrationsPerSnapshot;
	double[] percOfSaturatedLinksPerSnapshot;
	double avPercSaturatedLinks;
	double[] percOfSaturatedDataPerSnapshot;
	double avPercSaturatedData;
	double[] migratedRAMPerSnapshot;
	double totalMigratedRAM;
	double[] RAMInBWPerSnapshot;
	double totalRAMInBW;
	double[] timeMigratingPerSnapshot;
	double totalTimeMigrating;
	double[] timeMigratingPerVm;
	Integer[] migrationDistances;
	double averageMigrationDistances;
	int steps;
	double[] numIntercommunicationsPerSnapshot;
	double[] numIntracommunicationsPerSnapshot;
	int numInteractions;
	double[] dataFromIntercommunicationsPerSnapshot;
	double[] dataFromIntracommunicationsPerSnapshot;
	double[] dataFromCommunicationsPerSnapshot;
	double[] percNumIntercommunicationsPerSnapshot;
	double[] percNumIntracommunicationsPerSnapshot;
	double[] percDataIntercommunicationsPerSnapshot;
	double[] percDataIntracommunicationsPerSnapshot;
	double[] communicationEntropyPerSnapshot;
	double[] bandwidthEntropyPerSnapshot;

	List<Integer> migrationDistancesList;
	
	double energyMigrations;
	double energyMigrationsPaths;
	
	public NetworkResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter, int steps) {
		super(name, snapshots, datacenter);
		this.steps = steps;
		this.numSaturatedLinksPerSnapshot = new int[steps];
		this.totalSaturatedData = 0f;
		this.saturatedDataPerSnapshot = new float[steps];
		this.totalSaturatedLinks = 0;
		this.numUsedLinksPerSnapshot = new int[steps];
		this.numPathsPerSnapshot = new int[steps];
		this.numOverusedPathsPerSnapshot = new int[steps];
		this.BWUsedPerSnapshot = new float[steps];
		this.totalBWUsed = 0f;
		this.numLinks = calculateNumLinks();
		this.numMigrationsPerSnapshot = new int[steps];
		this.percOfSaturatedLinksPerSnapshot = new double[steps];
		this.avPercSaturatedLinks = 0.0;
		this.percOfSaturatedDataPerSnapshot = new double[steps];
		this.avPercSaturatedData = 0.0;
		this.migratedRAMPerSnapshot = new double[steps];
		this.totalMigratedRAM = 0.0;
		this.RAMInBWPerSnapshot = new double[steps];
		this.totalRAMInBW = 0.0;
		this.timeMigratingPerSnapshot = new double[steps];
		this.totalTimeMigrating = 0.0;
		//this.timeMigratingPerVm = new double[datacenter.getVmList().size()];
		this.timeMigratingPerVm = new double[VMLoadBalancer.allVmList.size()];
		this.migrationDistancesList = new ArrayList<>();
		this.numIntercommunicationsPerSnapshot = new double[steps];
		this.numIntracommunicationsPerSnapshot = new double[steps];
		this.numInteractions = 0;
		this.dataFromIntercommunicationsPerSnapshot = new double[steps];
		this.dataFromIntracommunicationsPerSnapshot = new double[steps];
		this.dataFromCommunicationsPerSnapshot = new double[steps];
		this.percNumIntercommunicationsPerSnapshot = new double[steps];
		this.percNumIntracommunicationsPerSnapshot = new double[steps];
		this.percDataIntercommunicationsPerSnapshot = new double[steps];
		this.percDataIntracommunicationsPerSnapshot = new double[steps];
		this.communicationEntropyPerSnapshot = new double[steps];
		this.bandwidthEntropyPerSnapshot = new double[steps];
		
		this.energyMigrations = 0.0;
		this.energyMigrationsPaths = 0.0;
		//if(instance == null) {
		instance = this;
		//}
	}
	
	public boolean isValidLink(Link link) {
		return true;
	}
	
	protected int calculateNumLinks() {
		int count = 0;
		for(Link link : snapshots.get(0).getAggregatedBWGraph().getLinks()) {
			if(isValidLink(link)) {
				count++;
			}
		}
		return count / 2; // Split by 2 due to links are counted in both sides
	}
	
	@Override
	public void calculateAll() {
		postCalculate();
	}

	public void calculate(int i) {
		Snapshot snapshot = snapshots.get(i);
		Graph remainingBWGraph = snapshot.getRemainingBWGraph();
		Set<Link> links = remainingBWGraph.getLinks();
		int currentSaturatedLinks = 0;
		float currentSaturatedData = 0f;

		for(Link link : links) {
			if(!isValidLink(link)) {
				continue;
			}

			if(link.getWeight() < 0) {
				currentSaturatedLinks++;
				currentSaturatedData += Math.abs(link.getWeight());
			}
		}
		currentSaturatedLinks = currentSaturatedLinks/2;
		currentSaturatedData = currentSaturatedData/2;
		this.numSaturatedLinksPerSnapshot[i] = currentSaturatedLinks;
		this.saturatedDataPerSnapshot[i] = currentSaturatedData;
		this.totalSaturatedLinks += currentSaturatedLinks;
		this.totalSaturatedData += currentSaturatedData;

		this.numUsedLinksPerSnapshot[i] = getNumUsedLinks(snapshot);
		this.numPathsPerSnapshot[i] = getNumPaths(snapshot);
		this.numOverusedPathsPerSnapshot[i] = getNumOverusedPaths(snapshot);
		this.BWUsedPerSnapshot[i] = getBWUsed(snapshot);
		this.totalBWUsed += this.BWUsedPerSnapshot[i];
		// A la funció calculateMigrationData
//		this.numMigrationsPerSnapshot[i] = snapshot.getMigrationMap().size();

		if(this.numUsedLinksPerSnapshot[i] == 0) {
			this.percOfSaturatedLinksPerSnapshot[i] = 0.0;
		} else {
			this.percOfSaturatedLinksPerSnapshot[i] = (double)this.numSaturatedLinksPerSnapshot[i] / this.numUsedLinksPerSnapshot[i];
		}

		this.avPercSaturatedLinks += this.percOfSaturatedLinksPerSnapshot[i];

		if(this.BWUsedPerSnapshot[i] == 0.0) {
			this.percOfSaturatedDataPerSnapshot[i] = 0.0;
		} else {
			this.percOfSaturatedDataPerSnapshot[i] = this.saturatedDataPerSnapshot[i] / this.BWUsedPerSnapshot[i];
		}
		this.avPercSaturatedData += this.percOfSaturatedDataPerSnapshot[i];

		// A la funció calculateMigrationData
//		for(Vm currentVm : snapshots.get(i).getMigrationMap().keySet()) {
//			this.migratedRAMPerSnapshot[i] += currentVm.getRam();
//			this.totalMigratedRAM += currentVm.getRam();
//		}
		
		// Original comments about migration time
		// we use BW / 2 to model BW available for migration purposes, the other
		// half of BW is for VM communication
		// around 16 seconds for 1024 MB using 1 Gbit/s network
		//send(
		//		getId(),
		//		vm.getRam() / ((double) targetHost.getBw() / (2 * 8000)),
		//		CloudSimTags.VM_MIGRATE,
		//		migrate);
		
		//for(int i=0; i<snapshots.size(); i++) {

		
		
		this.numInteractions = this.datacenter.datacenterBroker.getInteractionsContainer().getInteractions().size();
		List<Interaction> interactions = this.datacenter.datacenterBroker.getInteractionsContainer().getInteractions();
		Map<Integer, Integer> vmHostAllocationMap = new HashMap<>();
		for(Vm vm : this.datacenter.getVmList()) {
			vmHostAllocationMap.put(vm.getNetworkId(), vm.getHost().getNetworkId());
		}
		for(Interaction interaction : interactions) {
			double currentDataInteraction = interaction.getWeight(snapshot.getSnapshotTime());
			if(vmHostAllocationMap.get(interaction.getSourceId()) ==
					vmHostAllocationMap.get(interaction.getEndId())) {
				this.numIntracommunicationsPerSnapshot[i]++;
				this.dataFromIntracommunicationsPerSnapshot[i] += currentDataInteraction;
			} else {
				this.numIntercommunicationsPerSnapshot[i]++;
				this.dataFromIntercommunicationsPerSnapshot[i] += currentDataInteraction;
			}
			this.dataFromCommunicationsPerSnapshot[i] += currentDataInteraction;

		}
		
		if(this.numInteractions == 0) {
			this.percNumIntracommunicationsPerSnapshot[i] = 0.0;
			this.percNumIntercommunicationsPerSnapshot[i] = 0.0;
		} else {
			this.percNumIntracommunicationsPerSnapshot[i] = (double)this.numIntracommunicationsPerSnapshot[i] / this.numInteractions;
			this.percNumIntercommunicationsPerSnapshot[i] = (double)this.numIntercommunicationsPerSnapshot[i] / this.numInteractions;
		}
		
		if(this.dataFromCommunicationsPerSnapshot[i] == 0.0) {
			this.percDataIntracommunicationsPerSnapshot[i] = 0.0;
			this.percDataIntercommunicationsPerSnapshot[i] = 0.0;
		} else {
			this.percDataIntracommunicationsPerSnapshot[i] = this.dataFromIntracommunicationsPerSnapshot[i] / this.dataFromCommunicationsPerSnapshot[i];
			this.percDataIntercommunicationsPerSnapshot[i] = this.dataFromIntercommunicationsPerSnapshot[i] / this.dataFromCommunicationsPerSnapshot[i];
		}
		
		this.communicationEntropyPerSnapshot[i] = percNumIntercommunicationsPerSnapshot[i];
		this.bandwidthEntropyPerSnapshot[i] = percDataIntercommunicationsPerSnapshot[i];
	}
	
	public void calculateMigrationData(Snapshot snapshot, int i) {
		this.numMigrationsPerSnapshot[i] = snapshot.getMigrationMap().size();
		
		for(Vm currentVm : snapshots.get(i).getMigrationMap().keySet()) {
			this.migratedRAMPerSnapshot[i] += currentVm.getRam();
			this.totalMigratedRAM += currentVm.getRam();
		}
		
		for(Entry<Vm,Pair<PowerHost,PowerHost>> entry : snapshots.get(i).getEnhancedMigrationMap().entrySet()) {
			Vm currentVm = entry.getKey();
			int currentVmId = currentVm.getId();
			//double currentMigrationTime = currentVm.getRam() / (2 * 8000.0); // Same as original code
			// 8000 is 8 * 1000, 1000 bytes more or less 8000 bits
			double currentMigrationTime = currentVm.getRam() * 0.016;
			this.timeMigratingPerVm[currentVmId] += currentMigrationTime;
			this.timeMigratingPerSnapshot[i] += currentMigrationTime;
			this.totalTimeMigrating += currentMigrationTime;
			Pair<PowerHost, PowerHost> hosts = entry.getValue();
			int distance = TopologyHostDistanceManager.getShortestDistanceBetweenTwoHostNetworkIds(
					hosts.getLeft().getNetworkId(),
					hosts.getRight().getNetworkId()
					);
			migrationDistancesList.add(distance);
			double currentRAMinBW = distance * currentVm.getRam();
			this.RAMInBWPerSnapshot[i] += currentRAMinBW;
			this.totalRAMInBW += currentRAMinBW;
		}
	}
	
	public void postCalculate() {
		this.avPercSaturatedLinks = this.avPercSaturatedLinks / steps;
		this.avPercSaturatedData = this.avPercSaturatedData / steps;
		
		this.migrationDistances = migrationDistancesList.toArray(new Integer[0]);
		double sumMigrationDistances = 0.0;
		for(int distance : this.migrationDistances) {
			sumMigrationDistances += distance;
		}
		
		if(this.migrationDistances.length == 0) {
			this.averageMigrationDistances = 0.0;
		} else {
			this.averageMigrationDistances = sumMigrationDistances / this.migrationDistances.length;
		}
		
		this.energyMigrations = this.totalMigratedRAM * (1.7892 + 1.7596);
		this.energyMigrationsPaths = this.totalRAMInBW * (1.7892 + 1.7596);
		this.energyMigrations = this.energyMigrations / (3600 * 1000); // J (Ws) to kWh
		this.energyMigrationsPaths = this.energyMigrationsPaths / (3600 * 1000); // J (Ws) to kWh
	}
	
	protected float getBWUsed(Snapshot snapshot) {
		float BW = 0f;
		
		for(Link link : snapshot.getAggregatedBWGraph().getLinks()) {
			if(!isValidLink(link)) {
				continue;
			}
			BW += link.getWeight();
		}
		
		return BW / 2;
	}
	
	protected int getNumUsedLinks(Snapshot snapshot) {
		Graph bwGraph = snapshot.getAggregatedBWGraph();
		int numUsedLinks = 0;
		for(Link link : bwGraph.getLinks()) {
			if(!isValidLink(link)) {
				continue;
			}
			if(link.getWeight() != 0f) {
				numUsedLinks++;
			}
		}
		
		return numUsedLinks/2;
	}
	
	protected int getNumPaths(Snapshot snapshot) {
		return snapshot.getPathsGraph().getPaths().size();
	}
	
	protected int getNumOverusedPaths(Snapshot snapshot) {
		
		int numOverusedPaths = 0;
		Graph remainingBW = snapshot.getRemainingBWGraph();
		Set<svila.planetlabNetwork.Path> paths = snapshot.getPathsGraph().getPaths();
		for(svila.planetlabNetwork.Path path : paths) {
			for(Link link : path.getLinks()) {
				if(!isValidLink(link)) {
					continue;
				}
				Link linkWithRemainingWeight = remainingBW.getLinkFromNodes(link.getSourceNode(), link.getEndNode());
				if(linkWithRemainingWeight.getWeight() < 0f) {
					numOverusedPaths++;
					break;
				}
			}
		}
		
		return numOverusedPaths;
	}
	
	/*
	 * public List<Double> getCommunicationDistances() {
		List<Double> distances = new ArrayList<>();
		double distance;
		for(Interaction interaction : interactions) {
			if(!vmMap.containsKey(interaction.getSourceId()) ||
					!vmMap.containsKey(interaction.getEndId())) {
				continue;
			}
			distance = TopologyHostDistanceManager.
					getShortestDistanceBetweenTwoHostNetworkIds(
							vmMap.get(interaction.getSourceId()).host.netId,
							vmMap.get(interaction.getEndId()).host.netId
					);
			distances.add(new Double(distance));
		}
		Collections.sort(distances);
		return distances;
	}
	
	public double getAverageCommunicationDistance() {
		return getCommunicationDistances().stream().mapToDouble(d -> d).average().orElse(0.0);
	}
	
	public int getNumCommunicationsInSameHost() {
		int count = 0;
		for(Interaction interaction : interactions) {
			if(!vmMap.containsKey(interaction.getSourceId()) ||
					!vmMap.containsKey(interaction.getEndId())) {
				continue;
			}
			if(vmMap.get(interaction.getSourceId()).host.netId == vmMap.get(interaction.getEndId()).host.netId) {
				count++;
			}
		}
		return count;
	}
	
	public double getPercNumCommunicationsInSameHost() {
		double num = getNumCommunicationsInSameHost();
		return num / interactions.size();
	}
	 * 
	 * 
	 */

	public int[] getNumSaturatedLinksPerSnapshot() {
		return numSaturatedLinksPerSnapshot;
	}

	public void setNumSaturatedLinksPerSnapshot(int[] numSaturatedLinksPerSnapshot) {
		this.numSaturatedLinksPerSnapshot = numSaturatedLinksPerSnapshot;
	}

	public int getTotalSaturatedLinks() {
		return totalSaturatedLinks;
	}

	public void setTotalSaturatedLinks(int totalSaturatedLinks) {
		this.totalSaturatedLinks = totalSaturatedLinks;
	}

	public float[] getSaturatedDataPerSnapshot() {
		return saturatedDataPerSnapshot;
	}

	public void setSaturatedDataPerSnapshot(float[] saturatedDataPerSnapshot) {
		this.saturatedDataPerSnapshot = saturatedDataPerSnapshot;
	}

	public float getTotalSaturatedData() {
		return totalSaturatedData;
	}

	public void setTotalSaturatedData(float totalSaturatedData) {
		this.totalSaturatedData = totalSaturatedData;
	}

	public int getNumLinks() {
		return numLinks;
	}

	public void setNumLinks(int numLinks) {
		this.numLinks = numLinks;
	}

	public int[] getNumUsedLinksPerSnapshot() {
		return numUsedLinksPerSnapshot;
	}

	public void setNumUsedLinksPerSnapshot(int[] numUsedLinksPerSnapshot) {
		this.numUsedLinksPerSnapshot = numUsedLinksPerSnapshot;
	}

	public int[] getNumPathsPerSnapshot() {
		return numPathsPerSnapshot;
	}

	public void setNumPathsPerSnapshot(int[] numPathsPerSnapshot) {
		this.numPathsPerSnapshot = numPathsPerSnapshot;
	}

	public int[] getNumOverusedPathsPerSnapshot() {
		return numOverusedPathsPerSnapshot;
	}

	public void setNumOverusedPathsPerSnapshot(int[] numOverusedPathsPerSnapshot) {
		this.numOverusedPathsPerSnapshot = numOverusedPathsPerSnapshot;
	}

	public float[] getBWUsedPerSnapshot() {
		return BWUsedPerSnapshot;
	}

	public void setBWUsedPerSnapshot(float[] bWUsedPerSnapshot) {
		BWUsedPerSnapshot = bWUsedPerSnapshot;
	}

	public float getTotalBWUsed() {
		return totalBWUsed;
	}

	public void setTotalBWUsed(float totalBWUsed) {
		this.totalBWUsed = totalBWUsed;
	}

	public int[] getNumMigrationsPerSnapshot() {
		return numMigrationsPerSnapshot;
	}

	public void setNumMigrationsPerSnapshot(int[] numMigrationsPerSnapshot) {
		this.numMigrationsPerSnapshot = numMigrationsPerSnapshot;
	}

	public double[] getPercOfSaturatedLinksPerSnapshot() {
		return percOfSaturatedLinksPerSnapshot;
	}

	public void setPercOfSaturatedLinksPerSnapshot(double[] percOfSaturatedLinksPerSnapshot) {
		this.percOfSaturatedLinksPerSnapshot = percOfSaturatedLinksPerSnapshot;
	}

	public double getAvPercSaturatedLinks() {
		return avPercSaturatedLinks;
	}

	public void setAvPercSaturatedLinks(double avPercSaturatedLinks) {
		this.avPercSaturatedLinks = avPercSaturatedLinks;
	}

	public double[] getPercOfSaturatedDataPerSnapshot() {
		return percOfSaturatedDataPerSnapshot;
	}

	public void setPercOfSaturatedDataPerSnapshot(double[] percOfSaturatedDataPerSnapshot) {
		this.percOfSaturatedDataPerSnapshot = percOfSaturatedDataPerSnapshot;
	}

	public double getAvPercSaturatedData() {
		return avPercSaturatedData;
	}

	public void setAvPercSaturatedData(double avPercSaturatedData) {
		this.avPercSaturatedData = avPercSaturatedData;
	}

	public double[] getMigratedRAMPerSnapshot() {
		return migratedRAMPerSnapshot;
	}

	public void setMigratedRAMPerSnapshot(double[] migratedRAMPerSnapshot) {
		this.migratedRAMPerSnapshot = migratedRAMPerSnapshot;
	}

	public double getTotalMigratedRAM() {
		return totalMigratedRAM;
	}

	public void setTotalMigratedRAM(double totalMigratedRAM) {
		this.totalMigratedRAM = totalMigratedRAM;
	}

	public double getTotalRAMInBW() {
		return totalRAMInBW;
	}

	public void setTotalRAMInBW(double totalRAMInBW) {
		this.totalRAMInBW = totalRAMInBW;
	}

	public double[] getTimeMigratingPerSnapshot() {
		return timeMigratingPerSnapshot;
	}

	public void setTimeMigratingPerSnapshot(double[] timeMigratingPerSnapshot) {
		this.timeMigratingPerSnapshot = timeMigratingPerSnapshot;
	}

	public double getTotalTimeMigrating() {
		return totalTimeMigrating;
	}

	public void setTotalTimeMigrating(double totalTimeMigrating) {
		this.totalTimeMigrating = totalTimeMigrating;
	}

	public double[] getTimeMigratingPerVm() {
		return timeMigratingPerVm;
	}

	public void setTimeMigratingPerVm(double[] timeMigratingPerVm) {
		this.timeMigratingPerVm = timeMigratingPerVm;
	}

	public double[] getRAMInBWPerSnapshot() {
		return RAMInBWPerSnapshot;
	}

	public void setRAMInBWPerSnapshot(double[] rAMInBWPerSnapshot) {
		RAMInBWPerSnapshot = rAMInBWPerSnapshot;
	}

	public Integer[] getMigrationDistances() {
		return migrationDistances;
	}

	public void setMigrationDistances(Integer[] migrationDistances) {
		this.migrationDistances = migrationDistances;
	}

	public double getAverageMigrationDistances() {
		return averageMigrationDistances;
	}

	public void setAverageMigrationDistances(double averageMigrationDistances) {
		this.averageMigrationDistances = averageMigrationDistances;
	}

	public double[] getNumIntercommunicationsPerSnapshot() {
		return numIntercommunicationsPerSnapshot;
	}

	public void setNumIntercommunicationsPerSnapshot(double[] numIntercommunicationsPerSnapshot) {
		this.numIntercommunicationsPerSnapshot = numIntercommunicationsPerSnapshot;
	}

	public double[] getNumIntracommunicationsPerSnapshot() {
		return numIntracommunicationsPerSnapshot;
	}

	public void setNumIntracommunicationsPerSnapshot(double[] numIntracommunicationsPerSnapshot) {
		this.numIntracommunicationsPerSnapshot = numIntracommunicationsPerSnapshot;
	}

	public int getNumInteractions() {
		return numInteractions;
	}

	public void setNumInteractions(int numInteractions) {
		this.numInteractions = numInteractions;
	}

	public double[] getDataFromIntercommunicationsPerSnapshot() {
		return dataFromIntercommunicationsPerSnapshot;
	}

	public void setDataFromIntercommunicationsPerSnapshot(double[] dataFromIntercommunicationsPerSnapshot) {
		this.dataFromIntercommunicationsPerSnapshot = dataFromIntercommunicationsPerSnapshot;
	}

	public double[] getDataFromIntracommunicationsPerSnapshot() {
		return dataFromIntracommunicationsPerSnapshot;
	}

	public void setDataFromIntracommunicationsPerSnapshot(double[] dataFromIntracommunicationsPerSnapshot) {
		this.dataFromIntracommunicationsPerSnapshot = dataFromIntracommunicationsPerSnapshot;
	}

	public double[] getDataFromCommunicationsPerSnapshot() {
		return dataFromCommunicationsPerSnapshot;
	}

	public void setDataFromCommunicationsPerSnapshot(double[] dataFromCommunicationsPerSnapshot) {
		this.dataFromCommunicationsPerSnapshot = dataFromCommunicationsPerSnapshot;
	}

	public double[] getPercNumIntercommunicationsPerSnapshot() {
		return percNumIntercommunicationsPerSnapshot;
	}

	public void setPercNumIntercommunicationsPerSnapshot(double[] percNumIntercommunicationsPerSnapshot) {
		this.percNumIntercommunicationsPerSnapshot = percNumIntercommunicationsPerSnapshot;
	}

	public double[] getPercNumIntracommunicationsPerSnapshot() {
		return percNumIntracommunicationsPerSnapshot;
	}

	public void setPercNumIntracommunicationsPerSnapshot(double[] percNumIntracommunicationsPerSnapshot) {
		this.percNumIntracommunicationsPerSnapshot = percNumIntracommunicationsPerSnapshot;
	}

	public double[] getPercDataIntercommunicationsPerSnapshot() {
		return percDataIntercommunicationsPerSnapshot;
	}

	public void setPercDataIntercommunicationsPerSnapshot(double[] percDataIntercommunicationsPerSnapshot) {
		this.percDataIntercommunicationsPerSnapshot = percDataIntercommunicationsPerSnapshot;
	}

	public double[] getPercDataIntracommunicationsPerSnapshot() {
		return percDataIntracommunicationsPerSnapshot;
	}

	public void setPercDataIntracommunicationsPerSnapshot(double[] percDataIntracommunicationsPerSnapshot) {
		this.percDataIntracommunicationsPerSnapshot = percDataIntracommunicationsPerSnapshot;
	}

	public double[] getCommunicationEntropyPerSnapshot() {
		return communicationEntropyPerSnapshot;
	}

	public void setCommunicationEntropyPerSnapshot(double[] communicationEntropyPerSnapshot) {
		this.communicationEntropyPerSnapshot = communicationEntropyPerSnapshot;
	}

	public double[] getBandwidthEntropyPerSnapshot() {
		return bandwidthEntropyPerSnapshot;
	}

	public void setBandwidthEntropyPerSnapshot(double[] bandwidthEntropyPerSnapshot) {
		this.bandwidthEntropyPerSnapshot = bandwidthEntropyPerSnapshot;
	}

	public double getEnergyMigrations() {
		return energyMigrations;
	}

	public void setEnergyMigrations(double energyMigrations) {
		this.energyMigrations = energyMigrations;
	}

	public double getEnergyMigrationsPaths() {
		return energyMigrationsPaths;
	}

	public void setEnergyMigrationsPaths(double energyMigrationsPaths) {
		this.energyMigrationsPaths = energyMigrationsPaths;
	}
	
	
}
