package svila.planetlabNetwork.results;

import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmStateHistoryEntry;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.json.JSONObject;

import svila.planetlabNetwork.Snapshot;

public class SLAResults extends ExperimentResultsAbs implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	int[] numSatisfiedVMsPerSnapshot;
	int[] numUnsatisfiedVMsPerSnapshot;
	int[] numSatisfiedHostsPerSnapshot;
	int[] numUnsatisfiedHostsPerSnapshot;
	double[] allocatedMipsPerSnapshot;
	double[] requestedMipsPerSnapshot;
	double[] nonSatisfiedMipsPerSnapshot;
	double[] percentageOfNonSatisfiedMipsPerSnapshot;
	double[] percentageOfNonSatisfiedByTotalMipsPerSnapshot;
	int totalSatisfiedVMs;
	int totalUnsatisfiedVMs;
	int totalSatisfiedHosts;
	int totalUnsatisfiedHosts;
	double totalAllocatedMips;
	double totalRequestedMips;
	double totalNonSatisfiedMips;
	double averagePercentageOfNonSatisfiedMips;
	double averagePercentageOfNonSatisfiedByTotalMips;
	double[] percentageOfUnsatVMsPerSnapshot;
	double[] percentageOfUnsatHostsPerSnapshot;
	double avPercUnsatVMs;
	double avPercUnsatHosts;
	double[] wastedMIPSPerSnapshot;
	double totalWastedMIPS;
	double[] usedRAMPerSnapshot;
	double totalUsedRAM;
	double[] wastedRAMPerSnapshot;
	double totalWastedRAM;
	double totalAllocatedMipsPerTime;
	double totalRequestedMipsPerTime;
	double totalNonSatisfiedMipsPerTime;
	double[] wastedMIPSActiveHostsPerSnapshot;
	double totalWastedMIPSActiveHosts;
	double[] activeMIPSPerSnapshot;
	double totalActiveMIPS;
	double[] ratioAllocatedByTotalMIPSPerSnapshot;

	public SLAResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter, int steps) {
		super(name, snapshots, datacenter);
		this.numSatisfiedVMsPerSnapshot = new int[steps];
		this.numUnsatisfiedVMsPerSnapshot = new int[steps];
		this.numSatisfiedHostsPerSnapshot = new int[steps];
		this.numUnsatisfiedHostsPerSnapshot = new int[steps];
		this.allocatedMipsPerSnapshot = new double[steps];
		this.requestedMipsPerSnapshot = new double[steps];
		this.nonSatisfiedMipsPerSnapshot = new double[steps];
		this.totalSatisfiedVMs = 0;
		this.totalUnsatisfiedVMs = 0;
		this.totalSatisfiedHosts = 0;
		this.totalUnsatisfiedHosts = 0;
		this.totalAllocatedMips = 0;
		this.totalRequestedMips = 0;
		this.totalNonSatisfiedMips = 0;
		this.percentageOfNonSatisfiedMipsPerSnapshot = new double[steps];
		this.averagePercentageOfNonSatisfiedMips = 0.0;
		this.percentageOfUnsatVMsPerSnapshot = new double[steps];
		this.percentageOfUnsatHostsPerSnapshot = new double[steps];
		this.avPercUnsatVMs = 0.0;
		this.avPercUnsatHosts = 0.0;
		this.percentageOfNonSatisfiedByTotalMipsPerSnapshot = new double[steps];
		this.averagePercentageOfNonSatisfiedByTotalMips = 0.0;
		this.wastedMIPSPerSnapshot = new double[steps];
		this.totalWastedMIPS = 0.0;
		this.usedRAMPerSnapshot = new double[steps];
		this.totalUsedRAM = 0.0;
		this.wastedRAMPerSnapshot = new double[steps];
		this.totalWastedRAM = 0.0;
		this.totalAllocatedMipsPerTime = 0.0;
		this.totalRequestedMipsPerTime = 0.0;
		this.totalNonSatisfiedMipsPerTime = 0.0;
		this.wastedMIPSActiveHostsPerSnapshot = new double[steps];
		this.totalWastedMIPSActiveHosts = 0.0;
		this.activeMIPSPerSnapshot = new double[steps];
		this.totalActiveMIPS = 0.0;
		this.ratioAllocatedByTotalMIPSPerSnapshot = new double[steps];
	}
	
	public void calculate(int i) {
		double time = (i+1)*300.0 + 0.1;
		for (Vm vm : datacenter.getVmList()) {
			//VmStateHistoryEntry historyEntry = vm.getStateHistory().get(i);
			VmStateHistoryEntry historyEntry = vm.getStateHistoryMap().get(time);
			if(historyEntry == null) {
				return; // TODO: TEST SERGI
			}
			double requestedMips = historyEntry.getRequestedMips();
			double allocatedMips = historyEntry.getAllocatedMips();
			this.requestedMipsPerSnapshot[i] += requestedMips;
			this.allocatedMipsPerSnapshot[i] += allocatedMips;
			this.totalRequestedMips += requestedMips;
			this.totalAllocatedMips += allocatedMips;
			if(requestedMips > allocatedMips) {
				this.numUnsatisfiedVMsPerSnapshot[i]++;
				this.totalUnsatisfiedVMs++;
				this.nonSatisfiedMipsPerSnapshot[i] += (requestedMips - allocatedMips);
				this.totalNonSatisfiedMips += (requestedMips - allocatedMips);
			} else {
				this.numSatisfiedVMsPerSnapshot[i]++;
				this.totalSatisfiedVMs++;
			}
		}		

		for(Host host : datacenter.getHostList()) {
			HostDynamicWorkload hdw = (HostDynamicWorkload) host;
			double requestedMips = hdw.getStateHistory().get(i).getRequestedMips();
			double allocatedMips = hdw.getStateHistory().get(i).getAllocatedMips();
			if(requestedMips > allocatedMips) {
				this.numUnsatisfiedHostsPerSnapshot[i]++;
				this.totalUnsatisfiedHosts++;
			} else {
				this.numSatisfiedHostsPerSnapshot[i]++;
				this.totalSatisfiedHosts++;
			}
			double wastedMIPS = host.getTotalMips() - allocatedMips;
			this.wastedMIPSPerSnapshot[i] += wastedMIPS;
			this.totalWastedMIPS += wastedMIPS;
			
			this.usedRAMPerSnapshot[i] = host.getRamProvisioner().getUsedRam();
			this.wastedRAMPerSnapshot[i] = host.getRamProvisioner().getAvailableRam();
			this.totalUsedRAM = host.getRamProvisioner().getUsedRam();
			this.totalWastedRAM = host.getRamProvisioner().getAvailableRam();
			
			if(requestedMips > 0.0) {
				this.wastedMIPSActiveHostsPerSnapshot[i] += wastedMIPS;
				this.totalWastedMIPSActiveHosts += wastedMIPS;
				
				this.activeMIPSPerSnapshot[i] += host.getTotalMips();
				this.totalActiveMIPS += host.getTotalMips();
			}
		}

		this.percentageOfNonSatisfiedMipsPerSnapshot[i] = this.nonSatisfiedMipsPerSnapshot[i] / this.allocatedMipsPerSnapshot[i];
		this.averagePercentageOfNonSatisfiedMips += this.percentageOfNonSatisfiedMipsPerSnapshot[i];

		this.percentageOfNonSatisfiedByTotalMipsPerSnapshot[i] = this.nonSatisfiedMipsPerSnapshot[i] / (this.nonSatisfiedMipsPerSnapshot[i] + this.allocatedMipsPerSnapshot[i]);
		this.averagePercentageOfNonSatisfiedByTotalMips += this.percentageOfNonSatisfiedByTotalMipsPerSnapshot[i];


		this.percentageOfUnsatVMsPerSnapshot[i] = (double)this.numUnsatisfiedVMsPerSnapshot[i] / (this.numUnsatisfiedVMsPerSnapshot[i] + this.numSatisfiedVMsPerSnapshot[i]);
		this.percentageOfUnsatHostsPerSnapshot[i] = (double)this.numUnsatisfiedHostsPerSnapshot[i] / (this.numUnsatisfiedHostsPerSnapshot[i] + this.numSatisfiedHostsPerSnapshot[i]);
		this.avPercUnsatVMs += this.percentageOfUnsatVMsPerSnapshot[i];
		this.avPercUnsatHosts += this.percentageOfUnsatHostsPerSnapshot[i];
		
		this.ratioAllocatedByTotalMIPSPerSnapshot[i] = this.allocatedMipsPerSnapshot[i] / this.activeMIPSPerSnapshot[i];
	}
	
	@Override
	public void calculateAll() {
		postCalculate();
	}
	
	@Override
	public void postCalculate() {
		this.averagePercentageOfNonSatisfiedMips = this.averagePercentageOfNonSatisfiedMips/snapshots.size();
		this.averagePercentageOfNonSatisfiedByTotalMips = this.averagePercentageOfNonSatisfiedByTotalMips/snapshots.size();
		
		this.avPercUnsatVMs = this.avPercUnsatVMs / snapshots.size();
		this.avPercUnsatHosts = this.avPercUnsatHosts / snapshots.size();
		
		this.totalAllocatedMipsPerTime = this.totalAllocatedMips * 300;
		this.totalRequestedMipsPerTime = this.totalRequestedMips * 300;
		this.totalNonSatisfiedMipsPerTime = this.totalNonSatisfiedMips * 300;
	}

	public int[] getNumSatisfiedVMsPerSnapshot() {
		return numSatisfiedVMsPerSnapshot;
	}

	public void setNumSatisfiedVMsPerSnapshot(int[] numSatisfiedVMsPerSnapshot) {
		this.numSatisfiedVMsPerSnapshot = numSatisfiedVMsPerSnapshot;
	}

	public int[] getNumUnsatisfiedVMsPerSnapshot() {
		return numUnsatisfiedVMsPerSnapshot;
	}

	public void setNumUnsatisfiedVMsPerSnapshot(int[] numUnsatisfiedVMsPerSnapshot) {
		this.numUnsatisfiedVMsPerSnapshot = numUnsatisfiedVMsPerSnapshot;
	}

	public int[] getNumSatisfiedHostsPerSnapshot() {
		return numSatisfiedHostsPerSnapshot;
	}

	public void setNumSatisfiedHostsPerSnapshot(int[] numSatisfiedHostsPerSnapshot) {
		this.numSatisfiedHostsPerSnapshot = numSatisfiedHostsPerSnapshot;
	}

	public int[] getNumUnsatisfiedHostsPerSnapshot() {
		return numUnsatisfiedHostsPerSnapshot;
	}

	public void setNumUnsatisfiedHostsPerSnapshot(int[] numUnsatisfiedHostsPerSnapshot) {
		this.numUnsatisfiedHostsPerSnapshot = numUnsatisfiedHostsPerSnapshot;
	}

	public double[] getAllocatedMipsPerSnapshot() {
		return allocatedMipsPerSnapshot;
	}

	public void setAllocatedMipsPerSnapshot(double[] allocatedMipsPerSnapshot) {
		this.allocatedMipsPerSnapshot = allocatedMipsPerSnapshot;
	}

	public double[] getRequestedMipsPerSnapshot() {
		return requestedMipsPerSnapshot;
	}

	public void setRequestedMipsPerSnapshot(double[] requestedMipsPerSnapshot) {
		this.requestedMipsPerSnapshot = requestedMipsPerSnapshot;
	}

	public double[] getNonSatisfiedMipsPerSnapshot() {
		return nonSatisfiedMipsPerSnapshot;
	}

	public void setNonSatisfiedMipsPerSnapshot(double[] nonSatisfiedMipsPerSnapshot) {
		this.nonSatisfiedMipsPerSnapshot = nonSatisfiedMipsPerSnapshot;
	}

	public double[] getPercentageOfNonSatisfiedMipsPerSnapshot() {
		return percentageOfNonSatisfiedMipsPerSnapshot;
	}

	public void setPercentageOfNonSatisfiedMipsPerSnapshot(double[] percentageOfNonSatisfiedMipsPerSnapshot) {
		this.percentageOfNonSatisfiedMipsPerSnapshot = percentageOfNonSatisfiedMipsPerSnapshot;
	}

	public int getTotalSatisfiedVMs() {
		return totalSatisfiedVMs;
	}

	public void setTotalSatisfiedVMs(int totalSatisfiedVMs) {
		this.totalSatisfiedVMs = totalSatisfiedVMs;
	}

	public int getTotalUnsatisfiedVMs() {
		return totalUnsatisfiedVMs;
	}

	public void setTotalUnsatisfiedVMs(int totalUnsatisfiedVMs) {
		this.totalUnsatisfiedVMs = totalUnsatisfiedVMs;
	}

	public int getTotalSatisfiedHosts() {
		return totalSatisfiedHosts;
	}

	public void setTotalSatisfiedHosts(int totalSatisfiedHosts) {
		this.totalSatisfiedHosts = totalSatisfiedHosts;
	}

	public int getTotalUnsatisfiedHosts() {
		return totalUnsatisfiedHosts;
	}

	public void setTotalUnsatisfiedHosts(int totalUnsatisfiedHosts) {
		this.totalUnsatisfiedHosts = totalUnsatisfiedHosts;
	}

	public double getTotalAllocatedMips() {
		return totalAllocatedMips;
	}

	public void setTotalAllocatedMips(double totalAllocatedMips) {
		this.totalAllocatedMips = totalAllocatedMips;
	}

	public double getTotalRequestedMips() {
		return totalRequestedMips;
	}

	public void setTotalRequestedMips(double totalRequestedMips) {
		this.totalRequestedMips = totalRequestedMips;
	}

	public double getTotalNonSatisfiedMips() {
		return totalNonSatisfiedMips;
	}

	public void setTotalNonSatisfiedMips(double totalNonSatisfiedMips) {
		this.totalNonSatisfiedMips = totalNonSatisfiedMips;
	}

	public double getAveragePercentageOfNonSatisfiedMips() {
		return averagePercentageOfNonSatisfiedMips;
	}

	public void setAveragePercentageOfNonSatisfiedMips(double averagePercentageOfNonSatisfiedMips) {
		this.averagePercentageOfNonSatisfiedMips = averagePercentageOfNonSatisfiedMips;
	}

	public double[] getPercentageOfUnsatVMsPerSnapshot() {
		return percentageOfUnsatVMsPerSnapshot;
	}

	public void setPercentageOfUnsatVMsPerSnapshot(double[] percentageOfUnsatVMsPerSnapshot) {
		this.percentageOfUnsatVMsPerSnapshot = percentageOfUnsatVMsPerSnapshot;
	}

	public double[] getPercentageOfUnsatHostsPerSnapshot() {
		return percentageOfUnsatHostsPerSnapshot;
	}

	public void setPercentageOfUnsatHostsPerSnapshot(double[] percentageOfUnsatHostsPerSnapshot) {
		this.percentageOfUnsatHostsPerSnapshot = percentageOfUnsatHostsPerSnapshot;
	}

	public double getAvPercUnsatVMs() {
		return avPercUnsatVMs;
	}

	public void setAvPercUnsatVMs(double avPercUnsatVMs) {
		this.avPercUnsatVMs = avPercUnsatVMs;
	}

	public double getAvPercUnsatHosts() {
		return avPercUnsatHosts;
	}

	public void setAvPercUnsatHosts(double avPercUnsatHosts) {
		this.avPercUnsatHosts = avPercUnsatHosts;
	}

	public double[] getPercentageOfNonSatisfiedByTotalMipsPerSnapshot() {
		return percentageOfNonSatisfiedByTotalMipsPerSnapshot;
	}

	public void setPercentageOfNonSatisfiedByTotalMipsPerSnapshot(double[] percentageOfNonSatisfiedByTotalMipsPerSnapshot) {
		this.percentageOfNonSatisfiedByTotalMipsPerSnapshot = percentageOfNonSatisfiedByTotalMipsPerSnapshot;
	}

	public double getAveragePercentageOfNonSatisfiedByTotalMips() {
		return averagePercentageOfNonSatisfiedByTotalMips;
	}

	public void setAveragePercentageOfNonSatisfiedByTotalMips(double averagePercentageOfNonSatisfiedByTotalMips) {
		this.averagePercentageOfNonSatisfiedByTotalMips = averagePercentageOfNonSatisfiedByTotalMips;
	}

	public double[] getWastedMIPSPerSnapshot() {
		return wastedMIPSPerSnapshot;
	}

	public void setWastedMIPSPerSnapshot(double[] wastedMIPSPerSnapshot) {
		this.wastedMIPSPerSnapshot = wastedMIPSPerSnapshot;
	}

	public double getTotalWastedMIPS() {
		return totalWastedMIPS;
	}

	public void setTotalWastedMIPS(double totalWastedMIPS) {
		this.totalWastedMIPS = totalWastedMIPS;
	}

	public double[] getUsedRAMPerSnapshot() {
		return usedRAMPerSnapshot;
	}

	public void setUsedRAMPerSnapshot(double[] usedRAMPerSnapshot) {
		this.usedRAMPerSnapshot = usedRAMPerSnapshot;
	}

	public double getTotalUsedRAM() {
		return totalUsedRAM;
	}

	public void setTotalUsedRAM(double totalUsedRAM) {
		this.totalUsedRAM = totalUsedRAM;
	}

	public double[] getWastedRAMPerSnapshot() {
		return wastedRAMPerSnapshot;
	}

	public void setWastedRAMPerSnapshot(double[] wastedRAMPerSnapshot) {
		this.wastedRAMPerSnapshot = wastedRAMPerSnapshot;
	}

	public double getTotalWastedRAM() {
		return totalWastedRAM;
	}

	public void setTotalWastedRAM(double totalWastedRAM) {
		this.totalWastedRAM = totalWastedRAM;
	}

	public double getTotalAllocatedMipsPerTime() {
		return totalAllocatedMipsPerTime;
	}

	public void setTotalAllocatedMipsPerTime(double totalAllocatedMipsPerTime) {
		this.totalAllocatedMipsPerTime = totalAllocatedMipsPerTime;
	}

	public double getTotalRequestedMipsPerTime() {
		return totalRequestedMipsPerTime;
	}

	public void setTotalRequestedMipsPerTime(double totalRequestedMipsPerTime) {
		this.totalRequestedMipsPerTime = totalRequestedMipsPerTime;
	}

	public double getTotalNonSatisfiedMipsPerTime() {
		return totalNonSatisfiedMipsPerTime;
	}

	public void setTotalNonSatisfiedMipsPerTime(double totalNonSatisfiedMipsPerTime) {
		this.totalNonSatisfiedMipsPerTime = totalNonSatisfiedMipsPerTime;
	}

	public double[] getWastedMIPSActiveHostsPerSnapshot() {
		return wastedMIPSActiveHostsPerSnapshot;
	}

	public void setWastedMIPSActiveHostsPerSnapshot(double[] wastedMIPSActiveHostsPerSnapshot) {
		this.wastedMIPSActiveHostsPerSnapshot = wastedMIPSActiveHostsPerSnapshot;
	}

	public double getTotalWastedMIPSActiveHosts() {
		return totalWastedMIPSActiveHosts;
	}

	public void setTotalWastedMIPSActiveHosts(double totalWastedMIPSActiveHosts) {
		this.totalWastedMIPSActiveHosts = totalWastedMIPSActiveHosts;
	}

	public double[] getActiveMIPSPerSnapshot() {
		return activeMIPSPerSnapshot;
	}

	public void setActiveMIPSPerSnapshot(double[] activeMIPSPerSnapshot) {
		this.activeMIPSPerSnapshot = activeMIPSPerSnapshot;
	}

	public double getTotalActiveMIPS() {
		return totalActiveMIPS;
	}

	public void setTotalActiveMIPS(double totalActiveMIPS) {
		this.totalActiveMIPS = totalActiveMIPS;
	}

	public double[] getRatioAllocatedByTotalMIPSPerSnapshot() {
		return ratioAllocatedByTotalMIPSPerSnapshot;
	}

	public void setRatioAllocatedByTotalMIPSPerSnapshot(double[] ratioAllocatedByTotalMIPSPerSnapshot) {
		this.ratioAllocatedByTotalMIPSPerSnapshot = ratioAllocatedByTotalMIPSPerSnapshot;
	}	
	
}
