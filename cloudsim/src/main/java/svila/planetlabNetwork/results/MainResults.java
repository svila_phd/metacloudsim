package svila.planetlabNetwork.results;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.json.JSONObject;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;

public class MainResults extends ExportableExperimentResults {
	List<ExperimentResultsAbs> results;
	
	public MainResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super(name, snapshots, datacenter);
		this.results = new ArrayList<>();
	}
	
	public void addResults(ExperimentResultsAbs era) {
		this.results.add(era);
	}

	@Override
	public void calculateAll() {
		for(ExperimentResultsAbs era : results) {
			era.calculateAll();
		}
	}

	@Override
	public void exportAll() {
		JSONObject root = getJSONObject();
		ExperimentConfiguration ec = StaticResources.getCE();
		String outputFolderName = ec.getExperimentOutputFolder();
		Path outputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				outputFolderName);
		File outputFolder = outputPath.toFile();
		outputFolder.mkdir();
		Path exportFile = Paths.get(outputPath.toString(), ec.getName()+"_data.json");
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(exportFile.toString()));
			root.write(writer, 4, 0);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private JSONObject getJSONObject() {
		JSONObject root = new JSONObject();
		ExperimentConfiguration ec = StaticResources.getCE();
		root.put("experimentConfiguration", ec.getJSONObject());
		for(ExperimentResultsAbs result : results) {
			root.put(result.getExperimentResultsName(), new JSONObject(result));
		}
		return root;
	}

	@Override
	public void calculate(int i) {
		for(ExperimentResultsAbs result : results) {
			result.calculate(i);
		}
	}

	@Override
	public void export(int i) {

	}
}
