package svila.planetlabNetwork.results;

public class CorrelationsData {
	int snapshot;
	int hostId;
	int vmId;
	int hostNetId;
	int vmNetId;
	double cpuCorr;
	double netCorr;
	double hVal;
	int migratedVmId;
	
	public static final String CSVHeader = "snapshot,hostId,vmId,hostNetId,vmNetId,cpuCorr,netCorr,hVal,migratedVmId";
	
	public CorrelationsData(int snapshot, int hostId, int vmId, int hostNetId, int vmNetId,
			double cpuCorr, double netCorr, double hVal, int migratedVmId) {
		this.snapshot = snapshot;
		this.hostId = hostId;
		this.vmId = vmId;
		this.hostNetId = hostNetId;
		this.vmNetId = vmNetId;
		this.cpuCorr = cpuCorr;
		this.netCorr = netCorr;
		this.hVal = hVal;
		this.migratedVmId = migratedVmId;
	}
	
	public String getCSVLine() {
		return snapshot+","+hostId+","+vmId+","+hostId+","+vmId+","+cpuCorr+","+netCorr+","+hVal+","+migratedVmId;
	}
}
