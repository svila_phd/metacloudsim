package svila.planetlabNetwork.results;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.tuple.Pair;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;

public class MigrationResults extends ExportableExperimentResults {
	static MigrationResults instance;
	List<MigrationData> migrationDatas;
	List<MigrationData> tryMigrationDatas;
	
	public MigrationResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super(name, snapshots, datacenter);
		this.migrationDatas = new ArrayList<>();
		MigrationResults.instance = this;
	}
	
	public void start() {
		this.migrationDatas = new ArrayList<>();
		MigrationResults.instance = this;
	}
	
	public static MigrationResults getInstance() {
		return MigrationResults.instance;
	}
	
	public void addtryMigrationDatas() {
		
	}
	
	@Override
	public void calculateAll() {

	}
	
	@Override
	public void calculate(int i) {

	}
	
	public void calculatePost(int i) {
		ExperimentConfiguration ec = StaticResources.getCE();
		if(!ec.isOutputCSVTrace()) {
			return;
		}
		
		Map<Vm, Pair<PowerHost, PowerHost>> migrationMap = snapshots.get(i).getEnhancedMigrationMap();
		for (Entry<Vm, Pair<PowerHost, PowerHost>> entry : migrationMap.entrySet()) {
			int vmId = entry.getKey().getId();
			int originHostId = entry.getValue().getLeft().getId();
			int destinationHostId = entry.getValue().getRight().getId();
			migrationDatas.add(new MigrationData(i, originHostId, destinationHostId, vmId));
		}
	}

	@Override
	public void exportAll() {
		ExperimentConfiguration ec = StaticResources.getCE();

		String outputFolderName = ec.getExperimentOutputFolder();
		Path outputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				outputFolderName);
		File outputFolder = outputPath.toFile();
		outputFolder.mkdir();
		
		Path exportFile = Paths.get(outputPath.toString(), ec.getName()+"_migrations"+".csv");
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(exportFile.toString()));
			writer.write(MigrationData.CSVHeader + "\n");
			for(MigrationData migrationData : this.migrationDatas) {
				writer.write(migrationData.getCSVLine() + "\n");
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void export(int i) {
		// TODO Auto-generated method stub
		
	}

}
