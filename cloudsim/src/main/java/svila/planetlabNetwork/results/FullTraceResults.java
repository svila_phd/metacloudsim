package svila.planetlabNetwork.results;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.json.JSONArray;
import org.json.JSONObject;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;

public class FullTraceResults extends ExportableExperimentResults {

	public FullTraceResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super(name, snapshots, datacenter);
	}
	
	public JSONObject getJSONObject() {
		JSONObject json = new JSONObject();
		JSONArray fullTraceJson = new JSONArray();
		for(Snapshot snapshot : snapshots) {
			fullTraceJson.put(snapshot.toJSONObject());	
		}
		json.put("fullTrace", fullTraceJson);
		return json;
	}
	
	public void exportAll() {
		JSONObject root = this.getJSONObject();
		ExperimentConfiguration ec = StaticResources.getCE();
		String outputFolderName = ec.getExperimentOutputFolder();
		Path outputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				outputFolderName);
		File outputFolder = outputPath.toFile();
		outputFolder.mkdir();
		Path exportFile = Paths.get(outputPath.toString(), ec.getName()+"_fullTrace"+".json");
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(exportFile.toString()));
			root.write(writer, 4, 0);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void calculate(int i) {

	}

	@Override
	public void export(int i) {
		// TODO Auto-generated method stub
		
	}
	
	
}
