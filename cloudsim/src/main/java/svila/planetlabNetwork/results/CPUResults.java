package svila.planetlabNetwork.results;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.rank.Median;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.util.MathUtil;
import org.json.JSONObject;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.PowerHostData;
import svila.planetlabNetwork.PowerVmData;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;

public class CPUResults extends ExperimentResultsAbs implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private int steps;
	int[] usedHostsPerSnapshot;
	int[] saturatedHostsPerSnapshot;
	int totalUsedHosts;
	int totalSaturatedHosts;
	double[] totalAvailableMIPS;
	double[] totalUsedMIPS;
	double[] totalAllocatedMIPS;
	double[] totalRequestedMIPS;
	
	double[] averageCPUPercAllocatedOfVMsPerSnapshot;
	double averageCPUPercAllocatedOfVms;
	double[] averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot;
	double averageCPUOverUsageNonAllocatedOfOverUsedVM;
	
	double[] totalFreeMIPSFromActiveHostsPerSnapshot;
	double[] averageFreeMIPSActiveHostsPerSnapshot;
	double averageFreeMIPSActiveHosts;
	
	double[] CPUDispersionOfAllHostsPerSnapshot;
	double[] CPUDispersionOfActiveHostsPerSnapshot;
	double averageCPUDispersionOfAllHosts;
	double averageCPUDispersionOfActiveHosts;
	double medianCPUDispersionOfAllHosts;
	double medianCPUDispersionOfActiveHosts;
	

	public CPUResults() {
		
	}
	
	public CPUResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter, int steps) {
		super(name, snapshots, datacenter);
		this.steps = steps;
		this.usedHostsPerSnapshot = new int[steps];
		this.saturatedHostsPerSnapshot = new int[steps];
		this.totalUsedHosts = 0;
		this.totalSaturatedHosts = 0;
		this.totalAvailableMIPS = new double[steps];
		this.totalUsedMIPS = new double[steps];
		this.totalAllocatedMIPS = new double[steps];
		this.totalRequestedMIPS = new double[steps];
		this.totalFreeMIPSFromActiveHostsPerSnapshot = new double[steps];
		this.averageFreeMIPSActiveHostsPerSnapshot = new double[steps];
		this.averageFreeMIPSActiveHosts = 0;
		
		this.averageCPUPercAllocatedOfVMsPerSnapshot = new double[steps];
		this.averageCPUPercAllocatedOfVms = 0;
		this.averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot = new double[steps];
		this.averageCPUOverUsageNonAllocatedOfOverUsedVM = 0;
		
		this.CPUDispersionOfAllHostsPerSnapshot = new double[steps];
		this.CPUDispersionOfActiveHostsPerSnapshot = new double[steps];
		this.averageCPUDispersionOfAllHosts = 0;
		this.averageCPUDispersionOfActiveHosts = 0;
		this.medianCPUDispersionOfAllHosts = 0;
		this.medianCPUDispersionOfActiveHosts = 0;
	}
	
	public void calculateAll() {
		for(int i=0; i<averageCPUPercAllocatedOfVMsPerSnapshot.length; i++) {
			this.averageCPUPercAllocatedOfVms += averageCPUPercAllocatedOfVMsPerSnapshot[i];
		}
		if(averageCPUPercAllocatedOfVMsPerSnapshot.length > 0) {
			this.averageCPUPercAllocatedOfVms /= averageCPUPercAllocatedOfVMsPerSnapshot.length;
		} else {
			this.averageCPUPercAllocatedOfVms = 0;
		}
		
		for(int i=0; i<averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot.length; i++) {
			this.averageCPUOverUsageNonAllocatedOfOverUsedVM += averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot[i];
		}
		
		if(averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot.length > 0) {
			this.averageCPUOverUsageNonAllocatedOfOverUsedVM /= averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot.length;
		} else {
			this.averageCPUOverUsageNonAllocatedOfOverUsedVM = 0;
		}
		
		
		this.averageFreeMIPSActiveHosts = Arrays.stream(averageFreeMIPSActiveHostsPerSnapshot).average().orElse(0);
	
	
		this.averageCPUDispersionOfAllHosts = Arrays.stream(CPUDispersionOfAllHostsPerSnapshot).average().orElse(0);
		this.averageCPUDispersionOfActiveHosts = Arrays.stream(CPUDispersionOfActiveHostsPerSnapshot).average().orElse(0);
		this.medianCPUDispersionOfAllHosts = MathUtil.median(CPUDispersionOfAllHostsPerSnapshot);
		this.medianCPUDispersionOfActiveHosts = MathUtil.median(CPUDispersionOfActiveHostsPerSnapshot);
	}
	
	public void calculate(int i) {
		
		ExperimentConfiguration ec = StaticResources.getCE();
		
		this.usedHostsPerSnapshot[i] = 0;
		Snapshot currentSnapshot = snapshots.get(i);
		for(PowerHostData phd : currentSnapshot.getPowerHostDataMap().values()) {
			if(phd.getVmsData().size() > 0) {
				this.usedHostsPerSnapshot[i]++;
				this.totalUsedHosts++;
				this.totalFreeMIPSFromActiveHostsPerSnapshot[i] += phd.getAvailableMips();
			}
			if(phd.getUtilization() > ec.getUtilizationThreshold()) {
				this.saturatedHostsPerSnapshot[i]++;
				this.totalSaturatedHosts++;
			}
		}
		
		this.averageFreeMIPSActiveHostsPerSnapshot[i] = this.totalFreeMIPSFromActiveHostsPerSnapshot[i] / this.usedHostsPerSnapshot[i];

		for(PowerHostData phd : currentSnapshot.getPowerHostDataMap().values()) {
			totalAvailableMIPS[i] += phd.getTotalAvailableMips();
			totalUsedMIPS[i] += phd.getUsedMips();
			totalAllocatedMIPS[i] += phd.getAllocatedMips();
			totalRequestedMIPS[i] += phd.getRequestedMips();
		}	
		
		
		int overUsedVMs = 0;
		List<Vm> vmList = datacenter.getVmList();
		for(Vm vm : vmList) {
			double requestedMips = vm.getStateHistoryMap().get(snapshots.get(i).getSnapshotTime()).getRequestedMips();
			double allocatedMips = vm.getStateHistoryMap().get(snapshots.get(i).getSnapshotTime()).getAllocatedMips();

			VMData vmData = new VMData(vm.getId(), i, requestedMips, allocatedMips, vm.getMips());
			double percAllocatedMIPS;
			if(vmData.requestedMIPS > 0) {
				percAllocatedMIPS = vmData.allocatedMIPS / vmData.requestedMIPS;
			} else {
				percAllocatedMIPS = 1;
			}
			this.averageCPUPercAllocatedOfVMsPerSnapshot[i] += percAllocatedMIPS;
			if(vmData.overusedMIPS > 0 && vmData.requestedMIPS > 0) {
				overUsedVMs++;
				double percOverNonAllocated = vmData.allocatedMIPS / vmData.requestedMIPS;
				this.averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot[i] += percOverNonAllocated;
			}
		}
		this.averageCPUPercAllocatedOfVMsPerSnapshot[i] /= vmList.size();
		if(overUsedVMs > 0) {
			this.averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot[i] /= overUsedVMs;
		} else {
			this.averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot[i] = 0;
		}
		
		
		List<Double> allHostsUtilization = new ArrayList<>();
		List<Double> activeHostsUtilization = new ArrayList<>();
		for(PowerHostData phd : currentSnapshot.getPowerHostDataMap().values()) {
			if(phd.getVmsData().size() > 0) {
				activeHostsUtilization.add(phd.getUtilization());
			}
			allHostsUtilization.add(phd.getUtilization());
		}
		
		if(activeHostsUtilization.size() > 1) {
			this.CPUDispersionOfActiveHostsPerSnapshot[i] = MathUtil.stDev(activeHostsUtilization);
		} else {
			this.CPUDispersionOfActiveHostsPerSnapshot[i] = 0.0;
		}
		this.CPUDispersionOfAllHostsPerSnapshot[i] = MathUtil.stDev(allHostsUtilization);
		
		// Continuar
		for(PowerHostData phd : currentSnapshot.getPowerHostDataMap().values()) {
			// Mirar estat últim snapshot i contar encès, apagat o res
		}

	}

	public int[] getUsedHostsPerSnapshot() {
		return usedHostsPerSnapshot;
	}

	public void setUsedHostsPerSnapshot(int[] usedHostsPerSnapshot) {
		this.usedHostsPerSnapshot = usedHostsPerSnapshot;
	}

	public int[] getSaturatedHostsPerSnapshot() {
		return saturatedHostsPerSnapshot;
	}

	public void setSaturatedHostsPerSnapshot(int[] saturatedHostsPerSnapshot) {
		this.saturatedHostsPerSnapshot = saturatedHostsPerSnapshot;
	}

	public int getTotalUsedHosts() {
		return totalUsedHosts;
	}

	public void setTotalUsedHosts(int totalUsedHosts) {
		this.totalUsedHosts = totalUsedHosts;
	}

	public int getTotalSaturatedHosts() {
		return totalSaturatedHosts;
	}

	public void setTotalSaturatedHosts(int totalSaturatedHosts) {
		this.totalSaturatedHosts = totalSaturatedHosts;
	}

	public double[] getTotalAvailableMIPS() {
		return totalAvailableMIPS;
	}

	public void setTotalAvailableMIPS(double[] totalAvailableMIPS) {
		this.totalAvailableMIPS = totalAvailableMIPS;
	}

	public double[] getTotalUsedMIPS() {
		return totalUsedMIPS;
	}

	public void setTotalUsedMIPS(double[] totalUsedMIPS) {
		this.totalUsedMIPS = totalUsedMIPS;
	}

	public double[] getTotalAllocatedMIPS() {
		return totalAllocatedMIPS;
	}

	public void setTotalAllocatedMIPS(double[] totalAllocatedMIPS) {
		this.totalAllocatedMIPS = totalAllocatedMIPS;
	}

	public double[] getTotalRequestedMIPS() {
		return totalRequestedMIPS;
	}

	public void setTotalRequestedMIPS(double[] totalRequestedMIPS) {
		this.totalRequestedMIPS = totalRequestedMIPS;
	}

	public double[] getAverageCPUPercAllocatedOfVMsPerSnapshot() {
		return averageCPUPercAllocatedOfVMsPerSnapshot;
	}

	public void setAverageCPUPercAllocatedOfVMsPerSnapshot(double[] averageCPUPercAllocatedOfVMsPerSnapshot) {
		this.averageCPUPercAllocatedOfVMsPerSnapshot = averageCPUPercAllocatedOfVMsPerSnapshot;
	}

	public double getAverageCPUPercAllocatedOfVms() {
		return averageCPUPercAllocatedOfVms;
	}

	public void setAverageCPUPercAllocatedOfVms(double averageCPUPercAllocatedOfVms) {
		this.averageCPUPercAllocatedOfVms = averageCPUPercAllocatedOfVms;
	}

	public double[] getAverageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot() {
		return averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot;
	}

	public void setAverageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot(
			double[] averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot) {
		this.averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot = averageCPUOverUsageNonAllocatedOfOverUsedVMsPerSnapshot;
	}

	public double getAverageCPUOverUsageNonAllocatedOfOverUsedVM() {
		return averageCPUOverUsageNonAllocatedOfOverUsedVM;
	}

	public void setAverageCPUOverUsageNonAllocatedOfOverUsedVM(double averageCPUOverUsageNonAllocatedOfOverUsedVM) {
		this.averageCPUOverUsageNonAllocatedOfOverUsedVM = averageCPUOverUsageNonAllocatedOfOverUsedVM;
	}

	public double[] getAverageFreeMIPSActiveHostsPerSnapshot() {
		return averageFreeMIPSActiveHostsPerSnapshot;
	}

	public void setAverageFreeMIPSActiveHostsPerSnapshot(double[] averageFreeMIPSActiveHostsPerSnapshot) {
		this.averageFreeMIPSActiveHostsPerSnapshot = averageFreeMIPSActiveHostsPerSnapshot;
	}

	public double getAverageFreeMIPSActiveHosts() {
		return averageFreeMIPSActiveHosts;
	}

	public void setAverageFreeMIPSActiveHosts(double averageFreeMIPSActiveHosts) {
		this.averageFreeMIPSActiveHosts = averageFreeMIPSActiveHosts;
	}

	public double[] getTotalFreeMIPSFromActiveHostsPerSnapshot() {
		return totalFreeMIPSFromActiveHostsPerSnapshot;
	}

	public void setTotalFreeMIPSFromActiveHostsPerSnapshot(double[] totalFreeMIPSFromActiveHostsPerSnapshot) {
		this.totalFreeMIPSFromActiveHostsPerSnapshot = totalFreeMIPSFromActiveHostsPerSnapshot;
	}

	public double[] getCPUDispersionOfAllHostsPerSnapshot() {
		return CPUDispersionOfAllHostsPerSnapshot;
	}

	public void setCPUDispersionOfAllHostsPerSnapshot(double[] cPUDispersionOfAllHostsPerSnapshot) {
		CPUDispersionOfAllHostsPerSnapshot = cPUDispersionOfAllHostsPerSnapshot;
	}

	public double[] getCPUDispersionOfActiveHostsPerSnapshot() {
		return CPUDispersionOfActiveHostsPerSnapshot;
	}

	public void setCPUDispersionOfActiveHostsPerSnapshot(double[] cPUDispersionOfActiveHostsPerSnapshot) {
		CPUDispersionOfActiveHostsPerSnapshot = cPUDispersionOfActiveHostsPerSnapshot;
	}

	public double getAverageCPUDispersionOfAllHosts() {
		return averageCPUDispersionOfAllHosts;
	}

	public void setAverageCPUDispersionOfAllHosts(double averageCPUDispersionOfAllHosts) {
		this.averageCPUDispersionOfAllHosts = averageCPUDispersionOfAllHosts;
	}

	public double getAverageCPUDispersionOfActiveHosts() {
		return averageCPUDispersionOfActiveHosts;
	}

	public void setAverageCPUDispersionOfActiveHosts(double averageCPUDispersionOfActiveHosts) {
		this.averageCPUDispersionOfActiveHosts = averageCPUDispersionOfActiveHosts;
	}

	public double getMedianCPUDispersionOfAllHosts() {
		return medianCPUDispersionOfAllHosts;
	}

	public void setMedianCPUDispersionOfAllHosts(double medianCPUDispersionOfAllHosts) {
		this.medianCPUDispersionOfAllHosts = medianCPUDispersionOfAllHosts;
	}

	public double getMedianCPUDispersionOfActiveHosts() {
		return medianCPUDispersionOfActiveHosts;
	}

	public void setMedianCPUDispersionOfActiveHosts(double medianCPUDispersionOfActiveHosts) {
		this.medianCPUDispersionOfActiveHosts = medianCPUDispersionOfActiveHosts;
	}
	
	
}
