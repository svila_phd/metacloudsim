package svila.planetlabNetwork.results;

import java.util.List;

import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.Snapshot;

public class TimeResults extends ExperimentResultsAbs implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	long startTime;
	long endTime;
	long timeElapsed;
	double lastClock;
	
	public TimeResults(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super(name, snapshots, datacenter);
	}
	
	public void calculate(int i) {
		
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
		this.timeElapsed = this.endTime - this.startTime;
	}

	public double getLastClock() {
		return lastClock;
	}

	public void setLastClock(double lastClock) {
		this.lastClock = lastClock;
	}

	public long getTimeElapsed() {
		return timeElapsed;
	}

	public void setTimeElapsed(long timeElapsed) {
		this.timeElapsed = timeElapsed;
	}
}
