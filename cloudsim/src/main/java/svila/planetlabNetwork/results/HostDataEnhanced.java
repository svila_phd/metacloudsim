package svila.planetlabNetwork.results;

import org.cloudbus.cloudsim.HostDynamicWorkload.NetworkUtilization;

public class HostDataEnhanced {
	int hostId;
	int hostNetId;
	double time;
	double requestedMIPS;
	double allocatedMIPS;
	double percentageUsed;
	double overusedMIPS;
	double percentageOverused;
	double MIPSCapacity;
	double energy;
	double wastedMIPS;
	double RAMCapacity;
	double usedRAM;
	double wastedRAM;
	double requestedinterBW;
	double requestedintraBW;
	double interBWCapacity;
	double allocatedinterBW;
	double wastedinterBW;
	int numVms;
	
	double offsetTime;
	
	public static final String CSVHeader = "id" + "," + "netId" + "," + "time" + "," + "offsetTime" + "," + "requestedMIPS" + "," + 
			"allocatedMIPS" + "," + "percentageUsage" + "," + "overusedMIPS" + "," + "percentageOverused" + "," +
			"MIPSCapacity" + "," + "energy" + "," + "wastedMIPS" + "," + "RAMCapacity" + "," + "usedRAM" +
			"," + "requestedinterBW" + "," + "requestedintraBW" + "," + "interBWCapacity" + "," +
			"allocatedinterBW" + "," + "wastedinterBW" + "," + "numVms";
	
	public HostDataEnhanced(int hostId, int hostNetId, Double time,
			Double requestedMIPS, Double allocatedMIPS, Double MIPSCapacity,
			double energy, double RAMCapacity, double usedRAM, double wastedRAM,
			NetworkUtilization networkUtilization, int numVms) {
		this.hostId = hostId;
		this.hostNetId = hostNetId;
		this.time = time;
		this.requestedMIPS = requestedMIPS;
		this.allocatedMIPS = allocatedMIPS;
		this.overusedMIPS = Math.max(0, requestedMIPS - allocatedMIPS);
		this.MIPSCapacity = MIPSCapacity;
		if(allocatedMIPS == 0.0) {
			this.percentageUsed = 0.0;
			this.percentageOverused = 0.0;
			this.wastedMIPS = 0.0;
		} else {
			this.percentageUsed = requestedMIPS / allocatedMIPS;
			this.percentageOverused = overusedMIPS / allocatedMIPS;
			this.wastedMIPS = this.MIPSCapacity - this.allocatedMIPS;
		}
		
		this.energy = energy;
		this.RAMCapacity = RAMCapacity;
		this.usedRAM = usedRAM;
		this.wastedRAM = wastedRAM;
		
		this.interBWCapacity = networkUtilization.externalBWcapacity;
		this.requestedinterBW = networkUtilization.externalBW;
		this.requestedintraBW = networkUtilization.internalBW;
		this.allocatedinterBW = Math.min(this.interBWCapacity, this.requestedinterBW);
		this.wastedinterBW = this.interBWCapacity - this.allocatedinterBW;
		
		this.numVms = numVms;
	}
	
	public void setOffsetTime(HostDataEnhanced previousHostDataEnhanced) {
		this.offsetTime = this.time - previousHostDataEnhanced.time;
	}
	
	public void setOffsetTime(double offsetTime) {
		this.offsetTime = offsetTime;
	}
	
	public double getHostNetId() {
		return this.hostNetId;
	}
	
	public String getCSVLine() {
		return hostId + "," + hostNetId + "," + time + "," + offsetTime + "," + requestedMIPS + "," + 
				allocatedMIPS + "," + percentageUsed + "," + overusedMIPS + "," +
				percentageOverused + "," + MIPSCapacity + "," + energy + "," + wastedMIPS + "," +
				RAMCapacity + "," + usedRAM + "," + requestedinterBW + "," + requestedintraBW + "," +
				interBWCapacity + "," + allocatedinterBW + "," + wastedinterBW + "," + numVms;
	}
}
