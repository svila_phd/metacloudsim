package svila.planetlabNetwork.results;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.HostStateHistoryEntry;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmStateHistoryEntry;
import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;

public class HistoricVMResultsEnhanced extends ExportableExperimentResults {
	List<VMDataEnhanced> vmDatas;

	public HistoricVMResultsEnhanced(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		super(name, snapshots, datacenter);
		this.vmDatas = new ArrayList<>();
	}
	
	@Override
	public void calculateAll() {
		ExperimentConfiguration ec = StaticResources.getCE();
		if(!ec.isOutputCSVTrace()) {
			return;
		}
		for(Vm vm : datacenter.getVmList()) {
			List<VmStateHistoryEntry> stateHistory = vm.getStateHistory();
			for(VmStateHistoryEntry currentState : stateHistory) {
				VMDataEnhanced vmData = new VMDataEnhanced(vm.getId(), vm.getNetworkId(), currentState.getTime(),
						currentState.getRequestedMips(), currentState.getAllocatedMips(),
						vm.getMips(), vm.getHost().getId(), currentState.getExternalBW());
				this.vmDatas.add(vmData);
			}
		}
	}
	
	public void calculate(int i) {

	}

	@Override
	public void exportAll() {
		ExperimentConfiguration ec = StaticResources.getCE();

		String outputFolderName = ec.getExperimentOutputFolder();
		Path outputPath = Paths.get(StaticResources.baseFolder,
				StaticResources.rootOutputFolderName,
				outputFolderName);
		File outputFolder = outputPath.toFile();
		outputFolder.mkdir();
		
		Path exportFile = Paths.get(outputPath.toString(), ec.getName()+"_vms_enhanced"+".csv");
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(exportFile.toString()));
			writer.write(VMDataEnhanced.CSVHeader + "\n");
			for(VMDataEnhanced vmData : this.vmDatas) {
				writer.write(vmData.getCSVLine() + "\n");
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void export(int i) {
		// TODO Auto-generated method stub
		
	}
}
