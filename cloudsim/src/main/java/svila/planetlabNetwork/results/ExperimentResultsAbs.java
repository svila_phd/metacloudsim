package svila.planetlabNetwork.results;

import java.util.List;

import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.Snapshot;

public abstract class ExperimentResultsAbs {
	protected List<Snapshot> snapshots;
	protected PowerDatacenter datacenter;
	public String name;
	
	public ExperimentResultsAbs() {
		
	}
	
	public ExperimentResultsAbs(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		this.name = name;
		this.snapshots = snapshots;
		this.datacenter = datacenter;
	}
	
	public void init(String name, List<Snapshot> snapshots, PowerDatacenter datacenter) {
		this.name = name;
		this.snapshots = snapshots;
		this.datacenter = datacenter;
	}

	public double checkNaN(Double value) {
		if(value.isNaN()) {
			return 0.0;
		}
		return value;
	}
	
	protected String getExperimentResultsName() {
		return this.name;
	}
	
	public void calculateAll() {
		for(int i=0; i<snapshots.size(); i++) {
			calculate(i);
		}
		postCalculate();
	}
	
	public abstract void calculate(int i);
	
	public void postCalculate() {
		
	}
}
