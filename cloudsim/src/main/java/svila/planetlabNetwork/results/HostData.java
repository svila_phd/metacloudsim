package svila.planetlabNetwork.results;

import org.cloudbus.cloudsim.HostDynamicWorkload.NetworkUtilization;

public class HostData {
	public int hostId;
	public int hostNetId;
	public int snapshot;
	public double requestedMIPS;
	public double allocatedMIPS;
	public double percentageUsed;
	public double overusedMIPS;
	public double percentageOverused;
	public double MIPSCapacity;
	public double energy;
	public double wastedMIPS;
	public double RAMCapacity;
	public double usedRAM;
	public double wastedRAM;
	public double requestedinterBW;
	public double requestedintraBW;
	public double interBWCapacity;
	public double allocatedinterBW;
	public double wastedinterBW;
	public int numVms;
	
	public static final String CSVHeader = "id" + "," + "netId" + "," + "snapshot" + "," + "requestedMIPS" + "," + 
			"allocatedMIPS" + "," + "percentageUsage" + "," + "overusedMIPS" + "," + "percentageOverused" + "," +
			"MIPSCapacity" + "," + "energy" + "wastedMIPS" + "," + "RAMCapacity" + "," + "usedRAM" +
			"," + "requestedinterBW" + "," + "requestedintraBW" + "," + "interBWCapacity" + "," +
			"allocatedinterBW" + "," + "wastedinterBW" + "," + "numVms";
	
	public HostData(int hostId, int hostNetId, int snapshot, Double requestedMIPS, Double allocatedMIPS, Double MIPSCapacity, Double energy,
			double RAMCapacity, double usedRAM, double wastedRAM, NetworkUtilization networkUtilization, int numVms) {
		this.hostId = hostId;
		this.hostNetId = hostNetId;
		this.snapshot = snapshot;
		this.requestedMIPS = requestedMIPS;
		this.allocatedMIPS = allocatedMIPS;
		this.overusedMIPS = Math.max(0, requestedMIPS - allocatedMIPS);
		this.MIPSCapacity = MIPSCapacity;
		if(allocatedMIPS == 0.0) {
			this.percentageUsed = 0.0;
			this.percentageOverused = 0.0;
		} else {
			this.percentageUsed = requestedMIPS / allocatedMIPS;
			this.percentageOverused = overusedMIPS / allocatedMIPS;
		}
		this.energy = energy;
		this.RAMCapacity = RAMCapacity;
		this.usedRAM = usedRAM;
		this.wastedRAM = wastedRAM;
		
		this.interBWCapacity = networkUtilization.externalBWcapacity;
		this.requestedinterBW = networkUtilization.externalBW;
		this.requestedintraBW = networkUtilization.internalBW;
		this.allocatedinterBW = Math.min(this.interBWCapacity, this.requestedinterBW);
		this.wastedinterBW = this.interBWCapacity - this.allocatedinterBW;
		
		this.numVms = numVms;
	}
	
	public String getCSVLine() {
		return hostId + "," + hostNetId + "," + snapshot + "," + requestedMIPS + "," + 
				allocatedMIPS + "," + percentageUsed + "," + overusedMIPS + "," +
				percentageOverused + "," + MIPSCapacity + "," + energy + wastedMIPS + "," +
				RAMCapacity + "," + usedRAM + "," + requestedinterBW + "," + requestedintraBW + "," +
				interBWCapacity + "," + allocatedinterBW + "," + wastedinterBW + "," + numVms;
	}
}
