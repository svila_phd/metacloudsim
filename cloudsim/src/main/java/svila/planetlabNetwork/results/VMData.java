package svila.planetlabNetwork.results;

public class VMData {
	int VMId;
	int snapshot;
	double requestedMIPS;
	double allocatedMIPS;
	double percentageUsed;
	double overusedMIPS;
	double percentageOverused;
	double MIPSCapacity;
	
	public static final String CSVHeader = "id" + "," + "snapshot" + "," + "requestedMIPS" + "," + 
			"allocatedMIPS" + "," + "percentageUsage" + "," + "overusedMIPS" + "," + "percentageOverused"
			+ "," + "MIPSCapacity";
	
	public VMData(int VMId, int snapshot, Double requestedMIPS, Double allocatedMIPS, Double MIPSCapacity) {
		this.VMId = VMId;
		this.snapshot = snapshot;
		this.requestedMIPS = requestedMIPS;
		this.allocatedMIPS = allocatedMIPS;
		this.overusedMIPS = Math.max(0, requestedMIPS - allocatedMIPS);
		this.MIPSCapacity = MIPSCapacity;
		if(allocatedMIPS == 0.0) {
			this.percentageUsed = 0.0f;
			this.percentageOverused = 0.0f;
		} else {
			this.percentageUsed = requestedMIPS / allocatedMIPS;
			this.percentageOverused = overusedMIPS / allocatedMIPS;
		}
		
	}
	
	public String getCSVLine() {
		return VMId + "," + snapshot + "," + requestedMIPS + "," + 
				allocatedMIPS + "," + percentageUsed + "," + overusedMIPS + "," + percentageOverused
				+ "," + MIPSCapacity;
	}
}
