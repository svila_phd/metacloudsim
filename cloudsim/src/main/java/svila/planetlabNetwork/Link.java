package svila.planetlabNetwork;

public class Link {
	private String name;
	private Node sourceNode;
	private Node endNode;
	private Float weight;
	private LinkType linkType;
	
	public enum LinkType {none, hostVm, betweenSwitchs, switchHost, defaultWeight, defaultWeightInterconnected};
	
	public Link(Node sourceNode, Node endNode, Float weight) {
		this.name = sourceNode.getName() + "-" + endNode.getName();
		this.sourceNode = sourceNode;
		this.endNode = endNode;
		this.weight = weight;
		this.linkType = LinkType.none;
	}
	
	public Link(Node sourceNode, Node endNode, Float weight, LinkType linkType) {
		this.name = sourceNode.getName() + "-" + endNode.getName();
		this.sourceNode = sourceNode;
		this.endNode = endNode;
		this.weight = weight;
		this.linkType = linkType;
	}

	public LinkType getLinkType() {
		return linkType;
	}

	public void setLinkType(LinkType linkType) {
		this.linkType = linkType;
	}

	public String getName() {
		return name;
	}
	
	public String getReverseLinkName() {
		return this.endNode + "-" + this.sourceNode;
	}

	public Node getSourceNode() {
		return sourceNode;
	}

	public Node getEndNode() {
		return endNode;
	}
	
	public void clearWeight() {
		this.weight = 0.0f;
	}
	
	public void setWeight(float weight) {
		this.weight = weight;
	}
	
	public Float getWeight() {
		return weight;
	}
	
	public void addWeight(Float plusWeight) {
		this.weight += plusWeight;
	}
	
	public void substractWeight(Float substractWeight) {
		this.weight -= substractWeight;
	}
	
	public String toString() {
		return this.name + " weight: " + this.weight;
	}
	
	public Link copy() {
		return new Link(sourceNode.getCleanNodeCopy(),
						endNode.getCleanNodeCopy(),
						weight);
	}
}
