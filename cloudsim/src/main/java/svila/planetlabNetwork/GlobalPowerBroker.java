package svila.planetlabNetwork;

import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.SimEntity;
import org.cloudbus.cloudsim.core.SimEvent;
import org.cloudbus.cloudsim.examples.power.Helper;

public class GlobalPowerBroker extends SimEntity {

	private static final int CREATE_BROKER = 0;
	private List<Vm> vmList;
	private List<Cloudlet> cloudletList;
	private DatacenterBroker broker;
	private double time;
	private int count;
	public List<DatacenterBroker> brokers;

	public GlobalPowerBroker(String name, double time, List<Vm> vmList, List<Cloudlet> cloudletList) {
		super(name);
		this.time = time;
		this.count = 0;
		this.brokers = new ArrayList<>();
		this.vmList = vmList;
		this.cloudletList = cloudletList;
	}

	@Override
	public void processEvent(SimEvent ev) {
		switch (ev.getTag()) {
		case CREATE_BROKER:
			DatacenterBroker d = Helper.createBroker();

			//Create VMs and Cloudlets and send them to broker
			//setVmList(createVM(d.getId(), 5, 100*count)); //creating 5 vms
			//setCloudletList(createCloudlet(d.getId(), 10, 100*(count+1))); // creating 10 cloudlets

			VMLoadBalancer.loadNext();
			
			d.submitVmList(VMLoadBalancer.getNextVms());
			d.submitCloudletList(VMLoadBalancer.getNextCloudlets());

			CloudSim.resumeSimulation();
			count++;
			if(count < 3) {
				schedule(getId(), time*count, CREATE_BROKER);
			}
			
			break;

		default:
			Log.printLine(getName() + ": unknown event type");
			break;
		}
	}

	@Override
	public void startEntity() {
		Log.printLine(super.getName()+" is starting...");
		schedule(getId(), time, CREATE_BROKER);
	}

	@Override
	public void shutdownEntity() {
	}

	public List<Vm> getVmList() {
		return vmList;
	}

	protected void setVmList(List<Vm> vmList) {
		this.vmList = vmList;
	}

	public List<Cloudlet> getCloudletList() {
		return cloudletList;
	}

	protected void setCloudletList(List<Cloudlet> cloudletList) {
		this.cloudletList = cloudletList;
	}

}