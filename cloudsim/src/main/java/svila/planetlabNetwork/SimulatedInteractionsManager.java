package svila.planetlabNetwork;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SimulatedInteractionsManager {
	List<SimulatedInteraction> currentInteractions;
	List<Integer> nodeIds;
	Graph logicalGraph;
	String path;
	File interactionsFile;
	BufferedReader reader;
	int currentSlot;
	
	public SimulatedInteractionsManager(String path) {
		this.path = path;
		this.interactionsFile = new File(path);
		this.currentSlot = -1;
		this.currentInteractions = new ArrayList<>();
		this.nodeIds = new ArrayList<>();
		try {
			reader = new BufferedReader(new FileReader(interactionsFile));
		} catch(FileNotFoundException e ) {
			e.printStackTrace();
		}
	}
	
	public void readNextSimulatedInteractions() {
		currentInteractions.clear();
		nodeIds.clear();
		this.currentSlot++;
		try {
			while(true) {
				String line = reader.readLine();
				if(line.startsWith("l")) {
					String[] elements = line.split(" ");
					currentInteractions.add(
						new SimulatedInteraction(
							this.currentSlot,
							Integer.parseInt(elements[1]),
							Integer.parseInt(elements[2]),
							Float.parseFloat(elements[3])
						)
					);
				} else if(line.startsWith("n")) {
					String[] elements = line.split(" ");
					for(int i=1; i<elements.length; i++) {
						nodeIds.add(Integer.parseInt(elements[i]));
					}
				} else if(line.startsWith("#")) {
					// Nothing, a commentary
				}  else if (line.equals("0")) {
					generateLogicalGraph();
					break;
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void generateLogicalGraph() {
		logicalGraph = new Graph();
		for(Integer id : nodeIds) {
			Node node = new Node(id.toString());
			node.setId(id);
			node.setType(Node.TYPE.VM);
			logicalGraph.addNode(node);
		}
		
		for(SimulatedInteraction si : currentInteractions) {
			logicalGraph.linkNodesButOneLink(logicalGraph.getNodeById(si.vmIdStart),
					logicalGraph.getNodeById(si.vmIdEnd),
					si.bandwidth);
			}
	}
	
	public Graph getCurrentLogicalGraph() {
		return logicalGraph;
	}
	
	public List<SimulatedInteraction> getCurrentInteractions() {
		return currentInteractions;
	}
	
	public int getCurrentSlotNumber() {
		return this.currentSlot;
	}
}
