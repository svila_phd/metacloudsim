package svila.planetlabNetwork;

import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public class CCGRIDAllocator {

	
	/**
	 * Finds a PM that has enough resources to host a given VM
         * and that will not be overloaded after placing the VM on it.
         * The selected host will be that one with most efficient
         * power usage for the given VM.
         * 
         * Sergi: Custom implementation ordering the hosts by distance
	 * 
	 * @param vm the VM
	 * @param excludedHosts the excluded hosts
	 * @return the host found to host the VM
	 */
	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
		//System.out.println("Searching host for VM " + vm.getId());
		double minPower = Double.MAX_VALUE;
		PowerHost allocatedHost = null;

		Integer lastHostNetId = vm.lastHostUntilDestroy.getNetworkId();
		
		TreeMap<Integer, List<PowerHost>> otherHosts = TopologyHostDistanceManager.getHostsGroupedByDistance(lastHostNetId);
		
		for(List<PowerHost> list : otherHosts.values()) {			
			for(PowerHost host : list) {
				// Original code
				if (excludedHosts.contains(host)) {
					continue;
				}
				if (host.isSuitableForVm(vm)) {
					if (getUtilizationOfCpuMips(host) != 0 && isHostOverUtilizedAfterAllocation(host, vm)) {
						continue;
					}

					try {
						double powerAfterAllocation = getPowerAfterAllocation(host, vm);
						if (powerAfterAllocation != -1) {
							double powerDiff = powerAfterAllocation - host.getPower();
							if (powerDiff < minPower) {
								minPower = powerDiff;
								allocatedHost = host;
							}
						}
					} catch (Exception e) {
					}
				}
				// End of original code
			}
			
			if(allocatedHost != null) { // If we allocated the host in one group, stop searching other groups
				break;
			}
		}
		
		if(allocatedHost == null) {
			return null;
		}
		
		System.out.println("ALLOCATED HOST: " + allocatedHost.getId());
		
		return allocatedHost;
	}
}
