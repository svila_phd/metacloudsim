package svila.planetlabNetwork;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Path {
	private Node node1;
	private Node node2;
	private List<Link> links;

	public Path(Node node1, Node node2, List<Link> links) {
		this.node1 = node1;
		this.node2 = node2;
		this.links = links;
	}

	public Node getNode1() {
		return node1;
	}

	public Node getNode2() {
		return node2;
	}

	public List<Link> getLinks() {
		return links;
	}
	
	public void clearLinkWeights() {
		for(Link link : links) {
			link.clearWeight();
		}
	}
	
	public Path copy() {
		List<Link> copyLinks = new ArrayList<>();
		for(Link link : links) {
			copyLinks.add(link.copy());
		}
		
		return new Path(node1.getCleanNodeCopy(),
						node2.getCleanNodeCopy(),
						copyLinks);
	}
	
	@Override
	public String toString() {
		String s = "";
		s += node1.toString() + "-" + node2.toString() + " [";
		for(Link link : links) {
			s += link.toString() + "; ";
		}
		s += "]";
		return s;
	}
}
