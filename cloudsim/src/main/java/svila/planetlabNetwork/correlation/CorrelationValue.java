package svila.planetlabNetwork.correlation;

import java.util.List;

import org.cloudbus.cloudsim.power.PowerVm;

public class CorrelationValue {
	public PowerVm vm;
	public Double correlationValue;
	public Double sumOwnValue;
	public Double sumOtherValue;
	public Double normalizedValue;
	
	public CorrelationValue(PowerVm vm, Double pearsonValue) {
		this.vm = vm;
		this.correlationValue = pearsonValue;
	}
	
	public void setSumValues(Double sumOwnValue, Double sumOtherValue) {
		this.sumOwnValue = sumOwnValue;
		this.sumOtherValue = sumOtherValue;
	}
	
	@Override
	public String toString() {
		return "Correlation value: idVM: " + vm.getId() + " nVM: " + vm.getNetworkId() + " correlation value: " + correlationValue;
	}
}
