package svila.planetlabNetwork.correlation;

import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;

public class SpearmanMathCorrelation extends Correlation {
	SpearmansCorrelation spearmansCorrelation;
	
	public SpearmanMathCorrelation() {
		spearmansCorrelation = new SpearmansCorrelation();
	}
	
	@Override
	public double correlation(double[] xArray, double[] yArray) {
		return spearmansCorrelation.correlation(xArray, yArray);
	}

}
