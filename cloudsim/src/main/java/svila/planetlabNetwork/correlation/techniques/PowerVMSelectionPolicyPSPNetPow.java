package svila.planetlabNetwork.correlation.techniques;

import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

import svila.planetlabNetwork.correlation.Correlation;

public class PowerVMSelectionPolicyPSPNetPow extends PowerVMSelectionPolicyPSP {

	public PowerVMSelectionPolicyPSPNetPow(Correlation correlation, PowerVmSelectionPolicy fallbackPolicy) {
		super(correlation, fallbackPolicy);
	}
	
	@Override
	public Double getHVal(double cpuPearson, double networkPearson) {
		double weight = 0.5;
		double powNet = Math.pow(networkPearson, 2);
		return (1 - (weight * cpuPearson)) / (1 + (weight * cpuPearson) - (weight * powNet));
	}

}
