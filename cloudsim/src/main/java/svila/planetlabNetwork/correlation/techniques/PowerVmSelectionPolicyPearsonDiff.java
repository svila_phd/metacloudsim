package svila.planetlabNetwork.correlation.techniques;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

import svila.planetlabNetwork.correlation.Correlation;
import svila.planetlabNetwork.correlation.CorrelationValue;

public class PowerVmSelectionPolicyPearsonDiff extends PowerVmSelectionPolicyPearsonCorrelation {

	public PowerVmSelectionPolicyPearsonDiff(Correlation correlation, PowerVmSelectionPolicy fallbackPolicy) {
		super(correlation, fallbackPolicy);
	}

	@Override
	public Vm getVmToMigrate(PowerHost host) {
		checkDatacenter();
		
		//System.out.println("PC: Received host " + host.getId());
		List<PowerVm> migratableVms = getMigratableVms(host);
		if (migratableVms.isEmpty()) {
			return null;
		}
		
		Map<Integer, CorrelationValue> cpuPearsonValues = getCpuValues(host, migratableVms);
		Map<Integer, CorrelationValue> networkPearsonValues = getNetworkValues(host, migratableVms);
		
		/*System.out.println("CPU pearson values");
		for(PearsonValue pv : cpuPearsonValues.values()) {
			System.out.println(pv);
		}
		
		System.out.println("Network pearson values");
		for(PearsonValue pv : networkPearsonValues.values()) {
			System.out.println(pv);
		}*/
		
		HashMap<PowerVm, Double> heuristicValues = new HashMap<>();
		
		for(CorrelationValue pv : cpuPearsonValues.values()) {
			heuristicValues.put(pv.vm, getHVal(pv.correlationValue, networkPearsonValues.get(pv.vm.getNetworkId()).correlationValue));
		}
		PowerVm selectedVm = obtainBestMigratableVM(heuristicValues);
		
		if(selectedVm == null) {
			//System.out.println("PC: no VM migration");
			return null;
		}
		
		//printPearsonDecision(cpuPearsonValues, networkPearsonValues,
		//		 heuristicValues, selectedVm);
		
		//System.out.println("PC: migrate VM " + selectedVm);
		return selectedVm;
	}
	
	public Double getHVal(double cpuPearson, double networkPearson) {
		return cpuPearson - networkPearson;
	}
}
