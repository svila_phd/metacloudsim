package svila.planetlabNetwork.correlation.techniques;

import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

import svila.planetlabNetwork.correlation.Correlation;

public class PowerVmSelectionPolicyPearsonDiffAbs extends PowerVmSelectionPolicyPearsonDiff {
	public PowerVmSelectionPolicyPearsonDiffAbs(Correlation correlation, final PowerVmSelectionPolicy fallbackPolicy) {
		super(correlation, fallbackPolicy);
	}
	
	@Override
	public Double getHVal(double cpuPearson, double networkPearson) {
		return Math.abs(cpuPearson - networkPearson);
	}
}
