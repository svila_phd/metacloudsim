package svila.planetlabNetwork.correlation.techniques;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.util.Pair;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

import svila.planetlabNetwork.correlation.Correlation;
import svila.planetlabNetwork.correlation.CorrelationValue;

public class PowerVmSelectionPolicyPearsonNear extends PowerVmSelectionPolicyPearsonCorrelation {
	Pair<Double, Double> optimalPoint;
	double maxDistance;
	
	public PowerVmSelectionPolicyPearsonNear(Correlation correlation, final PowerVmSelectionPolicy fallbackPolicy, Pair<Double, Double> optimalPoint, double maxDistance) {
		super(correlation, fallbackPolicy);
		this.optimalPoint = optimalPoint;
		this.maxDistance = maxDistance;
		this.thresholdHeuristic = this.maxDistance;
	}

	@Override
	public Vm getVmToMigrate(PowerHost host) {
		checkDatacenter();
		
		List<PowerVm> migratableVms = getMigratableVms(host);
		if (migratableVms.isEmpty()) {
			return null;
		}
		
		Map<Integer, CorrelationValue> cpuPearsonValues = getCpuValues(host, migratableVms);
		Map<Integer, CorrelationValue> networkPearsonValues = getNetworkValues(host, migratableVms);

		HashMap<PowerVm, Double> heuristicValues = new HashMap<>();
		
		for(CorrelationValue pv : cpuPearsonValues.values()) {
			heuristicValues.put(pv.vm, getHVal(pv.correlationValue, networkPearsonValues.get(pv.vm.getNetworkId()).correlationValue));
		}
		PowerVm selectedVm = obtainBestMigratableVM(heuristicValues);
		
		if(selectedVm == null) {
			//System.out.println("PC: no VM migration");
			return null;
		}

		return selectedVm;
	}
	
	public Double getHVal(double cpuPearson, double networkPearson) {
		double distance = Math.sqrt(Math.pow(networkPearson-optimalPoint.getFirst(), 2) + Math.pow(cpuPearson-optimalPoint.getSecond(), 2));
		if(distance > maxDistance) {
			return -1.0;
		}
		
		return distance;
	}
}
