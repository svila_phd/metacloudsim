package svila.planetlabNetwork.correlation.techniques;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

import svila.planetlabNetwork.PowerVmData;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;
import svila.planetlabNetwork.correlation.Correlation;
import svila.planetlabNetwork.correlation.CorrelationValue;

public class PowerVmSelectionPolicyPearsonCorrelation extends PowerVmSelectionPolicyCorrelation {

	double thresholdHeuristic = 0.45;
	// double thresholdHeuristic = 1.30;

	public PowerVmSelectionPolicyPearsonCorrelation(Correlation correlation,
			final PowerVmSelectionPolicy fallbackPolicy) {
		super(correlation, fallbackPolicy);
		setFallbackPolicy(fallbackPolicy);
	}

	public Double getHVal(double cpuPearson, double networkPearson) {
		double weight = 0.5;
		return (1 - (weight * cpuPearson)) / (1 + (weight * cpuPearson) - (weight * networkPearson));
	}

	protected PowerVm obtainBestMigratableVM(HashMap<PowerVm, Double> heuristicValues) {
		PowerVm selectedVm = null;

		double maxHeuristicValue = Double.MIN_VALUE;
		for (Entry<PowerVm, Double> entry : heuristicValues.entrySet()) {
			if (entry.getValue() <= thresholdHeuristic && entry.getValue() > maxHeuristicValue) {
				selectedVm = entry.getKey();
				maxHeuristicValue = entry.getValue();
			}
		}

		if (maxHeuristicValue > thresholdHeuristic) {
			return null;
		}

		return selectedVm;
	}

	private boolean arePearsonValuesValid(Map<Integer, CorrelationValue> pearsonValues) {
		for (CorrelationValue pv : pearsonValues.values()) {
			if (Double.isNaN(pv.correlationValue)) {
				return false;
			}
		}

		return true;
	}

	private Vm getAlternativeDecision(List<PowerVm> migratableVms, PowerHost host) {
		System.out.println("ALTERNATIVE");
		Vm biggestCPUVm = null;
		double biggestCPUValue = Double.MIN_VALUE;
		List<Snapshot> snapshots = getPowerDatacenter().snapshots;
		int numHistory = StaticResources.getCE().getMigrationInterval();
		double[] CPUSumHistory = new double[migratableVms.size()];

		int i = 0;
		for (Vm vm : migratableVms) {
			// List<Double> currentVmCpuValues = new ArrayList<>();
			for (int j = 1; j <= numHistory; j++) {
				Snapshot snapshot = snapshots.get(snapshots.size() - j);
				PowerVmData powerVmData = snapshot.getPowerHostDataMap().get(host.getId()).getVmsData().get(vm.getId());
				Double allocatedMips = powerVmData.getRequestedMips();
				// currentVmCpuValues.add(allocatedMips);
				CPUSumHistory[i] += allocatedMips;
			}
			i++;
		}

		for (i = 0; i < migratableVms.size(); i++) {
			if (CPUSumHistory[i] > biggestCPUValue) {
				biggestCPUValue = CPUSumHistory[i];
				biggestCPUVm = migratableVms.get(i);
			}
		}

		return biggestCPUVm;
	}

	@Override
	public Vm getVmToMigrate(final PowerHost host) {
		checkDatacenter();

		// System.out.println("PC: Received host " + host.getId());
		List<PowerVm> migratableVms = getMigratableVms(host);
		if (migratableVms.isEmpty()) {
			return null;
		}

		Map<Integer, CorrelationValue> cpuPearsonValues = getCpuValues(host, migratableVms);
		Map<Integer, CorrelationValue> networkPearsonValues = getNetworkValues(host, migratableVms);

		if (!arePearsonValuesValid(cpuPearsonValues) || !arePearsonValuesValid(networkPearsonValues)) {
			System.out.println("CPU valid: " + arePearsonValuesValid(cpuPearsonValues));
			Map<Integer, CorrelationValue> cpuPearsonValues2 = getCpuValues(host, migratableVms);
			Map<Integer, CorrelationValue> networkPearsonValues2 = getNetworkValues(host, migratableVms);
			return getAlternativeDecision(migratableVms, host);
		}

		/*
		 * System.out.println("CPU pearson values"); for(PearsonValue pv :
		 * cpuPearsonValues.values()) { System.out.println(pv); }
		 * 
		 * System.out.println("Network pearson values"); for(PearsonValue pv :
		 * networkPearsonValues.values()) { System.out.println(pv); }
		 */

		/*
		 * System.out.println("VM / NetPearson / CPUPearson"); for(CorrelationValue pv :
		 * networkPearsonValues.values()) { System.out.println(pv.vm.getId() + " " +
		 * pv.correlationValue + " " + cpuPearsonValues.get(pv.vm.getId())); }
		 */

		HashMap<PowerVm, Double> heuristicValues = new HashMap<>();

		for (CorrelationValue pv : cpuPearsonValues.values()) {
			heuristicValues.put(pv.vm,
					getHVal(pv.correlationValue, networkPearsonValues.get(pv.vm.getNetworkId()).correlationValue));
		}
		PowerVm selectedVm = obtainBestMigratableVM(heuristicValues);

		if (selectedVm == null) {
			// System.out.println("PC: no VM migration");
			return null;
		}

		// System.out.println("PC: migrate VM " + selectedVm);

		// printPearsonDecision(cpuPearsonValues, networkPearsonValues,
		// heuristicValues, selectedVm);
		// System.out.println("VM migration");
		return selectedVm;
	}
}
