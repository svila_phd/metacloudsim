package svila.planetlabNetwork.correlation.techniques;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

import svila.planetlabNetwork.PowerVmData;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;
import svila.planetlabNetwork.correlation.Correlation;
import svila.planetlabNetwork.correlation.CorrelationValue;

public class PowerVMSelectionPolicyPSP2 extends PowerVmSelectionPolicyCorrelation {

	public PowerVMSelectionPolicyPSP2(Correlation correlation,
			final PowerVmSelectionPolicy fallbackPolicy) {
		super(correlation, fallbackPolicy);
		setFallbackPolicy(fallbackPolicy);
	}

	public Double getHVal(double cpuPearson, double normalizedCpu, double networkPearson, double normalizedNetwork) {
		double weight = 0.5;
		return (1 - (weight * cpuPearson*normalizedCpu)) / (1 + (weight * cpuPearson*normalizedCpu) - (weight * networkPearson*normalizedNetwork));
	}

	protected PowerVm obtainBestMigratableVM(HashMap<PowerVm, Double> heuristicValues) {
		PowerVm selectedVm = null;

		double minHeuristicValue = Double.MAX_VALUE;
		for (Entry<PowerVm, Double> entry : heuristicValues.entrySet()) {
			if (entry.getValue() <= thresholdHeuristic && entry.getValue() < minHeuristicValue) {
				selectedVm = entry.getKey();
				minHeuristicValue = entry.getValue();
			}
		}

		return selectedVm;
	}

	private boolean arePearsonValuesValid(Map<Integer, CorrelationValue> pearsonValues) {
		for (CorrelationValue pv : pearsonValues.values()) {
			if (Double.isNaN(pv.correlationValue)) {
				return false;
			}
		}

		return true;
	}

	private Vm getAlternativeDecision(List<PowerVm> migratableVms, PowerHost host) {
		System.out.println("ALTERNATIVE");
		Vm biggestCPUVm = null;
		double biggestCPUValue = Double.MIN_VALUE;
		List<Snapshot> snapshots = getPowerDatacenter().snapshots;
		int numHistory = StaticResources.getCE().getMigrationInterval();
		double[] CPUSumHistory = new double[migratableVms.size()];

		int i = 0;
		for (Vm vm : migratableVms) {
			// List<Double> currentVmCpuValues = new ArrayList<>();
			for (int j = 1; j <= numHistory; j++) {
				Snapshot snapshot = snapshots.get(snapshots.size() - j);
				PowerVmData powerVmData = snapshot.getPowerHostDataMap().get(host.getId()).getVmsData().get(vm.getId());
				Double allocatedMips = powerVmData.getRequestedMips();
				// currentVmCpuValues.add(allocatedMips);
				CPUSumHistory[i] += allocatedMips;
			}
			i++;
		}

		for (i = 0; i < migratableVms.size(); i++) {
			if (CPUSumHistory[i] > biggestCPUValue) {
				biggestCPUValue = CPUSumHistory[i];
				biggestCPUVm = migratableVms.get(i);
			}
		}

		return biggestCPUVm;
	}
	
	public Double normalise(Double inValue, Double min, Double max) {
	    return (inValue - min)/(max - min);
	}
	
	public void putNormalizedValues(Map<Integer, CorrelationValue> valueMaps) {
		Double maxValue = 0.0, minValue = Double.MAX_VALUE;
		for(CorrelationValue cv : valueMaps.values()) {
			if(cv.sumOwnValue > maxValue) {
				maxValue = cv.sumOwnValue;
			}
			if(cv.sumOwnValue < minValue) {
				minValue = cv.sumOwnValue;
			}
		}
		
		for(CorrelationValue cv : valueMaps.values()) {
			cv.normalizedValue = normalise(cv.sumOwnValue, minValue, maxValue);
		}
	}

	@Override
	public Vm getVmToMigrate(final PowerHost host) {
		checkDatacenter();

		// System.out.println("PC: Received host " + host.getId());
		List<PowerVm> migratableVms = getMigratableVms(host);
		if (migratableVms.isEmpty()) {
			return null;
		}

		Map<Integer, CorrelationValue> cpuPearsonValues = getCpuValues(host, migratableVms);
		Map<Integer, CorrelationValue> networkPearsonValues = getNetworkValues(host, migratableVms);

		putNormalizedValues(cpuPearsonValues);
		putNormalizedValues(networkPearsonValues);
		
		if (!arePearsonValuesValid(cpuPearsonValues) || !arePearsonValuesValid(networkPearsonValues)) {
			System.out.println("CPU valid: " + arePearsonValuesValid(cpuPearsonValues));
			return getAlternativeDecision(migratableVms, host);
		}

		/*
		 * System.out.println("CPU pearson values"); for(PearsonValue pv :
		 * cpuPearsonValues.values()) { System.out.println(pv); }
		 * 
		 * System.out.println("Network pearson values"); for(PearsonValue pv :
		 * networkPearsonValues.values()) { System.out.println(pv); }
		 */

		/*
		 * System.out.println("VM / NetPearson / CPUPearson"); for(CorrelationValue pv :
		 * networkPearsonValues.values()) { System.out.println(pv.vm.getId() + " " +
		 * pv.correlationValue + " " + cpuPearsonValues.get(pv.vm.getId())); }
		 */

		HashMap<PowerVm, Double> heuristicValues = new HashMap<>();

		for (CorrelationValue pv : cpuPearsonValues.values()) {
			CorrelationValue networkValue = networkPearsonValues.get(pv.vm.getNetworkId());
			Double hval = getHVal(pv.correlationValue, pv.normalizedValue,
								  networkValue.correlationValue, networkValue.normalizedValue);
			heuristicValues.put(pv.vm, hval);
		}
		PowerVm selectedVm = obtainBestMigratableVM(heuristicValues);

		addCorrelationsData(cpuPearsonValues,networkPearsonValues,heuristicValues, selectedVm);
		
		if (selectedVm == null) {
			// System.out.println("PC: no VM migration");
			return null;
		}

		// System.out.println("PC: migrate VM " + selectedVm);

		// printPearsonDecision(cpuPearsonValues, networkPearsonValues,
		// heuristicValues, selectedVm);
		// System.out.println("VM migration");
		return selectedVm;
	}
}