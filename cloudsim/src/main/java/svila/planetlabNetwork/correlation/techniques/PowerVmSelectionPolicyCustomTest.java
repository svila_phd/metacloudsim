package svila.planetlabNetwork.correlation.techniques;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

public class PowerVmSelectionPolicyCustomTest extends PowerVmSelectionPolicy {

	boolean oneTime;
	
	public PowerVmSelectionPolicyCustomTest() {
		super();
		oneTime = true;
	}
	
	@Override
	public Vm getVmToMigrate(PowerHost host) {
		System.out.println("getVmToMigrate");
		if(!oneTime) {
			return null;
		}
		
		for(Vm vm : host.getVmList()) {
			if(vm.getId() == 2) {
				oneTime = false;
				return vm;
			}
		}
		
		return null;
	}


}
