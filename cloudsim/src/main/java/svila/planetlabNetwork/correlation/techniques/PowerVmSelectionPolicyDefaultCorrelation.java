package svila.planetlabNetwork.correlation.techniques;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

import svila.planetlabNetwork.correlation.Correlation;
import svila.planetlabNetwork.correlation.CorrelationValue;

public class PowerVmSelectionPolicyDefaultCorrelation extends PowerVmSelectionPolicyCorrelation {

	public PowerVmSelectionPolicyDefaultCorrelation(Correlation correlation, PowerVmSelectionPolicy fallbackPolicy) {
		super(correlation, fallbackPolicy);
	}
	
	public Double getHVal(double cpuPearson, double networkPearson) {
		double weight = 0.5;
		return (1 - (weight * cpuPearson)) / (1 + (weight * cpuPearson) - (weight * networkPearson));
	}
	
	protected PowerVm obtainBestMigratableVM(HashMap<PowerVm, Double> heuristicValues) {
		PowerVm selectedVm = null;

		double maxHeuristicValue = Double.MIN_VALUE;
		for (Entry<PowerVm, Double> entry : heuristicValues.entrySet()) {
			if (entry.getValue() <= thresholdHeuristic && entry.getValue() > maxHeuristicValue) {
				selectedVm = entry.getKey();
				maxHeuristicValue = entry.getValue();
			}
		}

		if (maxHeuristicValue > thresholdHeuristic) {
			return null;
		}

		return selectedVm;
	}

	@Override
	public Vm getVmToMigrate(final PowerHost host) {
		checkDatacenter();

		List<PowerVm> migratableVms = getMigratableVms(host);
		if (migratableVms.isEmpty()) {
			return null;
		}

		Map<Integer, CorrelationValue> cpuPearsonValues = getCpuValues(host, migratableVms);
		Map<Integer, CorrelationValue> networkPearsonValues = getNetworkValues(host, migratableVms);


		HashMap<PowerVm, Double> heuristicValues = new HashMap<>();

		for (CorrelationValue pv : cpuPearsonValues.values()) {
			heuristicValues.put(pv.vm,
					getHVal(pv.correlationValue, networkPearsonValues.get(pv.vm.getNetworkId()).correlationValue));
		}
		PowerVm selectedVm = obtainBestMigratableVM(heuristicValues);

		return selectedVm;
	}
}
