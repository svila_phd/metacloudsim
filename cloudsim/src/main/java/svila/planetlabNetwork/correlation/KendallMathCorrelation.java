package svila.planetlabNetwork.correlation;

import org.apache.commons.math3.stat.correlation.KendallsCorrelation;

public class KendallMathCorrelation extends Correlation {
	KendallsCorrelation kendallCorrelation;
	
	public KendallMathCorrelation() {
		kendallCorrelation = new KendallsCorrelation();
	}
	
	@Override
	public double correlation(double[] xArray, double[] yArray) {
		return kendallCorrelation.correlation(xArray, yArray);
	}
}
