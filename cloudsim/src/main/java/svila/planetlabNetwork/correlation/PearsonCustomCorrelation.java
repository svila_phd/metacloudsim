package svila.planetlabNetwork.correlation;

public class PearsonCustomCorrelation extends Correlation {
	
	public PearsonCustomCorrelation() {
	
	}
	
	@Override
	public double correlation(double[] xArray, double[] yArray) {
		double[] vmData = xArray;
		double vmMedian = getMedian(vmData);
		double hostMedian = getMedian(yArray);
		double[] vmMedianDiff = getMedianDiff(vmData, vmMedian);
		double[] hostMedianDiff = getMedianDiff(yArray, hostMedian);
		double[] preSum = preSumNumerator(vmMedianDiff, hostMedianDiff);
		double finalNumeratorSum = sumArray(preSum);
		double[] vmDenominator = getPowerArray(vmMedianDiff);
		double[] hostDenominator = getPowerArray(hostMedianDiff);
		double sumVmDenominator = sumArray(vmDenominator);
		double sumHostDenominator = sumArray(hostDenominator);
		double finalDenominator = Math.sqrt(sumVmDenominator * sumHostDenominator);
		double result = finalNumeratorSum / finalDenominator;
			
		return result;
	}

	private double getMedian(double[] data) {
		double sum = 0.0;
		
		for(int i=0; i<data.length; i++) {
			sum += data[i];
		}
		
		return sum / data.length;
	}
	
	private double[] getMedianDiff(double[] data, double median) {
		double[] new_data = new double[data.length];
		
		for(int i=0; i<data.length; i++) {
			new_data[i] = data[i] - median;
		}
		
		return new_data;
	}
	
	private double[] preSumNumerator(double[] dataX, double[] dataY) {
		double[] result = new double[dataX.length];
		
		for(int i=0; i<result.length; i++) {
			result[i] = dataX[i] * dataY[i];
		}
		
		return result;
	}
	
	private double sumArray(double[] data) {
		double sum = 0.0;
		
		for(int i=0; i<data.length; i++) {
			sum += data[i];
		}
		
		return sum;
	}
	
	private double[] getPowerArray(double[] data) {
		double[] result = new double[data.length];
		
		for(int i=0; i<result.length; i++) {
			result[i] = data[i] * data[i];
		}
		
		return result;
	}
}
