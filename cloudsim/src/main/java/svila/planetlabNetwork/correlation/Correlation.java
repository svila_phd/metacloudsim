package svila.planetlabNetwork.correlation;

import java.util.List;

public abstract class Correlation {
	public abstract double correlation(final double[] xArray, final double[] yArray);
	public double correlation(List<Double> xList, List<Double> yList) {
		double[] xArray = getPrimitiveDoubleArray(xList);
		double[] yArray = getPrimitiveDoubleArray(yList);
		return correlation(xArray, yArray);
	}

	public double[] getPrimitiveDoubleArray(List<Double> doubleList) {
		double[] doubleArray = new double[doubleList.size()];
		for(int i=0; i<doubleList.size(); i++) {
			doubleArray[i] = doubleList.get(i);
		}
		return doubleArray;
	}
}
