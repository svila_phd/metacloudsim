package svila.planetlabNetwork.correlation;

import java.util.HashMap;
import java.util.Map;

import org.cloudbus.cloudsim.power.PowerVm;

public class NetworkBandwidthValues {
	private Float internalCommunicationsBandwidth;
	Map<PowerVm, Float> relationMap;
	
	public NetworkBandwidthValues() {
		this.internalCommunicationsBandwidth = 0.0f;
		this.relationMap = new HashMap<>();
	}
	
	public void addRelation(PowerVm vm1, PowerVm vm2, Float value) {
		addElementsToMap(vm1, vm2, value);
		internalCommunicationsBandwidth += value;
	}
	
	private void addElementsToMap(PowerVm vm1, PowerVm vm2, Float value) {
		if(!this.relationMap.containsKey(vm1)) {
			this.relationMap.put(vm1, 0f);
		}
		
		this.relationMap.put(vm1, this.relationMap.get(vm1) + value);
	}
	
	public Float getInternalCommunicationsBandwith() {
		return this.internalCommunicationsBandwidth;
	}
	
	public Map<PowerVm, Float> getRelationMap() {
		return this.relationMap;
	}
	
	@Override
	public String toString() {
		String s = "";
		s += "Internal communications: " + internalCommunicationsBandwidth + "\n";
		return s;
	}
}
