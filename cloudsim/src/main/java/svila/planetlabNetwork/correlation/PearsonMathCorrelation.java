package svila.planetlabNetwork.correlation;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

public class PearsonMathCorrelation extends Correlation {
	PearsonsCorrelation pearsonCorrelation;
	
	public PearsonMathCorrelation() {
		pearsonCorrelation = new PearsonsCorrelation();
	}
	
	@Override
	public double correlation(double[] xArray, double[] yArray) {
		return pearsonCorrelation.correlation(xArray, yArray);
	}
}
