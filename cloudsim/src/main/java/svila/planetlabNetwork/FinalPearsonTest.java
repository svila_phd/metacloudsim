package svila.planetlabNetwork;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.stat.correlation.KendallsCorrelation;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;

import svila.planetlabNetwork.correlation.KendallMathCorrelation;
import svila.planetlabNetwork.correlation.PearsonMathCorrelation;
import svila.planetlabNetwork.correlation.SpearmanMathCorrelation;
import svila.planetlabNetwork.correlation.techniques.PowerVmSelectionPolicyPearsonCorrelation;

public class FinalPearsonTest {
	public static void main(String[] args) {		
		PowerVmSelectionPolicyPearsonCorrelation pc = new PowerVmSelectionPolicyPearsonCorrelation(new PearsonMathCorrelation(), null);

		ArrayList<Double> vmCPUData = new ArrayList<>();
		vmCPUData.add(10.0);
		vmCPUData.add(6.0);
		vmCPUData.add(1.0);
		vmCPUData.add(2.0);
		vmCPUData.add(3.0);
		vmCPUData.add(9.0);
		vmCPUData.add(5.0);
		vmCPUData.add(10.0);
		vmCPUData.add(1.0);
		vmCPUData.add(4.0);

		ArrayList<Double> hostCPUData = new ArrayList<>();
		hostCPUData.add(14.0);
		hostCPUData.add(16.0);
		hostCPUData.add(3.0);
		hostCPUData.add(3.0);
		hostCPUData.add(8.0);
		hostCPUData.add(19.0);
		hostCPUData.add(5.0);
		hostCPUData.add(26.0);
		hostCPUData.add(3.0);
		hostCPUData.add(7.0);
		
		ArrayList<Double> vmNetData = new ArrayList<>();
		vmNetData.add(2.0);
		vmNetData.add(9.0);
		vmNetData.add(3.0);
		vmNetData.add(7.0);
		vmNetData.add(4.0);
		vmNetData.add(3.0);
		vmNetData.add(2.0);
		vmNetData.add(7.0);
		vmNetData.add(5.0);
		vmNetData.add(10.0);

		ArrayList<Double> hostNetData = new ArrayList<>();
		hostNetData.add(38.0);
		hostNetData.add(37.0);
		hostNetData.add(32.0);
		hostNetData.add(33.0);
		hostNetData.add(23.0);
		hostNetData.add(28.0);
		hostNetData.add(31.0);
		hostNetData.add(32.0);
		hostNetData.add(38.0);
		hostNetData.add(22.0);

		Double correlationCoefficientCPU = pc.getCorrelationCoefficient(vmCPUData, hostCPUData);
		Double correlationCoefficientNet = pc.getCorrelationCoefficient(vmNetData, hostNetData);

		double hval = pc.getHVal(correlationCoefficientCPU, correlationCoefficientNet);
		System.out.println("Pearson CPU: " + correlationCoefficientCPU);
		System.out.println("Pearson Net: " + correlationCoefficientNet);
		System.out.println("hval: " + hval);
		
		PearsonMathCorrelation pmc = new PearsonMathCorrelation();
		KendallMathCorrelation kmc = new KendallMathCorrelation();
		SpearmanMathCorrelation smc = new SpearmanMathCorrelation();
		
		double[] data1 = pmc.getPrimitiveDoubleArray(vmCPUData);
		double[] data2 = pmc.getPrimitiveDoubleArray(hostCPUData);
		
		System.out.println("Java pearson: " + pmc.correlation(data1, data2));
		System.out.println("Java kendall: " + kmc.correlation(data1, data2));
		System.out.println("Java spearman: " + smc.correlation(data1, data2));
	}
}
