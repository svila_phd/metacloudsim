package svila.planetlabNetwork;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.HostStateHistoryEntry;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVm;
import org.json.JSONArray;
import org.json.JSONObject;

public class PowerHostData {

	private PowerHost host;
	private double totalAvailableMips;
	private double availableMips;
	private double usedMips;
	private double allocatedMips;
	private double requestedMips;
	private Map<Integer, PowerVmData> vmsData;
	
	public PowerHostData(PowerHost host) {
		this.host = host;
		this.totalAvailableMips = host.getTotalMips();
		this.availableMips = host.getAvailableMips();
		this.usedMips = this.totalAvailableMips - this.availableMips;
		List<HostStateHistoryEntry> historyEntryList = host.getStateHistory();
		HostStateHistoryEntry lastHistoryEntry = historyEntryList.get(historyEntryList.size()-1);
		this.allocatedMips = lastHistoryEntry.getAllocatedMips();
		this.requestedMips = lastHistoryEntry.getRequestedMips();
		this.vmsData = new HashMap<>();
		for(Vm vm : host.getVmList()) {
			vmsData.put(vm.getId(), new PowerVmData((PowerVm)vm));
		}
	}
	
	public double getUtilization() {
		return this.usedMips / this.totalAvailableMips;
	}

	public PowerHost getHost() {
		return host;
	}

	public double getTotalAvailableMips() {
		return totalAvailableMips;
	}

	public Map<Integer, PowerVmData> getVmsData() {
		return vmsData;
	}
	
	public double getAvailableMips() {
		return availableMips;
	}
	
	public double getUsedMips() {
		return usedMips;
	}
	
	public double getUsedUtilizationByPowerVmData(PowerVmData pvd) {
		if(usedMips == 0) {
			return 0.0;
		}
		return pvd.getRequestedMips() / usedMips * 100.0;
	}
	
	public double getTotalUtilizationByPowerVmData(PowerVmData pvd) {
		return pvd.getRequestedMips() / totalAvailableMips * 100.0;
	}
	
	public double getAllocatedMips() {
		return allocatedMips;
	}

	public void setAllocatedMips(double allocatedMips) {
		this.allocatedMips = allocatedMips;
	}

	public double getRequestedMips() {
		return requestedMips;
	}

	public void setRequestedMips(double requestedMips) {
		this.requestedMips = requestedMips;
	}

	public String toString() {
		String s = "";
		DecimalFormat df = new DecimalFormat("#0.00"); 
		s += "Host " + host.getId() +
			 " Total available mips: " + df.format(totalAvailableMips) +
			 " available mips: " + df.format(availableMips) +
			 " used mips: " + df.format(usedMips) +
			 " % Utilization: " + df.format(getUtilization()) +
			 " Vms:\n";
		for(PowerVmData pvd : vmsData.values()) {
			s += pvd.toString() + "\n";
			s += df.format(getUsedUtilizationByPowerVmData(pvd)) + "% used utilization\n";
			s += df.format(getTotalUtilizationByPowerVmData(pvd)) + "% total utilization\n";
		}
		
		return s;
	}
	public String resume() {
		String s = "";
		DecimalFormat df = new DecimalFormat("#0.00"); 
		s += "Host " + host.getId() + " " +
					df.format(getUtilization() * 100.0) +"% " +
					"(" + df.format(totalAvailableMips-availableMips) + "/" + df.format(totalAvailableMips) + "):\n";

			for(PowerVmData pvd : vmsData.values()) {
				s += "\t VM " + pvd.getVm().getId() + " " + df.format(pvd.getRequestedMips() / totalAvailableMips * 100.0) + "% " +
						" (" + df.format(pvd.getRequestedMips()) + "/" + totalAvailableMips + ")\n";
			}
		return s;
	}
	
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		json.put("hostId", host.getId());
		json.put("hostIdNet", host.getNetworkId());
		json.put("totalAvailableMips", totalAvailableMips);
		json.put("availableMips", availableMips);
		json.put("usedMips", usedMips);
		json.put("percUtilization", getUtilization());
		
		JSONArray vmArray = new JSONArray();
		for(PowerVmData pvmd : vmsData.values()) {
			JSONObject vmJson = new JSONObject();
			vmJson.put("id", pvmd.getVm().getId());
			vmJson.put("networkId", pvmd.getVm().getNetworkId());
			vmJson.put("allocatedMips", pvmd.getRequestedMips());
			vmJson.put("usedUtilization", getUsedUtilizationByPowerVmData(pvmd));
			vmJson.put("totalUtilization", getTotalUtilizationByPowerVmData(pvmd));
			vmArray.put(vmJson);
		}
		json.put("powerVMData", vmArray);
		
		return json;
	}
}
