package svila.planetlabNetwork;

public class MigrationDelayer {
	
	private int start, interval, currentPosition;
	
	public MigrationDelayer(int start, int interval) {
		this.start = start;
		this.interval = interval;
		this.currentPosition = 0;
	}
	
	public void update() {
		currentPosition++;
	}
	
	public boolean canMigrate() {
		if(StaticResources.getCE().getMigrationInterval() < 1) {
			return false;
		}
		boolean migrate = currentPosition >= start && currentPosition % interval == 0;
		//System.out.println("Migrate: " + migrate);
		return migrate;
	}
}
