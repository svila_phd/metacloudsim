package svila.planetlabNetwork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.cloudbus.cloudsim.power.PowerHost;

import svila.planetlabNetwork.Node.TYPE;

public class TopologyHostDistanceManager {
	
	private static Graph physicalGraph;
	private static Map<Integer, TreeMap<Integer, List<Integer>>> nodeDistances;
	private static Map<Integer, TreeMap<Integer, List<PowerHost>>> hostDistances;
	private static List<PowerHost> hosts;
	private static Map<String, Integer> pairHostsDistancesMap;
	
	public TopologyHostDistanceManager() {
		
	}
	
	public static void loadPhysicalGraph(Graph graph) {
		physicalGraph = graph.copy();
		prepareGraph();
	}
	
	private static void prepareGraph() {
		physicalGraph.clearLinkWeights();
		Set<Link> links = physicalGraph.getLinks();
		Iterator<Link> it = links.iterator();
		while(it.hasNext()) {
			it.next().setWeight(1.0f);
		}
		for(Node node : physicalGraph.getNodes()) {
			for(Entry<Node, Float>  entry : node.getAdjacentNodes().entrySet()) {
				entry.setValue(1.0f);
			}
		}
	}
	
	public static void calculateDistances() {
		nodeDistances = new HashMap<>();
		for(Node node : physicalGraph.getNodes()) {
			if(node.getType() != TYPE.LEAF) {
				continue;
			}
			ShortestPathCalculator.resetDistances(physicalGraph);
			Graph g = ShortestPathCalculator.calculateShortestPathFromSource(physicalGraph, node);
			for(Node otherNode : g.getNodes()) {
				if(otherNode.getType() != TYPE.LEAF) {
					continue;
				}
				if (node != otherNode) {
					if(!nodeDistances.containsKey(otherNode.getId())) {
						nodeDistances.put(otherNode.getId(), new TreeMap<>());
					}
					TreeMap<Integer, List<Integer>> map = nodeDistances.get(otherNode.getId());
					Integer distance = Math.round(otherNode.getDistance());
					if(!map.containsKey(distance)) {
						map.put(distance, new ArrayList<>());
					}
					map.get(distance).add(node.getId());
				}
			}
		}
	}
	
	public static void loadHosts(List<PowerHost> hosts) {
		TopologyHostDistanceManager.hosts = hosts;
	}
	
	public static Map<Integer, TreeMap<Integer, List<Integer>>> getAllGroupedDistances() {
		return nodeDistances;
	}
	
	public static TreeMap<Integer, List<Integer>> getLeafNodeIdsGroupedByDistance(Integer nodeId) {
		return nodeDistances.get(nodeId);
	}
	
	public static void calculateHostDistances() {
		HashMap<Integer, PowerHost> hostIdMap = new HashMap<>();
		for(PowerHost host : hosts) {
			hostIdMap.put(host.getNetworkId(), host);
		}
		
		hostDistances = new HashMap<>();
		for(Integer hostId : nodeDistances.keySet()) {
			TreeMap<Integer, List<PowerHost>> currentHostDistances = new TreeMap<>();
			hostDistances.put(hostId, currentHostDistances);
			TreeMap<Integer, List<Integer>> currentTreeMap = nodeDistances.get(hostId);
			for(Entry<Integer, List<Integer>> entry : currentTreeMap.entrySet()) {
				List<PowerHost> currentHostList = new ArrayList<>();
				currentHostDistances.put(entry.getKey(), currentHostList);
				for(Integer hostNetworkId : entry.getValue()) {
					currentHostList.add(hostIdMap.get(hostNetworkId));
				}
			}
		}
		
		pairHostsDistancesMap = new HashMap<>();
		for(Entry<Integer, TreeMap<Integer, List<PowerHost>>> currentEntry : hostDistances.entrySet()) {
			int firstHostId = currentEntry.getKey();
			TreeMap<Integer, List<PowerHost>> treeMap = currentEntry.getValue();
			for(Entry<Integer, List<PowerHost>> otherEntry : treeMap.entrySet()) {
				int distance = otherEntry.getKey();
				List<PowerHost> otherHosts = otherEntry.getValue();
				for(PowerHost powerHost : otherHosts) {
					int otherHostId = powerHost.getNetworkId();
					pairHostsDistancesMap.put(firstHostId+"-"+otherHostId, distance);
				}
			}
		}
	}
	
	public static int getShortestDistanceBetweenTwoHostNetworkIds(int hostNetworkId1, int hostNetworkId2) {
		return hostNetworkId1 == hostNetworkId2 ? 0 : pairHostsDistancesMap.get(hostNetworkId1+"-"+hostNetworkId2);
	}
	
	public static TreeMap<Integer, List<PowerHost>> getHostsGroupedByDistance(Integer nodeId) {
		return hostDistances.get(nodeId);
	}
	
	public static void show() {
		System.out.println();
	}
}
