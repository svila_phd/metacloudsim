package svila.planetlabNetwork;

public interface NetworkElement {
	public void setNetworkId(int networkId);
	public int getNetworkId();
}
