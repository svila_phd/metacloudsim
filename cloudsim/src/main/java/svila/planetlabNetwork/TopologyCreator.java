package svila.planetlabNetwork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONObject;

public class TopologyCreator {
	
	private int numOfInterconnectedLevels;
	private int numOfTreeLevels;
	private List<Integer> numElementsPerInterconnectedLevelList;
	private List<Integer> numElementsPerTreeLevelList;
	private float degreeOfInterconnection;
	private int nodeCount;
	private long randomSeed;
	private Graph graph;
	private Map<Integer, Node> dijkstraNodeMap;
	protected Map<Integer, Node> leafNodeMap;
	private float defaultWeightInterconnected;
	private float defaultWeightTree;
	private List<Pair<Integer, Integer>> cutLinks;
	
	protected TopologyCreator() {
		this.graph = new Graph();
	}
	
	public TopologyCreator(	List<Integer> numElementsPerInterconnectedLevelList,
							List<Integer> numElementsPerTreeLevelList,
							float degreeOfInterconnection,
							long randomSeed,
							float defaultWeightInterconnected,
							float defaultWeightTree,
							List<Pair<Integer, Integer>> cutLinks
							) {
		this.numOfInterconnectedLevels = numElementsPerInterconnectedLevelList.size();
		this.numOfTreeLevels = numElementsPerTreeLevelList.size();
		this.numElementsPerInterconnectedLevelList = numElementsPerInterconnectedLevelList;
		this.numElementsPerTreeLevelList = numElementsPerTreeLevelList;
		this.degreeOfInterconnection = degreeOfInterconnection;
		this.nodeCount = 0;
		this.graph = new Graph();
		this.defaultWeightInterconnected = defaultWeightInterconnected;
		this.defaultWeightTree = defaultWeightTree;
		this.dijkstraNodeMap = new HashMap<>();
		this.leafNodeMap = new HashMap<>();
		this.randomSeed = randomSeed < 0 ? System.currentTimeMillis() : randomSeed;
		this.cutLinks = cutLinks;
	}

	public Graph getGraph() {
		return graph;
	}
	
	public Map<Integer, Node> getCentralNodes() {
		return dijkstraNodeMap;
	}
	
	public Map<Integer, Node> getLeafNodes() {
		return leafNodeMap;
	}
	
	public void cutForbiddenLinks() {
		if(cutLinks == null) {
			return;
		}
		
		for(Pair<Integer, Integer> pair : cutLinks) {
			graph.removeBothLink(pair.getLeft(), pair.getRight());
		}
	}
	
	public void createTopology() {
		List<Node> initialNodes = createGroupOfInterconnectedNodes(numElementsPerInterconnectedLevelList.get(0));
		int level = 1;
		for(Node n : initialNodes) {
			createExpansionNodes(n, level);
		}
		cutForbiddenLinks();
	}
	
	private void createExpansionNodes(Node n, int level) {
		if(level < this.numOfInterconnectedLevels) {
			n.setType(Node.TYPE.CENTRAL);
			dijkstraNodeMap.put(n.getId(), n);
			List<Node> newNodes = createGroupOfInterconnectedNodes(numElementsPerInterconnectedLevelList.get(level));
			linkNodeWithOneNodeOfGroupOfNodes(n, newNodes);
			level++;
			for(Node newNode : newNodes) {
				createExpansionNodes(newNode, level);
			}
		} else if(level < this.numOfInterconnectedLevels+this.numOfTreeLevels) {
			n.setType(Node.TYPE.TREE);
			List<Node> newNodes = createGroupOfTreeNodes(numElementsPerTreeLevelList.get(level-this.numOfInterconnectedLevels));
			linkNodeWithGroupOfNodes(n, newNodes);
			level++;
			for(Node newNode : newNodes) {
				createExpansionNodes(newNode, level);
			}
		} else {
			n.setType(Node.TYPE.LEAF);
			level++;
			leafNodeMap.put(n.getId(), n);
		}
	}
	
	protected List<Node> createNodes(int num) {
		List<Node> nodes = new ArrayList<>();
		for(int i=0; i<num; i++) {
			Node n = new Node(String.valueOf(nodeCount));
			n.setId(nodeCount);
			nodeCount++;
			nodes.add(n);
			graph.addNode(n);
		}
		
		return nodes;
	}
	
	private void linkNodeWithGroupOfNodes(Node node, List<Node> nodes) {
		for(Node n : nodes) {
			graph.linkNodes(node, n, defaultWeightTree, Link.LinkType.defaultWeight);
		}
	}
	
	private void linkNodeWithOneNodeOfGroupOfNodes(Node node, List<Node> nodes) {
		Random r = new Random(this.randomSeed);
		int randomIndex = r.nextInt(nodes.size());
		Node randomNode = nodes.get(randomIndex);
		graph.linkNodes(node, randomNode, defaultWeightInterconnected, Link.LinkType.defaultWeightInterconnected);
	}
	
	private Set<Pair<Integer, Integer>> createSetOfPossibleLinks(List<Node> interconnectedNodes) {
		Set<Pair<Integer, Integer>> linkPairs = new HashSet<Pair<Integer, Integer>>();

		for(int i=0; i<interconnectedNodes.size(); i++) {
			Node n1 = interconnectedNodes.get(i);
			for(int j=i+1; j<interconnectedNodes.size(); j++) {
				Node n2 = interconnectedNodes.get(j);
				linkPairs.add(new ImmutablePair<Integer, Integer>(n1.getId(), n2.getId()));
			}
		}
		
		return linkPairs;
	}
	
	private List<Node> createGroupOfInterconnectedNodes(int numOfNodes) {
		List<Node> interconnectedNodes = createNodes(numOfNodes);
		
		Random r = new Random(this.randomSeed);
		
		Set<Pair<Integer, Integer>> linkPairs = createSetOfPossibleLinks(interconnectedNodes);
		
		for(int i=1; i<interconnectedNodes.size(); i++) {
			Node currentNode = interconnectedNodes.get(i);
			int randomIndex = r.nextInt(i);
			Node nodeToConnect = interconnectedNodes.get(randomIndex);
			graph.linkNodes(currentNode, nodeToConnect, defaultWeightInterconnected, Link.LinkType.defaultWeightInterconnected);
			linkPairs.remove(new ImmutablePair<Integer, Integer>(nodeToConnect.getId(), currentNode.getId()));
		}
		
		int possibleLinks = interconnectedNodes.size() *
							(interconnectedNodes.size()-1)  / 2;
		
		float currentDegreeOfInterconnection = (possibleLinks - linkPairs.size()) / possibleLinks;
		
		while(currentDegreeOfInterconnection < this.degreeOfInterconnection) {
			Pair<Integer, Integer> newLink = linkPairs.stream().skip(new Random(this.randomSeed).nextInt(linkPairs.size())).findFirst().orElse(null);
			linkPairs.remove(newLink);
			Node n1 = graph.getNodeById(newLink.getLeft());
			Node n2 = graph.getNodeById(newLink.getRight());
			//graph.addLink(new Link(n1, n2, defaultWeightInterconnected, Link.LinkType.defaultWeightInterconnected));
			graph.linkNodes(n1, n2, defaultWeightInterconnected, Link.LinkType.defaultWeightInterconnected);
			currentDegreeOfInterconnection = (possibleLinks - linkPairs.size()) / possibleLinks;
		}
		
		return interconnectedNodes;
	}
	
	private List<Node> createGroupOfTreeNodes(int numOfNodes) {
		return createNodes(numOfNodes);
	}

	public List<Integer> getNumElementsPerInterconnectedLevelList() {
		return numElementsPerInterconnectedLevelList;
	}

	public void setNumElementsPerInterconnectedLevelList(List<Integer> numElementsPerInterconnectedLevelList) {
		this.numElementsPerInterconnectedLevelList = numElementsPerInterconnectedLevelList;
	}

	public List<Integer> getNumElementsPerTreeLevelList() {
		return numElementsPerTreeLevelList;
	}

	public void setNumElementsPerTreeLevelList(List<Integer> numElementsPerTreeLevelList) {
		this.numElementsPerTreeLevelList = numElementsPerTreeLevelList;
	}

	public float getDegreeOfInterconnection() {
		return degreeOfInterconnection;
	}

	public void setDegreeOfInterconnection(float degreeOfInterconnection) {
		this.degreeOfInterconnection = degreeOfInterconnection;
	}

	public long getRandomSeed() {
		return randomSeed;
	}

	public void setRandomSeed(long randomSeed) {
		this.randomSeed = randomSeed;
	}

	public float getDefaultWeightInterconnected() {
		return defaultWeightInterconnected;
	}

	public void setDefaultWeightInterconnected(float defaultWeightInterconnected) {
		this.defaultWeightInterconnected = defaultWeightInterconnected;
	}

	public float getDefaultWeightTree() {
		return defaultWeightTree;
	}

	public void setDefaultWeightTree(float defaultWeightTree) {
		this.defaultWeightTree = defaultWeightTree;
	}
	
	
}
