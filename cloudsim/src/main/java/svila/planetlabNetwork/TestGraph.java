package svila.planetlabNetwork;

public class TestGraph {
	public static void main(String[] args) {
		Graph g = new Graph();
		Node n0 = new Node(0);
		Node n1 = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		g.addNode(n0);
		g.addNode(n1);
		g.addNode(n2);
		g.addNode(n3);
		g.linkNodes(0, 1, 5);
		g.linkNodes(0, 2, 3);
		g.linkNodes(1, 3, 1);
		
		System.out.println("");
	}
}
