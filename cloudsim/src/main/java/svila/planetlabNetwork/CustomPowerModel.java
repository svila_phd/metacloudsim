package svila.planetlabNetwork;

import org.cloudbus.cloudsim.power.models.PowerModelSpecPower;

public class CustomPowerModel extends PowerModelSpecPower {

	private final double[] power;

	public CustomPowerModel(double[] power) {
		this.power = power;
	}
	
	@Override
	protected double getPowerData(int index) {
		return power[index];
	}
}
