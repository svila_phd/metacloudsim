/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package svila.tablesaw;

import static tech.tablesaw.aggregate.AggregateFunctions.sum;

import java.util.List;

import tech.tablesaw.api.IntColumn;
import tech.tablesaw.api.NumericColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.columns.numbers.IntColumnType;
import tech.tablesaw.plotly.Plot;
import tech.tablesaw.plotly.api.HorizontalBarPlot;
import tech.tablesaw.plotly.api.VerticalBarPlot;
import tech.tablesaw.plotly.components.Figure;
import tech.tablesaw.plotly.components.Layout;
import tech.tablesaw.plotly.components.Marker;
import tech.tablesaw.plotly.traces.BarTrace;
import tech.tablesaw.plotly.traces.Trace;

/** Basic sample vertical bar chart */
public class CustomBarExample {

  public static void main(String[] args) throws Exception {
    Table table = Table.read().csv("C:\\Users\\Sergi\\Documents\\vmsStackTest.csv");
    
    System.out.println(table);
    System.out.println(table.structure());
    
    
    List<String> vmList = table.columnNames();
    vmList.remove(0);
    
    Plot.show(
            VerticalBarPlot.create(
                "VM usage",
                table,
                "snapshot",
                Layout.BarMode.STACK,
                vmList.toArray(new String[0])));
  }
}