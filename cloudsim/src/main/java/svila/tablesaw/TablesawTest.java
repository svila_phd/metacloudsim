package svila.tablesaw;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

import org.apache.commons.lang3.ArrayUtils;
import org.cloudbus.cloudsim.UtilizationModelPlanetLabInMemory;
import org.cloudbus.cloudsim.examples.power.Constants;

import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.plotly.Plot;
import tech.tablesaw.plotly.api.LinePlot;
import tech.tablesaw.plotly.api.VerticalBarPlot;
import tech.tablesaw.plotly.components.Figure;
import tech.tablesaw.plotly.traces.ScatterTrace;
import tech.tablesaw.selection.Selection;

public class TablesawTest {
	
	
	private static void adjustRolling(Table t) {
		int minNumUnder = (int)(289 * 0.9);
		DoubleColumn diffColumn = t.doubleColumn("%").subtract(t.doubleColumn("rolling"));
		int numUnder = diffColumn.isLessThan(0).size();
		double mean = t.doubleColumn("%").mean();
		double offset = 0.0;
		while(numUnder < minNumUnder) {
			offset += mean*0.05;
			diffColumn = t.doubleColumn("%")
							.subtract(t.doubleColumn("rolling"))
							.subtract(offset);
			numUnder = diffColumn.isLessThan(0).size();
		}
		System.out.println(offset);
		DoubleColumn result = t.doubleColumn("rolling").add(offset);
		result.setName("result");
		t.addColumns(result);
	}
	
	public static void main(String[] args) throws NumberFormatException, IOException {

		String pathName = "C:\\Users\\Sergi\\Documents\\networkExperiments\\workloads\\planetlab\\20110303\\";
		
		//String file = "75-130-96-12_static_oxfr_ma_charter_com_irisaple_wup";
		String file = "146-179_surfsnel_dsl_internl_net_colostate_557";
		pathName += file;
		
		UtilizationModelPlanetLabInMemory utilization = new UtilizationModelPlanetLabInMemory(
				pathName, Constants.SCHEDULING_INTERVAL);
		
		DoubleColumn column = DoubleColumn.create("%", utilization.getData());
		Table t = Table.create("stats", column);
		
		//column.append(10);
		//column.append(20);
		
		int window = 6;
		
		//t.doubleColumn("CPU").append(30);
		//DoubleColumn result = t.doubleColumn("%").where(Selection.withRange(289-5,289));
		
		DoubleColumn rollingColumn = t.doubleColumn("%").rolling(6).geometricMean();
		rollingColumn.setName("rolling");
		t.addColumns(t.doubleColumn("%").rolling(window).geometricMean());
		t.addColumns(rollingColumn);
		DoubleColumn start = t.doubleColumn("%").where(Selection.withRange(0,window));
		double startMean = start.mean();
		for(int i=0; i<window; i++) {
			rollingColumn.set(i, startMean);
		}
		
		adjustRolling(t);
		
		System.out.println(t.print());
		
		double[] index = new double[289];
		for(int i=0; i<289; i++) {
			index[i] = i;
		}


		double[] percArray = ArrayUtils.toPrimitive(t.doubleColumn("%").asList().toArray(new Double[0]));
		double[] rollingArray = ArrayUtils.toPrimitive(t.doubleColumn("rolling").asList().toArray(new Double[0]));
		double[] resultArray = ArrayUtils.toPrimitive(t.doubleColumn("result").asList().toArray(new Double[0]));

		ScatterTrace trace1 = ScatterTrace
				.builder(index, percArray)
				.mode(ScatterTrace.Mode.LINE)
				.name("%")
				.build();
		ScatterTrace trace2 = ScatterTrace
				.builder(index, rollingArray)
				.mode(ScatterTrace.Mode.LINE)
				.name("rolling")
				.build();
		ScatterTrace trace3 = ScatterTrace
				.builder(index, resultArray)
				.fill(ScatterTrace.Fill.TO_ZERO_Y)
				.name("result")
				.build();
				
		Plot.show(new Figure(trace3, trace1, trace2));
		
		/*Plot.show(
		        VerticalBarPlot.create(
		            "Bar test", // plot title
		            t, // table
		            "%", // grouping column name
		            "%")); // numeric column name
		*/
		
	}
}
