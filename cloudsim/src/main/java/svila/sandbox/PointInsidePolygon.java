package svila.sandbox;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PointInsidePolygon {
	Map<String, Point2D.Double> points = new HashMap<>();
	List<Domain> domains = new ArrayList<>();
	
	public static void main(String[] args) {
		test1();
	}
	
	public PointInsidePolygon() {
		init();
	}
	
	public class Domain {
		public String name;
		List<Point2D.Double> points;
		
		public Domain(String name, List<Point2D.Double> points) {
			this.name = name;
			this.points = points;
		}
	}
	
	public static void test1() {
		PointInsidePolygon pip = new PointInsidePolygon();
		
		System.out.println(pip.getDomain(new Point2D.Double(0.3, 0.4)).name);
		System.out.println(pip.getDomain(new Point2D.Double(0.2, 0.8)).name);
		System.out.println(pip.getDomain(new Point2D.Double(0.2, 0.2)).name);
		System.out.println(pip.getDomain(new Point2D.Double(0.9, 0.1)).name);
		System.out.println(pip.getDomain(new Point2D.Double(0.9, 0.9)).name);
		System.out.println(pip.getDomain(new Point2D.Double(0.5, 0.5)).name);
		System.out.println(pip.getDomain(new Point2D.Double(0.4, 0.4)).name);
		System.out.println(pip.getDomain(new Point2D.Double(0.6, 0.6)).name);
		System.out.println(pip.getDomain(new Point2D.Double(1, 0.5)).name);

	}
	
	public void init() {
		points.put("A", new Point2D.Double(0, 0.5));
		points.put("B", new Point2D.Double(0.5, 1));
		points.put("C", new Point2D.Double(1, 0.5));
		points.put("D", new Point2D.Double(0.5, 0));
		points.put("E", new Point2D.Double(1, 1));
		points.put("F", new Point2D.Double(1, 0));
		points.put("G", new Point2D.Double(0, 1));
		points.put("O", new Point2D.Double(0, 0));
		
		
		domains.add(new Domain("BD", Arrays.asList(points.get("O"), points.get("A"), points.get("D"))));
		domains.add(new Domain("BD", Arrays.asList(points.get("A"), points.get("B"), points.get("D"))));
		domains.add(new Domain("BD", Arrays.asList(points.get("B"), points.get("C"), points.get("D"))));
		domains.add(new Domain("UD", Arrays.asList(points.get("A"), points.get("B"), points.get("G"))));
		domains.add(new Domain("UD", Arrays.asList(points.get("C"), points.get("D"), points.get("F"))));
		domains.add(new Domain("AD", Arrays.asList(points.get("B"), points.get("C"), points.get("E"))));
	}
	
	public Domain getDomain(Point2D.Double point) {
		for(Domain domain : domains) {
			if(pointInTriangle(point, domain)) {
				return domain;
			}
		}
		return null;
	}
	
	private boolean pointInTriangle(Point2D.Double pt, Domain domain) {
		double d1, d2, d3;
		boolean hasNeg, hasPos;
		List<Point2D.Double> domainPoints = domain.points;
		d1 = sign(pt, domainPoints.get(0), domainPoints.get(1));
		d2 = sign(pt, domainPoints.get(1), domainPoints.get(2));
		d3 = sign(pt, domainPoints.get(2), domainPoints.get(0));
		
		hasNeg = (d1 < 0) || (d2 < 0) || (d3 < 0);
		hasPos = (d1 > 0) || (d2 > 0) || (d3 > 0);
		
		return !(hasNeg && hasPos);
	}
	
	private double sign(Point2D.Double p1, Point2D.Double p2, Point2D.Double p3) {
		return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
	}
}
