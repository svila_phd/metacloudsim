package svila.sandbox;

import java.util.Arrays;

import org.cloudbus.cloudsim.util.MathUtil;

public class CountNonZeroBeginningTest {
	public static void main(String[] args) {
		double[] data = new double[6];		
		data[0] = 0.0;
		data[1] = 0.0;
		data[2] = 0.0;
		data[3] = 4.0;
		data[4] = 3.0;
		data[5] = 0.0;
		
		System.out.println("Array: " + Arrays.toString(data));

		System.out.println("countNonZeroBeginning: " + MathUtil.countNonZeroBeginning(data));
		System.out.println("countNonZeroEnding: " + MathUtil.countNonZeroEnding(data));
	}
}
