package svila.sandbox;

public class IntegrationTest {
	public static float integrate(float[] x, float[] y) {
		float area = 0.0f;
		float leftX = x[0], leftY = y[0];
		float rightX, rightY;
		for(int i=1; i<x.length; i++) {
			rightX = x[i];
			rightY = y[i];
			area += (leftY+rightY) / 2 * (rightX - leftX); // Area of trapezoid
			leftX = rightX;
			leftY = rightY;
		}
		return area;
	}
	
	public static void main(String[] args) {
		float[] x = { 0,  10, 20};
		float[] y = {50, 100, 20};
		float area = integrate(x, y);
		System.out.println("Area: " + area);
	}
}
