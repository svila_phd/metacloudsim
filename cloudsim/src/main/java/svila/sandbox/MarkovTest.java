package svila.sandbox;

import svila.policiesVmMigration.future.markov.MarkovStates;

public class MarkovTest {
	public static void main(String[] args) {
		double[] historic1 = {
				0.74, 0.78, 0.66, 0.65, 0.99, 0.64, 0.63, 0.88, 0.84, 0.71,
				0.94, 0.70, 0.79, 0.80, 0.62, 0.62, 0.60, 0.64, 0.73, 0.67,
				0.63, 0.86, 0.93, 0.68, 0.86, 0.95, 0.96, 0.71,	0.74, 0.80
		};
		
		double[] historic2 = {
				0.62, 0.60, 0.70, 0.86, 0.79, 0.96, 0.81, 0.83, 0.62, 0.85,
				0.85, 0.97, 0.72, 0.86, 0.87, 0.75, 0.67, 0.96, 0.99, 0.75,
				0.69, 0.65, 0.61, 0.87, 0.78, 0.97, 0.99, 0.93, 0.68, 0.92
		};

		double[] historic3 = {
				0.05, 0.08, 0.97, 0.92, 0.61, 0.95, 0.39, 0.92, 0.43, 0.60,
				0.07, 0.67, 0.66, 0.13, 0.37, 0.07, 0.73, 0.89, 0.40, 0.72,
				0.66, 0.33, 0.41, 0.66, 0.22, 0.98, 0.97, 0.25, 0.31, 0.91
		};
		
		double upperThreshold = 0.80;
		double lowerThreshold = 0.1;
		MarkovStates ms = new MarkovStates(historic3, upperThreshold, lowerThreshold);
		ms.showStates();
		ms.showAllProbabilities();
		ms.getFutureMarkovState();
	}
}
