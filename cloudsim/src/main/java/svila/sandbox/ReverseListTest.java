package svila.sandbox;

import java.util.Arrays;
import java.util.Collections;

public class ReverseListTest {
	
	public static void main(String[] args) {
		int[] implemented = implementedFunction();
		System.out.println();
		int[] corrected = correctedFunction();
		
		System.out.println("Implemented: " + Arrays.toString(implemented));
		System.out.println("Corrected: " + Arrays.toString(corrected));
	}
	
	public static int[] implementedFunction() {
		int[] mipsHistory = new int[5];
		for(int i=0; i<mipsHistory.length; i++) {
			mipsHistory[i] = i;
		}
		Collections.reverse(Arrays.asList(mipsHistory));
		return mipsHistory;
	}
	
	public static int[] correctedFunction() {
		int[] mipsHistory = new int[5];
		for(int i=0; i<mipsHistory.length; i++) {
			mipsHistory[i] = i;
		}
		reverseArray(mipsHistory);
		return mipsHistory;
	}
	
	public static void reverseArray(int intArray[]) 
    { 
        int i, temp, size = intArray.length;
        for (i = 0; i < size / 2; i++) { 
            temp = intArray[i]; 
            intArray[i] = intArray[size - i - 1]; 
            intArray[size - i - 1] = temp; 
        } 
 
        /*print the reversed array*/
       System.out.println("Reversed Array: \n" + Arrays.toString(intArray)); 
    } 
}
