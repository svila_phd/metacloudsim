package svila.workloads;

import java.io.IOException;

import org.cloudbus.cloudsim.UtilizationModelPlanetLabInMemory;

public class UtilizationModelAlibaba2017InMemory extends UtilizationModelPlanetLabInMemory {
	public UtilizationModelAlibaba2017InMemory(String inputPath, double schedulingInterval, int linesToJump, int numData) throws NumberFormatException, IOException {
		super(inputPath, schedulingInterval, linesToJump, numData);
	}
	
	@Override
	protected boolean getJumpFirstLine() {
		return true;
	}

	@Override
	protected double readData(String line) {
		System.out.println(line);
		return Double.parseDouble(line.split(",")[1]);
	}
}
