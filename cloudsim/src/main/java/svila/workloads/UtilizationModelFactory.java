package svila.workloads;

import java.io.IOException;

import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelPlanetLabInMemory;

import svila.planetlabNetwork.PlanetLabNetworkConstants;

public class UtilizationModelFactory {

	public UtilizationModelFactory() {
	}
	
	public UtilizationModel getUtilitzationModel(String workload, String inputPath) throws NumberFormatException, IOException {
		return getUtilitzationModel(workload, inputPath, PlanetLabNetworkConstants.SCHEDULING_INTERVAL, 0, 289);
	}
	
	public UtilizationModel getUtilitzationModel(String workload, String inputPath, double schedulingInterval, int linesToJump, int numData) throws NumberFormatException, IOException {
		
		switch(workload) {
		case "planetlab":
			return new UtilizationModelPlanetLabInMemory(
					inputPath,
					schedulingInterval, linesToJump, numData);
		case "materna":
			return new UtilizationModelMaternaInMemory(
					inputPath,
					schedulingInterval, linesToJump, numData);
		case "bitbrains":
			return new UtilizationModelBitbrainsInMemory(
					inputPath,
					schedulingInterval, linesToJump, numData);
		case "alibaba2017":
			return new UtilizationModelAlibaba2017InMemory(
					inputPath,
					schedulingInterval, linesToJump, numData
					);
		case "alibaba2018":
			return new UtilizationModelAlibaba2018InMemory(
					inputPath,
					schedulingInterval, linesToJump, numData
					);
		case "azure":
			return new UtilizationModelAzureInMemory(
					inputPath,
					schedulingInterval, linesToJump, numData
					);
		case "azure-min":
			return new UtilizationModelAzureMinInMemory(
					inputPath,
					schedulingInterval, linesToJump, numData
					);
		case "azure-max":
			return new UtilizationModelAzureMaxInMemory(
					inputPath,
					schedulingInterval, linesToJump, numData
					);
		case "google2011":
			return new UtilizationModelGoogleMaxInMemory(
					inputPath,
					schedulingInterval, linesToJump, numData
					);
		case "google2011-mean":
			return new UtilizationModelGoogleMeanInMemory(
					inputPath,
					schedulingInterval, linesToJump, numData
					);
		default:
			System.err.print("UtilizationModelFactory: " + workload + " does not exist.");
			System.exit(-1);
			return null;
		}
	}
		
	
}
