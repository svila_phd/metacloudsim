package svila.workloads.configs;

public class PlanetLabWorkloadConfig {
	protected boolean CPU = false,
			  CPUPerc = true,
			  RAM = false,
			  RAMPerc = false,
			  BW = false,
			  BWPerc = false;
}
