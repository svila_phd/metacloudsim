package svila.workloads.configs;

public class MaternaWorkloadConfig {
	protected boolean CPU = true,
			  CPUPerc = true,
			  RAM = true,
			  RAMPerc = true,
			  BW = true,
			  BWPerc = false;
}
