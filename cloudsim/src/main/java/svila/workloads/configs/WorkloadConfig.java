package svila.workloads.configs;

public abstract class WorkloadConfig {
	
	protected boolean CPU, CPUPerc, RAM, RAMPerc, BW, BWPerc;

	public boolean isCPUAvailable() {
		return CPU;
	}

	public boolean isCPUPercAvailable() {
		return CPUPerc;
	}
	
	public boolean isRAMAvailable() {
		return RAM;
	}

	public boolean isRAMPercAvailable() {
		return RAMPerc;
	}
	
	public boolean isBWAvailable() {
		return BW;
	}

	public boolean isBWPercAvailable() {
		return BWPerc;
	}
}
