package svila.workloads.configs;

public class BitBrainsWorkloadConfig extends WorkloadConfig {
	protected boolean CPU = true,
					  CPUPerc = true,
					  RAM = true,
					  RAMPerc = true,
					  BW = true,
					  BWPerc = false;
}
