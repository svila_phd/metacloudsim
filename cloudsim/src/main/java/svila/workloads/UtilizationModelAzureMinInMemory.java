package svila.workloads;

import java.io.IOException;

public class UtilizationModelAzureMinInMemory extends UtilizationModelAzureInMemory {
	public UtilizationModelAzureMinInMemory(String inputPath, double schedulingInterval, int linesToJump, int numData) throws NumberFormatException, IOException {
		super(inputPath, schedulingInterval, linesToJump, numData);
	}
	
	@Override
	protected double readData(String line) {
		return Double.parseDouble(line.split(",")[1]);
	}
}
