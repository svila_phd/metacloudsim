package svila;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.cloudbus.cloudsim.examples.power.planetlab.PlanetLabRunner;

/**
 *
 * @author Sergi
 */
public class PlanetLabExecuter {
    public static void main(String[] args) throws IOException {
    
        boolean enableOutput = false;
        boolean outputToFile = false;
        
        // Crear un configurador en JSON
	
    String inputFolder = Paths.get("C:\\Users\\Sergi\\Desktop\\cloudsim\\target\\classes\\workload\\planetlab").toString();
    //String inputFolder = PlanetLabExecuter.class.getClassLoader().getResource("workload/planetlab").getPath();
	String outputFolder = "output";
	String workload = "20110303"; // PlanetLab workload

        List<PlanetLabExperiment> experiments = getExperiments();

        experiments.forEach((experiment) -> {
            JSONOutput.init();
            System.out.println("Running experiment " + experiment);
            new PlanetLabRunner(
                    enableOutput,
                    outputToFile,
                    inputFolder,
                    outputFolder,
                    workload,
                    experiment.getVmAllocationPolicy(),
                    experiment.getVmSelectionPolicy(),
                    experiment.getParameter()
            );
            
            //JSONOutput.writeJSON("C:\\Users\\Sergi\\Documents\\cloudsim4.0\\modules\\cloudsim\\output\\json\\" + experiment + ".json");
        });
    }
    
    public static List<PlanetLabExperiment> getExperiments() {
        List<PlanetLabExperiment> experiments = new ArrayList<>();
        //experiments.add(new PlanetLabExperiment("dvfs", "", ""));
        experiments.add(new PlanetLabExperiment("iqr", "mc", "1.5"));
        /*experiments.add(new PlanetLabExperiment("iqr", "mmt", "1.5"));
        experiments.add(new PlanetLabExperiment("iqr", "mu", "1.5"));
        experiments.add(new PlanetLabExperiment("iqr", "rs", "1.5"));
        experiments.add(new PlanetLabExperiment("lr", "mc", "1.2"));
        experiments.add(new PlanetLabExperiment("lr", "mmt", "1.2"));
        experiments.add(new PlanetLabExperiment("lr", "mu", "1.2"));
        experiments.add(new PlanetLabExperiment("lr", "rs", "1.2"));
        experiments.add(new PlanetLabExperiment("lrr", "mc", "1.2"));
        experiments.add(new PlanetLabExperiment("lrr", "mmt", "1.2"));
        experiments.add(new PlanetLabExperiment("lrr", "mu", "1.2"));
        experiments.add(new PlanetLabExperiment("lrr", "rs", "1.2"));
        experiments.add(new PlanetLabExperiment("mad", "mc", "2.5"));
        experiments.add(new PlanetLabExperiment("mad", "mmt", "2.5"));
        experiments.add(new PlanetLabExperiment("mad", "mu", "2.5"));
        experiments.add(new PlanetLabExperiment("mad", "rs", "2.5"));
        experiments.add(new PlanetLabExperiment("thr", "mc", "0.8"));
        experiments.add(new PlanetLabExperiment("thr", "mmt", "0.8"));
        experiments.add(new PlanetLabExperiment("thr", "mu", "0.8"));
        experiments.add(new PlanetLabExperiment("thr", "rs", "0.8"));*/
        return experiments;
    }
}
