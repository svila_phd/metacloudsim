package svila.metricsLogic;

import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.StaticResources;

public class DatacenterAnalyzer {
	public DatacenterStats datacenterStats;
	public int index;
	int steps;
	
	public DatacenterAnalyzer() {
		this.index = -1;
	}
	
	public void init(PowerDatacenter datacenter, HostAnalyzer hostAnalyzer) {
		steps = (int) (StaticResources.getCE().getSimulationTimeLimit() / 300);
		this.datacenterStats = new DatacenterStats(datacenter, hostAnalyzer, 0, steps, 12);
	}
	
	public void updateStats() {
		datacenterStats.update();
	}
}
