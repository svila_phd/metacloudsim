package svila.metricsLogic;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.HostStateHistoryEntry;
import org.cloudbus.cloudsim.Vm;

import tech.tablesaw.api.BooleanColumn;
import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.IntColumn;
import tech.tablesaw.api.Table;

public class HostStats {
	public static int INDEX = 0, PERC = 1, CPU = 2,
			REQ_MIG = 3, ROLL_PERC = 4, ROLL_RESULT = 5;
	
	public HostDynamicWorkload host;
	Map<Integer, Set<Vm>> vmsMap;
	Table historicTable;
	List<HostStateHistoryEntry> history;
	int index;
	int firstStep;
	int simSteps;
	int forecastingSteps;
	int totalSteps;
	int rollingWindow = 2;
	double percentageUnder = 0.8;
	
	public HostStats(HostDynamicWorkload host, int firstStep, int simSteps, int forecastingSteps) {
		this.index = this.firstStep - 1;
		this.host = host;
		this.firstStep = firstStep;
		this.simSteps = simSteps;
		this.forecastingSteps = forecastingSteps;
		this.totalSteps = simSteps + forecastingSteps;
		this.history = this.host.getStateHistory();
		this.vmsMap = new HashMap<>();
		int[] indexArray = new int[this.totalSteps];
		Arrays.setAll(indexArray, i -> i+firstStep);
		this.historicTable = getNewHistoricTable(indexArray);
	}
	
	private Table getNewHistoricTable(int[] indexArray) {
		int length = indexArray.length;
		IntColumn indexColumn = IntColumn.create("index", indexArray);
		DoubleColumn percColumn = DoubleColumn.create("%", length);
		DoubleColumn cpuColumn = DoubleColumn.create("CPU", length);
		BooleanColumn requireMigrationColumn = BooleanColumn.create("RequireMigration", length);
		DoubleColumn rollPercColumn = DoubleColumn.create("Roll%", length);
		DoubleColumn rollResultColumn = DoubleColumn.create("Result", length);

		return Table.create("Host" + host.getNetworkId(),
				indexColumn, percColumn, cpuColumn,
				requireMigrationColumn, rollPercColumn, rollResultColumn); 
	}
	
	private HostStats(HostDynamicWorkload host, Table slicedTable, int firstStep, int simSteps, int forecastingSteps) {
		this.host = host;
		this.historicTable = slicedTable;
		this.firstStep = firstStep;
		this.simSteps = simSteps;
		this.forecastingSteps = forecastingSteps;
		this.totalSteps = this.simSteps + this.forecastingSteps;
		this.index = firstStep-1;
	}
	
	public HostStats getSlice(int firstStep, int lastStep, int forecastingSteps) {
		Table slicedTable = historicTable.inRange(firstStep, lastStep+1);
		int[] indexArray = new int[forecastingSteps];
		Arrays.setAll(indexArray, i -> lastStep + 1 + i);
		Table forecastingTable = this.getNewHistoricTable(indexArray);
		slicedTable.append(forecastingTable);
		return new HostStats(this.host, slicedTable, firstStep, lastStep-firstStep+1, forecastingSteps);
	}
	
	public int getFixedIndex(int index) {
		return index - this.firstStep;
	}
	
	public void putVms(int index, List<Vm> vms) {
		Set<Vm> vmSet = new HashSet<>();
		for(Vm vm: vms) {
			vmSet.add(vm);
		}
		
		vmsMap.put(getFixedIndex(index), vmSet);
	}
	
	public Set<Vm> getVms(int step) {
		return vmsMap.get(getFixedIndex(step));
	}
	
	public void putPerc(int index, double percValue) {
		historicTable.doubleColumn(PERC).set(getFixedIndex(index), percValue);
	}
	
	public void putCPU(int index, double mipsValue) {
		historicTable.doubleColumn(CPU).set(getFixedIndex(index), mipsValue);
	}
	
	public void putRequireMigration(int index) {
		historicTable.booleanColumn(REQ_MIG).set(getFixedIndex(index), true);
	}
	
	public double getCPU(int step) {
		return historicTable.doubleColumn(CPU).get(getFixedIndex(step));
	}
	
	public double getCPUPerc(int step) {
		return historicTable.doubleColumn(PERC).get(getFixedIndex(step));
	}
	
	public double getDoubleValue(int column, int step) {
		return historicTable.doubleColumn(column).get(getFixedIndex(step));
	}
	
	private void adjustRolling() {
		int minNumUnder = (int)(index * percentageUnder);
		DoubleColumn diffColumn = historicTable.doubleColumn("%").subtract(historicTable.doubleColumn("Roll%"));
		int numUnder = diffColumn.isLessThan(0).size();
		double mean = historicTable.doubleColumn("%").mean();
		double offset = 0.0;
		if(mean != 0.0) {
			while(numUnder < minNumUnder ) {
				offset += mean*0.05;
				diffColumn = historicTable.doubleColumn("%")
								.subtract(historicTable.doubleColumn("Roll%"))
								.subtract(offset);
				numUnder = diffColumn.isLessThan(0).size();
			}
		}
		DoubleColumn result = historicTable.doubleColumn("Roll%").copy();
		result = result.add(offset);
		result.setName("Result");
		historicTable.replaceColumn(ROLL_RESULT, result);
	}
	
	public void update() {
		index++;
		int last = history.size()-1;
		double mips = history.get(last).getRequestedMips();
		double perc = mips / host.getTotalMips();
		
		putPerc(index, perc);
		putCPU(index, mips);
		
		/*if(this.host.getId() == 14 && index >= 182) {
			double hostMips = 0.0;
			for(Vm vm : host.getVmList()) {
				hostMips += vm.getCurrentRequestedTotalMips();
			}
			System.out.println((index*300) + " " + index + " " + last + ": " + mips + " Vms: " + host.getVmList().size());
			System.out.println("Calc vms mips: " + hostMips);
		}*/
		
		//DoubleColumn rollingColumn = historicTable.doubleColumn("%").rolling(rollingWindow).mean();
		//rollingColumn.setName("Roll%");
		//historicTable.replaceColumn(ROLL_PERC, rollingColumn);
		
		//if(index==steps-2) {
		//	adjustRolling();
		//}
		
		//System.out.println(index);
		//System.out.println(historicTable.print(index));
		
		putVms(index, host.getVmList());
	}
}
