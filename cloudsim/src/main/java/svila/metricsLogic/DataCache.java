package svila.metricsLogic;

import java.util.HashMap;
import java.util.Map;

import org.cloudbus.cloudsim.power.PowerVm;

import svila.Tools;

public abstract class DataCache {
	Map<String, Float[]> arrayCache;
	Map<String, Float> valueCache;
	
	public DataCache() {
		this.arrayCache = new HashMap<>();
		this.valueCache = new HashMap<>();
	}
	
	public void putArray(String key, Float[] array) {
		this.arrayCache.put(key, array);
	}
	
	public void putArray(String key, double[] array) {
		this.arrayCache.put(key, Tools.doubleToFloatArray(array));
	}
	
	public Float[] getArray(String key) {
		if(!this.arrayCache.containsKey(key)) {
			this.calculateArray(key);
		}
		return this.arrayCache.get(key);
	}
	
	public abstract void calculateArray(String key);
	
	public void putValue(String key, Float value) {
		this.valueCache.put(key, value);
	}
	
	public void putValue(String key, double value) {
		this.valueCache.put(key, Double.valueOf(value).floatValue());
	}
	
	public Float getValue(String key) {
		if(!this.valueCache.containsKey(key)) {
			this.calculateValue(key);
		}
		return this.valueCache.get(key);
	}
	
	public abstract void calculateValue(String key);
	
	public void clearArray(String key) {
		this.arrayCache.remove(key);
	}
	
	public void clearValue(String key) {
		this.valueCache.remove(key);
	}
	
	public void clearCache() {
		this.arrayCache.clear();
		this.valueCache.clear();
	}
}
