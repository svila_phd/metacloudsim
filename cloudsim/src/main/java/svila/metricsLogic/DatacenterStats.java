package svila.metricsLogic;

import java.util.Arrays;

import org.cloudbus.cloudsim.power.PowerDatacenter;

import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.results.HostData;
import svila.planetlabNetwork.results.NetworkPhysicalResults;
import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.IntColumn;
import tech.tablesaw.api.Table;

public class DatacenterStats {
	public static int INDEX = 0, ALLOCATED_MIPS = 1, FREE_MIPS = 2, HOSTS_ON = 3,
			HOSTS_OFF = 4, FREE_MIPS_FROM_HOSTS_OFF = 5, HOSTS_INTER_BW = 6,
			LINKS_BW = 7, REQUESTED_MIPS = 8;
	
	PowerDatacenter datacenter;
	HostAnalyzer hostAnalyzer;
	Table historicTable;
	int firstStep;
	int simSteps;
	int forecastingSteps;
	int totalSteps;
	int index;
	
	public DatacenterStats(PowerDatacenter datacenter, HostAnalyzer hostAnalyzer, int firstStep, int simSteps, int forecastingSteps) {
		this.datacenter = datacenter;
		this.hostAnalyzer = hostAnalyzer;
		this.index = this.firstStep - 1;
		this.firstStep = firstStep;
		this.simSteps = simSteps;
		this.forecastingSteps = forecastingSteps;
		this.totalSteps = simSteps + forecastingSteps;
		int[] indexArray = new int[this.totalSteps];
		Arrays.setAll(indexArray, i -> i+firstStep);
		this.historicTable = getNewHistoricTable(indexArray);
	}
	
	private Table getNewHistoricTable(int[] indexArray) {
		int length = indexArray.length;
		IntColumn indexColumn = IntColumn.create("index", indexArray);
		DoubleColumn allocatedMipsColumn = DoubleColumn.create("AllocatedMIPS", length);
		DoubleColumn freeMipsColumn = DoubleColumn.create("FreeMIPS", length);
		DoubleColumn hostsOnColumn = DoubleColumn.create("hostsOn", length);
		DoubleColumn hostsOffColumn = DoubleColumn.create("hostsOff", length);
		DoubleColumn freeMIPSFromHostsOffColumn = DoubleColumn.create("freeMIPSFromHostsOff", length);
		DoubleColumn hostsInterBWColumn = DoubleColumn.create("hostsInterBW", length);
		DoubleColumn linksBWColumn = DoubleColumn.create("linksBW", length);
		DoubleColumn requestedMipsColumn = DoubleColumn.create("requestedMIPS", length);

		return Table.create("Datacenter", indexColumn, allocatedMipsColumn, freeMipsColumn,
				hostsOnColumn, hostsOffColumn, freeMIPSFromHostsOffColumn,
				hostsInterBWColumn, linksBWColumn, requestedMipsColumn); 
	}
	
	private DatacenterStats(PowerDatacenter datacenter, HostAnalyzer hostAnalyzer, Table slicedTable, int firstStep, int simSteps, int forecastingSteps) {
		this.datacenter = datacenter;
		this.hostAnalyzer = hostAnalyzer;
		this.historicTable = slicedTable;
		this.firstStep = firstStep;
		this.simSteps = simSteps;
		this.forecastingSteps = forecastingSteps;
		this.totalSteps = this.simSteps + this.forecastingSteps;
		this.index = firstStep-1;
	}
	
	public DatacenterStats getSlice(int firstStep, int lastStep, int forecastingSteps) {
		Table slicedTable = historicTable.inRange(firstStep, lastStep+1);
		int[] indexArray = new int[forecastingSteps];
		Arrays.setAll(indexArray, i -> lastStep + 1 + i);
		Table forecastingTable = this.getNewHistoricTable(indexArray);
		slicedTable.append(forecastingTable);
		return new DatacenterStats(datacenter, hostAnalyzer, slicedTable, firstStep, lastStep-firstStep+1, forecastingSteps);
	}
	
	public int getFixedIndex(int index) {
		return index - this.firstStep;
	}
	
	public void update() {
		index++;
		
		double allocatedMIPSDatacenter = 0.0;
		double freeMIPSDatacenter = 0.0;
		double requestedMIPSDatacenter = 0.0;
		double hostsOnDatacenter = 0.0;
		double hostsOffDatacenter = 0.0;
		double freeMIPSFromHostsOffDatacenter = 0.0;
		double requestedHostsInterBW = 0.0;
		double requestedLinksBW = 0.0;
		int currentIndex = getFixedIndex(index);
		for(HostStats hostStats : this.hostAnalyzer.hostStats.values()) {
			double requestedMIPS = hostStats.host.getUtilizationOfCpuMips();
			boolean hostOn = requestedMIPS > 0;
			double freeMIPS = hostOn ? hostStats.host.getAvailableMips() : 0;
			double usableMIPS = hostOn ? hostStats.host.getTotalMips() : 0;
			double allocatedMIPS = hostOn ? Math.max(requestedMIPS, usableMIPS) : 0;
			allocatedMIPSDatacenter += allocatedMIPS;
			freeMIPSDatacenter += freeMIPS;
			hostsOnDatacenter += hostOn ? 1 : 0;
			hostsOffDatacenter += hostOn ? 0 : 1;
			freeMIPSFromHostsOffDatacenter += hostOn ? 0 : hostStats.host.getTotalMips();
			requestedMIPSDatacenter += requestedMIPS;
			//HostData hostData = hostStats.host.hostDatas.get(currentIndex);
			//requestedHostsInterBW += hostData.requestedinterBW;
		}
		
		requestedHostsInterBW = NetworkPhysicalResults.instance.getDataFromIntercommunicationsPerSnapshot()[getFixedIndex(index)];
		requestedLinksBW = NetworkPhysicalResults.instance.getDataFromCommunicationsPerSnapshot()[getFixedIndex(index)];
		
		this.putAllocatedMIPS(index, allocatedMIPSDatacenter);
		this.putAvailableMIPS(index, freeMIPSDatacenter);
		this.putHostsOn(index, hostsOnDatacenter);
		historicTable.doubleColumn(HOSTS_OFF).set(currentIndex, hostsOffDatacenter);
		historicTable.doubleColumn(FREE_MIPS_FROM_HOSTS_OFF).set(currentIndex, freeMIPSFromHostsOffDatacenter);
		historicTable.doubleColumn(HOSTS_INTER_BW).set(currentIndex, requestedHostsInterBW);
		historicTable.doubleColumn(LINKS_BW).set(currentIndex, requestedLinksBW);
		historicTable.doubleColumn(REQUESTED_MIPS).set(currentIndex, requestedMIPSDatacenter);
	}
	
	public void putAllocatedMIPS(int index, double allocatedMIPS) {
		historicTable.doubleColumn(ALLOCATED_MIPS).set(getFixedIndex(index), allocatedMIPS);
	}
	
	public void putAvailableMIPS(int index, double freeMIPS) {
		historicTable.doubleColumn(FREE_MIPS).set(getFixedIndex(index), freeMIPS);
	}
	
	public void putHostsOn(int index, double hostsOn) {
		historicTable.doubleColumn(HOSTS_ON).set(getFixedIndex(index), hostsOn);
	}
	
	public Table getTable() {
		return this.historicTable;
	}
}
