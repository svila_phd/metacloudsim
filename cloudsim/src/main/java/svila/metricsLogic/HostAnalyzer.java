package svila.metricsLogic;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.Vm;

import svila.planetlabNetwork.StaticResources;
import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.IntColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.plotly.Plot;
import tech.tablesaw.plotly.api.VerticalBarPlot;
import tech.tablesaw.plotly.components.Axis;
import tech.tablesaw.plotly.components.Axis.Spikes;
import tech.tablesaw.plotly.components.Figure;
import tech.tablesaw.plotly.components.Layout;
import tech.tablesaw.plotly.components.Layout.BarMode;
import tech.tablesaw.plotly.components.Layout.HoverMode;
import tech.tablesaw.plotly.traces.BarTrace;
import tech.tablesaw.plotly.traces.ScatterTrace;

public class HostAnalyzer {
	public Map<Integer, HostStats> hostStats;
	List<HostDynamicWorkload> hosts;
	public int index;
	int steps;
	
	public HostAnalyzer() {
		this.index = -1;
	}
	
	public void initHosts(List<HostDynamicWorkload> hosts) {
		this.hosts = hosts;
		steps = (int) (StaticResources.getCE().getSimulationTimeLimit() / 300);
		this.hostStats = new HashMap<>();
		for(HostDynamicWorkload host : hosts) {
			HostStats currentHostStats = new HostStats(host, 0, steps, 12);
			host.setHostStats(currentHostStats);
			hostStats.put(host.getNetworkId(), currentHostStats);
			
			
			//host.setHostCalculator(hostCalculator);
		}
	}
	
	public void showInfo() {
		int stepShowed = 182; //steps-2;
		if(index==stepShowed) {
			//int hostNetId = hosts.get(1).getNetworkId();
			int hostNetId = 32;
			HostStats hostStat = hostStats.get(hostNetId);
			System.out.println(hostStat.historicTable.printAll());
			this.showGraphic(hostStat.historicTable, stepShowed);
			this.showResume(stepShowed);
		}
	}
	
	private void showGraphic(Table t, int stepShowed) {
		int[] indexArray = ArrayUtils.toPrimitive(t.intColumn(VmStats.INDEX).asList().toArray(new Integer[0]));
		double[] indexArrayDoubles = Arrays.stream(indexArray).asDoubleStream().toArray();
		double[] percArray = ArrayUtils.toPrimitive(t.doubleColumn(VmStats.PERC).asList().toArray(new Double[0]), 0.0);
		double[] rollingPercArray = ArrayUtils.toPrimitive(t.doubleColumn(VmStats.ROLL_PERC).asList().toArray(new Double[0]), 0.0);
		double[] resultRollingPercArray = ArrayUtils.toPrimitive(t.doubleColumn(VmStats.ROLL_RESULT).asList().toArray(new Double[0]), 0.0);

		ScatterTrace percTrace = ScatterTrace
				.builder(indexArrayDoubles, percArray)
				.mode(ScatterTrace.Mode.LINE)
				.name("%")
				.build();
		ScatterTrace rollingPercTrace = ScatterTrace
				.builder(indexArrayDoubles, rollingPercArray)
				.mode(ScatterTrace.Mode.LINE)
				.name("Rolling %")
				.build();
		ScatterTrace resultRollingPercTrace = ScatterTrace
				.builder(indexArrayDoubles, resultRollingPercArray)
				.mode(ScatterTrace.Mode.LINE)
				.name("Rolling result")
				.build();
		
		Layout layout = Layout.builder()
				.title("Step" + stepShowed + " " + t.name())
				.height(560)
				.width(1264)
				.build();
		
		Plot.show(new Figure(layout, percTrace,
							rollingPercTrace,
							resultRollingPercTrace));
	}
	
	public void showResume(int stepShowed) {
		int[] indexHosts = new int[hosts.size()];
		double[] usedMips = new double[hosts.size()];
		double[] freeMips = new double[hosts.size()];
		double[] usedMipsPerc = new double[hosts.size()];
		double[] freeMipsPerc = new double[hosts.size()];
		
		int current = 0;
		for(HostDynamicWorkload host : hosts) {
			indexHosts[current] = host.getNetworkId();
			usedMips[current] = host.getUtilizationMips();
			freeMips[current] = host.getAvailableMips();
			usedMipsPerc[current] = host.getUtilizationMips() / host.getTotalMips();
			freeMipsPerc[current] = host.getAvailableMips() / host.getTotalMips();
			current++;
		}
		
		IntColumn indexCol = IntColumn.create("hostId", indexHosts);
		DoubleColumn usedMipsCol = DoubleColumn.create("usedMips", usedMips);
		DoubleColumn freeMipsCol = DoubleColumn.create("freeMips", freeMips);
		DoubleColumn usedMipsPercCol = DoubleColumn.create("usedMipsPerc", usedMipsPerc);
		DoubleColumn freeMipsPercCol = DoubleColumn.create("freeMipsPerc", freeMipsPerc);
		Table table = Table.create(indexCol, usedMipsCol,
				freeMipsCol, usedMipsPercCol, freeMipsPercCol);
		String[] columnNames = {"usedMips", "freeMips"};
		System.out.println(table.printAll());
		Plot.show(
	            VerticalBarPlot.create(
	            	"Step" + stepShowed + " " + "Host usage",
	                table,
	                "hostId",
	                Layout.BarMode.STACK,
	                columnNames
	               ));
		String[] columnNamesPerc = {"usedMipsPerc", "freeMipsPerc"};
		Plot.show(
	            VerticalBarPlot.create(
	            	"Step" + stepShowed + " " + "Host usage %",
	                table,
	                "hostId",
	                Layout.BarMode.STACK,
	                columnNamesPerc
	               ));
	}
	
	public void updateStats() {
		for(HostStats hostStat : hostStats.values()) {
			hostStat.update();
		}
		
		//showInfo();
	}
	
	public boolean isFirstTime() {
		return index==0;
	}
}
