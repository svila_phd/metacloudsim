package svila.metricsLogic;

import org.cloudbus.cloudsim.power.PowerVm;

public class VMCache extends DataCache {
	
	public static final String CPU_FORECASTING = "CPU-forecasting";
	public static final String CPU_FORECASTING_MAX = "CPU-forecasting-max";

	
	PowerVm vm;
	
	public VMCache(PowerVm vm) {
		this.vm = vm;
	}

	@Override
	public void calculateArray(String key) {
		switch(key) {
		case CPU_FORECASTING:
			Float[] forecastArray = vm.getVMCalculator().getForecastedMIPS();
			float forecastMaxValue = vm.getVMCalculator().getMaxValue(forecastArray);
			this.putArray(CPU_FORECASTING, forecastArray);
			this.putValue(CPU_FORECASTING_MAX, forecastMaxValue);
			break;
		}
		
	}

	@Override
	public void calculateValue(String key) {
		switch(key) {	
		case CPU_FORECASTING_MAX:
			Float[] forecastArray = vm.getVMCalculator().getForecastedMIPS();
			float forecastMaxValue = vm.getVMCalculator().getMaxValue(forecastArray);
			this.putArray(CPU_FORECASTING, forecastArray);
			this.putValue(CPU_FORECASTING_MAX, forecastMaxValue);
			break;
		}
	}

}
