package svila.metricsLogic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public class MigrationsTracker {
	
	Map<Integer, TriggeredMigrations> migrationsMap; // step, specific migrations for this step 
	
	public class NetIdMigration {
		public int vmNetId, originHostNetId, targetHostNetId;
		public NetIdMigration(int vmNetId, int originHostNetId, int targetHostNetId) {
			this.vmNetId = vmNetId;
			this.originHostNetId = originHostNetId;
			this.targetHostNetId = targetHostNetId;
		}
	}
	
	public class TriggeredMigrations {
		int step;
		List<NetIdMigration> migrations;
		Map<Integer, List<NetIdMigration>> originHostMap; // By originHost
		Map<Integer, List<NetIdMigration>> targetHostMap; // By targetHost
		Map<Integer, List<NetIdMigration>>	vmMap;		   // By vm
		public TriggeredMigrations(int step, List<NetIdMigration> migrations) {
			this.step = step;
			this.migrations = migrations;
			this.originHostMap = new HashMap<>();
			this.targetHostMap = new HashMap<>();
			this.vmMap = new HashMap<>();
			for(NetIdMigration migration : migrations) {
				this.originHostMap.computeIfAbsent(migration.originHostNetId, k -> new ArrayList<>()).add(migration);
				this.targetHostMap.computeIfAbsent(migration.targetHostNetId, k -> new ArrayList<>()).add(migration);
				this.vmMap.computeIfAbsent(migration.vmNetId, k -> new ArrayList<>()).add(migration);
			}
		}
		
		public List<NetIdMigration> getMigrationsByOriginHost(int originHostNetId) {
			return this.originHostMap.get(originHostNetId);
		}
		
		public List<NetIdMigration> getMigrationsByTargetHost(int targetHostNetId) {
			return this.targetHostMap.get(targetHostNetId);
		}
		
		public List<NetIdMigration> getMigrationsByVm(int vmNetId) {
			return this.originHostMap.get(vmNetId);
		}
		
		public List<NetIdMigration> getMigrations() {
			return this.migrations;
		}
	}
	
	public MigrationsTracker() {
		this.migrationsMap = new HashMap<>();
	}
	
	public TriggeredMigrations getTriggeredMigrations(int step) {
		return this.migrationsMap.get(step);
	}
	
	private List<NetIdMigration> getMigrationList(List<Map<String, Object>> migrationMap) {
		List<NetIdMigration> migrationsList = new ArrayList<>();
		for(Map<String, Object> map : migrationMap) {
			Vm vm = (Vm) map.get("vm");
			PowerHost targetHost = (PowerHost) map.get("host");
			PowerHost oldHost = (PowerHost) vm.getHost();
			migrationsList.add(new NetIdMigration(vm.getNetworkId(), oldHost.getNetworkId(), targetHost.getNetworkId()));
		}
		return migrationsList;
	}

	public void setScheduledMigrations(int step, List<Map<String, Object>> migrationMap) {
		TriggeredMigrations currentTriggeredMigrations = new TriggeredMigrations(step, getMigrationList(migrationMap));
		this.migrationsMap.put(step, currentTriggeredMigrations);
	}
}
