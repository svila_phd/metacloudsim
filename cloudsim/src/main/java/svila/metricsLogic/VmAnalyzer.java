package svila.metricsLogic;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.cloudbus.cloudsim.Vm;

import svila.metricsLogic.CPUCalculator.ResourceHistoryCalculatorFactory;
import svila.metricsLogic.CPUCalculator.VMCalculator;
import svila.planetlabNetwork.StaticResources;
import tech.tablesaw.api.Table;
import tech.tablesaw.plotly.Plot;
import tech.tablesaw.plotly.components.Figure;
import tech.tablesaw.plotly.components.Layout;
import tech.tablesaw.plotly.traces.ScatterTrace;

public class VmAnalyzer {
	public Map<Integer, VmStats> vmStats;
	public int index;
	int steps;
	
	public VmAnalyzer() {
		this.index = -1;
		this.steps = (int) (StaticResources.getCE().getSimulationTimeLimit() / 300);
		this.vmStats = new HashMap<>();
	}
	
	public void tryAddNewVms(List<? extends Vm> vms) {		
		String dataSourceType = StaticResources.getCE().getDataSource();
		String signalProcessingMethodType = StaticResources.getCE().getSignalProcessing();
		String forecastingTechniqueType = StaticResources.getCE().getForecastingTechnique();
		
		for(Vm vm : vms) {
			if(vmStats.containsKey(vm.getNetworkId())) {
				continue;
			}
			VmStats currentVmStats = new VmStats(vm, 0, steps, 12);
			vmStats.put(vm.getNetworkId(), currentVmStats);
			vm.setVmStats(currentVmStats);
			//Codi antic vm.setCpuCalculator(CPUCalculatorFactory.getCPUCalculator(cpuCalculatorType, vm));
			//CPUCalculator cpuCalculator = CPUCalculatorFactory.getDefaultCPUCalculator(vm);
			VMCalculator vmCalculator = ResourceHistoryCalculatorFactory.getVMCalculator(
					vm,
					dataSourceType,
					signalProcessingMethodType,
					forecastingTechniqueType);
			vm.setVMCalculator(vmCalculator);
		}
		//System.out.println("CPU calculator: " + vms.get(0).getCpuCalculator().getName());
	}
	
	public VmStats getVmStats(int vmNetId) {
		return vmStats.get(vmNetId);
	}
	
	public void showInfo() {
		int currentStep = 100;
		if(index==-1) {
			//for(Vm vm : vms) {
				int vmNetId = 4;
				VmStats vmStat = vmStats.get(vmNetId);
				vmStat.updateRolling();
				System.out.println(vmStat.historicTable.printAll());
				this.showGraphic(vmStat.historicTable);
				System.out.println("CPU: " + vmStat.historicTable.column(VmStats.CPU).get(index));
				System.out.println("Rolling: " + vmStat.historicTable.column(VmStats.ROLL_RESULT).get(index));
				//System.out.println("Rolling fixed: " + vmStat.historicTable.column(VmStats.ROLL_RESULT_FIX).get(index));

			//}
		}
	}
	
	private void showGraphic(Table t) {
		int[] indexArray = ArrayUtils.toPrimitive(t.intColumn(VmStats.INDEX).asList().toArray(new Integer[0]));
		double[] indexArrayDoubles = Arrays.stream(indexArray).asDoubleStream().toArray();
		double[] percArray = ArrayUtils.toPrimitive(t.doubleColumn(VmStats.PERC).asList().toArray(new Double[0]), 0.0);
		double[] rollingPercArray = ArrayUtils.toPrimitive(t.doubleColumn(VmStats.ROLL_PERC).asList().toArray(new Double[0]), 0.0);
		double[] resultRollingPercArray = ArrayUtils.toPrimitive(t.doubleColumn(VmStats.ROLL_RESULT).asList().toArray(new Double[0]), 0.0);
		double[] upperBandPercArray = ArrayUtils.toPrimitive(t.doubleColumn(VmStats.UPPER_BAND).asList().toArray(new Double[0]), 0.0);

		ScatterTrace percTrace = ScatterTrace
				.builder(indexArrayDoubles, percArray)
				.mode(ScatterTrace.Mode.LINE)
				.name("%")
				.build();
		ScatterTrace rollingPercTrace = ScatterTrace
				.builder(indexArrayDoubles, rollingPercArray)
				.mode(ScatterTrace.Mode.LINE)
				.name("Rolling %")
				.build();
		ScatterTrace resultRollingPercTrace = ScatterTrace
				.builder(indexArrayDoubles, resultRollingPercArray)
				.mode(ScatterTrace.Mode.LINE)
				.name("Rolling result")
				.build();
		ScatterTrace upperBandPercTrace = ScatterTrace
				.builder(indexArrayDoubles, upperBandPercArray)
				.mode(ScatterTrace.Mode.LINE)
				.name("Upper band")
				.build();
		
		Layout layout = Layout.builder()
				.title(t.name())
				.height(560)
				.width(1264)
				.build();
		
		Plot.show(new Figure(layout, percTrace,
							rollingPercTrace,
							resultRollingPercTrace,
							upperBandPercTrace));
	}
	
	public void updateStats() {
		for(VmStats vmStat : vmStats.values()) {
			vmStat.update();
		}
		
		//this.showInfo();
	}
	
	public void updateVMsToMigrateStats(List<? extends Vm> vmsToMigrate) {
		for(Vm vm : vmsToMigrate) {
			vmStats.get(vm.getNetworkId()).putRequireMigration(index);
		}
	}
	
	public boolean isFirstTime() {
		return index==0;
	}
}