package svila.metricsLogic;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.util.ExecutionTimeMeasurer;

import svila.policiesVmOptimizer.ExpandVMOptimizerPolicy;
import svila.policiesVmOptimizer.MetricsVMOptimizerPolicy;

public class ExpandStrategy {
	ExpandVMOptimizerPolicy optimizerPolicy;
	
	public ExpandStrategy(ExpandVMOptimizerPolicy optimizerPolicy) {
		this.optimizerPolicy = optimizerPolicy;
	}
	
	public List<Map<String, Object>> optimize() {
		return this.customOptimization();
	}
	
	
	public List<Map<String, Object>> customOptimization() {
		
		//----------------------------------------
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationTotal");
		ExecutionTimeMeasurer.start("optimizeAllocationHostSelection");
		//----------------------------------------
		
		//----------------------------------------
		// HostSaturationPolicy
		// Step 1: Get overutilized hosts
		List<PowerHostUtilizationHistory> overUtilizedHosts = optimizerPolicy.hostOverSaturationPolicy.getOverUtilizedHosts();
		
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryHostSelection().add(
				ExecutionTimeMeasurer.end("optimizeAllocationHostSelection"));
		//----------------------------------------
		
		//----------------------------------------
		// Restore point
		optimizerPolicy.saveAllocation();
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationVmSelection");
		//----------------------------------------
		
		//----------------------------------------
		// VMSelectionPolicy
		// Step 2: Get VMs to migrate from overutilized hosts
		List<? extends Vm> vmsToMigrate = optimizerPolicy.getVmsToMigrateFromHosts(overUtilizedHosts);
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryVmSelection().add(ExecutionTimeMeasurer.end("optimizeAllocationVmSelection"));
		Log.printLine("Reallocation of VMs from the over-utilized hosts:");
		ExecutionTimeMeasurer.start("optimizeAllocationVmReallocation");
		//----------------------------------------

		//----------------------------------------
		// VMMigrationPolicy
		// Step 3: Get to the migrated VMs a new host
		List<Map<String, Object>> migrationMap = optimizerPolicy.getNewVmPlacement(vmsToMigrate, new HashSet<Host>(
				overUtilizedHosts));
		//----------------------------------------

		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryVmReallocation().add(
				ExecutionTimeMeasurer.end("optimizeAllocationVmReallocation"));
		Log.printLine();
		//----------------------------------------

		//----------------------------------------
		// SPLIT
		// HostSaturationPolicy - underutilized
		// VMMigrationPolicy
		// Step 4:   Migration map from underutilized hosts
		// Step 4.1: Underutilization host detection
		// Step 4.2: VM migrations
		migrationMap.addAll(optimizerPolicy.getMigrationMapFromUnderUtilizedHosts(overUtilizedHosts));
		//----------------------------------------
		
		//----------------------------------------
		// Restore the VMs to their original host
		optimizerPolicy.restoreAllocation();
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryTotal().add(ExecutionTimeMeasurer.end("optimizeAllocationTotal"));
		//----------------------------------------

		
		//showMigrationMap(migrationMap);
		// Return map with VM migrations
		return migrationMap;
	}
}
