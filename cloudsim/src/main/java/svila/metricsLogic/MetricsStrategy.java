package svila.metricsLogic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerDynamicWorkload;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.UtilizationModelNull;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.HostDynamicWorkload.NetworkUtilization;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.util.ExecutionTimeMeasurer;

import svila.Tools;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethod;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethodFactory;
import svila.minicloud.sim.Scenario;
import svila.planetlabNetwork.Interaction;
import svila.planetlabNetwork.PlanetLabNetworkConstants;
import svila.planetlabNetwork.PlanetLabNetworkRunner;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;
import svila.policiesVmOptimizer.MetricsVMOptimizerPolicy;
import svila.policiesVmOptimizer.MetricsVMOptimizerPolicy.HostPercResources;
import svila.policiesVmOptimizer.MetricsVMOptimizerPolicy.SortDescendingByPercCPU;
import svila.workloads.UtilizationModelFactory;

public class MetricsStrategy {

	MetricsVMOptimizerPolicy optimizerPolicy;
	
	public MetricsStrategy(MetricsVMOptimizerPolicy optimizerPolicy) {
		this.optimizerPolicy = optimizerPolicy;
	}
	
	public List<Map<String, Object>> optimize() {
		/*
		Scenario currentScenario = new Scenario(optimizerPolicy.hostAnalyzer,
											optimizerPolicy.vmAnalyzer,
											optimizerPolicy.vmAnalyzer.index);
		
		currentScenario.setInteractions(PlanetLabNetworkRunner
										.getDatacenterBroker()
										.getInteractionsContainer()
										.getInteractions());
		
		currentScenario.printResume();
		*/
		
		// Obtenir dades general datacenter i hosts
		// Analitzar la situació i procedir en funció de les necessitats
		// Calcular quants hosts es necessiten
		// Decidir quins hosts es necessiten
		// Mètriques qualitat de cada VM
		// Com analitzar el passat? El real, o el nou passat amb les VMs actuals?
		// Si s'utilitza el consum real, han set altres VMs les que han executat allò
		// Si es té en compte si la VM ha estat saturada, pot ser a un altre host
		// Balanceig de CPU i BW dels hosts
		// Triple anàlisi, CPU, BW hosts, BW links
		// Ús del minisimulador per generar solucions
		
		/*
		List<PowerHostUtilizationHistory> hostList = this.optimizerPolicy.<PowerHostUtilizationHistory> getHostList();
		SignalProcessingMethod bollingerSignal = SignalProcessingMethodFactory.getSignalProcessingMethod(SignalProcessingMethodFactory.bollinger);
		
		for(PowerHostUtilizationHistory host : hostList) {
			double[] originalCPUdouble = host.getCPUUtilizationHistory();
			Float[] originalCPU = Tools.doubleToFloatArray(originalCPUdouble);
			Float[] bollingerCPU = bollingerSignal.apply(originalCPU);
			
			double[] originalBWdouble = host.getExternalBWUtilizationHistory();
			Float[] originalBW = Tools.doubleToFloatArray(originalBWdouble);
			Float[] bollingerBW = bollingerSignal.apply(originalBW);
			
		}
		*/
		
		// Obtenir quants hosts estan saturats, MIPS d'excès, calcular ràtios, indicar la qualitat del datacetner
		// Realitzar el mateix per la xarxa a nivell de host
		// Realitzar el mateix pels links
		
		// Realitzar el balanç per CPU i xarxa, forçar migracions si el resultat millora l'estat general
		// Per exemple, si un host està saturat per 1000 MIPS i en un nou host ho estaria per 100, migrar
		// Calcular la prioritat, si és més important balancejar per CPU o per xarxa
		// A nivell de links, si un link està saturat, moure les VMs implicades a altres hosts on no intervingui aquell link
		
		
		// Per cada host, generar un objecte Prevision, inicialment té el forecasting del host
		// Té per col·locar les VMs per migrar, analitzar aquestes VMs
		// Fer la previsió d'aquestes VMs
		// Elegir el mètode per assignar una VM a un host
		// Al col·locar la VM, entra dins de l'objecte Prevision, no al host, el host segueix com estava
		// La VM es col·loca a la llista de VMs que li entra al host, fent el forecasting + la previsió de cada VM
		// Si s'utilitza només un valor, es tenen les dades directes de si el host acaba saturat o no
		// Si hi ha més steps, es poden generar informes sobre quin % haurà saturació
		
		// Un cop realitzades totes les migracions. Mirar si el datacenter està equilibrat
		// Si no ho està, repetir el procés treient VMs de hosts plens i repetir
		
		
				
		return this.defaultOptimization();
	}
	
	public List<Map<String, Object>> testOptimization() {

		//----------------------------------------
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationTotal");
		ExecutionTimeMeasurer.start("optimizeAllocationHostSelection");
		//----------------------------------------
		
		//----------------------------------------
		// HostSaturationPolicy
		// Step 1: Get overutilized hosts
		List<PowerHostUtilizationHistory> overUtilizedHosts = optimizerPolicy.hostOverSaturationPolicy.getOverUtilizedHosts();
		
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryHostSelection().add(
				ExecutionTimeMeasurer.end("optimizeAllocationHostSelection"));
		//----------------------------------------
		
		//----------------------------------------
		// Restore point
		optimizerPolicy.saveAllocation();
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationVmSelection");
		//----------------------------------------
		
		//----------------------------------------
		// VMSelectionPolicy
		// Step 2: Get VMs to migrate from overutilized hosts
		List<? extends Vm> vmsToMigrate = optimizerPolicy.getVmsToMigrateFromHosts(overUtilizedHosts);
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryVmSelection().add(ExecutionTimeMeasurer.end("optimizeAllocationVmSelection"));
		Log.printLine("Reallocation of VMs from the over-utilized hosts:");
		ExecutionTimeMeasurer.start("optimizeAllocationVmReallocation");
		//----------------------------------------

		//----------------------------------------
		// VMMigrationPolicy
		// Step 3: Get to the migrated VMs a new host
		HashSet<Host> excludedHostsSet = new HashSet<Host>(overUtilizedHosts);
		if(StaticResources.getCE().isOnlyInitialHosts()) {
			excludedHostsSet.addAll(this.optimizerPolicy.getSwitchedOffHosts());
		}
		List<Map<String, Object>> migrationMap = optimizerPolicy.getNewVmPlacement(vmsToMigrate, excludedHostsSet);
		//----------------------------------------

		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryVmReallocation().add(
				ExecutionTimeMeasurer.end("optimizeAllocationVmReallocation"));
		Log.printLine();
		//----------------------------------------

		//----------------------------------------
		// SPLIT
		// HostSaturationPolicy - underutilized
		// VMMigrationPolicy
		// Step 4:   Migration map from underutilized hosts
		// Step 4.1: Underutilization host detection
		// Step 4.2: VM migrations
		migrationMap.addAll(optimizerPolicy.getMigrationMapFromUnderUtilizedHosts(overUtilizedHosts));
		//----------------------------------------
		
		
		// Balancing
		balance();
		
		
		
		
		//----------------------------------------
		// Restore the VMs to their original host
		optimizerPolicy.restoreAllocation();
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryTotal().add(ExecutionTimeMeasurer.end("optimizeAllocationTotal"));
		//----------------------------------------

		
		//showMigrationMap(migrationMap);
		// Return map with VM migrations
		return migrationMap;
	}
	
	//private List<PowerHostUtilizationHistory> getOverUtilizedHostsCPU() {
	//	 optimizerPolicy.hostOverSaturationPolicy. <PowerHostUtilizationHistory> getHostList();
	//}
	
	public class HostPercResources {
		public HostDynamicWorkload host;
		public double percCPU;
		public double percBW;
		
		public HostPercResources(HostDynamicWorkload host, double percCPU, double percBW) {
			this.host = host;
			this.percCPU = percCPU;
			this.percBW = percBW;
		}
		
		public String toString() {
			return String.format("Host %d: %.2f CPU - %.2f BW", host.getId(), this.percCPU, this.percBW);  
		}
	}
	
	public class SortDescendingByPercCPU implements Comparator<HostPercResources> {
		@Override
		public int compare(HostPercResources o1, HostPercResources o2) {
			return Double.compare(o2.percCPU, o1.percCPU);
		}
	}
	
	public static double calculateSD(Double numArray[])
    {
        double sum = 0.0, standardDeviation = 0.0;
        int length = numArray.length;

        for(double num : numArray) {
            sum += num;
        }

        double mean = sum/length;

        for(double num: numArray) {
            standardDeviation += Math.pow(num - mean, 2);
        }

        return Math.sqrt(standardDeviation/length);
    }
	
	public static double calculateMean(Double numArray[]) {
		double sum = 0;
		for (int i = 0; i < numArray.length; i++) {
			sum += numArray[i];
		}
		return sum / numArray.length;
	}
	
	private List<Vm> getAndRemoveVmsFromOverCPUMean(HostDynamicWorkload host, double meanCPU) {
		if(hpr.percCPU > meanCPU) {
			// Remove VMs until host CPU < meanCPU
			Vm vmToMigrate = this.optimizerPolicy.vmSelectionPolicy.getVmToMigrate((PowerHost) hpr.host);
			
			if (vmToMigrate == null) {
				continue;
			}
			vmsToMigrate.add(vm);
			host.vmDestroy(vm);
			if (!isHostOverUtilized(host)) {
				break;
			}
		}
	}
	
	public void balance() {
		List<HostPercResources> hostPercResourcesList = new ArrayList<>();
		for(HostStats hostStats : this.optimizerPolicy.hostAnalyzer.hostStats.values()) {
			NetworkUtilization nu = hostStats.host.getNetworkBWUtilizationMbIntraAndInter();
			HostPercResources hpr = new HostPercResources(hostStats.host,
									hostStats.getCPUPerc(this.optimizerPolicy.hostAnalyzer.index),
									nu.externalBW / nu.externalBWcapacity);
			hostPercResourcesList.add(hpr);
		}
		
		hostPercResourcesList.sort(new SortDescendingByPercCPU());
		ArrayList<Double> cpuList = new ArrayList<>();
		ArrayList<Double> bwList = new ArrayList<>();
		for(int i=0; i<hostPercResourcesList.size(); i++) {
			if(hostPercResourcesList.get(i).percCPU > 0.0) {
				cpuList.add(hostPercResourcesList.get(i).percCPU);
			}
			if(hostPercResourcesList.get(i).percBW > 0.0) {
				bwList.add(hostPercResourcesList.get(i).percBW);
			}
		}

		for(HostPercResources hpr : hostPercResourcesList) {
			System.out.println(hpr);
		}
		
		double sdCPU = calculateSD(cpuList.toArray(new Double[0]));
		double sdBW = calculateSD(bwList.toArray(new Double[0]));
		double meanCPU = calculateMean(cpuList.toArray(new Double[0]));
		System.out.println(String.format("SD CPU: %.2f", sdCPU));
		System.out.println(String.format("SD BW: %.2f", sdBW));
		
		double deviationDiff = sdCPU;
		double maxDiff = 0.2;
		List<Vm> vmsToMigrate = new ArrayList<>();
		if(deviationDiff >= maxDiff) {
			for(HostPercResources hpr : hostPercResourcesList) {
				// Remove VMs until host CPU < meanCPU
				// List<Vm> = function(hpr.host)
				// Exclude hosts over mean
				// Send VMs to other hosts
				
				
			}
			
			
		}
	}
	
	public List<Map<String, Object>> getCPUAndBWOptimization() {

		//----------------------------------------
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationTotal");
		ExecutionTimeMeasurer.start("optimizeAllocationHostSelection");
		//----------------------------------------
		
		//----------------------------------------
		// HostSaturationPolicy
		// Step 1: Get overutilized hosts
		List<PowerHostUtilizationHistory> overUtilizedHosts = optimizerPolicy.hostOverSaturationPolicy.getOverUtilizedHosts();
		
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryHostSelection().add(
				ExecutionTimeMeasurer.end("optimizeAllocationHostSelection"));
		//----------------------------------------
		
		//----------------------------------------
		// Restore point
		optimizerPolicy.saveAllocation();
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationVmSelection");
		//----------------------------------------
		
		//----------------------------------------
		// VMSelectionPolicy
		// Step 2: Get VMs to migrate from overutilized hosts
		List<? extends Vm> vmsToMigrate = optimizerPolicy.getVmsToMigrateFromHosts(overUtilizedHosts);
		
		// Calcular previsió/tendència de les VMs
		// Utilitzar aquests càlculs a l'hora de provar hosts, no recalcular cada vegada
		
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryVmSelection().add(ExecutionTimeMeasurer.end("optimizeAllocationVmSelection"));
		Log.printLine("Reallocation of VMs from the over-utilized hosts:");
		ExecutionTimeMeasurer.start("optimizeAllocationVmReallocation");
		//----------------------------------------

		//----------------------------------------
		// VMMigrationPolicy
		// Step 3: Get to the migrated VMs a new host
		HashSet<Host> excludedHostsSet = new HashSet<Host>(overUtilizedHosts);
		if(StaticResources.getCE().isOnlyInitialHosts()) {
			excludedHostsSet.addAll(this.optimizerPolicy.getSwitchedOffHosts());
		}
		List<Map<String, Object>> migrationMap = optimizerPolicy.getNewVmPlacement(vmsToMigrate, excludedHostsSet);
		//----------------------------------------

		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryVmReallocation().add(
				ExecutionTimeMeasurer.end("optimizeAllocationVmReallocation"));
		Log.printLine();
		//----------------------------------------

		//----------------------------------------
		// SPLIT
		// HostSaturationPolicy - underutilized
		// VMMigrationPolicy
		// Step 4:   Migration map from underutilized hosts
		// Step 4.1: Underutilization host detection
		// Step 4.2: VM migrations
		List<Map<String,Object>>migrationsFromUnderHosts = optimizerPolicy.getMigrationMapFromUnderUtilizedHosts(overUtilizedHosts);
		
		migrationMap.addAll(migrationsFromUnderHosts);
		//----------------------------------------
		
		//----------------------------------------
		// Restore the VMs to their original host
		optimizerPolicy.restoreAllocation();
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryTotal().add(ExecutionTimeMeasurer.end("optimizeAllocationTotal"));
		//----------------------------------------

		
		//showMigrationMap(migrationMap);
		// Return map with VM migrations
		return migrationMap;
	}
	
	public List<Map<String, Object>> defaultOptimization() {
		
		//----------------------------------------
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationTotal");
		ExecutionTimeMeasurer.start("optimizeAllocationHostSelection");
		//----------------------------------------
		
		//----------------------------------------
		// HostSaturationPolicy
		// Step 1: Get overutilized hosts
		List<PowerHostUtilizationHistory> overUtilizedHosts = optimizerPolicy.hostOverSaturationPolicy.getOverUtilizedHosts();
		
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryHostSelection().add(
				ExecutionTimeMeasurer.end("optimizeAllocationHostSelection"));
		//----------------------------------------
		
		//----------------------------------------
		// Restore point
		optimizerPolicy.saveAllocation();
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationVmSelection");
		//----------------------------------------
		
		//----------------------------------------
		// VMSelectionPolicy
		// Step 2: Get VMs to migrate from overutilized hosts
		List<? extends Vm> vmsToMigrate = optimizerPolicy.getVmsToMigrateFromHosts(overUtilizedHosts);
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryVmSelection().add(ExecutionTimeMeasurer.end("optimizeAllocationVmSelection"));
		Log.printLine("Reallocation of VMs from the over-utilized hosts:");
		ExecutionTimeMeasurer.start("optimizeAllocationVmReallocation");
		//----------------------------------------

		//----------------------------------------
		// VMMigrationPolicy
		// Step 3: Get to the migrated VMs a new host
		HashSet<Host> excludedHostsSet = new HashSet<Host>(overUtilizedHosts);
		if(StaticResources.getCE().isOnlyInitialHosts()) {
			excludedHostsSet.addAll(this.optimizerPolicy.getSwitchedOffHosts());
		}
		List<Map<String, Object>> migrationMap = optimizerPolicy.getNewVmPlacement(vmsToMigrate, excludedHostsSet);
		//----------------------------------------

		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryVmReallocation().add(
				ExecutionTimeMeasurer.end("optimizeAllocationVmReallocation"));
		Log.printLine();
		//----------------------------------------

		//----------------------------------------
		// SPLIT
		// HostSaturationPolicy - underutilized
		// VMMigrationPolicy
		// Step 4:   Migration map from underutilized hosts
		// Step 4.1: Underutilization host detection
		// Step 4.2: VM migrations
		List<Map<String,Object>>migrationsFromUnderHosts = optimizerPolicy.getMigrationMapFromUnderUtilizedHosts(overUtilizedHosts);
		
		migrationMap.addAll(migrationsFromUnderHosts);
		//----------------------------------------
		
		//----------------------------------------
		// Restore the VMs to their original host
		optimizerPolicy.restoreAllocation();
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryTotal().add(ExecutionTimeMeasurer.end("optimizeAllocationTotal"));
		//----------------------------------------

		
		//showMigrationMap(migrationMap);
		// Return map with VM migrations
		return migrationMap;
	}
}
