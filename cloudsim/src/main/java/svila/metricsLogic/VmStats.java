package svila.metricsLogic;

import java.util.Arrays;
import java.util.List;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmStateHistoryEntry;
import org.cloudbus.cloudsim.power.PowerVm;

import com.workday.insights.timeseries.arima.struct.ArimaParams;

import svila.forecasting.ARIMA;
import svila.metricsLogic.CPUCalculator.DataSource;
import svila.metricsLogic.CPUCalculator.DataSourceFactory;
import tech.tablesaw.api.BooleanColumn;
import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.IntColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.columns.Column;
import tech.tablesaw.selection.Selection;

public class VmStats {
	
	public static int INDEX = 0, HOST = 1, PERC = 2, CPU = 3, RAM = 4,
			REQ_MIG = 5, ROLL_PERC = 6, ROLL_RESULT = 7, ROLL_OFFSET = 8,
			LAST_W_AVG = 9, UPPER_BAND = 10;
	
	public Vm vm;
	Table historicTable;
	List<VmStateHistoryEntry> history;
	int index;
	int firstStep;
	int simSteps;
	int forecastingSteps;
	int totalSteps;
	int rollingWindow = 6;
	double percentageUnder = 0.8;
	
	public VmStats(Vm vm, int firstStep, int simSteps, int forecastingSteps) {
		this.index = firstStep-1;
		this.firstStep = firstStep;
		this.simSteps = simSteps;
		this.forecastingSteps = forecastingSteps;
		this.totalSteps = simSteps + forecastingSteps;
		this.vm = vm;
		this.history = this.vm.getStateHistory();
		int numRows = simSteps + rollingWindow;
		int[] indexArray = new int[numRows];
		Arrays.setAll(indexArray, i -> i+firstStep);
		this.historicTable = getNewHistoricTable(indexArray);
	}
	
	private Table getNewHistoricTable(int[] indexArray) {
		int length = indexArray.length;
		IntColumn indexColumn = IntColumn.create("index", indexArray);
		IntColumn hostColumn = IntColumn.create("hostNetId", length);
		String[] newColNames = {"index", "hostNetId", "%", "CPU", "RAM", "RequireMigration",
							"Roll%", "Result", "Offset",
							"LastWindowAvg", "UpperBand"};
		Column[] newCols = new Column[newColNames.length];
		newCols[0] = indexColumn;
		newCols[1] = hostColumn;
		for(int i=2; i<newCols.length; i++) {
			if(newColNames[i] == "RequireMigration") {
				newCols[i] = BooleanColumn.create(newColNames[i], length);
			} else {
				newCols[i] = DoubleColumn.create(newColNames[i], length);
			}
		}
		
		return Table.create("VM" + vm.getNetworkId(), newCols);
	}
	
	private VmStats(Vm vm, Table slicedTable, int firstStep, int simSteps, int forecastingSteps) {
		this.vm = vm;
		this.historicTable = slicedTable;
		this.firstStep = firstStep;
		this.simSteps = simSteps;
		this.forecastingSteps = forecastingSteps;
		this.totalSteps = this.simSteps + this.forecastingSteps;
		this.index = firstStep-1;
	}
	
	public int getCurrentIndex() {
		return index;
	}
	
	public int getFixedIndex(int index) {
		return index - this.firstStep;
	}
	
	public void putPerc(int index, double percValue) {
		historicTable.doubleColumn(PERC).set(getFixedIndex(index), percValue);
	}
	
	public void putCPU(int index, double mipsValue) {
		historicTable.doubleColumn(CPU).set(getFixedIndex(index), mipsValue);
	}
	
	public void putHostNetId(int index, int hostNetId) {
		historicTable.intColumn(HOST).set(getFixedIndex(index), hostNetId);
	}
	
	public void putRequireMigration(int index) {
		historicTable.booleanColumn(REQ_MIG).set(getFixedIndex(index), true);
	}
	
	public double getCPU(int step) {
		return historicTable.doubleColumn(CPU).get(getFixedIndex(step));
	}
	
	public double getPerc(int step) {
		return historicTable.doubleColumn(PERC).get(getFixedIndex(step));
	}
	
	public int getHostNetId(int step) {
		return historicTable.intColumn(HOST).get(getFixedIndex(step));
	}
	
	public double getDoubleValue(int column, int step) {
		return historicTable.doubleColumn(column).get(getFixedIndex(step));
	}
	
	private void adjustRolling() {
		int minNumUnder = (int)((index+1) * percentageUnder);
		DoubleColumn percUntilNow = historicTable.doubleColumn("%").inRange(0, index+1);
		DoubleColumn rollingUntilNow = historicTable.doubleColumn("Roll%").inRange(0, index+1);
		DoubleColumn diffColumn = percUntilNow.subtract(rollingUntilNow);
		int numUnder = diffColumn.isLessThan(0).size();
		double mean = percUntilNow.mean();
		double offset = 0.0;
		if(mean != 0.0) {
			while(numUnder < minNumUnder) {
				offset += mean*0.05;
				diffColumn = percUntilNow
								.subtract(rollingUntilNow)
								.subtract(offset);
				numUnder = diffColumn.isLessThan(0).size();
			}
		}
		DoubleColumn result = historicTable.doubleColumn("Roll%").copy();
		result = result.add(offset);
		result.setName("Result");
		historicTable.replaceColumn(ROLL_RESULT, result);
		historicTable.doubleColumn(ROLL_OFFSET).set(index, offset);
	}

	
	public double getRollingValueDecision() {
		return historicTable.doubleColumn(ROLL_RESULT).get(index+rollingWindow-2);
	}
	
	public double getLastRollingValueDecision() {
		return historicTable.doubleColumn(ROLL_RESULT).get(index+rollingWindow-1);
	}
	
	public void update() {
		index++;
		int last = history.size()-1;
		double perc = vm.getTotalUtilizationOfCpu(history.get(last).getTime());
		double mips = history.get(last).getRequestedMips();
		putPerc(index, perc);
		putCPU(index, mips);
		putHostNetId(index, vm.getHost().getNetworkId());
		updateRolling();
		updateUpperBand();
		
		//System.out.println(index);
		//System.out.println(historicTable.print(index));
		
//		if(index == 60) {
//			DataSource dataSource = this.vm.getCpuCalculator().getDataSource();
//			Float[] lastData = dataSource.getLastData(30);
//			DataSource originalDataSource = DataSourceFactory.getDataSource("original", vm);
//			Float[] lastOriginalData = originalDataSource.getLastData(30);
//			DataSource originalHistoryEntryDataSource = DataSourceFactory.getDataSource("originalHistoryEntry", vm);
//			Float[] lastOriginalHistoryEntryData = originalHistoryEntryDataSource.getLastData(30);
//			PowerVm powerVm = (PowerVm)this.vm;
//			List<Double> utilizationHistory = powerVm.getUtilizationHistory();
//			for(int i=0; i<30; i++) {
//				System.out.println(i + "\t" + utilizationHistory.get(i) +
//						"\t" + lastData[i]
//						+ "\t" + lastOriginalData[i]
//						+ "\t" + lastOriginalHistoryEntryData[i]
//				);
//			}
//			System.out.println("---------------------");
//			
//			dataSource = this.vm.getCpuCalculator().getDataSource();
//			lastData = dataSource.getLastDataStraightened(30);
//			originalDataSource = DataSourceFactory.getDataSource("original", vm);
//			lastOriginalData = originalDataSource.getLastDataStraightened(30);
//			originalHistoryEntryDataSource = DataSourceFactory.getDataSource("originalHistoryEntry", vm);
//			lastOriginalHistoryEntryData = originalHistoryEntryDataSource.getLastDataStraightened(30);
//			powerVm = (PowerVm)this.vm;
//			utilizationHistory = powerVm.getUtilizationHistory();
//			for(int i=0; i<30; i++) {
//				System.out.println(i + "\t" + utilizationHistory.get(30-1-i) +
//						"\t" + lastData[i]
//						+ "\t" + lastOriginalData[i]
//						+ "\t" + lastOriginalHistoryEntryData[i]
//				);
//			}
//			System.out.println();
//		}
	}
	
	public void updateRolling() {
		DoubleColumn rollingColumn = historicTable.doubleColumn("%").rolling(rollingWindow).mean();
		double startingValue = historicTable.doubleColumn("%").inRange(0, Math.max(rollingWindow, index+1)).mean();
		rollingColumn.set(Selection.withRange(0, Math.min(rollingWindow, index+1)), startingValue);

		rollingColumn.setName("Roll%");
		historicTable.replaceColumn(ROLL_PERC, rollingColumn);
		historicTable.doubleColumn(LAST_W_AVG).set(index, getRollingValueDecision());
		adjustRolling();
//		if(index==50) {
//			adjustRolling();
//			System.out.println(vm.getNetworkId() + ": " + getRollingValueDecision());
//		}
	}
	
	public void updateUpperBand() {
		//rolling_mean = trace.rolling(window=window_size).mean()
		//rolling_std  = trace.rolling(window=window_size).std()
		//upper_band = rolling_mean + (rolling_std*num_of_std)
		double numOfStd = 1.7;
		DoubleColumn rolling = historicTable.doubleColumn("%").rolling(rollingWindow).mean();
		DoubleColumn std = historicTable.doubleColumn("%").rolling(rollingWindow).variance().sqrt();
		DoubleColumn upperBand = rolling.add(std.multiply(numOfStd));
		upperBand.setName("UpperBand");
		historicTable.replaceColumn(UPPER_BAND, upperBand);
	}
	
	// TODO
	public Table getMarkovChainTable(int start, int finish) {
		return null;
	}
	
	public void forecastCPUPerc() {
		this.forecastCPUPerc(this.firstStep, this.firstStep + this.simSteps, this.forecastingSteps);
	}
	
	public void forecastCPUPerc(int firstStep, int lastStep, int forecastingSteps) {
		DoubleColumn past = this.historicTable.doubleColumn(PERC).inRange(getFixedIndex(firstStep), getFixedIndex(lastStep));
		
		// ARIMA parameters
		int p = 3, d = 0, q = 3, P = 1, D = 1, Q = 0, m = 0;
		int forecastSize = 6;
		ArimaParams arimaParams = new ArimaParams(p,d,q,P,D,Q,m);
		ARIMA arima = new ARIMA(arimaParams);
		arima.applyForecast(past.asDoubleArray(), forecastSize);
		double[] forecastedData = arima.getForecastedData();
		DoubleColumn percColumn = this.historicTable.doubleColumn(PERC);
		int size = lastStep - firstStep + 1;
		for(int i=0; i<forecastingSteps; i++) {
			percColumn.set(i+size, forecastedData[i]);
		}
	}
	
	public VmStats getSlice(int firstStep, int lastStep, int forecastingSteps) {
		Table slicedTable = historicTable.inRange(firstStep, lastStep+1);
		int[] indexArray = new int[forecastingSteps];
		Arrays.setAll(indexArray, i -> lastStep + 1 + i);
		Table forecastingTable = this.getNewHistoricTable(indexArray);
		slicedTable.append(forecastingTable);
		return new VmStats(this.vm, slicedTable, firstStep, lastStep-firstStep+1, forecastingSteps);
	}
	
	public Float[] getLastPercData(int size) {
		DoubleColumn lastPercData = this.historicTable.doubleColumn(PERC).inRange(index-size, index);
		return lastPercData.asFloatColumn().asObjectArray();
	}
	
	public Float[] getRangeCPUData(int firstStep, int lastStep) {
		DoubleColumn lastCPUData = this.historicTable.doubleColumn(CPU).inRange(firstStep, lastStep);
		return lastCPUData.asFloatColumn().asObjectArray();
	}
	
	public Float[] getLastCPUData(int size) {
		DoubleColumn lastCPUData = this.historicTable.doubleColumn(CPU).inRange(index-size+1, index+1);
		Float[] array = lastCPUData.divide(this.vm.getMips()).asFloatColumn().asObjectArray();
		Float[] reversedArray = new Float[array.length];
		for(int i=0; i<array.length; i++) {
			reversedArray[i] = array[array.length-i-1];
		}
		return reversedArray;
	}
}