package svila.metricsLogic.CPUCalculator;

import java.util.List;

import svila.planetlabNetwork.StaticResources;
import svila.python.PythonBridge;

public class PercXForecastingTechnique implements ForecastingTechnique {

	float perc;
	
	public PercXForecastingTechnique() {
		this.perc = 0.95f;
	}
	
	public PercXForecastingTechnique(float perc) {
		this.perc = perc;
	}
	
	public PercXForecastingTechnique(List<Float> params) {
		this.perc = params.get(0);
	}
	
	@Override
	public void setParams(List<Float> params) {
		this.perc = params.get(0);
	}
	
	@Override
	public Float[] apply(Float[] values) {
		return PythonBridge.forecastingWrapper.getPercentile(
					values, StaticResources.getCE().getMigrationInterval(), perc);
	}

	@Override
	public String getName() {
		return "perc" + (this.perc*100);
	}

	public float getPerc() {
		return perc;
	}

	public void setPerc(float perc) {
		this.perc = perc;
	}
}
