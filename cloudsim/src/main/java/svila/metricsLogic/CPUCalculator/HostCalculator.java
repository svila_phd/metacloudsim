package svila.metricsLogic.CPUCalculator;

import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;

import svila.Tools;

public class HostCalculator {
	protected PowerHostUtilizationHistory host;
	
	private SignalProcessingMethod signalProcessingMethod;
	private ForecastingTechnique forecastingTechnique;
	
	public HostCalculator(PowerHostUtilizationHistory host,
						 SignalProcessingMethod signalProcessingMethod,
						 ForecastingTechnique forecastingTechnique) {
		this.host = host;
		this.signalProcessingMethod = signalProcessingMethod;
		this.forecastingTechnique = forecastingTechnique;
	}
	
	public double getCurrentMIPS() {
		return host.getUtilizationMips();
	}
	
	public double getCalculatedCurrentMIPS() {
		Float[] forecastedData = getForecastedMIPS();
		float maxValue = getMaxValue(forecastingTechnique.apply(forecastedData));
		return maxValue;
	}
	
	public double getCalculatedMIPSWithChanges() {
		
		// GUARDAR FORECASTING IF NULL: CALCULAR, IF EXIST: RETORNAR
		// CACHE
		// AGREGAR FUNCIO PER NETEJAR CACHE
		
		// Realitzar al començament
		// Si s'agrega una VM durant el mateix step, agregar la VM, provision
		
		return 0.0; // TODO
	}
	
	public Float[] getForecastedMIPS() {
		double[] cpuHistory = host.getCPUMIPSHistory();
		Float[] cpuHistoryFloat = Tools.doubleToFloatArray(cpuHistory);
		Float[] processedData = signalProcessingMethod.apply(cpuHistoryFloat);
		return forecastingTechnique.apply(processedData);
	}
	
	public String getName() {
		return "Signal processing method: " + this.signalProcessingMethod.getName() + ", " +
			   "Forecasting technique: " + this.forecastingTechnique.getName();
	}
	
	public float getMaxValue(Float[] array) {
		float maxValue = array[0];
	    for(int i=1;i<array.length;i++){
	    	if(array[i] > maxValue){
	    		maxValue = array[i];
	        }
	    }
	    return maxValue;
	}
}
