package svila.metricsLogic.CPUCalculator;

import java.util.List;

public interface ForecastingTechnique {
	public Float[] apply(Float[] values);
	public void setParams(List<Float> params);
	public String getName();
}
