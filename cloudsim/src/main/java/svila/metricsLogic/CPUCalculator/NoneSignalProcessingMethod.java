package svila.metricsLogic.CPUCalculator;

import java.util.List;

public class NoneSignalProcessingMethod implements SignalProcessingMethod {

	@Override
	public void setParams(List<Float> params) {}
	
	@Override
	public Float[] apply(Float[] values) {
		return values;
	}

	@Override
	public String getName() {
		return "none";
	}

}
