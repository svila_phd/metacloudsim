package svila.metricsLogic.CPUCalculator;

import java.util.List;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmStateHistoryEntry;
import org.cloudbus.cloudsim.power.PowerVm;

public class OriginalDataSource extends DataSource {

	PowerVm vm;
	
	public OriginalDataSource(Vm vm) {
		this.vm = (PowerVm) vm; // Necessary casting, better if all is PowerVm, but it requires lot of changes
	}
	
	public Float[] getLastData(int lastSteps) {
		List<Double> historyList = this.vm.getUtilizationHistory();
		int numData = Math.min(lastSteps, historyList.size());
		Float[] data = new Float[numData];
		for(int i=numData-1; i>=0; i--) {
			data[i] = historyList.get(i).floatValue();
		}
		return data;
	}

	public String getName() {
		return "original";
	}
}
