package svila.metricsLogic.CPUCalculator;

import org.cloudbus.cloudsim.Vm;

import svila.metricsLogic.VmStats;

public class VmStatsDataSource extends DataSource {

	VmStats vmStats;
	
	public VmStatsDataSource(Vm vm) {
		this.vmStats = vm.getVmStats();
	}

	public Float[] getLastData(int lastSteps) {
		return vmStats.getLastCPUData(lastSteps);
	}

	public String getName() {
		return "vmStats";
	}

}
