package svila.metricsLogic.CPUCalculator;

import java.util.List;

import svila.python.PythonBridge;

public class BollingerSignalProcessingMethod implements SignalProcessingMethod {

	int winma;
	float alpha;
	
	public BollingerSignalProcessingMethod() {
		this.winma = 4;
		this.alpha = 1.5f;
	}
	
	public BollingerSignalProcessingMethod(int winma, float alpha) {
		this.winma = winma;
		this.alpha = alpha;
	}
	
	public BollingerSignalProcessingMethod(List<Float> params) {
		setParams(params);
	}
	
	@Override
	public void setParams(List<Float> params) {
		this.winma = params.get(0).intValue();
		this.alpha = params.get(1);
	}
	
	public int getWinma() {
		return winma;
	}

	public void setWinma(int winma) {
		this.winma = winma;
	}

	public float getAlpha() {
		return alpha;
	}

	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}

	@Override
	public Float[] apply(Float[] values) {
		return PythonBridge.signalProcessingWrapper.getBollinger(values, this.winma, this.alpha);
	}
	
	public Float[] apply(Float[] values, int winma, float alpha) {
		return PythonBridge.signalProcessingWrapper.getBollinger(values, winma, alpha);
	}

	@Override
	public String getName() {
		return "bollinger";
	}
}
