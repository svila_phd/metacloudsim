package svila.metricsLogic.CPUCalculator;

import org.cloudbus.cloudsim.Vm;

public class OriginalCPUCalculator extends VMCPUCalculatoror {

	public OriginalCPUCalculator(Vm vm) {
		super(vm);
	}

	@Override
	public double getCalculatedCurrentMIPS() {
		return vm.getCurrentRequestedTotalMips();
	}
	
	public String getName() {
		return "original";
	}

}
