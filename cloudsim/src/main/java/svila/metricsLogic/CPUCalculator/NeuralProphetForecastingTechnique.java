package svila.metricsLogic.CPUCalculator;

import java.util.List;

import svila.planetlabNetwork.StaticResources;
import svila.python.PythonBridge;

public class NeuralProphetForecastingTechnique implements ForecastingTechnique {
	
	@Override
	public void setParams(List<Float> params) {}
	
	@Override
	public Float[] apply(Float[] values) {
		// Revisar que no hagi fallat la predicció
		Float[] result = PythonBridge.forecastingWrapper.getNeuralProphet(values,
				StaticResources.getCE().getMigrationInterval());
		if(result.length == 1 && result[0] == -1) {
			return new Float[]{values[values.length-1]};
		}
		return result;
	}

	@Override
	public String getName() {
		return "neuralProphet";
	}
}
