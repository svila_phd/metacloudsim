package svila.metricsLogic.CPUCalculator;

public class SignalProcessingMethodFactory {

	public static String none           = "none";
	public static String bollinger      = "bollinger";
	public static String interpeaks     = "interpeaks";
	public static String interbollinger = "interbollinger";
	public static String interpeaksbollinger = "interpeaksbollinger";
	
	public static SignalProcessingMethod getSignalProcessingMethod(String SignalProcessingMethodType) {
		switch(SignalProcessingMethodType) {
		case "none":
			return new NoneSignalProcessingMethod();
		case "bollinger":
			return new BollingerSignalProcessingMethod();
		case "interpeaks":
			return new InterPeaksSignalProcessingMethod();
		case "interbollinger":
			return new InterBollingerSignalProcessingMethod();
		case "interpeaksbollinger":
			return new InterpeaksBollingerSignalProcessingMethod();
		default:
			System.err.println("Unknown SignalProcessingMethod: " + SignalProcessingMethodType);
            System.exit(-1);
            return null;
		}
	}
}
