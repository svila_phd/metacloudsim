package svila.metricsLogic.CPUCalculator;

import java.util.List;

import svila.planetlabNetwork.StaticResources;
import svila.python.PythonBridge;

public class KNNForecastingTechnique implements ForecastingTechnique {

	int n_neighbors;
	
	public KNNForecastingTechnique() {
		this.n_neighbors = 5;
	}
	
	public KNNForecastingTechnique(int n_neighbors) {
		this.n_neighbors = n_neighbors;
	}
	
	public KNNForecastingTechnique(List<Float> params) {
		setParams(params);
	}
	
	@Override
	public void setParams(List<Float> params) {
		this.n_neighbors = params.get(0).intValue();
	}
	
	@Override
	public Float[] apply(Float[] values) {
		if(values.length < StaticResources.getCE().getMigrationInterval()) {
			return new Float[] {values[values.length-1]};
		}
		return PythonBridge.forecastingWrapper.getKNN(values,
				StaticResources.getCE().getMigrationInterval(), this.n_neighbors);
	}
	
	public Float[] apply(Float[] values, int n_neighbors) {
		return PythonBridge.forecastingWrapper.getKNN(values,
				StaticResources.getCE().getMigrationInterval(), n_neighbors);
	}

	public int getN_neighbors() {
		return n_neighbors;
	}

	public void setN_neighbors(int n_neighbors) {
		this.n_neighbors = n_neighbors;
	}

	@Override
	public String getName() {
		return "KNN";
	}
}
