package svila.metricsLogic.CPUCalculator;

import org.cloudbus.cloudsim.Vm;

public class DataSourceFactory {
	
	public static String original = "original";
	public static String originalHistoryEntry = "originalHistoryEntry";
	public static String historic = "historic";
	public static String vmStats  = "vmStats";
	
	public static DataSource getDataSource(String DataSourceType, Vm vm) {
		switch(DataSourceType) {
		case "original":
			return new OriginalDataSource(vm);
		case "originalHistoryEntry":
			return new OriginalHistoryEntryDataSource(vm);
		case "historic":
			return new HistoricDataSource(vm);
		case "vmStats":
			return new VmStatsDataSource(vm);
		default:
			System.err.println("Unknown DataSource: " + DataSourceType);
            System.exit(-1);
            return null;
		}
	}
}
