package svila.metricsLogic.CPUCalculator;

import org.apache.commons.lang3.tuple.Pair;
import org.cloudbus.cloudsim.Vm;

import svila.planetlabNetwork.StaticResources;
import svila.python.PythonBridge;
import svila.python.SignalProcessingWrapper.SeasonalityData;

public class AutoArimaCPUCalculator extends VMCPUCalculatoror {
	public AutoArimaCPUCalculator(Vm vm) {
		super(vm);
	}

	@Override
	public double getCalculatedCurrentMIPS() {
		Float[] array = vm.getVmStats().getLastCPUData(StaticResources.getCE().getDefaultHistoryLength());
		SeasonalityData seasonalityData = PythonBridge.signalProcessingWrapper.getSeasonality(array);
		Float[] forecasting = PythonBridge.forecastingWrapper.getAutoArima(
				array, StaticResources.getCE().getMigrationInterval(), seasonalityData);
		return this.getMaxValue(forecasting);
	}

	public String getName() {
		return "autoArima";
	}

}
