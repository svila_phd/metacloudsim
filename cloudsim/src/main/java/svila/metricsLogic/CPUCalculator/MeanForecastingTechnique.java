package svila.metricsLogic.CPUCalculator;

import java.util.Arrays;
import java.util.List;

import svila.planetlabNetwork.StaticResources;

public class MeanForecastingTechnique implements ForecastingTechnique {
	
	@Override
	public void setParams(List<Float> params) {}

	@Override
	public Float[] apply(Float[] values) {
		double sum = 0;
		for(int i=0; i<values.length; i++) {
			sum += values[i];
		}
		Double result = sum / values.length;
		Float floatResult = result.floatValue();
		Float[] forecast = new Float[StaticResources.getCE().getMigrationInterval()];
		Arrays.fill(forecast, floatResult);
		return forecast;
	}

	@Override
	public String getName() {
		return "mean";
	}
}
