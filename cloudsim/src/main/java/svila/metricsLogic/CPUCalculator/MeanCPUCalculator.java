package svila.metricsLogic.CPUCalculator;

import java.util.Arrays;

import org.cloudbus.cloudsim.Vm;

import svila.planetlabNetwork.StaticResources;

public class MeanCPUCalculator extends VMCPUCalculatoror {
	public MeanCPUCalculator(Vm vm) {
		super(vm);
	}

	@Override
	public double getCalculatedCurrentMIPS() {
		Float[] array = vm.getVmStats().getLastCPUData(StaticResources.getCE().getDefaultHistoryLength());
		double sum = 0;
		for(int i=0; i<array.length; i++) {
			sum += array[i];
		}
		return sum / array.length;
	}

	public String getName() {
		return "mean";
	}
}
