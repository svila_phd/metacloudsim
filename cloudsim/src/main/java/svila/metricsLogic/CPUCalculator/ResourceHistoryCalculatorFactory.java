package svila.metricsLogic.CPUCalculator;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;

public class ResourceHistoryCalculatorFactory {
	public static VMCalculator getVMCalculator(Vm vm, String dataSourceType,
				String signalProcessingMethodType, String forecastingTechniqueType) {
		return new VMCalculator(vm, DataSourceFactory.getDataSource(dataSourceType, vm),
				SignalProcessingMethodFactory.getSignalProcessingMethod(signalProcessingMethodType),
				ForecastingTechniqueFactory.getForecastingTechnique(forecastingTechniqueType)
				);
	}
	
	public static VMCalculator getDefaultVMCalculator(Vm vm) {
		return getVMCalculator(vm, DataSourceFactory.original,
				SignalProcessingMethodFactory.none,
				ForecastingTechniqueFactory.last);
	}
	
	public static HostCalculator getHostCalculator(PowerHostUtilizationHistory host, String dataSourceType,
			String signalProcessingMethodType, String forecastingTechniqueType) {
	return new HostCalculator(host, SignalProcessingMethodFactory.getSignalProcessingMethod(signalProcessingMethodType),
			ForecastingTechniqueFactory.getForecastingTechnique(forecastingTechniqueType)
			);
	}
}
