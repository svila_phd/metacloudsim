package svila.metricsLogic.CPUCalculator;

import java.util.List;

import svila.planetlabNetwork.StaticResources;
import svila.python.PythonBridge;

public class FBProphetForecastingTechnique implements ForecastingTechnique {

	@Override
	public void setParams(List<Float> params) {}
	
	@Override
	public Float[] apply(Float[] values) throws RuntimeException {
		if(values.length < StaticResources.getCE().getMigrationInterval()) {
			return new Float[]{values[values.length-1]};
		}
		Float[] result;
		try {
			result = PythonBridge.forecastingWrapper.getFacebookProphet(values,
				StaticResources.getCE().getMigrationInterval());
		} catch(RuntimeException e) {
			  System.err.println("Python runtime exception");
			  e.printStackTrace();
			  throw new RuntimeException("RuntimeException");
		  }
		if(result.length == 1 && result[0] == -1) {
			return new Float[]{values[values.length-1]};
		}
		return result;
	}

	@Override
	public String getName() {
		return "fbProphet";
	}

}
