package svila.metricsLogic.CPUCalculator;

import java.util.List;

import svila.planetlabNetwork.StaticResources;
import svila.python.PythonBridge;

public class Perc95ForecastingTechnique implements ForecastingTechnique {
	
	@Override
	public void setParams(List<Float> params) {}
	
	@Override
	public Float[] apply(Float[] values) {
		return PythonBridge.forecastingWrapper.getPercentile(
					values, StaticResources.getCE().getMigrationInterval(), 0.95f);
	}

	@Override
	public String getName() {
		return "perc95";
	}
}
