package svila.metricsLogic.CPUCalculator;

import java.util.List;

import svila.planetlabNetwork.StaticResources;
import svila.python.PythonBridge;

public class SimpleExpSmoothingForecastingTechnique implements ForecastingTechnique {

	Float smoothing_level;
	
	public SimpleExpSmoothingForecastingTechnique() {
		this.smoothing_level = 0.8f;
	}
	
	public SimpleExpSmoothingForecastingTechnique(List<Float> params) {
		setParams(params);
	}
	
	@Override
	public void setParams(List<Float> params) {
		this.smoothing_level = params.get(0);
	}
	
	@Override
	public Float[] apply(Float[] values) {
		if(values.length < 10) {
			return new Float[]{values[values.length-1]};
		}
		
		return PythonBridge.forecastingWrapper.getSimpleExpSmoothing(values,
					StaticResources.getCE().getDefaultHistoryLength());
	}

	@Override
	public String getName() {
		return "simpleExpSmoothing";
	}

	public Float getSmoothing_level() {
		return smoothing_level;
	}

	public void setSmoothing_level(Float smoothing_level) {
		this.smoothing_level = smoothing_level;
	}
}
