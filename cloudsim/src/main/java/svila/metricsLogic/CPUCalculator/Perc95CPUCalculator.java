package svila.metricsLogic.CPUCalculator;

import org.cloudbus.cloudsim.Vm;

import svila.planetlabNetwork.StaticResources;
import svila.python.PythonBridge;

public class Perc95CPUCalculator extends VMCPUCalculatoror {
	public Perc95CPUCalculator(Vm vm) {
		super(vm);
	}

	@Override
	public double getCalculatedCurrentMIPS() {
		Float[] array = vm.getVmStats().getLastCPUData(StaticResources.getCE().getDefaultHistoryLength());
		Float[] forecast = PythonBridge.forecastingWrapper.getPercentile(
								array, StaticResources.getCE().getMigrationInterval(), 0.95f);
		return forecast[0];
	}
	
	public String getName() {
		return "perc95";
	}
}
