package svila.metricsLogic.CPUCalculator;

import java.util.List;

import svila.planetlabNetwork.StaticResources;
import svila.python.PythonBridge;
import svila.python.SignalProcessingWrapper.SeasonalityData;

public class HoltWintersExpSmoothingForecastingTechnique implements ForecastingTechnique {
	Float smoothing_level_value;
	Float smoothing_trend_value;
	Float smoothing_seasonal_value;
	Float damping_trend_value;
	
	public HoltWintersExpSmoothingForecastingTechnique() {
		this.smoothing_level_value = 0.8f;
		this.smoothing_trend_value = 0.2f;
		this.smoothing_seasonal_value = null;
		this.damping_trend_value = null;
	}
	
	@Override
	public void setParams(List<Float> params) {
		this.smoothing_level_value = params.get(0);
		this.smoothing_trend_value = params.get(1);
		
		if(params.get(2) == -1) {
			this.smoothing_seasonal_value = null;
		} else {
			this.smoothing_seasonal_value = params.get(2);
		}
		
		if(params.get(3) == -1) {
			this.damping_trend_value = null;
		} else {
			this.damping_trend_value = params.get(3);
		}
	}
	
	@Override
	public Float[] apply(Float[] values) {
		if(values.length < 10) {
			return new Float[]{values[values.length-1]};
		}
		
		// TODO Falta revisar que el forecasting no ha fallat, si falla,
		// aplicar una altra tècnica que no falli mai, per exemple, perc95
		SeasonalityData seasonalityData = PythonBridge.signalProcessingWrapper.getSeasonality(values);
		return PythonBridge.forecastingWrapper.getHoltWintersExpSmoothing(values,
				StaticResources.getCE().getMigrationInterval(), seasonalityData,
				this.smoothing_level_value, this.smoothing_trend_value,
				this.smoothing_seasonal_value, this.damping_trend_value);
	}

	@Override
	public String getName() {
		return "holtWintersExpSmoothing";
	}

	public Float getSmoothing_level_value() {
		return smoothing_level_value;
	}

	public void setSmoothing_level_value(Float smoothing_level_value) {
		this.smoothing_level_value = smoothing_level_value;
	}

	public Float getSmoothing_trend_value() {
		return smoothing_trend_value;
	}

	public void setSmoothing_trend_value(Float smoothing_trend_value) {
		this.smoothing_trend_value = smoothing_trend_value;
	}

	public Float getSmoothing_seasonal_value() {
		return smoothing_seasonal_value;
	}

	public void setSmoothing_seasonal_value(Float smoothing_seasonal_value) {
		this.smoothing_seasonal_value = smoothing_seasonal_value;
	}

	public Float getDamping_trend_value() {
		return damping_trend_value;
	}

	public void setDamping_trend_value(Float damping_trend_value) {
		this.damping_trend_value = damping_trend_value;
	}
}
