package svila.metricsLogic.CPUCalculator;

import org.cloudbus.cloudsim.Vm;

public class RollingCPUCalculator extends CPUCalculator {
	public RollingCPUCalculator(Vm vm) {
		super(vm);
	}

	@Override
	public double getCalculatedCurrentMIPS() {
		return vm.getVmStats().getLastRollingValueDecision() * vm.getMips();
	}
	
	public String getName() {
		return "rolling";
	}
}
