package svila.metricsLogic.CPUCalculator;

import java.io.IOException;

import org.cloudbus.cloudsim.UtilizationModelPlanetLabInMemory;
import org.cloudbus.cloudsim.Vm;

import svila.planetlabNetwork.PlanetLabNetworkConstants;
import svila.planetlabNetwork.StaticResources;
import svila.workloads.UtilizationModelFactory;

public class HistoricDataSource extends DataSource {

	Vm vm;
	RangedList historicList;
	
	public class RangedList {
		private double[] data;
		private int index0;
		
		public RangedList(double[] data, int index0) {
			this.data = data;
			this.index0 = index0;
		}
		
		public double getValue(int index) {
			return this.data[index0 + index];
		}
	}
	
	public HistoricDataSource(Vm vm) {
		this.vm = vm;
		fillHistoric();
	}
	
	private void fillHistoric() {
		String path = vm.getCloudletScheduler().getCloudletExecList().get(0).getCloudlet().getSourceFilepath();
		System.out.println(path);
		UtilizationModelFactory utilizationModelFactory = new UtilizationModelFactory();
		String workloadType = StaticResources.getCE().getWorkload();
		UtilizationModelPlanetLabInMemory utilizationModel = null;
		try {
			utilizationModel = (UtilizationModelPlanetLabInMemory)utilizationModelFactory.getUtilitzationModel(
					workloadType,
					path,
					PlanetLabNetworkConstants.SCHEDULING_INTERVAL,
					0,
					StaticResources.getCE().getDataStartStep() + 289
			);
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		historicList = new RangedList(utilizationModel.getData(), StaticResources.getCE().getDataStartStep());

	}

	public Float[] getLastData(int lastSteps) {
		Float[] data = new Float[lastSteps];
		int index = vm.getVmStats().getCurrentIndex();
		int dataI = 0;
		for(int i=index; i>index-lastSteps; i--) {
			data[dataI] = Double.valueOf(this.historicList.getValue(i)).floatValue();
			dataI++;
		}
		return data;
	}

	public String getName() {
		return "historic";
	}
}
