package svila.metricsLogic.CPUCalculator;

import java.util.Arrays;
import java.util.List;

import svila.planetlabNetwork.StaticResources;

public class LastForecastingTechnique implements ForecastingTechnique {

	@Override
	public void setParams(List<Float> params) {}
	
	@Override
	public Float[] apply(Float[] values) {
		Float[] forecast = new Float[StaticResources.getCE().getMigrationInterval()];
		Arrays.fill(forecast, values[values.length - 1]);
		return forecast;
	}

	@Override
	public String getName() {
		return "last";
	}

}
