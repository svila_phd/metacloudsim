package svila.metricsLogic.CPUCalculator;

import java.util.List;

import svila.planetlabNetwork.StaticResources;
import svila.python.PythonBridge;

public class HoltExpSmoothingForecastingTechnique implements ForecastingTechnique {	

	Float smoothingLevelValue;
	Float smoothingTrendValue;
	
	public HoltExpSmoothingForecastingTechnique() {
		this.smoothingLevelValue = 0.8f;
		this.smoothingTrendValue = 0.2f;
	}
	
	public HoltExpSmoothingForecastingTechnique(List<Float> params) {
		setParams(params);
	}
	
	@Override
	public void setParams(List<Float> params) {
		this.smoothingLevelValue = params.get(0);
		this.smoothingTrendValue = params.get(1);
	}

	@Override
	public Float[] apply(Float[] values) {
		if(values.length < 10) {
			return new Float[]{values[values.length-1]};
		}
		
		return PythonBridge.forecastingWrapper.getHoltExpSmoothing(values,
				StaticResources.getCE().getMigrationInterval(),
				this.smoothingLevelValue, this.smoothingTrendValue);
	}

	@Override
	public String getName() {
		return "holtExpSmoothing";
	}

	public Float getSmoothingLevelValue() {
		return smoothingLevelValue;
	}

	public void setSmoothingLevelValue(Float smoothingLevelValue) {
		this.smoothingLevelValue = smoothingLevelValue;
	}

	public Float getSmoothingTrendValue() {
		return smoothingTrendValue;
	}

	public void setSmoothingTrendValue(Float smoothingTrendValue) {
		this.smoothingTrendValue = smoothingTrendValue;
	}
}
