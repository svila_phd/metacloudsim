package svila.metricsLogic.CPUCalculator;

import java.util.Map;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmStateHistoryEntry;

public class OriginalHistoryEntryDataSource extends DataSource {
	Vm vm;
	
	public OriginalHistoryEntryDataSource(Vm vm) {
		this.vm = vm; // Necessary casting, better if all is PowerVm, but it requires lot of changes
	}
	
	public Float[] getLastData(int lastSteps) {
		Map<Double, VmStateHistoryEntry> historyMap = this.vm.getStateHistoryMap();
		int index = this.vm.getVmStats().getCurrentIndex() + 1;
		int numData = Math.min(lastSteps, index);
		Float[] data = new Float[numData];
		double currentTime;
		int dataI = 0;
		for(int i=index; i>index-numData; i--) {
			currentTime = i * 300.0 + 0.1;
			data[dataI] = Double.valueOf(historyMap.get(currentTime).getRequestedMips() / this.vm.getMips()).floatValue();
			dataI++;
		}
		return data;
	}

	public String getName() {
		return "originalEntryHistory";
	}
}
