package svila.metricsLogic.CPUCalculator;

import java.util.List;

import svila.python.PythonBridge;

public class InterPeaksSignalProcessingMethod implements SignalProcessingMethod {
	
	int rolling;
	float zScoreThreshold;
	
	public InterPeaksSignalProcessingMethod() {
		this.rolling = 3;
		this.zScoreThreshold = 2.0f;
	}
	
	public InterPeaksSignalProcessingMethod(int rolling, float zScoreThreshold) {
		this.rolling = rolling;
		this.zScoreThreshold = zScoreThreshold;
	}
	
	public InterPeaksSignalProcessingMethod(List<Float> params) {
		setParams(params);
	}
	
	@Override
	public void setParams(List<Float> params) {
		this.rolling = params.get(0).intValue();
		this.zScoreThreshold = params.get(1).floatValue();
	}
	
	@Override
	public Float[] apply(Float[] values) {
		return PythonBridge.signalProcessingWrapper.getInterPeaks(values, this.rolling, this.zScoreThreshold);
	}
	
	public Float[] apply(Float[] values, int rolling, float zScoreThreshold) {
		return PythonBridge.signalProcessingWrapper.getInterPeaks(values, rolling, zScoreThreshold);
	}

	public int getRolling() {
		return rolling;
	}

	public void setRolling(int rolling) {
		this.rolling = rolling;
	}

	public float getzScoreThreshold() {
		return zScoreThreshold;
	}

	public void setzScoreThreshold(float zScoreThreshold) {
		this.zScoreThreshold = zScoreThreshold;
	}

	@Override
	public String getName() {
		return "interPeaks";
	}
}
