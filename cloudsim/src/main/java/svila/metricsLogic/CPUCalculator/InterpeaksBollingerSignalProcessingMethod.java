package svila.metricsLogic.CPUCalculator;

import java.util.List;

import svila.python.PythonBridge;

public class InterpeaksBollingerSignalProcessingMethod  implements SignalProcessingMethod {

	int winma;
	float alpha;
	int rolling;
	float zScore;
	
	public InterpeaksBollingerSignalProcessingMethod() {
		this.winma = 4;
		this.alpha = 1.5f;
		this.rolling = 3;
		this.zScore = 1;
	}
	
	public InterpeaksBollingerSignalProcessingMethod(int winma, float alpha, int rolling, int zScore) {
		this.winma = winma;
		this.alpha = alpha;
		this.rolling = rolling;
		this.zScore = zScore;
	}
	
	public InterpeaksBollingerSignalProcessingMethod(List<Float> params) {
		setParams(params);
	}
	
	@Override
	public void setParams(List<Float> params) {
		this.winma = params.get(0).intValue();
		this.alpha = params.get(1);
		this.rolling = params.get(2).intValue();
		this.zScore = params.get(3);
	}
	
	@Override
	public Float[] apply(Float[] values) {
		Float[] interpeaksValues = PythonBridge.signalProcessingWrapper.getInterPeaks(values, this.rolling, this.zScore);
		return PythonBridge.signalProcessingWrapper.getBollinger(interpeaksValues, this.winma, this.alpha);
	}
	
	public Float[] apply(Float[] values, int winma, float alpha, int rolling, float zScore) {
		Float[] interpeaksValues = PythonBridge.signalProcessingWrapper.getInterPeaks(values, rolling, zScore);
		return PythonBridge.signalProcessingWrapper.getBollinger(interpeaksValues, winma, alpha);
	}

	public int getWinma() {
		return winma;
	}

	public void setWinma(int winma) {
		this.winma = winma;
	}

	public float getAlpha() {
		return alpha;
	}

	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}

	public int getRolling() {
		return rolling;
	}

	public void setRolling(int rolling) {
		this.rolling = rolling;
	}

	public float getzScore() {
		return zScore;
	}

	public void setzScore(float zScore) {
		this.zScore = zScore;
	}

	@Override
	public String getName() {
		return "InterpeaksBollinger";
	}
}
