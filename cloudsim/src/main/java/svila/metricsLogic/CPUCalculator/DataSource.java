package svila.metricsLogic.CPUCalculator;

public abstract class DataSource {
	
	public abstract Float[] getLastData(int lastSteps); // Original CloudSim order, from new to old
	public Float[] getLastDataStraightened(int lastSteps) { // Normal order, from old to new
		Float[] array = this.getLastData(lastSteps);
		Float[] straightArray = new Float[array.length];
		for(int i=0; i<array.length; i++) {
			straightArray[i] = array[array.length-i-1];
		}
		return straightArray;
	}
	public abstract String getName();
}
