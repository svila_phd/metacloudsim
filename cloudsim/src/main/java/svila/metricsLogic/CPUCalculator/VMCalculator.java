package svila.metricsLogic.CPUCalculator;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerVm;

import svila.metricsLogic.VMCache;
import svila.planetlabNetwork.StaticResources;

public class VMCalculator {
	
	protected Vm vm;
	
	private DataSource dataSource;
	private SignalProcessingMethod signalProcessingMethod;
	private ForecastingTechnique forecastingTechnique;
	
	private VMCache vmCache;
	
	public VMCalculator(Vm vm, DataSource dataSource,
						 SignalProcessingMethod signalProcessingMethod,
						 ForecastingTechnique forecastingTechnique) {
		this.vm = vm;
		this.dataSource = dataSource;
		this.signalProcessingMethod = signalProcessingMethod;
		this.forecastingTechnique = forecastingTechnique;
		this.vmCache = new VMCache((PowerVm) vm);
	}
	
	public VMCache getVmCache() {
		return vmCache;
	}

	public DataSource getDataSource() {
		return this.dataSource;
	}
	
	public double getCurrentMIPS() {
		return vm.getCurrentRequestedTotalMips();
	}
	
	public double getCalculatedCurrentMIPSCached() {
		return this.vmCache.getValue(VMCache.CPU_FORECASTING_MAX);
	}
	
	public double getCalculatedCurrentMIPS() {
		Float[] data = dataSource.getLastData(StaticResources.getCE().getDefaultHistoryLength());
		Float[] processedData = signalProcessingMethod.apply(data);
		return getMaxValue(forecastingTechnique.apply(processedData));
	}
	
	public Float[] getForecastedMIPS() {
		Float[] data = dataSource.getLastData(StaticResources.getCE().getDefaultHistoryLength());
		Float[] processedData = signalProcessingMethod.apply(data);
		return forecastingTechnique.apply(processedData);
	}
	
	public String getName() {
		return "Data source: " + this.dataSource.getName() + ", " +
			   "Signal processing method: " + this.signalProcessingMethod.getName() + ", " +
			   "Forecasting technique: " + this.forecastingTechnique.getName();
	}
	
	public float getMaxValue(Float[] array) {
		float maxValue = array[0];
	    for(int i=1;i<array.length;i++){
	    	if(array[i] > maxValue){
	    		maxValue = array[i];
	        }
	    }
	    return maxValue;
	}
}
