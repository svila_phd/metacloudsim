package svila.metricsLogic.CPUCalculator;

import java.util.List;

import svila.planetlabNetwork.StaticResources;
import svila.python.PythonBridge;
import svila.python.SignalProcessingWrapper.SeasonalityData;

public class AutoArimaForecastingTechnique implements ForecastingTechnique {

	@Override
	public void setParams(List<Float> params) {}
	
	@Override
	public Float[] apply(Float[] values) {
		SeasonalityData seasonalityData = PythonBridge.signalProcessingWrapper.getSeasonality(values);
		// TODO Revisar que el forecasting no ha fallat, si falla,
		// aplicar una altra tècnica que no falli mai, per exemple, perc95
		
		Float[] result;
		try {
			result = PythonBridge.forecastingWrapper.getAutoArima(
					values, StaticResources.getCE().getMigrationInterval(), seasonalityData);
		  } catch(RuntimeException e) {
			  System.err.println("Python runtime exception");
			  e.printStackTrace();
			  return new Float[]{values[values.length-1]};
		  }

		if(result.length == 1 && result[0] == -1) {
			return new Float[]{values[values.length-1]};
		}
		return result;
	}

	@Override
	public String getName() {
		return "autoArima";
	}
}
