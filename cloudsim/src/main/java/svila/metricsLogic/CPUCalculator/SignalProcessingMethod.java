package svila.metricsLogic.CPUCalculator;

import java.util.List;

public interface SignalProcessingMethod {
	public void setParams(List<Float> params);
	public Float[] apply(Float[] values);
	public String getName();
}
