package svila.policiesFactories;

import java.util.List;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.examples.power.Constants;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationInterQuartileRange;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationLocalRegression;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationLocalRegressionRobust;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationMedianAbsoluteDeviation;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationStaticThreshold;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicySimple;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

import svila.mann.AbsoluteCapacity;
import svila.mann.DemandRisk;
import svila.mann.LagoAllocator;
import svila.mann.MWFDVP;
import svila.mann.PercentageUtil;
import svila.mann.SWFDVP;
import svila.planetlabNetwork.PowerVmAllocationPolicyCustomTest;
import svila.planetlabNetwork.optimizeAllocation.MinDistanceGroupAllocationPolicyIQR;
import svila.mann.GuazzoneBFD;

/**
 *
 * @author Sergi
 */
public class AllocationPolicyFactory extends AbstractPolicyFactory {
	
	@Override
	public String getVersion() {
		return "Original";
	}
	
    public VmAllocationPolicy getVmAllocationPolicy(String vmAllocationPolicyName) {
        
		if (!stringParameter.isEmpty()) {
	            utilizationThreshold = Double.valueOf(stringParameter);
		}

        if(vmAllocationPolicyName.isEmpty()) {
            return null;
        }
        
        switch(vmAllocationPolicyName) {
            case "iqr":
                return getPowerVmAllocationPolicyMigrationInterQuartileRange();
            case "mad":
                return getPowerVmAllocationPolicyMigrationMedianAbsoluteDeviation();
            case "lr":
                return getPowerVmAllocationPolicyMigrationLocalRegression();
            case "lrr":
                return getPowerVmAllocationPolicyMigrationLocalRegressionRobust();
            case"thr":
                return getPowerVmAllocationPolicyMigrationStaticThreshold();
            /*case "dt":
                return getPowerVmAllocationPolicyDoubleThresholdMinimizationOfMigrations();*/
            case "dvfs":
                return getPowerVmAllocationPolicySimple();
            case "lago":
            	return getPowerVmAllocationLago();
            case "chowdm":
            	return getPowerVmAllocationMWFDVP();
            case "chowds":
            	return getPowerVmAllocationSWFDVP();
            case "guazzone":
            	return getPowerVmAllocationGuazzone();
            case "perc":
            	return getPowerVmAllocationPercentageUtil();
            case "abs":
            	return getPowerVmAllocationAbsoluteCapacity();
            case "calavecchia":
            	return getPowerVmAllocationDemandRisk();
            /*case "mesd":
                return getPowerVmAllocationPolicyMESD();*/
            case "mdgiqr":
                return getPowerVmAllocationPolicyMigrationMinDistanceGroupAllocationIQR();
            case "test":
            	return getPowerVmAllocationCustomTest();
            default:
                System.err.println("Unknown VM allocation policy: " + vmAllocationPolicyName);
                System.exit(-1);
                return null;
        }
        
    }
    
    /*public static VmAllocationPolicy getPowerVmAllocationPolicyMESD() {
        return new MESDVmAllocationPolicy();
    }*/
    
    public VmAllocationPolicy getPowerVmAllocationPolicyMigrationInterQuartileRange() {
        PowerVmAllocationPolicyMigrationAbstract fallbackVmSelectionPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					utilizationThreshold);
	return new PowerVmAllocationPolicyMigrationInterQuartileRange(
					hostList,
					vmSelectionPolicy,
					doubleParameter,
					fallbackVmSelectionPolicy);
    }
    
    public VmAllocationPolicy getPowerVmAllocationPolicyMigrationMinDistanceGroupAllocationIQR() {
    	PowerVmAllocationPolicyMigrationAbstract fallbackVmSelectionPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					utilizationThreshold);
	return new MinDistanceGroupAllocationPolicyIQR(
					hostList,
					vmSelectionPolicy,
					doubleParameter,
					fallbackVmSelectionPolicy);
    }
    
    public VmAllocationPolicy getPowerVmAllocationPolicyMigrationMedianAbsoluteDeviation() {
        PowerVmAllocationPolicyMigrationAbstract fallbackVmSelectionPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					utilizationThreshold);
        return new PowerVmAllocationPolicyMigrationMedianAbsoluteDeviation(
					hostList,
					vmSelectionPolicy,
					doubleParameter,
					fallbackVmSelectionPolicy);
    }
    
    public VmAllocationPolicy getPowerVmAllocationPolicyMigrationLocalRegression() {
        PowerVmAllocationPolicyMigrationAbstract fallbackVmSelectionPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					utilizationThreshold);
        return new PowerVmAllocationPolicyMigrationLocalRegression(
					hostList,
					vmSelectionPolicy,
					doubleParameter,
					schedulingInterval,
					fallbackVmSelectionPolicy);   
    }
    
    public VmAllocationPolicy getPowerVmAllocationPolicyMigrationLocalRegressionRobust() {
        PowerVmAllocationPolicyMigrationAbstract fallbackVmSelectionPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					utilizationThreshold);
        return new PowerVmAllocationPolicyMigrationLocalRegressionRobust(
					hostList,
					vmSelectionPolicy,
					doubleParameter,
					schedulingInterval,
					fallbackVmSelectionPolicy);
    }
    
    public VmAllocationPolicy getPowerVmAllocationPolicyMigrationStaticThreshold() {
        return new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					utilizationThreshold);
    }

    /*public VmAllocationPolicy getPowerVmAllocationPolicyDoubleThresholdMinimizationOfMigrations() {
        return new PowerVmAllocationPolicyDoubleThresholdMinimizationOfMigrations(
                                        hostList,
                                        BSC.LOWER_THRESHOLD,
                                        BSC.UPPER_THRESHOLD
                        );
    }*/
    
    public VmAllocationPolicy getPowerVmAllocationPolicySimple() {
        return new PowerVmAllocationPolicySimple(hostList);
    }
    
    public VmAllocationPolicy getPowerVmAllocationLago() {
        return new LagoAllocator(hostList, vmSelectionPolicy, 0.7);
    }
    
    public VmAllocationPolicy getPowerVmAllocationMWFDVP() {
        return new MWFDVP(hostList, vmSelectionPolicy, 0.7);
    }
    
    public VmAllocationPolicy getPowerVmAllocationSWFDVP() {
        return new SWFDVP(hostList, vmSelectionPolicy, 0.7);
    }
    
    public VmAllocationPolicy getPowerVmAllocationGuazzone() {
        return new GuazzoneBFD(hostList, vmSelectionPolicy, 0.7);
    }
    
    public VmAllocationPolicy getPowerVmAllocationPercentageUtil() {
        return new PercentageUtil(hostList, vmSelectionPolicy, 0.7);
    }
    
    public VmAllocationPolicy getPowerVmAllocationAbsoluteCapacity() {
        return new AbsoluteCapacity(hostList, vmSelectionPolicy, 0.7);
    }
    
    public VmAllocationPolicy getPowerVmAllocationDemandRisk() {
        return new DemandRisk(hostList, vmSelectionPolicy, 0.7);
    }
    
    public VmAllocationPolicy getPowerVmAllocationCustomTest() {
    	return new PowerVmAllocationPolicyCustomTest(hostList, vmSelectionPolicy);
    }
}
