package svila.policiesFactories;

import svila.metricsLogic.CPUCalculator.ForecastingTechnique;
import svila.metricsLogic.CPUCalculator.ForecastingTechniqueFactory;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethod;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethodFactory;
import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.StaticResources;
import svila.policiesHostOverSaturation.HostOverSaturationEverTrue;
import svila.policiesHostOverSaturation.HostOverSaturationIQR;
import svila.policiesHostOverSaturation.HostOverSaturationLR;
import svila.policiesHostOverSaturation.HostOverSaturationLRR;
import svila.policiesHostOverSaturation.HostOverSaturationMAD;
import svila.policiesHostOverSaturation.HostOverSaturationSmoothingForecasting;
import svila.policiesHostOverSaturation.HostOverSaturationTHR;
import svila.vmOptimizerFix.HostOverSaturationPolicy;

public class HostOverSaturationPolicyFactory {
	public static HostOverSaturationPolicy getHostOverSaturationPolicy(String hostOverSaturationPolicyName) {
		
        if(hostOverSaturationPolicyName.isEmpty()) {
            return null;
        }
        
        double safetyParameter = Double.parseDouble(StaticResources.getCE().getParameter());
        double utilizationThreshold = StaticResources.getCE().getUtilizationThreshold();
        
        switch(hostOverSaturationPolicyName){
        	case "iqr":
        		HostOverSaturationIQR iqr = new HostOverSaturationIQR(
        				safetyParameter,
	        				new GeneralPolicyFactory().getVmAllocationPolicy(
	        					"fallbackStaticTHR"
	        				),
	        			safetyParameter
        				);
        		return iqr;
        	case "thr":
        		HostOverSaturationTHR thr = new HostOverSaturationTHR(utilizationThreshold);
        		return thr;
        	case "mad":
        		HostOverSaturationMAD mad = new HostOverSaturationMAD(
        				safetyParameter,
        					new GeneralPolicyFactory().getVmAllocationPolicy(
        							"fallbackStaticTHR"
        					),
        				safetyParameter
        				);
        		return mad;
        	case "lr":
        		HostOverSaturationLR lr = new HostOverSaturationLR(
        				safetyParameter,
        				300.0,
        				HostOverSaturationPolicyFactory.getHostOverSaturationPolicy("thr"),
        				utilizationThreshold
        				);
        		return lr;
        	case "lrr":
        		HostOverSaturationLRR lrr = new HostOverSaturationLRR(
        				safetyParameter,
        				300.0,
        				HostOverSaturationPolicyFactory.getHostOverSaturationPolicy("thr"),
        				utilizationThreshold
        				);
        		return lrr;
        	case "et":
        		HostOverSaturationEverTrue et = new HostOverSaturationEverTrue();
        		return et;
        	case "smoothingForecasting":
        		ExperimentConfiguration ec = StaticResources.getCE();
        		SignalProcessingMethod spm = SignalProcessingMethodFactory.getSignalProcessingMethod(
        									ec.getSignalProcessing());
        		spm.setParams(ec.getSignalProcessingParams());
        		ForecastingTechnique ft = ForecastingTechniqueFactory.getForecastingTechnique(
        									ec.getForecastingTechnique());
        		ft.setParams(ec.getForecastingTechniqueParams());
				
        		HostOverSaturationSmoothingForecasting smoothingForecasting = new HostOverSaturationSmoothingForecasting(spm, ft);
        		return smoothingForecasting;
        	default:
                System.out.println("HostOverSaturationPolicy Factory: Unknown HostOverSaturationPolicy: " + hostOverSaturationPolicyName);
                System.exit(0);
                return null;
        }
    }
}
