package svila.policiesFactories;

import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyMinimumMigrationTime;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyMinimumUtilization;

import svila.planetlabNetwork.PowerVmSelectionPolicyNull;
import svila.planetlabNetwork.correlation.PearsonMathCorrelation;
import svila.planetlabNetwork.correlation.techniques.PowerVMSelectionPolicyPSP2;
import svila.planetlabNetwork.results.CorrelationsResults;
import svila.policiesVmSelection.MaximumCorrelationVMSelection;
import svila.policiesVmSelection.MinimumMigrationTimeVmSelection;
import svila.policiesVmSelection.MinimumUtilizationVmSelection;
import svila.policiesVmSelection.NoVmSelection;
import svila.policiesVmSelection.PSP2VmSelection;
import svila.policiesVmSelection.RandomSearchVmSelection;
import svila.vmOptimizerFix.VMSelectionPolicy;

public class VMSelectionPolicyFactory implements IVMSelectionPolicyFactory {
	
	@Override
	public String getVersion() {
		return "V2";
	}
	
    public VMSelectionPolicy getVmSelectionPolicy(String vmSelectionPolicyName) {
        
        if(vmSelectionPolicyName.isEmpty()) {
            return null;
        }
        
        switch(vmSelectionPolicyName){
        	case "rs":
        		return new RandomSearchVmSelection();
        	case "mu":
                return new MinimumUtilizationVmSelection();
        	case "mmt":
        		return new MinimumMigrationTimeVmSelection();
        	case "mc":
        		return new MaximumCorrelationVMSelection(new MinimumMigrationTimeVmSelection());
        	case "nm":
            	return new NoVmSelection();
        	case "psp2":
        		new CorrelationsResults().start();
            	return new PSP2VmSelection(
        				new PearsonMathCorrelation(),
        				new MinimumMigrationTimeVmSelection()
        				);
            default:
                System.out.println("Power VM Policy Factory: Unknown VM selection policy: " + vmSelectionPolicyName);
                System.exit(0);
                return null;
        }
    }
}
