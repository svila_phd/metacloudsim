package svila.policiesFactories;

import org.apache.commons.math3.util.Pair;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyMaximumCorrelation;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyMinimumMigrationTime;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyMinimumUtilization;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyRandomSelection;

import svila.planetlabNetwork.PowerVmSelectionPolicyNull;
import svila.planetlabNetwork.correlation.Correlation;
import svila.planetlabNetwork.correlation.KendallMathCorrelation;
import svila.planetlabNetwork.correlation.PearsonMathCorrelation;
import svila.planetlabNetwork.correlation.SpearmanMathCorrelation;
import svila.planetlabNetwork.correlation.techniques.PowerVMSelectionPolicyPSP;
import svila.planetlabNetwork.correlation.techniques.PowerVMSelectionPolicyPSP2;
import svila.planetlabNetwork.correlation.techniques.PowerVmSelectionPolicyCustomTest;
import svila.planetlabNetwork.correlation.techniques.PowerVmSelectionPolicyPearsonCorrelation;
import svila.planetlabNetwork.correlation.techniques.PowerVmSelectionPolicyPearsonDiff;
import svila.planetlabNetwork.correlation.techniques.PowerVmSelectionPolicyPearsonDiffAbs;
import svila.planetlabNetwork.correlation.techniques.PowerVmSelectionPolicyPearsonNear;
import svila.planetlabNetwork.results.CorrelationsResults;
import svila.policiesVmSelection.RandomSearchVmSelection;

/**
 *
 * @author Sergi
 */
public class PowerVMSelectionPolicyFactory implements IVMSelectionPolicyFactory {
	@Override
	public String getVersion() {
		return "Original";
	}
	
	public PowerVmSelectionPolicy getVmSelectionPolicy(String vmSelectionPolicyName) {
        
        if(vmSelectionPolicyName.isEmpty()) {
            return null;
        }
        
        switch(vmSelectionPolicyName){
            case "mc":
                return new PowerVmSelectionPolicyMaximumCorrelation(
					new PowerVmSelectionPolicyMinimumMigrationTime());
            case "mmt":
                return new PowerVmSelectionPolicyMinimumMigrationTime();
            case "mu":
                return new PowerVmSelectionPolicyMinimumUtilization();
            case "rs":
                return new PowerVmSelectionPolicyRandomSelection();
            case "test":
            	return new PowerVmSelectionPolicyCustomTest();
            case "pearson":
            	return new PowerVmSelectionPolicyPearsonCorrelation(new PearsonMathCorrelation(),
            			new PowerVmSelectionPolicyMinimumMigrationTime());
            case "pearsonDiff":
            	return new PowerVmSelectionPolicyPearsonDiff(new PearsonMathCorrelation(),
            			new PowerVmSelectionPolicyMinimumMigrationTime());
            case "pearsonDiffAbs":
            	return new PowerVmSelectionPolicyPearsonDiffAbs(new PearsonMathCorrelation(),
					new PowerVmSelectionPolicyMinimumMigrationTime()
					);
            case "pearsonNear":
				Pair<Double, Double> optimalPoint = new Pair<>(0.65, 0.65);
				double maxDistance = 0.2;
				return new PowerVmSelectionPolicyPearsonNear(new PearsonMathCorrelation(),
						new PowerVmSelectionPolicyMinimumMigrationTime(),
						optimalPoint, maxDistance);
            case "spearman":
            	return new PowerVmSelectionPolicyPearsonCorrelation(new SpearmanMathCorrelation(),
            			new PowerVmSelectionPolicyMinimumMigrationTime());
            case "kendall":
            	return new PowerVmSelectionPolicyPearsonCorrelation(new KendallMathCorrelation(),
            			new PowerVmSelectionPolicyMinimumMigrationTime());
            case "nm":
            	return new PowerVmSelectionPolicyNull();
            case "psp":
    			new CorrelationsResults().start();
            	return new PowerVMSelectionPolicyPSP(
        				new PearsonMathCorrelation(),
        				new PowerVmSelectionPolicyMinimumMigrationTime()
        				);
            case "psp2":
    			new CorrelationsResults().start();
            	return new PowerVMSelectionPolicyPSP2(
        				new PearsonMathCorrelation(),
        				new PowerVmSelectionPolicyMinimumMigrationTime()
        				);
            default:
                System.out.println("Power VM Policy Factory: Unknown VM selection policy: " + vmSelectionPolicyName);
                System.exit(0);
                return null;
        }
    }
}
