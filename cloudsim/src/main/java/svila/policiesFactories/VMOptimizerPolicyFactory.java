package svila.policiesFactories;

import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

import svila.planetlabNetwork.StaticResources;
import svila.policiesFactories.AllocationPolicyFactory;
import svila.policiesVmOptimizer.CloudSimVMOptimizerPolicy;
import svila.policiesVmOptimizer.DebugVMOptimizerPolicy;
import svila.policiesVmOptimizer.GAVMOptimizerPolicy;
import svila.policiesVmOptimizer.MetricsVMOptimizerPolicy;
import svila.policiesVmOptimizer.future.FutureDefaultVMOptimizerPolicy;
import svila.policiesVmOptimizer.future.NoMigrationsVMOptimizerPolicy;
import svila.policiesVmSelection.RandomSearchVmSelection;
import svila.vmOptimizerFix.VMOptimizerPolicy;

public class VMOptimizerPolicyFactory {
	
	private static List<? extends Host> hostList;
	private static PowerVmSelectionPolicy vmSelectionPolicy;
	private static String stringParameter;
    private static double doubleParameter;
    private static double utilizationThreshold;
    private static double schedulingInterval;
    
    public static void setHostList(List<? extends Host> hostList) {
    	VMOptimizerPolicyFactory.hostList = hostList;
    }
    
    public static List<? extends Host> getHostList() {
    	return hostList;
    }
    
    public static void setPowerVmSelectionPolicy(PowerVmSelectionPolicy vmSelectionPolicy) {
    	VMOptimizerPolicyFactory.vmSelectionPolicy = vmSelectionPolicy;
    }
    
    public static void setStringParameter(String stringParameter) {
    	VMOptimizerPolicyFactory.stringParameter = stringParameter;
    }
    
    public static void setDoubleParameter(double doubleParameter) {
    	VMOptimizerPolicyFactory.doubleParameter = doubleParameter;
    }
    
    public static void setDoubleParameter(String doubleParameter) {
        if (!doubleParameter.isEmpty()) {
        	VMOptimizerPolicyFactory.doubleParameter = Double.valueOf(doubleParameter);
        } else {
        	VMOptimizerPolicyFactory.doubleParameter = 0;
        }
    }
    
    
    
    public static void setUtilizationThreshold(double utilizationThreshold) {
    	VMOptimizerPolicyFactory.utilizationThreshold = utilizationThreshold;
    }
    
    public static void setSchedulingInterval(double schedulingInterval) {
    	VMOptimizerPolicyFactory.schedulingInterval = schedulingInterval;
    }
	
	public static VMOptimizerPolicy getVMOptimizerPolicy(String VMOptimizerPolicyName) {
	
        if(VMOptimizerPolicyName.isEmpty()) {
            return null;
        }
        
        switch(VMOptimizerPolicyName){
        	case "cs":
        		CloudSimVMOptimizerPolicy cloudsimVMOptimizerPolicy = new CloudSimVMOptimizerPolicy(hostList);
        		return cloudsimVMOptimizerPolicy;
        	case "metrics":
        		MetricsVMOptimizerPolicy metricsVMOptimizerPolicy = new MetricsVMOptimizerPolicy(hostList);
        		return metricsVMOptimizerPolicy;
        	case "debug":
        		DebugVMOptimizerPolicy debugVMOptimizerPolicy = new DebugVMOptimizerPolicy(hostList);
        		return debugVMOptimizerPolicy;
        	case "GA":
        		GAVMOptimizerPolicy gaVMOptimizerPolicy = new GAVMOptimizerPolicy(hostList);
        		return gaVMOptimizerPolicy;
        	case "future":
        		FutureDefaultVMOptimizerPolicy futureDefaultVMOptimizerPolicy = new  FutureDefaultVMOptimizerPolicy(hostList);
        		return futureDefaultVMOptimizerPolicy;
        	case "nm":
        		NoMigrationsVMOptimizerPolicy noMigrationsVMOptimizerPolicy = new NoMigrationsVMOptimizerPolicy(hostList);
        		return noMigrationsVMOptimizerPolicy;
        	default:
                String errorMessage = "VMOptimizerPolicy Factory: Unknown VMOptimizerPolicy: " + VMOptimizerPolicyName;
                StaticResources.throwRuntimeException(new ClassNotFoundException(errorMessage), errorMessage);
                return null;
        }
    }
}
