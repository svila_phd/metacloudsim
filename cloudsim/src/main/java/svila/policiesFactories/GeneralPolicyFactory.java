package svila.policiesFactories;

import org.cloudbus.cloudsim.VmAllocationPolicy;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.StaticResources;
import svila.vmOptimizerFix.HostOverSaturationPolicy;
import svila.vmOptimizerFix.HostUnderUtilisationPolicy;
import svila.vmOptimizerFix.MigrationStatistics;
import svila.vmOptimizerFix.VMMigrationPolicy;
import svila.vmOptimizerFix.VMOptimizerPolicy;
import svila.vmOptimizerFix.VMSelectionPolicy;

public class GeneralPolicyFactory extends AbstractPolicyFactory {
	
	@Override
	public String getVersion() {
		return "V2";
	}
	
	@Override
	public VMOptimizerPolicy getVmAllocationPolicy(String generalPolicyName) {
		
        if(generalPolicyName.isEmpty()) {
            return null;
        }
        
        switch(generalPolicyName){
        	case "fallbackStaticTHR":
        		return getVMOptimizer("cs", "rs", "thr", "def", "def");
        	case "general1":
        		return getVMOptimizer("cs", "rs", "iqr", "def", "def");
        	case "custom":
        		ExperimentConfiguration ec = StaticResources.getCE();
        		return getVMOptimizer(ec);
        	default:
                System.out.println("GeneralPolicy Factory: Unknown GeneralPolicy: " + generalPolicyName);
                System.exit(0);
                return null;
        }
    }
	
	public static VMOptimizerPolicy getVMOptimizer(ExperimentConfiguration ec) {
		return getVMOptimizer(ec.getVmOptimizerPolicy(),
				ec.getSelectionPolicy(),
				ec.getHostOverSaturationPolicy(),
				ec.getHostUnderUtilisationPolicy(),
				ec.getVmMigrationPolicy());
	}
	
	public static VMOptimizerPolicy getVMOptimizer(String VMOptimizerPolicyName,
											String selectionPolicyName,
											String hostOverSaturationPolicyName,
											String hostUnderUtilisationPolicyName,
											String VMMigrationPolicyName
			) {
		VMSelectionPolicy VMSelectionPolicy = new VMSelectionPolicyFactory().getVmSelectionPolicy(selectionPolicyName);
		HostOverSaturationPolicy hostOverSaturationPolicy = HostOverSaturationPolicyFactory.getHostOverSaturationPolicy(hostOverSaturationPolicyName);
		HostUnderUtilisationPolicy hostUnderUtilisationPolicy = HostUnderUtilisationPolicyFactory.getHostUnderUtilisationPolicy(hostUnderUtilisationPolicyName);
		VMMigrationPolicy VMMigrationPolicy = VMMigrationPolicyFactory.getVMMigrationPolicy(VMMigrationPolicyName);
		
		VMOptimizerPolicy vmOptimizerPolicy = VMOptimizerPolicyFactory.getVMOptimizerPolicy(VMOptimizerPolicyName);
		
		/*VMOptimizerPolicyFactory.setDoubleParameter(0.7);
		VMOptimizerPolicyFactory.setPowerVmSelectionPolicy(randomSearchVMSelectionPolicy);
		VMOptimizerPolicyFactory.setUtilizationThreshold(0.7);
		VMOptimizerPolicyFactory.setSchedulingInterval(300);
		VMOptimizerPolicyFactory.setHostList(hostList);*/
		
		vmOptimizerPolicy.setVmSelectionPolicy(VMSelectionPolicy);
		vmOptimizerPolicy.setVmSelPolicy(VMSelectionPolicy);
		vmOptimizerPolicy.setHostOverSaturationPolicy(hostOverSaturationPolicy);
		vmOptimizerPolicy.setHostUnderUtilisationPolicy(hostUnderUtilisationPolicy);
		vmOptimizerPolicy.setVmMigrationPolicy(VMMigrationPolicy);
		vmOptimizerPolicy.setMigrationStatistics(new MigrationStatistics());
		vmOptimizerPolicy.resolveCrossDependencies();
		
		return vmOptimizerPolicy;
	}
}
