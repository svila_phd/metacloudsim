package svila.policiesFactories;

import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

public interface IVMSelectionPolicyFactory {
	public abstract PowerVmSelectionPolicy getVmSelectionPolicy(String vmSelectionPolicyName);
	public abstract String getVersion();
}
