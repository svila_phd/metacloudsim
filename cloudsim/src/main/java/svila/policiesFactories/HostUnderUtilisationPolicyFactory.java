package svila.policiesFactories;

import svila.policiesHostUnderUtilisation.DefaultHostUnderUtilisationPolicy;
import svila.policiesHostUnderUtilisation.FUSDHostUnderUtilisationPolicy;
import svila.policiesHostUnderUtilisation.NoHostUnderUtilisationPolicy;
import svila.policiesVmMigration.DefaultVMMigrationPolicy;
import svila.vmOptimizerFix.HostUnderUtilisationPolicy;
import svila.vmOptimizerFix.VMMigrationPolicy;

public class HostUnderUtilisationPolicyFactory {
    public static HostUnderUtilisationPolicy getHostUnderUtilisationPolicy(String HostUnderUtilisationPolicyName) {
        
        if(HostUnderUtilisationPolicyName.isEmpty()) {
            return null;
        }
        
        switch(HostUnderUtilisationPolicyName){
        	case "def":
        		DefaultHostUnderUtilisationPolicy def = new DefaultHostUnderUtilisationPolicy();
        		return def;
        	case "fusd":
        		FUSDHostUnderUtilisationPolicy fusd = new FUSDHostUnderUtilisationPolicy();
        		return fusd;
        	case "none":
        		NoHostUnderUtilisationPolicy none = new NoHostUnderUtilisationPolicy();
        		return none;
            default:
                System.out.println("HostUnderUtilisationPolicy Factory: Unknown HostUnderUtilisationPolicy policy: " + HostUnderUtilisationPolicyName);
                System.exit(0);
                return null;
        }
    }
}
