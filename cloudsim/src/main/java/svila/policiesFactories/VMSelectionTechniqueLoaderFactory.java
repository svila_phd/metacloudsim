package svila.policiesFactories;

import svila.policiesFactories.future.FutureVmSelectionPolicyFactory;

public class VMSelectionTechniqueLoaderFactory {
	public static IVMSelectionPolicyFactory getVMSelectionTechniqueLoaderPolicy(String vmSelectionTechniqueLoaderName) {

		if (vmSelectionTechniqueLoaderName.isEmpty()) {
			return null;
		}

		switch (vmSelectionTechniqueLoaderName) {
		case "original":
			return new PowerVMSelectionPolicyFactory();
		case "v2":
			return new VMSelectionPolicyFactory();
			//return new PowerVMSelectionPolicyFactory();
		case "v3":
			return new FutureVmSelectionPolicyFactory();
		default:
			System.out.println("VMSelectionTechniqueLoader Factory: Unknown VMSelectionTechniqueLoader: "
					+ vmSelectionTechniqueLoaderName);
			System.exit(0);
			return null;
		}
	}
}
