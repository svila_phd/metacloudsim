package svila.policiesFactories.future;

import java.util.List;

import org.cloudbus.cloudsim.Host;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.StaticResources;
import svila.policiesFactories.AbstractPolicyFactory;
import svila.policiesFactories.HostOverSaturationPolicyFactory;
import svila.policiesFactories.HostUnderUtilisationPolicyFactory;
import svila.policiesFactories.VMMigrationPolicyFactory;
import svila.policiesFactories.VMOptimizerPolicyFactory;
import svila.policiesFactories.VMSelectionPolicyFactory;
import svila.policiesHostOverSaturation.future.FutureHostOverSaturationBase;
import svila.policiesHostOverSaturation.future.FutureHostOverSaturationTHR;
import svila.policiesHostUnderUtilisation.future.FutureHostUnderUtilisationBase;
import svila.policiesVmMigration.future.FutureDefaultVMMigrationPolicy;
import svila.policiesVmMigration.future.FutureVmMigrationPolicyBase;
import svila.policiesVmOptimizer.future.FutureDefaultVMOptimizerPolicy;
import svila.policiesVmSelection.future.FutureRandomSearchVmSelection;
import svila.policiesVmSelection.future.FutureVmSelectionBase;
import svila.vmOptimizerFix.HostOverSaturationPolicy;
import svila.vmOptimizerFix.HostUnderUtilisationPolicy;
import svila.vmOptimizerFix.MigrationStatistics;
import svila.vmOptimizerFix.VMMigrationPolicy;
import svila.vmOptimizerFix.VMOptimizerPolicy;
import svila.vmOptimizerFix.VMSelectionPolicy;
import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public class FutureGeneralPolicyFactory extends AbstractPolicyFactory {
	@Override
	public String getVersion() {
		return "V3";
	}
	
	@Override
	public FutureVMOptimizerPolicy getVmAllocationPolicy(String generalPolicyName) {
		
        if(generalPolicyName.isEmpty()) {
            return null;
        }
        
        switch(generalPolicyName){
        	case "fallbackStaticTHR":
        		return getVMOptimizer("cs", "rs", "thr", "def", "def");
        	case "general1":
        		return getVMOptimizer("cs", "rs", "iqr", "def", "def");
        	case "custom":
        		ExperimentConfiguration ec = StaticResources.getCE();
        		return getVMOptimizer(ec);
        	default:
                System.out.println("GeneralPolicy Factory: Unknown GeneralPolicy: " + generalPolicyName);
                System.exit(0);
                return null;
        }
    }
	
	public static FutureVMOptimizerPolicy getVMOptimizer(ExperimentConfiguration ec) {
		return getVMOptimizer(ec.getVmOptimizerPolicy(),
				ec.getSelectionPolicy(),
				ec.getHostOverSaturationPolicy(),
				ec.getHostUnderUtilisationPolicy(),
				ec.getVmMigrationPolicy());
	}
	
	public static FutureVMOptimizerPolicy getVMOptimizer(String VMOptimizerPolicyName,
											String selectionPolicyName,
											String hostOverSaturationPolicyName,
											String hostUnderUtilisationPolicyName,
											String VmMigrationPolicyName
			) {
		
		FutureVmSelectionBase futureVmSelectionPolicy = FutureVmSelectionPolicyFactory.getFutureVmSelectionPolicy(selectionPolicyName);
		FutureHostOverSaturationBase futureHostOverSaturationPolicy = FutureHostOverSaturationPolicyFactory.getFutureHostOverSaturationPolicy(hostOverSaturationPolicyName);
		FutureHostUnderUtilisationBase futureHostUnderUtilisationPolicy = FutureHostUnderUtilisationPolicyFactory.getHostUnderUtilisationPolicy(hostUnderUtilisationPolicyName);
		FutureVmMigrationPolicyBase futureVmMigrationPolicy = FutureVmMigrationPolicyFactory.getVmMigrationPolicy(VmMigrationPolicyName);
		
		futureVmSelectionPolicy.setMode(StaticResources.getCE().getSelectionMode());
		
		FutureVMOptimizerPolicy futureVmOptimizerPolicy = (FutureVMOptimizerPolicy) VMOptimizerPolicyFactory.getVMOptimizerPolicy(VMOptimizerPolicyName);
		
		futureVmOptimizerPolicy.setFutureVmSelectionPolicy(futureVmSelectionPolicy);
		futureVmOptimizerPolicy.setFutureHostOverSaturationPolicy(futureHostOverSaturationPolicy);
		futureVmOptimizerPolicy.setFutureHostUnderUtilisationPolicy(futureHostUnderUtilisationPolicy);
		futureVmOptimizerPolicy.setFutureVmMigrationPolicy(futureVmMigrationPolicy);
		futureVmOptimizerPolicy.setMigrationStatistics(new MigrationStatistics());
		futureVmOptimizerPolicy.resolveCrossDependencies();
		
		return futureVmOptimizerPolicy;
	}
}
