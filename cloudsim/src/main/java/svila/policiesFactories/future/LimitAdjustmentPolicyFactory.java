package svila.policiesFactories.future;

import java.util.List;

import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.StaticResources;
import svila.policiesDatacenterAdjustment.AbstractDatacenterAdjustmentPolicy;
import svila.policiesDatacenterAdjustment.LimitDatacenterAdjustmentPolicy;
import svila.policiesDatacenterAdjustment.NoDatacenterAdjustmentPolicy;
import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public class LimitAdjustmentPolicyFactory {
	public static AbstractDatacenterAdjustmentPolicy getLimitAdjustmentPolicy(FutureVMOptimizerPolicy optimizerPolicy) {
        ExperimentConfiguration ec = StaticResources.getCE();
        String limitAdjustmentPolicyName = ec.getLimitAdjustment();
		
		if(limitAdjustmentPolicyName.isEmpty()) {
            return null;
        }
        
        switch(limitAdjustmentPolicyName){
        	case "none":
        		NoDatacenterAdjustmentPolicy no = new NoDatacenterAdjustmentPolicy(optimizerPolicy);
        		return no;
        	case "limitDatacenter":
        		LimitDatacenterAdjustmentPolicy ld = new LimitDatacenterAdjustmentPolicy(optimizerPolicy, ec.getLimitAdjustmentParams());
        		return ld;
        	default:
                System.out.println("LimitAdjustmentPolicyFactory Factory: Unknown LimitAdjustmentPolicyFactory: " + limitAdjustmentPolicyName);
                System.exit(0);
                return null;
        }
	}
}
