package svila.policiesFactories.future;

import svila.policiesHostUnderUtilisation.DefaultHostUnderUtilisationPolicy;
import svila.policiesHostUnderUtilisation.FUSDHostUnderUtilisationPolicy;
import svila.policiesHostUnderUtilisation.NoHostUnderUtilisationPolicy;
import svila.policiesHostUnderUtilisation.future.FutureHostUnderUtilisationBase;
import svila.policiesHostUnderUtilisation.future.FutureNoHostUnderUtilisationPolicy;
import svila.vmOptimizerFix.HostUnderUtilisationPolicy;

public class FutureHostUnderUtilisationPolicyFactory {
    public static FutureHostUnderUtilisationBase getHostUnderUtilisationPolicy(String HostUnderUtilisationPolicyName) {
        
        if(HostUnderUtilisationPolicyName.isEmpty()) {
            return null;
        }
        
        switch(HostUnderUtilisationPolicyName){
        	case "def":
        		FutureHostUnderUtilisationBase def = new FutureHostUnderUtilisationBase();
        		return def;
        	case "none":
        		FutureNoHostUnderUtilisationPolicy none = new FutureNoHostUnderUtilisationPolicy();
        		return none;
            default:
                String errorMessage = "FutureHostUnderUtilisationPolicy Factory: Unknown FutureHostUnderUtilisationPolicy policy: " + HostUnderUtilisationPolicyName;
                System.err.println(errorMessage);
                throw new RuntimeException(new ClassNotFoundException(errorMessage));
        }
    }
}
