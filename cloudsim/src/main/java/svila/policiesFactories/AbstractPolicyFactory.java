package svila.policiesFactories;

import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;

public abstract class AbstractPolicyFactory {
	protected List<? extends Host> hostList;
	protected PowerVmSelectionPolicy vmSelectionPolicy;
	protected String stringParameter;
	protected double doubleParameter;
	protected double utilizationThreshold;
	protected double schedulingInterval;

                    
    public void setHostList(List<? extends Host> hostList) {
    	this.hostList = hostList;
    }
    
    public void setPowerVmSelectionPolicy(PowerVmSelectionPolicy vmSelectionPolicy) {
    	this.vmSelectionPolicy = vmSelectionPolicy;
    }
    
    public void setStringParameter(String stringParameter) {
    	this.stringParameter = stringParameter;
    }
    
    public void setDoubleParameter(double doubleParameter) {
    	this.doubleParameter = doubleParameter;
    }
    
    public void setDoubleParameter(String doubleParameter) {
        if (!doubleParameter.isEmpty()) {
        	this.doubleParameter = Double.valueOf(doubleParameter);
        } else {
        	this.doubleParameter = 0;
        }
    }
    
    public void setUtilizationThreshold(double utilizationThreshold) {
    	this.utilizationThreshold = utilizationThreshold;
    }
    
    public void setSchedulingInterval(double schedulingInterval) {
    	this.schedulingInterval = schedulingInterval;
    }
    
    public abstract VmAllocationPolicy getVmAllocationPolicy(String vmAllocationPolicyName);
    public abstract String getVersion();
}
