package svila.policiesFactories;

import svila.policiesVmMigration.AbsoluteCapacityVmMigrationPolicy;
import svila.policiesVmMigration.DefaultVMMigrationPolicy;
import svila.policiesVmMigration.DemandRiskVmMigrationPolicy;
import svila.policiesVmMigration.GuazzoneBFDVmMigrationPolicy;
import svila.policiesVmMigration.LagoVmMigrationPolicy;
import svila.policiesVmMigration.MDGVmMigrationPolicy;
import svila.policiesVmMigration.MWFDVPVmMigrationPolicy;
import svila.policiesVmMigration.PercentageUtilVmMigrationPolicy;
import svila.policiesVmMigration.Rolling2LastLagoVmMigrationPolicy;
import svila.policiesVmMigration.Rolling2LastVmMigrationPolicy;
import svila.policiesVmMigration.RollingLastLagoVmMigrationPolicy;
import svila.policiesVmMigration.RollingLastVmMigrationPolicy;
import svila.policiesVmMigration.SWFDVPVmMigrationPolicy;
import svila.policiesVmMigration.workingOn.MetricsVmMigrationPolicy;
import svila.policiesVmMigration.workingOn.MinPRImpVmMigrationPolicy;
import svila.policiesVmMigration.workingOn.MinPRVmMigrationPolicy;
import svila.policiesVmMigration.workingOn.StabilityVmMigrationPolicy;
import svila.vmOptimizerFix.VMMigrationPolicy;

public class VMMigrationPolicyFactory {
    public static VMMigrationPolicy getVMMigrationPolicy(String VMMigrationPolicyFactoryName) {
        
        if(VMMigrationPolicyFactoryName.isEmpty()) {
            return null;
        }
        
        switch(VMMigrationPolicyFactoryName){
        	case "def":
        		DefaultVMMigrationPolicy def = new DefaultVMMigrationPolicy();
        		return def;
        	case "mdg":
        		MDGVmMigrationPolicy mdg = new MDGVmMigrationPolicy();
        		return mdg;
        	case "ac":
        		AbsoluteCapacityVmMigrationPolicy ac = new AbsoluteCapacityVmMigrationPolicy();
        		return ac;
        	case "dr":
        		DemandRiskVmMigrationPolicy dr = new DemandRiskVmMigrationPolicy();
        		return dr;
        	case "bfd":
        		GuazzoneBFDVmMigrationPolicy bfd = new GuazzoneBFDVmMigrationPolicy();
        		return bfd;
        	case "lago":
        		LagoVmMigrationPolicy lago = new LagoVmMigrationPolicy();
        		return lago;
        	case "mwfdvp":
        		MWFDVPVmMigrationPolicy mwfdvp = new MWFDVPVmMigrationPolicy();
        		return mwfdvp;
        	case "swfdvp":
        		SWFDVPVmMigrationPolicy swfdvp = new SWFDVPVmMigrationPolicy();
        		return swfdvp;
        	case "pu":
        		PercentageUtilVmMigrationPolicy pu = new PercentageUtilVmMigrationPolicy();
        		return pu;
        	case "metrics":
        		MetricsVmMigrationPolicy metrics = new MetricsVmMigrationPolicy();
        		return metrics;
        	case "rollingLast":
        		RollingLastVmMigrationPolicy rolling = new RollingLastVmMigrationPolicy();
        		return rolling;
        	case "rolling2Last":
        		Rolling2LastVmMigrationPolicy rolling2 = new Rolling2LastVmMigrationPolicy();
        		return rolling2;
        	case "rollingLastLago":
        		RollingLastLagoVmMigrationPolicy rollingLago = new RollingLastLagoVmMigrationPolicy();
        		return rollingLago;
        	case "rolling2LastLago":
        		Rolling2LastLagoVmMigrationPolicy rolling2Lago = new Rolling2LastLagoVmMigrationPolicy();
        		return rolling2Lago;
        	case "minPR":
        		MinPRVmMigrationPolicy minPR = new MinPRVmMigrationPolicy();
        		return minPR;
        	case "minPRImp":
        		MinPRVmMigrationPolicy minPRImp = new MinPRImpVmMigrationPolicy();
        		return minPRImp;
        	case "stability":
        		StabilityVmMigrationPolicy stability = new StabilityVmMigrationPolicy();
        		return stability;
        	default:
                System.out.println("VMMigrationPolicy Factory: Unknown VMMigration policy: " + VMMigrationPolicyFactoryName);
                System.exit(0);
                return null;
        }
    }
}
