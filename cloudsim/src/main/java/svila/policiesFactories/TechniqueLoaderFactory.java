package svila.policiesFactories;

import svila.policiesFactories.future.FutureGeneralPolicyFactory;
import svila.policiesHostOverSaturation.HostOverSaturationIQR;
import svila.policiesHostOverSaturation.HostOverSaturationTHR;
import svila.vmOptimizerFix.HostOverSaturationPolicy;

public class TechniqueLoaderFactory {
	public static AbstractPolicyFactory getTechniqueLoaderPolicy(String techniqueLoaderName) {

		if (techniqueLoaderName.isEmpty()) {
			return null;
		}

		switch (techniqueLoaderName) {
		case "original":
			return new AllocationPolicyFactory();
		case "v2":
			return new GeneralPolicyFactory();
		case "v3":
			return new FutureGeneralPolicyFactory();
		default:
			System.out.println("TechniqueLoader Factory: Unknown TechniqueLoader: "
					+ techniqueLoaderName);
			System.exit(0);
			return null;
		}
	}
}
