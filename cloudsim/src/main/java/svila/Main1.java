package svila;

import java.io.IOException;

/**
 *
 * @author Sergi
 */
public class Main1 {
    
    public static void main(String[] args) throws IOException {
		boolean enableOutput = true;
		boolean outputToFile = false;
		String inputFolder = "";
		String outputFolder = "";
		String workload = "basic"; // Random workload
		String vmAllocationPolicy = "iqr"; // Inter Quartile Range (IQR) VM allocation policy
		//String vmAllocationPolicy = "dt"; // Inter Quartile Range (IQR) VM allocation policy
		String vmSelectionPolicy = "mc"; // Maximum Correlation (MC) VM selection policy
		String parameter = "1.5"; // the safety parameter of the IQR policy

                JSONOutput.init();
                
		new BasicRunner(
				enableOutput,
				outputToFile,
				inputFolder,
				outputFolder,
				workload,
				vmAllocationPolicy,
				vmSelectionPolicy,
				parameter);
                
                JSONOutput.showJSON();
                //ExperimentReader er = new ExperimentReader(JSONOutput.getJSON());
                //er.show();
	}

}
