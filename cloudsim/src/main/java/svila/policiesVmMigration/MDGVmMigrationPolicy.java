package svila.policiesVmMigration;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.SvilaPrinter;
import svila.planetlabNetwork.TopologyHostDistanceManager;
import svila.vmOptimizerFix.VMMigrationPolicy;

public class MDGVmMigrationPolicy extends VMMigrationPolicy {

	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
		//System.out.println("Searching host for VM " + vm.getId());
		double minPower = Double.MAX_VALUE;
		PowerHost allocatedHost = null;

		Integer lastHostNetId = vm.lastHostUntilDestroy.getNetworkId();
		
		TreeMap<Integer, List<PowerHost>> otherHosts = TopologyHostDistanceManager.getHostsGroupedByDistance(lastHostNetId);
		
		for(List<PowerHost> list : otherHosts.values()) {			
			for(PowerHost host : list) {
				// Original code
				if (excludedHosts.contains(host)) {
					continue;
				}
				if (host.isSuitableForVm(vm)) {
					if (this.getVmOptimizerPolicy().getUtilizationOfCpuMipsOriginal(host) != 0 && this.getVmOptimizerPolicy().isHostOverUtilizedAfterAllocationOriginal(host, vm)) {
						continue;
					}

					try {
						double powerAfterAllocation = this.getVmOptimizerPolicy().getPowerAfterAllocationOriginal(host, vm);
						if (powerAfterAllocation != -1) {
							double powerDiff = powerAfterAllocation - host.getPower();
							if (powerDiff < minPower) {
								minPower = powerDiff;
								allocatedHost = host;
							}
						}
					} catch (Exception e) {
					}
				}
				// End of original code
			}
			
			if(allocatedHost != null) { // If we allocated the host in one group, stop searching other groups
				break;
			}
		}
		
		if(allocatedHost == null) {
			return null;
		}
		
		SvilaPrinter.print("ALLOCATED HOST: " + allocatedHost.getId());
		
		return allocatedHost;
	}

	@Override
	public List<Map<String, Object>> getNewVmPlacement(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return this.getVmOptimizerPolicy().getNewVmPlacementOriginal(vmsToMigrate, excludedHosts);
	}

	@Override
	public boolean allocateHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm, host);
	}

	@Override
	public PowerHost findHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().findHostForVmOriginal(vm);
	}

	@Override
	public List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return this.getVmOptimizerPolicy().getNewVmPlacementFromUnderUtilizedHostOriginal(vmsToMigrate, excludedHosts);
	}
}
