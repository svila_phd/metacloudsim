package svila.policiesVmMigration;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.vmOptimizerFix.VMMigrationPolicy;

public class HumanVmMigrationPolicy extends VMMigrationPolicy {

	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
	    // Plot VM and hosts
		
		// Send data to file
		
		
		// Execute python code
		
		// Choose the host
		Scanner in = new Scanner(System.in);
		int selectedHostId = in.nextInt();
		in.close();
		List<PowerHost> hostList = this.getVmOptimizerPolicy().<PowerHost> getHostList();
		for(PowerHost currentHost : hostList) {
			if(currentHost.getId() == selectedHostId) {
				return currentHost;
			}
		}
		return null;
	}
	
	public List<Map<String, Object>> getNewVmPlacement(
			List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		
		//Collections.sort(vmsToMigrate, VmComparator);	// sort VMs with custom comparator, otherwise it would be the same as in the superclass
		
		for (Vm vm : vmsToMigrate) {
			PowerHost allocatedHost = findHostForVm(vm, excludedHosts);
			if (allocatedHost != null) {
				allocatedHost.vmCreate(vm);
				Log.printLine("VM #" + vm.getId() + " allocated to host #" + allocatedHost.getId());

				Map<String, Object> migrate = new HashMap<String, Object>();
				migrate.put("vm", vm);
				migrate.put("host", allocatedHost);
				migrationMap.add(migrate);
			}
		}
		return migrationMap;
	}

	@Override
	public PowerHost findHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().findHostForVmOriginal(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm, host);
	}
	
	@Override
	public List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return this.getVmOptimizerPolicy().getNewVmPlacementFromUnderUtilizedHostOriginal(vmsToMigrate, excludedHosts);
	}

}
