package svila.policiesVmMigration;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.vmOptimizerFix.VMMigrationPolicy;

public class LagoVmMigrationPolicy extends VMMigrationPolicy {
	
	int findHost;
	
	public LagoVmMigrationPolicy() {
		findHost = 0;
	}
	
	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
	    double bestEnergyEfficiency = Double.MIN_VALUE;
	    PowerHost allocatedHost = null;
	    
	    for (PowerHost host : this.getVmOptimizerPolicy().<PowerHost> getHostList()) {
			if (excludedHosts.contains(host)) {
				continue;
			}	
	    	if (host.isSuitableForVm(vm)) {
		        double utilization = this.getVmOptimizerPolicy().getMaxUtilizationAfterAllocationOriginal(host, vm);
		        if ((!vm.isBeingInstantiated()) && utilization > 0.7 || (vm.isBeingInstantiated() && utilization > 1.0)) {
		          continue;
	        }
	        
	        try {
	          double powerAfterAllocation = this.getVmOptimizerPolicy().getPowerAfterAllocationOriginal(host, vm);
	          if (powerAfterAllocation != -1) {
	            // Host can be used

	            // Calculates the power quality
	            double energyEfficiency = getHostEnergyEfficiency(host);
	            if (energyEfficiency > bestEnergyEfficiency) {
	              bestEnergyEfficiency = energyEfficiency;
	              allocatedHost = host;
	            } else if (energyEfficiency == bestEnergyEfficiency) {
	              // Tie break

	              // The lesser power consumption with the VMs instantiated at datacenter will be chosen
	              double power_vm_allocated_at_host = allocatedHost.getPower() + this.getVmOptimizerPolicy().getPowerAfterAllocationOriginal(host, vm);
	              double power_vm_allocated_at_allocatedHost = host.getPower() + this.getVmOptimizerPolicy().getPowerAfterAllocationOriginal(allocatedHost, vm);
	              if (power_vm_allocated_at_host < power_vm_allocated_at_allocatedHost) {
	                allocatedHost = host;
	              } else if (power_vm_allocated_at_host == power_vm_allocated_at_allocatedHost) {
	                // Calculates the best using MIPS / total MIPS to minimized vm
	                // migration
	                if (host.getUtilizationOfCpu() > allocatedHost.getUtilizationOfCpu()) {
	                  allocatedHost = host;
	                } else if (host.getUtilizationOfCpu() == allocatedHost.getUtilizationOfCpu()) {
	                  // The best processor will be allocated
	                  if (host.getTotalMips() > allocatedHost.getTotalMips()) {
	                    allocatedHost = host;
	                  }
	                }
	              }
	            }
	          }
	        } catch (Exception e) {
	        }
	      }
	    }

	    return allocatedHost;
	}
	
	public static double getHostEnergyEfficiency(PowerHost host) {
	    return (host.getTotalMips() / host.getMaxPower());
	  }

	@Override
	public PowerHost findHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().findHostForVmOriginal(vm);
	}

	@Override
	public List<Map<String, Object>> getNewVmPlacement(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return this.getVmOptimizerPolicy().getNewVmPlacementOriginal(vmsToMigrate, excludedHosts);
	}

	@Override
	public boolean allocateHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm, host);
	}
	
	@Override
	public List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return this.getVmOptimizerPolicy().getNewVmPlacementFromUnderUtilizedHostOriginal(vmsToMigrate, excludedHosts);
	}
}
