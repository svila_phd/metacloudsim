package svila.policiesVmMigration;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.vmOptimizerFix.VMMigrationPolicy;

public class CPUNetVmMigrationPolicy extends VMMigrationPolicy {
	// TODO all
	
	@Override
	public List<Map<String, Object>> getNewVmPlacement(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return this.getVmOptimizerPolicy().getNewVmPlacementOriginal(vmsToMigrate, excludedHosts);
		
		
		
		
	}
	
	@Override
	public PowerHost findHostForVm(Vm vm) {
		return this.findHostForVm(vm);
	}

	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
		return this.findHostForVm(vm, excludedHosts);
	}

	@Override
	public List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return this.getVmOptimizerPolicy().getNewVmPlacementFromUnderUtilizedHostOriginal(vmsToMigrate, excludedHosts);
	}

	@Override
	public boolean allocateHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm, host);
	}
}
