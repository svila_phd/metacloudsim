package svila.policiesVmMigration.workingOn;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public class MinPRImpVmMigrationPolicy extends MinPRVmMigrationPolicy {
	
	@Override
	public List<VmAllocation> MinPRPhase2() {
		List<VmAllocation> vmAllocations = new ArrayList<>();
		
		List<PowerHost> hostsList = this.getVmOptimizerPolicy().getHostList();
		// Hosts encessos
		// Hosts apagats
		List<PowerHost> hostsOn = new LinkedList<>();
		List<PowerHost> hostsOff = new LinkedList<>();
		
		for(PowerHost host : hostsList) {
			if(host.getUtilizationOfCpu() == 0.0) {
				hostsOff.add(host);
			} else {
				hostsOn.add(host);
			}
		}

		// Ordenar hosts apagats descendentment per eficiencia energetica
		Collections.sort(hostsOff, HostPowerEfficiencyComparator);
		
		// Per cada host ences:
		int onIndex, offIndex;
		for(onIndex=0; onIndex < hostsOn.size(); onIndex++) {
			PowerHost on = hostsOn.get(onIndex);
			System.out.println("On host: " + on.getNetworkId());
			// Calcular Ap
			Ap ap = new Ap(on);
			// Si Ap cau a UD:
			String domainName = getDomain(ap);
			System.out.println("Domain: " + domainName);
			if(domainName == "UD") {
				// Per cada host apagat: Domini AD
				for(offIndex = 0; offIndex < hostsOff.size(); offIndex++) {
					PowerHost off = hostsOff.get(offIndex);
					System.out.println("Off host: " + off.getNetworkId());
					// Movem les VMs al host apagat
					boolean offHostCanAllocateOnVms = checkHostCanAllocateVms(off, on.getVmList());
					System.out.println("offHostCanAllocateOnVms: " + offHostCanAllocateOnVms);
					if(offHostCanAllocateOnVms) {
						// Sabem segur que les VMs caben al host apagat, les movem
						moveVmsToHost(off, on.getVmList());
						Ap aq = new Ap(off);
						String aqDomain = getDomain(aq);
						Point2D.Double point = getPoint(aq);
						System.out.println("Point: (" + point.x + "," + point.y + ")");
						System.out.println("aqDomain: " + aqDomain);
						if(aqDomain == "AD") {
							// Les VMs ja estan al host correcte, cal actualitzar les llistes de hosts
							hostsOn.remove(onIndex);
							hostsOff.remove(offIndex);
							hostsOn.add(off);
							hostsOff.add(on);
							// Apuntem les noves migracions
							for(Vm vm : off.getVmList()) {
								vmAllocations.add(new VmAllocation(vm, off));
							}
						} else {
							// Tornem a moure les VMs del host apagat a l'ences
							moveVmsToHost(on, off.getVmList());
						}
					}
				}
				
				// Per cada host apagat: Domini BD
				for(offIndex = 0; offIndex < hostsOff.size(); offIndex++) {
					PowerHost off = hostsOff.get(offIndex);
					System.out.println("Off host: " + off.getNetworkId());
					// Movem les VMs al host apagat
					boolean offHostCanAllocateOnVms = checkHostCanAllocateVms(off, on.getVmList());
					System.out.println("offHostCanAllocateOnVms: " + offHostCanAllocateOnVms);
					if(offHostCanAllocateOnVms) {
						// Sabem segur que les VMs caben al host apagat, les movem
						moveVmsToHost(off, on.getVmList());
						Ap aq = new Ap(off);
						String aqDomain = getDomain(aq);
						Point2D.Double point = getPoint(aq);
						System.out.println("Point: (" + point.x + "," + point.y + ")");
						System.out.println("aqDomain: " + aqDomain);
						if(aqDomain == "BD") {
							// Les VMs ja estan al host correcte, cal actualitzar les llistes de hosts
							hostsOn.remove(onIndex);
							hostsOff.remove(offIndex);
							hostsOn.add(off);
							hostsOff.add(on);
							// Apuntem les noves migracions
							for(Vm vm : off.getVmList()) {
								vmAllocations.add(new VmAllocation(vm, off));
							}
						} else {
							// Tornem a moure les VMs del host apagat a l'ences
							moveVmsToHost(on, off.getVmList());
						}
					}
				}
			}
		}
		for(VmAllocation alloc : vmAllocations) {
			System.out.println(alloc.toString());
		}
		return vmAllocations;
	}
}
