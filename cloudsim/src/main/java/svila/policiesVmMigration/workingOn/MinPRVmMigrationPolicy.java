package svila.policiesVmMigration.workingOn;

import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.sandbox.PointInsidePolygon;
import svila.vmOptimizerFix.VMMigrationPolicy;
import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.IntColumn;
import tech.tablesaw.api.Table;
import tech.tablesaw.io.Destination;
import tech.tablesaw.io.csv.CsvWriter;

/*
 * From:
 * An energy-efficient algorithm for virtual
 * machine placement optimization in cloud data centers
 * 
 * It sorts the VMs decreasingly based on their resources similar to FFD;
 * afterwards each VM is assigned to a PM with the minimum residual resource
 * that could fit the VM.
 * Igual que al FFD, s’ordenen les VMs decreixentment en base a l’ús de recursos.
 * Cada VM és assignada al PM que queda més ajustat a la VM.
 * És a dir, es selecciona el PM que un cop encabida la VM,
 * té menys recursos disponibles.

 */

public class MinPRVmMigrationPolicy extends VMMigrationPolicy {
	PointInsidePolygon pip;
	
	public class MinPRParameters {
		public double alpha, beta, gamma, sigma;
		
		public MinPRParameters(double alpha, double beta, double gamma, double sigma) {
			this.alpha = alpha;
			this.beta = beta;
			this.gamma = gamma;
			this.sigma = sigma;
		}
	}
	
	MinPRParameters params;
	
	public MinPRVmMigrationPolicy() {
		this.pip = new PointInsidePolygon();
		this.params = new MinPRParameters(0.25, 0.5, 4.0, 70.0);
	}
	
	public Comparator<Vm> VmComparator = new Comparator<Vm>()
	{
		@Override
		public int compare(Vm a, Vm b) throws ClassCastException {
			Double aUtilization = a.getCurrentRequestedTotalMips();
			Double bUtilization = b.getCurrentRequestedTotalMips();
			return bUtilization.compareTo(aUtilization);
		}
	};
	
	@Override
	public PowerHost findHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().findHostForVmOriginal(vm);
	}
	
	public class WastedHostResources implements Cloneable {
		public PowerHost host;
		public double totalCPU, usedCPU, wastedCPU, wastedCPUPerc;
		public double totalRAM, usedRAM, wastedRAM, wastedRAMPerc;
		public double totalBW, usedBW, wastedBW, wastedBWPerc;
		public double result;
		
		private WastedHostResources() {
		}
		
		public WastedHostResources(PowerHost host) {
			this.host = host;
			calculateWastedCPU(host);
			calculateWastedRAM(host);
			calculateWastedBW(host);
			calculateResult();
		}
		
		private void calculateWastedCPU(PowerHost host) {
			totalCPU = host.getTotalMips();
			wastedCPU = host.getAvailableMips();
			usedCPU = totalCPU - wastedCPU;
			wastedCPUPerc = wastedCPU/totalCPU;
			if(wastedCPUPerc < 0) {
				wastedCPUPerc = 0.01;
			}
		}
		
		private void calculateWastedRAM(PowerHost host) {
			totalRAM = host.getRam();
			wastedRAM = host.getRamProvisioner().getAvailableRam();
			usedRAM = totalRAM - wastedRAM;
			wastedRAMPerc = wastedRAM/totalRAM;
			if(wastedRAMPerc < 0) {
				wastedRAMPerc = 0.01;
			}
		}
		
		private void calculateWastedBW(PowerHost host) {
			totalBW = host.getHostVmBandwith();
			usedBW = host.getNetworkBWUtilizationMb();
			wastedBW = totalBW - usedBW;
			wastedBWPerc = wastedBW/totalBW;
			if(wastedBWPerc < 0) {
				wastedBWPerc = 0.01;
			}
		}
		
		private void calculateResult() {
			result = wastedCPUPerc * wastedRAMPerc * wastedBWPerc;
		}
		
		public WastedHostResources clone() {
			WastedHostResources copy = new WastedHostResources();
			copy.host = this.host;
			copy.totalCPU = this.totalCPU;
			copy.usedCPU = this.usedCPU;
			copy.wastedCPU = this.wastedCPU;
			copy.wastedCPUPerc = this.wastedCPUPerc;
			copy.totalRAM = this.totalRAM;
			copy.usedRAM = this.usedRAM;
			copy.wastedRAM = this.wastedRAM;
			copy.wastedRAMPerc = this.wastedRAMPerc;
			copy.totalBW = this.totalBW;
			copy.usedBW = this.usedBW;
			copy.wastedBW = this.wastedBW;
			copy.wastedBWPerc = this.wastedBWPerc;
			copy.result = this.result;
			return copy;
		}
	}
	
	public class WastedHostResourcesVm {
		public WastedHostResources wastedHostResourcesBefore;
		public Vm vm;
		public double usedCPU, vmUsedCPU, wastedCPU;
		public double usedRAM, vmUsedRAM, wastedRAM;
		public double usedBW, vmUsedBW, wastedBW;
		public double wastedCPUPerc;
		public double wastedRAMPerc;
		public double wastedBWPerc;
		public double result;
		
		public WastedHostResourcesVm(WastedHostResources wastedHostResources, Vm vm) {
			this.wastedHostResourcesBefore = wastedHostResources;
			this.vm = vm;
			calculateWastedCPU();
			calculateWastedRAM();
			calculateWastedBW();
			calculateResult();
		}
		
		private void calculateWastedCPU() {
			vmUsedCPU = vm.getVmCalculator).getCalculatedCurrentMIPS();
			usedCPU = wastedHostResourcesBefore.usedCPU + vmUsedCPU;			
			wastedCPU = wastedHostResourcesBefore.totalCPU - usedCPU;
			wastedCPUPerc = wastedCPU/wastedHostResourcesBefore.totalCPU;
			if(wastedCPUPerc < 0) {
				wastedCPUPerc = 0.01;
			}
		}
		
		private void calculateWastedRAM() {
			vmUsedRAM = vm.getRam();
			usedRAM = wastedHostResourcesBefore.usedRAM + vmUsedRAM;
			wastedRAM = wastedHostResourcesBefore.totalRAM - usedRAM;
			wastedRAMPerc = wastedRAM/wastedHostResourcesBefore.totalRAM;
			if(wastedRAMPerc < 0) {
				wastedRAMPerc = 0.01;
			}
		}
		
		private void calculateWastedBW() {
			usedBW = wastedHostResourcesBefore.host.getNetworkBWUtilizationMbWithSimulatedVM(this.vm);
			wastedBW = wastedHostResourcesBefore.totalBW - usedBW;
			wastedBWPerc = wastedBW/wastedHostResourcesBefore.totalBW;
			if(wastedBWPerc < 0) {
				wastedBWPerc = 0.01;
			}
		}
		
		private void calculateResult() {
			result = wastedCPUPerc * wastedRAMPerc * wastedBWPerc;
		}
	}
	
	
	// Before - after
	// Example: before result = 0.5
	//			after result = 0.3
	// 0.5 - 0.3 = 0.2 The highest positive diff is the best usage
	public class WastedHostResourcesVmVariation {
		public WastedHostResources wastedHostResourcesBefore;
		public WastedHostResourcesVm wastedHostResourcesVmAfter;
		public Vm vm;
		public double usedCPUDiff, wastedCPUDiff;
		public double usedRAMDiff, wastedRAMDiff;
		public double usedBWDiff, wastedBWDiff;
		public double wastedCPUPercDiff;
		public double wastedRAMPercDiff;
		public double wastedBWPercDiff;
		public double resultDiff;
		
		public WastedHostResourcesVmVariation(WastedHostResourcesVm wastedHostResourcesVmAfter) {
			this.wastedHostResourcesBefore = wastedHostResourcesVmAfter.wastedHostResourcesBefore;
			this.wastedHostResourcesVmAfter = wastedHostResourcesVmAfter;
			this.vm = this.wastedHostResourcesVmAfter.vm;
			this.usedBWDiff = this.wastedHostResourcesBefore.usedBW - this.wastedHostResourcesVmAfter.usedBW;
			this.usedRAMDiff = this.wastedHostResourcesBefore.usedRAM - this.wastedHostResourcesVmAfter.usedRAM;
			this.usedBWDiff = this.wastedHostResourcesBefore.usedBW - this.wastedHostResourcesVmAfter.usedBW;

			this.wastedCPUDiff = this.wastedHostResourcesBefore.wastedCPU - this.wastedHostResourcesVmAfter.wastedCPU;
			this.wastedRAMDiff = this.wastedHostResourcesBefore.wastedRAM - this.wastedHostResourcesVmAfter.wastedRAM;
			this.wastedBWDiff = this.wastedHostResourcesBefore.wastedBW - this.wastedHostResourcesVmAfter.wastedBW;
		
			this.wastedCPUPercDiff = this.wastedHostResourcesBefore.wastedCPUPerc - this.wastedHostResourcesVmAfter.wastedCPUPerc;
			this.wastedRAMPercDiff = this.wastedHostResourcesBefore.wastedRAMPerc - this.wastedHostResourcesVmAfter.wastedRAMPerc;
			this.wastedBWPercDiff = this.wastedHostResourcesBefore.wastedBWPerc - this.wastedHostResourcesVmAfter.wastedBWPerc;

			this.resultDiff = this.wastedHostResourcesBefore.result - this.wastedHostResourcesVmAfter.result;
		}
	}
	
	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
	    PowerHost allocatedHost = null;
	    Map<Integer, WastedHostResources> wastedResourcesMap = new HashMap<>();
	    
	    for (PowerHost host : this.getVmOptimizerPolicy().<PowerHost> getHostList()) {
			if (excludedHosts.contains(host)) {
				continue;
			}	
	    	if (host.isSuitableForVm(vm)) {
		        double utilization = this.getVmOptimizerPolicy().getMaxUtilizationAfterAllocationOriginal(host, vm);
		        if ((!vm.isBeingInstantiated()) && utilization > 0.7 || (vm.isBeingInstantiated() && utilization > 1.0)) {
		          continue;
	        }
	        
	        try {
	          double powerAfterAllocation = this.getVmOptimizerPolicy().getPowerAfterAllocationOriginal(host, vm);
	          if (powerAfterAllocation != -1) {
	            // Host can be used
	        	  WastedHostResources currentWasted = new WastedHostResources(host);
	        	  wastedResourcesMap.put(host.getNetworkId(), currentWasted);
	          }
	        } catch (Exception e) {
	        }
	      }
	    }

	    return allocatedHost;
	}
	
	private Map<Host, WastedHostResources> getHostWastageMap(List<PowerHost> hosts) {
		HashMap<Host, WastedHostResources> hostWastageMap = new HashMap<>();
		for(PowerHost host : hosts) {
			hostWastageMap.put(host, new WastedHostResources(host));
		}
		return hostWastageMap;
	}
	
	private Map<String, WastedHostResourcesVm> getWastedVmHostResourcesMap(
					Map<Host, WastedHostResources> hostWastageMap, List<? extends Vm> vms) {
		HashMap<String, WastedHostResourcesVm> vmHostWastageMap = new HashMap<>();
		for(WastedHostResources hostWastage : hostWastageMap.values()) {
			for(Vm vm : vms) {
				vmHostWastageMap.put(vm.getNetworkId() + "-" + hostWastage.host.getNetworkId(),
						new WastedHostResourcesVm(hostWastage, vm));
			}
		}
		return vmHostWastageMap;
	}
	
	private Map<String, WastedHostResourcesVmVariation> getWastedVmHostVmResourcesVariationMap(
			Map<String, WastedHostResourcesVm> wastedVmHostVmResourcesMap) {
		Map<String, WastedHostResourcesVmVariation> wastedVmHostVmResourcesVariationMap = new HashMap<>();

		for(Entry<String, WastedHostResourcesVm> entry : wastedVmHostVmResourcesMap.entrySet()) {
			wastedVmHostVmResourcesVariationMap.put(entry.getKey(),
					new WastedHostResourcesVmVariation(entry.getValue()));
		}
		
		return wastedVmHostVmResourcesVariationMap;
	}
	
	class VmAllocation {
		public Vm vm;
		public PowerHost host;
		
		public VmAllocation(Vm vm, PowerHost host) {
			this.vm = vm;
			this.host = host;
		}
		
		@Override
		public String toString() {
			return "Vm: " + vm.getNetworkId() + " host: " + host.getNetworkId();
		}
	}
	
	private void plotBefore(Map<Host, WastedHostResources> wastedHostResourcesMap, UUID uuid) {
		// hostNetId | wastedCPUPerc | wastedRAMPerc | wastedBWPerc
		int tableSize = wastedHostResourcesMap.size();
		IntColumn hostNetIdColumn = IntColumn.create("hostNetId", tableSize);
		DoubleColumn wastedCPUPercColumn = DoubleColumn.create("wastedCPUPerc", tableSize);
		DoubleColumn wastedRAMPercColumn = DoubleColumn.create("wastedRAMPerc", tableSize);
		DoubleColumn wastedBWPercColumn = DoubleColumn.create("wastedBWPerc", tableSize);
		DoubleColumn resultColumn = DoubleColumn.create("result", tableSize);

		Table wastageTable = Table.create("WastageTable", hostNetIdColumn,
				wastedCPUPercColumn, wastedRAMPercColumn, wastedBWPercColumn, resultColumn);
		
		int i = 0;
		for(WastedHostResources whr : wastedHostResourcesMap.values()) {
			hostNetIdColumn.set(i, whr.host.getNetworkId());
			wastedCPUPercColumn.set(i, whr.wastedCPUPerc);
			wastedRAMPercColumn.set(i, whr.wastedRAMPerc);
			wastedBWPercColumn.set(i, whr.wastedBWPerc);
			resultColumn.set(i, whr.result);
			i++;
		}
		CsvWriter writer = new CsvWriter();
		Destination destination;
		try {
			destination = new Destination(
					new File("C:\\Users\\Sergi\\Documents\\networkExperiments\\debug\\wastageTableBefore_" + 
							 uuid.toString().substring(0, 4) + ".csv")
			);
			writer.write(wastageTable, destination);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void plotAfter(Map<String, WastedHostResourcesVm> wastedVmHostVmResourcesMap, UUID uuid) {
		int tableSize = wastedVmHostVmResourcesMap.size();
		int i = 0;
		
		IntColumn hostNetIdColumn = IntColumn.create("hostNetId", tableSize);
		IntColumn vmNetIdColumn = IntColumn.create("vmNetId", tableSize);
		DoubleColumn wastedCPUPercColumn = DoubleColumn.create("wastedCPUPerc", tableSize);
		DoubleColumn wastedRAMPercColumn = DoubleColumn.create("wastedRAMPerc", tableSize);
		DoubleColumn wastedBWPercColumn = DoubleColumn.create("wastedBWPerc", tableSize);
		DoubleColumn resultColumn = DoubleColumn.create("result", tableSize);
		
		Table wastageTable = Table.create("WastageTable", hostNetIdColumn, vmNetIdColumn,
				wastedCPUPercColumn, wastedRAMPercColumn, wastedBWPercColumn, resultColumn);
		
		for(WastedHostResourcesVm whrvm : wastedVmHostVmResourcesMap.values()) {
			hostNetIdColumn.set(i, whrvm.wastedHostResourcesBefore.host.getNetworkId());
			vmNetIdColumn.set(i, whrvm.vm.getNetworkId());
			wastedCPUPercColumn.set(i, whrvm.wastedCPUPerc);
			wastedRAMPercColumn.set(i, whrvm.wastedRAMPerc);
			wastedBWPercColumn.set(i, whrvm.wastedBWPerc);
			resultColumn.set(i, whrvm.result);
			i++;
		}
		
		CsvWriter writer = new CsvWriter();
		Destination destination;
		try {
			destination = new Destination(
					new File("C:\\Users\\Sergi\\Documents\\networkExperiments\\debug\\wastageTableAfter_" + 
							 uuid.toString().substring(0, 4) + ".csv")
			);
			writer.write(wastageTable, destination);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void plotDiff(Map<String, WastedHostResourcesVmVariation> wastedVmHostVmResourcesVariationMap,
			UUID uuid) {
		int tableSize = wastedVmHostVmResourcesVariationMap.size();
		int i = 0;
		
		IntColumn hostNetIdColumn = IntColumn.create("hostNetId", tableSize);
		IntColumn vmNetIdColumn = IntColumn.create("vmNetId", tableSize);
		DoubleColumn wastedCPUPercColumn = DoubleColumn.create("wastedCPUPercDiff", tableSize);
		DoubleColumn wastedRAMPercColumn = DoubleColumn.create("wastedRAMPercDiff", tableSize);
		DoubleColumn wastedBWPercColumn = DoubleColumn.create("wastedBWPercDiff", tableSize);
		DoubleColumn resultColumn = DoubleColumn.create("resultDiff", tableSize);
		
		Table wastageTable = Table.create("WastageTable", hostNetIdColumn, vmNetIdColumn,
				wastedCPUPercColumn, wastedRAMPercColumn, wastedBWPercColumn, resultColumn);
		
		for(WastedHostResourcesVmVariation whrvm : wastedVmHostVmResourcesVariationMap.values()) {
			hostNetIdColumn.set(i, whrvm.wastedHostResourcesBefore.host.getNetworkId());
			vmNetIdColumn.set(i, whrvm.vm.getNetworkId());
			wastedCPUPercColumn.set(i, whrvm.wastedCPUDiff);
			wastedRAMPercColumn.set(i, whrvm.wastedRAMDiff);
			wastedBWPercColumn.set(i, whrvm.wastedBWDiff);
			resultColumn.set(i, whrvm.resultDiff);
			i++;
		}
		
		CsvWriter writer = new CsvWriter();
		Destination destination;
		try {
			destination = new Destination(
					new File("C:\\Users\\Sergi\\Documents\\networkExperiments\\debug\\wastageTableDiff_" + 
							 uuid.toString().substring(0, 4) + ".csv")
			);
			writer.write(wastageTable, destination);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void plots(Map<Host, WastedHostResources> wastedHostResourcesMap,
			Map<String, WastedHostResourcesVm> wastedVmHostVmResourcesMap,
			Map<String, WastedHostResourcesVmVariation> wastedVmHostVmResourcesVariationMap) {
		UUID uuid = UUID.randomUUID();
		plotBefore(wastedHostResourcesMap, uuid);
		plotAfter(wastedVmHostVmResourcesMap, uuid);
		plotDiff(wastedVmHostVmResourcesVariationMap, uuid);
	}
	
	private boolean lagoCheckAllocation(Vm vm, PowerHost host) {
		double upperUtilization = 0.7;
		if(host == null || vm == null) {
			System.out.println("AAA");
		}
		if (host.isSuitableForVm(vm)) {
	        double utilization = this.getVmOptimizerPolicy().getMaxUtilizationAfterAllocationOriginal(host, vm);
	        if ((!vm.isBeingInstantiated()) && utilization > upperUtilization || (vm.isBeingInstantiated() && utilization > 1.0)) {
	          return false;
	        }
		}
		return true;
	}
	
	private VmAllocation getBestVmAllocation(Map<Host, WastedHostResources> wastedHostResourcesMap,
			Map<String, WastedHostResourcesVm> wastedVmHostVmResourcesMap,
			Map<String, WastedHostResourcesVmVariation> wastedVmHostVmResourcesVariationMap) {
		// Get plots
		int slotNum = this.getVmOptimizerPolicy().getPowerDatacenter().slotNum;
		if(slotNum == 60) {
			plots(wastedHostResourcesMap, wastedVmHostVmResourcesMap, wastedVmHostVmResourcesVariationMap);
		}

		// Find best VM allocation
		Vm bestVm = null;
		PowerHost bestHost = null;
		while(true) {
			double highestResult = -1;
			bestVm = null;
			bestHost = null;
			WastedHostResourcesVm currentWhrvm;
			String bestAllocCode = null;
			//System.out.println("Num data: " + wastedVmHostVmResourcesMap.size());
			for(Entry<String, WastedHostResourcesVm> entry : wastedVmHostVmResourcesMap.entrySet()) {
				currentWhrvm = entry.getValue();
				if(currentWhrvm.result > highestResult) {
					highestResult = currentWhrvm.result;
					bestVm = currentWhrvm.vm;
					bestHost = currentWhrvm.wastedHostResourcesBefore.host;
					bestAllocCode = entry.getKey();
				}
			}
			if(highestResult == -1) {
				return null;
			}
			
			// Including VM can be allocated in host
			if(lagoCheckAllocation(bestVm, bestHost)) {
				return new VmAllocation(bestVm, bestHost);
			} else {
				wastedVmHostVmResourcesMap.remove(bestAllocCode);
				if(wastedVmHostVmResourcesMap.size() == 0) {
					return null;
				}
			}
		}		
	}
	
	public Comparator<PowerHost> HostPowerEfficiencyComparator = new Comparator<PowerHost>()
	{
		@Override
		public int compare(PowerHost a, PowerHost b) throws ClassCastException {
			Double aEfficiency = a.getHostEnergyEfficiency() ;
			Double bEfficiency = b.getHostEnergyEfficiency();
			int efficiency = bEfficiency.compareTo(aEfficiency);	//descending
			if(efficiency == 0) {
				Double aMips = a.getAvailableMips();
				Double bMips = b.getAvailableMips();
				int capacity = bMips.compareTo(aMips); // descending
				return capacity;
			}
			
			return efficiency;
		}
	};
	
	class Ap {
		protected PowerHost host;
		public Map<String, Double> normalizedResources;
		
		protected Ap() {
			
		}
		
		public Ap(PowerHost host) {
			this.host = host;
			normalizedResources = new HashMap<>();
			calculateNormalizedResources();
		}
		
		// Override this function to change the resources
		protected void calculateNormalizedResources() {
			normalizedResources.put("CPU", getNormalizedCPU());
			normalizedResources.put("RAM", getNormalizedRAM());
			normalizedResources.put("BW", getNormalizedBW());
		}
		
		protected double getNormalizedCPU() {
			double totalCPU = host.getTotalMips();
			double wastedCPU = host.getAvailableMips();
			double usedCPU = totalCPU - wastedCPU;
			return usedCPU / totalCPU;
		}
		
		protected double getNormalizedRAM() {
			double totalRAM = host.getRam();
			double wastedRAM = host.getRamProvisioner().getAvailableRam();
			double usedRAM = totalRAM - wastedRAM;
			return usedRAM / totalRAM;
		}
		
		protected double getNormalizedBW() {
			double totalBW = host.getHostVmBandwith();
			double wastedBW = host.getNetworkBWUtilizationMbIntraAndInter().externalBW;
			double usedBW = totalBW - wastedBW;
			return usedBW / totalBW;
		}
	}
	
	private class Bpv extends Ap {
		Vm vm;
		
		public Bpv(PowerHost host, Vm vm) {
			this.host = host;
			this.vm = vm;
			this.normalizedResources = new HashMap<>();
			calculateNormalizedResources();
		}
		
		protected double getNormalizedCPU() {
			double totalCPU = host.getTotalMips();
			double wastedCPU = host.getAvailableMips();
			double usedCPU = totalCPU - wastedCPU;
			double vmUsedCPU = vm.ggetVmCalculator.getCalculatedCurrentMIPS();
			double usedCPUAfter = usedCPU + vmUsedCPU;
			if(usedCPUAfter > totalCPU) {
				return 0.99;
			}
			return usedCPUAfter / totalCPU;
		}
		
		protected double getNormalizedRAM() {
			double totalRAM = host.getRam();
			double wastedRAM = host.getRamProvisioner().getAvailableRam();
			double usedRAM = totalRAM - wastedRAM;
			double vmUsedRAM = vm.getRam();
			double usedRAMAfter = usedRAM + vmUsedRAM;
			if(usedRAMAfter > totalRAM) {
				return 0.99;
			}
			return usedRAMAfter / totalRAM;
		}
		
		protected double getNormalizedBW() {
			double totalBW = host.getHostVmBandwith();
			double usedBWAfter = host.getNetworkBWUtilizationMbWithSimulatedVM(this.vm);
			if(usedBWAfter > totalBW) {
				return 0.99;
			}
			return usedBWAfter / totalBW;
		}
	}

	
	private double getF1(Bpv bpv, double score) {
		if(getDomain(bpv) == "AD") {
			return this.params.alpha * score;
		}
		return 0.0;
	}
	
	private double getF2(Bpv bpv, double score) {
		if(getDomain(bpv) == "UD") {
			return this.params.beta * score;
		}
		return 0.0;
	}
	
	private boolean isGreaterDimension(Bpv bpv) {
		String[] resourceNames = {"CPU", "RAM"};
		for(String resourceName : resourceNames) {
			if(bpv.normalizedResources.get(resourceName) > this.params.sigma) {
				return true;
			}
		}
		
		return false;
	}
	
	private double getF3(Bpv bpv, double score) {
		if(isGreaterDimension(bpv)) {
			return this.params.gamma * score;
		}
		return 0.0;
	}
	
	// MinPR domain, two dimensions
	protected String getDomain(Ap ap) {
		Point2D.Double CPU_RAM_point = new Point2D.Double(
				ap.normalizedResources.get("CPU"), ap.normalizedResources.get("RAM"));
		return this.pip.getDomain(CPU_RAM_point).name;
	}
	
	protected Point2D.Double getPoint(Ap ap) {
		return new Point2D.Double(ap.normalizedResources.get("CPU"), ap.normalizedResources.get("RAM"));
	}
	
	private double getBaseScore(Ap ap) {
		String[] resourceNames = {"CPU", "RAM"};
		double baseScore = 1.0;
		for(String resourceName : resourceNames) {
			baseScore *= ap.normalizedResources.get(resourceName);
		}
		return baseScore;
	}
	
	public double getF(PowerHost host, Vm vm) {
		
		//Ap ap = new Ap(host);
		Bpv bpv = new Bpv(host, vm);
		double baseScore = getBaseScore(bpv); // Multiplication sequence
		double f1 = getF1(bpv, baseScore);
		double f2 = getF2(bpv, baseScore);
		double f3 = getF3(bpv, baseScore);
		System.out.println("baseScore: " + baseScore + "; f1: " + f1 + "; f2: " + f2 + "; f3: " + f3);
		double finalScore = baseScore + f1 - (f2+f3);
		System.out.println("Final score: " + finalScore);
		return finalScore;
	}
	
	public List<VmAllocation> MinPRPhase1(List<? extends Vm> vms, List<PowerHost> availableHosts) {
		List<VmAllocation> vmAllocations = new ArrayList<>();
		List<? extends Vm> pendingVms = new ArrayList<>(vms);
		List<PowerHost> candidateHosts = new ArrayList<>(availableHosts);
		
		Collections.sort(candidateHosts, HostPowerEfficiencyComparator);
		/*for(PowerHost host : candidateHosts) {
			System.out.println("hostNetId: " + host.getNetworkId() + 
					" pow eff: " + host.getHostEnergyEfficiency() +
					" av mips: " + host.getAvailableMips());
		}*/
		int currentHostIndex = 0;
		Vm currentVm = null;
		
		while(pendingVms.size() > 0 && currentHostIndex < candidateHosts.size()) {
			double max = 0;
			System.out.println("Host: " + candidateHosts.get(currentHostIndex).getNetworkId());
			for(Vm vm : pendingVms) {
				System.out.println("Vm: " + vm.getNetworkId());
				if(candidateHosts.get(currentHostIndex).isSuitableForVm(vm)) {
					System.out.println("Suitable");
					double f = this.getF(candidateHosts.get(currentHostIndex), vm);
					System.out.println("f:" + f);
					if(max < f) {
						max = f;
						currentVm = vm;
						System.out.println("New best vm");
					}
				} else {
					System.out.println("Not suitable");
				}
			}
			
			if(max != 0) {
				System.out.println("Vm " + currentVm.getNetworkId() + " moved to host " + candidateHosts.get(currentHostIndex).getNetworkId());
				vmAllocations.add(new VmAllocation(currentVm, candidateHosts.get(currentHostIndex)));
				// Assignar VM al host
				candidateHosts.get(currentHostIndex).vmCreate(currentVm);
				// Treure currentVM de la llista
				pendingVms.remove(currentVm);
			} else {
				currentHostIndex++;
			}
		}
		
		for(VmAllocation alloc : vmAllocations) {
			System.out.println(alloc.toString());
		}
		return vmAllocations;
	}
	
	public boolean checkHostCanAllocateVms(PowerHost host, List<Vm> vms) {
		// Save original hosts
		Map<Vm, Host> vmHostMap = new HashMap<>();
		for(Vm vm : vms) {
			vmHostMap.put(vm, vm.getHost());
		}
		
		List<Vm> usedVms = new ArrayList<>(vms);
		
		// Move VMs to new host
		boolean canAllocate = true;
		for(Vm vm : usedVms) {
			//System.out.println("Available MIPS: " + host.getAvailableMips());

			if(host.isSuitableForVm(vm)) {
				vm.getHost().vmDestroy(vm);
				host.vmCreate(vm);
			} else {
				canAllocate = false;
				break;
			}
			//System.out.println("Available MIPS with new VM: " + host.getAvailableMips());
		}
		
		// Move VMs to original host
		for(Entry<Vm, Host> entry : vmHostMap.entrySet()) {
			Host originHost = entry.getValue();
			Vm vm = entry.getKey();
			if(vm.getHost() != null) {
				if(vm.getHost() != originHost) {
					vm.getHost().vmDestroy(vm);
					originHost.vmCreate(vm);
				} else if(originHost == null) {
					vm.getHost().vmDestroy(vm);
				} else {
					// Vm is in original host
				}
			}
		}
		
		return canAllocate;
	}
	
	public boolean moveVmsToHost(PowerHost host, List<Vm> vms) {
		List<Vm> usedVms = new ArrayList<>(vms);
		for(Vm vm : usedVms) {
			if(vm.getHost() != null) {
				vm.getHost().vmDestroy(vm);
				if(!host.vmCreate(vm)) {
					return false;
				}
			}
		}
		return true;
	}
	
	public List<VmAllocation> MinPRPhase2() {
		List<VmAllocation> vmAllocations = new ArrayList<>();
		
		List<PowerHost> hostsList = this.getVmOptimizerPolicy().getHostList();
		// Hosts encessos
		// Hosts apagats
		List<PowerHost> hostsOn = new LinkedList<>();
		List<PowerHost> hostsOff = new LinkedList<>();
		
		for(PowerHost host : hostsList) {
			if(host.getUtilizationOfCpu() == 0.0) {
				hostsOff.add(host);
			} else {
				hostsOn.add(host);
			}
		}

		// Ordenar hosts apagats descendentment per eficiencia energetica
		Collections.sort(hostsOff, HostPowerEfficiencyComparator);
		
		// Per cada host ences:
		int onIndex, offIndex;
		for(onIndex=0; onIndex < hostsOn.size(); onIndex++) {
			PowerHost on = hostsOn.get(onIndex);
			System.out.println("On host: " + on.getNetworkId());
			// Calcular Ap
			Ap ap = new Ap(on);
			// Si Ap cau a UD:
			String domainName = getDomain(ap);
			System.out.println("Domain: " + domainName);
			if(domainName == "UD") {
				// Per cada host apagat:
				for(offIndex = 0; offIndex < hostsOff.size(); offIndex++) {
					PowerHost off = hostsOff.get(offIndex);
					System.out.println("Off host: " + off.getNetworkId());
					// Movem les VMs al host apagat
					boolean offHostCanAllocateOnVms = checkHostCanAllocateVms(off, on.getVmList());
					System.out.println("offHostCanAllocateOnVms: " + offHostCanAllocateOnVms);
					if(offHostCanAllocateOnVms) {
						// Sabem segur que les VMs caben al host apagat, les movem
						moveVmsToHost(off, on.getVmList());
						Ap aq = new Ap(off);
						String aqDomain = getDomain(aq);
						Point2D.Double point = getPoint(aq);
						System.out.println("Point: (" + point.x + "," + point.y + ")");
						System.out.println("aqDomain: " + aqDomain);
						if(aqDomain == "AD" || aqDomain == "BD") {
							// Les VMs ja estan al host correcte, cal actualitzar les llistes de hosts
							hostsOn.remove(onIndex);
							hostsOff.remove(offIndex);
							hostsOn.add(off);
							hostsOff.add(on);
							// Apuntem les noves migracions
							for(Vm vm : off.getVmList()) {
								vmAllocations.add(new VmAllocation(vm, off));
							}
						} else {
							// Tornem a moure les VMs del host apagat a l'ences
							moveVmsToHost(on, off.getVmList());
						}
					}
				}
			}
		}
		for(VmAllocation alloc : vmAllocations) {
			System.out.println(alloc.toString());
		}
		return vmAllocations;
	}
	
	public void debugTest1(List<? extends Vm> vms, List<PowerHost> availableHosts) {
		List<PowerHost> hostsList = this.getVmOptimizerPolicy().getHostList();
		// Hosts encessos
		// Hosts apagats
		List<PowerHost> hostsOn = new ArrayList<>();
		List<PowerHost> hostsOff = new ArrayList<>();
		
		for(PowerHost host : hostsList) {
			if(host.getUtilizationOfCpu() == 0.0) {
				hostsOff.add(host);
			} else {
				hostsOn.add(host);
			}
		}
		
		PowerHost hostOn = hostsOn.get(0);
		PowerHost hostOff = hostsOff.get(0);
		
		System.out.println("Host on MIPS: " + hostOn.getTotalMips());
		System.out.println("Host on MIPS requested: " + hostOn.getUtilizationMips());
		System.out.println("Host off MIPS: " + hostOff.getTotalMips());
		
		System.out.println("Host on has " + hostOn.getVmList().size() + " VMs.");
		
		double vmMips = 0.0;
		for(Vm vm : hostOn.getVmList()) {
			vmMips += vm.getCurrentRequestedTotalMips();
		}
		System.out.println("vmMips: " + vmMips);
		
		System.out.println("Now we move the vms to a off host. First, we check they can be placed");
		if(checkHostCanAllocateVms(hostOff, hostOn.getVmList())) {
			System.out.println("Host off can allocate. VMs are currently in host on");
			System.out.println("Host on vms: " + hostOn.getVmList().size());
			System.out.println("Host off vms: " + hostOff.getVmList().size());
		}
		
		moveVmsToHost(hostOff, hostOn.getVmList());
		System.out.println("VMs are currently in host off");
		System.out.println("Host on vms: " + hostOn.getVmList().size());
		System.out.println("Host off vms: " + hostOff.getVmList().size());
		

	}
	
	public Map<Vm, Host> decideVMAllocations(List<? extends Vm> vms, List<PowerHost> availableHosts) {		
		//Map<Vm, Host> migrationMap = new HashMap<>();
		
		/*List<PowerHost> hostsList = this.getVmOptimizerPolicy().getHostList();
		List<Vm> vmsList = this.getVmOptimizerPolicy().getPowerDatacenter().getVmList();
		CloudScenario cs = new CloudScenario(hostsList, vmsList);
		MoveAllVmsFromHostToHostAction mvAll = new MoveAllVmsFromHostToHostAction(cs.getHostByNetId(10), cs.getHostByNetId(14));
		cs.addPendingAction(mvAll);
		List<Vm> vmsToMove = new ArrayList<>();
		vmsToMove.add(cs.getVmByNetId(19));
		MoveVmsToHostAction mvVm = new MoveVmsToHostAction(cs.getHostByNetId(14), vmsToMove);
		cs.addPendingAction(mvVm);
		Map<Vm, PowerHost> migrations = cs.getMigrationMap();
		cs.applyPendingActions();
		Map<Vm, PowerHost> migrations2 = cs.getMigrationMap();
		cs.rollbackAll();
		Map<Vm, PowerHost> migrations3 = cs.getMigrationMap();*/

		
		//debugTest1(vms, availableHosts);
		return decideVMAllocationsMinPR(vms, availableHosts);
		
		//return migrationMap;
	}
	
	public Map<Vm, Host> decideVMAllocationsMinPR(List<? extends Vm> vms, List<PowerHost> availableHosts) {
		System.out.println("Vms to migrate: " + vms.size());
		List<VmAllocation> vmAllocations = MinPRPhase1(vms, availableHosts);
		System.out.println("Num vm allocations: " + vmAllocations.size());
		List<VmAllocation> vmReallocations = MinPRPhase2();
		System.out.println("Num vm reallocations: " + vmReallocations.size());
		vmAllocations.addAll(vmReallocations);
		
		// Si una mateixa VM s'ha anat movent a diversos hosts,
		// al migrationMap quedarà l'últim moviment
		Map<Vm, Host> migrationMap = new HashMap<>();
		for(VmAllocation vmAllocation : vmAllocations) {
			migrationMap.put(vmAllocation.vm, vmAllocation.host);
		}
		
		return migrationMap;
	}

	
	public Map<Vm, Host> decideVMAllocations2(List<? extends Vm> vms, List<PowerHost> availableHosts) {
		Map<Vm, Host> vmAllocations = new HashMap<>();
		boolean vmsCanMigrate = true;
		
		List<? extends Vm> pendingVms = new ArrayList<>(vms);
		List<PowerHost> candidateHosts = new ArrayList<>(availableHosts);
		
		
		while(vmsCanMigrate) {
			System.out.println("Num vms: " + pendingVms.size());
			// TODO: Recalcular només els hosts i VMs implicades
			Map<Host, WastedHostResources> wastedHostResourcesMap = getHostWastageMap(candidateHosts);
			Map<String, WastedHostResourcesVm> wastedVmHostVmResourcesMap = getWastedVmHostResourcesMap(wastedHostResourcesMap, pendingVms);
			Map<String, WastedHostResourcesVmVariation> wastedVmHostVmResourcesVariationMap = getWastedVmHostVmResourcesVariationMap(wastedVmHostVmResourcesMap);
			// TODO: Clean hosts that are not useful for the next loop
			VmAllocation vmAllocation = getBestVmAllocation(wastedHostResourcesMap, wastedVmHostVmResourcesMap, wastedVmHostVmResourcesVariationMap);
			if(vmAllocation == null) {
				vmsCanMigrate = false;
			} else {
				vmAllocations.put(vmAllocation.vm, vmAllocation.host);
				vmAllocation.host.vmCreate(vmAllocation.vm);
				pendingVms.remove(vmAllocation.vm);
				if(pendingVms.size() == 0) {
					vmsCanMigrate = false;
				}
			}
			
			//hostStats = getHostStats(availableHosts);
			//vmHostStats = getStats(vms, availableHosts);
			// clean hosts that are not useful
			// decide vmAllocation
			// remove VM from pendingVms
			// check vmsCanMigrate
		}
		System.out.println("Stop");
		return vmAllocations;		
	}
	
	private List<Map<String, Object>> getNewVmPlacementList(Map<Vm, Host> migrations) {
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		for(Entry<Vm, Host> entry : migrations.entrySet()) {
			Map<String, Object> migrate = new HashMap<String, Object>();
			migrate.put("vm", entry.getKey());
			migrate.put("host", entry.getValue());
			migrationMap.add(migrate);
		}
		return migrationMap;
	}

	public List<Map<String, Object>> getNewVmPlacement(
			List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		List<PowerHost> allHosts = this.getVmOptimizerPolicy().<PowerHost> getHostList();
		List<PowerHost> availableHosts = allHosts.stream()
				.filter(host -> !excludedHosts.contains(host)).collect(Collectors.toList());
		
		Map<Vm, Host> migrations = decideVMAllocations(vmsToMigrate, availableHosts);
		List<Map<String, Object>> newVmPlacementList = getNewVmPlacementList(migrations);
		 
		/*
		Collections.sort(vmsToMigrate, VmComparator);	// sort VMs with custom comparator, otherwise it would be the same as in the superclass
		
		for (Vm vm : vmsToMigrate) {
			PowerHost allocatedHost = findHostForVm(vm, excludedHosts);
			if (allocatedHost != null) {
				allocatedHost.vmCreate(vm);
				Log.printLine("VM #" + vm.getId() + " allocated to host #" + allocatedHost.getId());

				Map<String, Object> migrate = new HashMap<String, Object>();
				migrate.put("vm", vm);
				migrate.put("host", allocatedHost);
				migrationMap.add(migrate);
			}
		}
		*/
		return newVmPlacementList;
	}

	@Override
	public boolean allocateHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm, host);
	}

	@Override
	public List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return this.getVmOptimizerPolicy().getNewVmPlacementFromUnderUtilizedHostOriginal(vmsToMigrate, excludedHosts);
	}
}
