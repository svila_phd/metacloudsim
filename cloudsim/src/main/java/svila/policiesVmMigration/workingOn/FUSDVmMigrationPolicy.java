package svila.policiesVmMigration.workingOn;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.power.lists.PowerVmList;

import svila.vmOptimizerFix.VMMigrationPolicy;

public class FUSDVmMigrationPolicy extends VMMigrationPolicy {
	
	double differenceDefault = 10;
	double hotThresholdDefault = 0.90;
	double coldThresholdDefault = 0.25;
	double greenThresholdDefault = 0.40;
	double minRamDefault = 4096.0;
	double hotTemperatureOfServerThresholdDefault = 0.9;
	
	/* TODO
	 * S'utilitza CPU, RAM i BW, separar la tècnica amb només CPU, i CPU i RAM
	 * Adaptar el BW amb la implementació pròpia
	 */
	
	/** The vm selection policy. */
	//private PowerVmSelectionPolicy vmSelectionPolicy;

	/** The saved allocation. */
	//private final List<Map<String, Object>> savedAllocation = new ArrayList<Map<String, Object>>();

	/** The utilization history. */
	//private final Map<Integer, List<Double>> utilizationHistory = new HashMap<Integer, List<Double>>();

	/** The metric history. */

	/** The time history. */
	//private final Map<Integer, List<Double>> timeHistory = new HashMap<Integer, List<Double>>();

	/** The execution time history vm selection. */
	//private final List<Double> executionTimeHistoryVmSelection = new LinkedList<Double>();

	/** The execution time history host selection. */
	//private final List<Double> executionTimeHistoryHostSelection = new LinkedList<Double>();

	/** The execution time history vm reallocation. */
	//private final List<Double> executionTimeHistoryVmReallocation = new LinkedList<Double>();

	/** The execution time history total. */
	//private final List<Double> executionTimeHistoryTotal = new LinkedList<Double>();

	/**
	 * Instantiates a new power vm allocation policy migration abstract.
	 * 
	 * @param hostList the host list
	 * @param vmSelectionPolicy the vm selection policy
	 */
	/*public FUSDVmMigrationPolicy(
			List<? extends Host> hostList,
			PowerVmSelectionPolicy vmSelectionPolicy) {
		super(hostList, vmSelectionPolicy);
		setVmSelectionPolicy(vmSelectionPolicy);
		Log.printLine("\n ********************* Fast Up Slow Down Algortihm - Vm Allocation Policy **********************\n ");
	}*/
	
	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
		// TODO Auto-generated method stub
		return this.getVmOptimizerPolicy().findHostForVm(vm, excludedHosts);
	}

	@Override
	public boolean allocateHostForVm(Vm vm) {
		// TODO Auto-generated method stub
		return this.getVmOptimizerPolicy().allocateHostForVm(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		// TODO Auto-generated method stub
		return this.getVmOptimizerPolicy().allocateHostForVm(vm, host);
	}
	
	/**
	 * Find host for vm.
	 * 
	 * @param vm the vm
	 * @return the power host
	 */
	@Override
	public PowerHost findHostForVm(Vm vm) {
		Set<Host> excludedHosts = new HashSet<Host>();
		if (vm.getHost() != null) {
			excludedHosts.add(vm.getHost());
		}
		return findHostForVmByFUSDforHotSpots(vm, excludedHosts);
	}

	/**
	 * Find host for vm. by FUSD alg. for hot spot
	 * 
	 * @param vm the vm
	 * @param excludedHosts the excluded hosts
	 * @return the power host
	 */
	public PowerHost findHostForVmByFUSDforHotSpots(Vm vm, Set<? extends Host> excludedHosts) {

		PowerHost allocatedHost = null;
		double currentTemperature;
		double currentSkewness;
		double tempWithoutVM;
		double skewWithoutVM;
		double tempDif;
		double skewDif;
		
		//double difference=10;
		double difference = differenceDefault;
		
		PowerHost tempHost =null ;
		List<PowerHost> hostList = this.getVmOptimizerPolicy().getHostList();
		for (PowerHost host : hostList) {
			if (excludedHosts.contains(host)) {
				continue;
			}
			if (host.isSuitableForVm(vm)) {
				currentTemperature = host.hotTemperatureOfServer();
				currentSkewness = host.skewnessOfServer();
				tempWithoutVM = calculateNewTempHS(host,vm);
				skewWithoutVM= calculateNewSkewHS(host,vm);
				tempDif = Math.abs(currentTemperature - tempWithoutVM);
				skewDif = Math.abs(currentSkewness - skewWithoutVM);
				if (currentTemperature ==0 && currentSkewness ==0){
					tempHost=host;
				}
				if ((tempDif + skewDif) < difference){
					difference = tempDif + skewDif;
					tempHost = host;
				}
			}
		}
		allocatedHost = tempHost;
		return allocatedHost;
	}
	
	public double calculateNewSkewHS(PowerHost host, Vm vm){
		
		// current resource utilization, only bottleneck resources are considered
		double currentRAMutilization = (host.getRamProvisioner().getUsedRam() - vm.getRam())/(host.getRamProvisioner().getRam());
		double currentBWutilization = (host.getBwProvisioner().getUsedBw() - vm.getBw())/(host.getBwProvisioner().getBw());
		double currentMIPSutilization = 1- ( (host.getAvailableMips() - vm.getMips())/host.getTotalMips());
		
		// average resourse utilization is needed for the skewness formula
		double averageUtilization = (currentRAMutilization + currentBWutilization + currentMIPSutilization)/3;
		
		// applying skewness formula
		double ram = Math.pow(((currentRAMutilization/averageUtilization)-1), 2);
		double bw = Math.pow(((currentBWutilization/averageUtilization)-1), 2);
		double cpu = Math.pow(((currentMIPSutilization/averageUtilization)-1), 2);
		double skewness = Math.sqrt(ram + bw + cpu);
		
		return skewness;
	}
	
	public double calculateNewTempHS(PowerHost host, Vm vm){
		
		// current resource utilization, only bottleneck resources are considered
		double currentRAMutilization = (host.getRamProvisioner().getUsedRam() - vm.getRam())/(host.getRamProvisioner().getRam());
		double currentBWutilization = (host.getBwProvisioner().getUsedBw() - vm.getBw())/(host.getBwProvisioner().getBw());
		double currentMIPSutilization = 1- ( (host.getAvailableMips() - vm.getMips())/host.getTotalMips());
		
		// definitions for threshold values for resources (as stated in corresponding paper)
		//double hotThreshold = 0.90;
		double hotThreshold = hotThresholdDefault;
		
		//double coldThreshold = 0.25;
		//double warmThreshold = 0.65;
		//double greenComputingThreshold = 0.40;
		//double consolidationLimit = 0.05;
		
		double temperatureHot=0;
		
		//applying temperature formula for hotThreshold
		if (currentRAMutilization >= hotThreshold){
			temperatureHot = temperatureHot + Math.pow((currentRAMutilization-hotThreshold), 2);
		}
		if (currentBWutilization >= hotThreshold){
			temperatureHot = temperatureHot + Math.pow((currentBWutilization-hotThreshold), 2);
		}
		if (currentMIPSutilization >= hotThreshold){
			temperatureHot = temperatureHot + Math.pow((currentMIPSutilization-hotThreshold), 2);
		}
		
		return temperatureHot;
	}
	
	
	/**
	 * Find host for vm. by FUSD alg. for hot spot
	 * 
	 * @param vm the vm
	 * @param excludedHosts the excluded hosts
	 * @return the power host
	 */
	public PowerHost findHostForVmByFUSDforColdSpots(Vm vm, Set<? extends Host> excludedHosts) {

		PowerHost allocatedHost = null;
		double currentTemperature;
		double currentSkewness;
		double tempWithVM;
		double skewWithVM;
		double tempDif;
		double skewDif;
		//double difference=10;
		double difference = differenceDefault;
		
		PowerHost tempHost =null ;
		List<PowerHost> hostList = this.getVmOptimizerPolicy().getHostList();
		for (PowerHost host : hostList) {
			if (excludedHosts.contains(host)) {
				continue;
			}
			if (host.isSuitableForVm(vm)) {
				currentTemperature = host.coldTemperatureOfServer();
				currentSkewness = host.skewnessOfServer();
				tempWithVM = calculateNewTempCS(host,vm);
				skewWithVM= calculateNewSkewCS(host,vm);
				tempDif = Math.abs(currentTemperature - tempWithVM);
				skewDif = Math.abs(currentSkewness - skewWithVM);
				if (((tempDif + skewDif) < difference) && (tempWithVM <= 0.65)){
					difference = tempDif + skewDif;
					tempHost = host;
				}
			}
		}
		allocatedHost = tempHost;
		return allocatedHost;
	}
	
	public double calculateNewSkewCS(PowerHost host, Vm vm){
		
		// current resource utilization, only bottleneck resources are considered
		double currentRAMutilization = (host.getRamProvisioner().getUsedRam() + vm.getRam())/(host.getRamProvisioner().getRam());
		double currentBWutilization = (host.getBwProvisioner().getUsedBw() + vm.getBw())/(host.getBwProvisioner().getBw());
		double currentMIPSutilization = 1- ( (host.getAvailableMips() + vm.getMips())/host.getTotalMips());
		
		// average resourse utilization is needed for the skewness formula
		double averageUtilization = (currentRAMutilization + currentBWutilization + currentMIPSutilization)/3;
		
		// applying skewness formula
		double ram = Math.pow(((currentRAMutilization/averageUtilization)-1), 2);
		double bw = Math.pow(((currentBWutilization/averageUtilization)-1), 2);
		double cpu = Math.pow(((currentMIPSutilization/averageUtilization)-1), 2);
		double skewness = Math.sqrt(ram + bw + cpu);
		
		return skewness;
	}
	
	public double calculateNewTempCS(PowerHost host, Vm vm){
		
		// current resource utilization, only bottleneck resources are considered
		double currentRAMutilization = (host.getRamProvisioner().getUsedRam() + vm.getRam())/(host.getRamProvisioner().getRam());
		double currentBWutilization = (host.getBwProvisioner().getUsedBw() + vm.getBw())/(host.getBwProvisioner().getBw());
		double currentMIPSutilization = 1- ( (host.getAvailableMips() + vm.getMips())/host.getTotalMips());
		
		// definitions for threshold values for resources (as stated in corresponding paper)
		//double hotThreshold = 0.90;
		
		//double coldThreshold = 0.25;
		double coldThreshold = coldThresholdDefault;
		
		//double warmThreshold = 0.65;
		//double greenComputingThreshold = 0.40;
		//double consolidationLimit = 0.05;
		
		double temperatureCold=0;
		
		//applying temperature formula for hotThreshold
		if (currentRAMutilization <= coldThreshold){
			temperatureCold = temperatureCold + Math.pow((currentRAMutilization-coldThreshold), 2);
			if (currentBWutilization <= coldThreshold){
				temperatureCold = temperatureCold + Math.pow((currentBWutilization-coldThreshold), 2);
				if (currentMIPSutilization <= coldThreshold){
					temperatureCold = temperatureCold + Math.pow((currentMIPSutilization-coldThreshold), 2);
				}
			}
		}
				
		return temperatureCold;
	}
	
	//FUSD alg, this method brings the sorted list of high temperature hosts
		protected List<PowerHostUtilizationHistory> getOverUtilizedHostsFUSD() {
			Log.printLine("inside getOverUtilizedHostsFUSD function");
			List<PowerHostUtilizationHistory> overUtilizedHosts = new LinkedList<PowerHostUtilizationHistory>();
			List<PowerHostUtilizationHistory> tempHosts = new LinkedList<PowerHostUtilizationHistory>();
			List<PowerHostUtilizationHistory> hostList = this.getVmOptimizerPolicy().getHostList();
			for (PowerHostUtilizationHistory host : hostList) {
				tempHosts.add(host);
			}
			int i;
			double maxTemperature=0;
			int maxHostID=0;
			int size = tempHosts.size();
			
				for (i=0 ; i< tempHosts.size(); i++){
					if(tempHosts.get(i).hotTemperatureOfServer() == 0){
						continue;
					}
					if( tempHosts.get(i).hotTemperatureOfServer() > maxTemperature){
						maxTemperature= tempHosts.get(i).hotTemperatureOfServer() ;
						maxHostID=i;
					}
					if (i == (size-1)){
						overUtilizedHosts.add(tempHosts.get(maxHostID));
						tempHosts.remove(maxHostID);
					}
				}
			
			return overUtilizedHosts;
		}
		
		/**
		 * Gets the new vm placement.
		 * 
		 * @param vmsToMigrate the vms to migrate
		 * @param excludedHosts the excluded hosts
		 * @return the new vm placement
		 */
		@Override
		public List<Map<String, Object>> getNewVmPlacement(
				List<? extends Vm> vmsToMigrate,
				Set<? extends Host> excludedHosts) {
			List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
			PowerVmList.sortByCpuUtilization(vmsToMigrate);
			for (Vm vm : vmsToMigrate) {
				PowerHost allocatedHost = findHostForVmByFUSDforHotSpots( vm , excludedHosts);
				//PowerHost allocatedHost = findHostForVm(vm, excludedHosts);
				if (allocatedHost != null) {
					allocatedHost.vmCreate(vm);
					Log.printLine("VM #" + vm.getId() + " allocated to host #" + allocatedHost.getId());

					Map<String, Object> migrate = new HashMap<String, Object>();
					migrate.put("vm", vm);
					migrate.put("host", allocatedHost);
					migrationMap.add(migrate);
				}
			}
			return migrationMap;
		}

		/**
		 * Gets the new vm placement from under utilized host.
		 * 
		 * @param vmsToMigrate the vms to migrate
		 * @param excludedHosts the excluded hosts
		 * @return the new vm placement from under utilized host
		 */
		public List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(
				List<? extends Vm> vmsToMigrate,
				Set<? extends Host> excludedHosts) {
			List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
			PowerVmList.sortByCpuUtilization(vmsToMigrate);
			for (Vm vm : vmsToMigrate) {
				PowerHost allocatedHost = findHostForVmByFUSDforColdSpots(vm, excludedHosts);
				if (allocatedHost != null) {
					allocatedHost.vmCreate(vm);
					Log.printLine("VM #" + vm.getId() + " allocated to host #" + allocatedHost.getId());

					Map<String, Object> migrate = new HashMap<String, Object>();
					migrate.put("vm", vm);
					migrate.put("host", allocatedHost);
					migrationMap.add(migrate);
				} else {
					Log.printLine("Not all VMs can be reallocated from the host, reallocation cancelled");
					for (Map<String, Object> map : migrationMap) {
						((Host) map.get("host")).vmDestroy((Vm) map.get("vm"));
					}
					migrationMap.clear();
					break;
				}
			}
			return migrationMap;
		}

		/**
		 * Gets the vms to migrate from hosts.
		 * 
		 * @param overUtilizedHosts the over utilized hosts
		 * @return the vms to migrate from hosts
		 */
		protected
				List<? extends Vm>
				getVmsToMigrateFromHosts(List<PowerHostUtilizationHistory> overUtilizedHosts) {
			List<Vm> vmsToMigrate = new LinkedList<Vm>();
			for (PowerHostUtilizationHistory host : overUtilizedHosts) {
				while (true) {
					Vm vm = this.getVmOptimizerPolicy().getVmSelectionPolicy().getVmToMigrate(host);
					if (vm == null) {
						break;
					}
					vmsToMigrate.add(vm);
					host.vmDestroy(vm);
					if (!isHostOverUtilized(host)) {
						break;
					}
				}
			}
			return vmsToMigrate;
		}
		
		/**
		 * Gets the under utilized host. by FUSD alg.
		 * 
		 * @param excludedHosts the excluded hosts
		 * @return the under utilized host
		 */
		protected PowerHost getUnderUtilizedHostFUSD(Set<? extends Host> excludedHosts) {
			Log.printLine("inside getUnderUtilizedHostsFUSD function");
			PowerHost underUtilizedHost = null;
			
			//double coldThreshold=0.25;
			//double greenThreshold = 0.40;
			//double minRam = 4096.0;
			double coldThreshold = coldThresholdDefault;
			double greenThreshold = greenThresholdDefault;
			double minRam = minRamDefault;
			
			
			List<PowerHost> hostList = this.getVmOptimizerPolicy().getHostList();
			for (PowerHost host : hostList) {
				if (excludedHosts.contains(host)) {
					continue;
				}
				double coldTempOfHost = host.coldTemperatureOfServer();
				double greenTempOfHost = host.greenComputingTemperatureOfServer();
				double currentRAM = host.getUtilizationOfRam();
				if ( (coldTempOfHost<= coldThreshold) && (greenTempOfHost <= greenThreshold) &&
					 (currentRAM <= minRam) && !areAllVmsMigratingOutOrAnyVmMigratingIn(host)) {
					minRam = currentRAM;
					underUtilizedHost=host;
									
				}
			}
			return underUtilizedHost;
		}

		/**
		 * Checks whether all vms are in migration.
		 * 
		 * @param host the host
		 * @return true, if successful
		 */
		protected boolean areAllVmsMigratingOutOrAnyVmMigratingIn(PowerHost host) {
			for (PowerVm vm : host.<PowerVm> getVmList()) {
				if (!vm.isInMigration()) {
					return false;
				}
				if (host.getVmsMigratingIn().contains(vm)) {
					return true;
				}
			}
			return true;
		}

		/**
		 * Checks if is host over utilized.
		 * 
		 * @param host the host
		 * @return true, if is host over utilized
		 */

		protected boolean isHostOverUtilized(PowerHost host) {
			
			if (host.hotTemperatureOfServer() >= hotTemperatureOfServerThresholdDefault){
				return true;
			}
			else {
				return false;
			}
		}

	/**
	 * Optimize allocation of the VMs according to current utilization.
	 * 
	 * @param vmList the vm list
	 * 
	 * @return the array list< hash map< string, object>>
	 */
//	@Override
//	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
//		ExecutionTimeMeasurer.start("optimizeAllocationTotal");
//
//		ExecutionTimeMeasurer.start("optimizeAllocationHostSelection");
//		List<PowerHostUtilizationHistory> overUtilizedHosts = getOverUtilizedHostsFUSD();
//		getExecutionTimeHistoryHostSelection().add(
//				ExecutionTimeMeasurer.end("optimizeAllocationHostSelection"));
//
//		printOverUtilizedHosts(overUtilizedHosts);
//
//		saveAllocation();
//
//		ExecutionTimeMeasurer.start("optimizeAllocationVmSelection");
//		List<? extends Vm> vmsToMigrate = getVmsToMigrateFromHosts(overUtilizedHosts);
//		getExecutionTimeHistoryVmSelection().add(ExecutionTimeMeasurer.end("optimizeAllocationVmSelection"));
//
//		Log.printLine("Reallocation of VMs from the over-utilized hosts:");
//		ExecutionTimeMeasurer.start("optimizeAllocationVmReallocation");
//		List<Map<String, Object>> migrationMap = getNewVmPlacement(vmsToMigrate, new HashSet<Host>(
//				overUtilizedHosts));
//		getExecutionTimeHistoryVmReallocation().add(
//				ExecutionTimeMeasurer.end("optimizeAllocationVmReallocation"));
//		Log.printLine();
//
//		migrationMap.addAll(getMigrationMapFromUnderUtilizedHosts(overUtilizedHosts));
//
//		restoreAllocation();
//
//		getExecutionTimeHistoryTotal().add(ExecutionTimeMeasurer.end("optimizeAllocationTotal"));
//				
//		return migrationMap;
//	}

	/**
	 * Gets the migration map from under utilized hosts.
	 * 
	 * @param overUtilizedHosts the over utilized hosts
	 * @return the migration map from under utilized hosts
	 */
//	protected List<Map<String, Object>> getMigrationMapFromUnderUtilizedHosts(
//			List<PowerHostUtilizationHistory> overUtilizedHosts) {
//		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
//		List<PowerHost> switchedOffHosts = getSwitchedOffHosts();
//
//		// over-utilized hosts + hosts that are selected to migrate VMs to from over-utilized hosts
//		Set<PowerHost> excludedHostsForFindingUnderUtilizedHost = new HashSet<PowerHost>();
//		excludedHostsForFindingUnderUtilizedHost.addAll(overUtilizedHosts);
//		excludedHostsForFindingUnderUtilizedHost.addAll(switchedOffHosts);
//		excludedHostsForFindingUnderUtilizedHost.addAll(extractHostListFromMigrationMap(migrationMap));
//
//		// over-utilized + under-utilized hosts
//		Set<PowerHost> excludedHostsForFindingNewVmPlacement = new HashSet<PowerHost>();
//		excludedHostsForFindingNewVmPlacement.addAll(overUtilizedHosts);
//		excludedHostsForFindingNewVmPlacement.addAll(switchedOffHosts);
//
//		int numberOfHosts = this.getVmOptimizerPolicy().getHostList().size();
//
//		while (true) {
//			if (numberOfHosts == excludedHostsForFindingUnderUtilizedHost.size()) {
//				break;
//			}
//
//			PowerHost underUtilizedHost = getUnderUtilizedHostFUSD(excludedHostsForFindingUnderUtilizedHost);
//			if (underUtilizedHost == null) {
//				break;
//			}
//
//			Log.printLine("Under-utilized host: host #" + underUtilizedHost.getId() + "\n");
//
//			excludedHostsForFindingUnderUtilizedHost.add(underUtilizedHost);
//			excludedHostsForFindingNewVmPlacement.add(underUtilizedHost);
//
//			List<? extends Vm> vmsToMigrateFromUnderUtilizedHost = getVmsToMigrateFromUnderUtilizedHost(underUtilizedHost);
//			if (vmsToMigrateFromUnderUtilizedHost.isEmpty()) {
//				continue;
//			}
//
//			Log.print("Reallocation of VMs from the under-utilized host: ");
//			if (!Log.isDisabled()) {
//				for (Vm vm : vmsToMigrateFromUnderUtilizedHost) {
//					Log.print(vm.getId() + " ");
//				}
//			}
//			Log.printLine();
//
//			List<Map<String, Object>> newVmPlacement = getNewVmPlacementFromUnderUtilizedHost(
//					vmsToMigrateFromUnderUtilizedHost,
//					excludedHostsForFindingNewVmPlacement);
//
//			excludedHostsForFindingUnderUtilizedHost.addAll(extractHostListFromMigrationMap(newVmPlacement));
//
//			migrationMap.addAll(newVmPlacement);
//			Log.printLine();
//		}
//
//		return migrationMap;
//	}

	/**
	 * Prints the over utilized hosts.
	 * 
	 * @param overUtilizedHosts the over utilized hosts
	 */
//	protected void printOverUtilizedHosts(List<PowerHostUtilizationHistory> overUtilizedHosts) {
//		if (!Log.isDisabled()) {
//			Log.printLine("Over-utilized hosts:");
//			for (PowerHostUtilizationHistory host : overUtilizedHosts) {
//				Log.printLine("Host #" + host.getId());
//			}
//			Log.printLine();
//		}
//	}

	
	/**
	 * Checks if is host over utilized after allocation.
	 * 
	 * @param host the host
	 * @param vm the vm
	 * @return true, if is host over utilized after allocation
	 */
//	protected boolean isHostOverUtilizedAfterAllocation(PowerHost host, Vm vm) {
//		boolean isHostOverUtilizedAfterAllocation = true;
//		if (host.vmCreate(vm)) {
//			isHostOverUtilizedAfterAllocation = isHostOverUtilized(host);
//			host.vmDestroy(vm);
//		}
//		return isHostOverUtilizedAfterAllocation;
//	}

	
	
	
	
	/**
	 * Extract host list from migration map.
	 * 
	 * @param migrationMap the migration map
	 * @return the list
	 */
//	protected List<PowerHost> extractHostListFromMigrationMap(List<Map<String, Object>> migrationMap) {
//		List<PowerHost> hosts = new LinkedList<PowerHost>();
//		for (Map<String, Object> map : migrationMap) {
//			hosts.add((PowerHost) map.get("host"));
//		}
//		return hosts;
//	}

	
	
	/**
	 * Gets the vms to migrate from under utilized host.
	 * 
	 * @param host the host
	 * @return the vms to migrate from under utilized host
	 */
//	protected List<? extends Vm> getVmsToMigrateFromUnderUtilizedHost(PowerHost host) {
//		List<Vm> vmsToMigrate = new LinkedList<Vm>();
//		for (Vm vm : host.getVmList()) {
//			if (!vm.isInMigration()) {
//				vmsToMigrate.add(vm);
//			}
//		}
//		return vmsToMigrate;
//	}
	
	

	/**
	 * Gets the switched off host.
	 * 
	 * @return the switched off host
	 */
//	protected List<PowerHost> getSwitchedOffHosts() {
//		List<PowerHost> switchedOffHosts = new LinkedList<PowerHost>();
//		List<PowerHost> hostList = this.getVmOptimizerPolicy().getHostList();
//		for (PowerHost host : hostList) {
//			if (host.getUtilizationOfCpu() == 0) {
//				switchedOffHosts.add(host);
//			}
//		}
//		return switchedOffHosts;
//	}	
	
	/**
	 * Adds the history value.
	 * 
	 * @param host the host
	 * @param metric the metric
	 */
	// SERGI comment: apparently not used
//	protected void addHistoryEntry(HostDynamicWorkload host, double metric) {
//		int hostId = host.getId();
//		if (!getTimeHistory().containsKey(hostId)) {
//			getTimeHistory().put(hostId, new LinkedList<Double>());
//		}
//		if (!getUtilizationHistory().containsKey(hostId)) {
//			getUtilizationHistory().put(hostId, new LinkedList<Double>());
//		}
//		if (!getMetricHistory().containsKey(hostId)) {
//			getMetricHistory().put(hostId, new LinkedList<Double>());
//		}
//		if (!getTimeHistory().get(hostId).contains(CloudSim.clock())) {
//			getTimeHistory().get(hostId).add(CloudSim.clock());
//			getUtilizationHistory().get(hostId).add(host.getUtilizationOfCpu());
//			getMetricHistory().get(hostId).add(metric);
//		}
//	}

	/**
	 * Save allocation.
	 */
//	protected void saveAllocation() {
//		getSavedAllocation().clear();
//		List<Host> hostList = this.getVmOptimizerPolicy().getHostList();
//		for (Host host : hostList) {
//			for (Vm vm : host.getVmList()) {
//				if (host.getVmsMigratingIn().contains(vm)) {
//					continue;
//				}
//				Map<String, Object> map = new HashMap<String, Object>();
//				map.put("host", host);
//				map.put("vm", vm);
//				getSavedAllocation().add(map);
//			}
//		}
//	}

	/**
	 * Restore allocation.
	 */
//	protected void restoreAllocation() {
//		List<Host> hostList = this.getVmOptimizerPolicy().getHostList();
//		for (Host host : hostList) {
//			host.vmDestroyAll();
//			host.reallocateMigratingInVms();
//		}
//		for (Map<String, Object> map : getSavedAllocation()) {
//			Vm vm = (Vm) map.get("vm");
//			PowerHost host = (PowerHost) map.get("host");
//			if (!host.vmCreate(vm)) {
//				Log.printLine("Couldn't restore VM #" + vm.getId() + " on host #" + host.getId());
//				System.exit(0);
//			}
//			this.getVmOptimizerPolicy().getVmTable().put(vm.getUid(), host);
//		}
//	}

	/**
	 * Gets the power after allocation.
	 * 
	 * @param host the host
	 * @param vm the vm
	 * 
	 * @return the power after allocation
	 */
//	protected double getPowerAfterAllocation(PowerHost host, Vm vm) {
//		double power = 0;
//		try {
//			power = host.getPowerModel().getPower(getMaxUtilizationAfterAllocation(host, vm));
//		} catch (Exception e) {
//			e.printStackTrace();
//			System.exit(0);
//		}
//		return power;
//	}

	/**
	 * Gets the power after allocation. We assume that load is balanced between PEs. The only
	 * restriction is: VM's max MIPS < PE's MIPS
	 * 
	 * @param host the host
	 * @param vm the vm
	 * 
	 * @return the power after allocation
	 */
//	protected double getMaxUtilizationAfterAllocation(PowerHost host, Vm vm) {
//		double requestedTotalMips = vm.getCurrentRequestedTotalMips();
//		double hostUtilizationMips = getUtilizationOfCpuMips(host);
//		double hostPotentialUtilizationMips = hostUtilizationMips + requestedTotalMips;
//		double pePotentialUtilization = hostPotentialUtilizationMips / host.getTotalMips();
//		return pePotentialUtilization;
//	}
	
	/**
	 * Gets the utilization of the CPU in MIPS for the current potentially allocated VMs.
	 *
	 * @param host the host
	 *
	 * @return the utilization of the CPU in MIPS
	 */
//	protected double getUtilizationOfCpuMips(PowerHost host) {
//		double hostUtilizationMips = 0;
//		for (Vm vm2 : host.getVmList()) {
//			if (host.getVmsMigratingIn().contains(vm2)) {
//				// calculate additional potential CPU usage of a migrating in VM
//				hostUtilizationMips += host.getTotalAllocatedMipsForVm(vm2) * 0.9 / 0.1;
//			}
//			hostUtilizationMips += host.getTotalAllocatedMipsForVm(vm2);
//		}
//		return hostUtilizationMips;
//	}

	/**
	 * Gets the saved allocation.
	 * 
	 * @return the saved allocation
	 */
//	protected List<Map<String, Object>> getSavedAllocation() {
//		return savedAllocation;
//	}

	/**
	 * Sets the vm selection policy.
	 * 
	 * @param vmSelectionPolicy the new vm selection policy
	 */
//	protected void setVmSelectionPolicy(PowerVmSelectionPolicy vmSelectionPolicy) {
//		this.vmSelectionPolicy = vmSelectionPolicy;
//	}

	/**
	 * Gets the vm selection policy.
	 * 
	 * @return the vm selection policy
	 */
//	protected PowerVmSelectionPolicy getVmSelectionPolicy() {
//		return vmSelectionPolicy;
//	}

	/**
	 * Gets the utilization history.
	 * 
	 * @return the utilization history
	 */
//	public Map<Integer, List<Double>> getUtilizationHistory() {
//		return utilizationHistory;
//	}

	/**
	 * Gets the metric history.
	 * 
	 * @return the metric history
	 */
//	public Map<Integer, List<Double>> getMetricHistory() {
//		return metricHistory;
//	}

	/**
	 * Gets the time history.
	 * 
	 * @return the time history
	 */
//	public Map<Integer, List<Double>> getTimeHistory() {
//		return timeHistory;
//	}

	/**
	 * Gets the execution time history vm selection.
	 * 
	 * @return the execution time history vm selection
	 */
//	public List<Double> getExecutionTimeHistoryVmSelection() {
//		return executionTimeHistoryVmSelection;
//	}

	/**
	 * Gets the execution time history host selection.
	 * 
	 * @return the execution time history host selection
	 */
//	public List<Double> getExecutionTimeHistoryHostSelection() {
//		return executionTimeHistoryHostSelection;
//	}

	/**
	 * Gets the execution time history vm reallocation.
	 * 
	 * @return the execution time history vm reallocation
	 */
//	public List<Double> getExecutionTimeHistoryVmReallocation() {
//		return executionTimeHistoryVmReallocation;
//	}

	/**
	 * Gets the execution time history total.
	 * 
	 * @return the execution time history total
	 */
//	public List<Double> getExecutionTimeHistoryTotal() {
//		return executionTimeHistoryTotal;
//	}

}
