package svila.policiesVmMigration.workingOn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.lists.PowerVmList;

import svila.policiesVmOptimizer.MetricsVMOptimizerPolicy;
import svila.vmOptimizerFix.VMMigrationPolicy;

public class MetricsVmMigrationPolicy extends VMMigrationPolicy {
	
	MetricsVMOptimizerPolicy metricsVMOptimizerPolicy;
	
	public MetricsVmMigrationPolicy() {
		
	}

	@Override
	public PowerHost findHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().findHostForVmOriginal(vm);
	}

	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
		return this.getVmOptimizerPolicy().findHostForVm(vm, excludedHosts);
	}
	
	
	
	@Override
	public List<Map<String, Object>> getNewVmPlacement(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		if(metricsVMOptimizerPolicy == null) {
			metricsVMOptimizerPolicy = (MetricsVMOptimizerPolicy) this.getVmOptimizerPolicy();
		}
		metricsVMOptimizerPolicy.vmAnalyzer.updateVMsToMigrateStats(vmsToMigrate);
		
		//addVMData(vmsToMigrate);
		
		// Calcular hostRisk
		
		// Calcular dispersionScore
		
		
		// Llista de migracions a realitzar
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		
		// Ordena les VMs per CPU
		PowerVmList.sortByCpuUtilization(vmsToMigrate);
		
		/*Collections.sort(vmsToMigrate, new Comparator<Vm>(){
		     public int compare(Vm vm1, Vm vm2){
		    	 metricsVMOptimizerPolicy.vmAnalyzer.getVmStats(o1)
		     }
		});*/
		
		
		// A part de per consum real, pel previst, evitarem pics
		
		List<PowerHost> hostList = this.<PowerHost>getVmOptimizerPolicy().getHostList();
		List<PowerHost> availableHost = getAvailableHosts(hostList, excludedHosts);
		decideMigrations(vmsToMigrate, availableHost);
		
//		// S'assigna un nou host a cada VM
//		for (Vm vm : vmsToMigrate) {
//			// S'obté el host
//			PowerHost allocatedHost = findHostForVm(vm, excludedHosts);
//			// Si existeix, es realitza el canvi
//			if (allocatedHost != null) {
//				// En teoria aquestes VM no tenen host, es creen al nou host
//				// En teoria, si no es migra, no es borra on estava originalment
//				allocatedHost.vmCreate(vm);
//				Log.printConcatLine("VM #", vm.getId(), " allocated to host #", allocatedHost.getId());
//				// Es crea el hashmap amb la migració
//				// Aquest sistema és bastant dolent...
//				Map<String, Object> migrate = new HashMap<String, Object>();
//				migrate.put("vm", vm);
//				migrate.put("host", allocatedHost);
//				migrationMap.add(migrate);
//			}
//		}
		return migrationMap;
	}
	
	private List<PowerHost> getAvailableHosts(List<PowerHost> hostList, Set<? extends Host> excludedHosts) {
		List<PowerHost> availableHosts = new ArrayList<>(hostList);
		availableHosts.removeAll(excludedHosts);
	    return availableHosts;
	}
	
	private void decideMigrations(List<? extends Vm> vmsToMigrate, List<PowerHost> availableHosts) {
		
	}

	@Override
	public boolean allocateHostForVm(Vm vm) {
		//System.out.println(vm.getClass().toString());
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm, host);
	}

	@Override
	public List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return this.getVmOptimizerPolicy().getNewVmPlacementFromUnderUtilizedHostOriginal(vmsToMigrate, excludedHosts);
	}
}
