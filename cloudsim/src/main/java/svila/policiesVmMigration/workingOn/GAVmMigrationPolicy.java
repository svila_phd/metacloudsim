package svila.policiesVmMigration.workingOn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.vmOptimizerFix.VMMigrationPolicy;
import org.moeaframework.core.Problem;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.EncodingUtils;
import org.moeaframework.util.Vector;

public class GAVmMigrationPolicy extends VMMigrationPolicy {

	public class CloudProblem implements Problem {
		@Override
		public void evaluate(Solution solution) {
			
			// negate the objectives since Knapsack is maximization
			solution.setObjectives(Vector.negate(f));
			solution.setConstraints(g);
		}

		@Override
		public String getName() {
			return "Cloud";
		}

		@Override
		public int getNumberOfConstraints() {
			return nsacks;
		}

		@Override
		public int getNumberOfObjectives() {
			return nsacks;
		}

		@Override
		public int getNumberOfVariables() {
			return 1;
		}

		@Override
		public Solution newSolution() {
			Solution solution = new Solution(1, nsacks, nsacks);
			solution.setVariable(0, EncodingUtils.newBinary(nitems));
			return solution;
		}

		@Override
		public void close() {
			//do nothing
		}
	}
	
	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private Map<Vm, PowerHost> getGABestSolution() {
		Map<Vm, PowerHost> best = new HashMap<>();
		
		
		
		return best;
	}

	@Override
	public List<Map<String, Object>> getNewVmPlacement(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public PowerHost findHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().findHostForVmOriginal(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm, host);
	}
	
	@Override
	public List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		return this.getVmOptimizerPolicy().getNewVmPlacementFromUnderUtilizedHostOriginal(vmsToMigrate, excludedHosts);
	}

}
