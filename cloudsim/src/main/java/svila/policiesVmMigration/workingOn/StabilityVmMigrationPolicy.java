package svila.policiesVmMigration.workingOn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.core.tools.picocli.CommandLine.Help.Column;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.vmOptimizerFix.VMMigrationPolicy;
import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.Table;

public class StabilityVmMigrationPolicy extends VMMigrationPolicy {
	
	// Ordenar les VMs per dispersió
	// Ordenar els hosts per dispersió
	// Classificar entitats per categories en funció de la variabilitat i el consum
	// % de MIPS requerits vs % de MIPS disponibles, ajustar obrint o tancant hosts
	// Obtenir el ratio d'estabilitat en funció de la variabilitat i el consum
	// Obtenir la distribució de cada VM i host, i tenir-la en compte.
	//		Pot ser a nivell de tota la simulació, o a nivell de finestra,
	//		o cap a on està tendint la distribució al llarg del temps
	//		Si és a nivell de host, tenir en compte les VMs actuals
	
	public StabilityVmMigrationPolicy() {
	}
	
	@Override
	public List<Map<String, Object>> getNewVmPlacement(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		
		
		return this.getVmOptimizerPolicy().getNewVmPlacementOriginal(vmsToMigrate, excludedHosts);
	}
	
	// Original Lago implementation
	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
	    double bestEnergyEfficiency = Double.MIN_VALUE;
	    PowerHost allocatedHost = null;
	    
	    for (PowerHost host : this.getVmOptimizerPolicy().<PowerHost> getHostList()) {
			if (excludedHosts.contains(host)) {
				continue;
			}	
	    	if (host.isSuitableForVm(vm)) {
		        double utilization = this.getVmOptimizerPolicy().getMaxUtilizationAfterAllocationOriginal(host, vm);
		        if ((!vm.isBeingInstantiated()) && utilization > 0.7 || (vm.isBeingInstantiated() && utilization > 1.0)) {
		          continue;
	        }
	        
	        try {
	          double powerAfterAllocation = this.getVmOptimizerPolicy().getPowerAfterAllocationOriginal(host, vm);
	          if (powerAfterAllocation != -1) {
	            // Host can be used

	            // Calculates the power quality
	            double energyEfficiency = getHostEnergyEfficiency(host);
	            if (energyEfficiency > bestEnergyEfficiency) {
	              bestEnergyEfficiency = energyEfficiency;
	              allocatedHost = host;
	            } else if (energyEfficiency == bestEnergyEfficiency) {
	              // Tie break

	              // The lesser power consumption with the VMs instantiated at datacenter will be chosen
	              double power_vm_allocated_at_host = allocatedHost.getPower() + this.getVmOptimizerPolicy().getPowerAfterAllocationOriginal(host, vm);
	              double power_vm_allocated_at_allocatedHost = host.getPower() + this.getVmOptimizerPolicy().getPowerAfterAllocationOriginal(allocatedHost, vm);
	              if (power_vm_allocated_at_host < power_vm_allocated_at_allocatedHost) {
	                allocatedHost = host;
	              } else if (power_vm_allocated_at_host == power_vm_allocated_at_allocatedHost) {
	                // Calculates the best using MIPS / total MIPS to minimized vm
	                // migration
	                if (host.getUtilizationOfCpu() > allocatedHost.getUtilizationOfCpu()) {
	                  allocatedHost = host;
	                } else if (host.getUtilizationOfCpu() == allocatedHost.getUtilizationOfCpu()) {
	                  // The best processor will be allocated
	                  if (host.getTotalMips() > allocatedHost.getTotalMips()) {
	                    allocatedHost = host;
	                  }
	                }
	              }
	            }
	          }
	        } catch (Exception e) {
	        }
	      }
	    }

	    return allocatedHost;
	}	
	
	public static double getHostEnergyEfficiency(PowerHost host) {
	    return (host.getTotalMips() / host.getMaxPower());
	  }

	@Override
	public PowerHost findHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().findHostForVmOriginal(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		return this.getVmOptimizerPolicy().allocateHostForVmOriginal(vm, host);
	}
	
	private double getMIPSPercDeviation(List<PowerHost> activeHosts) {
		DoubleColumn cpuUtilizations = DoubleColumn.create("CPU_utilization", activeHosts.size());
		int i = 0;
		for(PowerHost host : activeHosts) {
			cpuUtilizations.set(i, host.getUtilizationOfCpu());
			i++;
		}
		return cpuUtilizations.standardDeviation();
	}
	
	private double getMIPSPercMean(List<PowerHost> activeHosts) {
		DoubleColumn cpuUtilizations = DoubleColumn.create("CPU_utilization", activeHosts.size());
		int i = 0;
		for(PowerHost host : activeHosts) {
			cpuUtilizations.set(i, host.getUtilizationOfCpu());
			i++;
		}
		return cpuUtilizations.mean();
	}
	
	private List<PowerHost> getActiveHosts(List<PowerHost> hosts) {
		List<PowerHost> hostsOn = new ArrayList<>();
		for(PowerHost host : hosts) {
			if(host.getUtilizationOfCpu() > 0.0) {
				hostsOn.add(host);
			}
		}
		return hostsOn;
	}
	
	@Override
	public List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		
		List<PowerHost> hosts = this.getVmOptimizerPolicy().getHostList();
		List<PowerHost> hostsOn = getActiveHosts(hosts);
		double datacenterAllocatedMIPSPercDeviation = getMIPSPercDeviation(hostsOn);
		double datacenterAllocatedMIPSPercMean = getMIPSPercMean(hostsOn);
		Map<Integer, Double> stabilityMap = new HashMap<>();

		// VM sort
		// Host sort
		
		
		return this.getVmOptimizerPolicy().getNewVmPlacementFromUnderUtilizedHostOriginal(vmsToMigrate, excludedHosts);
	}

}
