package svila.policiesVmMigration.workingOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.Domination;
import org.cloudbus.cloudsim.power.GAInd;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.vmOptimizerFix.VMMigrationPolicy;

public class GALitVmMigrationPolicy extends VMMigrationPolicy {

	public static Random rnd;

	List<GAInd> pop, pareto;
	
	public GALitVmMigrationPolicy(
			List<? extends Host> hostList) {
		Log.setDisabled(true);
	}
	
	@Override
	public PowerHost findHostForVm(Vm vm) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> getNewVmPlacement(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean allocateHostForVm(Vm vm) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		// TODO Auto-generated method stub
		return false;
	}

	
	private void initGA() {
		rnd = new Random();
		pop = new ArrayList<GAInd>();
		pareto = new ArrayList<>();

		for (int i = 0; i < 50; i++) {
			try {
				addToPopulation(new GAInd(this));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}
		}
		
		for (int i = 0; i < 500; i++) {
			mutation();
			crossover();
		}
	}
	
	private void crossover() {
		GAInd p1, p2;
		p1 = pop.get(rnd.nextInt(pop.size()));
		do {
			p2 = pop.get(rnd.nextInt(pop.size()));
		} while (p1.equals(p2));
		
		try {
			addToPopulation(new GAInd(p1, p2));
		} catch (Exception e) {
		}
		//crossover takes too much time
		/*for (int i = 0; i < pop.size(); i++) {
			if (rnd.nextInt(100) < 90) {
				GAInd p1, p2;
				p1 = pop.get(rnd.nextInt(pop.size()));
				do {
					p2 = pop.get(rnd.nextInt(pop.size()));
				} while (p1.equals(p2));
				
				try {
					addToPopulation(new GAInd(p1, p2));
				} catch (Exception e) {
				}
			}
		}*/
	}

	private void mutation() {
		for (GAInd ind : pop) {
			if (!ind.isPareto && rnd.nextInt(1000) < 10) {
				try {
					ind.Mutation();
				} catch (Exception e) {
				}
			}
		}
	}

	private void removeIndividual(GAInd ind) {
		pop.remove(ind);
		pareto.remove(ind);
	}

	private boolean addToPareto(GAInd ind) {
		List<GAInd> dominatedInds = new ArrayList<>();
		for (GAInd target : pareto) {
			if (ind.dominates(target) == Domination.True) {
				dominatedInds.add(target);
			} else if (ind.dominates(target) == Domination.False) {
				return false;
			}
		}

		for (GAInd gaInd : dominatedInds) {
			removeIndividual(gaInd);
		}
		if (pareto.size() < 20) {
			pareto.add(ind);
			ind.isPareto = true;
			return true;
		}
		return false;
	}

	private boolean addToPopulation(GAInd ind) {
		if (pop.size() < 50) {
			pop.add(ind);
			addToPareto(ind);
			return true;
		}
		List<GAInd> dominatedInds = new ArrayList<>();
		for (GAInd gaInd : pop) {
			if (ind.dominates(gaInd) == Domination.True) {
				dominatedInds.add(gaInd);
			}
		}

		if (dominatedInds.size() > 0) {
			GAInd random = dominatedInds.get(rnd.nextInt(dominatedInds.size()));
			removeIndividual(random);
			pop.add(ind);
			addToPareto(ind);
			return true;
		}
		return false;
	}
}
