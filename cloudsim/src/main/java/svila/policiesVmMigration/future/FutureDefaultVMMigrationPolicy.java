package svila.policiesVmMigration.future;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Log;

import svila.policiesHostOverSaturation.future.OverSaturationData;
import svila.policiesHostUnderUtilisation.future.FutureVmList;
import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;

public class FutureDefaultVMMigrationPolicy extends FutureVmMigrationPolicyBase {

	@Override
	public FutureHost findHostForVm(FutureVm vm, Set<FutureHost> specificValidHosts) {
		//System.out.println("Searching host for VM " + vm.getId());
		double minPower = Double.MAX_VALUE;
		FutureHost allocatedHost = null;

		for (FutureHost host : specificValidHosts) {
			if (host.isSuitableForVm(vm)) {
				OverSaturationData osd = this.vmOptimizerPolicy.futureHostOverSaturationPolicy.getOverSaturationDataAfterAllocation(host, vm);
				if (host.getForecastedMIPS() != 0 && osd.existUpperThresholdSaturation) {
					continue;
				}

				try {
					double powerAfterAllocation = this.vmOptimizerPolicy.futureHostOverSaturationPolicy.getPowerAfterAllocation(host, vm);
					if (powerAfterAllocation != -1) {
						double powerDiff = powerAfterAllocation - host.getCurrentHost().getPower();
						if (powerDiff < minPower) {
							minPower = powerDiff;
							allocatedHost = host;
						}
					}
				} catch (Exception e) {
				}
			}
		}
		return allocatedHost;
	}

	@Override
	public List<Map<String, Object>> getNewVmPlacement(HashMap<Integer, FutureVm> vmsToMigrate,
			HashMap<String, Set<FutureHost>> futureHostClassified) {
		
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		List<FutureVm> vmsToMigrateList = new ArrayList<>(vmsToMigrate.values());
		FutureVmList.sortByCpuUtilization(vmsToMigrateList);
		
		for (FutureVm vm : vmsToMigrateList) {
			Set<FutureHost> validHosts = new HashSet<>(futureHostClassified.get("valid"));
			validHosts.remove(vm.getFutureHost());
			
			FutureHost allocatedHost = findHostForVm(vm, validHosts);
			if (allocatedHost != null) {
				allocatedHost.moveVm(vm);
				Log.printConcatLine("VM #", vm.getCurrentVm().getId(), " allocated to host #", allocatedHost.getCurrentHost().getId());

				Map<String, Object> migrate = new HashMap<String, Object>();
				migrate.put("vm", vm.getCurrentVm());
				migrate.put("host", allocatedHost.getCurrentHost());
				migrationMap.add(migrate);
			}
		}
		return migrationMap;
	}

}
