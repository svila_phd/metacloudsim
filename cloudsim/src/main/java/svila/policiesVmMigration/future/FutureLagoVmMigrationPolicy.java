package svila.policiesVmMigration.future;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;

public class FutureLagoVmMigrationPolicy extends FutureDefaultVMMigrationPolicy {
	int findHost;
	
	public FutureLagoVmMigrationPolicy() {
		findHost = 0;
	}
	
	@Override
	public FutureHost findHostForVm(FutureVm vm, Set<FutureHost> specificValidHosts) {
	    double bestEnergyEfficiency = Double.MIN_VALUE;
	    FutureHost allocatedHost = null;
		List<FutureHost> futureHosts = new ArrayList<>(specificValidHosts);

	    for (FutureHost host : futureHosts) {
	    	if (host.isSuitableForVm(vm)) {		        
		        double utilization = this.getVmOptimizerPolicy().futureHostOverSaturationPolicy.getMaxUtilizationAfterAllocation(host, vm);
		        if ((!vm.getCurrentVm().isBeingInstantiated()) && utilization > 0.7 || (vm.getCurrentVm().isBeingInstantiated() && utilization > 1.0)) {
		          continue;
		        }
	        
	        try {
		      double powerAfterAllocation = this.getVmOptimizerPolicy().futureHostOverSaturationPolicy.getPowerAfterAllocation(host, vm);

	          if (powerAfterAllocation != -1) {
	            // Host can be used

	            // Calculates the power quality
	            double energyEfficiency = getHostEnergyEfficiency(host);
	            if (energyEfficiency > bestEnergyEfficiency) {
	              bestEnergyEfficiency = energyEfficiency;
	              allocatedHost = host;
	            } else if (energyEfficiency == bestEnergyEfficiency) {
	              // Tie break

	              // The lesser power consumption with the VMs instantiated at datacenter will be chosen
	              double power_vm_allocated_at_host = allocatedHost.getPower() + this.getVmOptimizerPolicy().futureHostOverSaturationPolicy.getPowerAfterAllocation(host, vm);
	              double power_vm_allocated_at_allocatedHost = host.getPower() + this.getVmOptimizerPolicy().futureHostOverSaturationPolicy.getPowerAfterAllocation(allocatedHost, vm);
	              if (power_vm_allocated_at_host < power_vm_allocated_at_allocatedHost) {
	                allocatedHost = host;
	              } else if (power_vm_allocated_at_host == power_vm_allocated_at_allocatedHost) {
	                // Calculates the best using MIPS / total MIPS to minimized vm
	                // migration
	                if (host.getForecastedPercCPUWithMigrations() > allocatedHost.getForecastedPercCPUWithMigrations()) {
	                  allocatedHost = host;
	                } else if (host.getForecastedPercCPUWithMigrations() == allocatedHost.getForecastedPercCPUWithMigrations()) {
	                  // The best processor will be allocated
	                  if (host.getCurrentHost().getTotalMips() > allocatedHost.getCurrentHost().getTotalMips()) {
	                    allocatedHost = host;
	                  }
	                }
	              }
	            }
	          }
	        } catch (Exception e) {
	        }
	      }
	    }

	    return allocatedHost;
	}
	
	public static double getHostEnergyEfficiency(FutureHost host) {
	    return (host.getForecastedMIPSWithMigrations() / host.getCurrentHost().getMaxPower());
	  }

	@Override
	public List<Map<String, Object>> getNewVmPlacement(HashMap<Integer, FutureVm> vmsToMigrate,
			HashMap<String, Set<FutureHost>> futureHostClassified) {
		return super.getNewVmPlacement(vmsToMigrate, futureHostClassified);
	}
}
