package svila.policiesVmMigration.future;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;

public class FutureGuazzoneBFDVmMigrationPolicy extends FutureVmMigrationPolicyBase {
	public FutureGuazzoneBFDVmMigrationPolicy() {
	}
	
	public Comparator<FutureVm> VmComparator = new Comparator<FutureVm>()
	{
		@Override
		public int compare(FutureVm a, FutureVm b) throws ClassCastException {
			Double aUtilization = a.getForecastedMIPS();
			Double bUtilization = b.getForecastedMIPS();
			return bUtilization.compareTo(aUtilization);
		}
	};
	
	public Comparator<FutureHost> HostComparator = new Comparator<FutureHost>()
	{
		@Override
		public int compare(FutureHost a, FutureHost b) throws ClassCastException {
			Integer aUtilization = (a.getForecastedPercCPUWithMigrations()==0)?0:1;	//its name is misleading, but this is actually the correct method to use
			Integer bUtilization = (b.getForecastedPercCPUWithMigrations()==0)?0:1;	//we know this because the  getSwitchedOffHosts method of CloudSim calls this too
			int cUtilization = bUtilization.compareTo(aUtilization);	//descending
			
			if (cUtilization!=0) return cUtilization;
			
			Integer aTotal = a.getCurrentHost().getTotalMips();
			Integer bTotal = b.getCurrentHost().getTotalMips();
			int cTotal = bTotal.compareTo(aTotal);	//descending
			
			if (cTotal!=0) return cTotal;
			
			Double aIdle = a.getCurrentHost().getPowerModel().getPower(0);	//idle power consumption
			Double bIdle = b.getCurrentHost().getPowerModel().getPower(0);
			int cIdle = aIdle.compareTo(bIdle);	//ascending
			
			return cIdle;
		}
	};
	
	public List<Map<String, Object>> getNewVmPlacement(
			HashMap<Integer, FutureVm> vmsToMigrate,
			HashMap<String, Set<FutureHost>> futureHostClassified) {
		
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		List<FutureVm> vmsToMigrateList = new ArrayList<>(vmsToMigrate.values());
		Collections.sort(vmsToMigrateList, VmComparator);	// sort VMs with custom comparator, otherwise it would be the same as in the superclass
		
		for (FutureVm vm : vmsToMigrateList) {
			Set<FutureHost> validHosts = new HashSet<>(futureHostClassified.get("valid"));
			validHosts.remove(vm.getFutureHost());
			FutureHost allocatedHost = findHostForVm(vm, validHosts);
			if (allocatedHost != null) {
				allocatedHost.moveVm(vm);
				Log.printLine("VM #" + vm.getCurrentVm().getId() + " allocated to host #" + allocatedHost.getCurrentHost().getId());

				Map<String, Object> migrate = new HashMap<String, Object>();
				migrate.put("vm", vm.getCurrentVm());
				migrate.put("host", allocatedHost.getCurrentHost());
				migrationMap.add(migrate);
			}
		}
		return migrationMap;
	}
	
	@Override
	public FutureHost findHostForVm(FutureVm vm, Set<FutureHost> specificValidHosts) {
		List<FutureHost> futureHosts = new ArrayList<>(specificValidHosts);
		
		Collections.sort(futureHosts, HostComparator);	//sort the hosts too, then do a simple ffd
		for (FutureHost host : futureHosts) {
			if (host.isSuitableForVm(vm)) {				
				if (host.getForecastedMIPSWithMigrations() != 0 &&
					this.getVmOptimizerPolicy().futureHostOverSaturationPolicy.getOverSaturationDataAfterAllocation(host, vm).existUpperThresholdSaturation) {
						continue;
					}

				return host;
			}
		}
		return null;
	}
}
