package svila.policiesVmMigration.future;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;

public class FuturePercentageUtilVmMigrationPolicy extends FutureVmMigrationPolicyBase {
	public FuturePercentageUtilVmMigrationPolicy() {
	}

	public Comparator<FutureVm> VmComparator = new Comparator<FutureVm>()
	{
		@Override
		public int compare(FutureVm a, FutureVm b) throws ClassCastException {
			Double aUtilization = a.getForecastedPercCPU();
			Double bUtilization = b.getForecastedPercCPU();
			return bUtilization.compareTo(aUtilization);
		}
	};
	
	public Comparator<FutureHost> HostComparator = new Comparator<FutureHost>()
	{
		@Override
		public int compare(FutureHost a, FutureHost b) throws ClassCastException {
			Double aUtilization = a.getForecastedMIPSWithMigrations();
			Double bUtilization = b.getForecastedMIPSWithMigrations();
			int percentage = bUtilization.compareTo(aUtilization);	//descending
			
			return percentage;
		}
	};
	
	public List<Map<String, Object>> getNewVmPlacement(
			HashMap<Integer, FutureVm> vmsToMigrate,
			HashMap<String, Set<FutureHost>> futureHostClassified) {
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		List<FutureVm> vmsToMigrateList = new ArrayList<>(vmsToMigrate.values());
		Collections.sort(vmsToMigrateList, VmComparator);	// sort VMs with custom comparator, otherwise it would be the same as in the superclass
		
		for (FutureVm vm : vmsToMigrateList) {
			Set<FutureHost> validHosts = new HashSet<>(futureHostClassified.get("valid"));
			validHosts.remove(vm.getFutureHost());
			FutureHost allocatedHost = findHostForVm(vm, validHosts);
			if (allocatedHost != null) {
				allocatedHost.moveVm(vm);
				Log.printLine("VM #" + vm.getCurrentVm().getId() + " allocated to host #" + allocatedHost.getCurrentHost().getId());

				Map<String, Object> migrate = new HashMap<String, Object>();
				migrate.put("vm", vm.getCurrentVm());
				migrate.put("host", allocatedHost.getCurrentHost());
				migrationMap.add(migrate);
			}
		}
		return migrationMap;
	}
	
	@Override
	public FutureHost findHostForVm(FutureVm vm, Set<FutureHost> specificValidHosts) {
		List<FutureHost> futureHosts = new ArrayList<>(specificValidHosts);
		Collections.sort(futureHosts, HostComparator);	//sort the hosts too, then do a simple ffd
		for (FutureHost host : futureHosts) {
			if (host.isSuitableForVm(vm)) {
				if (host.getForecastedMIPSWithMigrations() != 0 &&
						this.vmOptimizerPolicy.futureHostOverSaturationPolicy.getOverSaturationDataAfterAllocation(host, vm).existUpperThresholdSaturation) {
						continue;
				}

				return host;
			}
		}
		return null;
	}
}
