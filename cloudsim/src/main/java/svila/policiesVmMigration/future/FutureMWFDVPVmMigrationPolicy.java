package svila.policiesVmMigration.future;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.lists.PowerVmList;

import svila.policiesHostUnderUtilisation.future.FutureVmList;
import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;

public class FutureMWFDVPVmMigrationPolicy extends FutureVmMigrationPolicyBase {
	public FutureMWFDVPVmMigrationPolicy() {
	}
	
	@Override
	public List<Map<String, Object>> getNewVmPlacement(HashMap<Integer, FutureVm> vmsToMigrate,
			HashMap<String, Set<FutureHost>> futureHostClassified) {
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		List<FutureVm> vmsToMigrateList = new ArrayList<>(vmsToMigrate.values());

		FutureVmList.sortByCpuUtilization(vmsToMigrateList);

		for (FutureVm vm : vmsToMigrateList) {
			Set<FutureHost> validHosts = new HashSet<>(futureHostClassified.get("valid"));
			validHosts.remove(vm.getFutureHost());
			FutureHost allocatedHost = findHostForVm(vm, validHosts);
			if (allocatedHost != null) {
				allocatedHost.moveVm(vm);
				Log.printLine("VM #" + vm.getCurrentVm().getId() + " allocated to host #" + allocatedHost.getCurrentHost().getId());

				Map<String, Object> migrate = new HashMap<String, Object>();
				migrate.put("vm", vm.getCurrentVm());
				migrate.put("host", allocatedHost.getCurrentHost());
				migrationMap.add(migrate);
			}
		}
		return migrationMap;
	}


	@Override
	public FutureHost findHostForVm(FutureVm vm, Set<FutureHost> specificValidHosts) {
		double maxPower = Double.MIN_VALUE;
		FutureHost allocatedHost = null;

		for (FutureHost host : specificValidHosts) {
			if (host.isSuitableForVm(vm)) {
				if (host.getForecastedPercCPUWithMigrations() != 0 &&
						this.vmOptimizerPolicy.futureHostOverSaturationPolicy.getOverSaturationDataAfterAllocation(host, vm).existUpperThresholdSaturation) {
						continue;
					}	//is this really necessary? (comment from original code)

				try {
					double powerAfterAllocation = this.getVmOptimizerPolicy().futureHostOverSaturationPolicy.getPowerAfterAllocation(host, vm);
					if (powerAfterAllocation != -1) {
						double powerDiff = powerAfterAllocation - host.getPower();
						if (powerDiff > maxPower) {
							maxPower = powerDiff;
							allocatedHost = host;
						}
					}
				} catch (Exception e) {
				}
			}
		}
		return allocatedHost;
	}
}
