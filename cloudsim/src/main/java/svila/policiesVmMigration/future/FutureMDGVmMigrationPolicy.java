package svila.policiesVmMigration.future;

import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.SvilaPrinter;
import svila.planetlabNetwork.TopologyHostDistanceManager;
import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;

public class FutureMDGVmMigrationPolicy extends FutureDefaultVMMigrationPolicy {

	@Override
	public FutureHost findHostForVm(FutureVm vm, Set<FutureHost> specificValidHosts) {
		//System.out.println("Searching host for VM " + vm.getId());
		double minPower = Double.MAX_VALUE;
		FutureHost allocatedHost = null;

		//Integer lastHostNetId = vm.lastHostUntilDestroy.getNetworkId();
		Integer lastHostNetId = vm.getCurrentVm().getHost().getNetworkId();

		TreeMap<Integer, List<PowerHost>> otherHosts = TopologyHostDistanceManager.getHostsGroupedByDistance(lastHostNetId);
		
		
		for(List<PowerHost> list : otherHosts.values()) {			
			for(PowerHost host : list) {
				FutureHost futureHost = this.vmOptimizerPolicy.futureHosts.get(host.getNetworkId());
				
				if(!specificValidHosts.contains(futureHost)) {
					continue;
				}

				if (futureHost.isSuitableForVm(vm)) {
					if (futureHost.getForecastedMIPSWithMigrations() != 0 && this.vmOptimizerPolicy.futureHostOverSaturationPolicy.getOverSaturationDataAfterAllocation(futureHost, vm).existUpperThresholdSaturation) {
						continue;
					}

					try {
						double powerAfterAllocation = this.vmOptimizerPolicy.futureHostOverSaturationPolicy.getPowerAfterAllocation(futureHost, vm);
						if (powerAfterAllocation != -1) {
							double powerDiff = powerAfterAllocation - host.getPower();
							if (powerDiff < minPower) {
								minPower = powerDiff;
								allocatedHost = futureHost;
							}
						}
					} catch (Exception e) {
					}
				}
				// End of original code
			}
			
			if(allocatedHost != null) { // If we allocated the host in one group, stop searching other groups
				break;
			}
		}
		
		if(allocatedHost == null) {
			return null;
		}
		
		SvilaPrinter.print("Allocated host: " + allocatedHost.getCurrentHost().getId());
		
		return allocatedHost;
	}
}
