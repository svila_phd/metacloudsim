package svila.policiesVmMigration.future.markov;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.util.MathUtil;

import svila.planetlabNetwork.StaticResources;
import svila.policiesHostOverSaturation.future.OverSaturationData;
import svila.policiesVmMigration.future.FutureDefaultVMMigrationPolicy;
import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;

// Paper: Markov Prediction Model for Host Load Detection and VM Placement in Live Migration
// Markov Power Aware Best Fit Decreasing (MPABFD)

public class FutureMPABFDVmMigrationPolicy extends FutureDefaultVMMigrationPolicy {
	
	//double upperThreshold = 0.9; // determined  dinamically by MAD
	double lowerThreshold = 0.1;
	
	public double getHostUtilizationMad(FutureHost host) throws IllegalArgumentException {
		double[] data = host.getCPUPercHistoryWithFutureVMs0OldNNew();
		// Changed countNonZeroBeginning to countNonZeroEnding due to data is reversed
		if (MathUtil.countNonZeroEnding(data) >= 10) { // Markov paper uses 10
			return MathUtil.mad(data);
		}
		throw new IllegalArgumentException();
	}
	
	public OverSaturationData getOverSaturationDataMAD(FutureHost host) {
		double upperThreshold = 0;
		double safetyParameter = Double.parseDouble(StaticResources.getCE().getParameter());
		try {
    		upperThreshold = 1 - safetyParameter * getHostUtilizationMad(host);
		} catch (IllegalArgumentException e) {
			return this.vmOptimizerPolicy.futureHostOverSaturationPolicy.getFallbackVmAllocationPolicy().getOverSaturationData(host);
		}
		
		double utilization = host.getForecastedPercCPUWithMigrations();
		OverSaturationData osd = new OverSaturationData(host, upperThreshold, utilization);
		return osd;
	}
	
	public OverSaturationData getOverSaturationDataMADWithNewVm(FutureHost host, FutureVm vm) {
		host.putVm(vm);
		OverSaturationData osd = getOverSaturationDataMAD(host);
		host.clearPutVm(vm);
		return osd;
	}
	
	/*private boolean isHostInOversaturatedStateAfterVmAllocation(FutureHost host, FutureVm vm) {
		
		/*
		double[] mipsHistory = new double[PowerVm.HISTORY_LENGTH];
		// Host history
		for(FutureVm currentVm : host.getFutureVms().values()) {
			double[] currentHistory = currentVm.getOriginalCPUPercHistory(); // index 0: newest value
			double totalMips = currentVm.getCurrentVm().getMips();
			for(int i=0; i<currentHistory.length; i++) {
				mipsHistory[i] = currentHistory[i] * totalMips;
			}
			
		}
		mipsHistory = MathUtil.trimZeroTail(mipsHistory);
		
		double hostTotalMips = host.getCurrentHost().getTotalMips();
		double[] percCPUHistory = new double[mipsHistory.length];
		for(int i=0; i<mipsHistory.length; i++) {
			percCPUHistory[i] = mipsHistory[i] / hostTotalMips;
		}*/
/*	
		host.putVm(vm);
		double[] percHistory = host.getCPUPercHistoryWithFutureVMs0OldNNew();
		
		// (MAD) Upper threshold
		OverSaturationData osd = getOverSaturationDataMAD(host);
		double upperThreshold = osd.upperThreshold;
		
		// get host future state
		MarkovStates ms = new MarkovStates(percHistory, upperThreshold, 0.1);
		MarkovState nextMostProbablestate = ms.getFutureMarkovState();
		
		host.clearPutVm(vm);
		
		return nextMostProbablestate == MarkovState.O;
	}*/
	
	@Override
	public FutureHost findHostForVm(FutureVm vm, Set<FutureHost> specificValidHosts) {
		
		/*if(this.vmOptimizerPolicy.datacenterAnalyzer.index > 40) {
			System.out.println();
		}*/
		
		double minPower = Double.MAX_VALUE;
		FutureHost allocatedHost = null;
		List<FutureHost> tempHostList1 = new ArrayList<>();
		List<FutureHost> tempHostList2 = new ArrayList<>();
		
		for(FutureHost host : specificValidHosts) {
			/*
			// -------------------------------------------------------------------------------
			double[] hostPercHistory = host.getCPUPercHistoryWithFutureVMs0OldNNew();
			OverSaturationData osd_debug = getOverSaturationDataMADWithNewVm(host, vm);
			MarkovStates ms = new MarkovStates(hostPercHistory, osd_debug.upperThreshold, 0.1);
			//MarkovState nextMostProbableState = ms.getFutureMarkovState();
			ms.showStates();
			ms.showAllProbabilities();
			// -------------------------------------------------------------------------------
			*/
			// check host can allocate VM
			boolean hostCanAllocateVm = host.isSuitableForVm(vm);
			if(!hostCanAllocateVm) {
				continue;
			}
			
			// host is active ¿?¿?
			// Not implemented, no sense

			// host after allocation is not marked as oversaturated
			OverSaturationData osd = getOverSaturationDataMADWithNewVm(host, vm);
			boolean isHostSaturatedAfterAllocation = osd.existUpperThresholdSaturation;
			
			if(hostCanAllocateVm && !isHostSaturatedAfterAllocation) {
				tempHostList1.add(host);
			}
		}
		
		for(FutureHost host : tempHostList1) {
			double[] hostPercHistory = host.getCPUPercHistoryWithFutureVMs0OldNNew();
			
			OverSaturationData osd = getOverSaturationDataMADWithNewVm(host, vm);
			MarkovStates ms = new MarkovStates(hostPercHistory, osd.upperThreshold, 0.1);
			MarkovState nextMostProbableState = ms.getFutureMarkovState();
			//ms.showStates();
			//ms.showAllProbabilities();
			if(nextMostProbableState.equals(MarkovState.U) || nextMostProbableState.equals(MarkovState.N)) {
				tempHostList2.add(host);
			}
		}
		
		for(FutureHost host : tempHostList2) {
			host.putVm(vm);
			double power = host.getPower();
			host.clearPutVm(vm);
			if(power < minPower) {
				minPower = power;
				allocatedHost = host;
			}
		}
		
		return allocatedHost;
	}
}
