package svila.policiesVmMigration.future.markov;

import java.util.HashMap;

public class MarkovStates {	
	private double[] array;
	private MarkovState[] statesArray;
	private double upperThreshold;
	private double lowerThreshold;
	private HashMap<MarkovState, HashMap<MarkovState, Double>> transitionsCount;
	private HashMap<MarkovState, HashMap<MarkovState, Double>> probabilities;
	
	public MarkovStates(double[] array, double upperThreshold, double lowerThreshold) {
		this.array = array;
		this.upperThreshold = upperThreshold;
		this.lowerThreshold = lowerThreshold;
		this.statesArray = getStatesArray();
		transitionsCount = new HashMap<>();
		probabilities = new HashMap<>();
		generateTransitionMatrix();
	}
	
	public void showStates() {
		System.out.println("States: ");
		for(MarkovState state : statesArray) {
			System.out.print(state.name() + " ");
			//if(state.equals(MarkovState.O)) {
			//	System.out.println();
			//}
		}
		System.out.println();
	}
	
	private MarkovState[] getStatesArray() {
		MarkovState[] states = new MarkovState[array.length];
		double currentValue;
		for(int i=0; i<states.length; i++) {
			currentValue = array[i];
			if(currentValue > upperThreshold) {
				states[i] = MarkovState.O;
			} else if(currentValue < lowerThreshold) {
				states[i] = MarkovState.U;
			} else {
				states[i] = MarkovState.N;
			}
		}
		return states;
	}
	
	private void generateTransitionMatrix() {
		generateStates();
		addTransitions();
		addProbabilities();
	}
	
	private void generateStates() {
		MarkovState[] states = {MarkovState.U, MarkovState.N, MarkovState.O};
		for(MarkovState from : states) {
			transitionsCount.put(from, new HashMap<>());
			probabilities.put(from, new HashMap<>());
			for(MarkovState to : states) {
				transitionsCount.get(from).put(to, 0.0);
				probabilities.get(from).put(to, 0.0);
			}
		}
	}
	
	private void addTransitions() {
		MarkovState first = statesArray[0];
		MarkovState second;
		double value;
		for(int i=1; i<array.length; i++) {
			second = statesArray[i];
			value = transitionsCount.get(first).get(second);
			transitionsCount.get(first).put(second, value + 1);
			first = second;
		}
	}
	
	private double getFromSum(MarkovState from) {
		double sum = 0.0;
		for(Double value : transitionsCount.get(from).values()) {
			sum += value;
		}
		return sum;
	}
	
	private void addProbabilities() {
		MarkovState[] states = {MarkovState.U, MarkovState.N, MarkovState.O};
		double fromSum, currentWeight;
		for(MarkovState from : states) {
			fromSum = getFromSum(from);
			if(fromSum == 0.0) {
				continue;
			}
			
			for(MarkovState to : states) {
				currentWeight = getTransitionCount(from, to);
				if(currentWeight != 0.0) {
					probabilities.get(from).put(to, currentWeight/fromSum);
				}
			}
		}
	}
	
	public double getTransitionCount(MarkovState from, MarkovState to) {
		return transitionsCount.get(from).get(to);
	}
	
	public double getP(MarkovState from, MarkovState to) {
		return probabilities.get(from).get(to);
	}
	
	public void showAllProbabilities() {
		MarkovState[] states = {MarkovState.U, MarkovState.N, MarkovState.O};
		System.out.println("Probabilities:");
		for(MarkovState from : states) {
			for(MarkovState to : states) {
				System.out.println("P(" + from.name() + ", " + to.name() + "): " + getP(from, to));
			}
		}
	}
	
	public MarkovState getFutureMarkovState() {
		MarkovState[] states = {MarkovState.U, MarkovState.N, MarkovState.O};
		MarkovState lastState = getLastState();
		MarkovState nextMostProbableState = null;
		double maxProbability = -1;
		for(MarkovState nextState : states) {
			double prob = getP(lastState, nextState);
			//System.out.println("P(" + lastState.name() + ", " + nextState.name() + "): " + prob);
			if(prob > maxProbability) {
				nextMostProbableState = nextState;
				maxProbability = prob;
			}
		}
		//System.out.println("Result: P(" + lastState.name() + ", " + nextMostProbableState.name() + "): " + maxProbability);
		return nextMostProbableState;
	}
	
	public MarkovState getLastState() {
		return statesArray[statesArray.length-1];
	}
}
