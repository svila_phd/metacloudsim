package svila.policiesVmMigration.future;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.policiesHostOverSaturation.future.OverSaturationData;
import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;

public class FutureDemandRiskVmMigrationPolicy extends FutureVmMigrationPolicyBase {
	public FutureDemandRiskVmMigrationPolicy() {
	}
	
	public Comparator<FutureVm> VmComparator = new Comparator<FutureVm>()
	{
		@Override
		public int compare(FutureVm a, FutureVm b) throws ClassCastException {
			Double aUtilization = a.getForecastedPercCPU();
			Double bUtilization = b.getForecastedPercCPU();
			return bUtilization.compareTo(aUtilization);
		}
	};
	
	
	double UDScore(double A, double B, double delta, FutureHost h) //unsatisfied demand
	{
		return (h.getForecastedPercCPUWithMigrations() - delta) / delta;
	}
	
	double DRScore(double A, double B, double delta, FutureHost h, FutureVm V)
	{
		
		return A*UDScore(A,B,delta,h)+B*UDScore(A,B,delta,h);
	}

	public List<Map<String, Object>> getNewVmPlacement(
			HashMap<Integer, FutureVm> vmsToMigrate,
			HashMap<String, Set<FutureHost>> futureHostClassified) {
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		
		List<FutureVm> vmsToMigrateList = new ArrayList<>(vmsToMigrate.values());
		
		Collections.sort(vmsToMigrateList, VmComparator);	// sort VMs with custom comparator, otherwise it would be the same as in the superclass
		Set<FutureHost> validHosts = futureHostClassified.get("valid");
		for (FutureVm vm : vmsToMigrateList) {
			FutureHost allocatedHost = findHostForVm(vm, validHosts);
			if (allocatedHost != null) {
				allocatedHost.moveVm(vm);
				Log.printLine("VM #" + vm.getCurrentVm().getId() + " allocated to host #" + allocatedHost.getCurrentHost().getId());

				Map<String, Object> migrate = new HashMap<String, Object>();
				migrate.put("vm", vm.getCurrentVm());
				migrate.put("host", allocatedHost.getCurrentHost());
				migrationMap.add(migrate);
			}
		}
		return migrationMap;
	}
	
	@Override
	public FutureHost findHostForVm(FutureVm vm, Set<FutureHost> specificValidHosts) {
		 double minDRScore = Double.MAX_VALUE;
         FutureHost allocatedHost = null;

         for (FutureHost host : specificValidHosts) {
        	 if (host.isSuitableForVm(vm)) {
        		 OverSaturationData osdAfterAllocation = this.getVmOptimizerPolicy().futureHostOverSaturationPolicy.getOverSaturationDataAfterAllocation(host, vm);
        		 if (host.getForecastedMIPSWithMigrations() != 0 && osdAfterAllocation.existUpperThresholdSaturation) {
        			 continue;
        		 }

        		 try {
        			 double powerAfterAllocation = this.getVmOptimizerPolicy().futureHostOverSaturationPolicy.getPowerAfterAllocation(host, vm);
                     if (powerAfterAllocation != -1) {
                    	 double score = DRScore(10,1,0.9,host,vm);
                         if (score < minDRScore) {
                        	 minDRScore = score;
                             allocatedHost = host;
                         }
                     }
                 } catch (Exception e) {
                 }
        	 }
         }
         return allocatedHost;
	}
}
