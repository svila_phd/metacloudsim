package svila.policiesVmMigration.future;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVm;

import svila.policiesHostOverSaturation.future.OverSaturationData;
import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureVm;
import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public abstract class FutureVmMigrationPolicyBase {
	
	protected FutureVMOptimizerPolicy vmOptimizerPolicy;
	
	public FutureVMOptimizerPolicy getVmOptimizerPolicy() {
		return vmOptimizerPolicy;
	}

	public void setVmOptimizerPolicy(FutureVMOptimizerPolicy vmOptimizerPolicy) {
		this.vmOptimizerPolicy = vmOptimizerPolicy;
	}
	
	public FutureHost findHostForVm(FutureVm futureVm) {
		Vm vm = futureVm.getCurrentVm();
		if(vm.hasPreAssignedHost) {
			vm.hasPreAssignedHost = false;
			return this.vmOptimizerPolicy.futureHosts.get(vm.preAssignedHost.getNetworkId());
		}
		
		Set<FutureHost> specificValidHosts =  new HashSet<>();
		specificValidHosts.addAll(this.vmOptimizerPolicy.futureHostClassified.get("valid"));
		if (futureVm.getFutureHost() != null) {
			specificValidHosts.remove(futureVm.getFutureHost());
		}
		
		return findHostForVm(futureVm, specificValidHosts);
	}
		
	public abstract List<Map<String, Object>> getNewVmPlacement(HashMap<Integer, FutureVm> vmsToMigrate, HashMap<String, Set<FutureHost>> futureHostClassified);
	
	public FutureHost findHostForVm(FutureVm vm, Set<FutureHost> specificValidHosts) {
		//System.out.println("Searching host for VM " + vm.getId());
		double minPower = Double.MAX_VALUE;
		FutureHost allocatedHost = null;

		for (FutureHost host : specificValidHosts) {
			if (host.isSuitableForVm(vm)) {
				OverSaturationData osd = this.vmOptimizerPolicy.futureHostOverSaturationPolicy.getOverSaturationDataAfterAllocation(host, vm);
				if (host.getForecastedMIPS() != 0 && osd.existUpperThresholdSaturation) {
					continue;
				}

				try {
					double powerAfterAllocation = this.vmOptimizerPolicy.futureHostOverSaturationPolicy.getPowerAfterAllocation(host, vm);
					if (powerAfterAllocation != -1) {
						double powerDiff = powerAfterAllocation - host.getCurrentHost().getPower();
						if (powerDiff < minPower) {
							minPower = powerDiff;
							allocatedHost = host;
						}
					}
				} catch (Exception e) {
				}
			}
		}
		return allocatedHost;
	}
	
	public boolean allocateHostForVm(FutureVm vm) {
		return allocateHostForVm(vm, findHostForVm(vm));
	}

	public boolean allocateHostForVm(FutureVm futureVm, FutureHost futureHost) {
		Vm vm = futureVm.getCurrentVm();
		Host host = futureHost.getCurrentHost();
		if (host == null) {
			Log.formatLine("%.2f: No suitable host found for VM #" + vm.getId() + "\n", CloudSim.clock());
			return false;
		}
		if (host.vmCreate(vm)) { // if vm has been succesfully created in the host
			this.vmOptimizerPolicy.getVmTable().put(vm.getUid(), host);
			Log.formatLine(
					"%.2f: VM #" + vm.getId() + " has been allocated to the host #" + host.getId(),
					CloudSim.clock());
			return true;
		}
		Log.formatLine(
				"%.2f: Creation of VM #" + vm.getId() + " on the host #" + host.getId() + " failed\n",
				CloudSim.clock());
		return false;
	}
}
