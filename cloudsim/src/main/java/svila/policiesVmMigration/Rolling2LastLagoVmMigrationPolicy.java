package svila.policiesVmMigration;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

public class Rolling2LastLagoVmMigrationPolicy extends Rolling2LastVmMigrationPolicy {
	@Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
		List<PowerHost> lph = this.getVmOptimizerPolicy().<PowerHost> getHostList();
		Collections.sort(lph, HostComparator);	//sort the hosts too, then do a simple ffd
		
		double bestEnergyEfficiency = Double.MIN_VALUE;
	    PowerHost allocatedHost = null;
	    
	    for (PowerHost host : lph) {
			if (excludedHosts.contains(host)) {
				continue;
			}	
	    	if (host.isSuitableForVm(vm)) {
		        double utilization = this.getVmOptimizerPolicy().getMaxUtilizationAfterAllocationOriginal(host, vm);
		        if ((!vm.isBeingInstantiated()) && utilization > 0.7 || (vm.isBeingInstantiated() && utilization > 1.0)) {
		          continue;
	        }
	        
	        try {
	          double powerAfterAllocation = this.getVmOptimizerPolicy().getPowerAfterAllocationOriginal(host, vm);
	          if (powerAfterAllocation != -1) {
	            // Host can be used

	            // Calculates the power quality
	            double energyEfficiency = getHostEnergyEfficiency(host);
	            if (energyEfficiency > bestEnergyEfficiency) {
	              bestEnergyEfficiency = energyEfficiency;
	              allocatedHost = host;
	            } else if (energyEfficiency == bestEnergyEfficiency) {
	              // Tie break

	              // The lesser power consumption with the VMs instantiated at datacenter will be chosen
	              double power_vm_allocated_at_host = allocatedHost.getPower() + this.getVmOptimizerPolicy().getPowerAfterAllocationOriginal(host, vm);
	              double power_vm_allocated_at_allocatedHost = host.getPower() + this.getVmOptimizerPolicy().getPowerAfterAllocationOriginal(allocatedHost, vm);
	              if (power_vm_allocated_at_host < power_vm_allocated_at_allocatedHost) {
	                allocatedHost = host;
	              } else if (power_vm_allocated_at_host == power_vm_allocated_at_allocatedHost) {
	                // Calculates the best using MIPS / total MIPS to minimized vm
	                // migration
	                if (host.getUtilizationOfCpu() > allocatedHost.getUtilizationOfCpu()) {
	                  allocatedHost = host;
	                } else if (host.getUtilizationOfCpu() == allocatedHost.getUtilizationOfCpu()) {
	                  // The best processor will be allocated
	                  if (host.getTotalMips() > allocatedHost.getTotalMips()) {
	                    allocatedHost = host;
	                  }
	                }
	              }
	            }
	          }
	        } catch (Exception e) {
	        }
	      }
	    }
	    
	    return allocatedHost;
	}
	
	public static double getHostEnergyEfficiency(PowerHost host) {
	    return (host.getTotalMips() / host.getMaxPower());
	  }
	
	@Override
	public List<Map<String, Object>> getNewVmPlacement(
			List<? extends Vm> vmsToMigrate,
			Set<? extends Host> excludedHosts) {
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		
		Collections.sort(vmsToMigrate, VmComparator);	// sort VMs with custom comparator, otherwise it would be the same as in the superclass
		
		for (Vm vm : vmsToMigrate) {
			PowerHost allocatedHost = findHostForVm(vm, excludedHosts);
			if (allocatedHost != null) {
				allocatedHost.vmCreate(vm);
				Log.printLine("VM #" + vm.getId() + " allocated to host #" + allocatedHost.getId());

				Map<String, Object> migrate = new HashMap<String, Object>();
				migrate.put("vm", vm);
				migrate.put("host", allocatedHost);
				migrationMap.add(migrate);
			}
		}
		return migrationMap;
	}
}
