package svila.mesd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Sergi
 */
public class Allocation {
    
    private Application application;
    private HashMap<Task, Machine> taskAllocations;
    private double currentTime;
    private double maxDuration;
    
    public Allocation(Application application, double currentTime) {
        this.application = application;
        this.currentTime = currentTime;
    }

    public Application getApplication() {
        return application;
    }
    
    public void allocate(List<Machine> machines) {
        List<Task> tasks = application.getTasks();
        if(machines.size() < tasks.size()) {
            taskAllocations = null;
        } else {
            taskAllocations = new HashMap<>();
            for(int i=0; i<tasks.size(); i++) {
                taskAllocations.put(tasks.get(i), machines.get(i));
            }
            // No co-allocation implementation
            maxDuration = getWorstTime();
            MESDLogger.print("Correct allocation of app " + application.getId());
        }
    }
    
    public HashMap<Task, Machine> getCurrentAllocation() {
        return this.taskAllocations;
    }
    
    private double getWorstTime() {
        double worstTime = Double.MIN_VALUE;
        for(Task task : taskAllocations.keySet()) {
            double currentCompletionTime = taskAllocations.get(task).getCompletionTime(task);
            MESDLogger.print("currentCompletionTime: " + currentCompletionTime);
            if(currentCompletionTime > worstTime) {
                worstTime = currentCompletionTime;
            }
        }
        MESDLogger.print("worstTime: " + worstTime);
        return worstTime;
    }
    
    public Double getInitTime() {
        return this.currentTime;
    }

    public double getMaxDuration() {
        return this.maxDuration;
    }
    
    public double getFinishTime() {
        return this.currentTime + this.maxDuration;
    }
    
    @Override
    public String toString() {
        String s = "Application " + application.getId() + " MIPS: " + application.getMips() + 
                " Start time: " + currentTime + " End time: " + getFinishTime() +
                " Duration: " + maxDuration + " Assignation: ";
        List<String> machineIds = new ArrayList<>();
        for(Machine machine : taskAllocations.values()) {
            machineIds.add(machine.getId());
        }
        s += String.join(", ", machineIds);
        return s;
    }
}
