package svila.mesd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Sergi
 */
public class MESD {

    private double currentTime;
    private List<Machine> datacenterMachines;
    private HashMap<String, Application> applicationsMap;
    private List<Application> applications;
    private HashMap<Application, Allocation> currentAllocations;
    private HashMap<Application, Allocation> idealAllocations;
    private HashMap<Application, Allocation> finalAllocations;
    private HashMap<String, String> taskMachineRelation;
    
    public MESD(List<Machine> datacenterMachines, List<Application> applications) {
        this.currentTime = 0.0;
        this.applications = applications;
        this.applicationsMap = new HashMap<>();
        for(Application a : applications) {
            this.applicationsMap.put(a.getId(), a);
        }
        this.datacenterMachines = datacenterMachines;
        Collections.sort(datacenterMachines, (Machine t, Machine t1) ->
                                           t.getMips().compareTo(t1.getMips()));
        Collections.reverse(datacenterMachines);
        this.currentAllocations = new HashMap<>();
        this.idealAllocations = new HashMap<>();
        this.finalAllocations = new HashMap<>();
        this.taskMachineRelation = new HashMap<>();
    }
    
    public static MESD getAdaptedMESDFromHost(List<Host> hostList, List<Cloudlet> cloudletList) {
        List<Machine> dm = new ArrayList<>();
        List<Application> cl = new ArrayList<>();
        
        int i=0;
        for(Host host : hostList) {
            dm.add(new Machine(String.valueOf(host.getId()), host.getTotalMips()));
        }
        
        i=0;
        for(Cloudlet cloudlet : cloudletList) {
            cl.add(new Application(String.valueOf(cloudlet.getCloudletId()), cloudlet.getCloudletLength(), 1, 1));
        }
        
        return new MESD(dm, cl);
    }
    
    public static MESD getAdaptedMESDFromVm(List<? extends Vm> vmList, List<? extends Cloudlet> cloudletList) {
        List<Machine> dm = new ArrayList<>();
        List<Application> cl = new ArrayList<>();
        
        int i=0;
        for(Vm vm : vmList) {
            dm.add(new Machine(String.valueOf(vm.getId()), vm.getMips()));
        }
        
        i=0;
        for(Cloudlet cloudlet : cloudletList) {
            Log.printConcatLine(cloudlet.getCloudletLength());
            cl.add(new Application(String.valueOf(cloudlet.getCloudletId()), cloudlet.getCloudletLength(), 1, 1));
        }
        
        return new MESD(dm, cl);
    }
    
    public String getHostIdFromCloudletId(String cloudletId) {
        Log.printConcatLine(cloudletId + " " + taskMachineRelation.get(cloudletId));
        String s = taskMachineRelation.get(cloudletId);
        return taskMachineRelation.get(cloudletId);
    }
    
    public double getCloudletStartTime(String id) {
        return finalAllocations.get(applicationsMap.get(id)).getInitTime();
    }
    
    public void mainAlgorithm() {
        calculateIdealAllocations();
        showIdealAllocations();
        int iteration = 0;
        while(!applications.isEmpty()) {
            MESDLogger.print("Iteration: " + iteration);
            MESDLogger.print("Time: " + currentTime);
            MESDLogger.print("Nº pending applications: " + applications.size());
            iteration++;
            double minExec = Double.MAX_VALUE;
            Application selectedJob = null;
            Allocation selectedAllocation = null;
            for(Application application : applications) {
                Allocation currentAllocation = calculateAllocation(application, getAvailableMachines());
                if(currentAllocation.getCurrentAllocation() != null) {
                    MESDLogger.print("Current finish time: " + currentAllocation.getFinishTime() + " ideal finish time: " + idealAllocations.get(application).getFinishTime());
                    double time = currentAllocation.getFinishTime() - idealAllocations.get(application).getFinishTime();
                    MESDLogger.print("App " + application.getId() + " time: " + time);
                    if(time < minExec) {
                        minExec = time;
                        selectedJob = application;
                        selectedAllocation = currentAllocation;
                    }
                }
            }
         
            if(selectedJob == null) {
                MESDLogger.print("No suitable allocations");
                Allocation jj = getFirstFinishAllocation();
                MESDLogger.print("Released machines for app " + jj.getApplication().getId());
                currentTime = jj.getFinishTime();
                freeMachines(jj);
            } else {
                MESDLogger.print("Selected allocation for app " + selectedAllocation.getApplication().getId());
                finalAllocations.put(selectedJob, selectedAllocation);
                for(Machine m : selectedAllocation.getCurrentAllocation().values()) {
                    taskMachineRelation.put(selectedJob.getId(), m.getId());
                }
                applications.remove(selectedJob);
                lockMachines(selectedAllocation);
            }
        }
        
        showJSONResults();
    }
    
    public Allocation getFirstFinishAllocation() {
        double minFinishTime = Double.MAX_VALUE;
        Allocation minFinishTimeAllocation = null;
        for(Allocation allocation : currentAllocations.values()) {
            if(allocation.getFinishTime() < minFinishTime) {
                minFinishTime = allocation.getFinishTime();
                minFinishTimeAllocation = allocation;
            }
        }
        return minFinishTimeAllocation;
    }
            
    public void calculateIdealAllocations() {
        for(Application application : applications) {
            idealAllocations.put(application, calculateAllocation(application, datacenterMachines));
        }
    }
    
    public void showIdealAllocations() {
        for(Allocation allocation : idealAllocations.values()) {
            System.out.println(allocation);
        }
    }
            
    public Allocation calculateAllocation(Application application, List<Machine> machines) {
        Allocation allocation = new Allocation(application, currentTime);
        allocation.allocate(machines);
        return allocation;
    }
    
    public Machine getLessMIPSMachine(List<Machine> machines) {
        double mips = Double.MAX_VALUE;
        Machine lessMipsMachine = null;
        for(Machine machine : machines) {
            if(machine.getMips() < mips) {
                lessMipsMachine = machine;
            }
        }
        return lessMipsMachine;
    }
    
    public List<Machine> getAvailableMachines() {
        List<Machine> availableMachines = new ArrayList<>();
        for(Machine m : datacenterMachines) {
            if(m.isAvailable()) {
                availableMachines.add(m);
            }
        }
        return availableMachines;
    }
    
    public List<Machine> getAvailableMachinesWithSameMIPS(Machine machine) {
        List<Machine> availableMachines = new ArrayList<>();
        for(Machine m : datacenterMachines) {
            if(m.isAvailable() && m.getMips().equals(machine.getMips())) {
                availableMachines.add(m);
            }
        }        
        return availableMachines;
    }
    
    public void lockMachines(Allocation allocation) {
        MESDLogger.print("Lock of application " + allocation.getApplication().getId());
        currentAllocations.put(allocation.getApplication(), allocation);
        for(Machine machine : allocation.getCurrentAllocation().values()) {
            machine.setAvailable(false);
        }
    }
    
    public void freeMachines(Allocation allocation) {
        currentAllocations.remove(allocation.getApplication());
        for(Machine machine : allocation.getCurrentAllocation().values()) {
            machine.setAvailable(true);
        }
    }
    
    public void showResults() {
        List<Allocation> results = new ArrayList<>();
        results.addAll(finalAllocations.values());
        Collections.sort(results, (Allocation a, Allocation a1) ->
                                           a.getInitTime().compareTo(a1.getInitTime()));
        for(Allocation allocation : results) {
            System.out.println(allocation);
        }
    }
    
    public void showJSONResults() {
        JSONObject root = new JSONObject();

        JSONArray taskAllocations = new JSONArray();
        for(Allocation allocation : finalAllocations.values()) {
            String applicationId = allocation.getApplication().getId();
            for(Machine m : allocation.getCurrentAllocation().values()) {
                JSONObject task = new JSONObject();
                task.put("Application", applicationId);
                task.put("Machine", m.getId());
                task.put("StartTime", allocation.getInitTime());
                task.put("EndTime", allocation.getFinishTime());
                taskAllocations.put(task);
            }
        }
        root.put("tasks", taskAllocations);
        
        JSONArray ticks = new JSONArray();
        Set<Double> times = new HashSet<>();

        for(Allocation allocation : finalAllocations.values()) {
            for(Machine m : allocation.getCurrentAllocation().values()) {
                times.add(allocation.getInitTime());
                times.add(allocation.getFinishTime());
                break;
            }
        }
        
        for(Double time : times) {
            JSONObject tick = new JSONObject();
            tick.put("time", Double.parseDouble(String.format(Locale.ROOT, "%.2f", time)));
            ticks.put(tick);
        }

        root.put("ticks", ticks);
        
        System.out.println(root);
    }
}
