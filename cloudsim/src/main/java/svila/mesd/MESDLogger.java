package svila.mesd;

/**
 *
 * @author Sergi
 */
public class MESDLogger {
    
    static boolean enabled = false;
    
    public static void print(String string) {
        if(enabled) {
            System.out.println(string);
        }
    }
}
