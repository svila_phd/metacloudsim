package svila.mesd;

/**
 *
 * @author Sergi
 */

public class Task {
        
    private String id;
    private double mips;
    private double procRatio;
        
    public Task(String id, double mips, double procRatio) {
        this.id = id;
        this.mips = mips;
        this.procRatio = procRatio;
    }

    public String getId() {
        return id;
    }

    public double getMips() {
        return mips;
    }
    
    public double getProcRatio() {
        return procRatio;
    }
    
    public double getRealMips() {
        return mips * procRatio;
    }
}
