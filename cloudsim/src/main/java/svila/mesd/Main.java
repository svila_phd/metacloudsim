package svila.mesd;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sergi
 */
public class Main {
    public static void main(String[] args) {
        List<Machine> machines = new ArrayList<>();
        machines.add(new Machine("1", 75));
        machines.add(new Machine("2", 75));
        machines.add(new Machine("3", 50));
        machines.add(new Machine("4", 25));
        machines.add(new Machine("5", 15));

        List<Application> applications = new ArrayList<>();
        applications.add(new Application("1", 100, 1, 2));
        applications.add(new Application("2", 50, 1, 3));
        applications.add(new Application("3", 75, 1, 2));
        applications.add(new Application("4", 100, 1, 2));

        MESD mesd = new MESD(machines, applications);
        mesd.mainAlgorithm();
        mesd.showResults();
        mesd.showJSONResults();
    }
}
