package svila.mesd;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sergi
 */
public class Application {
        private String id;
        private double mips;
        private double procRatio;
        private boolean completed;
        private List<Task> tasks;
        
        public Application(String id, double mips, double procRatio, int numTasks) {
            this.id = id;
            this.mips = mips;
            this.procRatio = procRatio;
            this.completed = false;
            tasks = new ArrayList<>();
            for(int i=0; i<numTasks; i++) {
                tasks.add(new Task(id+String.valueOf(i), mips, procRatio));
            }
        }
        
        public String getId() {
            return id;
        }
        
        public double getMips() {
            return mips;
        }
        
        public List<Task> getTasks() {
            return tasks;
        }
        
        public double getProcRatio() {
            return procRatio;
        }
        
        public void setCompleted(boolean completed) {
            this.completed = completed;
        }
        
        public boolean isCompleted() {
            return completed;
        }
        
        @Override
        public String toString() {
            return "Application " + id;
        }
}
