package svila.mesd;

/**
 *
 * @author Sergi
 */
public class Machine {
        private String id;
        private double mips;
        private boolean available;
        
        public Machine(String id, double mips) {
            this.id = id;
            this.mips = mips;
            available = true;
        }
        
        public double getCompletionTime(Application application) {
            return application.getMips()*application.getProcRatio() / mips;
        }
        
        public double getCompletionTime(Task task) {
            MESDLogger.print("Machine mips: " + mips + " Task mips: " + task.getRealMips() + " Time: " + task.getMips() / mips);
            return task.getRealMips() / mips;
        }
        
        public void setAvailable(boolean available) {
            this.available = available;
        }
        
        public boolean isAvailable() {
            return available;
        }

        public String getId() {
            return id;
        }

        public Double getMips() {
            return mips;
        }
    }
