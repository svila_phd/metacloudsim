package svila;

import java.util.Iterator;
import org.json.JSONObject;

/**
 *
 * @author Sergi
 */
public class ExperimentReader {
    
    private JSONObject root;
    
    public ExperimentReader(JSONObject root) {
        this.root = root;
    }
    
    public void show() {
        Iterator it = root.getJSONArray("Experiment").iterator();
        while(it.hasNext()) {
            JSONObject currentEvent = (JSONObject)it.next();
            System.out.println(currentEvent);
        }
    }
}
