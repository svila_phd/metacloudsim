package svila;

import org.cloudbus.cloudsim.power.models.PowerModel;
import org.cloudbus.cloudsim.power.models.PowerModelSpecPowerHpProLiantMl110G4Xeon3040;
import org.cloudbus.cloudsim.power.models.PowerModelSpecPowerHpProLiantMl110G5Xeon3075;
import svila.CustomConfiguration;

/**
 *
 * @author Sergi
 */
public class CCE1 extends CustomConfiguration {
    
    public CCE1() {
        numVMs = 2;
        numHosts = 2;
        cloudletUtilizationSeed = -1;

        enableOutput = true;
        outputCSV    = false;

        //schedulingInterval = 300;
        //simulationLimit = 24 * 60 * 60;
        schedulingInterval = 100;
        simulationLimit = 1000;

        cloudletLength	= 2500 * (int) simulationLimit;
        numCloudletPEs	= 1;

        vmTypes	= 4;
        vmMips	= new int[] { 2500, 2000, 1000, 500 };
        vmPEs	= new int[] { 1, 1, 1, 1 };
        vmRAM	= new int[] { 870,  1740, 1740, 613 };
        vmBW		= 100000;
        vmSize		= 2500;

        numHostTypes	 = 2;
        hostMIPS	 = new int[] { 500, 2660 };
        hostPEs	 = new int[] { 1, 1 };
        hostRAM	 = new int[] { 4096, 4096 };
        hostBW		 = 1000000; 
        hostStorage = 1000000;

        lowerThreshold = 0.3;
        upperThreshold = 0.7;
        
        cloudlet1path = "C:\\Users\\Sergi\\Documents\\cloudsim4.0\\modules\\resources\\basic\\cloudlet1.txt";
        cloudlet2path = "C:\\Users\\Sergi\\Documents\\cloudsim4.0\\modules\\resources\\basic\\cloudlet2.txt";
        cloudletFilepaths = new String[] {
                            cloudlet1path,
                            cloudlet2path
        };
        }
}
