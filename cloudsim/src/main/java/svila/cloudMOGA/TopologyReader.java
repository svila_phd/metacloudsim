package svila.cloudMOGA;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

public class TopologyReader {
	
	private static List<HostWorkload> hostWorkloadList;
	private static List<Vm> vmList;
	private static List<Host> hostList;
	private static Map<Vm, Host> vmHostMap;
	
	public static void init() {
		hostWorkloadList = new ArrayList<>();
		vmList = new ArrayList<>();
		hostList = new ArrayList<>();
		vmHostMap = new HashMap<>();
	}
	
	public static void readTopology(String filepath) {
		BufferedReader br = null;
		FileReader fr = null;
		
		try{
			fr = new FileReader(filepath);
			br = new BufferedReader(fr);
			String currentLine;
			String[] splitLine;
			
			while((currentLine = br.readLine()) != null) {
				if(currentLine.charAt(0) == ';') {
					continue;
				}
				splitLine = currentLine.trim().split("\\s+");
				hostWorkloadList.add(new HostWorkload(Integer.parseInt(splitLine[0]),
													  Integer.parseInt(splitLine[1]),
													  Integer.parseInt(splitLine[2]),
													  Integer.parseInt(splitLine[4]),
													  Integer.parseInt(splitLine[5]),
													  Integer.parseInt(splitLine[8])));
			}
			
			TopologyCreator tc = TopologyCreator.getInstance();
			for(HostWorkload hw : hostWorkloadList) {
				Host host = tc.createHost(hw.getNumMachines() * hw.getPEperMachine(), hw.getNumMachines() * hw.getMIPSperMachine());
				hostList.add(host);
				for(int i=0; i<hw.getNumMachines(); i++) {
					Vm vm = tc.createVm(hw.getMIPSperMachine(), hw.getPEperMachine());
					vmList.add(vm);
					vmHostMap.put(vm, host);
				}
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}
		}
	}
	
	public static List<Host> getHosts() {
		return hostList;
	}
	
	public static List<Vm> getVms() {
		return vmList;
	}
	
	public static Map<Vm, Host> getVmHostMap() {
		return vmHostMap;
	}
	
	public static int getMaxPE() {
		int maxPE = 0;
		for(Vm vm : vmList) {
			if(vm.getNumberOfPes() > maxPE) {
				maxPE = vm.getNumberOfPes();
			}
		}
		
		return maxPE;
	}
}
