package svila.cloudMOGA;

public class HostWorkload {
	private int id, numMachines, MIPSperMachine, idlePower, fullPower, PEperMachine;
	
	public HostWorkload(int id, int numMachines, int mIPSperMachine, int idlePower, int fullPower, int PEperMachine) {
		this.id = id;
		this.numMachines = numMachines;
		MIPSperMachine = mIPSperMachine;
		this.idlePower = idlePower;
		this.fullPower = fullPower;
		this.PEperMachine = PEperMachine;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumMachines() {
		return numMachines;
	}

	public void setNumMachines(int numMachines) {
		this.numMachines = numMachines;
	}

	public int getMIPSperMachine() {
		return MIPSperMachine;
	}

	public void setMIPSperMachine(int mIPSperMachine) {
		MIPSperMachine = mIPSperMachine;
	}

	public int getIdlePower() {
		return idlePower;
	}

	public void setIdlePower(int idlePower) {
		this.idlePower = idlePower;
	}

	public int getFullPower() {
		return fullPower;
	}

	public void setFullPower(int fullPower) {
		this.fullPower = fullPower;
	}

	public int getPEperMachine() {
		return PEperMachine;
	}

	public void setPEperMachine(int pEperMachine) {
		PEperMachine = pEperMachine;
	}
	
}
