package svila.cloudMOGA;

public class Task {
	private int id, mi, nPes;
	
	public Task(int id, int mi, int nPes) {
		this.id = id;
		this.mi = mi;
		this.nPes = nPes;
	}

	public int getId() {
		return id;
	}

	public int getMi() {
		return mi;
	}

	public int getnPes() {
		return nPes;
	}
	
}
