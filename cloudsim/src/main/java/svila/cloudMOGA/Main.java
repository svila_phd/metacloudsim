package svila.cloudMOGA;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.examples.power.Constants;
import org.cloudbus.cloudsim.examples.power.Helper;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerDatacenterBroker;

public class Main {

	public static void main(String[] args) {
		
		Path topologyRootPath = FileSystems.getDefault().getPath("C:\\Users\\Sergi\\workspace2\\workloads\\CEA-CURIE\\work200_0");
		Path topologyPath = FileSystems.getDefault().getPath(topologyRootPath.toString(), "input", "environment.smcf");
		
		Path workloadPath = FileSystems.getDefault().getPath("C:\\Users\\Sergi\\workspace2\\workloads\\CEA-CURIE\\work200_0");
		Path tasksPath = FileSystems.getDefault().getPath(workloadPath.toString(), "input", "workload.swfM");
		//String[] simArgs = new String[] { "C:\\Users\\Sergi\\workspace2\\workloads\\CEA-CURIE\\work200_0" };
		//checkFolders(simArgs[0]);

		int num_user = 1; // number of cloud users
		Calendar calendar = Calendar.getInstance(); // Calendar whose fields have been initialized with the current date and time.
		boolean trace_flag = false; // trace events

		CloudSim.init(num_user, calendar, trace_flag);
		
		// Read the topology
		TopologyCreator tc = TopologyCreator.getInstance();
		PowerDatacenter dc = tc.createDataCenter("datacenter", topologyPath.toString());
		// Read the workloadSet algorithm (MOGA)
		WorkloadReader wr = new WorkloadReader();
		wr.readWorkload(tasksPath.toString());
		wr.filterWorkloadsMaxPEs(TopologyReader.getMaxPE());
		
		printTopology();
		printCloudlets(wr.getCloudlets());
		
		// Execute strategy
		dc.setDisableMigrations(true);
		DatacenterBroker broker = tc.getBroker();
		List<Cloudlet> cloudlets = wr.getCloudlets();
		List<Vm> vms = TopologyReader.getVms();
		bindCloudletsWithVMs(cloudlets, vms);
		broker.submitVmList(TopologyReader.getVms());
		broker.submitCloudletList(wr.getCloudlets());
		
		CloudSim.terminateSimulation(Constants.SIMULATION_LIMIT);
		
		double lastClock = CloudSim.startSimulation();

		List<Cloudlet> newList = broker.getCloudletReceivedList();
		Log.printLine("Received " + newList.size() + " cloudlets");

		CloudSim.stopSimulation();
		
		// Save results
	}
	
	public static void bindCloudletsWithVMs(List<Cloudlet> cloudlets, List<Vm> vms) {
		for(Cloudlet cloudlet : cloudlets) {
			cloudlet.setUserId(1);
			cloudlet.setVmId(vms.get(0).getId());
		}
	}
	
	public static void printTopology() {
		System.out.println("Datacenter info:");
		List<Host> hosts = TopologyReader.getHosts();
		Map<Host, List<Vm>> hostsWithVmsMap = new HashMap<>();
		for(Host host : hosts) {
			hostsWithVmsMap.put(host, new ArrayList<>());
		}
		Map<Vm, Host> vmHostMap = TopologyReader.getVmHostMap();
		Iterator<Entry<Vm, Host>> iterator = vmHostMap.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry<Vm, Host> pair = iterator.next();
			hostsWithVmsMap.get(pair.getValue()).add(pair.getKey());
		}
		
		for(Entry<Host, List<Vm>> hostData : hostsWithVmsMap.entrySet()) {
			Host currentHost = hostData.getKey();
			List<Vm> currentVmList = hostData.getValue();
			System.out.println("\tHost " + currentHost.getId() + " PEs: " + currentHost.getNumberOfPes() + " Num VMs: " + currentVmList.size());
			for(Vm vm : currentVmList) {
				System.out.println("\t\tVm " + vm.getId() + " PEs: " + vm.getNumberOfPes() + " MIPS: " + vm.getMips());	
			}
		}
	}
	
	public static void printCloudlets(List<Cloudlet> cloudlets) {
		System.out.println("Cloudlets info: " + cloudlets.size() + " cloudlets");
		for(Cloudlet cloudlet : cloudlets) {
			System.out.println("\t" + "Id: " + cloudlet.getCloudletId() +
					" Lenght: " + cloudlet.getCloudletLength() + " PEs: " + cloudlet.getNumberOfPes());
		}
	}

	public static void checkFolders(String path) {
		// TODO Auto-generated method stub
		// ------------------------------------------------
		// CHECK, VALIDATE, AND CREATE FOLDERS
		// ------------------------------------------------
		// check input folder
		String projectPath = path;
		File file = new File(projectPath);
		if (!file.isDirectory() && !file.exists()) {
			System.out.println("Input folder does not exist or is not accessible.");
			System.out.println("Exit.");
			System.exit(0);
		}
		// adjust '/' termination
		if (projectPath.charAt(projectPath.length() - 1) != '/') {
			projectPath = projectPath + "/";
		}

		System.out.println("Project folder: " + projectPath + "\n");
		// check base output path
		file = new File(projectPath + "output/");
		if (!file.exists()) {
			System.out.println("Base output directory does not exist. Creating it...");
			// if output directory does not exist, create it.
			file.mkdir();
		}
		System.out.println("Base onput folder: " + projectPath + "output/\n");
	}
}
