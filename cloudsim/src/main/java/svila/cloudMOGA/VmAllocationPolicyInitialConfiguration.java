package svila.cloudMOGA;

import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicy;

public class VmAllocationPolicyInitialConfiguration extends VmAllocationPolicy {

	private Map<Vm, Host> vmHostMap;
	
	public VmAllocationPolicyInitialConfiguration(List<? extends Host> list, Map<Vm, Host> vmHostMap) {
		super(list);
		this.vmHostMap = vmHostMap;
	}

	@Override
	public boolean allocateHostForVm(Vm vm) {
		return vmHostMap.get(vm).vmCreate(vm);
	}

	@Override
	public boolean allocateHostForVm(Vm vm, Host host) {
		return vmHostMap.get(vm).vmCreate(vm);
	}

	@Override
	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deallocateHostForVm(Vm vm) {
		vm.getHost().vmDestroy(vm);
	}

	@Override
	public Host getHost(Vm vm) {
		return vmHostMap.get(vm);
	}

	@Override
	public Host getHost(int vmId, int userId) {
		for(Vm vm : vmHostMap.keySet()) {
			if(vm.getId() == vmId) {
				return vmHostMap.get(vm);
			}
		}
		return null;
	}

}
