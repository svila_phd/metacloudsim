package svila.cloudMOGA;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

public class WorkloadReader {
	
	public List<Task> taskList;
	
	public void readWorkload(String filepath) {
		BufferedReader br = null;
		FileReader fr = null;
		
		try{
			fr = new FileReader(filepath);
			br = new BufferedReader(fr);
			String currentLine;
			String[] splitLine;
			
			while((currentLine = br.readLine()) != null) {
				if(currentLine.charAt(0) == ';') {
					continue;
				}
				splitLine = currentLine.trim().split("\\s+");
				taskList.add(new Task(Integer.parseInt(splitLine[0]),
									Integer.parseInt(splitLine[3]),
									Integer.parseInt(splitLine[4])));
			}
			System.out.println("");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}
		}
	}
	
	public void filterWorkloadsMaxPEs(int maxPEfilter) {
		Iterator<Task> iterator = taskList.iterator();
		while(iterator.hasNext()) {
			Task task = iterator.next();
			if(task.getnPes() > maxPEfilter) {
				iterator.remove();
			}
		}
	}
	
	public WorkloadReader() {
		taskList = new LinkedList<>();
	}
	
	public List<Cloudlet> getCloudlets() {
		return transformTasksToCloudlets(taskList);
	}
	
	private List<Cloudlet> transformTasksToCloudlets(List<Task> taskList) {
		TopologyCreator tc = TopologyCreator.getInstance();
		List<Cloudlet> cloudletList = new ArrayList<>();
		for(Task task : taskList) {
			cloudletList.add(tc.createCloudlet(task.getId(), task.getMi(), task.getnPes()));
		}
		return cloudletList;
	}
}
