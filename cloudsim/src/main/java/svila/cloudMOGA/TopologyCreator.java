package svila.cloudMOGA;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerSpaceShared;
import org.cloudbus.cloudsim.CloudletSchedulerTimeShared;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicySimple;
import org.cloudbus.cloudsim.VmSchedulerSpaceShared;
import org.cloudbus.cloudsim.VmSchedulerTimeShared;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerDatacenterBroker;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;

public class TopologyCreator {
	private static TopologyCreator instance = null;
	
	private static int cloudletID = 0;
	private static int VmID = 0;
	private static int hostID = 0;
	private int userID = 1;
	PowerDatacenterBroker broker;

	int defaultCloudlet_fileSize = 1;
	int defaultCloudlet_outputSize = 1;
	
	int defaultVM_RAM = 1;
	int defaultVM_BW = 1;
	int defaultVM_size = 100;
	
	int defaultHost_RAM = 1000;
	int defaultHost_BW = 1000;
	int defaultHost_size = 10000;
	
	private TopologyCreator() {
		broker = createBroker();
	}
	
	public static TopologyCreator getInstance() {
		if(instance == null) {
			instance = new TopologyCreator();
		}
		return instance;
	}
	
	public PowerDatacenterBroker getBroker() {
		return this.broker;
	}
	
	private static PowerDatacenterBroker createBroker(){

		PowerDatacenterBroker broker = null;
		try {
			broker = new PowerDatacenterBroker("Broker");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return broker;
	}
	
	public Cloudlet createCloudlet(int length, int nPes) {
		return createCloudlet(++cloudletID, length, nPes);
	}
	
	public Cloudlet createCloudlet(int id, int length, int nPes) {
		UtilizationModel utilizationModel = new UtilizationModelFull();
		Cloudlet cloudlet  = new Cloudlet(id, length, nPes,
				defaultCloudlet_fileSize, defaultCloudlet_outputSize,
				utilizationModel, utilizationModel, utilizationModel);
		cloudlet.setUserId(userID);
		return cloudlet;
	}
	
	public Vm createVm(int mips, int nPes) {
		return new Vm(++VmID, broker.getId(), mips, nPes,
				defaultVM_RAM, defaultVM_BW, defaultVM_size,
				"Xen", new CloudletSchedulerSpaceShared());
	}
	
	public Host createHost(int nPes, int mips) {
		List<Pe> peList = new ArrayList<Pe>();
		for(int i=0; i<nPes; i++) {
			peList.add(new Pe(i, new PeProvisionerSimple(mips)));
		}
		
		return new Host(++hostID, new RamProvisionerSimple(defaultHost_RAM), new BwProvisionerSimple(defaultHost_BW),
				defaultHost_size, peList, new VmSchedulerSpaceShared(peList));
	}
	
	public PowerDatacenter createDataCenter(String name, String topologyPath) {
		String arch = "x86"; // system architecture
		String os = "Linux"; // operating system
		String vmm = "Xen";
		double time_zone = 10.0; // time zone this resource located
		double cost = 3.0; // the cost of using processing in this resource
		double costPerMem = 0.05; // the cost of using memory in this resource
		double costPerStorage = 0.001; // the cost of using storage in this
										// resource
		double costPerBw = 0.0; // the cost of using bw in this resource
		LinkedList<Storage> storageList = new LinkedList<Storage>(); // we are not adding SAN
													// devices by now
		TopologyReader.init();
		TopologyReader.readTopology(topologyPath);
		List<Host> hostList = TopologyReader.getHosts();

		DatacenterCharacteristics characteristics = new DatacenterCharacteristics(
				arch, os, vmm, hostList, time_zone, cost, costPerMem,
				costPerStorage, costPerBw);

		// 6. Finally, we need to create a PowerDatacenter object.
		PowerDatacenter datacenter = null;
		try {
			datacenter = new PowerDatacenter(name, characteristics, new VmAllocationPolicyInitialConfiguration(hostList, TopologyReader.getVmHostMap()), storageList, 0);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return datacenter;
	}
}
