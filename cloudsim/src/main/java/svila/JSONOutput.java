package svila;

import com.cedarsoftware.util.io.JsonWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletScheduler;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.ResCloudlet;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.examples.power.RunnerAbstract;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerHost;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Sergi
 */
public class JSONOutput {
    
    private static JSONObject root;
    private static JSONArray array;
    private static PowerDatacenter powerDatacenter;
    
    private static String EXPERIMENT = "Experiment";
    private static String TIME = "Time";
    private static String HOSTS = "Hosts";
    private static String VMS = "VMs";
    private static String CLOUDLETS = "Cloudlets";
    private static String ID = "Id";
    
    public static void init() {
        root = new JSONObject();
        array = new JSONArray();
        root.put(EXPERIMENT, array);
    }
    
    public static void setEnvironment(PowerDatacenter powerDatacenter, List<PowerHost> powerHostList, List<Vm> vmList, List<Cloudlet> cloudletList) {
        if(root == null) {
            return;
        }
        
        JSONOutput.powerDatacenter = powerDatacenter;
        
        JSONObject joEnv = new JSONObject();

        JSONArray hostArray = new JSONArray(),
                  vmArray = new JSONArray(),
                  cloudletArray = new JSONArray();
        joEnv.put(HOSTS, hostArray);
        joEnv.put(VMS, vmArray);
        joEnv.put(CLOUDLETS, cloudletArray);
        
        for(PowerHost host : powerHostList) {
            JSONObject joHost = new JSONObject();
            joHost.put(ID, host.getId());
            hostArray.put(joHost);
        }
        
        for(Vm vm : vmList) {
            JSONObject joVM = new JSONObject();
            joVM.put(ID, vm.getId());
            vmArray.put(joVM);
        }
        
        for(Cloudlet cloudlet : cloudletList) {
            JSONObject joCloudlet = new JSONObject();
            joCloudlet.put(ID, cloudlet.getCloudletId());
            joCloudlet.put("AssignedVM", cloudlet.getVmId());
            cloudletArray.put(joCloudlet);
        }
        
        root.put("Environment", joEnv);
    }
    
    public static void addPowerDatacenterState(PowerDatacenter powerDatacenter) {
        if(root == null) {
            return;
        }
        
        
        JSONObject jo = new JSONObject();
        
        jo.put(TIME, CloudSim.clock());
        
        double usedMIPS;
        JSONArray hostArray = new JSONArray();
        jo.put(HOSTS, hostArray);
        
        for(Host host : powerDatacenter.getHostList()) {
            JSONObject joHost = new JSONObject();
            joHost.put(ID, host.getId());
            joHost.put("Total MIPS", host.getTotalMips());
            usedMIPS = host.getTotalMips()-host.getAvailableMips();
            joHost.put("Used MIPS", usedMIPS);
            joHost.put("% used MIPS", usedMIPS/host.getTotalMips());
            //joHost.put()

            JSONArray VMArray = new JSONArray();
            joHost.put(VMS, VMArray);
            
            for(Vm vm : host.getVmList()) {
                JSONObject joVM = new JSONObject();
                joVM.put(ID, vm.getId());
                joVM.put("IsInMigration", vm.isInMigration());
                if(vm.isInMigration()) {
                    joVM.put("NewHost", vm.getHost().getId());
                }
                JSONArray cloudletArray = new JSONArray();
                joVM.put(CLOUDLETS, cloudletArray);
                CloudletScheduler cloudletScheduler = vm.getCloudletScheduler();
                for(ResCloudlet resCloudlet : cloudletScheduler.getCloudletExecList()) {
                    Cloudlet cloudlet = resCloudlet.getCloudlet();
                    JSONObject joCloudlet = new JSONObject();
                    joCloudlet.put(ID, cloudlet.getCloudletId());
                    cloudletArray.put(joCloudlet);
                }
                
                VMArray.put(joVM);
            }
            
            hostArray.put(joHost);
        }
        
        JSONObject joDatacenter = new JSONObject();
        joDatacenter.put("energy", powerDatacenter.getPower() / (3600 * 1000));
        joDatacenter.put("numberOfMigrations", powerDatacenter.getMigrationCount());
        jo.put("Datacenter", joDatacenter);
        
        array.put(jo);
    }
    
    public static JSONObject getJSON() {
        return root;
    }
    
    public static void showJSON() {
        System.out.println(root);
    }
    
    public static void writeJSON(String path) {
        
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(path));
            bw.write(JsonWriter.formatJson(root.toString()));
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(JSONOutput.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
