package svila.policiesVmOptimizer;

import java.util.List;

import org.cloudbus.cloudsim.Host;

import svila.vmOptimizerFix.VMOptimizerPolicy;

public class MinPRVMOptimizerPolicy extends VMOptimizerPolicy {

	public MinPRVMOptimizerPolicy(List<? extends Host> hostList) {
		super(hostList);
	}

}
