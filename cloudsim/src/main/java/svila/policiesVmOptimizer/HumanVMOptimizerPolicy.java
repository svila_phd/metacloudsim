package svila.policiesVmOptimizer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import svila.interactive.HumanStrategy;
import svila.metricsLogic.HostAnalyzer;
import svila.metricsLogic.VmAnalyzer;
import svila.planetlabNetwork.StaticResources;
import svila.vmOptimizerFix.VMOptimizerPolicy;

public class HumanVMOptimizerPolicy extends VMOptimizerPolicy {

	HumanStrategy humanStrategy;
	public VmAnalyzer vmAnalyzer;
	public HostAnalyzer hostAnalyzer;
	
	JSONObject jsonObject;
	List<Double> stopAtTimeList;
	//Double[] stopAtSteps;
	//Double[]
	
	public HumanVMOptimizerPolicy(List<? extends Host> hostList) {
		super(hostList);
		this.humanStrategy = new HumanStrategy(this);
		this.vmAnalyzer = new VmAnalyzer();
		this.hostAnalyzer = new HostAnalyzer();
	}

	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
		return humanStrategy.optimize();
	}

	@Override
	public void update() {
		this.vmAnalyzer.index++;
		
		if(this.vmAnalyzer.isFirstTime()) {
			this.vmAnalyzer.initVms(getPowerDatacenter().getVmList());
			//this.vmAnalyzer.initVms(getPowerDatacenter().getVmList(), "rolling");
		}
		
		vmAnalyzer.updateStats();
		//vmAnalyzer.showInfo();
		
		
		
		this.hostAnalyzer.index++;
		
		if(this.hostAnalyzer.isFirstTime()) {
			this.hostAnalyzer.initHosts(getPowerDatacenter().getHostList());
		}
		
		hostAnalyzer.updateStats();
		//hostAnalyzer.showInfo();
		
		
		readInteractiveFile();
	}
	
	private void readInteractiveFile() {
		File file = new File(StaticResources.interactiveFilename);
		JSONObject launcherObject = null;
		try {
			launcherObject = new JSONObject(new String(Files.readAllBytes(file.toPath()),"UTF-8"));
		} catch (JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JSONArray stopAtTimeArray = launcherObject.getJSONArray("stopAtTime");
		stopAtTimeList = new ArrayList<>();
		for(int i=0; i<stopAtTimeArray.length(); i++) {
			stopAtTimeList.add(stopAtTimeArray.getDouble(i));
		}

		checkStopAtTime();
	}
	
	private void checkStopAtTime() {
		double time = this.vmAnalyzer.index * 300.0;
		for(int i=0; i<stopAtTimeList.size(); i++) {
			if(time == stopAtTimeList.get(i)) {
				StaticResources.pressAnyKeyToContinue();
			}
		}
	}

}
