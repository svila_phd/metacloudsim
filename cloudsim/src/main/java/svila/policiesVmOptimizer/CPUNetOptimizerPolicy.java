package svila.policiesVmOptimizer;

import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

import svila.metricsLogic.DatacenterAnalyzer;
import svila.metricsLogic.HostAnalyzer;
import svila.metricsLogic.MetricsStrategy;
import svila.metricsLogic.VmAnalyzer;
import svila.vmOptimizerFix.VMOptimizerPolicy;

public class CPUNetOptimizerPolicy extends VMOptimizerPolicy {
	public VmAnalyzer vmAnalyzer;
	public HostAnalyzer hostAnalyzer;
	public DatacenterAnalyzer datacenterAnalyzer;
	
	public CPUNetOptimizerPolicy(List<? extends Host> hostList) {
		super(hostList);
		this.vmAnalyzer = new VmAnalyzer();
		this.hostAnalyzer = new HostAnalyzer();
		this.datacenterAnalyzer = new DatacenterAnalyzer();
	}

	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
		return metricsStrategy.optimize();
	}

	@Override
	public void update() {
		this.vmAnalyzer.index++;
		
		//if(this.vmAnalyzer.isFirstTime()) {
		this.vmAnalyzer.tryAddNewVms(getPowerDatacenter().getVmList());
			//this.vmAnalyzer.initVms(getPowerDatacenter().getVmList(), "rolling");
		//}
				
		vmAnalyzer.updateStats();
		//vmAnalyzer.showInfo();
		
		
		
		this.hostAnalyzer.index++;
		
		if(this.hostAnalyzer.isFirstTime()) {
			this.hostAnalyzer.initHosts(getPowerDatacenter().getHostList());
			this.datacenterAnalyzer.init(getPowerDatacenter(), this.hostAnalyzer);
		}
		
		hostAnalyzer.updateStats();
		//hostAnalyzer.showInfo();
		
		datacenterAnalyzer.index++;
		datacenterAnalyzer.updateStats();
	}
}
