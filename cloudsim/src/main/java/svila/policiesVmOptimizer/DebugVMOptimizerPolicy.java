package svila.policiesVmOptimizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.metricsLogic.HostAnalyzer;
import svila.metricsLogic.MigrationsTracker;
import svila.metricsLogic.VmAnalyzer;
import svila.minicloud.sim.Scenario;
import svila.vmOptimizerFix.VMOptimizerPolicy;

public class DebugVMOptimizerPolicy extends VMOptimizerPolicy {
	
	public VmAnalyzer vmAnalyzer;
	public HostAnalyzer hostAnalyzer;
	public MigrationsTracker migrationsTracker;

	public DebugVMOptimizerPolicy(List<? extends Host> hostList) {
		super(hostList);
		this.vmAnalyzer = new VmAnalyzer();
		this.hostAnalyzer = new HostAnalyzer();
		this.migrationsTracker = new MigrationsTracker();
	}
	
	private Map<String, Object> getVmMigrationMap(int vmNetId, int hostNetId) {
		Map<String, Object> migrate = new HashMap<String, Object>();
		migrate.put("vm", getVm(vmNetId));
		migrate.put("host", this.getHost(hostNetId));
		return migrate;
	}
	
	private Vm getVm(int vmNetId) {
		for(Vm vm : this.vmSelPolicy.getPowerDatacenter().getVmList()) {
			if(vm.getNetworkId() == vmNetId) {
				return vm;
			}
		}
		return null;
	}
	
	private Host getHost(int hostNetId) {
		for(Host host : this.getHostList()) {
			if(host.getNetworkId() == hostNetId) {
				return host;
			}
		}
		return null;
	}
	
	@Override
	public void update() {
		this.vmAnalyzer.index++;
		
		if(this.vmAnalyzer.isFirstTime()) {
			this.vmAnalyzer.initVms(getPowerDatacenter().getVmList());
			//this.vmAnalyzer.initVms(getPowerDatacenter().getVmList(), "rolling");
		}
		
		vmAnalyzer.updateStats();
		//vmAnalyzer.showInfo();
		
		
		
		this.hostAnalyzer.index++;
		
		if(this.hostAnalyzer.isFirstTime()) {
			this.hostAnalyzer.initHosts(getPowerDatacenter().getHostList());
		}
		
		hostAnalyzer.updateStats();
		//hostAnalyzer.showInfo();
	}
	
	@Override
	public void updateTriggeredMigrations(List<Map<String, Object>> migrationMap) {
		this.migrationsTracker.setScheduledMigrations(this.hostAnalyzer.index, migrationMap);
	}
	
	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
		int slotNum = this.vmSelPolicy.getPowerDatacenter().slotNum;
		List<Map<String, Object>> migrationList = new ArrayList<>();
		List<Host> hosts = this.getHostList();
		
		/*if(slotNum == 6) {
			migrationList.add(getVmMigrationMap(5, 4));
		}*/
		
		if(slotNum == 12) {
			Scenario slicedScenario = new Scenario(this.hostAnalyzer, this.vmAnalyzer, 2, 4, 4);
			slicedScenario.setApplicationSteps(new int[] {2,3,4});
			slicedScenario.setStep(2);
			//slicedScenario.loadCurrentStepFromReal();
			slicedScenario.loadCurrentStepFromSlice();
			slicedScenario.hostMap.get(3).cpuPerc = 0.1;
			slicedScenario.saveToSlice();
			System.out.println("");
			//slicedScenario.applyForwardMigrations(this.migrationsTracker, 1, 3);
			//slicedScenario.migrateVmAndSaveMigration(5, 4);
			//slicedScenario.commit();
		}
		
		
		/*if(slotNum == 240) {
			migrationList.add(getVmMigrationMap(6, 3));
		}
		
		if(slotNum == 252) {
			migrationList.add(getVmMigrationMap(5, 4));
			migrationList.add(getVmMigrationMap(6, 4));
		}*/
		
		return migrationList;
	}

}
