package svila.policiesVmOptimizer.future;

public class FutureMigration {
	
	private FutureVm vm;
	private FutureHost oldHost;
	private FutureHost newHost;
	
	
	public FutureMigration(FutureVm vm, FutureHost oldHost, FutureHost newHost) {
		this.vm = vm;
		this.oldHost = oldHost;
		this.newHost = newHost;
	}


	public FutureVm getVm() {
		return vm;
	}


	public FutureHost getOldHost() {
		return oldHost;
	}


	public FutureHost getNewHost() {
		return newHost;
	}
	
	public void revertMigration() {
		if(oldHost != null) {
			this.oldHost.moveVm(vm);
		}
	}
}
