package svila.policiesVmOptimizer.future;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Vm;

import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public class NoMigrationsStrategy extends FutureStrategy {

	public NoMigrationsStrategy(FutureVMOptimizerPolicy optimizerPolicy) {
		super(optimizerPolicy);
	}

	@Override
	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
		return new ArrayList<>();
	}
}
