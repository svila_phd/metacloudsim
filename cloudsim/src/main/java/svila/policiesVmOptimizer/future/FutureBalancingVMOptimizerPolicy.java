package svila.policiesVmOptimizer.future;

import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

public class FutureBalancingVMOptimizerPolicy extends FutureDefaultVMOptimizerPolicy {

	public FutureBalancingVMOptimizerPolicy(List<? extends Host> hostList) {
		super(hostList);
		// TODO Auto-generated constructor stub
	}

	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {

		return futureStrategy.optimizeAllocation(vmList);
	}
}
