package svila.policiesVmOptimizer.future;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.util.MathUtil;

import svila.Tools;
import svila.metricsLogic.CPUCalculator.ForecastingTechnique;
import svila.metricsLogic.CPUCalculator.ForecastingTechniqueFactory;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethod;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethodFactory;
import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.StaticResources;

public class FutureHost {
	private PowerHostUtilizationHistory currentHost;
	FutureTechnique forecastingStrategy;
	private double mipsForecasting;
	private double percCPUForecasting;
	
	private double mipsForecastingWithMigrations;
	private double percCPUForecastingWithMigrations;
	
	//private double bwForecasting;
	public HashMap<Integer, FutureVm> inVms;
	public HashMap<Integer, FutureVm> outVms;
	private boolean requireUpdate;
	private boolean requireUpdateMigrations;
	public HashMap<Integer, FutureVm> futureVms;

	public FutureHost(PowerHostUtilizationHistory currentHost, FutureTechnique forecastingStrategy) {
		this.currentHost = currentHost;
		this.inVms = new HashMap<>();
		this.outVms = new HashMap<>();
		this.futureVms = new HashMap<>();
		this.forecastingStrategy = forecastingStrategy;
		this.requireUpdate = true;
		this.requireUpdateMigrations = true;
	}
	
	public PowerHostUtilizationHistory getCurrentHost() {
		return this.currentHost;
	}
	
	public void calculateMIPSForecasting() {
		if(this.getFutureVms().size() == 0) {
			percCPUForecasting = 0;
			mipsForecasting = 0;
			requireUpdate = false;
			return;
		}
		
		double cpuPercForecasting = forecastingStrategy.getForecasting(currentHost.getCPUUtilizationHistory0NewNOld());
		if(cpuPercForecasting < 0) {
			cpuPercForecasting = 0;
		}
		percCPUForecasting = cpuPercForecasting;
		mipsForecasting = percCPUForecasting * currentHost.getTotalMips();
		requireUpdate = false;
	}
	
	public void addCurrentVms() {
		ExperimentConfiguration ec = StaticResources.getCE();
		
		SignalProcessingMethod accurateSignalProcessing = SignalProcessingMethodFactory.getSignalProcessingMethod(ec.getVmSignalProcessingAccurate());
		accurateSignalProcessing.setParams(ec.getVmSignalProcessingParamsAccurate());
		ForecastingTechnique accurateForecastingTechnique = ForecastingTechniqueFactory.getForecastingTechnique(ec.getVmForecastingTechniqueAccurate());
		accurateForecastingTechnique.setParams(ec.getVmForecastingTechniqueParamsAccurate());
		FutureTechnique futureForeTechnique = new FutureTechnique(accurateSignalProcessing, accurateForecastingTechnique, ec.getVmForecastingResumeAccurate());
		
		SignalProcessingMethod basicSignalProcessing = SignalProcessingMethodFactory.getSignalProcessingMethod(ec.getVmSignalProcessingBasic());
		basicSignalProcessing.setParams(ec.getVmSignalProcessingParamsBasic());
		ForecastingTechnique basicForecastingTechnique = ForecastingTechniqueFactory.getForecastingTechnique(ec.getVmForecastingTechniqueBasic());
		basicForecastingTechnique.setParams(ec.getVmForecastingTechniqueParamsBasic());
		FutureTechnique futureSmoothTechnique = new FutureTechnique(basicSignalProcessing, basicForecastingTechnique, ec.getVmForecastingResumeBasic());

		for(Vm vm : this.currentHost.getVmList()) {
			PowerVm powerVm = (PowerVm) vm;
			FutureVm futureVm = new FutureVm(powerVm, futureForeTechnique, futureSmoothTechnique);
			futureVm.setFutureHost(this);
			futureVms.put(powerVm.getNetworkId(), futureVm);
		}
	}
	
	public HashMap<Integer, FutureVm> getFutureVms() {
		return this.futureVms;
	}
	
	public FutureMigration moveVm(FutureVm futureVm) {
		
		FutureHost currentHost = futureVm.getFutureHost();
		int vmNetId = futureVm.getCurrentVm().getNetworkId();
		
		if(currentHost.inVms.containsKey(vmNetId)) {
			currentHost.clearPutVm(futureVm);
		} else {
			currentHost.popVm(futureVm);
		}
		
		if(this.outVms.containsKey(vmNetId)) {
			this.clearPopVm(futureVm);
		} else {
			this.putVm(futureVm);
		}
		
		futureVm.setFutureHost(this);
		return new FutureMigration(futureVm, currentHost, this);
		
		/*FutureHost currentHost = futureVm.getFutureHost();
		
		Host originalHost = futureVm.getCurrentVm().getHost();
		//if(originalHost != null && // return to the original host
		//	currentHost.getCurrentHost().getNetworkId() != this.getCurrentHost().getNetworkId() &&
		//	originalHost.getNetworkId() == this.getCurrentHost().getNetworkId()) {
		if(originalHost != null && // return to the original host
			this.originalFutureVms.containsKey(futureVm.getCurrentVm().getNetworkId())) {
			currentHost.clearPutVm(futureVm);
			this.clearPopVm(futureVm);
		} else {
			currentHost.popVm(futureVm);
			this.putVm(futureVm);
		}
		futureVm.setFutureHost(this);
		return new FutureMigration(futureVm, currentHost, this);
		*/
	}

	public void putVm(FutureVm futureVm) {
		this.futureVms.put(futureVm.getCurrentVm().getNetworkId(), futureVm);
		this.inVms.put(futureVm.getCurrentVm().getNetworkId(), futureVm);
		requireUpdateMigrations = true;
	}
	
	public void clearPutVm(FutureVm futureVm) {
		this.futureVms.remove(futureVm.getCurrentVm().getNetworkId());
		this.inVms.remove(futureVm.getCurrentVm().getNetworkId());
		requireUpdateMigrations = true;
	}
	
	public void popVm(FutureVm futureVm) {
		this.futureVms.remove(futureVm.getCurrentVm().getNetworkId());
		this.outVms.put(futureVm.getCurrentVm().getNetworkId(), futureVm);
		requireUpdateMigrations = true;
	}
	
	public void clearPopVm(FutureVm futureVm) {
		this.futureVms.put(futureVm.getCurrentVm().getNetworkId(), futureVm);
		this.outVms.remove(futureVm.getCurrentVm().getNetworkId());
		requireUpdateMigrations = true;
	}

	public boolean isBlocked() {
		return this.currentHost.isBlocked();
	}

	public void setBlocked(boolean isBlocked) {
		this.currentHost.setBlocked(isBlocked);
	}
	
	public double getForecastedMIPS() {
		if(requireUpdate) {
			calculateMIPSForecasting();
		}
		return mipsForecasting;
	}
	
	public double getForecastedPercCPU() {
		if(requireUpdate) {
			calculateMIPSForecasting();
		}
		return percCPUForecasting;
	}
	
	public void calculateForecastedMIPSWithMigrations() {
		if(this.getFutureVms().size() == 0) {
			requireUpdateMigrations = false;
			mipsForecastingWithMigrations = 0;
			percCPUForecastingWithMigrations = 0;
			return;
		}
		
		double finalMIPS = getForecastedMIPS();
		for(FutureVm futureVm : inVms.values()) {
			finalMIPS += futureVm.getForecastedMIPS();
		}
		
		for(FutureVm futureVm : outVms.values()) {
			finalMIPS -= futureVm.getForecastedMIPS();
		}
		
		if(finalMIPS < 0) {
			finalMIPS = 0;
		}
		requireUpdateMigrations = false;
		mipsForecastingWithMigrations = finalMIPS;
		percCPUForecastingWithMigrations = mipsForecastingWithMigrations / currentHost.getTotalMips();
	}
	
	public double getForecastedMIPSWithMigrations() {
		if(requireUpdateMigrations) {
			calculateForecastedMIPSWithMigrations();
		}
		return mipsForecastingWithMigrations;
	}
	
	public double getForecastedPercCPUWithMigrations() {
		if(requireUpdateMigrations) {
			calculateForecastedMIPSWithMigrations();
		}
		return percCPUForecastingWithMigrations;
	}
	
	public boolean isSuitableForVm(FutureVm vm) {
		double vmFutureMips = vm.getForecastedMIPS();
		double hostFutureMips = this.getForecastedMIPSWithMigrations();
		double hostAvailableMips = this.getCurrentHost().getTotalMips() - hostFutureMips;
		
		return hostAvailableMips >= vmFutureMips;
	}
	
	public double getPower() {
		return this.currentHost.getPower(this.getForecastedPercCPUWithMigrations());
	}

	
	public double[] getMIPSHistoryWithFutureVMs() {
		double[] mipsHistory = new double[PowerVm.HISTORY_LENGTH];
		// Host history
		for(FutureVm currentVm : this.getFutureVms().values()) {
			double[] currentHistory = currentVm.getOriginalCPUPercHistory(); // index 0: newest value
			double totalMips = currentVm.getCurrentVm().getMips();
			for(int i=0; i<currentHistory.length; i++) {
				mipsHistory[i] += currentHistory[i] * totalMips;
			}
			
		}
		mipsHistory = MathUtil.trimZeroTail(mipsHistory);
		return mipsHistory;
	}
	
	public double[] getMIPSHistoryWithFutureVMs0OldNNew() {
		double[] mipsHistory = getMIPSHistoryWithFutureVMs();
	    //System.out.println("Original Array: " + Arrays.toString(mipsHistory)); 
		//Collections.reverse(Arrays.asList(mipsHistory)); // Totally incorrect
		Tools.reverseArray(mipsHistory);
	    //System.out.println("Reversed Array: " + Arrays.toString(mipsHistory)); 
		return mipsHistory;
	}
	
	public double[] getCPUPercHistoryWithFutureVMs() {
		double[] mipsHistory = getMIPSHistoryWithFutureVMs();
		double[] percHistory = new double[mipsHistory.length];
		double totalMips = this.currentHost.getTotalMips();

		for(int i=0; i<percHistory.length; i++) {
			percHistory[i] = mipsHistory[i] / totalMips;
		}
		
		return percHistory;
	}
	
	public double[] getCPUPercHistoryWithFutureVMs0OldNNew() {
		double[] percHistory = getCPUPercHistoryWithFutureVMs();
		Tools.reverseArray(percHistory);
		return percHistory;
	}
	
	public HashMap<String, Double> getCPUPercentages() {
		// 
		return null;
	}
}
