package svila.policiesVmOptimizer.future;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Pattern;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.util.ExecutionTimeMeasurer;

import svila.metricsLogic.HostStats;
import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.StaticResources;
import svila.policiesDatacenterAdjustment.AbstractDatacenterAdjustmentPolicy;
import svila.policiesDatacenterAdjustment.LimitDatacenterAdjustmentPolicy;
import svila.policiesFactories.future.LimitAdjustmentPolicyFactory;
import svila.policiesHostOverSaturation.future.OverSaturationData;
import svila.policiesVmMigration.future.markov.FutureMPABFDVmMigrationPolicy;
import svila.policiesVmMigration.future.markov.MarkovState;
import svila.policiesVmMigration.future.markov.MarkovStates;
import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public class FutureStrategy {
	FutureVMOptimizerPolicy optimizerPolicy;
	
	public FutureStrategy(FutureVMOptimizerPolicy optimizerPolicy) {
		this.optimizerPolicy = optimizerPolicy;
	}
	
	private HashMap<Integer, OverSaturationData> getOverSaturationDataMap(HashMap<Integer, FutureHost> futureHosts) {
		HashMap<Integer, OverSaturationData> oversaturationDataMap = new HashMap<>();
		for(FutureHost futureHost : futureHosts.values()) {
			OverSaturationData osd = this.optimizerPolicy.futureHostOverSaturationPolicy.getOverSaturationData(futureHost);
			oversaturationDataMap.put(futureHost.getCurrentHost().getNetworkId(), osd);
		}
		return oversaturationDataMap;
	}
	
	private HashMap<Integer, FutureHost> getOverUtilizedHosts(HashMap<Integer, OverSaturationData> overSaturationDataMap) {
		HashMap<Integer, FutureHost> oversaturatedHosts = new HashMap<>();
		
		for(OverSaturationData osd : overSaturationDataMap.values()) {
			if(osd.existUpperThresholdSaturation) {
				oversaturatedHosts.put(osd.futureHost.getCurrentHost().getNetworkId(), osd.futureHost);
			}
		}
		
		return oversaturatedHosts;
	}
	
	private HashMap<String, Set<FutureHost>> getSplitFutureHostSets(HashMap<Integer, OverSaturationData> overSaturationDataMap) {
		HashMap<String, Set<FutureHost>> hostsSplit = new HashMap<>();
		hostsSplit.put("all", new HashSet<FutureHost>());
		hostsSplit.put("overloaded", new HashSet<FutureHost>());
		hostsSplit.put("valid", new HashSet<FutureHost>());
		hostsSplit.put("active", new HashSet<FutureHost>());
		hostsSplit.put("blocked", new HashSet<FutureHost>());
		hostsSplit.put("poweroff", new HashSet<FutureHost>());
		
		for(OverSaturationData osd : overSaturationDataMap.values()) {
			FutureHost futureHost = osd.futureHost;
			hostsSplit.get("all").add(futureHost);
			if(futureHost.getFutureVms().size() == 0) {
				hostsSplit.get("poweroff").add(futureHost);
			} else if(osd.existUpperThresholdSaturation) {
				hostsSplit.get("overloaded").add(futureHost);
			} else {
				hostsSplit.get("active").add(futureHost);
				hostsSplit.get("valid").add(futureHost);
			}
			
			if(futureHost.isBlocked()) {
				hostsSplit.get("blocked").add(futureHost);
			}
		}
		
		ExperimentConfiguration ec = StaticResources.getCE();
		if(!ec.isOnlyInitialHosts()) {
			hostsSplit.get("valid").addAll(hostsSplit.get("poweroff"));
		}
		
		return hostsSplit;
	}
	
	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
		//----------------------------------------
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationTotal");
		ExecutionTimeMeasurer.start("optimizeAllocationHostSelection");
		//----------------------------------------
		
		HashMap<Integer, FutureHost> futureHosts = this.optimizerPolicy.getFutureHosts();
		HashMap<Integer, FutureVm> futureVms = this.optimizerPolicy.getFutureVms(futureHosts);
		
//		FutureMPABFDVmMigrationPolicy markov = new FutureMPABFDVmMigrationPolicy();
//		markov.setVmOptimizerPolicy(this.optimizerPolicy);
//		for(FutureHost host : futureHosts.values()) {
//			//if(host.getCurrentHost().getId() != 14 || this.optimizerPolicy.datacenterAnalyzer.index < 182) {
//			//	continue;
//			//}
//			/*if(host.getCurrentHost().getId() != 14 || CloudSim.clock() < 54600.0) {
//				continue;
//			}*/
//			
//			
//			// -------------------------------------------------------------------------------
//			// L'històric que es rep aquí és molt més petit del que tocaria, realment està saturat
//			// Un 150% d'us, tenint 5320 MIPS, gasta, 7000 aprox, però a l'agafar l'històric, surt 0.4
//			//System.out.println("Time: " + CloudSim.clock());
//			double[] mipsHistory = host.getMIPSHistoryWithFutureVMs0OldNNew(); // Funció correcta, genera el mateix que els fitxers de cloudlets
//			double[] hostPercHistory = host.getCPUPercHistoryWithFutureVMs0OldNNew();
//			
//			double[] perc = host.getCurrentHost().getCPUUtilizationHistory();
//			double[] percToMIPS = new double[perc.length];
//			for(int i=0; i<perc.length; i++) {
//				percToMIPS[i] = perc[i] * host.getCurrentHost().getTotalMips();
//			}
//			
//			HostStats slice = this.optimizerPolicy.hostAnalyzer.hostStats.get(32).getSlice(
//					this.optimizerPolicy.datacenterAnalyzer.index-10,
//					this.optimizerPolicy.datacenterAnalyzer.index, 0);
//			
//			List<Vm> currentVms = host.getCurrentHost().getVmList();
//			for(Vm currentVm : currentVms) {
//				String path = currentVm.getCloudletScheduler().getCloudletExecList().get(0).getCloudlet().getSourceFilepath();
//				String[] splitPath = path.split(Pattern.quote(File.separator));
//				String name = splitPath[splitPath.length-1];
//				System.out.println(name + " " + currentVm.getMips());
//			}
//			System.out.println();
//			
//			OverSaturationData osd_debug = markov.getOverSaturationDataMAD(host);
//			MarkovStates ms = new MarkovStates(hostPercHistory, osd_debug.upperThreshold, 0.1);
//			MarkovState nextMostProbableState = ms.getFutureMarkovState();
//			ms.showStates();
//			ms.showAllProbabilities();
//			// -------------------------------------------------------------------------------
//		}
		
		
		this.optimizerPolicy.futureHosts = futureHosts;
		this.optimizerPolicy.futureVms = futureVms;
		
		//----------------------------------------
		// HostSaturationPolicy
		// Step 1: Get overutilized hosts
		//List<PowerHostUtilizationHistory> overUtilizedHosts = hostOverSaturationPolicy.getOverUtilizedHosts();
		HashMap<Integer, OverSaturationData> overSaturationDataMap = getOverSaturationDataMap(futureHosts);
		HashMap<String, Set<FutureHost>> futureHostClassified = getSplitFutureHostSets(overSaturationDataMap);		
		
		this.optimizerPolicy.futureHostClassified = futureHostClassified;
				
		Set<FutureHost> overloadedHosts = futureHostClassified.get("overloaded");
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		optimizerPolicy.getExecutionTimeHistoryHostSelection().add(
				ExecutionTimeMeasurer.end("optimizeAllocationHostSelection"));
		//----------------------------------------
		
		//----------------------------------------
		// Restore point
		optimizerPolicy.saveAllocation();
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationVmSelection");
		//----------------------------------------


		// Datacenter hosts adjustment
		AbstractDatacenterAdjustmentPolicy adjustmentPolicy = LimitAdjustmentPolicyFactory.getLimitAdjustmentPolicy(this.optimizerPolicy);
		adjustmentPolicy.adjust();
		
		//----------------------------------------
		// VMSelectionPolicy
		// Step 2: Get VMs to migrate from overutilized hosts
		//List<? extends Vm> vmsToMigrate = getVmsToMigrateFromHosts(overUtilizedHosts);
		HashMap<Integer, FutureVm> vmsToMigrate = this.optimizerPolicy.futureVmSelectionPolicy.getVmsToMigrateFromHosts(overloadedHosts);
		adjustmentPolicy.addVMsFromBlockedHosts(vmsToMigrate);
		
		
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		this.optimizerPolicy.getExecutionTimeHistoryVmSelection().add(ExecutionTimeMeasurer.end("optimizeAllocationVmSelection"));
		Log.printLine("Reallocation of VMs from the over-utilized hosts:");
		ExecutionTimeMeasurer.start("optimizeAllocationVmReallocation");
		//----------------------------------------

		//----------------------------------------
		// VMMigrationPolicy
		// Step 3: Get to the migrated VMs a new host
		//List<Map<String, Object>> migrationMap = getNewVmPlacement(vmsToMigrate, new HashSet<Host>(
		//		overUtilizedHosts));
		
		List<Map<String, Object>> migrationMap = this.optimizerPolicy.futureVmMigrationPolicy.getNewVmPlacement(vmsToMigrate, futureHostClassified); 
		
		//----------------------------------------

		//----------------------------------------
		// Statistics
		this.optimizerPolicy.getExecutionTimeHistoryVmReallocation().add(
				ExecutionTimeMeasurer.end("optimizeAllocationVmReallocation"));
		Log.printLine();
		//----------------------------------------

		//----------------------------------------
		// SPLIT
		// HostSaturationPolicy - underutilized
		// VMMigrationPolicy
		// Step 4:   Migration map from underutilized hosts
		// Step 4.1: Underutilization host detection
		// Step 4.2: VM migrations
		
		List<FutureHost> overloadedHostsList = new ArrayList<>(overloadedHosts);
		List<FutureHost> allHostsList = new ArrayList<>(futureHostClassified.get("all"));
		
		migrationMap.addAll(
			this.optimizerPolicy.futureHostUnderUtilisationPolicy.getMigrationMapFromUnderUtilizedHosts(
					overloadedHostsList, allHostsList));
		//----------------------------------------
		
		//----------------------------------------
		// Restore the VMs to their original host
		this.optimizerPolicy.restoreAllocation();
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		this.optimizerPolicy.getExecutionTimeHistoryTotal().add(ExecutionTimeMeasurer.end("optimizeAllocationTotal"));
		//----------------------------------------

		
		//showMigrationMap(migrationMap);
		// Return map with VM migrations
		
		/*for (Map<String, Object> migrate : migrationMap) {
			Vm vm = (Vm) migrate.get("vm");
			PowerHost targetHost = (PowerHost) migrate.get("host");
			PowerHost oldHost = (PowerHost) vm.getHost();
			System.out.println("VM(" + vm.getNetworkId() + "): old (" + oldHost.getNetworkId() + " -> " + "new (" + targetHost.getNetworkId() + ")");
		}*/
		
		/*migrationMap.clear();
		
		for(FutureHost host : futureHosts.values()) {
			if(host.getCurrentHost().getNetworkId() > 10) {
				for(Vm vm : host.getCurrentHost().getVmList()) {
					Map<String, Object> migrate = new HashMap<String, Object>();
					migrate.put("vm", vm);
					migrate.put("host", futureHosts.get(10).getCurrentHost());
					migrationMap.add(migrate);
				}
			}
		}*/
		
		/*migrationMap.clear();
		Random rand = new Random();
		int randomHost = rand.nextInt(8) + 10;
		//int randomHost = 12; 
		for(FutureHost host : futureHosts.values()) {
			if(host.getCurrentHost().getNetworkId() != randomHost) {
				for(Vm vm : host.getCurrentHost().getVmList()) {
					Map<String, Object> migrate = new HashMap<String, Object>();
					migrate.put("vm", vm);
					migrate.put("host", futureHosts.get(randomHost).getCurrentHost());
					migrationMap.add(migrate);
				}
			}
		}*/
		
		
		/*for (Map<String, Object> migrate : migrationMap) {
			Vm vm = (Vm) migrate.get("vm");
			PowerHost targetHost = (PowerHost) migrate.get("host");
			PowerHost oldHost = (PowerHost) vm.getHost();
			System.out.println("VM(" + vm.getNetworkId() + "): old (" + oldHost.getNetworkId() + " -> " + "new (" + targetHost.getNetworkId() + ")");
			if(oldHost.getNetworkId() == targetHost.getNetworkId()) {
				System.out.println("aaa");
			}
		}*/
		
		return migrationMap;
	}
}
