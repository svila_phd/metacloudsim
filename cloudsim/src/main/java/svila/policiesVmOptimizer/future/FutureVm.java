package svila.policiesVmOptimizer.future;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerVm;

public class FutureVm {
	private PowerVm currentVm;
	private double mipsForecasting;
	private double percCPUForecasting;
	
	private double mipsSmoothing;
	private double percCPUSmoothing;
	
	private FutureHost futureHost;
	private FutureTechnique forecastingStrategy;
	private FutureTechnique smoothingStrategy;
	private boolean requireForecastingUpdate;
	private boolean requireSmoothingUpdate;
	
	public FutureVm(PowerVm currentVm, FutureTechnique forecastingStrategy, FutureTechnique smoothingStrategy) {
		this.currentVm = currentVm;
		this.forecastingStrategy = forecastingStrategy;
		this.smoothingStrategy = smoothingStrategy;

		this.requireForecastingUpdate = true;
		this.requireSmoothingUpdate = true;
	}
	
	public PowerVm getCurrentVm() {
		return currentVm;
	}
	
	public void setFutureHost(FutureHost futureHost) {
		this.futureHost = futureHost;
	}
	
	public FutureHost getFutureHost() {
		return this.futureHost;
	}
	
	// Most recent time: index 0
	public double[] getOriginalCPUPercHistory() {
		double[] utilizationHistory = new double[PowerVm.HISTORY_LENGTH];
		for (int i = 0; i < currentVm.getUtilizationHistory().size(); i++) {
			utilizationHistory[i] += currentVm.getUtilizationHistory().get(i);
		}
		return utilizationHistory;
	}
	
	// Farthest time: index 0
	public double[] getCPUPercHistory() {
		int arrayLength = Math.min(PowerVm.HISTORY_LENGTH, currentVm.getUtilizationHistory().size());
		double[] utilizationHistory = new double[arrayLength];
		for (int i = 0; i < arrayLength; i++) {
			utilizationHistory[arrayLength -i -1] += currentVm.getUtilizationHistory().get(i);
		}
		return utilizationHistory;
	}
	
	public double[] getCPUPercHistory0NewNOld() {
		return getCPUPercHistory();
	}
	
	public void calculateMIPSForecasting() {
		double cpuPercForecasting = forecastingStrategy.getForecasting(this.getCPUPercHistory0NewNOld());
		if(cpuPercForecasting < 0) {
			cpuPercForecasting = 0;
		}
		if(cpuPercForecasting > 100) {
			cpuPercForecasting = 100;
		}
		percCPUForecasting = cpuPercForecasting;
		mipsForecasting = percCPUForecasting * currentVm.getMips();
		this.requireForecastingUpdate = false;
	}
	
	public double getForecastedMIPS() {
		if(requireForecastingUpdate) {
			calculateMIPSForecasting();
		}
		return mipsForecasting;
	}
	
	public void calculateMIPSSmoothing() {
		double cpuPercForecasting = smoothingStrategy.getForecasting(this.getCPUPercHistory0NewNOld());
		if(cpuPercForecasting < 0) {
			cpuPercForecasting = 0;
		}
		if(cpuPercForecasting > 100) {
			cpuPercForecasting = 100;
		}
		percCPUSmoothing = cpuPercForecasting;
		mipsSmoothing = percCPUSmoothing * currentVm.getMips();
		this.requireSmoothingUpdate = false;
	}
	
	public double getSmoothingMIPS() {
		if(requireSmoothingUpdate) {
			calculateMIPSSmoothing();
		}
		return mipsSmoothing;
	}
	
	public double getForecastedPercCPU() {
		if(requireForecastingUpdate) {
			calculateMIPSForecasting();
		}
		return percCPUForecasting;
	}
	
	public double getSmoothingPercCPU() {
		if(requireSmoothingUpdate) {
			calculateMIPSSmoothing();
		}
		return percCPUSmoothing;
	}
}
