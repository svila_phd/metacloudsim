package svila.policiesVmOptimizer.future;

import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public class FutureDefaultVMOptimizerPolicy extends FutureVMOptimizerPolicy {
	FutureStrategy futureStrategy;
	
	public FutureDefaultVMOptimizerPolicy(List<? extends Host> hostList) {
		super(hostList);
		this.futureStrategy = new FutureStrategy(this);
	}
	
	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
		/*
		if(this.vmAnalyzer.index < 60) {
			return new ArrayList<>();
		}
		*/
		return futureStrategy.optimizeAllocation(vmList);
	}
	
	@Override
	public void update() {
		this.vmAnalyzer.index++;
		
		this.vmAnalyzer.tryAddNewVms(getPowerDatacenter().getVmList());
				
		vmAnalyzer.updateStats();
		//vmAnalyzer.showInfo();
		
		
		this.hostAnalyzer.index++;
		
		if(this.hostAnalyzer.isFirstTime()) {
			this.hostAnalyzer.initHosts(getPowerDatacenter().getHostList());
			this.datacenterAnalyzer.init(getPowerDatacenter(), this.hostAnalyzer);
		}
		
		hostAnalyzer.updateStats();
		//hostAnalyzer.showInfo();
		
		datacenterAnalyzer.index++;
		datacenterAnalyzer.updateStats();
	}
}
