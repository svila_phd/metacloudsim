package svila.policiesVmOptimizer.future;

import java.util.List;

import org.cloudbus.cloudsim.Host;

public class NoMigrationsVMOptimizerPolicy extends FutureDefaultVMOptimizerPolicy {

	public NoMigrationsVMOptimizerPolicy(List<? extends Host> hostList) {
		super(hostList);
		this.futureStrategy = new NoMigrationsStrategy(this);
	}

}
