package svila.policiesVmOptimizer.future;

import org.cloudbus.cloudsim.util.MathUtil;

import svila.Tools;
import svila.metricsLogic.CPUCalculator.ForecastingTechnique;
import svila.metricsLogic.CPUCalculator.SignalProcessingMethod;

public class FutureTechnique {
	private SignalProcessingMethod signalProcessingMethod;
	private ForecastingTechnique forecastingTechnique;
	private String resumeOperation;
	
	public FutureTechnique(SignalProcessingMethod signalProcessingMethod, ForecastingTechnique forecastingTechnique) {
		this.signalProcessingMethod = signalProcessingMethod;
		this.forecastingTechnique = forecastingTechnique;
		this.resumeOperation = "max";
	}
	
	public FutureTechnique(SignalProcessingMethod signalProcessingMethod, ForecastingTechnique forecastingTechnique, String resumeOperation) {
		this.signalProcessingMethod = signalProcessingMethod;
		this.forecastingTechnique = forecastingTechnique;
		this.resumeOperation = resumeOperation;
	}
	
	public Float[] getForecastingArray(double[] history) {
		Float[] historyFloat = Tools.doubleToFloatArray(history);
		Float[] processedData = signalProcessingMethod.apply(historyFloat);
		return forecastingTechnique.apply(processedData);
	}
	
	public double getForecasting(double[] history) {
		if(history.length == 1 && history[0] == 0.0) { // No history, empty host
			return 0.0;
		}
		
		//Float[] forecastedData = getForecastingArray(history);
		Float[] historyFloat = Tools.doubleToFloatArray(history);
		Float[] processedData = signalProcessingMethod.apply(historyFloat);
		Float[] forecastedData = forecastingTechnique.apply(processedData);
		
		
		float finalValue;
		switch(this.resumeOperation) {
		case "min":
			finalValue = getMinValue(forecastedData);
			break;
		case "mean":
			finalValue = getMeanValue(forecastedData);
			break;
		case "median":
			finalValue = getMedianValue(forecastedData);
			break;
		case "max":
		default:
			finalValue = getMaxValue(forecastedData);
			break;
		}

		if(Double.isNaN(finalValue)) {
			finalValue = 0;
		}
		
		/*
		 * if store forecasting file:
		 *		# save randomUUID, smoothing, forecasting, type, value
		 *		# history
		 *		# processed
		 *		# forecasted
		 */
		
		return finalValue;
	}
	
	public float getMaxValue(Float[] array) {
		float maxValue = array[0];
	    for(int i=1;i<array.length;i++){
	    	if(array[i] > maxValue){
	    		maxValue = array[i];
	        }
	    }
	    return maxValue;
	}
	
	public float getMinValue(Float[] array) {
		float minValue = array[0];
	    for(int i=1;i<array.length;i++){
	    	if(array[i] < minValue){
	    		minValue = array[i];
	        }
	    }
	    return minValue;
	}
	
	public float getMeanValue(Float[] array) {
		float sum = 0;
		for (Float number : array) {
			sum += number;
		}
		return sum / array.length;
	}
	
	public float getMedianValue(Float[] array) {
		double[] doubleArray = Tools.floatToDoubleArray(array);
		return (float) MathUtil.median(doubleArray);
	}
	
	public float getPercentileValue(Float[] array, double percentile) {
		double[] doubleArray = Tools.floatToDoubleArray(array);
		return (float) MathUtil.getStatistics(doubleArray).getPercentile(percentile);
	}
}
