package svila.policiesVmOptimizer;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;
import org.cloudbus.cloudsim.util.ExecutionTimeMeasurer;

import svila.vmOptimizerFix.VMOptimizerPolicy;

public class CloudSimVMOptimizerPolicy extends VMOptimizerPolicy {

	public CloudSimVMOptimizerPolicy(List<? extends Host> hostList) {
		super(hostList);
	}

	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
		//----------------------------------------
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationTotal");
		ExecutionTimeMeasurer.start("optimizeAllocationHostSelection");
		//----------------------------------------
		
		//----------------------------------------
		// HostSaturationPolicy
		// Step 1: Get overutilized hosts
		List<PowerHostUtilizationHistory> overUtilizedHosts = hostOverSaturationPolicy.getOverUtilizedHosts();
		
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		getExecutionTimeHistoryHostSelection().add(
				ExecutionTimeMeasurer.end("optimizeAllocationHostSelection"));
		//----------------------------------------
		
		//----------------------------------------
		// Restore point
		saveAllocation();
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		ExecutionTimeMeasurer.start("optimizeAllocationVmSelection");
		//----------------------------------------
		
		//----------------------------------------
		// VMSelectionPolicy
		// Step 2: Get VMs to migrate from overutilized hosts
		List<? extends Vm> vmsToMigrate = getVmsToMigrateFromHosts(overUtilizedHosts);
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		getExecutionTimeHistoryVmSelection().add(ExecutionTimeMeasurer.end("optimizeAllocationVmSelection"));
		Log.printLine("Reallocation of VMs from the over-utilized hosts:");
		ExecutionTimeMeasurer.start("optimizeAllocationVmReallocation");
		//----------------------------------------

		//----------------------------------------
		// VMMigrationPolicy
		// Step 3: Get to the migrated VMs a new host
		List<Map<String, Object>> migrationMap = getNewVmPlacement(vmsToMigrate, new HashSet<Host>(
				overUtilizedHosts));
		//----------------------------------------

		//----------------------------------------
		// Statistics
		getExecutionTimeHistoryVmReallocation().add(
				ExecutionTimeMeasurer.end("optimizeAllocationVmReallocation"));
		Log.printLine();
		//----------------------------------------

		//----------------------------------------
		// SPLIT
		// HostSaturationPolicy - underutilized
		// VMMigrationPolicy
		// Step 4:   Migration map from underutilized hosts
		// Step 4.1: Underutilization host detection
		// Step 4.2: VM migrations
		migrationMap.addAll(getMigrationMapFromUnderUtilizedHosts(overUtilizedHosts));
		//----------------------------------------
		
		//----------------------------------------
		// Restore the VMs to their original host
		restoreAllocation();
		//----------------------------------------
		
		//----------------------------------------
		// Statistics
		getExecutionTimeHistoryTotal().add(ExecutionTimeMeasurer.end("optimizeAllocationTotal"));
		//----------------------------------------

		
		//showMigrationMap(migrationMap);
		// Return map with VM migrations
		return migrationMap;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}
}
