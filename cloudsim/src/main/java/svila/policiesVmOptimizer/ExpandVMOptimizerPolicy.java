package svila.policiesVmOptimizer;

import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;

import svila.metricsLogic.ExpandStrategy;
import svila.metricsLogic.HostAnalyzer;
import svila.metricsLogic.MetricsStrategy;
import svila.metricsLogic.VmAnalyzer;
import svila.vmOptimizerFix.VMOptimizerPolicy;

public class ExpandVMOptimizerPolicy extends VMOptimizerPolicy {
	ExpandStrategy expandStrategy;
	public VmAnalyzer vmAnalyzer;
	public HostAnalyzer hostAnalyzer;
	
	public ExpandVMOptimizerPolicy(List<? extends Host> hostList) {
		super(hostList);
		this.expandStrategy = new ExpandStrategy(this);
		this.vmAnalyzer = new VmAnalyzer();
		this.hostAnalyzer = new HostAnalyzer();
	}

	public List<Map<String, Object>> optimizeAllocation(List<? extends Vm> vmList) {
		return expandStrategy.optimize();
	}

	@Override
	public void update() {
		this.vmAnalyzer.index++;
		
		if(this.vmAnalyzer.isFirstTime()) {
			this.vmAnalyzer.initVms(getPowerDatacenter().getVmList());
			//this.vmAnalyzer.initVms(getPowerDatacenter().getVmList(), "rolling");
		}
		
		vmAnalyzer.updateStats();
		//vmAnalyzer.showInfo();
		
		
		
		this.hostAnalyzer.index++;
		
		if(this.hostAnalyzer.isFirstTime()) {
			this.hostAnalyzer.initHosts(getPowerDatacenter().getHostList());
		}
		
		hostAnalyzer.updateStats();
		//hostAnalyzer.showInfo();
	}
}
