package svila.policiesHostOverSaturationExtended;

public class HostState {
	
	
	public boolean isHostOverSaturated() {
		return isHostOverSaturatedCPU;
	}
	
	public boolean isHostOverSaturatedCPU() {
		return false;
	}
	
	public boolean isHostOverSaturatedBW() {
		return false;
	}
}
