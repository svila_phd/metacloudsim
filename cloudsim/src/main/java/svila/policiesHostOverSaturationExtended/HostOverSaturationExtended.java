package svila.policiesHostOverSaturationExtended;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;

import svila.policiesHostOverSaturation.HostOverSaturationBase;
import svila.vmOptimizerFix.VMOptimizerPolicy;

public abstract class HostOverSaturationExtended extends HostOverSaturationBase {

	// CPU
	// Inter network
	// Hops
	// Tipus de VMs
	// Relació entre les VMs
	// Objecte informant de l'estat del host
		// Generar puntuació
		
		// Sistema de puntuació incremental
		// Normalització de dades
		// Si la CPU està bé, sumar el % de marge calculat, sinó, -% de saturació
		// Si la xarxa està bé sumar el % de marge calculat, sinó, -% de saturació
		// Mitja de salts de les seves comunicacions
		// Puntuació basal del host + relació de les migracions
	
	// Quan un host està saturat, guardar el motiu pel qual està saturat
	// Quan s'hagin de migrar VMs d'un host, obtenir aquest motiu i mirar les VMs a hosts compatibles
	// Un host pot estar saturat per xarxa, per tant, els hosts que estan saturats per CPU, en buidar-se, podrien
	// rebre aquestes VMs si hi caben
	// Quan hi ha saturació per xarxa, al moure VMs al host indicat, es reduirà tant al host origen com al destí
	// Amb CPU movem els recursos, però sempre tindrem els mateixos
	// En xarxa no, en xarxa, una bona assignació permet reduïr el cost entre hosts
		// Intercanvi-join de VMs
	
	public HostOverSaturationExtended(double safetyParameter, VMOptimizerPolicy fallbackVmAllocationPolicy,
			double utilizationThreshold) {
		super(safetyParameter, fallbackVmAllocationPolicy, utilizationThreshold);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isHostOverUtilized(PowerHost host) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isHostOverUtilizedAfterAllocation(PowerHost host, Vm vm) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public abstract HostState generateHostState();
	public abstract 
	
}
