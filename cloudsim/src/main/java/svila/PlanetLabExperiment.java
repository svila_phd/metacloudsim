package svila;

/**
 *
 * @author Sergi
 */
public class PlanetLabExperiment {
    private String vmAllocationPolicy;
    private String vmSelectionPolicy;
    private String parameter;
    
    public PlanetLabExperiment(String vmAllocationPolicy, String vmSelectionPolicy, String parameter) {
        this.vmAllocationPolicy = vmAllocationPolicy;
        this.vmSelectionPolicy = vmSelectionPolicy;
        this.parameter = parameter;
    }

    public String getVmAllocationPolicy() {
        return vmAllocationPolicy;
    }

    public String getVmSelectionPolicy() {
        return vmSelectionPolicy;
    }

    public String getParameter() {
        return parameter;
    }
    
    @Override
    public String toString() {
        return vmAllocationPolicy + "_" + vmSelectionPolicy + "_" + parameter;
    }
}
