package svila.policiesHostUnderUtilisation;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;

public class NoHostUnderUtilisationPolicy extends DefaultHostUnderUtilisationPolicy {
	@Override
	public PowerHost getUnderUtilizedHost(Set<? extends Host> excludedHosts) {
		return this.vmOptimizerPolicy.getUnderUtilizedHostOriginal(excludedHosts);
	}

	@Override
	public List<? extends Vm> getVmsToMigrateFromUnderUtilizedHost(PowerHost host) {
		return this.vmOptimizerPolicy.getVmsToMigrateFromUnderUtilizedHostOriginal(host);
	}

	@Override
	public List<Map<String, Object>> getMigrationMapFromUnderUtilizedHosts(
		List<PowerHostUtilizationHistory> overUtilizedHosts) {
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		return migrationMap;
	}
}
