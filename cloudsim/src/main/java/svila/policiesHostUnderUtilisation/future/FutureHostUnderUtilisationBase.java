package svila.policiesHostUnderUtilisation.future;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.power.lists.PowerVmList;

import svila.policiesHostUnderUtilisation.DefaultHostUnderUtilisationPolicy;
import svila.policiesVmOptimizer.future.FutureHost;
import svila.policiesVmOptimizer.future.FutureMigration;
import svila.policiesVmOptimizer.future.FutureVm;
import svila.vmOptimizerFix.VMOptimizerPolicy;
import svila.vmOptimizerFix.future.FutureVMOptimizerPolicy;

public class FutureHostUnderUtilisationBase {
	
	protected FutureVMOptimizerPolicy vmOptimizerPolicy;
	
	public FutureVMOptimizerPolicy getVmOptimizerPolicy() {
		return vmOptimizerPolicy;
	}

	public void setVmOptimizerPolicy(FutureVMOptimizerPolicy vmOptimizerPolicy) {
		this.vmOptimizerPolicy = vmOptimizerPolicy;
	}
	
	public FutureHost getUnderUtilizedHost(Set<FutureHost> validHosts) {
		double minUtilization = 1;
		FutureHost underUtilizedHost = null;
		for (FutureHost host : validHosts) {
			double utilization = host.getForecastedPercCPUWithMigrations();
			if (utilization > 0 && utilization < minUtilization
					&& !areAllVmsMigratingOutOrAnyVmMigratingIn(host.getCurrentHost())) {
				minUtilization = utilization;
				underUtilizedHost = host;
			}
		}
		return underUtilizedHost;
	}

	// Original function
	public List<? extends Vm> getVmsToMigrateFromUnderUtilizedHost(PowerHost host) {
		List<Vm> vmsToMigrate = new LinkedList<Vm>();
		for (Vm vm : host.getVmList()) {
			if (!vm.isInMigration()) {
				vmsToMigrate.add(vm);
			}
		}
		return vmsToMigrate;
	}

	public List<Map<String, Object>> getMigrationMapFromUnderUtilizedHosts(
			List<FutureHost> overUtilizedHosts, List<FutureHost> allHosts) {
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		List<FutureHost> switchedOffHosts = getSwitchedOffHosts(allHosts);

		// over-utilized hosts + hosts that are selected to migrate VMs to from over-utilized hosts
		Set<FutureHost> excludedHostsForFindingUnderUtilizedHost = new HashSet<FutureHost>();
		excludedHostsForFindingUnderUtilizedHost.addAll(overUtilizedHosts);
		excludedHostsForFindingUnderUtilizedHost.addAll(switchedOffHosts);
		excludedHostsForFindingUnderUtilizedHost.addAll(extractHostListFromMigrationMap(migrationMap));

		// over-utilized + under-utilized hosts
		Set<FutureHost> excludedHostsForFindingNewVmPlacement = new HashSet<FutureHost>();
		excludedHostsForFindingNewVmPlacement.addAll(overUtilizedHosts);
		excludedHostsForFindingNewVmPlacement.addAll(switchedOffHosts);

		int numberOfHosts = allHosts.size();
		

		while (true) {
			if (numberOfHosts == excludedHostsForFindingUnderUtilizedHost.size()) {
				break;
			}
			
			HashSet<FutureHost> validSet = new HashSet<>(allHosts);
			HashSet<FutureHost> excludedSet = new HashSet<>(excludedHostsForFindingUnderUtilizedHost);
			validSet.removeAll(excludedSet); // all - excluded
			
			FutureHost underUtilizedHost = getUnderUtilizedHost(validSet);
			
			//Sergi
			//underUtilizedHost = null;
			
			if (underUtilizedHost == null) {
				break;
			}

			Log.printConcatLine("Under-utilized host: host #", underUtilizedHost.getCurrentHost().getId(), "\n");

			excludedHostsForFindingUnderUtilizedHost.add(underUtilizedHost);
			excludedHostsForFindingNewVmPlacement.add(underUtilizedHost);
			
			List<FutureVm> vmsToMigrateFromUnderUtilizedHost = getVmsToMigrateFromUnderUtilizedHost(underUtilizedHost);
			if (vmsToMigrateFromUnderUtilizedHost.isEmpty()) {
				continue;
			}

			Log.print("Reallocation of VMs from the under-utilized host: ");
			if (!Log.isDisabled()) {
				for (FutureVm vm : vmsToMigrateFromUnderUtilizedHost) {
					Log.print(vm.getCurrentVm().getId() + " ");
				}
			}
			Log.printLine();

			validSet = new HashSet<>(allHosts);
			excludedSet = new HashSet<>(excludedHostsForFindingNewVmPlacement);
			validSet.removeAll(excludedSet); // all - excluded
			
			List<Map<String, Object>> newVmPlacement = getNewVmPlacementFromUnderUtilizedHost(
					vmsToMigrateFromUnderUtilizedHost, validSet);

			excludedHostsForFindingUnderUtilizedHost.addAll(extractHostListFromMigrationMap(newVmPlacement));

			migrationMap.addAll(newVmPlacement);
			Log.printLine();
		}

		return migrationMap;
	}
	
	protected List<Map<String, Object>> getNewVmPlacementFromUnderUtilizedHost(
			List<FutureVm> vmsToMigrate,
			Set<FutureHost> validHosts) {
		List<Map<String, Object>> migrationMap = new LinkedList<Map<String, Object>>();
		
		FutureVmList.sortByCpuUtilization(vmsToMigrate);
		
		List<FutureMigration> currentMigrations = new ArrayList<>();
		for (FutureVm vm : vmsToMigrate) {
			FutureHost allocatedHost = this.vmOptimizerPolicy.futureVmMigrationPolicy.findHostForVm(vm, validHosts);
			if (allocatedHost != null) {
				allocatedHost.moveVm(vm);
				Log.printConcatLine("VM #", vm.getCurrentVm().getId(), " allocated to host #", allocatedHost.getCurrentHost().getId());

				Map<String, Object> migrate = new HashMap<String, Object>();
				migrate.put("vm", vm.getCurrentVm());
				migrate.put("host", allocatedHost.getCurrentHost());
				migrationMap.add(migrate);
			} else {
				Log.printLine("Not all VMs can be reallocated from the host, reallocation cancelled");

				// S'elimina el destroy ja que els hosts no sofreixen canvis, només els futurehosts
				//for (Map<String, Object> map : migrationMap) {
				//	((Host) map.get("host")).vmDestroy((Vm) map.get("vm"));
				//}
				migrationMap.clear();
				
				for(FutureMigration migration : currentMigrations) {
					migration.revertMigration();
				}
				break;
			}
		}
		return migrationMap;
	}
	
	public List<FutureHost> getSwitchedOffHosts(List<FutureHost> allHosts) {
		List<FutureHost> switchedOffHosts = new LinkedList<FutureHost>();
		for (FutureHost host : allHosts) {
			if (host.getFutureVms().size() == 0) {
				switchedOffHosts.add(host);
			}
		}
		return switchedOffHosts;
	}
	
	protected List<FutureVm> getVmsToMigrateFromUnderUtilizedHost(FutureHost host) {
		List<FutureVm> vmsToMigrate = new LinkedList<FutureVm>();
		for (FutureVm vm : host.getFutureVms().values()) {
			if (!vm.getCurrentVm().isInMigration()) {
				vmsToMigrate.add(vm);
			}
		}
		return vmsToMigrate;
	}
	
	// Original function
	protected boolean areAllVmsMigratingOutOrAnyVmMigratingIn(PowerHost host) {
		for (PowerVm vm : host.<PowerVm> getVmList()) {
			if (!vm.isInMigration()) {
				return false;
			}
			if (host.getVmsMigratingIn().contains(vm)) {
				return true;
			}
		}
		return true;
	}
	
	// Original function with Future adaptations
	protected List<FutureHost> extractHostListFromMigrationMap(List<Map<String, Object>> migrationMap) {
		List<FutureHost> hosts = new LinkedList<FutureHost>();
		for (Map<String, Object> map : migrationMap) {
			PowerHost powerHost = (PowerHost) map.get("host");
			hosts.add(this.vmOptimizerPolicy.futureHosts.get(powerHost.getNetworkId()));
		}
		return hosts;
	}
}
