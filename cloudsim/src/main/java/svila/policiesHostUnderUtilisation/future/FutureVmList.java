package svila.policiesHostUnderUtilisation.future;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.lists.VmList;

import svila.policiesVmOptimizer.future.FutureVm;

public class FutureVmList extends VmList {
	/**
	 * Sort a given list of VMs by cpu utilization.
	 * 
	 * @param vmList the vm list to be sorted
	 */
	public static <T extends FutureVm> void sortByCpuUtilization(List<T> vmList) {
		Collections.sort(vmList, new Comparator<T>() {

			@Override
			public int compare(T a, T b) throws ClassCastException {
				Double aUtilization = a.getSmoothingMIPS();
				Double bUtilization = b.getSmoothingMIPS();
				return bUtilization.compareTo(aUtilization);
			}
		});
	}
}
