package svila.policiesHostUnderUtilisation;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;

import svila.vmOptimizerFix.HostUnderUtilisationPolicy;

public class FUSDHostUnderUtilisationPolicy extends HostUnderUtilisationPolicy {

	double coldThresholdDefault = 0.25;
	double greenThresholdDefault = 0.40;
	double minRamDefault = 4096.0;
	
	@Override
	public List<Map<String, Object>> getMigrationMapFromUnderUtilizedHosts(
			List<PowerHostUtilizationHistory> overUtilizedHosts) {
		// TODO Auto-generated method stub
		return this.getVmOptimizerPolicy().getMigrationMapFromUnderUtilizedHostsOriginal(overUtilizedHosts);
	}

	protected PowerHost getUnderUtilizedHost(Set<? extends Host> excludedHosts) {
		Log.printLine("inside getUnderUtilizedHostsFUSD function");
		PowerHost underUtilizedHost = null;
		
		//double coldThreshold=0.25;
		//double greenThreshold = 0.40;
		//double minRam = 4096.0;
		double coldThreshold = coldThresholdDefault;
		double greenThreshold = greenThresholdDefault;
		double minRam = minRamDefault;
		
		
		List<PowerHost> hostList = this.getVmOptimizerPolicy().getHostList();
		for (PowerHost host : hostList) {
			if (excludedHosts.contains(host)) {
				continue;
			}
			double coldTempOfHost = host.coldTemperatureOfServer();
			double greenTempOfHost = host.greenComputingTemperatureOfServer();
			double currentRAM = host.getUtilizationOfRam();
			if ( (coldTempOfHost<= coldThreshold) && (greenTempOfHost <= greenThreshold) &&
				 (currentRAM <= minRam) && !areAllVmsMigratingOutOrAnyVmMigratingIn(host)) {
				minRam = currentRAM;
				underUtilizedHost=host;
								
			}
		}
		return underUtilizedHost;
	}
	
	/**
	 * Checks whether all vms are in migration.
	 * 
	 * @param host the host
	 * @return true, if successful
	 */
	protected boolean areAllVmsMigratingOutOrAnyVmMigratingIn(PowerHost host) {
		for (PowerVm vm : host.<PowerVm> getVmList()) {
			if (!vm.isInMigration()) {
				return false;
			}
			if (host.getVmsMigratingIn().contains(vm)) {
				return true;
			}
		}
		return true;
	}

	@Override
	public List<? extends Vm> getVmsToMigrateFromUnderUtilizedHost(PowerHost host) {
		// TODO Auto-generated method stub
		return this.getVmOptimizerPolicy().getVmsToMigrateFromUnderUtilizedHostOriginal(host);
	}

}
