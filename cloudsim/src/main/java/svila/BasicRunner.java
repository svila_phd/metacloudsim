package svila;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerDynamicWorkload;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.UtilizationModelNull;
import org.cloudbus.cloudsim.UtilizationModelPlanetLabInMemory;
import org.cloudbus.cloudsim.UtilizationModelStochastic;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmSchedulerTimeSharedOverSubscription;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.examples.power.Helper;
import org.cloudbus.cloudsim.examples.power.RunnerAbstract;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerHostUtilizationHistory;
import org.cloudbus.cloudsim.power.PowerVm;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;

/**
 *
 * @author Sergi
 */
public class BasicRunner extends RunnerAbstract {
    
    /**
	 * @param enableOutput
	 * @param outputToFile
	 * @param inputFolder
	 * @param outputFolder
	 * @param workload
	 * @param vmAllocationPolicy
	 * @param vmSelectionPolicy
	 * @param parameter
	 */
	public BasicRunner(
			boolean enableOutput,
			boolean outputToFile,
			String inputFolder,
			String outputFolder,
			String workload,
			String vmAllocationPolicy,
			String vmSelectionPolicy,
			String parameter) {
		super(
				enableOutput,
				outputToFile,
				inputFolder,
				outputFolder,
				workload,
				vmAllocationPolicy,
				vmSelectionPolicy,
				parameter);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cloudbus.cloudsim.examples.power.RunnerAbstract#init(java.lang.String)
	 */
	@Override
	protected void init(String inputFolder) {
		try {
                        System.out.println("Input folder: " + inputFolder);
			CloudSim.init(1, Calendar.getInstance(), false);

			broker = Helper.createBroker();
			int brokerId = broker.getId();
                        
                        Experiment experiment = new Experiment1();

			cloudletList = experiment.createCustomCloudletList(brokerId);
                        /*
                        Es creen tantes VM com cloudlets, ja que inicialment és com si cada VM tingués
                        una aplicació funcionant, els cloudlets variarian les seves caracerístiques
                        al llarg del temps, provocant under o overutilization, fent que el cloudlet migri
                        a una altra VM en funció de les necessitats i en base a la VmSeletionPolicy.
                        Totes les VM acabaran al mateix temps, els cloudlets no funcionen en base a uns MIPS
                        que s'han de consumir, sino en base a un temps de funcionament
                        */
                        vmList = experiment.createCustomVmList(brokerId, cloudletList.size());
			hostList = experiment.createCustomHostList();
		} catch (Exception e) {
			e.printStackTrace();
			Log.printLine("The simulation has been terminated due to an unexpected error");
			System.exit(0);
		}
	}
        
        /*private List<Cloudlet> createCustomCloudletList(int brokerId) throws NumberFormatException, IOException {
            List<Cloudlet> list = new ArrayList<>();
            addCloudlet(0, BSC.CLOUDLET1PATH, brokerId, list);
            addCloudlet(1, BSC.CLOUDLET2PATH, brokerId, list);
            return list;
        }
        
        private void addCloudlet(int id, String cloudletPath, int brokerId, List<Cloudlet> list) throws NumberFormatException, IOException {
            Cloudlet cloudlet = new Cloudlet(
						id,
						BSC.CLOUDLET_LENGTH,
						BSC.CLOUDLET_PES,
						300,
						300,
                                                //utilizationModelCpu
                                                    //new UtilizationModelStochastic(),
                                                    new UtilizationModelPlanetLabInMemory(
                                                        cloudletPath,
                                                        BSC.SCHEDULING_INTERVAL, 11),
                                                    //utilizationModelNull,
                                                //utilizationModelRam
                                                    new UtilizationModelNull(),
                                                //utilizationModelBw
                                                    new UtilizationModelNull()
                                                );
            cloudlet.setUserId(brokerId);
            cloudlet.setVmId(id);
            list.add(cloudlet);
        }
        
        private List<Cloudlet> createCloudletList(int brokerId, int cloudletsNumber) throws NumberFormatException, IOException {
            List<Cloudlet> list = new ArrayList<>();

		long fileSize = 300;
		long outputSize = 300;
		long seed = BSC.CLOUDLET_UTILIZATION_SEED;
		UtilizationModel utilizationModelNull = new UtilizationModelNull();
                UtilizationModel utilizationModelFull = new UtilizationModelFull();

		for (int i = 0; i < cloudletsNumber; i++) {
			Cloudlet cloudlet = null;
			if (seed == -1) {
                            /*  UtilizationModel utilizationModelCpu,
                                UtilizationModel utilizationModelRam,
                                UtilizationModel utilizationModelBw*/
	/*			cloudlet = new Cloudlet(
						i,
						BSC.CLOUDLET_LENGTH,
						BSC.CLOUDLET_PES,
						fileSize,
						outputSize,
                                                //utilizationModelCpu
                                                    //new UtilizationModelStochastic(),
                                                    new UtilizationModelPlanetLabInMemory(
                                                        BSC.CLOUDLET1PATH,
                                                        BSC.SCHEDULING_INTERVAL),
                                                    //utilizationModelNull,
                                                //utilizationModelRam
                                                    utilizationModelNull,
                                                //utilizationModelBw
                                                    utilizationModelNull
                                                );
			} else {
				cloudlet = new Cloudlet(
						i,
						BSC.CLOUDLET_LENGTH,
						BSC.CLOUDLET_PES,
						fileSize,
						outputSize,
						new UtilizationModelStochastic(seed * i),
						utilizationModelNull,
						utilizationModelNull);
			}
			cloudlet.setUserId(brokerId);
			cloudlet.setVmId(i);
			list.add(cloudlet);
		}

		return list;
        }

        private List<Vm> createCustomVmList(int brokerId, int vmsNumber) {
            List<Vm> vms = new ArrayList<>();
            addPowerVm(0, brokerId, 3, vms);
            addPowerVm(1, brokerId, 3, vms);
            return vms;
        }
        
        private List<Vm> createVmList(int brokerId, int vmsNumber) {
		List<Vm> vms = new ArrayList<>();
		for (int i = 0; i < vmsNumber; i++) {
			int vmType = i / (int) Math.ceil((double) vmsNumber / BSC.VM_TYPES);
			addPowerVm(i, brokerId, vmType, vms);
		}
		return vms;
	}
        
        private void addPowerVm(int id, int brokerId, int vmType, List<Vm> vms) {
            vms.add(new PowerVm(
					id,
					brokerId,
					BSC.VM_MIPS[vmType],
					BSC.VM_PES[vmType],
					BSC.VM_RAM[vmType],
					BSC.VM_BW,
					BSC.VM_SIZE,
					1,
					"Xen",
					new CloudletSchedulerDynamicWorkload(BSC.VM_MIPS[vmType], BSC.VM_PES[vmType]),
					BSC.SCHEDULING_INTERVAL));
        }
        
        private List<PowerHost> createCustomHostList() {
            List<PowerHost> hostList = new ArrayList<PowerHost>();
            addPowerHostUtilizationHistory(0, 0, hostList);
            addPowerHostUtilizationHistory(1, 0, hostList);
            return hostList;
        }
        
        private List<PowerHost> createHostList(int hostsNumber) {
		List<PowerHost> hostList = new ArrayList<PowerHost>();
		for (int i = 0; i < hostsNumber; i++) {
			int hostType = i % BSC.HOST_TYPES;

			addPowerHostUtilizationHistory(i, hostType, hostList);
		}
		return hostList;
	}
        
        private void addPowerHostUtilizationHistory(int id, int hostType, List<PowerHost> hostList) {
            List<Pe> peList = new ArrayList<Pe>();
			for (int j = 0; j < BSC.HOST_PES[hostType]; j++) {
				peList.add(new Pe(j, new PeProvisionerSimple(BSC.HOST_MIPS[hostType])));
			}
            
            hostList.add(new PowerHostUtilizationHistory(
					id,
					new RamProvisionerSimple(BSC.HOST_RAM[hostType]),
					new BwProvisionerSimple(BSC.HOST_BW),
					BSC.HOST_STORAGE,
					peList,
					new VmSchedulerTimeSharedOverSubscription(peList),
					BSC.HOST_POWER[hostType]));
        }*/
}
