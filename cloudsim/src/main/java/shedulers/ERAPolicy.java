package shedulers;

import java.util.ArrayList;
import java.util.Collections;

import geneticsimulator.Job;
import gridsim.Machine;
import gridsim.MachineList;
import gridsim.PE;
import gridsim.ResGridlet;
import simulator.EnergyMachine;
import simulator.EnergyPolicy;
import simulator.ResJob;
import util.MachinesComparatorByEnergyPower;


public class ERAPolicy extends EnergyPolicy {
	 
	public ERAPolicy(String resourceName, String entityName) throws Exception {
		super(resourceName, entityName);
		System.out.println("Policy "+ entityName);
	}

	@Override
	protected void orderWaitingQueue() {		
		//As FCFS Politic no order is required
	}
	@Override
	protected boolean howToAllocatePEtoGridlet(ResGridlet rgl) {
		MachineList l=super.resource_.getMachineList();
		int requirements=rgl.getNumPE();
		if(requirements>super.resource_.getNumFreePE()){
			return false;
		}
		ArrayList<Integer> allocMachines=new ArrayList<Integer>();
		
		while(requirements>0){
			double minEnergy=Integer.MAX_VALUE;
			int allocMachine=0;
			for(int i=0;i<l.size();i++){
				if(l.get(i).getNumFreePE()>0 && !allocMachines.contains(l.get(i).getMachineID())){
					double currentEnergy=super.calculateExecTimeForMachine((ResJob) rgl,l.get(i).getMachineID())*((EnergyMachine)l.get(i)).getEnergyComputing();
					if(currentEnergy<minEnergy){
						minEnergy=currentEnergy;
						allocMachine=l.get(i).getMachineID();
					}
				}
			}
			requirements-=l.getMachine(allocMachine).getNumFreePE();
			allocMachines.add(allocMachine);
		}	
		if(requirements>0){
			return false;
		}
		requirements=rgl.getNumPE();
		for(Integer machineID : allocMachines){
			Machine m=l.getMachine(machineID);
			int freeNodes=m.getNumFreePE();
			for(int i=0;i<freeNodes&&requirements>0;i++,requirements--){
				int freeM = m.getMachineID();
				int freePE = m.getPEList().getFreePEID();
				rgl.setMachineAndPEID(freeM, freePE);
				super.changeStatusPE(PE.BUSY, freeM, freePE);
			}
		}
		return true;	
	}
}
