package shedulers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAIIBuilder;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.SolutionListEvaluator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;
import org.uma.jmetal.util.fileoutput.SolutionSetOutput;
import org.uma.jmetal.util.fileoutput.impl.DefaultFileOutputContext;

import genetic.JobCrossover;
import genetic.JobData;
import genetic.JobSwapMutation;
import genetic.NSGALog;
import genetic.SchedulingProblemMultiObjective;
import genetic.SchedulingSolution;
import geneticsimulator.Cluster;
import geneticsimulator.Job;
import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;
import gridsim.Machine;
import gridsim.MachineList;
import gridsim.PE;
import gridsim.ResGridlet;
import simulator.EnergyMachine;
import simulator.EnergyPolicy;
import simulator.ResJob;
import util.MachinesComparatorByEnergyConsumition;
import util.MachinesComparatorByNodePower;
import geneticsimulator.BadAllocationException;

public class MOGAPolicy extends EnergyPolicy {
	private int packageSize=20;
	private int generations=300;
	private int population=500;
	private int probability_cx=80;
	private int probability_mu=30;
	private String evaluationMetric="m";
	private String output ="./";
	private String idProva="0";
	public MOGAPolicy(String resourceName, String entityName,String[] args,String output,String idProva) throws Exception {
		super(resourceName, entityName);
		this.output=output;
		this.idProva=idProva;
		System.out.println("Sergi"); // Sergi
		for(String s : args) {
			System.out.println(s);
		}
		if(args!=null && args.length>5){
			try{
				packageSize=Integer.parseInt(args[0]);
				generations=Integer.parseInt(args[1]);
				population=Integer.parseInt(args[2]);
				probability_cx=Integer.parseInt(args[3]);
				probability_mu=Integer.parseInt(args[4]);
				evaluationMetric=args[5];
			}catch(NumberFormatException e){
				System.out.println("The parameters are not correct");
				System.exit(0);
			}
		}
		System.out.println("Policy "+ entityName);
		System.out.println("Package Size "+packageSize);
		System.out.println("Num Generations "+generations);
		System.out.println("Population Size "+population);
		System.out.println("Probability of Crosover "+probability_cx);
		System.out.println("Probability of Mutation "+probability_mu);
		System.out.println("Evaluation Metric "+evaluationMetric);

	}

	@Override
	protected void orderWaitingQueue() {
		//If we have preAllocatedJobs, the first jobs in the queue are already ordered.
		if(((ResJob)this.gridletQueueList_.getFirst()).isPreAllocated()){
			return;
		}
		//We have jobs in the queue and we want to order and preAllocate them.
		ArrayList<ResJob> listToOrder=new ArrayList<ResJob>();
		ArrayList<Job> processingJobs=new ArrayList<Job>();
		ArrayList<Cluster>multi=new ArrayList<Cluster>();
		
		while(!this.gridletQueueList_.isEmpty()&& listToOrder.size()<this.packageSize){
			//Random r=new Random();
			//ResJob rj=(ResJob)this.gridletQueueList_.remove(r.nextInt(gridletQueueList_.size()));
			ResJob rj= (ResJob)this.gridletQueueList_.remove();
			listToOrder.add(rj);
			processingJobs.add(new Job(rj.getGridletID(),rj.getNumPE(),rj.getGridletLength(),rj.getSigma(),rj.getPPBW()));
		}
				
		for(Machine m :super.resource_.getMachineList()){
			EnergyMachine em=(EnergyMachine)m;
			multi.add(new Cluster(m.getMachineID(),m.getNumPE(),(m.getMIPSRating()/m.getNumPE()),em.getMaxBW(),em.getEnergyIdle(),em.getEnergyComputing(),em.getEnergyStart(),em.getEnergyStop()));
		}
		
		//get the current stat of the simulator
		HashMap<Job,ArrayList<Integer>> inExec=new HashMap<Job,ArrayList<Integer>>();
		for(ResGridlet rg:this.gridletInExecList_){
			ResJob rj=(ResJob)rg;
		    Job job=new Job(rj.getGridletID(),rj.getNumPE(),rj.getRemainingGridletLength(),rj.getSigma(),rj.getPPBW());
		    ArrayList<Integer> alloc=new ArrayList<Integer>();
		    if(rj.getNumPE()>1){
		    	for(int i:rj.getListMachineID()){
		    		alloc.add(new Integer(i));
		    	}
		    }else{
		    	alloc.add(new Integer(rj.getMachineID()));
		    }
		    inExec.put(job,alloc);
		}
				
		JobList.setInstance(processingJobs);
		try {
			MultiCluster.setInstance(multi, inExec);
		} catch (BadAllocationException e1) {
			e1.printStackTrace();
			System.exit(1);
		}    	
		
		//Execute the GA
		 SchedulingProblemMultiObjective problem;
		 Algorithm<List<SchedulingSolution>> algorithm;
		 CrossoverOperator<SchedulingSolution> crossover;
		 MutationOperator<SchedulingSolution> mutation;
		 SelectionOperator<List<SchedulingSolution>, SchedulingSolution> selection;
		 SolutionListEvaluator<SchedulingSolution> evaluator;
		 evaluator = new SequentialSolutionListEvaluator<SchedulingSolution>();	
		 problem = new SchedulingProblemMultiObjective(JobList.instance(),MultiCluster.instance());
		 crossover = new JobCrossover() ;
		 mutation = new JobSwapMutation();
		 selection = new BinaryTournamentSelection<SchedulingSolution>(new RankingAndCrowdingDistanceComparator<SchedulingSolution>());
		 algorithm = (Algorithm<List<SchedulingSolution>>) new NSGALog(problem,this.generations,this.population,crossover,mutation,selection, evaluator,this.output+"ConvergenceE"+this.evaluationMetric+idProva+".csv");
		 AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
		 .execute();
		 List<SchedulingSolution> population = algorithm.getResult() ;
		 long computingTime = algorithmRunner.getComputingTime() ;
		 ArrayList<SchedulingSolution> solutions = new ArrayList<SchedulingSolution>();
		 solutions.add(population.get(0));
		 /*for(SchedulingSolution p: population){
			 ArrayList<SchedulingSolution> dominated=new ArrayList<SchedulingSolution>();
			 boolean nonDominated=true;
			 for(SchedulingSolution s:solutions){
				 if(p.getObjective(0)<=s.getObjective(0) && p.getObjective(1)<=s.getObjective(1)){
					 dominated.add(s);
				 }
				 if(s.getObjective(0)<=p.getObjective(0) && s.getObjective(1)<=p.getObjective(1)){
					 nonDominated=false;
				 }
			 }
			 for(SchedulingSolution s:dominated){
				 solutions.remove(s);
			 }
			 if(nonDominated){
				 solutions.add(p);
			 }
		 }*/	 
		 new SolutionSetOutput.Printer(solutions)
		 .setSeparator("\t")
		 .setVarFileOutputContext(new DefaultFileOutputContext(this.output+"Var"+this.evaluationMetric+idProva+".csv"))
		 .setFunFileOutputContext(new DefaultFileOutputContext(this.output+"Fun"+this.evaluationMetric+idProva+".csv"))
		 .print();
		 SchedulingSolution result=null;
		 double mk=Double.MAX_VALUE;
		 for(SchedulingSolution p :solutions){
			 int objective;
			 if(this.evaluationMetric=="m"){
				 objective=0;
			 }else{
				 objective=1;
			 }
			 if(p.getObjective(objective)<mk){
				 result=p;
				 mk=p.getObjective(objective);
			 }
		 }

		 JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");
		 JMetalLogger.logger.info("Objectives values have been written to file FUN.tsv");
		 JMetalLogger.logger.info("Variables values have been written to file VAR.tsv");
		 
		 MultiCluster mc=MultiCluster.instance();
		 for(int i=result.getNumberOfVariables()-1;i>=0;i--){
			 JobData jd=result.getVariableValue(i);
			 for(ResJob rj : listToOrder){
				 if(jd.job.getId()==rj.getGridletID()){
					 int[] allocPreferences=new int[mc.size()];
					 for(int j=0;j<mc.size();j++){
						 allocPreferences[j]=(int) ((1-jd.forbidenNodes[j])*super.resource_.getMachineList().getMachineInPos(j).getNumPE());
					 }
					 rj.setAllocationPreferences(allocPreferences);
					 rj.preAllcoate();
					 this.gridletQueueList_.addFirst(rj);
					 break;
				 }
			  }
		 }	
	}

	@Override
	protected boolean howToAllocatePEtoGridlet(ResGridlet rgl) {
		ResJob rj=(ResJob)rgl;
		//order the machines for they relative power
		MachineList list =super.resource_.getMachineList();
		
		HashMap <Machine,Integer> allowedNodes=new HashMap<Machine,Integer>();
		int availableNodes=0;
		for(int i=0;i<list.size();i++){
			Machine m=list.get(i);
			int nodes= m.getNumFreePE();
			if(rj.getAllocationPreferences()!=null){
				nodes=Math.min(m.getNumFreePE(),rj.getAllocationPreferences()[i]);
			}
			allowedNodes.put(m,nodes);
			availableNodes+=nodes;
		}
		if(availableNodes<rj.getNumPE()){
			return false;
		}
		Collections.sort(list,Collections.reverseOrder(new MachinesComparatorByNodePower()));
		//Allocate to the best resources(unused best resources free)
		int freeMachines=0;
		int machineStartId=0;
		//search for the first machine to start allocating the job.(Start for the most powerful)
		for(int i=0;i<list.size();i++){
			Machine m=list.get(i);
			freeMachines+=allowedNodes.get(m);
			//if there are enough resources finish. if not, add the nodes in the next ordered machine.
			if(rj.getNumPE()<=freeMachines){
				machineStartId=i;
				break;
			}
		}
		// here the machineStartId is pointing to the less powerful machine required to run the job.
		for(int i=0;i<rj.getNumPE();i++){
			Machine m=list.getMachineInPos(machineStartId);
			int aNodes=allowedNodes.get(m);
			while(aNodes == 0){
				//if we run out of nodes in this machine, we take some from more powerful machines.
				m=list.get(--machineStartId);
				aNodes=allowedNodes.get(m);

			}
			int machineID=m.getMachineID();
			int peID=m.getPEList().getFreePEID();
			rj.setMachineAndPEID(machineID, peID);
			super.changeStatusPE(PE.BUSY, machineID, peID);
			allowedNodes.put(m, aNodes-1);
		}
		return true;
	}
}
