package shedulers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAIIBuilder;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.fileoutput.SolutionSetOutput;
import org.uma.jmetal.util.fileoutput.impl.DefaultFileOutputContext;

import PSO.SchedulingPSO;
import PSO.SchedulingPSOAlloc;
import genetic.JobCrossover;
import genetic.JobData;
import genetic.JobSwapMutation;
import genetic.SchedulingProblem;
import genetic.SchedulingProblemEnergy;
import genetic.SchedulingProblemMakespan;
import genetic.SchedulingProblemMultiObjective;
import genetic.SchedulingSolution;
import geneticsimulator.Cluster;
import geneticsimulator.Job;
import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;
import gridsim.Machine;
import gridsim.MachineList;
import gridsim.PE;
import gridsim.ResGridlet;
import simulator.EnergyMachine;
import simulator.EnergyPolicy;
import simulator.ResJob;
import util.MachinesComparatorByEnergyConsumition;
import util.MachinesComparatorByNodePower;
import geneticsimulator.BadAllocationException;

public class PSOPolicy extends EnergyPolicy {
	private int packageSize=20;
	private int iterations=300;
	private int swarmSize=500;
	private float pmass=0.3f;
	private float lmass=0.4f;
	private float gmass=0.2f;
	private String metric="m";
	private String output ="./";
	private String idProva="0";
	public PSOPolicy(String resourceName, String entityName,String[] args,String output,String idProva) throws Exception {
		super(resourceName, entityName);
		this.output=output;
		this.idProva=idProva;
		if(args!=null && args.length>5){
			try{
				packageSize=Integer.parseInt(args[0]);
				iterations=Integer.parseInt(args[1]);
				swarmSize=Integer.parseInt(args[2]);
				pmass=Float.parseFloat(args[3]);
				lmass=Float.parseFloat(args[4]);
				gmass=Float.parseFloat(args[5]);
				metric=args[6];
			}catch(NumberFormatException e){
				System.out.println("The parameters are not correct");
				System.exit(0);
			}
		}
		System.out.println("Policy "+ entityName);
		System.out.println("Package Size "+packageSize);
		System.out.println("Num Iterations "+iterations);
		System.out.println("Swarm Size "+swarmSize);
		System.out.println("Particle mass "+pmass);
		System.out.println("Local best mass "+lmass);
		System.out.println("Global best mass "+gmass);
		System.out.println("Metric "+metric);
	}

	@Override
	protected void orderWaitingQueue() {
		//If we have preAllocatedJobs, the first jobs in the queue are already ordered.
		if(((ResJob)this.gridletQueueList_.getFirst()).isPreAllocated()){
			return;
		}
		//We have jobs in the queue and we want to order and preAllocate them.
		ArrayList<ResJob> listToOrder=new ArrayList<ResJob>();
		ArrayList<Job> processingJobs=new ArrayList<Job>();
		ArrayList<Cluster>multi=new ArrayList<Cluster>();
		
		while(!this.gridletQueueList_.isEmpty()&& listToOrder.size()<packageSize){
			//Random r=new Random();
			//ResJob rj=(ResJob)this.gridletQueueList_.remove(r.nextInt(gridletQueueList_.size()));
			ResJob rj= (ResJob)this.gridletQueueList_.remove();
			listToOrder.add(rj);
			processingJobs.add(new Job(rj.getGridletID(),rj.getNumPE(),rj.getGridletLength(),rj.getSigma(),rj.getPPBW()));
		}
				
		for(Machine m :super.resource_.getMachineList()){
			EnergyMachine em=(EnergyMachine)m;
			multi.add(new Cluster(m.getMachineID(),m.getNumPE(),(m.getMIPSRating()/m.getNumPE()),em.getMaxBW(),em.getEnergyIdle(),em.getEnergyComputing(),em.getEnergyStart(),em.getEnergyStop()));
		}
		
		//get the current stat of the simulator
		HashMap<Job,ArrayList<Integer>> inExec=new HashMap<Job,ArrayList<Integer>>();
		
		for(ResGridlet rg:this.gridletInExecList_){
			ResJob rj=(ResJob)rg;
		    Job job=new Job(rj.getGridletID(),rj.getNumPE(),rj.getRemainingGridletLength(),rj.getSigma(),rj.getPPBW());
		    ArrayList<Integer> alloc=new ArrayList<Integer>();
		    if(rj.getNumPE()>1){
		    	for(int i:rj.getListMachineID()){
		    		alloc.add(new Integer(i));
		    	}
		    }else{
		    	alloc.add(new Integer(rj.getMachineID()));
		    }
		    inExec.put(job,alloc);
		}
				
		JobList.setInstance(processingJobs);
		try {
			MultiCluster.setInstance(multi, inExec);
		} catch (BadAllocationException e1) {
			e1.printStackTrace();
			System.exit(1);
		}    	
		
		//Execute the PSO
		 SchedulingProblem problem;
		 Algorithm<List<SchedulingSolution>> algorithm;
		 if(metric.equals("e")){  
			 problem = new SchedulingProblemEnergy(JobList.instance(),MultiCluster.instance());
		 }else if(metric.equals("m")){
			 problem = new SchedulingProblemMakespan(JobList.instance(),MultiCluster.instance());			 
		 }else{
			 problem=null;
			 System.out.println("Unespecified metric");
			 System.exit(0);
		 }
		 algorithm = new SchedulingPSOAlloc(problem, this.swarmSize, this.iterations,this.pmass,this.gmass,this.lmass);	 
		 AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
		 .execute();
		 List<SchedulingSolution> population = algorithm.getResult() ;
		 System.out.println(population.size());
		 long computingTime = algorithmRunner.getComputingTime() ;
		 ArrayList<SchedulingSolution> solutions = new ArrayList<SchedulingSolution>();
		 solutions.add(population.get(0));
		 SchedulingSolution result=null;
		 double mk=Double.MAX_VALUE;
		 if(solutions.size()==0){
			 System.out.println("No Solution Found");
			 System.exit(1);
		 }
		 result=solutions.get(0);
		  new SolutionSetOutput.Printer(population)
          .setSeparator("\t")
          .setVarFileOutputContext(new DefaultFileOutputContext("VAR.tsv"))
          .setFunFileOutputContext(new DefaultFileOutputContext("FUN.tsv"))
          .print();
		 JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");
		 JMetalLogger.logger.info("Objectives values have been written to file FUN.tsv");
		 JMetalLogger.logger.info("Variables values have been written to file VAR.tsv");
		 
		 MultiCluster mc=MultiCluster.instance();
		 for(int i=result.getNumberOfVariables()-1;i>=0;i--){
			 JobData jd=result.getVariableValue(i);
			 for(ResJob rj : listToOrder){
				 if(jd.job.getId()==rj.getGridletID()){
					 int[] allocPreferences=new int[mc.size()];
					 for(int j=0;j<mc.size();j++){
						 allocPreferences[j]=(int) ((1-jd.forbidenNodes[j])*super.resource_.getMachineList().getMachineInPos(j).getNumPE());
					 }
					 rj.setAllocationPreferences(allocPreferences);
					 rj.preAllcoate();
					 this.gridletQueueList_.addFirst(rj);
					 break;
				 }
			  }
		 }	
	}

	@Override
	protected boolean howToAllocatePEtoGridlet(ResGridlet rgl) {
		ResJob rj=(ResJob)rgl;
		//order the machines for they relative power
		MachineList list =super.resource_.getMachineList();
		
		HashMap <Machine,Integer> allowedNodes=new HashMap<Machine,Integer>();
		int availableNodes=0;
		for(int i=0;i<list.size();i++){
			Machine m=list.get(i);
			int nodes= m.getNumFreePE();
			if(rj.getAllocationPreferences()!=null){
				nodes=Math.min(m.getNumFreePE(),rj.getAllocationPreferences()[i]);
			}
			allowedNodes.put(m,nodes);
			availableNodes+=nodes;
		}
		if(availableNodes<rj.getNumPE()){
			return false;
		}
		Collections.sort(list,Collections.reverseOrder(new MachinesComparatorByNodePower()));
		//Allocate to the best resources(unused best resources free)
		int freeMachines=0;
		int machineStartId=0;
		//search for the first machine to start allocating the job.(Start for the most powerful)
		for(int i=0;i<list.size();i++){
			Machine m=list.get(i);
			freeMachines+=allowedNodes.get(m);
			//if there are enough resources finish. if not, add the nodes in the next ordered machine.
			if(rj.getNumPE()<=freeMachines){
				machineStartId=i;
				break;
			}
		}
		// here the machineStartId is pointing to the less powerful machine required to run the job.
		for(int i=0;i<rj.getNumPE();i++){
			Machine m=list.getMachineInPos(machineStartId);
			int aNodes=allowedNodes.get(m);
			while(aNodes == 0){
				//if we run out of nodes in this machine, we take some from more powerful machines.
				m=list.get(--machineStartId);
				aNodes=allowedNodes.get(m);

			}
			int machineID=m.getMachineID();
			int peID=m.getPEList().getFreePEID();
			rj.setMachineAndPEID(machineID, peID);
			super.changeStatusPE(PE.BUSY, machineID, peID);
			allowedNodes.put(m, aNodes-1);
		}
		return true;
	}
}
