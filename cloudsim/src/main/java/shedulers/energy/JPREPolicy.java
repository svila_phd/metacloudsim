package shedulers.energy;

import java.util.Collections;

import gridsim.Machine;
import gridsim.MachineList;
import gridsim.PE;
import gridsim.ResGridlet;
import simulator.EnergyPolicy;
import util.MachinesComparatorByEnergyPower;
import util.MachinesComparatorByNodePower;


public class JPREPolicy  extends EnergyPolicy {
	 
	public JPREPolicy(String resourceName, String entityName) throws Exception {
		super(resourceName, entityName);
		System.out.println("Policy "+ entityName);
	}

	@Override
	protected void orderWaitingQueue() {		
		//As FCFS Politic no order is required
	}


	@Override
	protected boolean howToAllocatePEtoGridlet(ResGridlet rgl) {
		MachineList l=super.resource_.getMachineList();
		
		Collections.sort(l,Collections.reverseOrder(new MachinesComparatorByEnergyPower()));
		
		int requirements=rgl.getNumPE();
		for(Machine m:l){
			for(;requirements>0;requirements--){
				if(m.getNumFreePE()>0){
					int freeM = m.getMachineID();
					int freePE = m.getPEList().getFreePEID();
					rgl.setMachineAndPEID(freeM, freePE);
					super.changeStatusPE(PE.BUSY, freeM, freePE);
				}else{
					break;
				}
			}
		}
		return true;
	}
}
