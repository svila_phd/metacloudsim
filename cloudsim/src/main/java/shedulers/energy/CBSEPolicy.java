package shedulers.energy;

import java.util.Collections;

import gridsim.Machine;
import gridsim.MachineList;
import gridsim.PE;
import gridsim.ResGridlet;
import simulator.EnergyPolicy;
import util.MachinesComparatorByEnergyPower;
import util.MachinesComparatorByFreePE;


public class CBSEPolicy extends EnergyPolicy {
	 
	public CBSEPolicy(String resourceName, String entityName) throws Exception {
		super(resourceName, entityName);
		System.out.println("Policy "+ entityName);
	}

	@Override
	protected void orderWaitingQueue() {		
		//As FCFS Politic no order is required
	}


	@Override
	protected boolean howToAllocatePEtoGridlet(ResGridlet rgl) {
		MachineList l=super.resource_.getMachineList();
		
		Collections.sort(l,Collections.reverseOrder(new MachinesComparatorByEnergyPower()));
		
		int requirements=rgl.getNumPE();
		int minCoAlloc=1;
		for(int i=0;i<l.size();i++){
			if(l.getMachineInPos(i).getNumPE()<requirements){
				minCoAlloc++;
				requirements-=l.getMachineInPos(i).getNumPE();
			}else{
				break;
			}
		}
		requirements=rgl.getNumPE();
		
		for(int i=0;i<minCoAlloc;i++){
			if(l.getMachineInPos(i).getNumFreePE()<requirements*0.7 && l.getMachineInPos(0).getNumBusyPE()>0){
				return false;
			}
		}
		
		
		requirements=rgl.getNumPE();
		for(Machine m:l){
			for(;requirements>0;requirements--){
				if(m.getNumFreePE()>0){
					int freeM = m.getMachineID();
					int freePE = m.getPEList().getFreePEID();
					rgl.setMachineAndPEID(freeM, freePE);
					super.changeStatusPE(PE.BUSY, freeM, freePE);
				}else{
					break;
				}
			}
		}
		return true;
	}
}
