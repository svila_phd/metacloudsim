package shedulers.energy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import geneticsimulator.BadAllocationException;
import geneticsimulator.Cluster;
import geneticsimulator.Job;
import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;
import gridsim.Machine;
import gridsim.MachineList;
import gridsim.PE;
import gridsim.ResGridlet;
import simulator.EnergyMachine;
import simulator.EnergyPolicy;
import simulator.ResJob;
import util.MachinesComparatorByEnergyConsumition;
import util.MachinesComparatorByEnergyPower;

public class MinMinPolicy extends EnergyPolicy {

	public MinMinPolicy(String resourceName, String entityName, String[] policyModificator)
			throws Exception {
		super(resourceName, entityName);
	}

	@Override
	protected void orderWaitingQueue() {
		if(((ResJob)this.gridletQueueList_.getFirst()).isPreAllocated()){
			return;
		}
		//We have jobs in the queue and we want to order and preAllocate them.
		ArrayList<ResJob> listToOrder=new ArrayList<ResJob>();
		ArrayList<Job> processingJobs=new ArrayList<Job>();
		ArrayList<Cluster>multi=new ArrayList<Cluster>();
		
		while(!this.gridletQueueList_.isEmpty()){
			ResJob rj= (ResJob)this.gridletQueueList_.remove();
			listToOrder.add(rj);
			processingJobs.add(new Job(rj.getGridletID(),rj.getNumPE(),rj.getGridletLength(),rj.getSigma(),rj.getPPBW()));
		}
				
		for(Machine m :super.resource_.getMachineList()){
			EnergyMachine em=(EnergyMachine)m;
			multi.add(new Cluster(m.getMachineID(),m.getNumPE(),(m.getMIPSRating()/m.getNumPE()),em.getMaxBW(),em.getEnergyIdle(),em.getEnergyComputing(),em.getEnergyStart(),em.getEnergyStop()));
		}
		
		//get the current stat of the simulator
		HashMap<Job,ArrayList<Integer>> inExec=new HashMap<Job,ArrayList<Integer>>();
		
		for(ResGridlet rg:this.gridletInExecList_){
			ResJob rj=(ResJob)rg;
		    Job job=new Job(rj.getGridletID(),rj.getNumPE(),rj.getRemainingGridletLength(),rj.getSigma(),rj.getPPBW());
		    ArrayList<Integer> alloc=new ArrayList<Integer>();
		    if(rj.getNumPE()>1){
		    	for(int i:rj.getListMachineID()){
		    		alloc.add(new Integer(i));
		    	}
		    }else{
		    	alloc.add(new Integer(rj.getMachineID()));
		    }
		    inExec.put(job,alloc);
		}
				
		JobList.setInstance(processingJobs);
		try {
			MultiCluster.setInstance(multi, inExec);
		} catch (BadAllocationException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		ArrayList<ResJob> orderedList=this.orderAndAllocateListUsingMinMin(listToOrder);
		Collections.reverse(orderedList);
		for(ResJob rj:orderedList){
			rj.preAllcoate();
			this.gridletQueueList_.addFirst(rj);
		}
	}
	

	@Override
	protected boolean howToAllocatePEtoGridlet(ResGridlet rgl) {
		ResJob rj=(ResJob)rgl;
		//order the machines for they relative power
		MachineList list =super.resource_.getMachineList();
		Collections.sort(list,new MachinesComparatorByEnergyPower());
		int resources=0;
		int freeResources=0;
		for(int i=0;i<list.size()-1;i++){
			resources+=list.getMachineInPos(i).getNumPE();
			freeResources+=list.getMachineInPos(i).getNumFreePE();
		}
		
		if(rj.getNumPE()<=resources && rj.getNumPE()>freeResources){
			return false;
		}
		// here the machineStartId is pointing to the less powerful machine required to run the job.
		int machineStartId=0;
		for(int i=0;i<rj.getNumPE();i++){
			Machine m=list.getMachineInPos(machineStartId);		
			while(m.getNumFreePE()==0){
				//if we run out of nodes in this machine, we take some from more powerful machines.
				m=list.getMachineInPos(++machineStartId);
			}
			int machineID=m.getMachineID();
			int peID=m.getPEList().getFreePEID();
			rj.setMachineAndPEID(machineID, peID);
			super.changeStatusPE(PE.BUSY, machineID, peID);
		}
		return true;
	}
	
	
	public ArrayList<ResJob> orderAndAllocateListUsingMinMin(ArrayList<ResJob>list){
		ArrayList<ResJob> orderedQueue=new ArrayList<ResJob>();
		//set up the simulator for the prediction.
		MultiCluster mc=null;
		ArrayList<Cluster> nodes=new ArrayList<Cluster>();
		for(Machine m : super.resource_.getMachineList()){
			EnergyMachine em=(EnergyMachine)m;
			nodes.add(new Cluster(m.getMachineID(),m.getNumPE(),(m.getMIPSRating()/m.getNumPE()),em.getMaxBW(),em.getEnergyIdle(),em.getEnergyComputing(),em.getEnergyStart(),em.getEnergyStop()));
		}
		
		try {
			MultiCluster.setInstance(nodes,null);
			mc=MultiCluster.instance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//add the current executing jobs.
		for(ResGridlet rg:this.gridletInExecList_){
			ResJob rj=(ResJob)rg;
			Job j=new Job(rj.getGridletID(), rj.getNumPE(),rj.getRemainingGridletLength(),rj.getSigma(), rj.getPPBW());
			Integer[] machines=new Integer[rj.getNumPE()];
			if(rj.getNumPE()==1){
				machines[0]=rj.getMachineID();
			}else{
				for(int i=0;i<rj.getListMachineID().length;i++){
					machines[i]=rj.getListMachineID()[i];
				}
			}
			if(!mc.sheduleWithAllocation(j,machines)){
					System.out.println("Extrange bug");
			}
		}
		
		//search for the job with minimum execution time
		while(list.size()>0){	
			Iterator<ResJob> it=list.iterator();
			ResJob best=null;
			Job bestsim=null;
			int bigerSize=0;
			double minCT=Double.MAX_VALUE;
		
			while(it.hasNext()){
				ResJob rj=(ResJob)it.next();
				Job j=new Job(rj.getGridletID(), rj.getNumPE(),rj.getGridletLength(),rj.getSigma(), rj.getPPBW());
				double energy=mc.getEnergyDifference(j);
				if(energy!=-1){
					double CT = energy;
					if(CT<minCT){
						best=rj;
						bestsim=j;
						minCT=CT;
						bigerSize=rj.getNumPE();
					}
					if(CT == minCT){
						if(rj.getNumPE()>bigerSize){
							best=rj;
							bestsim=j;
							bigerSize=rj.getNumPE();
						}
						
					}
				}
			}
			//if best=null there is no jobs that fit in the system, so advance some time in the estimation.
			if(best==null){
				mc.execute();
				
			}else{
				//put the best job in the queue and remove from the list.
				best.setAllocationPreferences(mc.getAllocationPreferences(bestsim));
				orderedQueue.add(best);
				list.remove(best);
				//also execute the best in the simulator
				mc.sheduleWithAllocation(bestsim,mc.getEAAlloc(bestsim));
			}
		}
		return orderedQueue;
	}
	

}
