package shedulers.energy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import gridsim.Machine;
import gridsim.MachineList;
import gridsim.PE;
import gridsim.ResGridlet;
import simulator.EnergyMachine;
import simulator.EnergyPolicy;
import simulator.ResJob;
import system.eaclustersim.Cluster;
import system.eaclustersim.MultiCluster;
import system.jobs.Job;
import util.MachinesComparatorByEnergyConsumition;
import util.MachinesComparatorByNodePower;
public class METLEPolicy  extends EnergyPolicy {
	private int packageSize=20;
	private int times=0;
	public METLEPolicy(String resourceName, String entityName,String[] args) throws Exception {
		super(resourceName, entityName);
		System.out.println("Policy "+ entityName);
		if(args!=null && args.length>0){
			try{
				packageSize=Integer.parseInt(args[0]);
			}catch(NumberFormatException e){
				System.out.println("The parameter packageSize is not correct");
				System.exit(0);
			}
		}
		System.out.println("Policy "+ entityName);
		System.out.println("packageSize "+packageSize);
		
	}

	@Override
	protected void orderWaitingQueue() {		
		//If we have preAllocatedJobs, the first jobs in the queue are already ordered.
				if(((ResJob)this.gridletQueueList_.getFirst()).isPreAllocated()){
					return;
				}
				System.out.println(times++ +" "+ gridletQueueList_.size());
				//We have jobs in the queue and we want to order and preAllocate them.
				ArrayList<ResJob> listToOrder=new ArrayList<ResJob>();
				while(!this.gridletQueueList_.isEmpty() && listToOrder.size()<packageSize){
					listToOrder.add((ResJob)this.gridletQueueList_.remove());
				}
				ArrayList<ResJob> orderedList=this.orderAndAllocateListUsingMesd(listToOrder);
				Collections.reverse(orderedList);
				for(ResJob rj:orderedList){
					rj.preAllcoate();
					this.gridletQueueList_.addFirst(rj);
				}
	}


	@Override
	protected boolean howToAllocatePEtoGridlet(ResGridlet rgl) {
		ResJob rj=(ResJob)rgl;
		//order the machines for they relative power
		MachineList list =super.resource_.getMachineList();
		Collections.sort(list,new MachinesComparatorByNodePower());
		int resources=0;
		int freeResources=0;
		for(int i=0;i<list.size()-1;i++){
			resources+=list.getMachineInPos(i).getNumPE();
			freeResources+=list.getMachineInPos(i).getNumFreePE();
		}
		
		if(rj.getNumPE()<=resources && rj.getNumPE()>freeResources){
			return false;
		}
		// here the machineStartId is pointing to the less powerful machine required to run the job.
		int machineStartId=0;
		for(int i=0;i<rj.getNumPE();i++){
			Machine m=list.getMachineInPos(machineStartId);		
			while(m.getNumFreePE()==0){
				//if we run out of nodes in this machine, we take some from more powerful machines.
				m=list.getMachineInPos(++machineStartId);
			}
			int machineID=m.getMachineID();
			int peID=m.getPEList().getFreePEID();
			rj.setMachineAndPEID(machineID, peID);
			super.changeStatusPE(PE.BUSY, machineID, peID);
		}
		return true;
	}
	
	public ArrayList<ResJob> orderAndAllocateListUsingMesd(ArrayList<ResJob>list){
		ArrayList<ResJob> orderedQueue=new ArrayList<ResJob>();
		//set up the simulator for the prediction.
		MultiCluster mc=null;
		ArrayList<Cluster> nodes=new ArrayList<Cluster>();
		for(Machine m : super.resource_.getMachineList()){
			EnergyMachine em=(EnergyMachine)m;
			nodes.add(new Cluster(m.getMachineID(),m.getNumPE(),(m.getMIPSRating()/m.getNumPE()),em.getMaxBW(),em.getEnergyIdle(),em.getEnergyComputing(),em.getEnergyStart(),em.getEnergyStop()));
		}
		
		try {
			MultiCluster.setInstance(nodes, null);
			mc=MultiCluster.instance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//add the current executing jobs.
		for(ResGridlet rg:this.gridletInExecList_){
			ResJob rj=(ResJob)rg;
			Job j=new Job(rj.getGridletID(), rj.getNumPE(),rj.getRemainingGridletLength(),rj.getSigma(), rj.getPPBW());
			Integer[] machines=new Integer[rj.getNumPE()];
			if(rj.getNumPE()==1){
				machines[0]=rj.getMachineID();
			}else{
				for(int i=0;i<rj.getListMachineID().length;i++){
					machines[i]=rj.getListMachineID()[i];
				}
			}
			if(!mc.sheduleWithAllocation(j,machines)){
					System.out.println("Extrange bug");
			}
		}
		
		//search for the job with minimum energy consumition
		while(list.size()>0){	
			Iterator<ResJob> it=list.iterator();
			ResJob best=null;
			Job bestsim=null;
			int bigerSize=0;
			double minconsumition=Double.MAX_VALUE;
		
			while(it.hasNext()){
				ResJob rj=(ResJob)it.next();
				Job j=new Job(rj.getGridletID(), rj.getNumPE(),rj.getGridletLength(),rj.getSigma(), rj.getPPBW());
				if(mc.getFreeResources()==super.resource_.getNumPE()){
					if(rj.getNumPE()>bigerSize){
						bigerSize=rj.getNumPE();
						best=rj;
						bestsim=j;
					}
				}else{
					double slowdown=mc.getSlowDownDifference(j);
					if(slowdown!=-1){
						if(slowdown<minconsumition){
							minconsumition=slowdown;
							best=rj;
							bestsim=j;
							bigerSize=rj.getNumPE();
						}
					if(slowdown==minconsumition){
						if(rj.getNumPE()>bigerSize){
							best=rj;
							bestsim=j;
							bigerSize=rj.getNumPE();
						}
					}
					
					}
				}
			}
			
			//if best=null there is no jobs that fit in the system, so advance some time in the estimation.
			if(best==null){
				mc.execute();
			}else{
				//put the best job in the queue and remove from the list.
				orderedQueue.add(best);
				list.remove(best);
				//also execute the best in the simulator
				mc.sheduleWithAllocation(bestsim,mc.getMESDAlloc(bestsim));
			}
		}
		return orderedQueue;
	}

}
