package shedulers;

import gridsim.Machine;
import gridsim.PE;
import gridsim.ResGridlet;
import simulator.EnergyPolicy;
import simulator.ResJob;


public class FCFSPolicy  extends EnergyPolicy {
	 
	public FCFSPolicy(String resourceName, String entityName) throws Exception {
		super(resourceName, entityName);
		System.out.println("Policy "+ entityName);
	}

	@Override
	protected void orderWaitingQueue() {		
		//As FCFS Politic no order is required
	}


	@Override
	protected boolean howToAllocatePEtoGridlet(ResGridlet rgl) {
		ResJob rj=(ResJob)rgl;
		Machine m=super.resource_.getMachineWithFreePE();
		
		for(int i=0;i<rj.getNumPE();i++){
			if(m.getNumFreePE()==0){
				m=super.resource_.getMachineWithFreePE();
			}
			int freeM = m.getMachineID();
			int freePE = m.getPEList().getFreePEID();
			rj.setMachineAndPEID(freeM, freePE);
			super.changeStatusPE(PE.BUSY, freeM, freePE);
		}
		return true;
	}
}
