package shedulers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import org.jaga.definitions.FitnessEvaluationAlgorithm;
import org.jaga.util.DefaultParameterSet;
import org.jaga.util.DefaultRandomGenerator;

import geneticOrder.fitnessevaluation.FunctionFactory;
import geneticOrder.fitnessevaluation.ValueFitness;
import geneticOrder.representation.SheduleIndividual;
import geneticOrder.representation.SheduleIndividualFactory;
import gridsim.Machine;
import gridsim.MachineList;
import gridsim.PE;
import gridsim.ResGridlet;
import gridsim.ResGridletList;
import simulator.EnergyMachine;
import simulator.EnergyPolicy;
import simulator.ResJob;
import system.eaclustersim.Cluster;
import system.eaclustersim.MultiCluster;
import system.jobs.Job;
import system.jobs.JobList;
import util.BadAllocationException;
import util.MachinesComparatorByEnergyPower;
import util.MachinesComparatorByNodePower;


public class HillClimbingPolicy  extends EnergyPolicy {
	private int packageSize=20;
	private int numStarts=10;
	private int neighbour=10;
	private String evaluationMetric="m";
	private float alpha = 0.5f;
	
	public HillClimbingPolicy(String resourceName, String entityName,String[] args) throws Exception {
		super(resourceName, entityName);
	    if(args!=null && args.length>3){
			try{
				packageSize=Integer.parseInt(args[0]);
				numStarts=Integer.parseInt(args[1]);
				neighbour=Integer.parseInt(args[2]);
				evaluationMetric=args[3];
				if(args.length>4){
					alpha=Float.parseFloat(args[4]);
				}else{
					alpha=(float) 0.5;
				}
			}catch(NumberFormatException e){
				System.out.println("The parameters are not correct");
				System.exit(0);
			}
		}
		System.out.println("Policy "+ entityName);
		System.out.println("Package Size "+packageSize);
		System.out.println("Num restarts "+numStarts);
		System.out.println("Neighbour Size "+ neighbour);
		System.out.println("Evaluation Metric "+evaluationMetric);
		System.out.println("alpha "+alpha);
	}

	@Override
	protected void orderWaitingQueue() {	
		//If we have preAllocatedJobs, the first jobs in the queue are already ordered.
		if(((ResJob)this.gridletQueueList_.getFirst()).isPreAllocated()){
			return;
		}
		//We have jobs in the queue and we want to order and preAllocate them.
		ArrayList<ResJob> listToOrder=new ArrayList<ResJob>();
		ArrayList<Job> processingJobs=new ArrayList<Job>();
		ArrayList<Cluster>multi=new ArrayList<Cluster>();
		Random r=new Random();
		while(!this.gridletQueueList_.isEmpty()&& listToOrder.size()<packageSize){
			
			ResJob rj=(ResJob)this.gridletQueueList_.remove(r.nextInt(gridletQueueList_.size()));
			//ResJob rj= (ResJob)this.gridletQueueList_.remove();
			listToOrder.add(rj);
			processingJobs.add(new Job(rj.getGridletID(),rj.getNumPE(),rj.getGridletLength(),rj.getSigma(),rj.getPPBW()));
		}
		
		for(Machine m :super.resource_.getMachineList()){
			EnergyMachine em=(EnergyMachine)m;
			multi.add(new Cluster(m.getMachineID(),m.getNumPE(),(m.getMIPSRating()/m.getNumPE()),em.getMaxBW(),em.getEnergyIdle(),em.getEnergyComputing(),em.getEnergyStart(),em.getEnergyStop()));
    	}
		//get the current stat of the simulator
    	HashMap<Job,ArrayList<Integer>> inExec=new HashMap<Job,ArrayList<Integer>>();
    	
    	for(ResGridlet rg:this.gridletInExecList_){
    		ResJob rj=(ResJob)rg;
    		Job job=new Job(rj.getGridletID(),rj.getNumPE(),rj.getRemainingGridletLength(),rj.getSigma(),rj.getPPBW());
    		ArrayList<Integer> alloc=new ArrayList<Integer>();
    		if(rj.getNumPE()>1){
    			for(int i:rj.getListMachineID()){
    				alloc.add(new Integer(i));
    			}
    		}else{
    			alloc.add(new Integer(rj.getMachineID()));
    		}
    		inExec.put(job,alloc);
    	}
		
		JobList.setInstance(processingJobs);
    	try {
			MultiCluster.setInstance(multi, inExec);
		} catch (BadAllocationException e1) {
			e1.printStackTrace();
			System.exit(1);
		}    	
    	
    	DefaultParameterSet dps=new DefaultParameterSet();
    	SheduleIndividualFactory iFac= new SheduleIndividualFactory();
    	dps.setRandomGenerator(new DefaultRandomGenerator());
    	FitnessEvaluationAlgorithm eval= FunctionFactory.getFunction(evaluationMetric,alpha);
    	SheduleIndividual[] solutions =new SheduleIndividual[this.numStarts];
    	ValueFitness[] solutionFitness=new ValueFitness[this.numStarts];
    	for(int i=0;i<this.numStarts;i++){
    		SheduleIndividual ind=(SheduleIndividual)iFac.createRandomIndividual(dps);
    		ValueFitness fitnes=(ValueFitness) eval.evaluateFitness(ind, 0, null, dps);
    		
    		SheduleIndividual bestneighbour=null;
    		ValueFitness bestFitnes=null;
    		while(bestneighbour==null || bestFitnes.isBetter(fitnes)){
    			if(bestneighbour!=null){
    				ind=bestneighbour;
    				fitnes=bestFitnes;
    			}else{
    				bestneighbour=ind;
    				bestFitnes=fitnes;
    			}
    			for(int j=0;j<this.neighbour;j++){
    				SheduleIndividual neigb=ind.getNeighbour();
    				ValueFitness neigbFitness=(ValueFitness) eval.evaluateFitness(neigb, 0, null, dps);
    				if(neigbFitness.isBetter(fitnes)){
    					bestneighbour=neigb;
    					bestFitnes=neigbFitness;
    					break;
    				}
				}
    		}
    			
     		solutions[i]=ind;
     		solutionFitness[i]=fitnes;
    	}
    	SheduleIndividual ind=solutions[0];
    	ValueFitness fitnes=solutionFitness[0];
    	for(int i=0;i<numStarts;i++){
    		if(solutionFitness[i].isBetter(fitnes)){
    			ind=solutions[i];
    			fitnes=solutionFitness[i];
    		}
    	}
    	
    	System.out.println(ind);
    	Job[] resultJobs =ind.getPreAllocatedJobs();
    	ResGridletList orderedList=new ResGridletList();
    	
    	for(Job jresult:resultJobs){
    		for(ResGridlet jorig:listToOrder){
    			if(jorig.getGridletID()==jresult.getId()){
    				((ResJob)jorig).setAllocationPreferences(jresult.getPreAssign());
    				orderedList.add(jorig);
    			}
    		}
    	}
    	Collections.reverse(orderedList);
    	for(ResGridlet rg: orderedList){
    		ResJob rj=(ResJob) rg;
    		rj.preAllcoate();
    		gridletQueueList_.addFirst(rj);
    	}
    }
	
	@Override
	protected boolean howToAllocatePEtoGridlet(ResGridlet rgl) {
		ResJob rj=(ResJob)rgl;
			//order the machines for they relative power
			MachineList list =super.resource_.getMachineList();
			if(this.evaluationMetric=="m"){
				Collections.sort(list,Collections.reverseOrder(new MachinesComparatorByNodePower()));
				//Allocate to the best resources(unused best resources free)
				int freeMachines=0;
				int machineStartId=0;
				//search for the first machine to start allocating the job.(Start for the most powerful)
				for(int i=0;i<list.size();i++){
					Machine m=list.get(i);
					freeMachines+=m.getNumFreePE();
					//if there are enough resources finish. if not, add the nodes in the next ordered machine.
					if(rj.getNumPE()<=freeMachines){
						machineStartId=i;
						break;
					}
				}
				// here the machineStartId is pointing to the less powerful machine required to run the job.
				for(int i=0;i<rj.getNumPE();i++){
					Machine m=list.get(machineStartId);		
					while(m.getNumFreePE()==0){
						//if we run out of nodes in this machine, we take some from more powerful machines.
						m=list.get(--machineStartId);
					}
					int machineID=m.getMachineID();
					int peID=m.getPEList().getFreePEID();
					rj.setMachineAndPEID(machineID, peID);
					super.changeStatusPE(PE.BUSY, machineID, peID);
				}
				return true;
			}else{
				Collections.sort(list,Collections.reverseOrder(new MachinesComparatorByEnergyPower()));
				int machineStartId=0;
				// here the machineStartId is pointing to the less powerful machine required to run the job.
				for(int i=0;i<rj.getNumPE();i++){
					Machine m=list.get(machineStartId);		
					while(m.getNumFreePE()==0){
						//if we run out of nodes in this machine, we take some from more powerful machines.
						m=list.get(++machineStartId);
					}
					int machineID=m.getMachineID();
					int peID=m.getPEList().getFreePEID();
					rj.setMachineAndPEID(machineID, peID);
					super.changeStatusPE(PE.BUSY, machineID, peID);
				}
				return true;
			}
	}	
}

