package shedulers;

import java.util.Collections;

import gridsim.Machine;
import gridsim.MachineList;
import gridsim.PE;
import gridsim.ResGridlet;
import simulator.EnergyPolicy;
import simulator.ResJob;
import util.MachinesComparatorByEnergyConsumition;


public class EAAllocPolicy  extends EnergyPolicy {
	 
	public EAAllocPolicy(String resourceName, String entityName) throws Exception {
		super(resourceName, entityName);
		System.out.println("Policy "+ entityName);
	}

	@Override
	protected void orderWaitingQueue() {		
		//As FCFS Politic no order is required
	}


	@Override
	protected boolean howToAllocatePEtoGridlet(ResGridlet rgl) {
		ResJob rj=(ResJob)rgl;
		//order the machines for they relative power
		MachineList list =super.resource_.getMachineList();
		Collections.sort(list,new MachinesComparatorByEnergyConsumition());
		int resources=0;
		int freeResources=0;
		for(int i=0;i<list.size()-1;i++){
			resources+=list.getMachineInPos(i).getNumPE();
			freeResources+=list.getMachineInPos(i).getNumFreePE();
		}
		
		if(rj.getNumPE()<=resources && rj.getNumPE()>freeResources){
			return false;
		}
		// here the machineStartId is pointing to the less powerful machine required to run the job.
		int machineStartId=0;
		for(int i=0;i<rj.getNumPE();i++){
			Machine m=list.getMachineInPos(machineStartId);		
			while(m.getNumFreePE()==0){
				//if we run out of nodes in this machine, we take some from more powerful machines.
				m=list.getMachineInPos(++machineStartId);
			}
			int machineID=m.getMachineID();
			int peID=m.getPEList().getFreePEID();
			rj.setMachineAndPEID(machineID, peID);
			super.changeStatusPE(PE.BUSY, machineID, peID);
		}
		return true;
	}
}
