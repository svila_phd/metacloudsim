package shedulers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

import gridsim.Machine;
import gridsim.MachineList;
import gridsim.PE;
import gridsim.ResGridlet;
import simulator.EnergyMachine;
import simulator.EnergyPolicy;
import simulator.ResJob;
import system.eaclustersim.Cluster;
import system.eaclustersim.MultiCluster;
import system.jobs.Job;
import util.MachinesComparatorByNodePower;


public class GRASPMESDPolicy  extends EnergyPolicy {
	private int packageSize=20;
	private int numCandidates=5;
	public GRASPMESDPolicy(String resourceName, String entityName,String[] args) throws Exception {
		super(resourceName, entityName);
		if(args!=null && args.length>1){
			try{
				packageSize=Integer.parseInt(args[0]);
				numCandidates=Integer.parseInt(args[1]);
			}catch(NumberFormatException e){
				System.out.println("The parameter packageSize is not correct");
				System.exit(0);
			}
		}
		System.out.println("Policy "+ entityName);
		System.out.println("Package Size "+packageSize);
		System.out.println("Num Candidates "+numCandidates);
	}

	@Override
	protected void orderWaitingQueue() {
		//If we have preAllocatedJobs, the first jobs in the queue are already ordered.
		if(((ResJob)this.gridletQueueList_.getFirst()).isPreAllocated()){
			return;
		}
		//We have jobs in the queue and we want to order and preAllocate them.
		ArrayList<ResJob> listToOrder=new ArrayList<ResJob>();
		while(!this.gridletQueueList_.isEmpty() && listToOrder.size()<packageSize){
			listToOrder.add((ResJob)this.gridletQueueList_.remove());
		}
		ArrayList<ResJob> orderedList=this.orderAndAllocateList(listToOrder);
		Collections.reverse(orderedList);
		for(ResJob rj:orderedList){
			rj.preAllcoate();
			this.gridletQueueList_.addFirst(rj);
		}
		
	}


	@Override
	protected boolean howToAllocatePEtoGridlet(ResGridlet rgl) {
		ResJob rj=(ResJob)rgl;
		//order the machines for they relative power
		MachineList list =super.resource_.getMachineList();
		Collections.sort(list,Collections.reverseOrder(new MachinesComparatorByNodePower()));
		//Allocate to the best resources(unused best resources free)
		int freeMachines=0;
		int machineStartId=0;
		//search for the first machine to start allocating the job.(Start for the most powerful)
		for(int i=0;i<list.size();i++){
			Machine m=list.get(i);
			freeMachines+=m.getNumFreePE();
			//if there are enough resources finish. if not, add the nodes in the next ordered machine.
			if(rj.getNumPE()<=freeMachines){
				machineStartId=i;
				break;
			}
		}
		// here the machineStartId is pointing to the less powerful machine required to run the job.
		for(int i=0;i<rj.getNumPE();i++){
			Machine m=list.get(machineStartId);		
			while(m.getNumFreePE()==0){
				//if we run out of nodes in this machine, we take some from more powerful machines.
				m=list.get(--machineStartId);
			}
			int machineID=m.getMachineID();
			int peID=m.getPEList().getFreePEID();
			rj.setMachineAndPEID(machineID, peID);
			super.changeStatusPE(PE.BUSY, machineID, peID);
		}
		return true;
	}
	
	public ArrayList<ResJob> orderAndAllocateList(ArrayList<ResJob>list){
		ArrayList<ResJob> orderedQueue=new ArrayList<ResJob>();
		//set up the simulator for the prediction.
		MultiCluster mc=null;
		ArrayList<Cluster> nodes=new ArrayList<Cluster>();
		for(Machine m : super.resource_.getMachineList()){
			EnergyMachine em=(EnergyMachine)m;
			nodes.add(new Cluster(m.getMachineID(),m.getNumPE(),(m.getMIPSRating()/m.getNumPE()),em.getMaxBW(),em.getEnergyIdle(),em.getEnergyComputing(),em.getEnergyStart(),em.getEnergyStop()));
		}
		try {
			MultiCluster.setInstance(nodes,null);
			mc=MultiCluster.instance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//add the current executing jobs.
		for(ResGridlet rg:this.gridletInExecList_){
			ResJob rj=(ResJob)rg;
			Job j=new Job(rj.getGridletID(), rj.getNumPE(),rj.getRemainingGridletLength(),rj.getSigma(), rj.getPPBW());
			Integer[] machines=new Integer[rj.getNumPE()];
			if(rj.getNumPE()==1){
				machines[0]=rj.getMachineID();
			}else{
				for(int i=0;i<rj.getListMachineID().length;i++){
					machines[i]=rj.getListMachineID()[i];
				}
			}
			if(!mc.sheduleWithAllocation(j,machines)){
					System.out.println("Extrange bug");
			}
		}
		
		//search for the job with minimum slow down
		while(list.size()>0){	
			Iterator<ResJob> it=list.iterator();
			ResJob[] best=new ResJob[numCandidates];
			Job[] bestsim= new Job[numCandidates];
			int bigerSize=0;
			double minSlowDown=Double.MAX_VALUE;
			int pointTobest=0;
			while(it.hasNext()){
				ResJob rj=(ResJob)it.next();
				Job j=new Job(rj.getGridletID(), rj.getNumPE(),rj.getRemainingGridletLength(),rj.getSigma(), rj.getPPBW());
				if(mc.getFreeResources()==super.resource_.getNumPE()){
					if(rj.getNumPE()>=bigerSize){
						bigerSize=rj.getNumPE();
						best[pointTobest%numCandidates]=rj;
						bestsim[pointTobest%numCandidates]=j;
						pointTobest++;
					}
				}else{
					double currentSlowDown=mc.getSlowDownDifference(j);
					if(currentSlowDown!=-1){
						if(currentSlowDown<=minSlowDown){
							minSlowDown=currentSlowDown;
							best[pointTobest%numCandidates]=rj;
							bestsim[pointTobest%numCandidates]=j;
							pointTobest++;
						}
					}
				}
			}
			//if pointTobest=0, no jobs found that fit the system so advanse some time in the simulator.
			if(pointTobest==0){
				mc.execute();
			}else{
				//select one random job from the best list.
				Random r=new Random();
				int maxRandom=0;
				if(pointTobest<numCandidates){
					maxRandom=pointTobest;
				}else{
					maxRandom=numCandidates;
				}
				int rnd=r.nextInt(maxRandom);
				orderedQueue.add(best[rnd]);
				list.remove(best[rnd]);
				//also execute the best in the simulator
				mc.sheduleWithAllocation(bestsim[rnd],mc.getMESDAlloc(bestsim[rnd]));
			}
		}
		return orderedQueue;
	}

}
