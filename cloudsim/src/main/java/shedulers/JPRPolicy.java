package shedulers;

import java.util.Collections;

import gridsim.Machine;
import gridsim.MachineList;
import gridsim.PE;
import gridsim.ResGridlet;
import simulator.EnergyPolicy;
import util.MachinesComparatorByNodePower;


public class JPRPolicy  extends EnergyPolicy {
	 
	public JPRPolicy(String resourceName, String entityName) throws Exception {
		super(resourceName, entityName);
		System.out.println("Policy "+ entityName);
	}

	@Override
	protected void orderWaitingQueue() {		
		//As FCFS Politic no order is required
	}


	@Override
	protected boolean howToAllocatePEtoGridlet(ResGridlet rgl) {
		MachineList l=super.resource_.getMachineList();
		
		Collections.sort(l,Collections.reverseOrder(new MachinesComparatorByNodePower()));
		
		int requirements=rgl.getNumPE();
		for(Machine m:l){
			for(;requirements>0;requirements--){
				if(m.getNumFreePE()>0){
					int freeM = m.getMachineID();
					int freePE = m.getPEList().getFreePEID();
					rgl.setMachineAndPEID(freeM, freePE);
					super.changeStatusPE(PE.BUSY, freeM, freePE);
				}else{
					break;
				}
			}
		}
		return true;
	}
}
