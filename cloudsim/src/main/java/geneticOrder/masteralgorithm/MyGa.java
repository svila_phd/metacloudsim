package geneticOrder.masteralgorithm;

import geneticOrder.representation.SheduleIndividual;
import geneticOrder.selectionalgorithm.IndividualComparator;

import java.util.Arrays;

import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.GAResult;
import org.jaga.definitions.Individual;
import org.jaga.definitions.IndividualsFactory;
import org.jaga.definitions.Population;
import org.jaga.masterAlgorithm.ElitistGA;
import org.jaga.util.FittestIndividualResult;


public class MyGa extends ElitistGA {
	private Object[] initialIndividuals;
	
	public MyGa(Object[] initial){
		super();
		this.initialIndividuals=initial;
	}
	
	protected Population createInitialPopulation(GAParameterSet params) {
		Population pop = createEmptyPopulation(params);
		if(this.initialIndividuals!=null){
			while (pop.getSize() < params.getPopulationSize()-1) {
				Individual ind;
				ind = params.getIndividualsFactory().createRandomIndividual(params);
				pop.add(ind);
			}
			Individual ind=params.getIndividualsFactory().createSpecificIndividual(initialIndividuals[0], params);
			pop.add(ind);
			
			System.out.println("MESD ADD");
			System.out.println(ind);
		}else{
			while (pop.getSize() < params.getPopulationSize()-1) {
				Individual ind;
				ind = params.getIndividualsFactory().createRandomIndividual(params);
				pop.add(ind);
			}
		}
		return pop;
	}

	protected Population generateNextPopulation(Population paramPopulation, int paramInt, GAResult paramGAResult, GAParameterSet paramGAParameterSet)
	  {
	    FittestIndividualResult localFittestIndividualResult = (FittestIndividualResult)paramGAResult;
	    Population localPopulation = createEmptyPopulation(paramGAParameterSet);
	    IndividualsFactory localIndividualsFactory = paramGAParameterSet.getIndividualsFactory();
	    Individual[] arrayOfIndividual1 = paramPopulation.getAllMembers();
	    Arrays.sort(arrayOfIndividual1, new IndividualComparator());
	    int i = (int)(arrayOfIndividual1.length * (1.0D - this.getBadProportion()));
	    Individual[] arrayOfIndividual2 = new Individual[i];
	    System.arraycopy(arrayOfIndividual1, arrayOfIndividual1.length - i, arrayOfIndividual2, 0, i);
	    int j = (int)(paramGAParameterSet.getPopulationSize() * getEliteProportion());
	    int k = i - 1;
	    Object localObject;
	    while (localPopulation.getSize() < j)
	    {
	      localObject = localIndividualsFactory.createSpecificIndividual(((SheduleIndividual)arrayOfIndividual2[k]).getChromosome(), paramGAParameterSet);
	      ((Individual)localObject).setFitness(arrayOfIndividual2[k].getFitness());
	      localPopulation.add((Individual)localObject);
	      k--;
	      if (k < 0) {
	        k = i - 1;
	      }
	    }
	    while (localPopulation.getSize() < paramGAParameterSet.getPopulationSize())
	    {
	      localObject = selectForReproduction(paramPopulation, paramInt, paramGAParameterSet);
	      notifySelectedForReproduction((Individual[])localObject, paramPopulation, paramInt, paramGAResult, paramGAParameterSet);
	      Individual[] arrayOfIndividual3 = haveSex((Individual[])localObject, paramGAParameterSet);
	      for (int m = 0; m < arrayOfIndividual3.length; m++) {
	        if (null == arrayOfIndividual3[m].getFitness())
	        {
	          updateIndividualFitness(arrayOfIndividual3[m], paramPopulation, paramInt, paramGAParameterSet);
	          if (arrayOfIndividual3[m].getFitness().isBetter(localFittestIndividualResult.getBestFitness())) {
	            localFittestIndividualResult.setFittestIndividual(arrayOfIndividual3[m]);
	          }
	        }
	      }
	      notifyReproduced(arrayOfIndividual3, (Individual[])localObject, paramPopulation, paramInt, paramGAResult, paramGAParameterSet);
	      localPopulation.addAll(arrayOfIndividual3);
	    }
	    return localPopulation;
	  }
}
