package geneticOrder.metrics;


import geneticOrder.representation.SheduleIndividual;

import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.Individual;
import org.jaga.definitions.Population;

import system.eaclustersim.MultiCluster;
import system.jobs.Job;
import system.jobs.JobList;
public class FlowTimeEvaluator implements Evaluator { 
	

	public double evaluateMetric(Individual individual, int age,
		Population population, GAParameterSet params) throws ClassCastException {
		SheduleIndividual si=(SheduleIndividual) individual;
		double time=0;
		try {
			MultiCluster mc=MultiCluster.instance();
			JobList jobList=JobList.instance();
			mc.reset();
			jobList.reset();
			for(Job j:si.getPreAllocatedJobs()){
				Integer[] alloc=mc.getMESDAlloc(j);
				while(alloc==null){	
					int befsize=mc.ExecutingSize();
					double temp=mc.execute();
					int aftsize=mc.ExecutingSize();
					time+=temp*(befsize-aftsize);	
					alloc=mc.getMESDAlloc(j);
				}
				if(!mc.sheduleWithAllocation(j, alloc)){
					System.out.println("Extrage bug2!");
				}
			}
			mc.finishExecution();
	    	return time;

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(2);
		}
		return 0;
	}
}

