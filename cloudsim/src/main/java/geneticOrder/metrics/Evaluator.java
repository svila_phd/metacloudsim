package geneticOrder.metrics;

import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.Individual;
import org.jaga.definitions.Population;

public interface Evaluator {
	public double evaluateMetric(Individual individual, int age,
			Population population, GAParameterSet params) throws ClassCastException;

}
