package geneticOrder.metrics;


import geneticOrder.representation.SheduleIndividual;

import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.Individual;
import org.jaga.definitions.Population;
import system.eaclustersim.MultiCluster;
import system.jobs.Job;
import system.jobs.JobList;
public class EnergyEvaluator implements Evaluator { 

	
	public double evaluateMetric(Individual individual, int age,
		Population population, GAParameterSet params) throws ClassCastException {
		SheduleIndividual si=(SheduleIndividual) individual;
		try {
			MultiCluster mc=MultiCluster.instance();
			JobList jobList=JobList.instance();
			mc.reset();
			jobList.reset();
			for(Job j:si.getPreAllocatedJobs()){
				Integer[] alloc=mc.getEAAlloc(j);
				while(alloc==null){	
					mc.execute();
					alloc=mc.getEAAlloc(j);
				}
				if(!mc.sheduleWithAllocation(j, alloc)){
					System.out.println("Extrage bug2!");
				}
			}
			mc.finishExecution();
			return mc.getEnergyConsumition();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}

