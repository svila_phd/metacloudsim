package geneticOrder.reproduction;

import geneticOrder.representation.Chromosomepkg;
import geneticOrder.representation.SheduleIndividual;

import java.util.ArrayList;
import java.util.Collections;

import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.Individual;
import org.jaga.definitions.ReproductionAlgorithm;

import system.jobs.Job;


public class OnePointCrossover implements ReproductionAlgorithm {
	
	private int mutation=0;
	private int crossover=0;
	@Override
	public int getRequiredNumberOfParents() {
		return 2;
	}
	
	@Override
	public Class <SheduleIndividual>getApplicableClass() {
		return SheduleIndividual.class;
	}
	
	public OnePointCrossover (int probC,int probM){
		mutation=probM;
		crossover=probC;
	}
	
	@Override
	public Individual[] reproduce(Individual[] parents, GAParameterSet params) throws ClassCastException, IllegalArgumentException {
		
		
		int prob=params.getRandomGenerator().nextInt(0,100);
		Chromosomepkg parent1=((SheduleIndividual)parents[0]).getChromosome();
		Chromosomepkg parent2=((SheduleIndividual)parents[1]).getChromosome();
		
		if(crossover>prob){
			
			
			
			
			SheduleIndividual[] child= new SheduleIndividual[2];
			int job_size=parent1.jobOrder.length;
			Chromosomepkg child1=new Chromosomepkg(job_size);
			Chromosomepkg child2=new Chromosomepkg(job_size);
			boolean[] mask=new boolean[job_size];
			
			for(int i=0;i<job_size;i++){
				mask[i]=params.getRandomGenerator().nextBoolean();
			}
			
			//child 1;
			ArrayList<Integer> missingJobs1=new ArrayList<Integer>();
			ArrayList<Integer> missingJobs2=new ArrayList<Integer>();
			for( int i=0;i<job_size;i++){
				if(mask[i]==true){
					child1.jobOrder[i]=parent1.jobOrder[i];
					child2.jobOrder[i]=parent2.jobOrder[i];
				}else{
					child1.jobOrder[i]=null;
					child2.jobOrder[i]=null;
					for(int j=0;j<job_size;j++){
						if(parent1.jobOrder[i].getId()==parent2.jobOrder[j].getId()){
							missingJobs1.add(j);
							break;
						}
					}
					for(int j=0;j<job_size;j++){
						if(parent2.jobOrder[i].getId()==parent1.jobOrder[j].getId()){
							missingJobs2.add(j);
							break;
						}
					}
				}
			}
			Collections.sort(missingJobs1);
			Collections.sort(missingJobs2);
			
			int index=0;
			for( int i=0;i<job_size;i++){
				if(mask[i]==false){
					child1.jobOrder[i]=parent2.jobOrder[missingJobs1.get(index)];
					child2.jobOrder[i]=parent1.jobOrder[missingJobs2.get(index)];
					index++;
				}
			}
					
			child[0]=(SheduleIndividual) params.getIndividualsFactory().createSpecificIndividual(child1, params);
			child[1]=(SheduleIndividual) params.getIndividualsFactory().createSpecificIndividual(child2, params);
			mutation(child[0].getChromosome(),params);
			mutation(child[1].getChromosome(),params);
			return child;
		}else{
			SheduleIndividual[] child= new SheduleIndividual[2];
			child[0]=(SheduleIndividual) params.getIndividualsFactory().createRandomIndividual(params);
			child[1]=(SheduleIndividual) params.getIndividualsFactory().createRandomIndividual(params);
			return child;
		}	
		
	} 
	
	public boolean mutation(Chromosomepkg child,GAParameterSet params) {
		int prob=params.getRandomGenerator().nextInt();
		if(prob<mutation && child.jobOrder.length>1){
			int change1=params.getRandomGenerator().nextInt(0,(child.jobOrder.length)-1);
			int change2=change1+1;
			Job temp=child.jobOrder[change1];
			child.jobOrder[change1]=child.jobOrder[change2];
			child.jobOrder[change2]=temp;
			return true;
		}
		return false;
	}
}