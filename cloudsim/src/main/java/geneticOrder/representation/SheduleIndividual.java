package geneticOrder.representation;



import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import org.jaga.definitions.Fitness;
import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.Individual;

import system.eaclustersim.MultiCluster;
import system.jobs.Job;
import system.jobs.JobList;


public class SheduleIndividual implements Individual  {
	JobList jobs;
	Fitness fitness;
	MultiCluster mc;
	Job[] jobOrder;
		
	public SheduleIndividual(GAParameterSet params){
		try {
			jobs=JobList.instance();
			mc=MultiCluster.instance();
			int job_size=jobs.size();
			jobOrder=new Job[job_size];
			ArrayList<Job> shuffleJobs=new ArrayList<Job>();
		
			for(int i=0;i<job_size;i++){
				shuffleJobs.add(jobs.get(i));
			}
			Random r=(Random) params.getRandomGenerator();
			Collections.shuffle(shuffleJobs, r);
			for(int i=0;i<job_size;i++){
				jobOrder[i]=shuffleJobs.get(i);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(2);
		}
		
	}
	
	
	
	public SheduleIndividual(Chromosomepkg pchromosome){
		try {
			jobs=JobList.instance();
			mc=MultiCluster.instance();
			int job_size=jobs.size();
			
			jobOrder=new Job[job_size];
			for(int i=0;i<job_size;i++){
				jobOrder[i]=pchromosome.jobOrder[i];
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(2);
		}
	}
	public void setValues(SheduleIndividual s){
		this.fitness=s.fitness;
		this.jobOrder=s.jobOrder.clone();
	}

	public Chromosomepkg getChromosome(){
			return new Chromosomepkg(this.jobOrder.clone());
	}
	
	public String toString(){
		String schromosome=new String("");
		for (Job x:jobOrder){
			schromosome+=x.getId()+" ";
		}
		if(this.fitness!=null){
			schromosome+="Fitness "+this.fitness;
		}
		return schromosome;
	}
	@Override
	public Fitness getFitness() {
		return this.fitness;
	}

	@Override
	public void setFitness(Fitness fitness) {
		this.fitness=fitness;
		
	}
	public Job[] getPreAllocatedJobs(){
	return jobOrder;
	}
	public SheduleIndividual getNeighbour(){
		int job_size=this.jobs.size();
		Chromosomepkg chromosome = new Chromosomepkg(job_size);
		for(int i=0;i<job_size;i++){
			chromosome.jobOrder[i]=this.jobOrder[i];
		}
		//swap 2 jobs
		Random r=new Random();
		int posx=r.nextInt(job_size);
		int posy=r.nextInt(job_size);
		Job x = chromosome.jobOrder[posx];
		chromosome.jobOrder[posx]=chromosome.jobOrder[posy];
		chromosome.jobOrder[posy]=x;
		
		return new SheduleIndividual(chromosome);
		
	}
}