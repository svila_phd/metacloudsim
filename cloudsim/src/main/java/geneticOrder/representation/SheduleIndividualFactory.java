package geneticOrder.representation;

import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.Individual;
import org.jaga.definitions.IndividualsFactory;

public class SheduleIndividualFactory implements IndividualsFactory{
	@Override
	public Individual createDefaultIndividual(GAParameterSet params) {
		return null;
	}

	@Override
	public Individual createRandomIndividual(GAParameterSet params) {
			return new SheduleIndividual(params);
	}

	@Override
	public Individual createSpecificIndividual(Object init,
			GAParameterSet params) throws NullPointerException,
			ClassCastException {
		if(init.getClass()!=Chromosomepkg.class){
			throw new ClassCastException();
		}
		return  new SheduleIndividual((Chromosomepkg)init);
	}

}
