package geneticOrder.hooks;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Vector;

import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.GAResult;
import org.jaga.definitions.Population;
import org.jaga.hooks.SimpleGAHook;
import org.jaga.masterAlgorithm.SimpleGA;
import org.jaga.util.FittestIndividualResult;

public class LogHook extends SimpleGAHook {
	private Vector<String> messages;
	private long lastWrite=0;
	private Calendar now;
		
	private String writeMode = "each";
			// each: write to disk each message
			// intelligent: write every 10 seconds, or 100 messages.
	private int NUM_MESSAGES_TRIGGER = 100;
		
	private BufferedWriter outFile;
	private PrintWriter out;
		
	
	public LogHook(String filename) throws IOException{
		messages = new Vector<String>();
		now = Calendar.getInstance();
		
		// If the file already exists, remove it.
	 	File file = new File(filename);
	 	if (file.exists()){
	 		file.delete();
	 	}
		outFile = new BufferedWriter(new FileWriter(filename));
		out = new PrintWriter(outFile);
	}		
		
	public void addMessage(String message){
		String str =  message;
		messages.add(str);
		check();
	}
		
	private void check(){
		if (this.writeMode.equals("intelligent")){
			long current = now.getTimeInMillis();
			if ((this.lastWrite < (current-10000)) || (this.messages.size() > NUM_MESSAGES_TRIGGER) ){
				write();
			}
		}else if(this.writeMode.equals("each")){
			write();
		}
	}
		
	private void write(){
		for (int i=0; i <messages.size(); i++){
			out.println(messages.elementAt(i));
		}
		messages.clear();
		try {
			outFile.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.lastWrite = now.getTimeInMillis();
	}
		
	public void close(){
		// write the remaining messages
		this.write();
		out.close();
	}
		
	private void printPopulation(Population pop, int age) {
		double average=0;
		double max=0;
		double min=Double.MAX_VALUE;
		for (int i = 0; i < pop.getSize(); i++) {
			double val=Double.parseDouble(pop.getMember(i).getFitness().toString());
			average+=val;
			if(val>max) max=val;
			if(val<min) min=val;
		}
		this.addMessage("Average fittnes: "+average/pop.getSize());
		this.addMessage("Max fittnes: "+max);
		this.addMessage("Min fittnes: "+min);
	}
	private void printResult(GAResult result, int age) {
		if (!(result instanceof FittestIndividualResult)) {
			return;
		}
		this.addMessage("Best result:" + ((FittestIndividualResult) result).getFittestIndividual().getFitness().toString());
	}
	
	
	public void generationChanged(SimpleGA caller, Population pop, int age, GAResult result, GAParameterSet paramss) {
		this.addMessage("\nGENERATION "+age);
		printPopulation(pop,age);
		printResult(result,age);
		this.addMessage("--------------------------------------------------");
	}
	public void initialisationDone(SimpleGA caller, Population pop, int age,
			   GAResult result, GAParameterSet params) {
		this.addMessage("\nGENERATION 0");
		printPopulation(pop,age);
		printResult(result,age);
		this.addMessage("--------------------------------------------------");
	}
}
