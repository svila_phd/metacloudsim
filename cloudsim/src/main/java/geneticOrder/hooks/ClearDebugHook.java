package geneticOrder.hooks;

import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.GAResult;
import org.jaga.definitions.Individual;
import org.jaga.definitions.Population;
import org.jaga.hooks.SimpleGAHook;
import org.jaga.masterAlgorithm.SimpleGA;
import org.jaga.util.FittestIndividualResult;

public class ClearDebugHook extends SimpleGAHook {
	public ClearDebugHook() {
	}
	private void printPopulation(Population pop, int age) {
		System.out.println("Population in generation " + age + " has "
						   + pop.getSize() + " members:");
		for (int i = 0; i < pop.getSize(); i++) {
			System.out.println("  " + i + ") " + pop.getMember(i));
		}
	}

	private void printResult(GAResult result, int age) {
		if (!(result instanceof FittestIndividualResult)) {
			return;
		}
		System.out.println("Best result (in generation " + age + "):");
		System.out.println(((FittestIndividualResult) result).
						   getFittestIndividual());
	}

	protected void printIndividuals(Individual[] inds) {
		for (int i = 0; i < inds.length; i++) {
			System.out.println("  " + i + ": " + inds[i]);
		}
	}

	public void initialisationDone(SimpleGA caller, Population pop, int age,
								   GAResult result, GAParameterSet params) {
		System.out.println("\nINITIALISATION DONE.");
		printPopulation(pop, age);
		printResult(result, age);
		System.out.println("--------------------------------------------------");
	}

	public void foundNewResult(SimpleGA caller, Population pop, int age,
							   GAResult result, GAParameterSet params) {
		System.out.println("\nFOUND NEW RESULT.");
		//printPopulation(pop, age);
		printResult(result, age);
		System.out.println("--------------------------------------------------");
	}

	public void generationChanged(SimpleGA caller, Population pop, int age,
								  GAResult result, GAParameterSet paramss) {
		System.out.println("\nNEXT GENERATION.");
		printPopulation(pop, age);
		printResult(result, age);
		System.out.println("--------------------------------------------------");
	}

	public void terminationConditionApplies(SimpleGA caller, Population pop,
											int age,
											GAResult result,
											GAParameterSet params) {
		System.out.println("\nTERMINATION APPLIED.");
		printPopulation(pop, age);
		printResult(result, age);
		System.out.println("--------------------------------------------------");
	}

	public void selectedForReproduction(SimpleGA caller,
										Individual[] selectedParents,
										Population pop, int age,
										GAResult result,
										GAParameterSet params) {
		//System.out.println("\nPARENTS SELECTED.");
		//printPopulation(pop, age);
		//printResult(result, age);
		//System.out.println("Parents:");
		//printIndividuals(selectedParents);
		//System.out.println("--------------------------------------------------");
	}

	public void reproduced(SimpleGA caller, Individual[] children,
						   Individual[] parents,
						   Population pop, int age, GAResult result,
						   GAParameterSet params) {
		//System.out.println("\nCHILDREN PRODUCED.");
		//printPopulation(pop, age);
		//printResult(result, age);
		//System.out.println("Parents:");
		//printIndividuals(parents);
		//System.out.println("Children:");
		//printIndividuals(children);
		//System.out.println("--------------------------------------------------");
	}

	public void fitnessCalculated(SimpleGA caller, Individual updatedIndividual,
								  Population pop, int age,
								  GAParameterSet params) {
		//System.out.println("\nFITNESS CALCULATED.");
		//System.out.println("Updated individual: " + updatedIndividual);
		//System.out.println("--------------------------------------------------");
	}

}
