package geneticOrder.fitnessevaluation;

import org.jaga.definitions.FitnessEvaluationAlgorithm;

public class FunctionFactory {
	public static FitnessEvaluationAlgorithm getFunction(String functionName,float alpha){
		if(functionName.equals("m")){
			return new MakespanFunction();
		}else if(functionName.equals("f")){
			return new FlowTimeFunction();
		}else if(functionName.equals("mf")){
			return new FlowTimeMakespanFunction(alpha);
		}else if(functionName.equals("e")){
			return new EnergyFunction();
		}else if(functionName.equals("em")){
			return new EnergyMakespanFunction(alpha);
		}else{
			System.out.println("Invalid Evaluation Function for the GA");
			System.exit(2);
			return null;
		}
	}
}
