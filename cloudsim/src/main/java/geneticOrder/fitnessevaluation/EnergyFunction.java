package geneticOrder.fitnessevaluation;

import geneticOrder.metrics.EnergyEvaluator;
import geneticOrder.metrics.Evaluator;
import geneticOrder.representation.SheduleIndividual;

import org.jaga.definitions.Fitness;
import org.jaga.definitions.FitnessEvaluationAlgorithm;
import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.Individual;
import org.jaga.definitions.Population;

public class EnergyFunction implements FitnessEvaluationAlgorithm { 

	@Override
	public Class<SheduleIndividual> getApplicableClass() {
		return SheduleIndividual.class;
	}

	@Override
	public Fitness evaluateFitness(Individual individual, int age,
		Population population, GAParameterSet params) throws ClassCastException {
			Evaluator metric=new EnergyEvaluator();
			return new ValueFitness(metric.evaluateMetric(individual, age, population, params));		
	}
}

