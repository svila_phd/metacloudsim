package geneticOrder.fitnessevaluation;

import org.jaga.definitions.Fitness;
import org.jaga.selection.AbsoluteFitness;

public class ValueFitness extends AbsoluteFitness{
	public double value;
	
	public ValueFitness(double time){
		super(time);
		value=time;
	}
	@Override
	public boolean isBetter(Fitness fitness) throws ClassCastException {
		ValueFitness mf=(ValueFitness) fitness;
		if(mf==null){return true;}
		if(this.value<mf.value){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean isWorse(Fitness fitness) throws ClassCastException {
		ValueFitness mf=(ValueFitness) fitness;
		if(this.value>mf.value){
			return true;
		}else{
			return false;
		}
	}
	
	public String toString(){
		return new String(""+value);
	}
}
