package geneticOrder.fitnessevaluation;



import geneticOrder.metrics.Evaluator;
import geneticOrder.metrics.FlowTimeEvaluator;
import geneticOrder.metrics.MakespanEvaluator;
import geneticOrder.representation.SheduleIndividual;

import org.jaga.definitions.Fitness;
import org.jaga.definitions.FitnessEvaluationAlgorithm;
import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.Individual;
import org.jaga.definitions.Population;

public class FlowTimeMakespanFunction implements FitnessEvaluationAlgorithm {
	float alpha;
	public FlowTimeMakespanFunction(float alpha){
		this.alpha=alpha;
	}
	@Override
	public Class<SheduleIndividual> getApplicableClass() {
		return SheduleIndividual.class;
	}

	@Override
	public Fitness evaluateFitness(Individual individual, int age,
			Population population, GAParameterSet params){
		Evaluator Makespan=new MakespanEvaluator();
		Evaluator Flowtime=new FlowTimeEvaluator();
		System.out.println(population.getMember(0).getFitness());
	return new ValueFitness(alpha*Makespan.evaluateMetric(individual, age, population, params)+(1-alpha)*Flowtime.evaluateMetric(individual, age, population, params));
	}

}
