package geneticOrder.fitnessevaluation;

import geneticOrder.metrics.EnergyEvaluator;
import geneticOrder.metrics.Evaluator;
import geneticOrder.metrics.MakespanEAEvaluator;
import geneticOrder.representation.SheduleIndividual;

import org.jaga.definitions.Fitness;
import org.jaga.definitions.FitnessEvaluationAlgorithm;
import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.Individual;
import org.jaga.definitions.Population;

public class EnergyMakespanFunction implements FitnessEvaluationAlgorithm {

	float alpha=(float) 0.5;
	@Override
	public Class<SheduleIndividual> getApplicableClass() {
		return SheduleIndividual.class;
	}

	public EnergyMakespanFunction(float alpha){
		this.alpha=alpha;
	}
	@Override
	public Fitness evaluateFitness(Individual individual, int age,
			Population population, GAParameterSet params){
		Evaluator Makespan=new MakespanEAEvaluator();
		Evaluator Energy=new EnergyEvaluator();
	return new ValueFitness(alpha*Makespan.evaluateMetric(individual, age, population, params)+(1-alpha)*Energy.evaluateMetric(individual, age, population, params));
	}

}
