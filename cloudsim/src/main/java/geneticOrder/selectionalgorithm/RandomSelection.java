package geneticOrder.selectionalgorithm;

import geneticOrder.representation.SheduleIndividual;

import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.Individual;
import org.jaga.definitions.Population;
import org.jaga.definitions.SelectionAlgorithm;

public class RandomSelection  implements SelectionAlgorithm {

	@Override
	public Individual select(Population population, int age,
			GAParameterSet params) throws ClassCastException {
		int random=params.getRandomGenerator().nextInt(0, population.getSize());
		return population.getMember(random);
	}

	@Override
	public Individual[] select(Population population, int howMany, int age,
			GAParameterSet params) throws ClassCastException {
		Individual[] individuals=new Individual[howMany];
		for(int i=0;i<howMany;i++){
			individuals[i]=this.select(population, age, params);
		}
		return individuals;
	}

	@Override
	public Class<SheduleIndividual> getApplicableFitnessClass() {
		return SheduleIndividual.class;
	}

}
