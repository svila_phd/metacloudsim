package geneticOrder.selectionalgorithm;

import java.util.ArrayList;
import java.util.Collections;

import org.jaga.definitions.Fitness;
import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.Individual;
import org.jaga.definitions.Population;
import org.jaga.definitions.SelectionAlgorithm;

public class LinearRankingSelection implements SelectionAlgorithm {

	private static final Class<Fitness> applicableFitnessClass = Fitness.class;
	
	double sp;
	
	public LinearRankingSelection(double sp){
		this.sp=sp;
	}
	@Override
	public Individual select(Population population, int age, GAParameterSet params) throws ClassCastException {
		ArrayList<Individual> list=new ArrayList<Individual>();
		for(Individual i:population.getAllMembers()){
			list.add(i);
		}
		if(list.size()==1){
			return list.get(0);
		}
		Collections.sort(list,new IndividualComparator());
		
		double[] s=new double[list.size()];
		
		s[0]=(2-sp);
		for(int i=2;i<=s.length;i++){
			s[i-1]=s[i-2]+(double)2-sp+(double)2*(sp-(double)1)*(((double)i-(double)1)/((double)s.length-(double)1));
		}
		double selec=params.getRandomGenerator().nextDouble(0,s[s.length-1]);
		
		for(int i=0;i<s.length;i++){
			if(selec<s[i]) return list.get(i);
		}
		return list.get(0);
	}
	@Override
	public Individual[] select(Population population, int howMany, int age, GAParameterSet params) throws ClassCastException {
		Individual[] newpop=new Individual[howMany];
		for(int i=0;i<howMany;i++){
			newpop[i]=select(population,age,params);
		}
		return newpop;
	}
	@Override
	public Class<Fitness> getApplicableFitnessClass() {
		return LinearRankingSelection.applicableFitnessClass;
	}
}