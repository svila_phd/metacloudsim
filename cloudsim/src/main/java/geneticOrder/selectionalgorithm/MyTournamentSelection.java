package geneticOrder.selectionalgorithm;

import geneticOrder.representation.SheduleIndividual;

import java.util.ArrayList;
import java.util.Collections;

import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.Individual;
import org.jaga.definitions.Population;
import org.jaga.definitions.SelectionAlgorithm;

public class MyTournamentSelection  implements SelectionAlgorithm {
	private int numCandidates=2;
	private int probabilityOfBest=75;
	public MyTournamentSelection(){}
	
	public MyTournamentSelection(int numCandidates,int probability){
		this.numCandidates=numCandidates;
		this.probabilityOfBest=probability;
	}
	@Override
	public Individual select(Population population, int age,
			GAParameterSet params) throws ClassCastException {
		ArrayList<Individual>FearlessCombatents=new ArrayList<Individual>();
		for(int i=0;i<numCandidates;i++){
			int r=params.getRandomGenerator().nextInt(0,population.getSize());
			FearlessCombatents.add(population.getMember(r));
		}
		Collections.sort(FearlessCombatents,new IndividualComparator());
		int ci = numCandidates - 1;
		for (; ; ) {
			double dice = params.getRandomGenerator().nextInt(1,101);
			if (dice < probabilityOfBest){
				return FearlessCombatents.get(ci);
			}
			if(--ci < 0){
				ci = numCandidates - 1;
			}
		}
	}

	@Override
	public Individual[] select(Population population, int howMany, int age,
			GAParameterSet params) throws ClassCastException {
		Individual[] individuals=new Individual[howMany];
		for(int i=0;i<howMany;i++){
			individuals[i]=this.select(population, age, params);
		}
		return individuals;
	}

	@Override
	public Class<SheduleIndividual> getApplicableFitnessClass() {
		return SheduleIndividual.class;
	}

}
