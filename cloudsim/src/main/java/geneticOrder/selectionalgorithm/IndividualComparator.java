package geneticOrder.selectionalgorithm;

import java.util.Comparator;

import org.jaga.definitions.Fitness;
import org.jaga.definitions.Individual;

public class IndividualComparator implements Comparator< Individual> {
	public int compare(Individual arg0, Individual arg1) {
		Fitness fit0=(Fitness) ((Individual) arg0).getFitness();
		Fitness fit1=(Fitness) ((Individual) arg1).getFitness();
		
		if(fit0.isBetter(fit1))return 1;
		if(fit0.isWorse(fit1))return -1;
		return 0;
	}

}
