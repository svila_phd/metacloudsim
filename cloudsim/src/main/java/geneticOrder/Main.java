package geneticOrder;

import geneticOrder.fitnessevaluation.MakespanFunction;
import geneticOrder.masteralgorithm.MyGa;
import geneticOrder.representation.SheduleIndividual;
import geneticOrder.representation.SheduleIndividualFactory;
import geneticOrder.reproduction.OnePointCrossover;
import geneticOrder.selectionalgorithm.RandomSelection;

import java.util.ArrayList;

import org.jaga.hooks.DebugHook;
import org.jaga.util.DefaultParameterSet;
import org.jaga.util.DefaultRandomGenerator;
import org.jaga.util.FittestIndividualResult;

import system.eaclustersim.Cluster;
import system.eaclustersim.MultiCluster;
import system.jobs.Job;
import system.jobs.JobList;
import util.BadAllocationException;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//creem els jobs
		ArrayList<Job> jobs=new ArrayList<Job>();
		jobs.add(new Job(1,16,(double)500,(double)0.4,(double)100));
		jobs.add(new Job(2,7,(double)300,(double)0.3,(double)50));
		jobs.add(new Job(3,8,(double)400,(double)0.5,(double)60));
		JobList.setInstance(jobs);
		//creem el multicluster
		ArrayList<Cluster> clusters=new ArrayList<Cluster>();
		clusters.add(new Cluster(1,10,5,1000,300,350,400,200));
		clusters.add(new Cluster(1,15,5,1000,300,350,400,200));
		try {
			MultiCluster.setInstance(clusters,null);
		} catch (BadAllocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		
		DefaultParameterSet dps=new DefaultParameterSet();
    	dps.setFitnessEvaluationAlgorithm(new MakespanFunction());
    	dps.setIndividualsFactory(new SheduleIndividualFactory());
    	dps.setMaxBadReproductionAttempts(0);
    	dps.setMaxGenerationNumber(2);
    	dps.setPopulationSize(5);
    	dps.setRandomGenerator(new DefaultRandomGenerator());
    	dps.setReproductionAlgorithm(new OnePointCrossover(75,25));
    	dps.setSelectionAlgorithm(new RandomSelection());
    	MyGa ga =new MyGa(null);
    	ga.addHook(new DebugHook());
    	FittestIndividualResult result = (FittestIndividualResult) ga.exec(dps);
    	SheduleIndividual ind=(SheduleIndividual)result.getFittestIndividual();
		System.out.println("FINISH"+ind);
	}

}
