package simulator;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SettingsLoader {
	private String filename;
	private HashMap<String, String> settings;
	
	public SettingsLoader(String path){
		if (path.charAt(path.length()-1) == '/'){
			// nothing
		}else{
			path = path + "/";
		}
		
		this.filename = path + "input/settings.xml";
		
		this.settings = new HashMap<String, String>();
	}
	
	public boolean readFile(){
		File file = new File(this.filename);
		if (file.exists() && file.isFile()){
			parseFile(file);
		}else{
			System.out.println("ERROR: can't load settings file.");
			return false;
		}
		
		System.out.println("Settings loaded. " + this.settings.size() + " elements loaded.");
		return true;
	}

	
	private void parseFile(File file){
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		try {
			db = dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		Document doc = null;
		try {
			doc = db.parse(file);
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		doc.getElementById("Settings");
		NodeList settingList = doc.getElementsByTagName("Setting");
		for (int i = 0; i < settingList.getLength(); i++) {
			Element setting = (Element) settingList.item(i);
			String name = setting.getAttribute("name");
			String value = setting.getAttribute("value");
			setSetting(name, value);
		}
	}
	
	
	private void setSetting(String name, String value){
		this.settings.put(name, value);
	}
	
	
	private String getSetting(String name){
		return this.settings.get(name);
	}
	
	public String getSettingString(String name){
		return getSetting(name);
	}
	
	public int getSettingInt(String name){
		return Integer.parseInt(getSetting(name));
	}
	
	public double getSettingDouble(String name){
		
		return 0.0;	
	}
	
	public boolean getSettingBoolean(String name){
		String value = getSetting(name);
		if (value.equals("true")){
			return true;
		}else if (value.equals("false")){
			return false;
		}else{
			System.out.println("The setting element: " + name + " is not boolean.");
			System.exit(1);
		}
		
		// return something
		return false;
	}
	
	
	
}
