package simulator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import simulator.Job;
import gridsim.AllocPolicy;
import gridsim.GridResource;
import gridsim.Gridlet;
import gridsim.Machine;
import gridsim.MachineList;
import gridsim.PE;
import gridsim.ResourceCalendar;
import gridsim.ResourceCharacteristics;
import gridsim.net.Link;
import gridsim.net.SimpleLink;

public class MCEnvironment extends GridResource {

	

	public MCEnvironment(String name, Link link,
			ResourceCharacteristics resource, ResourceCalendar calendar,
			AllocPolicy policy) throws Exception {
		super(name, link, resource, calendar, policy);
	}

	public HashMap<Integer,HashMap<Integer,Double>> calculateEnvironmentExecTime(CommWorkload workload){
		// iterar cada job, y sumar el tiempo que ha estado ejecutando en cada
		// maquina.
		HashMap<Integer,HashMap<Integer,Double>>machineTime=new HashMap<Integer,HashMap<Integer,Double>>();
		for(int i=0; i<this.resource_.getMachineList().size(); i++){
			int machineID=this.resource_.getMachineList().get(i).getMachineID();
			machineTime.put(machineID, new HashMap<Integer,Double>());
			for(int j =0; j < this.resource_.getMachineList().get(i).getPEList().size(); j++){
				int peID=this.resource_.getMachineList().get(i).getPEList().get(j).getID();
				machineTime.get(machineID).put(peID, 0.0);
			}
		}
		
		
		Iterator<Gridlet> iter = workload.getGridletList().iterator();
		while (iter.hasNext()){
			Job job = (Job)iter.next();
			int m[][] = job.getAllocation();
			for (int i=0; i<m.length; i++){
				double execTime = job.getFinishTime()-job.getExecStartTime();
				double procTime = (execTime*job.getSigma());
				procTime+=machineTime.get(m[i][0]).get(m[i][1]);
				machineTime.get(m[i][0]).put(m[i][1],procTime); 
			}
		}
		
		return machineTime;
	}
	
	public double getEnergyConsumed(){
		double energy=0;
		for (Machine m : this.resource_.getMachineList()){
			energy+=((EnergyMachine)m).getConsumition();
		}
		return energy;
	}
	
	public HashMap<Integer,HashMap<Integer,Double>> calculateEnvironmentusage(CommWorkload workload){
		// iterar cada job, y sumar el tiempo que ha estado ejecutando en cada
		// maquina.
		HashMap<Integer,HashMap<Integer,Double>>machineTime=new HashMap<Integer,HashMap<Integer,Double>>();
		for(int i=0; i<this.resource_.getMachineList().size(); i++){
			int machineID=this.resource_.getMachineList().get(i).getMachineID();
			machineTime.put(machineID, new HashMap<Integer,Double>());
			for(int j =0; j < this.resource_.getMachineList().get(i).getPEList().size(); j++){
				int peID=this.resource_.getMachineList().get(i).getPEList().get(j).getID();
				machineTime.get(machineID).put(peID, 0.0);
			}
		}
	
		HashMap<Integer,HashMap<Integer,Double>>machineusage=new HashMap<Integer,HashMap<Integer,Double>>();
		for(int i=0; i<this.resource_.getMachineList().size(); i++){
			int machineID=this.resource_.getMachineList().get(i).getMachineID();
			machineusage.put(machineID, new HashMap<Integer,Double>());
		}
		//Iterator iter = this.jobs.entrySet().iterator();
		Iterator<Gridlet> iter = workload.getGridletList().iterator();
		while (iter.hasNext()){
			Job job = (Job)iter.next();
			int m[][] = job.getAllocation();
			for (int i=0; i<m.length; i++){
				double time=machineTime.get(m[i][0]).get(m[i][1]);
				time+=job.getFinishTime()-job.getExecStartTime();
				machineTime.get(m[i][0]).put(m[i][1],time); 
			}
		}
		
		for(int i :machineTime.keySet()){
			for(int j : machineTime.get(i).keySet()){
				machineusage.get(i).put(j,machineTime.get(i).get(j)/workload.getMakespan());
			}
		}
		
		return machineusage;
	}
	
	public static MCEnvironment CreateCluster(String name,CommPolicy policy, String path,
			SimulatorLog log_,String[] policyArguments) {
		MCEnvironment cluster=null;
		MachineList machines = new MachineList();
		readFile(machines, path,log_);
		// --------------------------------
		// CREATE BASIC STRUCTURE
		// --------------------------------S
		// 4. Create a ResourceCharacteristics object that stores the
		// properties of a Grid resource: architecture, OS, list of
		// Machines, allocation policy: time- or space-shared, time zone
		// and its price (G$/PE time unit).
		String arch = "Sun Ultra"; // system architecture
		String os = "Solaris"; // operating system
		double time_zone = 9.0; // time zone this resource located
		double cost = 3.0; // the cost of using this resource
		ResourceCharacteristics resConfig = new ResourceCharacteristics(arch,
				os, machines, ResourceCharacteristics.SPACE_SHARED,
				time_zone, cost);

		// 5. Finally, we need to create a GridResource object.
		long seed = 11L * 13 * 17 * 19 * 23 + 1;
		double peakLoad = 0.0; // the resource load during peak hour
		double offPeakLoad = 0.0; // the resource load during off-peak hr
		double holidayLoad = 0.0; // the resource load during holiday

		// incorporates weekends so the grid resource is on 7 days a week
		LinkedList<Integer> Weekends = new LinkedList<Integer>();
		Weekends.add(new Integer(Calendar.SATURDAY));
		Weekends.add(new Integer(Calendar.SUNDAY));

		// incorporates holidays. However, no holidays are set in this example
		LinkedList<Integer> Holidays = new LinkedList<Integer>();

		int MTU = 1500;
		int delay = 10;
		int baud_rate = 1000000;
		try {
			ResourceCalendar resCalendar = new ResourceCalendar(time_zone,
					peakLoad, offPeakLoad, holidayLoad, Weekends, Holidays,
					seed);

			// With policy specified
				cluster = new MCEnvironment(name, new SimpleLink(
						"MultiCluster_link", baud_rate, delay, MTU), resConfig,
						resCalendar, policy);
				

		} catch (Exception e) {
			System.out.println("MCEnvironment ERROR: ");
			log_.addMessage("MCEnvironment ERROR: ");
			e.printStackTrace();
			System.out.println("Exit.");
			log_.addMessage("Exit.");
			System.exit(1);
		}
		
		//
		log_.addMessage("\nLIST OF CLUSTERS");
		Iterator<Machine> iter = machines.iterator();
		while (iter.hasNext()) {
			Machine m = iter.next();
			log_.addMessage("Cluster " + m.getMachineID() + ": nodes="
					+ m.getNumPE() + " MIPS:" + m.getMIPSRating());
		}
		log_.addMessage("");

		return cluster;

	}
	
	public static MCEnvironment CreateCustomCluster(String name,CommPolicy policy, String path,
			SimulatorLog log_,String[] policyArguments, MachineList machines) {
		MCEnvironment cluster=null;
		//MachineList machines = new MachineList();
		//readFile(machines, path,log_);
		// --------------------------------
		// CREATE BASIC STRUCTURE
		// --------------------------------S
		// 4. Create a ResourceCharacteristics object that stores the
		// properties of a Grid resource: architecture, OS, list of
		// Machines, allocation policy: time- or space-shared, time zone
		// and its price (G$/PE time unit).
		String arch = "Sun Ultra"; // system architecture
		String os = "Solaris"; // operating system
		double time_zone = 9.0; // time zone this resource located
		double cost = 3.0; // the cost of using this resource
		ResourceCharacteristics resConfig = new ResourceCharacteristics(arch,
				os, machines, ResourceCharacteristics.SPACE_SHARED,
				time_zone, cost);

		// 5. Finally, we need to create a GridResource object.
		long seed = 11L * 13 * 17 * 19 * 23 + 1;
		double peakLoad = 0.0; // the resource load during peak hour
		double offPeakLoad = 0.0; // the resource load during off-peak hr
		double holidayLoad = 0.0; // the resource load during holiday

		// incorporates weekends so the grid resource is on 7 days a week
		LinkedList<Integer> Weekends = new LinkedList<Integer>();
		Weekends.add(new Integer(Calendar.SATURDAY));
		Weekends.add(new Integer(Calendar.SUNDAY));

		// incorporates holidays. However, no holidays are set in this example
		LinkedList<Integer> Holidays = new LinkedList<Integer>();

		int MTU = 1500;
		int delay = 10;
		int baud_rate = 1000000;
		try {
			ResourceCalendar resCalendar = new ResourceCalendar(time_zone,
					peakLoad, offPeakLoad, holidayLoad, Weekends, Holidays,
					seed);

			// With policy specified
				cluster = new MCEnvironment(name, new SimpleLink(
						"MultiCluster_link", baud_rate, delay, MTU), resConfig,
						resCalendar, policy);
				

		} catch (Exception e) {
			System.out.println("MCEnvironment ERROR: ");
			//log_.addMessage("MCEnvironment ERROR: ");
			e.printStackTrace();
			System.out.println("Exit.");
			//log_.addMessage("Exit.");
			System.exit(1);
		}
		
		//
		//log_.addMessage("\nLIST OF CLUSTERS");
		Iterator<Machine> iter = machines.iterator();
		while (iter.hasNext()) {
			Machine m = iter.next();
			//log_.addMessage("Cluster " + m.getMachineID() + ": nodes="
			//		+ m.getNumPE() + " MIPS:" + m.getMIPSRating());
		}
		//log_.addMessage("");

		return cluster;

	}

	private static boolean readFile(MachineList machines, String path,SimulatorLog log_) {
		// start time for reading
		// --------------------------------
		// READ FROM FILE
		// --------------------------------
		// add the clusters into the multicluster objects
		// if the check is ok, proceed with the reading of the files
		if (checkPath(path,log_) == true) {
			BufferedReader reader = null;
			try {
				System.out.println("Reading environment file...");
				log_.addMessage("Reading environment file...");
				FileInputStream file = new FileInputStream(path);
				reader = new BufferedReader(new InputStreamReader(file));
				// read one line at the time
				while (reader.ready()) {
					Machine m=parseValue(reader.readLine(),log_);
					if(m!=null){
						machines.add(m);
					}
				}
				reader.close(); // close the file
				return true;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return false;
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}

		} else {
			return false;
		}

	}

	private static Machine parseValue(String line,SimulatorLog log_) {
		// skip a comment line
		if (line.startsWith(";")) {
			return null;
		}
		// arguments to read
		int id = 0;
		int numMachines = 0;
		int mips = 0;
		double acbw=0;
		double idleE=0;
		double compE=0;
		double startE=0;
		double stopE=0;
		// Array indexes
		int CLUSTER_ID = 0;
		int NUM_MACHINES = 1;
		int MIPS = 2;
		int ACBW = 3;
		int IDLEE=4;
		int COMPE=5;
		int STARTE=6;
		int STOPE=7;

		// max number of fields
		int MAX_FIELD = 8;

		String[] fieldArray = new String[MAX_FIELD];

		String[] sp = line.split("\\s+"); // split the fields based on a space
		int len = 0; // length of a string
		int index = 0; // the index of an array
		// check for each field in the array
		for (int i = 0; i < sp.length; i++) {
			len = sp[i].length(); // get the length of a string
			// if it is empty then ignore
			if (len == 0) {
				continue;
			} else {
				// if not, then put into the array
				fieldArray[index] = sp[i];
				index++;
			}
		}

		try {
			// Extract values and then, create a cluster object
			// Cluster ID

			id = new Integer(fieldArray[CLUSTER_ID].trim()).intValue();

			// number of machines
			numMachines = new Integer(fieldArray[NUM_MACHINES].trim())
					.intValue();

			// MIPS rating
			mips = new Integer(fieldArray[MIPS].trim()).intValue();

			// ACBW ( CURRENTLY NOT USED!! )
			// **********
			acbw = new Double(fieldArray[ACBW].trim()).doubleValue();
			// **********
			idleE= new Double(fieldArray[IDLEE].trim()).doubleValue();
			compE= new Double(fieldArray[COMPE].trim()).doubleValue();
			startE=new Double(fieldArray[STARTE].trim()).doubleValue();
			stopE=new Double(fieldArray[STOPE].trim()).doubleValue();
			
			return new EnergyMachine(id, numMachines, mips,acbw,idleE,compE,startE,stopE);

		} catch (Exception e) {
			System.out.println("Environment"
					+ ": Exception in reading file at line #" + line
					+ ", exception: " + e.getMessage());
			log_.addMessage("Environment"
					+ ": Exception in reading file at line #" + line
					+ ", exception: " + e.getMessage());
			return null;
		}
	}

	private static boolean checkPath(String path,SimulatorLog log_) {
		File file = new File(path);
		if (!file.isFile() && !file.isDirectory()) {
			System.out
					.println("MCEnviroment: error checking environment file: "
							+ path);
			System.out.println("Exit.");
			log_.addMessage("MCEnviroment: error checking environment file: "
					+ path);
			log_.addMessage("Exit.");
			return false;
		} else {
			return true;
		}
	}
}
