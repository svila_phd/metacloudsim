package simulator;

import gridsim.GridSim;
import gridsim.PE;

import java.util.ArrayList;

public class EnergyMachine extends CommMachine {

	private double energyIdle;
	private double energyComputing;
	private double energyStart;
	private double energyStop;
	private ArrayList<Double> timeComputing;
	private ArrayList<Double> timeIdle;
	private ArrayList<Double> lastUpdated;
	
	public EnergyMachine(int id, int numPE, int ratingPE,double MaxBW,double energyIdle,double energyComputing,double energyStart,double energyStop) {
		super(id, numPE, ratingPE,MaxBW);
		this.energyIdle=energyIdle;
		this.energyComputing=energyComputing;
		this.energyStart=energyStart;
		this.energyStop=energyStop;
		this.timeComputing=new ArrayList<Double>();
		this.timeIdle=new ArrayList<Double>();
		this.lastUpdated=new ArrayList<Double>();
		for(int i=0;i<numPE;i++){
			timeComputing.add(new Double(0f));
			timeIdle.add(new Double(0f));
			lastUpdated.add(new Double(0f));
		}
	}
	
	public double getEnergyIdle(){
		return this.energyIdle;
	}
	public double getEnergyComputing(){
		return this.energyComputing;
	}
	public double getEnergyStart(){
		return this.energyStart;
	}
	public double getEnergyStop(){
		return this.energyStop;
	}
	public void addTimeComputing(int PEID,double time){
		if(time<0){
			time=0;
		}
		Double value=timeComputing.get(PEID);
		value+=time;
		timeComputing.set(PEID,value);
	}
	public void addTimeIdle(int PEID,double time){
		if(time<0){
			time=0;
		}
		Double value=timeIdle.get(PEID);
		value+=time;
		timeIdle.set(PEID, value);

	}
	
	public boolean setStatusPE(boolean status, int peID) {
		//when a node is occupied, the time spent since last update until now is time spent idle
		boolean prevStatus=this.getPEList().get(peID).getStatus();
		double timeSpent=GridSim.clock()-this.lastUpdated.get(peID);
		if(prevStatus==PE.FREE && status==PE.BUSY){
			if(timeSpent*this.energyIdle<this.energyStart+this.energyStop){
				this.addTimeIdle(peID, timeSpent);
			}else{
				this.addTimeIdle(peID, (this.energyStart+this.energyStop)/this.energyIdle);
			}
		}
		if(prevStatus==PE.FREE && status==PE.FREE){
			this.addTimeIdle(peID, timeSpent-100);
		}
		this.lastUpdated.set(peID, GridSim.clock());
		return super.setStatusPE(status, peID);
	}
	
	public double getConsumition(){
		double consumition=0;
		for(int PEID=0;PEID<super.getNumPE();PEID++){
			double timeI=timeIdle.get(PEID);
			double timeC=timeComputing.get(PEID);
			consumition+=energyIdle*timeI/3600;
			consumition+=energyComputing*timeC/3600;
			//System.out.println(timeI+" "+timeC);
		}
		return consumition;
	}
}
