package simulator;

import simulator.Job;
import gridsim.PE;

public abstract class EnergyPolicy extends CommPolicy {
	public EnergyPolicy(String resourceName, String entityName) throws Exception {
		super(resourceName, entityName);
	}
	
	protected double CalculateSlowDownforMachine(ResJob rj,int machineID) {
		double machineRating = super.resource_.getMIPSRatingOfOnePE(machineID,0);
		// Calculate the Slowdown of processing (Basic rating is 1000)

		double SP = (double) 1000 / (double) machineRating;
		Job j = (Job) rj.getGridlet();
		return j.getSigma() * SP;
	}
	
	protected double calculateExecTimeForMachine(ResJob rj,int machineID){
		double finishTime = (double) rj.getGridletLength() * (double) this.CalculateSlowDownforMachine(rj,machineID);
		if (finishTime < 1.0) {
			finishTime = 1.0;
		}
		return finishTime;
	}
	
	
	protected void gridletFinish(ResJob rj, int status) {
		// the order is important! Set the status first then finalize
		// due to timing issues in ResGridlet class
		rj.setGridletStatus(status);
		rj.finalizeGridlet();
		super.sendFinishGridlet(rj.getGridlet());
		// Set PE on which Gridlet finished to FREE and calculate the energy for each node
		if(rj.getNumPE()==1){
			changeStatusPE(PE.FREE,rj.getMachineID(), rj.getPEID());
			double busyTime=(rj.getGridlet().getFinishTime()-rj.getGridlet().getExecStartTime());
			double computingTime=this.calculateExecTimeForMachine(rj, rj.getMachineID());
			double idleTime=busyTime-computingTime;
			addTimeComputing(computingTime,rj.getMachineID(),rj.getPEID());
			addTimeIdle(idleTime,rj.getMachineID(),rj.getPEID());
			
			
		}else{
			for(int i=0;i<rj.getListMachineID().length;i++){
				changeStatusPE(PE.FREE, rj.getListMachineID()[i], rj.getListPEID()[i]);
				double busyTime=(rj.getGridlet().getFinishTime()-rj.getGridlet().getExecStartTime());
				double computingTime=this.calculateExecTimeForMachine(rj, rj.getListMachineID()[i]);
				double idleTime=busyTime-computingTime;
				addTimeComputing(computingTime,rj.getListMachineID()[i],rj.getListPEID()[i]);
				addTimeIdle(idleTime,rj.getListMachineID()[i],rj.getListPEID()[i]);
			}
		}
		allocateQueueGridlet(); // move Queued Gridlet into exec list
	}
	public void addTimeComputing(double time,int machineID,int peID){
		EnergyMachine m=(EnergyMachine)resource_.getMachineList().getMachine(machineID);
		m.addTimeComputing(peID, time);
	}
	
	public void addTimeIdle(double time,int machineID,int peID){
		EnergyMachine m=(EnergyMachine)resource_.getMachineList().getMachine(machineID);
		m.addTimeIdle(peID, time);
	}
}
