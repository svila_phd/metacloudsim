package simulator;

import simulator.Job;
import gridsim.ResGridlet;

	
	
public class ResJob extends ResGridlet{
	private boolean preAllocation;
	private int[] allocationPreferences=null;
	
	public ResJob(Job job, long startTime, int duration, int reservID) {
		super(job, startTime, duration, reservID);
	}

	public ResJob(Job j) {
		super(j);
	}
	@Override
	public void updateGridletFinishedSoFar(double exectime){
		double soFar=(exectime-(this.getGridletLength()-this.getRemainingGridletLength()));
		super.updateGridletFinishedSoFar(soFar);
	}
	@Override
	public double getGridletLength() {
		if(super.getGridletFinishTime()==-1){
			return ((Job)super.getGridlet()).getOriginalRuntime();
		}else{
			super.getGridlet().setGridletLength(super.getGridletFinishTime());
			return super.getGridlet().getGridletLength();
		}
		
	}
	public double getBandwidthUsageInCluster(int ClusterID){
		//count the number of tasks executing in cluster ClusterID
		int numTasksInCluster=0;
		if(super.getNumPE()==1){
			if(super.getMachineID()==ClusterID){
				numTasksInCluster=1;
			}
		}else{
			for(int id:super.getListMachineID()){
				if(id==ClusterID){
					numTasksInCluster++;
				}
			}
		}
		//Calculate the formula of the comunication.
		double percentageOfComunication=0;
		Job j=(Job)this.getGridlet();
		double totalConsumedBW=numTasksInCluster*j.getPPBW();
		if(j.getNumPE()>1){
		percentageOfComunication=(j.getNumPE()-numTasksInCluster)/(j.getNumPE()-1);
		}
		return totalConsumedBW*percentageOfComunication;
	}

	public int[] getAllocationPreferences(){
		return this.allocationPreferences;
	}
	
	public void setAllocationPreferences(int[] allocationPreferences){
		this.allocationPreferences=allocationPreferences;
	}
	public void preAllcoate(){
		this.preAllocation=true;
	}
	
	public boolean isPreAllocated(){
		return preAllocation;
	}
	
	public double getSigma(){
		return ((Job)this.getGridlet()).getSigma();
	}
	
	public void setSigma(double sgm){
		((Job)this.getGridlet()).setSigma(sgm);
	}
	
	public double getPPBW(){
		return ((Job)this.getGridlet()).getPPBW();
	}
	
	public void setPPBW(double ppbw){
		((Job)this.getGridlet()).setPPBW(ppbw);
	}
}
