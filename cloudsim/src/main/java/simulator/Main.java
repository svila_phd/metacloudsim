package simulator;
import gridsim.GridSim;
import gridsim.Machine;
import gridsim.net.FIFOScheduler;
import gridsim.net.RIPRouter;
import gridsim.net.Router;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.HashMap;

import shedulers.CBSPolicy;
import shedulers.EAAllocPolicy;
import shedulers.EGAPolicy;
import shedulers.ERAPolicy;
import shedulers.FCFSPolicy;
/*import shedulers.GAOrderMESDEAPolicy;
import shedulers.GAOrderMESDPolicy;
import shedulers.GAOrderPolicy;
import shedulers.GRASPMESDPolicy;
import shedulers.HillClimbingPolicy;*/
import shedulers.JPRPolicy;
import shedulers.MESDPolicy;
import shedulers.MOGAPolicy;
import shedulers.PSOMOPolicy;
import shedulers.PSOPolicy;
import shedulers.energy.CBSEPolicy;
import shedulers.energy.JPREPolicy;
import shedulers.energy.METLEPolicy;
import shedulers.energy.MinMinPolicy;

public class Main {
	
	
	
	//---------------------------------------------
	// VERSION
	//---------------------------------------------
	
	private static String version = "MCGridsim version 1.0.0";
	
	//---------------------------------------------
	//---------------------------------------------
	
	private static String POLICY_FCFS = "fcfs";
	private static String POLICY_CBS = "cbs";
	private static String POLICY_JPR = "jpr";
	
	private static String POLICY_MESD = "mesd";
	private static String POLICY_GAM = "gaMESD";
	private static String POLICY_GAMEA = "gaMESDEA";
	private static String POLICY_GA = "ga";
	private static String POLICY_GRASP = "grasp";
	private static String POLICY_HILL = "hill";
	private static String POLICY_EA = "ea";
	private static String POLICY_ERA="era";
	private static String POLICY_MOGA="moga";
	
	private static String POLICY_EGA="ega";
	private static String POLICY_MINMIN="minmin";
	private static String POLICY_JPREA = "jpre";
	private static String POLICY_CBSE = "cbse";
	private static String POLICY_METLE = "metle";
	private static String POLICY_PSO = "pso";

/*
 * 
 * private static String POLICY_FCFS = "fcfs";
	private static String POLICY_CBS = "cbs";
	private static String POLICY_JPR = "jpr";
	
	private static String POLICY_MESD = "mesd"; // NO
	private static String POLICY_GAM = "gaMESD"; // NO
	private static String POLICY_GAMEA = "gaMESDEA"; // NO
	private static String POLICY_GA = "ga"; // NO
	private static String POLICY_GRASP = "grasp"; // NO
	private static String POLICY_HILL = "hill";
	private static String POLICY_EA = "ea"; // NO
	private static String POLICY_ERA="era"; // NO
	private static String POLICY_MOGA="moga";
	
	private static String POLICY_EGA="ega"; // NO
	private static String POLICY_MINMIN="minmin";
	private static String POLICY_JPREA = "jpre";
	private static String POLICY_CBSE = "cbse";
	private static String POLICY_METLE = "metle"; // NO
	private static String POLICY_PSO = "pso";
 */
	

	
	// a flag that denotes whether to trace GridSim events or not.
	static SimulatorLog log;

	public static void main(String[] args) throws Exception {
		
		String[] policyModificator = null;
		String idProva="0";
		
		System.out.println(version);
		System.out.println(" by Hector Blanco de Frutos. 2011 and Eloi Gabaldon Ponsa. 2014");
		System.out.println("------------------------------------------------");
		System.out.println("");
		
		//------------------------------------------------
		// ARGUMENTS PARSING AND CHECK
		//------------------------------------------------
		if (args.length < 2){
			System.out.println("Not enough arguments specified.");
			System.out.println("Call mode:  MCGridsim.jar input-folder alloc-policy policy-parameters(OPTIONAL)\n");
			System.out.println("Exit.");
			System.exit(0);
		}
		
		if (args.length > 2){
			policyModificator=new String[args.length-3];
			for(int i=2;i<args.length-1;i++){
				policyModificator[i-2] = args[i];
				System.out.println("Allocation policy arguments: " + policyModificator[i-2]);
			}
			idProva=args[args.length-1];
		}
		
		long start = System.currentTimeMillis();
		//------------------------------------------------
		// CHECK, VALIDATE, AND CREATE FOLDERS
		//------------------------------------------------
		// check input folder
		String projectPath = args[0];
		File file = new File(projectPath);
		if (!file.isDirectory() && !file.exists()){
			System.out.println("Input folder does not exist or is not accessible.");
			System.out.println("Exit.");
			System.exit(0);
		}
		// adjust '/' termination
		if (projectPath.charAt(projectPath.length()-1) != '/'){
			projectPath = projectPath + "/";
		}
		
		System.out.println("Project folder: " +  projectPath + "\n");
		// check base output path
		file = new File(projectPath + "output/");
		if (!file.exists()){
			System.out.println("Base output directory does not exist. Creating it...");
			// if output directory does not exist, create it.
			file.mkdir();
		}
		System.out.println("Base onput folder: " +  projectPath + "output/\n");
		
		
		
		//------------------------------------------------
		//	SETTINGS LOADER
		//------------------------------------------------
		SettingsLoader settings = new SettingsLoader(projectPath);
		if (settings.readFile() == false){
			System.out.println("\nMCGridSim abruptly exited due to I/O errors.\n");
			System.exit(1);
		}
		
		//------------------------------------------------
		//	LOG
		//------------------------------------------------
		log = new SimulatorLog();
		String modifi="";
		if(policyModificator!=null){
			for(String value : policyModificator){
				modifi+=value+"-";
			}
		}
		try {
		
			log.setFilename(projectPath + "output/" + args[1] + "_"+modifi+"_Log.txt");
		} catch (IOException e1) {
			System.out.println("Error creating the log file.");
			e1.printStackTrace();
			System.exit(1);
		}
				
		System.out.println();
		System.out.println("Starting a simulation...\n");		
		
		
		//------------------------------------------------
		//	GRIDSIM INITIALIZATION
		//------------------------------------------------
		
		//////////////////////////////////////////
		// First step: Initialize the GridSim package. It should be called
		// before creating any entities. We can't run this example without
		// initializing GridSim first. We will get run-time exception
		// error.
		Calendar calendar = Calendar.getInstance();

		
		// Initialize the GridSim package
		System.out.println("Initializing GridSim package");
		// 1 is the number of users
		GridSim.init(1, calendar, false);
		GridSim.disableDebugMode();
		
		
		
		//------------------------------------------------
		//	MULTICLUSTER ENVIRONMENT LOADER ANAD ALLOCATION POLICY
		//------------------------------------------------
		String envpath=projectPath + "input/" + settings.getSettingString("environment-file");
		String ClusterName="MultiC";
		CommPolicy policy=null;
		
		
		if(args[1].equals(POLICY_FCFS)){
			policy = new FCFSPolicy(ClusterName,"POLICY_FCFS");
		}else if(args[1].equals(POLICY_CBS)){
			policy=new CBSPolicy(ClusterName,"POLICY_CBS");
		}else if(args[1].equals(POLICY_JPR)){
			policy=new JPRPolicy(ClusterName,"POLICY_JPR");
		}else if(args[1].equals(POLICY_JPREA)){
			policy=new JPREPolicy(ClusterName,"POLICY_JPREA");
		}else if(args[1].equals(POLICY_MESD)){
			policy=new MESDPolicy(ClusterName,"POLICY_MESD",policyModificator);
		}/*else if(args[1].equals(POLICY_GRASP)){
			policy=new GRASPMESDPolicy(ClusterName,"POLICY_GRASP",policyModificator);
		}else if(args[1].equals(POLICY_GA)){
			policy=new GAOrderPolicy(ClusterName,"POLICY_GA",policyModificator);
		}else if(args[1].equals(POLICY_GAM)){
			policy=new GAOrderMESDPolicy(ClusterName,"POLICY_GAM",policyModificator);
		}else if(args[1].equals(POLICY_GAMEA)){
			policy=new GAOrderMESDEAPolicy(ClusterName,"POLICY_GAMEA",policyModificator);
		}else if(args[1].equals(POLICY_HILL)){
			policy=new HillClimbingPolicy(ClusterName,"POLICY_HILL",policyModificator);
		}*/else if(args[1].equals(POLICY_EA)){
			policy=new EAAllocPolicy(ClusterName,"POLICY_EA");
		}else if(args[1].equals(POLICY_METLE)){
			policy=new METLEPolicy(ClusterName,"POLICY_EB",policyModificator);
		}else if(args[1].equals(POLICY_ERA)){
			policy=new ERAPolicy(ClusterName,"POLICY_ERA");
		}else if(args[1].equals(POLICY_MOGA)){
			policy=new MOGAPolicy(ClusterName,"POLICY_MOGA",policyModificator,projectPath + "output/",idProva);
		}else if(args[1].equals(POLICY_EGA)){
			policy=new EGAPolicy(ClusterName,"POLICY_EGA",policyModificator,projectPath + "output/",idProva);
		}else if(args[1].equals(POLICY_MINMIN)){
			policy=new MinMinPolicy(ClusterName,"POLICY_MINMIN",policyModificator);
		}else if(args[1].equals(POLICY_CBSE)){
			policy=new CBSEPolicy(ClusterName,"POLICY_CBSE");
		}else if(args[1].equals(POLICY_PSO)){
			policy=new PSOMOPolicy(ClusterName,"POLICY_PSO",policyModificator,projectPath+"output/",idProva);
		}else{
			System.out.println("No allocation policy specified. Exit.");
			System.exit(1);
		}
		System.out.println("Allocation policy: " + policy);
		
		MCEnvironment cluster=MCEnvironment.CreateCluster(ClusterName,policy, envpath, log, policyModificator);
		
		for(Machine m:cluster.getResourceCharacteristics().getMachineList()){
			System.out.println("Machine: "+m.getMachineID()+" has "+m.getNumPE());
		}
		
		String workpath=projectPath + "input/"+ settings.getSettingString("workload-file");
		double baud_rate = 1000; // bits/sec
		double propDelay = 10;   // propagation delay in millisecond
		int mtu = 1500;          // max. transmission unit in byte
		
		CommWorkload workload = null;
		
		try {
			//workload = new CommWorkload("Workload",baud_rate,propDelay,mtu,workpath,cluster.get_name());
			} catch (Exception e1) {
			e1.printStackTrace();
			log.close();
			System.exit(1);
		}
		Router rC = new RIPRouter("routerC", false);
		FIFOScheduler userSched = new FIFOScheduler("UserSched_0");
		rC.attachHost(workload, userSched);
		FIFOScheduler resSched = new FIFOScheduler("MultiClusterSched_0");
		rC.attachHost(cluster, resSched);
		
		GridSim.startGridSimulation();
		workload.printGridletList(false);
		
		double end=System.currentTimeMillis();
		double simtime=(end-start)/1000;
		System.out.println("Simulation time : "+simtime);
		System.out.println("Makespan: "+workload.getMakespan());
		System.out.println("Energy: "+ cluster.getEnergyConsumed() );

		printReports(new String(projectPath + "output/" + args[1]+"_"+modifi+"_"+idProva+"_Result.txt"),simtime,workload,cluster);
		
	}
	public static void printReports(String reportPath,double simtime,CommWorkload workload,MCEnvironment environment) throws Exception{
		PrintWriter writer = new PrintWriter(reportPath, "UTF-8");
		writer.println("MAKESPAN="+workload.getMakespan());
		writer.println("ENERGY="+ environment.getEnergyConsumed() );
		writer.println("TIME="+simtime);
		writer.println("AVERAGE_WAIT=" + workload.getAverageWaitTime());
		writer.println("AVERAGE_EXEC=" + workload.getAverageExecTime());
		writer.println("AVERAGE_SLD=" +workload.getAverageSlowdown());
		HashMap<Integer,HashMap<Integer,Double>> machineUsage=environment.calculateEnvironmentusage(workload);
		for(int i:machineUsage.keySet()){
			for(int j:machineUsage.get(i).keySet()){
				writer.println("USAGE:"+i+"-"+j+"-"+machineUsage.get(i).get(j));
			}
		}
		HashMap<Integer,HashMap<Integer,Double>> machineTime=environment.calculateEnvironmentExecTime(workload);
		for(int i:machineTime.keySet()){
			for(int j:machineTime.get(i).keySet()){
				writer.println("EXEC:"+i+"-"+j+"-"+machineTime.get(i).get(j));
			}
		}
		writer.close();
	}
}
	