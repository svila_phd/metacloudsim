package simulator;

import simulator.Job;
import gridsim.Gridlet;
import gridsim.Machine;
import gridsim.ResGridlet;

public abstract class CommPolicy extends Policy {
	public CommPolicy(String resourceName, String entityName) throws Exception {
		super(resourceName, entityName);
	}

	protected double CalculateSlowDown(ResJob rj) {
		// Get the slowest Machine rating
		int slowRating = Integer.MAX_VALUE;
		if (rj.getNumPE() == 1) {
			slowRating = super.resource_.getMIPSRatingOfOnePE(
					rj.getMachineID(), rj.getPEID());
		} else {
			for (int i = 0; i < rj.getListMachineID().length; i++) {
				slowRating = Math.min(slowRating,super.resource_.getMIPSRatingOfOnePE(rj.getListMachineID()[i], 0));
			}
		}
		// Calculate the Slowdown of processing (Basic rating is 1000)
		double SP = (double) 1000 / (double) slowRating;

		// Get the most saturated link
		double saturedLink = 1.0;
		if (rj.getNumPE() > 1) {
			for (int i = 0; i < rj.getListMachineID().length; i++) {
				CommMachine cm = (CommMachine) super.resource_.getMachineList()
						.getMachine(rj.getListMachineID()[i]);
				if (cm.isSatured()) {
					saturedLink = Math.min(saturedLink, cm.getSaturedLink());
				}
			}
		}
		double SC = 1.0;
		if (saturedLink < 1.0) {
			SC = 1.0 / saturedLink;

		}
		// Calculate the Slowdown
		Job j = (Job) rj.getGridlet();
		return j.getSigma() * SP + ((double) 1 - j.getSigma()) * SC;
	}

	@Override
	protected double forecastFinishTime(ResJob rj) {

		double finishTime = (double) rj.getGridletLength() * (double) this.CalculateSlowDown(rj);
		// This is as a safeguard since the finish time can be extremely
		// small close to 0.0, such as 4.5474735088646414E-14. Hence causing
		// some Gridlets never to be finished and consequently hang the program
		if (finishTime < 1.0) {
			finishTime = 1.0;
		}
		return finishTime;
	}
	protected double forecastMidExecutionFinishTime(ResJob rj) {
		double finishTime= (double) rj.getRemainingGridletLength()*(double) this.CalculateSlowDown(rj);
		// This is as a safeguard since the finish time can be extremely
		// small close to 0.0, such as 4.5474735088646414E-14. Hence causing
		// some Gridlets never to be finished and consequently hang the program
		if (finishTime < 1.0) {
			finishTime = 1.0;
		}
		return finishTime;
	}

	protected void setComunicationUsage() {
		for (Machine m : super.resource_.getMachineList()) {
			CommMachine cm = (CommMachine) m;
			double machineBWUsage = 0.0;
			for (ResGridlet rg : this.gridletInExecList_) {
				ResJob j = (ResJob) rg;
				machineBWUsage += j.getBandwidthUsageInCluster(cm.getMachineID());
			}
			cm.setCommunicationLoad(machineBWUsage);
		}
	}

	protected void checkChangeOnSaturedMachines() {
		for (Machine m : super.resource_.getMachineList()) {
			CommMachine cm = (CommMachine) m;
			// if a change in saturation conditions occurrs in one machine
			if (cm.SaturationChange()) {
				// Search for all jobs using this machine
				for (ResGridlet rg : this.gridletInExecList_) {
					boolean jobMigthChange = false;
					ResJob j = (ResJob) rg;
					if (j.getNumPE() > 1) {
						for (int mid : j.getListMachineID()) {
							if (cm.getMachineID() == mid) {
								jobMigthChange = true;
								break;
							}
						}
					}
					// We know if this job j is executing in a machine that has
					// changed his saturation status sinse the job started
					if (jobMigthChange) {
						double newFinishTime = forecastMidExecutionFinishTime(j);
						double realFinishTime=j.getGridletLength()-j.getRemainingGridletLength()+newFinishTime;
						int roundUpNewFinish= (int) (newFinishTime +1);
						int roundUpRealFinish= (int) (realFinishTime +1);
						j.setFinishTime(roundUpRealFinish);
						//System.out.println("GRIDLED LENGTH: "+rj.getGridletLength());
						j.getGridletLength();
						// then send this into itself
						super.sendInternalEvent(roundUpNewFinish);
					}
				}
			}
		}
	}
	protected synchronized void checkGridletCompletion() {
		ResJob rj = null;
		int i = 0;

		// NOTE: This one should stay as it is since gridletFinish()
		// will modify the content of this list if a Gridlet has finished.
		// Can't use iterator since it will cause an exception
		while (i < gridletInExecList_.size()) {
			rj = (ResJob) gridletInExecList_.get(i);
			if (rj.getRemainingGridletLength() == 0.0) {
				gridletInExecList_.remove(rj);
				gridletFinish(rj, Gridlet.SUCCESS);
				//System.out.println("Gridlet "+rj.getGridletID()+ " finishes. remaining "+(gridletInExecList_.size()+this.gridletQueueList_.size()));
				continue;
			}
			i++;
		}
		this.setComunicationUsage();
		this.checkChangeOnSaturedMachines();
	}
	protected boolean allocatePEtoGridlet(ResGridlet rgl) {
		
		if (this.resource_.getNumFreePE() < rgl.getNumPE()) {
			return false;
		}
		
		ResJob rj=(ResJob)rgl;
		if(!howToAllocatePEtoGridlet( rgl)){
			return false;
		}
		
		rj.setGridletStatus(Gridlet.INEXEC); // change Gridlet status
		// add this Gridlet into execution list
		gridletInExecList_.add(rj);
		Job j =(Job)rgl.getGridlet();
		if(j.getNumPE()>1){
			j.allocateJobToPE(rgl.getListMachineID(), rgl.getListPEID());
		}else{
			int[] machineID=new int[1];
			int[] PEID=new int[1];
			machineID[0]=rgl.getMachineID();
			PEID[0]=rgl.getPEID();
			j.allocateJobToPE(machineID, PEID);
		}
		// Set allocated PE to BUSY status
		// Identify Completion Time and Set Interrupt
		this.setComunicationUsage();
		double time = forecastFinishTime(rj);
		int roundUpTime = (int) (time + 1); // rounding up
		rj.setFinishTime(roundUpTime);
		//System.out.println("GRIDLED LENGTH: "+rj.getGridletLength());
		rj.getGridletLength();
		// then send this into itself
		super.sendInternalEvent(roundUpTime);
		this.checkChangeOnSaturedMachines();
		return true;
	}
}
