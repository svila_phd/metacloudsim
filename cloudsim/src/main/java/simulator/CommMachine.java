package simulator;

import gridsim.Machine;

public class CommMachine extends Machine {

	private double MaxBW;
	private double communicationLoad;
	private boolean satured;
	public CommMachine(int id, int numPE, int ratingPE,double MaxBW) {
		super(id, numPE, ratingPE);
		this.MaxBW=MaxBW;
		this.communicationLoad=0;
		satured=false;
	}
	public double getMaxBW(){
		return this.MaxBW;
	}
	
	public void setMaxBW(double MaxBW){
		this.MaxBW=MaxBW;
	}
	public void setCommunicationLoad(double communicationLoad){
		this.communicationLoad=communicationLoad;
	}
	public double getSaturedLink(){
		if(communicationLoad==0){
			return 1;
		}
		return Math.min(MaxBW/communicationLoad,1);
	}
	public boolean SaturationChange(){
		boolean change;
		if((MaxBW/communicationLoad)<1){
			change=(true!=satured);
			satured=true;
		}else{
			change=(false!=satured);
			satured=false;
		}
		return change;
	}
	
	public boolean isSatured(){
		return satured;
	}
}
