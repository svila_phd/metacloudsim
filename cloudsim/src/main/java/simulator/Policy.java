/*
 * Title:        GridSim Toolkit
 * Description:  GridSim (Grid Simulation) Toolkit for Modeling and Simulation
 *               of Parallel and Distributed Systems such as Clusters and Grids
 * License:      GPL - http://www.gnu.org/copyleft/gpl.html
 *
 */

package simulator;


import eduni.simjava.Sim_event;
import eduni.simjava.Sim_system;
import simulator.Job;
import gridsim.AllocPolicy;
import gridsim.GridSim;
import gridsim.GridSimTags;
import gridsim.Gridlet;
import gridsim.Machine;
import gridsim.PE;
import gridsim.ResGridlet;
import gridsim.ResGridletList;

/**
 * SpaceShared class is an allocation policy for GridResource that behaves
 * exactly like First Come First Serve (FCFS). This is a basic and simple
 * scheduler that runs each Gridlet to one Processing Element (PE). If a Gridlet
 * requires more than one PE, then this scheduler only assign this Gridlet to
 * one PE.
 * 
 * @author Manzur Murshed and Rajkumar Buyya
 * @author Anthony Sulistio (re-written this class)
 * @author Marcos Dias de Assuncao (has made some methods synchronized)
 * @since GridSim Toolkit 2.2
 * @see gridsim.GridSim
 * @see gridsim.ResourceCharacteristics
 * @invariant $none
 */
public abstract class Policy extends AllocPolicy {
	protected ResGridletList gridletQueueList_; // Queue list
	protected ResGridletList gridletInExecList_; // Execution list
	private ResGridletList gridletPauseList_;// Pause list
	private double lastUpdateTime_;
	/**
	 * Allocates a new SpaceShared object
	 * 
	 * @param resourceName
	 *            the GridResource entity name that will contain this allocation
	 *            policy
	 * @param entityName
	 *            this object entity name
	 * @throws Exception
	 *             This happens when one of the following scenarios occur:
	 *             <ul>
	 *             <li>creating this entity before initializing GridSim package
	 *             <li>this entity name is <tt>null</tt> or empty
	 *             <li>this entity has <tt>zero</tt> number of PEs (Processing
	 *             Elements). <br>
	 *             No PEs mean the Gridlets can't be processed. A GridResource
	 *             must contain one or more Machines. A Machine must contain one
	 *             or more PEs.
	 *             </ul>
	 * @see gridsim.GridSim#init(int, Calendar, boolean, String[], String[],
	 *      String)
	 * @pre resourceName != null
	 * @pre entityName != null
	 * @post $none
	 */
	public Policy(String resourceName, String entityName) throws Exception {
		super(resourceName, entityName);

		// initialises local data structure
		this.gridletInExecList_ = new ResGridletList();
		this.gridletQueueList_ = new ResGridletList();
		this.gridletPauseList_=new ResGridletList();
	}

	/**
	 * Handles internal events that are coming to this entity.
	 * 
	 * @pre $none
	 * @post $none
	 */
	public void body() {
		// a loop that is looking for internal events only
		Sim_event ev = new Sim_event();
		while (Sim_system.running()) {
			super.sim_get_next(ev);

			// if the simulation finishes then exit the loop
			if (ev.get_tag() == GridSimTags.END_OF_SIMULATION
					|| super.isEndSimulation()) {
				for(Machine m :super.resource_.getMachineList()){
					for(PE p:m.getPEList()){
						m.setStatusPE(PE.FREE, p.getID());
					}
				}
				break;
			}

			// Internal Event if the event source is this entity
			if (ev.get_src() == super.myId_ && gridletInExecList_.size() > 0) {
				updateGridletProcessing(); // update Gridlets
				checkGridletCompletion(); // check for finished Gridlets
			}
		}

		// CHECK for ANY INTERNAL EVENTS WAITING TO BE PROCESSED
		while (super.sim_waiting() > 0) {
			// wait for event and ignore since it is likely to be related to
			// internal event scheduled to update Gridlets processing
			super.sim_get_next(ev);
			System.out.println(super.resName_
					+ ".SpaceShared.body(): ignore internal events");
		}
	}

	/**
	 * Schedules a new Gridlet that has been received by the GridResource
	 * entity.
	 * 
	 * @param gl
	 *            a Gridlet object that is going to be executed
	 * @param ack
	 *            an acknowledgement, i.e. <tt>true</tt> if wanted to know
	 *            whether this operation is success or not, <tt>false</tt>
	 *            otherwise (don't care)
	 * @pre gl != null
	 * @post $none
	 */
	public synchronized void gridletSubmit(Gridlet gl, boolean ack) { 
		// update the current Gridlets in exec list up to this point
		
		updateGridletProcessing();
		Job j=(Job) gl;
		ResJob rg = new ResJob(j);
		
		boolean success=false;
		if(rg.getNumPE()>super.resource_.getNumPE()){
			System.out.println("The job requires more nodes than aviable. REJECTED");
			rg.setGridletStatus(Gridlet.FAILED_RESOURCE_UNAVAILABLE);
			return;
		}
		// if the queue list is empty (no other jobs are waiting) and enought PE aviable
		if (gridletQueueList_.size() ==0 ) {
		//	try to allocate it directly
			success = allocatePEtoGridlet(rg);
		}
		if(!success){
			// if no available PE then put the ResGridlet into a Queue list
			rg.setGridletStatus(Gridlet.QUEUED);
			gridletQueueList_.add(rg);
		}
		
		// sends back an ack if required
		if (ack) {
			super.sendAck(GridSimTags.GRIDLET_SUBMIT_ACK, true,
					gl.getGridletID(), gl.getUserID());
		}
	}

	/**
	 * Finds the status of a specified Gridlet ID.
	 * 
	 * @param gridletId
	 *            a Gridlet ID
	 * @param userId
	 *            the user or owner's ID of this Gridlet
	 * @return the Gridlet status or <tt>-1</tt> if not found
	 * @see gridsim.Gridlet
	 * @pre gridletId > 0
	 * @pre userId > 0
	 * @post $none
	 */
	public synchronized int gridletStatus(int gridletId, int userId) {
		ResJob rgl = null;

		// Find in EXEC List first
		int found = gridletInExecList_.indexOf(gridletId, userId);
		if (found >= 0) {
			// Get the Gridlet from the execution list
			rgl = (ResJob) gridletInExecList_.get(found);
			return rgl.getGridletStatus();
		}

		// Find in Queue List
		found = gridletQueueList_.indexOf(gridletId, userId);
		if (found >= 0) {
			// Get the Gridlet from the execution list
			rgl = (ResJob) gridletQueueList_.get(found);
			return rgl.getGridletStatus();
		}
		// Find in Pause List
		found = gridletPauseList_.indexOf(gridletId, userId);
		if (found >= 0) {
			// Get the Gridlet from the execution list
			rgl = (ResJob) gridletPauseList_.get(found);
			return rgl.getGridletStatus();
		}


		// if not found in all 3 lists then no found
		return -1;
	}

	/**
	 * Cancels a Gridlet running in this entity. This method will search the
	 * execution, queued and paused list. The User ID is important as many users
	 * might have the same Gridlet ID in the lists. <b>NOTE:</b>
	 * <ul>
	 * <li>Before canceling a Gridlet, this method updates all the Gridlets in
	 * the execution list. If the Gridlet has no more MIs to be executed, then
	 * it is considered to be <tt>finished</tt>. Hence, the Gridlet can't be
	 * canceled.
	 * 
	 * <li>Once a Gridlet has been canceled, it can't be resumed to execute
	 * again since this method will pass the Gridlet back to sender, i.e. the
	 * <tt>userId</tt>.
	 * 
	 * <li>If a Gridlet can't be found in both execution and paused list, then a
	 * <tt>null</tt> Gridlet will be send back to sender, i.e. the
	 * <tt>userId</tt>.
	 * </ul>
	 * 
	 * @param gridletId
	 *            a Gridlet ID
	 * @param userId
	 *            the user or owner's ID of this Gridlet
	 * @pre gridletId > 0
	 * @pre userId > 0
	 * @post $none
	 */
	public synchronized void gridletCancel(int gridletId, int userId) {
		// cancels a Gridlet
		ResJob rgl = cancel(gridletId, userId);

		// if the Gridlet is not found
		if (rgl == null) {
			System.out.println(super.resName_
					+ ".SpaceShared.gridletCancel(): Cannot find "
					+ "Gridlet #" + gridletId + " for User #" + userId);

			super.sendCancelGridlet(GridSimTags.GRIDLET_CANCEL, null,
					gridletId, userId);
			return;
		}

		// if the Gridlet has finished beforehand then prints an error msg
		if (rgl.getGridletStatus() == Gridlet.SUCCESS) {
			System.out.println(super.resName_
					+ ".SpaceShared.gridletCancel(): Cannot cancel"
					+ " Gridlet #" + gridletId + " for User #" + userId
					+ " since it has FINISHED.");
		}

		// sends the Gridlet back to sender
		rgl.finalizeGridlet();
		super.sendCancelGridlet(GridSimTags.GRIDLET_CANCEL, rgl.getGridlet(),
				gridletId, userId);
	}

	/**
	 * Pauses a Gridlet only if it is currently executing. This method will
	 * search in the execution list. The User ID is important as many users
	 * might have the same Gridlet ID in the lists.
	 * 
	 * @param gridletId
	 *            a Gridlet ID
	 * @param userId
	 *            the user or owner's ID of this Gridlet
	 * @param ack
	 *            an acknowledgement, i.e. <tt>true</tt> if wanted to know
	 *            whether this operation is success or not, <tt>false</tt>
	 *            otherwise (don't care)
	 * @pre gridletId > 0
	 * @pre userId > 0
	 * @post $none
	 */
	public synchronized void gridletPause(int gridletId, int userId, boolean ack) {
		boolean status = false;

		// Find in EXEC List first
		int found = gridletInExecList_.indexOf(gridletId, userId);
		if (found >= 0) {
			// updates all the Gridlets first before pausing
			updateGridletProcessing();

			// Removes the Gridlet from the execution list
			ResJob rgl = (ResJob) gridletInExecList_.remove(found);

			// if a Gridlet is finished upon cancelling, then set it to success
			// instead.
			if (rgl.getRemainingGridletLength() == 0.0) {
				found = -1; // meaning not found in Queue List
				gridletFinish(rgl, Gridlet.SUCCESS);
				System.out.println(super.resName_
						+ ".SpaceShared.gridletPause(): Cannot pause"
						+ " Gridlet #" + gridletId + " for User #" + userId
						+ " since it has FINISHED.");
			} else {
				status = true;
				rgl.setGridletStatus(Gridlet.PAUSED); // change the status
				gridletPauseList_.add(rgl); // add into the paused list

				// Set the PE on which Gridlet finished to FREE
				for(int i=0;i<rgl.getListMachineID().length;i++){
					changeStatusPE(PE.FREE, rgl.getListMachineID()[i],rgl.getListPEID()[i]);
				}

				// empty slot is available, hence process a new Gridlet
				allocateQueueGridlet();
			}
		} else { // Find in QUEUE list
			found = gridletQueueList_.indexOf(gridletId, userId);
		}

		// if found in the Queue List
		if (status == false && found >= 0) {
			status = true;

			// removes the Gridlet from the Queue list
			ResJob rgl = (ResJob) gridletQueueList_.remove(found);
			rgl.setGridletStatus(Gridlet.PAUSED); // change the status
			gridletPauseList_.add(rgl); // add into the paused list
		}
		// if not found anywhere in both exec and paused lists
		else if (found == -1) {
			System.out.println(super.resName_
					+ ".SpaceShared.gridletPause(): Error - cannot "
					+ "find Gridlet #" + gridletId + " for User #" + userId);
		}

		// sends back an ack if required
		if (ack) {
			super.sendAck(GridSimTags.GRIDLET_PAUSE_ACK, status, gridletId,
					userId);
		}
	}

	/**
	 * Moves a Gridlet from this GridResource entity to a different one. This
	 * method will search in both the execution and paused list. The User ID is
	 * important as many Users might have the same Gridlet ID in the lists.
	 * <p>
	 * If a Gridlet has finished beforehand, then this method will send back the
	 * Gridlet to sender, i.e. the <tt>userId</tt> and sets the acknowledgment
	 * to false (if required).
	 * 
	 * @param gridletId
	 *            a Gridlet ID
	 * @param userId
	 *            the user or owner's ID of this Gridlet
	 * @param destId
	 *            a new destination GridResource ID for this Gridlet
	 * @param ack
	 *            an acknowledgement, i.e. <tt>true</tt> if wanted to know
	 *            whether this operation is success or not, <tt>false</tt>
	 *            otherwise (don't care)
	 * @pre gridletId > 0
	 * @pre userId > 0
	 * @pre destId > 0
	 * @post $none
	 */
	public synchronized void gridletMove(int gridletId, int userId, int destId,
			boolean ack) {
		// cancels the Gridlet
		ResJob rgl = cancel(gridletId, userId);

		// if the Gridlet is not found
		if (rgl == null) {
			System.out.println(super.resName_
					+ ".SpaceShared.gridletMove(): Cannot find " + "Gridlet #"
					+ gridletId + " for User #" + userId);

			if (ack) // sends back an ack if required
			{
				super.sendAck(GridSimTags.GRIDLET_SUBMIT_ACK, false, gridletId,
						userId);
			}

			return;
		}

		// if the Gridlet has finished beforehand
		if (rgl.getGridletStatus() == Gridlet.SUCCESS) {
			System.out.println(super.resName_
					+ ".SpaceShared.gridletMove(): Cannot move Gridlet #"
					+ gridletId + " for User #" + userId
					+ " since it has FINISHED.");

			if (ack) // sends back an ack if required
			{
				super.sendAck(GridSimTags.GRIDLET_SUBMIT_ACK, false, gridletId,
						userId);
			}

			gridletFinish(rgl, Gridlet.SUCCESS);
		} else // otherwise moves this Gridlet to a different GridResource
		{
			rgl.finalizeGridlet();

			// Set PE on which Gridlet finished to FREE
			for(int i=0;i<rgl.getListMachineID().length;i++){
				changeStatusPE(PE.FREE, rgl.getListMachineID()[i],rgl.getListPEID()[i]);
			}

			super.gridletMigrate(rgl.getGridlet(), destId, ack);
			allocateQueueGridlet();
		}
	}

	/**
	 * Resumes a Gridlet only in the paused list. The User ID is important as
	 * many Users might have the same Gridlet ID in the lists.
	 * 
	 * @param gridletId
	 *            a Gridlet ID
	 * @param userId
	 *            the user or owner's ID of this Gridlet
	 * @param ack
	 *            an acknowledgement, i.e. <tt>true</tt> if wanted to know
	 *            whether this operation is success or not, <tt>false</tt>
	 *            otherwise (don't care)
	 * @pre gridletId > 0
	 * @pre userId > 0
	 * @post $none
	 */
	public synchronized void gridletResume(int gridletId, int userId,
			boolean ack) {
		boolean status = false;

		// finds the Gridlet in the execution list first
		int found = gridletPauseList_.indexOf(gridletId, userId);
		if (found >= 0) {
			// removes the Gridlet
			ResJob rgl = (ResJob) gridletPauseList_.remove(found);
			rgl.setGridletStatus(Gridlet.RESUMED);

			// update the Gridlets up to this point in time
			updateGridletProcessing();
			status = true;

			// if there is an available PE slot, then allocate immediately
			boolean success = false;
			if (rgl.getNumPE() < super.resource_.getNumFreePE()) {
				success = allocatePEtoGridlet(rgl);
			}

			// otherwise put into Queue list
			if (!success) {
				rgl.setGridletStatus(Gridlet.QUEUED);
				gridletQueueList_.add(rgl);
			}

			System.out.println(super.resName_ + "TimeShared.gridletResume():"
					+ " Gridlet #" + gridletId + " with User ID #" + userId
					+ " has been sucessfully RESUMED.");
		} else {
			System.out.println(super.resName_
					+ "TimeShared.gridletResume(): Cannot find " + "Gridlet #"
					+ gridletId + " for User #" + userId);
		}

		// sends back an ack if required
		if (ack) {
			super.sendAck(GridSimTags.GRIDLET_RESUME_ACK, status, gridletId,
					userId);
		}
	}

	// /////////////////////////// PRIVATE METHODS /////////////////////
	abstract protected void orderWaitingQueue();
	/**
	 * Allocates the first Gridlet in the Queue list (if any) to execution list
	 * 
	 * @pre $none
	 * @post $none
	 */
	protected void allocateQueueGridlet() {
		// if there are Gridlets in the QUEUE, then order the list depending on the policy 
		if (gridletQueueList_.size() > 0){
			orderWaitingQueue();
			
			ResGridlet rg =  gridletQueueList_.get(0);
			//try to allocate the Gridlet into the system and remove from the queue list
			boolean success = allocatePEtoGridlet(rg);
			
			if (success) {
				gridletQueueList_.remove(rg);
				allocateQueueGridlet();
				
			}
			return;
		}
	}
	

	/**
	 * Updates the execution of all Gridlets for a period of time. The time
	 * period is determined from the last update time up to the current time.
	 * Once this operation is successfull, then the last update time refers to
	 * the current time.
	 * 
	 * @pre $none
	 * @post $none
	 */
	private synchronized void updateGridletProcessing() {
		// Identify MI share for the duration (from last event time)
		double time = GridSim.clock();
		double timeSpan = time - lastUpdateTime_;

		// if current time is the same or less than the last update time,
		// then ignore
		if (timeSpan <= 0.0) {
			return;
		}

		// Update Current Time as Last Update
		lastUpdateTime_ = time;

		// update the GridResource load
		for(ResGridlet rg:this.gridletInExecList_){
			ResJob rj=(ResJob)rg;
			double startTime=rj.getExecStartTime();
			double exectime=time-startTime;
			int roundUp=(int)exectime+1;
			rj.updateGridletFinishedSoFar(roundUp);
		}
	}

	abstract protected boolean howToAllocatePEtoGridlet(ResGridlet rgl);
	/**
	 * Allocates a Gridlet into a free PE and sets the Gridlet status into
	 * INEXEC and PE status into busy afterwards
	 * 
	 * @param rgl
	 *            a ResGridlet object
	 * @return <tt>true</tt> if there is an empty PE to process this Gridlet,
	 *         <tt>false</tt> otherwise
	 * @pre rgl != null
	 * @post $none
	 */
	protected boolean allocatePEtoGridlet(ResGridlet rgl){
		if (this.resource_.getNumFreePE() < rgl.getNumPE()) {
			return false;
		}
		// IDENTIFY MACHINE which has a free PE and add this Gridlet to it.
		// gets the list of PEs and find one empty PE
		if(!howToAllocatePEtoGridlet(rgl)){
			return false;
		}
		rgl.setGridletStatus(Gridlet.INEXEC); // change Gridlet status
			
		// add this Gridlet into execution list
		gridletInExecList_.add(rgl);
		// Set allocated PE to BUSY status
		// Identify Completion Time and Set Interrupt
		ResJob rj=(ResJob) rgl;
		double time = forecastFinishTime(rj);
		int roundUpTime = (int) (time + 1); // rounding up
		rj.setFinishTime(roundUpTime);
		//System.out.println("GRIDLED LENGTH: "+rj.getGridletLength());
		rj.getGridletLength();
		// then send this into itself
		super.sendInternalEvent(roundUpTime);
		return true;
	}
	/**
	 * Forecast finish time of a Gridlet.
	 * @return Gridlet's finish time.
	 * @pre availableRating >= 0.0
	 * @pre length >= 0.0
	 * @post $none
	 */
	protected double forecastFinishTime(ResJob rj) {
		//Get the slowest Machine rating
		int slowRating=Integer.MAX_VALUE;
		if(rj.getNumPE()==1){
			slowRating=super.resource_.getMIPSRatingOfOnePE(rj.getMachineID(), rj.getPEID());
		}else {
			for(int i=0;i<rj.getListMachineID().length;i++){
				slowRating=Math.min(slowRating,super.resource_.getMIPSRatingOfOnePE(rj.getListMachineID()[i], 0));
			}
		}
		// Calculate the Slowdown of processing (Basic rating is 1000)
		double SP=(double)1000/(double)slowRating;
		
		
		double finishTime=(double)rj.getGridletLength()*(double)SP;
		
		// This is as a safeguard since the finish time can be extremely
		// small close to 0.0, such as 4.5474735088646414E-14. Hence causing
		// some Gridlets never to be finished and consequently hang the program
		if (finishTime < 1.0) {
			finishTime = 1.0;
		}
		return finishTime;
	}

	/**
	 * Checks all Gridlets in the execution list whether they are finished or
	 * not.
	 * 
	 * @pre $none
	 * @post $none
	 */
	protected synchronized void checkGridletCompletion() {
		ResJob rj = null;
		int i = 0;

		// NOTE: This one should stay as it is since gridletFinish()
		// will modify the content of this list if a Gridlet has finished.
		// Can't use iterator since it will cause an exception
		while (i < gridletInExecList_.size()) {
			rj = (ResJob) gridletInExecList_.get(i);
			if (rj.getRemainingGridletLength() == 0.0) {
				gridletInExecList_.remove(rj);
				gridletFinish(rj, Gridlet.SUCCESS);
				continue;
			}

			i++;
		}

		// if there are still Gridlets left in the execution
		// then send this into itself for an hourly interrupt
		// NOTE: Setting the internal event time too low will make the
		// simulation more realistic, BUT will take longer time to
		// run this simulation. Also, size of sim_trace will be HUGE!
		if (gridletInExecList_.size() > 0) {
			super.sendInternalEvent(60.0 * 60.0);
		}
	}

	/**
	 * Updates the Gridlet's properties, such as status once a Gridlet is
	 * considered finished.
	 * 
	 * @param rgl
	 *            a ResGridlet object
	 * @param status
	 *            the Gridlet status
	 * @pre rgl != null
	 * @pre status >= 0
	 * @post $none
	 */
	protected void changeStatusPE(boolean status,int machineID,int PEID){
		Machine m = resource_.getMachineList().getMachine(machineID);
		m.setStatusPE(status, PEID);
	}
	
	protected void gridletFinish(ResJob rj, int status) {
		// Set PE on which Gridlet finished to FREE
		if(rj.getNumPE()==1){
			changeStatusPE(PE.FREE,rj.getMachineID(), rj.getPEID());
		}else{
			for(int i=0;i<rj.getListMachineID().length;i++){
				changeStatusPE(PE.FREE, rj.getListMachineID()[i], rj.getListPEID()[i]);
			}
		}
		// the order is important! Set the status first then finalize
		// due to timing issues in ResGridlet class
		rj.setGridletStatus(status);
		rj.finalizeGridlet();
		super.sendFinishGridlet(rj.getGridlet());
		allocateQueueGridlet(); // move Queued Gridlet into exec list
	}

	/**
	 * Handles an operation of canceling a Gridlet in either execution list or
	 * paused list.
	 * 
	 * @param gridletId
	 *            a Gridlet ID
	 * @param userId
	 *            the user or owner's ID of this Gridlet
	 * @return an ResGridlet object <tt>null</tt> if this Gridlet is not found
	 * @pre gridletId > 0
	 * @pre userId > 0
	 * @post $none
	 */
	private ResJob cancel(int gridletId, int userId) {
		ResJob rgl = null;

		// Find in EXEC List first
		int found = gridletInExecList_.indexOf(gridletId, userId);
		if (found >= 0) {
			// update the gridlets in execution list up to this point in time
			updateGridletProcessing();

			// Get the Gridlet from the execution list
			rgl = (ResJob) gridletInExecList_.remove(found);

			// if a Gridlet is finished upon cancelling, then set it to success
			// instead.
			if (rgl.getRemainingGridletLength() == 0.0) {
				rgl.setGridletStatus(Gridlet.SUCCESS);
			} else {
				rgl.setGridletStatus(Gridlet.CANCELED);
			}

			// Set PE on which Gridlet finished to FREE
			for(int i=0;i<rgl.getListMachineID().length;i++){
				changeStatusPE(PE.FREE, rgl.getListMachineID()[i], rgl.getListPEID()[i]);
			}
			allocateQueueGridlet();
			return rgl;
		}

		// Find in QUEUE list
		found = gridletQueueList_.indexOf(gridletId, userId);
		if (found >= 0) {
			rgl = (ResJob) gridletQueueList_.remove(found);
			rgl.setGridletStatus(Gridlet.CANCELED);
		}

		// if not, then find in the Paused list
		else {
			found = gridletPauseList_.indexOf(gridletId, userId);

			// if found in Paused list
			if (found >= 0) {
				rgl = (ResJob) gridletPauseList_.remove(found);
				rgl.setGridletStatus(Gridlet.CANCELED);
			}

		}
		return rgl;
	}
}
