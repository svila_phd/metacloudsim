package simulator;

import gridsim.GridSim;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Vector;

public class SimulatorLog {
	private Vector<String> messages;
	private String filename = "";
	
	private long lastWrite=0;
	private Calendar now;
	
	private String writeMode = "each";
		// each: write to disk each message
		// intelligent: write every 10 seconds, or 100 messages.
	private int NUM_MESSAGES_TRIGGER = 100;
	
	private BufferedWriter outFile;
	private PrintWriter out;
	
	public SimulatorLog(){
		messages = new Vector<String>();
		now = Calendar.getInstance();
	}
	
	public void setFilename(String filename_) throws IOException{
		/*
		File file = new File(filename_);
		if (!file.isFile() && !file.isDirectory()){
			System.out.println("SimulatorLog: error checking file: " + this.filename );
			System.out.println("Exit.");
			return false;
		}else{
			this.filename = filename_;
			return true;
		}
		*/
		this.filename = filename_;
		// If the file already exists, remove it.
 		File file = new File(filename);
 		if (file.exists()){
 			file.delete();
 		}
		outFile = new BufferedWriter(new FileWriter(filename));
		out = new PrintWriter(outFile);
		
	}
	
	
	public void addMessageWithNoTime(String message){
		String str =  message;
		messages.add(str);
		check();
	}
	
	public void addMessage(String message){
		DecimalFormat df = new DecimalFormat("#.##");
		String str = "* " + df.format(GridSim.clock()-5) + ": " + message;
		messages.add(str);
		check();
	}
	
	public void addMessage(double time, String message){
		String str = "* " + time + ": " + message;
		messages.add(str);
		check();
	}
	
	private void check(){
		if (this.writeMode.equals("intelligent")){
			long current = now.getTimeInMillis();
			if ((this.lastWrite < (current-10000)) || (this.messages.size() > NUM_MESSAGES_TRIGGER) ){
				write();
			}
		}else if(this.writeMode.equals("each")){
			write();
		}
	}
	
	private void write(){
		for (int i=0; i <messages.size(); i++){
			//out.println(messages.elementAt(i));
		}
		messages.clear();
		try {
			outFile.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.lastWrite = now.getTimeInMillis();
	}
	
	public void close(){
		// write the remaining messages
		this.write();
		out.close();
	}
	
	public void writeLogFile() throws IOException{
		
		for (int i=0; i <messages.size(); i++){
			out.println(messages.elementAt(i));
		}
		outFile.flush();
	}
}
