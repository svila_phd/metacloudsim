package simulator;

import simulator.Job;
import gridsim.Gridlet;

public class Job extends Gridlet{

	private double ppbw;
	private double sigma;
	private double originalRuntime;
	private int[][] allocation;
	
	// Create an empty job
	public Job(int gridletID){
		super(gridletID, 1, 1, 1, false);
		this.ppbw=0;
		this.sigma=0.8;
		this.originalRuntime=1;
	}
	
	
	public Job(int gridletID, double originalRuntime,int numProc, double sigma,double PPBW) {
		super(gridletID, 1, 1, 1, false);
		this.ppbw = PPBW;
		this.sigma=sigma;
		this.originalRuntime=originalRuntime;
		super.setNumPE(numProc);
		allocation=new int[numProc][2];
	}
	
	public Job(Job j){
		this(j.getGridletID(),1,j.getNumPE(),j.ppbw,j.sigma);
		this.originalRuntime=j.originalRuntime;
	}
	
	public void allocateJobToPE(int[] machineID, int[] PEID){
		for(int task=0;task<this.getNumPE();task++){
			allocation[task][0]=machineID[task];
			allocation[task][1]=PEID[task];
		}
	}
	
	public int[][] getAllocation(){
		return this.allocation;
	}
	public double getPPBW(){
		return this.ppbw;
	}
	
	public void setPPBW(double PPBW){
		this.ppbw = PPBW;
	}	
	
	
		
	public double getSigma(){
		return this.sigma;
	}
	
	public void setSigma(double sgm){
		this.sigma = sgm;
	}	
	public double getOriginalRuntime(){
		return this.originalRuntime;
	}
	public void setOriginalRuntime(double or){
		this.originalRuntime=or;
	}
}
