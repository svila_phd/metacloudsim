package simulator;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import eduni.simjava.Sim_event;
import eduni.simjava.Sim_system;
import simulator.Job;
import svila.staticAllocation.schedulingTechniques.GridSimManager;
import gridsim.GridSim;
import gridsim.GridSimTags;
import gridsim.Gridlet;
import gridsim.IO_data;
import gridsim.ParameterException;
import gridsim.net.InfoPacket;
import gridsim.net.Link;
import gridsim.net.SimpleLink;

public class CommWorkload extends GridSim {

	private String fileName_; // file name
	private String resName_; // resource name
	private int resID_; // resource ID
	private int numGridlets; // the number of gridlets
	public ArrayList<Gridlet> list_; // a list for getting all the Gridlets

	// constant
	private int JOB_NUM; // job number
	private int SUBMIT_TIME; // submit time of a Gridlet
	private int RUN_TIME; // running time of a Gridlet
	private int NUM_PROC; // number of processors needed for a Gridlet
	private int SIGMA; // required number of processors
	private int PPBW; // required running time
	private int MAX_FIELD; // max number of field in the trace file
	private String COMMENT; // a string that denotes the start of a comment
	private static final int IRRELEVANT = -1; // irrelevant number
	private static final int INTERVAL = 10; // number of intervals
	private String[] fieldArray_; // a temp array storing all the fields

	public CommWorkload(String name, String fileName, String resourceName) throws ParameterException, Exception {
		super(name, GridSimTags.DEFAULT_BAUD_RATE);

		// check the input parameters first
		String msg = name + "(): Error - ";
		if (fileName == null || fileName.length() == 0) {
			throw new ParameterException(msg + "invalid trace file name.");
		} else if (resourceName == null || resourceName.length() == 0) {
			throw new ParameterException(msg + "invalid resource name.");
		}

		System.out.println(name + ": Creating a workload object ...");
		init(fileName, resourceName);
	}

	/**
	 * Create a new Workload object <b>with</b> the network extension. This
	 * means this entity directly sends Gridlets to a destinated resource
	 * through a link. The link is automatically created by this constructor.
	 * 
	 * @param name
	 *            this entity name
	 * @param baudRate
	 *            baud rate of this link (bits/s)
	 * @param propDelay
	 *            Propagation delay of the Link in milli seconds
	 * @param MTU
	 *            Maximum Transmission Unit of the Link in bytes. Packets which
	 *            are larger than the MTU should be split up into MTU size
	 *            units. For example, a 1024 byte packet trying to cross a 576
	 *            byte MTU link should get split into 2 packets of 576 bytes and
	 *            448 bytes.
	 * @param fileName
	 *            the workload trace filename in one of the following format:
	 *            <i>ASCII text, zip, gz.</i>
	 * @param resourceName
	 *            the resource name
	 * @param rating
	 *            the resource's PE rating
	 * @throws Exception
	 *             This happens when creating this entity before initializing
	 *             GridSim package or this entity name is <tt>null</tt> or empty
	 * @throws ParameterException
	 *             This happens for the following conditions:
	 *             <ul>
	 *             <li>the entity name is null or empty
	 *             <li>baudRate <= 0
	 *             <li>propDelay <= 0
	 *             <li>MTU <= 0
	 *             <li>the workload trace file name is null or empty
	 *             <li>the resource entity name is null or empty
	 *             <li>the resource PE rating <= 0
	 *             </ul>
	 * @pre name != null
	 * @pre baudRate > 0
	 * @pre propDelay > 0
	 * @pre MTU > 0
	 * @pre fileName != null
	 * @pre resourceName != null
	 * @pre rating > 0
	 * @post $none
	 */
	
	public List<geneticsimulator.Job> jobs;
	
	public CommWorkload(String name, double baudRate, double propDelay, int MTU,
			String fileName, String resourceName, List<geneticsimulator.Job> jobs)
			throws ParameterException, Exception {
		super(name, new SimpleLink(name + "_link", baudRate, propDelay, MTU));

		// check the input parameters first
		String msg = name + "(): Error - ";
		if (fileName == null || fileName.length() == 0) {
			throw new ParameterException(msg + "invalid trace file name.");
		} else if (resourceName == null || resourceName.length() == 0) {
			throw new ParameterException(msg + "invalid resource name.");
		}

		System.out.println(name + ": Creating a workload object ...");
		this.jobs = jobs;
		init(fileName, resourceName);
	}

	/**
	 * Create a new Workload object <b>with</b> the network extension. This
	 * means this entity directly sends Gridlets to a destinated resource
	 * through a link. The link is automatically created by this constructor.
	 * 
	 * @param name
	 *            this entity name
	 * @param link
	 *            the link that will be used to connect this Workload to another
	 *            entity or a Router.
	 * @param fileName
	 *            the workload trace filename in one of the following format:
	 *            <i>ASCII text, zip, gz.</i>
	 * @param resourceName
	 *            the resource name
	 * @param rating
	 *            the resource's PE rating
	 * @throws Exception
	 *             This happens when creating this entity before initializing
	 *             GridSim package or this entity name is <tt>null</tt> or empty
	 * @throws ParameterException
	 *             This happens for the following conditions:
	 *             <ul>
	 *             <li>the entity name is null or empty
	 *             <li>the link is empty
	 *             <li>the workload trace file name is null or empty
	 *             <li>the resource entity name is null or empty
	 *             <li>the resource PE rating <= 0
	 *             </ul>
	 * @pre name != null
	 * @pre link != null
	 * @pre fileName != null
	 * @pre resourceName != null
	 * @pre rating > 0
	 * @post $none
	 */
	public CommWorkload(String name, Link link, String fileName,
			String resourceName) throws ParameterException,
			Exception {
		super(name, link);

		// check the input parameters first
		String msg = name + "(): Error - ";
		if (fileName == null || fileName.length() == 0) {
			throw new ParameterException(msg + "invalid trace file name.");
		} else if (resourceName == null || resourceName.length() == 0) {
			throw new ParameterException(msg + "invalid resource name.");
		}

		System.out.println(name + ": Creating a workload object ...");
		init(fileName, resourceName);
	}

	/**
	 * Initialises all the attributes
	 * 
	 * @param fileName
	 *            trace file name
	 * @param resourceName
	 *            resource entity name
	 * @param rating
	 *            resource PE rating
	 * @pre $none
	 * @post $none
	 */
	private void init(String fileName, String resourceName) {
		fileName_ = fileName;
		resName_ = resourceName;
		resID_ = GridSim.getEntityId(resName_);
		numGridlets = 0; // starts at 1 to make it the same as in a trace file
		list_ = new ArrayList<Gridlet>();

		// if using Standard Workload Format -- don't forget to substract by 1
		// since an array starts at 0, but the field in a trace starts at 1
		JOB_NUM = 0;
		SUBMIT_TIME = 1;
		RUN_TIME = 3;
		NUM_PROC = 4;
		SIGMA = 18;
		PPBW = 19;

		COMMENT = ";"; // semicolon means the start of a comment
		MAX_FIELD = 20; // standard workload format has 18 fields
		fieldArray_ = null;
	}

	/**
	 * Identifies the start of a comment line. Hence, a line that starts with a
	 * given comment will be ignored.
	 * 
	 * @param comment
	 *            a character that denotes the start of a comment, e.g. ";" or
	 *            "#"
	 * @return <tt>true</tt> if it is successful, <tt>false</tt> otherwise
	 * @pre comment != null
	 * @post $none
	 */
	public boolean setComment(String comment) {
		boolean success = false;
		if (comment != null && comment.length() > 0) {
			COMMENT = comment;
			success = true;
		}
		return success;
	}

	/**
	 * Tells this class what to look in the trace file. This method should be
	 * called before the start of the simulation.
	 * <p>
	 * By default, this class follows the standard workload format as specified
	 * in <a href="http://www.cs.huji.ac.il/labs/parallel/workload/">
	 * http://www.cs.huji.ac.il/labs/parallel/workload/</a> <br>
	 * However, you can use other format by calling this method.
	 * <p>
	 * The parameters must be a positive integer number starting from 1. A
	 * special case is where <tt>jobNum == -1</tt>, meaning the job or gridlet
	 * ID starts at 1.
	 * 
	 * @param maxField
	 *            max. number of field/column in one row
	 * @param jobNum
	 *            field/column number for locating the job ID
	 * @param submitTime
	 *            field/column number for locating the job submit time
	 * @param runTime
	 *            field/column number for locating the job run time
	 * @param numProc
	 *            field/column number for locating the number of PEs required to
	 *            run a job
	 * @return <tt>true</tt> if successful, <tt>false</tt> otherwise
	 * @pre maxField > 0
	 * @pre submitTime > 0
	 * @pre runTime > 0
	 * @pre numProc > 0
	 * @post $none
	 */
	public boolean setField(int maxField, int jobNum, int submitTime,
			int runTime, int numProc,int sigma,int ppbw) {
		// need to substract by 1 since array starts at 0. Need to convert,
		// position in a field into the index of the array
		if (jobNum > 0) {
			JOB_NUM = jobNum - 1;
		} else if (jobNum == 0) {
			System.out.println(super.get_name()
					+ ".setField(): Invalid job number field.");
			return false;
		} else {
			JOB_NUM = -1;
		}

		// get the max. number of field
		if (maxField > 0) {
			MAX_FIELD = maxField;
		} else {
			System.out.println(super.get_name()
					+ ".setField(): Invalid max. number of field.");
			return false;
		}

		// get the submit time field
		if (submitTime > 0) {
			SUBMIT_TIME = submitTime - 1;
		} else {
			System.out.println(super.get_name()
					+ ".setField(): Invalid submit time field.");
			return false;
		}

		// get the run time field
		if (runTime > 0) {
			RUN_TIME = runTime - 1;
		} else {
			System.out.println(super.get_name()
					+ ".setField(): Invalid run time field.");
			return false;
		}

		// get the number of processors field
		if (numProc > 0) {
			NUM_PROC = numProc - 1;
		} else {
			System.out.println(super.get_name()
					+ ".setField(): Invalid number of processors field.");
			return false;
		}
		// get the sigma field
		if (sigma > 0) {
			SIGMA = sigma - 1;
		} else {
			System.out.println(super.get_name()
					+ ".setField(): Invalid sigma field.");
			return false;
		}
		// get the ppbw field
		if (ppbw > 0) {
			PPBW = ppbw - 1;
		} else {
			System.out.println(super.get_name()
					+ ".setField(): Invalid ppbw field.");
			return false;
		}

		return true;
	}

	/**
	 * Gets a list of completed Gridlets
	 * 
	 * @return a list of Gridlets
	 * @pre $none
	 * @post $none
	 */
	public ArrayList<Gridlet> getGridletList() {
		return list_;
	}

	//Obtain the different metrics
	/** Obtains the makespan of the execution of this workload
	 * 
	 * @return the makespan
	 */
	public double getMakespan(){
		double makespan=0.0;
		Iterator<Gridlet> iter = this.list_.iterator();
		while (iter.hasNext()){
			Job job = (Job)iter.next();
			if(job.getFinishTime()>makespan){
				makespan=job.getFinishTime();
			}
		}
		return makespan;
	}
	
	/** Obtain the average Execution time of this workload
	 * 
	 * @return the average Execution time of this workload
	 */
	public double getAverageExecTime(){
		double totalExec = 0.0;
		Iterator<Gridlet> iter = this.list_.iterator();
		while (iter.hasNext()){
			Job job = (Job)iter.next();
			totalExec = totalExec + (job.getFinishTime() - job.getExecStartTime());
		}
		return totalExec/list_.size();
		
	}
	/**Obtain the average Waiting time of this workload
	 * 
	 * @return the average waiting time
	 */
	public double getAverageWaitTime(){
		double totalWait = 0.0;
		//Iterator<Entry<Integer, Job>> iter = this.jobs.entrySet().iterator();
		Iterator<Gridlet> iter = this.list_.iterator();
		while (iter.hasNext()){
			Job job = (Job)iter.next();
			totalWait = totalWait + (job.getExecStartTime() - job.getSubmissionTime());
		}
		return  totalWait/list_.size();
	}
	
	/** Obtains the average Runtime of this workload
	 * 
	 * @return the average Runtime
	 */
	public double getAverageRuntime(){
		double totalRuntime = 0.0;
		Iterator<Gridlet> iter = this.list_.iterator();
		while (iter.hasNext()){
			Job job = (Job)iter.next();
			totalRuntime = totalRuntime + (job.getFinishTime() - job.getExecStartTime());
		}
		return totalRuntime/list_.size();
	}
	
	public double getAverageSlowdown(){
		double totalSlowdown=0.0;
		Iterator<Gridlet> iter = this.list_.iterator();
		while (iter.hasNext()){
			Job job = (Job)iter.next();
			totalSlowdown +=((job.getFinishTime() - job.getExecStartTime())*100/job.getOriginalRuntime())-100;
		}
		
		totalSlowdown/=this.list_.size();
		return totalSlowdown;
	}
	
	/** Orders the jobs for the starting time
	 * 
	 * @param list_ The list of jobs to order
	 * @return ArrayList of gridlets, ordered by their starting time
	 */
	public ArrayList<Gridlet> orderByStartTime(ArrayList<Gridlet> list_){
		ArrayList<Gridlet> orderedList=new ArrayList<Gridlet>();
		ArrayList<Gridlet> list=new ArrayList<Gridlet>();
		list.addAll(list_);
		while(!list.isEmpty()){
			double time=Double.MAX_VALUE;
			int first=-1;
			for(int i=0;i<list.size();i++){
				Gridlet g=list.get(i);
				if(g.getExecStartTime()<time){
					time=g.getExecStartTime();
					first=i;
				}
			}
			orderedList.add(list.remove(first));
		}
		return orderedList;
	}
	
	
	
	
	public String toString(){
		String ret=new String("");
		ArrayList<Gridlet>orderedQueue=orderByStartTime(this.list_);
		for(Gridlet gl:orderedQueue){
			ret+=gl.getGridletID()+",";
		}
		return ret;
	}
	/** Prints the list of jobs in this workload
	 * 
	 * @param history a boolean for more information
	 */
	public void printGridletList(boolean history) {
		String name = super.get_name();
		int size = list_.size();
		Job gridlet = null;
		String indent = "\t";
		System.out.println();
		System.out.println("================================ OUTPUT for " + name + " ========================================");
		System.out.println("Gridlet_ID" + indent + "SubmitTime" + indent +"StartTime"+ indent+ "FinishTime"+ indent+"ExecTime");
		int i = 0;
		for (i = 0; i < size; i++) {
			gridlet = (Job)list_.get(i);
			System.out.print(gridlet.getGridletID() + indent);
			System.out.print(gridlet.getSubmissionTime()+indent);
			System.out.print(gridlet.getExecStartTime()+indent);
			System.out.print(gridlet.getFinishTime()+indent);
			System.out.println(gridlet.getOriginalRuntime());
		}

		System.out.println();
		if (history) {
			// a loop to print each Gridlet's history
			System.out.println();
			for (i = 0; i < size; i++) {
				gridlet = (Job)list_.get(i);
				System.out.println(gridlet.getGridletHistory());

				System.out.print("Gridlet #" + gridlet.getGridletID());
				System.out.println(", length = " + gridlet.getOriginalRuntime()
						+ ", finished so far = "
						+ gridlet.getGridletFinishedSoFar());
				System.out.println("=========================================");
				System.out.println();
			}
		}
	}

	/**
	 * Reads from a given file when the simulation starts running. Then submits
	 * Gridlets to a resource and collects them before exiting. To collect the
	 * completed Gridlets, use {@link #getGridletList()}
	 * 
	 * @pre $none
	 * @post $none
	 */
	public void body() {
		System.out.println();
		System.out.println(super.get_name() + ".body() :%%%% Start ...");

		// create a temp array
		fieldArray_ = new String[MAX_FIELD];

		// get the resource id
		if (resID_ < 0) {
			System.out.println(super.get_name()
					+ ".body(): Error - invalid resource name: " + resName_);
			return;
		}

		boolean success = true;

		submitGridlet(0, 500, GridSimManager.numProcs, 1,1.0,1.0);
		for(geneticsimulator.Job j : jobs) {
			submitGridlet(j.getId()+1, 0, j.getTimeBase(), j.getRequirements(), 1.0, 1.0);
		}
		
		/*// read the gz file
		if (fileName_.endsWith(".gz")) {
			success = readGZIPFile(fileName_);
		}
		// read the zip file
		else if (fileName_.endsWith(".zip")) {
			success = readZipFile(fileName_);
		}
		// read from uncompressed file as well
		else {
			success = readCustomFile(fileName_);
		}*/
		// if all the gridlets have been submitted
		if (success == true) {
			collectGridlet();
		} else {
			System.out.println(super.get_name()
					+ ".body(): Error - unable to parse from a file.");
		}

		// shut down all the entities, including GridStatistics entity since
		// we used it to record certain events.
		shutdownGridStatisticsEntity();
		shutdownUserEntity();
		terminateIOEntities();

		System.out.println(super.get_name() + ".body() : %%%% Exit ...");
	}

	// ////////////////////// PRIVATE METHODS ///////////////////////

	/**
	 * Collects Gridlets sent and stores them into a list.
	 * 
	 * @pre $none
	 * @post $none
	 */
	private void collectGridlet() {
		System.out.println(super.get_name() + ": Collecting Gridlets ...");
		list_ = new ArrayList<Gridlet>();

		Object data = null;
		Gridlet gl = null;
		int counter=0;
		Sim_event ev = new Sim_event();
		while (Sim_system.running()) {
			super.sim_get_next(ev); // get the next available event
			data = ev.get_data(); // get the event's data

			// handle ping request
			if (ev.get_tag() == GridSimTags.INFOPKT_SUBMIT) {
				processPingRequest(ev);
				continue;
			}

			// get the Gridlet data
			if (data != null && data instanceof Gridlet) {
				gl = (Gridlet) data;
				//System.out.println("GRIDLET COLLECTED: "+gl.getGridletID());
				list_.add(gl);
				counter++;
			}

			// if all the Gridlets have been collected
			if (counter == numGridlets) {
				break;
			}
		}
	}

	/**
	 * Processes a ping request.
	 * 
	 * @param ev
	 *            a Sim_event object
	 * @pre ev != null
	 * @post $none
	 */
	private void processPingRequest(Sim_event ev) {
		InfoPacket pkt = (InfoPacket) ev.get_data();
		pkt.setTag(GridSimTags.INFOPKT_RETURN);
		pkt.setDestID(pkt.getSrcID());

		// sends back to the sender
		super.send(super.output, GridSimTags.SCHEDULE_NOW,
				GridSimTags.INFOPKT_RETURN,
				new IO_data(pkt, pkt.getSize(), pkt.getSrcID()));
	}

	/**
	 * Breaks a line of string into many fields.
	 * 
	 * @param line
	 *            a line of string
	 * @param lineNum
	 *            a line number
	 * @pre line != null
	 * @pre lineNum > 0
	 * @post $none
	 */
	private void parseValue(String line) {
		// skip a comment line
		if (line.startsWith(COMMENT)) {
			return;
		}

		String[] sp = line.split("\\s+"); // split the fields based on a space
		int index = 0; // the index of an array

		// check for each field in the array
		for (int i = 0; i < sp.length; i++) {
			// if it is empty then ignore
			if (sp[i].length() == 0) {
				continue;
			}
			// if not, then put into the array
			else {
				fieldArray_[index] = sp[i];
				index++;
			}
		}

		if (index == MAX_FIELD) {
			extractField(fieldArray_);
		}
	}

	/**
	 * Extracts relevant information from a given array
	 * 
	 * @param array
	 *            an array of String
	 * @param line
	 *            a line number
	 * @pre array != null
	 * @pre line > 0
	 */
	private void extractField(String[] array) {
		try {

			// get the job number
			int id = 0;
			if (JOB_NUM == IRRELEVANT) {
				id = numGridlets;
			} else {
				id = new Integer(array[JOB_NUM].trim()).intValue();
			}

			// get the submit time
			long submitTime = new Long(array[SUBMIT_TIME].trim()).intValue();

			// get the run time
			 double runTime= new Double(array[RUN_TIME].trim()).doubleValue();


			// according to the SWF manual, runtime of 0 is possible due
			// to rounding down. E.g. runtime is 0.4 seconds -> runtime = 0
			if (runTime == 0) {
				runTime = 1; // change to 1 second
			}

			// get the number of allocated processors
			int numProc = new Integer(array[NUM_PROC].trim()).intValue();
			

			// finally, check if the num of PEs required is valid or not
			if (numProc <= 0) {
				System.out.println(super.get_name() + ": Warning - job #" + id
						+ " requires " + numProc
						+ " CPU. Change to 1 CPU.");
				numProc = 1;
			}

			// get the sigma value
			double sigma = new Double(array[SIGMA].trim()).doubleValue();
			
			// get the ppbw value
			double ppbw = new Double(array[PPBW].trim()).doubleValue();
			
			// submit a Gridlet
			submitGridlet(id, submitTime, runTime, numProc,sigma,ppbw);
		} catch (Exception e) {
			System.out.println(super.get_name()
					+ ": Exception in reading file"
					+ ", exception: " + e.getMessage());
		}
	}
	
	private void extractCustomField(String[] array) {
		try {

			// get the job number
			int id = 0;
			if (JOB_NUM == IRRELEVANT) {
				id = numGridlets;
			} else {
				id = new Integer(array[JOB_NUM].trim()).intValue();
			}

			// get the submit time
			long submitTime = 0;

			// get the run time
			 double runTime= new Double(array[RUN_TIME].trim()).doubleValue();


			// according to the SWF manual, runtime of 0 is possible due
			// to rounding down. E.g. runtime is 0.4 seconds -> runtime = 0
			if (runTime == 0) {
				runTime = 1; // change to 1 second
			}

			// get the number of allocated processors
			int numProc = new Integer(array[NUM_PROC].trim()).intValue();
			

			// finally, check if the num of PEs required is valid or not
			if (numProc <= 0) {
				System.out.println(super.get_name() + ": Warning - job #" + id
						+ " requires " + numProc
						+ " CPU. Change to 1 CPU.");
				numProc = 1;
			}

			// get the sigma value
			double sigma = 1.0;
			
			// get the ppbw value
			double ppbw = 1.0;
			
			// submit a Gridlet
			submitGridlet(id, submitTime, runTime, numProc,sigma,ppbw);
		} catch (Exception e) {
			System.out.println(super.get_name()
					+ ": Exception in reading file"
					+ ", exception: " + e.getMessage());
		}
	}

	/**
	 * Creates a Gridlet with the given information, then submit it to a
	 * resource
	 * 
	 * @param id
	 *            a Gridlet ID
	 * @param submitTime
	 *            Gridlet's submit time
	 * @param runTime
	 *            Gridlet's run time
	 * @param numProc
	 *            number of processors
	 * @pre id >= 0
	 * @pre submitTime >= 0
	 * @pre runTime >= 0
	 * @pre numProc > 0
	 * @post $none
	 */
	private void submitGridlet(int id, long submitTime, double runTime, int numProc,double sigma, double ppbw) {
		// create the gridlet
		Job j = new Job(id, runTime, numProc,sigma,ppbw);
		j.setUserID(super.get_id()); // set the owner ID
		// printing to inform user
		if (numGridlets == 1 || numGridlets % INTERVAL == 0) {
			System.out.println(super.get_name() + ": Submitting Gridlets to "
					+ resName_ + " ...");
		}


		numGridlets++; // increment the counter
		
		// submit a gridlet to resource
		super.send(super.output, submitTime, GridSimTags.GRIDLET_SUBMIT, new IO_data(j,j.getGridletFileSize(),resID_));
	}

	/**
	 * Reads a text file one line at the time
	 * 
	 * @param fileName
	 *            a file name
	 * @return <tt>true</tt> if reading a file is successful, <tt>false</tt>
	 *         otherwise.
	 * @pre fileName != null
	 * @post $none
	 */
	private boolean readFile(String fileName) {
		boolean success = false;
		BufferedReader reader = null;
		try {
			FileInputStream file = new FileInputStream(fileName);
			reader = new BufferedReader(new InputStreamReader(file));

			// read one line at the time
			while (reader.ready()) {
				parseValue(reader.readLine());
			}

			reader.close(); // close the file
			success = true;
		} catch (FileNotFoundException f) {
			System.out.println(super.get_name()
					+ ": Error - the file was not found: " + f.getMessage());
		} catch (IOException e) {
			System.out.println(super.get_name()
					+ ": Error - an IOException occurred: " + e.getMessage());
		} finally {
			if (reader != null) {
				try {
					reader.close(); // close the file
				} catch (IOException e) {
					System.out.println(super.get_name()
							+ ": Error - an IOException occurred: "
							+ e.getMessage());
				}
			}
		}

		return success;
	}
	
	private boolean readCustomFile(String fileName) {
		boolean success = false;
		BufferedReader reader = null;
		try {
			FileInputStream file = new FileInputStream(fileName);
			reader = new BufferedReader(new InputStreamReader(file));

			// read one line at the time
			while (reader.ready()) {
				parseValue(reader.readLine());
			}

			reader.close(); // close the file
			success = true;
		} catch (FileNotFoundException f) {
			System.out.println(super.get_name()
					+ ": Error - the file was not found: " + f.getMessage());
		} catch (IOException e) {
			System.out.println(super.get_name()
					+ ": Error - an IOException occurred: " + e.getMessage());
		} finally {
			if (reader != null) {
				try {
					reader.close(); // close the file
				} catch (IOException e) {
					System.out.println(super.get_name()
							+ ": Error - an IOException occurred: "
							+ e.getMessage());
				}
			}
		}

		return success;
	}

	/**
	 * Reads a gzip file one line at the time
	 * 
	 * @param fileName
	 *            a gzip file name
	 * @return <tt>true</tt> if reading a file is successful, <tt>false</tt>
	 *         otherwise.
	 * @pre fileName != null
	 * @post $none
	 */
	private boolean readGZIPFile(String fileName) {
		boolean success = false;
		BufferedReader reader = null;
		try {
			FileInputStream file = new FileInputStream(fileName);
			GZIPInputStream gz = new GZIPInputStream(file);
			reader = new BufferedReader(new InputStreamReader(gz));

			// read one line at the time
			while (reader.ready()) {
				parseValue(reader.readLine());
			}

			reader.close(); // close the file
			success = true;
		} catch (FileNotFoundException f) {
			System.out.println(super.get_name()
					+ ": Error - the file was not found: " + f.getMessage());
		} catch (IOException e) {
			System.out.println(super.get_name()
					+ ": Error - an IOException occurred: " + e.getMessage());
		} finally {
			if (reader != null) {
				try {
					reader.close(); // close the file
				} catch (IOException e) {
					System.out.println(super.get_name()
							+ ": Error - an IOException occurred: "
							+ e.getMessage());
				}
			}
		}

		return success;
	}

	/**
	 * Reads a Zip file. Iterating through each entry and reading it one line at
	 * the time.
	 * 
	 * @param fileName
	 *            a zip file name
	 * @return <tt>true</tt> if reading a file is successful, <tt>false</tt>
	 *         otherwise.
	 * @pre fileName != null
	 * @post $none
	 */
	private boolean readZipFile(String fileName) {
		boolean success = false;
		ZipFile zipFile = null;
		try {
			BufferedReader reader = null;

			// ZipFile offers an Enumeration of all the files in the Zip file
			zipFile = new ZipFile(fileName);
			Enumeration<? extends ZipEntry> e = zipFile.entries();
			while (e.hasMoreElements()) {
				success = false; // reset the value again
				ZipEntry zipEntry = e.nextElement();

				reader = new BufferedReader(new InputStreamReader(
						zipFile.getInputStream(zipEntry)));

				// read one line at the time
				while (reader.ready()) {
					parseValue(reader.readLine());
				}

				reader.close(); // close the file
				success = true;
			}
		} catch (IOException e) {
			System.out.println(super.get_name()
					+ ": Error - an IOException occurred: " + e.getMessage());
		} finally {
			if (zipFile != null) {
				try {
					zipFile.close(); // close the file
				} catch (IOException e) {
					System.out.println(super.get_name()
							+ ": Error - an IOException occurred: "
							+ e.getMessage());
				}
			}
		}
		return success;
	}
	

}
