package hybridTechnique;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.IntegerSBXCrossover;
import org.uma.jmetal.operator.impl.mutation.IntegerPolynomialMutation;
import org.uma.jmetal.operator.impl.mutation.SimpleRandomMutation;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.solution.IntegerSolution;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.SolutionListEvaluator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;

import genetic.JobCrossover;
import genetic.JobSwapMutation;
import genetic.NSGALog;
import genetic.SchedulingProblemMultiObjective;
import genetic.SchedulingSolution;
import geneticsimulator.BadAllocationException;
import geneticsimulator.Cluster;
import geneticsimulator.Job;
import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;
import svila.GridCloudBridge.FromCloudSimToGridSim;
import svila.staticAllocation.CloudletsAssignation;
import svila.staticAllocation.SWFAnalyzer;
import svila.staticAllocation.SvilaHelper;
import svila.staticAllocation.schedulingTechniques.StaticTaskSchedulingTechnique;

public class HybridTechnique extends StaticTaskSchedulingTechnique {
	CloudletsAssignation bestAssignation;
	
	public HybridTechnique() {
		this.techniqueName = "Hybrid";
	}
	
	public HybridTechnique(DatacenterBroker broker, List<Cloudlet> cloudlets, List<Vm> vms, List<Host> hosts) {
		super(broker, cloudlets, vms, hosts);
		this.techniqueName = "Hybrid";
	}
	
	@Override
	public void startTechnique() {		
		SchedulingSolution solution = obtainSolution();
		
		SetCloudletsToVms(solution);
		
		ei.launchStandaloneExperiment();
	}
	
	private void SetCloudletsToVms(SchedulingSolution solution) {
		SchedulingProblemMultiObjective problem = new SchedulingProblemMultiObjective(JobList.instance(),MultiCluster.instance());
		problem.evaluate(solution);
		HashMap<Integer, Integer> allocations = MultiCluster.instance().jobAllocations;
		
		bestAssignation = new CloudletsAssignation();
		for(Entry<Integer, Integer> entry : allocations.entrySet()) {
			bestAssignation.linkCloudletWithVM(entry.getKey(), entry.getValue());
			//System.out.println(entry.getKey() + ": " + entry.getValue());
		}
	}

	@Override
	public void execute() {
		Iterator<Entry<Integer, Integer>> iterator = bestAssignation.cloudletVMMap.entrySet().iterator();
		while(iterator.hasNext()) {
			Entry<Integer, Integer> entry = iterator.next();
			broker.bindCloudletToVm(entry.getKey(), entry.getValue());
		}
		
	}
	
	private SchedulingSolution obtainSolution() {
		ArrayList<Vm> vmList = SvilaHelper.createVmList(0, ei.getVmInputFile());
    	SWFAnalyzer swfAnalyzer = new SWFAnalyzer(ei.getWorkloadInputFile(), true, 0);
    	ArrayList<Cloudlet> cloudletList = swfAnalyzer.getAllCloudlets();
		
		//Execute the GA
		 HybridProblem problem;
		 //Algorithm<List<IntegerSolution>> algorithm;
		 NSGALog algorithm;
		 CrossoverOperator<IntegerSolution> crossover;
		 MutationOperator<IntegerSolution> mutation;
		 SelectionOperator<List<IntegerSolution>, IntegerSolution> selection;
		 SolutionListEvaluator<IntegerSolution> evaluator;
		 evaluator = new SequentialSolutionListEvaluator<IntegerSolution>();	
		 problem = new HybridProblem(cloudletList, vmList);
		 crossover = new IntegerSBXCrossover(0.1, 0.2) ;
		 mutation = new IntegerPolynomialMutation(problem, 0.1);
		 selection = new BinaryTournamentSelection<IntegerSolution>(new RankingAndCrowdingDistanceComparator<IntegerSolution>());
		 //algorithm = (Algorithm<List<IntegerSolution>>) new NSGALog(problem,10,100,crossover,mutation,selection, evaluator,"test.csv");
		 algorithm = new NSGALog(problem,10,100,crossover,mutation,selection, evaluator,"test.csv");

		 AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
		 .execute();
		 List<SchedulingSolution> population = algorithm.getResult() ;
		 long computingTime = algorithmRunner.getComputingTime() ;
		 ArrayList<SchedulingSolution> solutions = new ArrayList<SchedulingSolution>();
		 solutions.add(population.get(0));
 
		 return population.get(0);

	}
}
