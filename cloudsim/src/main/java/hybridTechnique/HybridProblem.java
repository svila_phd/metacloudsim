package hybridTechnique;

import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Vm;
import org.uma.jmetal.problem.impl.AbstractIntegerProblem;
import org.uma.jmetal.solution.IntegerSolution;

public class HybridProblem extends AbstractIntegerProblem {
	
	ArrayList<Vm> vmList;
	ArrayList<Cloudlet> cloudletList;
	
	
	public HybridProblem(ArrayList<Cloudlet> cloudletList, ArrayList<Vm> vmList) {
		super();
		this.setNumberOfObjectives(2);
		this.setNumberOfVariables(cloudletList.size());

		List<Integer> upperLimit = new ArrayList<Integer>();
		List<Integer> lowerLimit = new ArrayList<Integer>();
		for(int i=0; i<vmList.size(); i++) {
			upperLimit.add(vmList.size());
			lowerLimit.add(0);
		}
		
		this.setUpperLimit(upperLimit);
		this.setLowerLimit(lowerLimit);
	}
	
	@Override
	public void evaluate(IntegerSolution solution) {
		makespanSolution(solution);
		energySolution(solution);
	}
	
	private void makespanSolution(IntegerSolution solution) {
		double solutionValue = 0.0;
		
		for(int i=0; i<solution.getNumberOfVariables(); i++) {
			int cloudletId = i;
			int vmId = solution.getVariableValue(i);
			
			Cloudlet cloudlet = cloudletList.get(cloudletId);
			Vm vm = vmList.get(vmId);
			
			int maxPE = Math.min(cloudlet.getNumberOfPes(), vm.getNumberOfPes());
			solutionValue += (cloudlet.getCloudletLength() * cloudlet.getNumberOfPes()) / (vm.getMips() * vm.getNumberOfPes());
		}
		
		solution.setObjective(0, solutionValue);
	}
	
	private void energySolution(IntegerSolution solution) {
		
	}

}
