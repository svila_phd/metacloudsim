package PSOMO2;

import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.impl.mutation.NonUniformMutation;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.JMetalException;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

public class CustomNonUniformMutation extends NonUniformMutation {
	  private double perturbation;
	  private int maxIterations;
	  private double mutationProbability;

	  private int currentIteration;
	  private JMetalRandom randomGenenerator ;

	  /** Constructor */
	  public CustomNonUniformMutation(double mutationProbability, double perturbation, int maxIterations) {
	    super(perturbation, perturbation, maxIterations);
		this.perturbation = perturbation ;
	    this.mutationProbability = mutationProbability ;
	    this.maxIterations = maxIterations ;

	    randomGenenerator = JMetalRandom.getInstance() ;
	  }

	  /* Getters */
	  public double getPerturbation() {
	    return perturbation;
	  }

	  public int getMaxIterations() {
	    return maxIterations;
	  }

	  public double getMutationProbability() {
	    return mutationProbability;
	  }

	  public int getCurrentIteration() {
	    return currentIteration;
	  }

	  /* Setter */
	  public void setCurrentIteration(int currentIteration) {
	    if (currentIteration < 0) {
	      throw new JMetalException("Iteration number cannot be a negative value: " + currentIteration) ;
	    }

	    this.currentIteration = currentIteration;
	  }

	  /** Execute() method */
	  @Override
	  public DoubleSolution execute(DoubleSolution solution) {
	    if (null == solution) {
	      throw new JMetalException("Null parameter") ;
	    }

	    doMutation(mutationProbability, solution);

	    return solution;
	  }

	  /**
	   * Perform the mutation operation
	   *
	   * @param probability Mutation setProbability
	   * @param solution    The solution to mutate
	   */
	  public void doMutation(double probability, DoubleSolution solution){
	    for (int i = 0; i < solution.getNumberOfVariables(); i++) {
	        solution.setVariableValue(i, randomGenenerator.nextDouble());
	    }
	  }


	  /** Calculates the delta value used in NonUniform mutation operator */
	  private double delta(double y, double bMutationParameter) {
	    double rand = randomGenenerator.nextDouble();
	    int it, maxIt;
	    it = currentIteration;
	    maxIt = maxIterations;

	    return (y * (1.0 -
	        Math.pow(rand,
	            Math.pow((1.0 - it / (double) maxIt), bMutationParameter)
	        )));
	  }
	}
