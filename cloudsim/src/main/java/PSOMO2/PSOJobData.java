package PSOMO2;

import genetic.JobData;
import geneticsimulator.Job;

public class PSOJobData extends JobData {
	public double [][] possibleForbidenNodes;
	
	public PSOJobData(Job j,int mcSize){
		super(j,mcSize);
		possibleForbidenNodes=new double[10][mcSize];
	}
	
	public PSOJobData(PSOJobData j){
		super(j);
		for(int i=0;i<10;i++){
			for(int k=0;k<j.forbidenNodes.length;k++){
				this.possibleForbidenNodes[i][k]=j.possibleForbidenNodes[i][k];
			}
		}
	}
	
	public String toString(){
		String ret=job.getId()+" {";
		for(int i=0;i<this.forbidenNodes.length;i++){
			ret += this.forbidenNodes[i];
			if(i<this.forbidenNodes.length-1){
				ret +=",";
			}
		}
		ret +="}";
		return ret;
	}
}
