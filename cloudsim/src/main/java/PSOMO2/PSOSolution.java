package PSOMO2;


import PSOMO2.PSOJobData;
import geneticsimulator.Job;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.solution.impl.AbstractGenericSolution;



public class PSOSolution extends AbstractGenericSolution<Double, PSOProblem> implements DoubleSolution {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1566646965472012015L;
	Random r = new Random();
	ArrayList<PSOJobData> jobs;
	public PSOSolution(PSOProblem psoProblem) {
		super(psoProblem);
	    overallConstraintViolationDegree = 0.0 ;
	    numberOfViolatedConstraints = 0 ;
	    List<PSOJobData> randomSequence = new ArrayList<PSOJobData>(psoProblem.getPermutationLength());
	    for (int j = 0; j < psoProblem.getPermutationLength(); j++) {
	    	randomSequence.add(initializeJobData(psoProblem.getJobList().get(j)));
	    }
	    java.util.Collections.shuffle(randomSequence);
	    jobs = new ArrayList<PSOJobData>();
	    jobs.addAll(randomSequence);
	    for (int position=0;position < psoProblem.getPermutationLength(); position++) {
	    	PSOJobData job=randomSequence.get(position);
	    	int index=0;
	    	for(;index<psoProblem.getJobList().size();index++){
	    		if(job.job.getId()==psoProblem.getJobList().get(index).getId()){
	    			break;
	    		}
	    	}
	    	setVariableValue(index,(double)((double)position/(double)psoProblem.getPermutationLength()));
	    	
	    	//int indexAlloc=(psoProblem.getMultiClusterLength()*index)+psoProblem.getPermutationLength();
	    	//int endAlloc=indexAlloc+psoProblem.getMultiClusterLength();
	    	//for(int i=0;indexAlloc<endAlloc;indexAlloc++,i++){
	    	//	setVariableValue(indexAlloc,job.forbidenNodes[i]);
	    	//}
	    }
	    this.repairSolution();
	}
	
	public PSOSolution(PSOSolution sol){
		super(sol.problem);
		overallConstraintViolationDegree = 0.0 ;
	    numberOfViolatedConstraints = 0 ;
	    for (int i = 0; i < getNumberOfVariables(); i++) {
	    	setVariableValue(i, sol.getVariableValue(i));
		}
	    for (int i = 0; i < getNumberOfObjectives(); i++) {
	    	setObjective(i, sol.getObjective(i));
		}
	    this.jobs=(ArrayList<PSOJobData>) sol.jobs.clone();
	    this.repairSolution();
	}

	public String getVariableValueString(int index) {
		if(index<this.problem.getPermutationLength()){
			PSOJobData variable=getJobs().get(index);
			String solution="Job "+variable.job.getId()+" {";
			for(int i=0;i<variable.forbidenNodes.length;i++){
			solution+=variable.forbidenNodes[i]+",";
			}
			return solution+"}";
		}else{
			return "";
		}
	}

	public PSOSolution copy() {
		return new PSOSolution(this);
	}
	
	public PSOJobData initializeJobData(Job j){
		PSOJobData n=new PSOJobData(j,problem.getMultiClusterLength());
		for(int i=0;i<n.forbidenNodes.length;i++){
			n.forbidenNodes[i]=this.randomGenerator.nextDouble(0d, 1d);
		}
		for(int i=0;i<10;i++){
			for(int k=0;k<n.forbidenNodes.length;k++){
				n.possibleForbidenNodes[i][k]=this.randomGenerator.nextDouble(0,1d);
			}
		}
		return n;
	}
	
	public void repairSolution(){
		ArrayList<PSOJobData> jobs=this.getJobs();
		for(int i=0;i<problem.getPermutationLength();i++){
			PSOJobData j=jobs.get(i);
			boolean valid = false;
			while(!valid){
				int availableNodes=0;
				for(int c=0;c<j.forbidenNodes.length;c++){
					if(j.forbidenNodes[c]>1){j.forbidenNodes[c]=1;}
					if(j.forbidenNodes[c]<0){j.forbidenNodes[c]=0;}
				}
				for(int c=0;c<j.forbidenNodes.length;c++){
					int numNodesc=this.problem.mc.getClusterInPos(c).getMaxResources();
					availableNodes+=(int) (numNodesc*(1-j.forbidenNodes[c]));
				}
				if(availableNodes>=j.job.getRequirements()){
					valid=true;
				}else{
					ArrayList<Integer>cid = new ArrayList<Integer>();
					for(int c=0;c<j.forbidenNodes.length;c++){
						if(j.forbidenNodes[c]>0){
							cid.add(c);
						}
					}
					int ctemp=0;
					if(cid.size()>0){
						ctemp=r.nextInt(cid.size());
					}
					j.forbidenNodes[cid.get(ctemp)]-=0.1;
				}
			}
		}
	}
	
	public Double getLowerBound(int index) {
		return problem.getLowerBound(index);
	}

	public Double getUpperBound(int index) {
		return problem.getUpperBound(index);
	}
	
	public ArrayList<PSOJobData> getJobs(){
		ArrayList<PSOJobData> jobs=new ArrayList<PSOJobData>();
		ArrayList<Double> convert=new ArrayList<Double>();
		for (int i=0;i<this.problem.getPermutationLength();i++){
			convert.add(getVariableValue(i));
		}
		
		for (int position=0;position < this.problem.getPermutationLength(); position++) {
	    	int index=0;
	    	double min=Double.MAX_VALUE;
	    	for(int i=0;i<this.problem.getPermutationLength();i++){
	    		double value=convert.get(i);
	    		if(value<0){
	    			continue;
	    		}else{
	    			if(value < min){
	    				index=i;
	    				min=value;
	    			}
	    		}
	    	}
	    	PSOJobData n=new PSOJobData(this.problem.getJobList().get(index),this.problem.getMultiClusterLength());
	    	int found_position=0;
	    	for(;found_position<this.jobs.size();found_position++){
	    		if(n.job.getId() == this.jobs.get(found_position).job.getId()){
	    			break;
	    		}
	    	}
	    	PSOJobData n_real=this.jobs.get(found_position);
	    	for(int i=0;i<n.forbidenNodes.length;i++){
				n.forbidenNodes[i]=n_real.forbidenNodes[i];
			}
	    	
	    	convert.set(index, (double)-1);
	    	jobs.add(n_real);
		}
		return jobs;
	}
	
}
