package PSOMO2;

import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.impl.mutation.UniformMutation;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.JMetalException;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

public class CustomUniformMutation extends UniformMutation {
	  private Double perturbation;
	  private Double mutationProbability = null;
	  private JMetalRandom randomGenenerator ;

	  /** Constructor */
	  public CustomUniformMutation(double mutationProbability, double perturbation) {
		super(perturbation, perturbation);
	    this.mutationProbability = mutationProbability ;
	    this.perturbation = perturbation ;
	    randomGenenerator = JMetalRandom.getInstance() ;
	  }

	  /* Getters */
	  public Double getPerturbation() {
	    return perturbation;
	  }

	  public Double getMutationProbability() {
	    return mutationProbability;
	  }

	  /**
	   * Perform the operation
	   *
	   * @param probability Mutation setProbability
	   * @param solution    The solution to mutate
	   */
	public void doMutation(double probability, DoubleSolution solution)  {
	    for (int i = 0; i < solution.getNumberOfVariables(); i++) {
	        solution.setVariableValue(i, randomGenenerator.nextDouble());
	    }
	  }

	  /** Execute() method */
	  @Override
	  public DoubleSolution execute(DoubleSolution solution) {
	    if (null == solution) {
	      throw new JMetalException("Null parameter");
	    }

	    doMutation(mutationProbability, solution);

	    return solution;
	  }

}
