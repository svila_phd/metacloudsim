package PSOMO2;

import genetic.JobData;
import genetic.SchedulingSolution;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.uma.jmetal.algorithm.multiobjective.omopso.OMOPSO;
import org.uma.jmetal.operator.impl.mutation.NonUniformMutation;
import org.uma.jmetal.operator.impl.mutation.UniformMutation;
import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.evaluator.SolutionListEvaluator;

public class OMPSOLog extends OMOPSO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	long time;
	PrintWriter writer;
	UniformMutation uniformMutation;
	public OMPSOLog(DoubleProblem problem,
			SolutionListEvaluator<DoubleSolution> evaluator, int swarmSize,
			int maxIterations, int archiveSize,
			UniformMutation uniformMutation,
			NonUniformMutation nonUniformMutation,
			String logfile) {
		super(problem, evaluator, swarmSize, maxIterations, archiveSize,
				uniformMutation, nonUniformMutation);
		this.uniformMutation=uniformMutation;
		time=System.currentTimeMillis();
		try {
			writer = new PrintWriter(logfile, "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void initProgress(){
		super.initProgress();
		/*System.out.println("initial");
		List<DoubleSolution> l=this.getResult();
		double bigest=l.get(0).getObjective(0);
		for(DoubleSolution ss:l){
			if(bigest>ss.getObjective(0)){
				bigest=ss.getObjective(0);
			}
		}
		System.out.println(bigest);
		writer.println(bigest+";"+(System.currentTimeMillis()-this.time)/1000);
		writer.flush();*/
	}
	@Override
	public void updateProgress(){
		super.updateProgress();
		/*System.out.println("iteration");

		List<DoubleSolution> l=this.getResult();
		double bigest=l.get(0).getObjective(0);
		for(DoubleSolution ss:l){
			if(bigest>ss.getObjective(0)){
				bigest=ss.getObjective(0);
			}
		}
		System.out.println(bigest);
		writer.println(bigest+";"+(System.currentTimeMillis()-this.time)/1000);
		writer.flush();*/
	}
	
	 /*@Override
	  protected List<DoubleSolution> evaluateSwarm(List<DoubleSolution> swarm) {
	    List<DoubleSolution> newSwarm = super.evaluateSwarm(swarm);
	    for(DoubleSolution currentSwarm : newSwarm){
	    	PSOSolution psos=(PSOSolution) currentSwarm;
	    	for(PSOJobData job : psos.getJobs()){
	    		System.out.print(job.job.getId()+" ");
	    	}
	    	System.out.println(currentSwarm.getObjective(0));
	    }
	    return newSwarm;
	  }*/
	
	@Override
	  protected void perturbation(List<DoubleSolution> swarm)  {

	    for (int i = 0; i < swarm.size(); i++) {
	        uniformMutation.execute(swarm.get(i));
	    }
	  }
}
