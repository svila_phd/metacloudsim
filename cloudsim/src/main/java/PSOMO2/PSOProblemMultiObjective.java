package PSOMO2;


import java.util.ArrayList;

import org.uma.jmetal.solution.DoubleSolution;

import genetic.JobData;
import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;



public  class PSOProblemMultiObjective extends PSOProblem {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PSOProblemMultiObjective(JobList jobs, MultiCluster mc) {
		super(jobs, mc);
		this.setNumberOfObjectives(2);
	}

	public void evaluate(PSOSolution solution) {
		solution.repairSolution();
		ArrayList<PSOJobData> jobs = solution.getJobs();
		try{
			mc.reset();
			for(int i=0;i<this.getPermutationLength();i++){
				JobData j = jobs.get(i);
				int availableNodes=0;
				for(int c=0;c<j.forbidenNodes.length;c++){
					int numNodesc=this.mc.getClusterInPos(c).getMaxResources();
					availableNodes+=(int) (numNodesc*(1-j.forbidenNodes[c]));
				}
				if(availableNodes<j.job.getRequirements()){
					System.out.println(j);
					System.out.println("Error: blacklist does not leave enougth available nodes "+availableNodes+"-"+j.job.getRequirements());
					System.exit(0);
				}
				Integer[] alloc=mc.getMESDAllocForbiden(j.job, j.forbidenNodes);
				while(!mc.sheduleWithAllocation(j.job,alloc)){
					mc.execute();
					alloc=mc.getMESDAllocForbiden(j.job, j.forbidenNodes);
				}
				
			}
			solution.setObjective(0,mc.finishExecution());
			solution.setObjective(1,mc.getEnergyConsumition());
		}catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
	}

	public void evaluate(DoubleSolution solution) {
		evaluate((PSOSolution)solution);
	}


}
