package PSO;

import genetic.JobData;
import genetic.SchedulingProblem;
import genetic.SchedulingSolution;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.uma.jmetal.algorithm.impl.AbstractParticleSwarmOptimization;
import org.uma.jmetal.operator.impl.mutation.NonUniformMutation;
import org.uma.jmetal.operator.impl.mutation.UniformMutation;
import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.archive.impl.CrowdingDistanceArchive;
import org.uma.jmetal.util.archive.impl.NonDominatedSolutionListArchive;
import org.uma.jmetal.util.comparator.CrowdingDistanceComparator;
import org.uma.jmetal.util.comparator.DominanceComparator;
import org.uma.jmetal.util.evaluator.SolutionListEvaluator;
import org.uma.jmetal.util.fileoutput.SolutionSetOutput;
import org.uma.jmetal.util.fileoutput.impl.DefaultFileOutputContext;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;
import org.uma.jmetal.util.solutionattribute.impl.CrowdingDistance;

public class SchedulingPSO extends AbstractParticleSwarmOptimization<SchedulingSolution,List<SchedulingSolution>> {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int currentIteration;
	private int swarmSize;
	private int maxIterations;
	private SchedulingProblem problem;
	private SchedulingSolution leader;
	private SchedulingSolution[] localBest;
	private int[] permutationSpeed;
	private double[][][] allocationSpeed;
	private Random rgenerator;
	private double mass;
	private double gmass;
	private double blmass;
	private double time;
	PrintWriter writer;
	List<SchedulingSolution> swarm ;
    public SchedulingPSO(SchedulingProblem problem,int swarmSize, int maxIterations,double mass,double gmass,double blmass) {
		this.swarmSize=swarmSize;
		this.maxIterations=maxIterations;
		this.problem=problem;
		this.permutationSpeed=new int[swarmSize];
		this.allocationSpeed=new double[swarmSize][problem.getPermutationLength()][problem.getMultiClusterLength()];
		this.localBest=new SchedulingSolution[swarmSize];
		this.rgenerator=new Random();
		this.mass=mass;
		this.gmass=gmass;
		this.blmass=blmass;
		this.time=System.currentTimeMillis();
		try {
			writer = new PrintWriter("pso_info", "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    @Override
    public void run() {
      
      swarm = createInitialSwarm() ;
      swarm = evaluateSwarm(swarm);
      initializeLeaders(swarm) ;
      initializeParticlesMemory(swarm) ;
      initializeLeaders(swarm);
      initProgress();

      while (!isStoppingConditionReached()) {
        updateVelocity(swarm);
        updatePosition(swarm);
        perturbation(swarm);
        swarm = evaluateSwarm(swarm) ;
        updateLeaders(swarm) ;
        updateParticlesMemory(swarm) ;
        updateProgress();
      }
    }
    
    
	@Override protected void initProgress() {
		SchedulingSolution sol = (SchedulingSolution) this.getResult();
		System.out.println(sol.getObjective(0));
	    currentIteration = 0;
	}

	@Override protected void updateProgress() {
		SchedulingSolution sol = (SchedulingSolution) this.getResult();
		System.out.println(sol.getObjective(0));
		/*ArrayList solutionFound=new ArrayList<SchedulingSolution>();
		solutionFound.add(leader);
		if(currentIteration%50==0){
			String file=new String("file_"+(System.currentTimeMillis()-this.time)/1000+"tsv");
			writer.println(leader.getObjective(0)+" "+(System.currentTimeMillis()-this.time)/1000);
			new SolutionSetOutput.Printer(swarm)
	        .setSeparator("\t")
	        .setVarFileOutputContext(new DefaultFileOutputContext("VAR.tsv"))
	        .setFunFileOutputContext(new DefaultFileOutputContext(file))
	        .print();
		}*/
		
		currentIteration += 1;
	}

	@Override
	protected boolean isStoppingConditionReached() {
		if(currentIteration>=maxIterations){
			return true;
		}
		return false;
	}

	@Override
	protected List<SchedulingSolution> createInitialSwarm() {
		List<SchedulingSolution> initial = new ArrayList<SchedulingSolution>();
		for(int i=0;i<this.swarmSize;i++){
			initial.add(problem.createSolution());
		}
		
		return initial;
	}

	@Override
	protected List<SchedulingSolution> evaluateSwarm(
			List<SchedulingSolution> swarm) {
		List<SchedulingSolution> evaluated=new ArrayList<SchedulingSolution>();
		for(int i=0;i<swarm.size();i++){
			problem.evaluate(swarm.get(i));
			evaluated.add(swarm.get(i));
		}
		return evaluated;
	}

	@Override
	protected void initializeLeaders(List<SchedulingSolution> swarm) {
		double best=Double.MAX_VALUE;
		for(int i=0;i<swarm.size();i++){
			SchedulingSolution individual=swarm.get(i);
			if(individual.getObjective(0)<best){
				best=individual.getObjective(0);
				leader=individual;
			}
		}
	}

	@Override
	protected void initializeParticlesMemory(List<SchedulingSolution> swarm) {
		for(int i=0;i<swarm.size();i++){
			localBest[i]=swarm.get(i);
		}
	}

	@Override
	protected void initializeVelocity(List<SchedulingSolution> swarm) {
		for(int i=0;i<swarm.size();i++){
			permutationSpeed[i]=rgenerator.nextInt(problem.getPermutationLength());
			for(int j=0;j<problem.getPermutationLength();j++){
				for(int k=0;k<problem.getMultiClusterLength();k++){
					allocationSpeed[i][j][k]=rgenerator.nextDouble();
				}
			}
		}
		
	}

	@Override
	protected void updateVelocity(List<SchedulingSolution> swarm) {
		
		
		for(int i=0;i<swarm.size();i++){
			SchedulingSolution particle=swarm.get(i);
			//permutation speed updater
			int currPermSpeed =0;
			for(int p=0;p<this.leader.getNumberOfVariables();p++){
				if(! particle.getVariableValue(p).equals(leader.getVariableValue(p))){
					currPermSpeed+=1;
				}
			}
			this.permutationSpeed[i]=currPermSpeed;
			//allocation speed updater
			SchedulingSolution local=localBest[i];
			for(int p=0;p<leader.getNumberOfVariables();p++){
				JobData currentPosition=particle.getVariableValue(p);
				JobData localPosition=local.getVariableValue(p);
				JobData globalPosition=leader.getVariableValue(p);
				for(int a=0;a<problem.getMultiClusterLength();a++){
					double globalRandom=rgenerator.nextDouble();
					double localRandom=rgenerator.nextDouble();
					double localDiff=currentPosition.forbidenNodes[a]-localPosition.forbidenNodes[a];
					double globalDiff=currentPosition.forbidenNodes[a]-globalPosition.forbidenNodes[a];
					this.allocationSpeed[i][p][a]=mass*this.allocationSpeed[i][p][a]+blmass*localRandom*localDiff+gmass*globalRandom+globalDiff;
			
				}
			}
		}
	}

	@Override
	protected void updatePosition(List<SchedulingSolution> swarm) {
		int selector=rgenerator.nextInt(100);
		if(selector<50){
			//update allocation position
			for(int i=0;i<swarm.size();i++){
				SchedulingSolution particle=swarm.get(i);
				for(int p=0;p<particle.getNumberOfVariables();p++){
					JobData currentPosition = particle.getVariableValue(p);
					for(int a=0;a<problem.getMultiClusterLength();a++){
						currentPosition.forbidenNodes[a]+=this.allocationSpeed[i][p][a];
					}
				}
				swarm.set(i, particle);
			}
		}else{
			//update permutation position
			for(int i=0;i<swarm.size();i++){
				SchedulingSolution particle=swarm.get(i);
				int permProb=this.permutationSpeed[i]/problem.getPermutationLength();
				for(int pos=0;pos<problem.getPermutationLength();pos++){
					double r=rgenerator.nextDouble();
					if(r<permProb){
						JobData currentPosition = particle.getVariableValue(pos);
						for(int gpos=0;gpos<problem.getPermutationLength();gpos++){
							JobData gBestPosition=leader.getVariableValue(gpos);
							if(currentPosition.equals(gBestPosition)){
								JobData temp=particle.getVariableValue(gpos);
								particle.setVariableValue(gpos, particle.getVariableValue(pos));
								particle.setVariableValue(pos,temp);
							}
						}
					}
				}
				swarm.set(i, particle);
			}
		}
		for(int i=0;i<swarm.size();i++){
			swarm.get(i).repairSolution();
		}
	}

	@Override
	protected void perturbation(List<SchedulingSolution> swarm) {
		/*for(int i=0;i<swarm.size();i++){
			SchedulingSolution particle=swarm.get(i);
			int permProb=this.permutationSpeed[i]/problem.getPermutationLength();
			for(int pos=0;pos<problem.getPermutationLength();pos++){
				double r=rgenerator.nextDouble();
				if(r<permProb){
					JobData currentPosition = particle.getVariableValue(pos);
					for(int gpos=0;gpos<problem.getPermutationLength();gpos++){
						JobData gBestPosition=localBest[i].getVariableValue(gpos);
						if(currentPosition.equals(gBestPosition)){
							JobData temp=particle.getVariableValue(gpos);
							particle.setVariableValue(gpos, particle.getVariableValue(pos));
							particle.setVariableValue(pos,temp);
						}
					}
				}
			}
		}
		for(int i=0;i<swarm.size();i++){
			swarm.get(i).repairSolution();
		}*/
		
	}

	@Override
	protected void updateLeaders(List<SchedulingSolution> swarm) {
		double best=leader.getObjective(0);
		for(int i=0;i<swarm.size();i++){
			SchedulingSolution individual=swarm.get(i);
			if(individual.getObjective(0)<best){
				best=individual.getObjective(0);
				leader=individual;
			}
		}
	}

	@Override
	protected void updateParticlesMemory(List<SchedulingSolution> swarm) {
		for(int i=0;i<swarm.size();i++){
			SchedulingSolution individual=swarm.get(i);
			if(individual.getObjective(0)<localBest[i].getObjective(0)){
				localBest[i]=individual;
			}
		}
	}

	@Override
	public List<SchedulingSolution> getResult() {
		List<SchedulingSolution> result=new ArrayList<SchedulingSolution>();
		result.add(leader);
		return result;
	}

	
}
