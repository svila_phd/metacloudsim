package org.cloudbus.cloudsim.examples.power;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.VmSchedulerTimeShared;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationAbstract;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationInterQuartileRange;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationLocalRegression;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationLocalRegressionRobust;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationMedianAbsoluteDeviation;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicyMigrationStaticThreshold;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicySimple;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicy;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyMaximumCorrelation;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyMinimumMigrationTime;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyMinimumUtilization;
import org.cloudbus.cloudsim.power.PowerVmSelectionPolicyRandomSelection;

import svila.BSC;
import svila.JSONOutput;
import svila.PowerVmAllocationPolicyDoubleThresholdMinimizationOfMigrations;
import svila.mann.AbsoluteCapacity;
import svila.mann.DemandRisk;
import svila.mann.GuazzoneBFD;
import svila.mann.LagoAllocator;
import svila.mann.MWFDVP;
import svila.mann.PercentageUtil;
import svila.mann.SWFDVP;
import svila.planetlabNetwork.ExperimentConfiguration;
import svila.planetlabNetwork.PowerVmAllocationPolicyCustomTest;
import svila.planetlabNetwork.StaticResources;
import svila.planetlabNetwork.VMLoadBalancer;
import svila.planetlabNetwork.correlation.PearsonMathCorrelation;
import svila.planetlabNetwork.correlation.techniques.PowerVMSelectionPolicyPSP;
import svila.planetlabNetwork.correlation.techniques.PowerVmSelectionPolicyCustomTest;
import svila.planetlabNetwork.correlation.techniques.PowerVmSelectionPolicyPearsonCorrelation;
import svila.planetlabNetwork.correlation.techniques.PowerVmSelectionPolicyPearsonDiff;
import svila.planetlabNetwork.correlation.techniques.PowerVmSelectionPolicyPearsonDiffAbs;
import svila.planetlabNetwork.correlation.techniques.PowerVmSelectionPolicyPearsonNear;
import svila.planetlabNetwork.optimizeAllocation.MinDistanceGroupAllocationPolicyIQR;
import svila.planetlabNetwork.results.CorrelationsResults;
import svila.planetlabNetwork.results.ExperimentResults;
import svila.planetlabNetwork.results.ResultsManager;
import svila.policiesFactories.AbstractPolicyFactory;
import svila.policiesFactories.IVMSelectionPolicyFactory;
import svila.policiesFactories.TechniqueLoaderFactory;
import svila.policiesFactories.VMSelectionTechniqueLoaderFactory;

/**
 * The Class RunnerAbstract.
 * 
 * If you are using any algorithms, policies or workload included in the power package, please cite
 * the following paper:
 * 
 * Anton Beloglazov, and Rajkumar Buyya, "Optimal Online Deterministic Algorithms and Adaptive
 * Heuristics for Energy and Performance Efficient Dynamic Consolidation of Virtual Machines in
 * Cloud Data Centers", Concurrency and Computation: Practice and Experience (CCPE), Volume 24,
 * Issue 13, Pages: 1397-1420, John Wiley & Sons, Ltd, New York, USA, 2012
 * 
 * @author Anton Beloglazov
 */
public abstract class RunnerAbstract {

	/** The enable output. */
	private static boolean enableOutput;

	/** The broker. */
	protected static DatacenterBroker broker;

	/** The cloudlet list. */
	protected static List<Cloudlet> cloudletList;

	/** The vm list. */
	public static List<Vm> vmList;

	/** The host list. */
	protected static List<PowerHost> hostList;
	
	protected int migrationInterval;

	/**
	 * Run.
	 * 
	 * @param enableOutput the enable output
	 * @param outputToFile the output to file
	 * @param inputFolder the input folder
	 * @param outputFolder the output folder
	 * @param workload the workload
	 * @param vmAllocationPolicy the vm allocation policy
	 * @param vmSelectionPolicy the vm selection policy
	 * @param parameter the parameter
	 */
	public RunnerAbstract(
			boolean enableOutput,
			boolean outputToFile,
			String inputFolder,
			String outputFolder,
			String workload,
			String vmAllocationPolicy,
			String vmSelectionPolicy,
			String parameter,
			int migrationInterval) {
		try {
			initLogOutput(
					enableOutput,
					outputToFile,
					outputFolder,
					workload,
					vmAllocationPolicy,
					vmSelectionPolicy,
					parameter);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		this.migrationInterval = migrationInterval;
		
		init(inputFolder + "/" + workload);
		
				
		ExperimentConfiguration ec = StaticResources.getCE();		
		AbstractPolicyFactory policyFactory = TechniqueLoaderFactory.getTechniqueLoaderPolicy(ec.getTechniqueLoader());
		IVMSelectionPolicyFactory vmSelectionPolicyFactory = VMSelectionTechniqueLoaderFactory.getVMSelectionTechniqueLoaderPolicy(ec.getTechniqueLoader());
		PowerVmSelectionPolicy chosenVmSelectionPolicy = vmSelectionPolicyFactory.getVmSelectionPolicy(vmSelectionPolicy);

		System.out.print("VM Selection Policy Factory: " + vmSelectionPolicyFactory.getVersion());
		System.out.print("Abstract/General/Allocation Policy Factory: " + policyFactory.getVersion());

		policyFactory.setHostList(hostList);
		policyFactory.setPowerVmSelectionPolicy(chosenVmSelectionPolicy);
		policyFactory.setDoubleParameter(parameter);
		policyFactory.setStringParameter(parameter);
		policyFactory.setUtilizationThreshold(0.7);
		policyFactory.setSchedulingInterval(Constants.SCHEDULING_INTERVAL);
        VmAllocationPolicy chosenVmAllocationPolicy = policyFactory.getVmAllocationPolicy(vmAllocationPolicy);
                
                
                
		/*start(
				getExperimentName(workload, vmAllocationPolicy, vmSelectionPolicy, parameter),
				outputFolder,
				getVmAllocationPolicy(vmAllocationPolicy, vmSelectionPolicy, parameter));*/
        start(
				getExperimentName(workload, vmAllocationPolicy, vmSelectionPolicy, parameter),
				outputFolder,
				chosenVmAllocationPolicy);
	}
	
	public RunnerAbstract(
			ExperimentConfiguration ec) {
		try {
			initLogOutput(
					ec.isEnableOutput(),
					ec.isOutputToFile(),
					ec.getLogOutputFolder(),
					ec.getWorkload()+"-"+ec.getWorkloadTrace(),
					ec.getAllocationPolicy(),
					ec.getSelectionPolicy(),
					ec.getParameter());
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		this.migrationInterval = ec.getMigrationInterval();
		
		init(Paths.get(StaticResources.baseFolder, StaticResources.workloadsFolderName, ec.getWorkload()).toString());
		
		AbstractPolicyFactory policyFactory = TechniqueLoaderFactory.getTechniqueLoaderPolicy(ec.getTechniqueLoader());
		IVMSelectionPolicyFactory vmSelectionPolicyFactory = VMSelectionTechniqueLoaderFactory.getVMSelectionTechniqueLoaderPolicy(ec.getTechniqueLoader());
		PowerVmSelectionPolicy chosenVmSelectionPolicy = vmSelectionPolicyFactory.getVmSelectionPolicy(ec.getSelectionPolicy());

		policyFactory.setHostList(hostList);
		policyFactory.setPowerVmSelectionPolicy(chosenVmSelectionPolicy);
		policyFactory.setDoubleParameter(ec.getParameter());
		policyFactory.setStringParameter(ec.getParameter());
		policyFactory.setUtilizationThreshold(ec.getUtilizationThreshold());
		policyFactory.setSchedulingInterval(Constants.SCHEDULING_INTERVAL);
        VmAllocationPolicy chosenVmAllocationPolicy = policyFactory.getVmAllocationPolicy(ec.getAllocationPolicy());
        chosenVmAllocationPolicy.setHostList(hostList);
        
        start(
		getExperimentName(ec.getWorkload(),
						ec.getAllocationPolicy(),
						ec.getSelectionPolicy(), ec.getParameter()),
						ec.getLogOutputFolder(),
						chosenVmAllocationPolicy);
	}

	/**
	 * Inits the log output.
	 * 
	 * @param enableOutput the enable output
	 * @param outputToFile the output to file
	 * @param outputFolder the output folder
	 * @param workload the workload
	 * @param vmAllocationPolicy the vm allocation policy
	 * @param vmSelectionPolicy the vm selection policy
	 * @param parameter the parameter
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws FileNotFoundException the file not found exception
	 */
	protected void initLogOutput(
			boolean enableOutput,
			boolean outputToFile,
			String outputFolder,
			String workload,
			String vmAllocationPolicy,
			String vmSelectionPolicy,
			String parameter) throws IOException, FileNotFoundException {
		setEnableOutput(enableOutput);
		Log.setDisabled(!isEnableOutput());
		if (isEnableOutput() && outputToFile) {
			File folder = new File(outputFolder);
			if (!folder.exists()) {
				folder.mkdir();
			}

			File folder2 = new File(outputFolder + "/log");
			if (!folder2.exists()) {
				folder2.mkdir();
			}

			File file = new File(outputFolder + "/log/"
					+ getExperimentName(workload, vmAllocationPolicy, vmSelectionPolicy, parameter) + ".txt");
			file.createNewFile();
			Log.setOutput(new FileOutputStream(file));
		}
	}

	/**
	 * Inits the simulation.
	 * 
	 * @param inputFolder the input folder
	 */
	protected abstract void init(String inputFolder);

	/**
	 * Starts the simulation.
	 * 
	 * @param experimentName the experiment name
	 * @param outputFolder the output folder
	 * @param vmAllocationPolicy the vm allocation policy
	 */
	protected void start(String experimentName,
			String outputFolder,
			VmAllocationPolicy vmAllocationPolicy) {
		System.out.println("Starting " + experimentName);
		
		try {
			PowerDatacenter datacenter = (PowerDatacenter) Helper.createDatacenter(
					"Datacenter",
					PowerDatacenter.class,
					hostList,
					vmAllocationPolicy);

			datacenter.setDisableMigrations(false);

			broker.submitVmList(vmList);
			broker.submitCloudletList(cloudletList);
                        
			JSONOutput.setEnvironment(datacenter, hostList, vmList, cloudletList);
                        
			CloudSim.terminateSimulation(Constants.SIMULATION_LIMIT);
			
			long startTime = System.nanoTime();
			double lastClock = CloudSim.startSimulation();

			List<Cloudlet> newList = broker.getCloudletReceivedList();
			Log.printLine("Received " + newList.size() + " cloudlets");

			CloudSim.stopSimulation();
			long endTime = System.nanoTime();

			Helper.printResults(
					datacenter,
					vmList,
					lastClock,
					experimentName,
					Constants.OUTPUT_CSV,
					outputFolder);
			
			Log.enable();
			List<Cloudlet> cList = broker.getCloudletReceivedList();
			printCloudletList(cList);
			
			//exportResults(datacenter, lastClock, startTime, endTime);
			exportResults(startTime, endTime, lastClock);
					

		} catch (Exception e) {
			e.printStackTrace();
			Log.printLine("The simulation has been terminated due to an unexpected error");
			System.exit(0);
		}

		Log.printLine("Finished " + experimentName);
	}
	
	public void exportResults(long startTime, long endTime, double lastClock) {
		ResultsManager.experimentsResultsStream.addTimeResults(startTime, endTime, lastClock);
		ResultsManager.experimentsResultsStream.calculateResults();
		ResultsManager.experimentsResultsStream.export();
	}
	
	protected static void printCloudletList(List<Cloudlet> list) {
		int size = list.size();
		Cloudlet cloudlet;

		String indent = "    ";
		Log.printLine();
		Log.printLine("========== OUTPUT ==========");
		Log.printLine("Cloudlet ID" + indent + "STATUS" + indent
				+ "Data center ID" + indent + "VM ID" + indent + "Time" + indent
				+ "Start Time" + indent + "Finish Time");

		DecimalFormat dft = new DecimalFormat("###.##");
		for (int i = 0; i < size; i++) {
			cloudlet = list.get(i);
			Log.print(indent + cloudlet.getCloudletId() + indent + indent);

			if (cloudlet.getCloudletStatus() == Cloudlet.SUCCESS) {
				Log.print("SUCCESS");

				Log.printLine(indent + indent + cloudlet.getResourceId()
						+ indent + indent + indent + cloudlet.getVmId()
						+ indent + indent
						+ dft.format(cloudlet.getActualCPUTime()) + indent
						+ indent + dft.format(cloudlet.getExecStartTime())
						+ indent + indent
						+ dft.format(cloudlet.getFinishTime()));
			}
		}
	}

	/**
	 * Gets the experiment name.
	 * 
	 * @param args the args
	 * @return the experiment name
	 */
	protected String getExperimentName(String... args) {
		StringBuilder experimentName = new StringBuilder();
		for (int i = 0; i < args.length; i++) {
			if (args[i].isEmpty()) {
				continue;
			}
			if (i != 0) {
				experimentName.append("_");
			}
			experimentName.append(args[i]);
		}
		return experimentName.toString();
	}

	/**
	 * Gets the vm allocation policy.
	 * 
	 * @param vmAllocationPolicyName the vm allocation policy name
	 * @param vmSelectionPolicyName the vm selection policy name
	 * @param parameterName the parameter name
	 * @return the vm allocation policy
	 */
	protected VmAllocationPolicy getVmAllocationPolicy(
			String vmAllocationPolicyName,
			String vmSelectionPolicyName,
			String parameterName) {
		VmAllocationPolicy vmAllocationPolicy = null;
		PowerVmSelectionPolicy vmSelectionPolicy = null;
		if (!vmSelectionPolicyName.isEmpty()) {
			vmSelectionPolicy = getVmSelectionPolicy(vmSelectionPolicyName);
		}
		double parameter = 0;
		if (!parameterName.isEmpty()) {
			parameter = Double.valueOf(parameterName);
		}
		if (vmAllocationPolicyName.equals("iqr")) {
			PowerVmAllocationPolicyMigrationAbstract fallbackVmSelectionPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					0.7);
			vmAllocationPolicy = new PowerVmAllocationPolicyMigrationInterQuartileRange(
					hostList,
					vmSelectionPolicy,
					parameter,
					fallbackVmSelectionPolicy);
		} else if (vmAllocationPolicyName.equals("mad")) {
			PowerVmAllocationPolicyMigrationAbstract fallbackVmSelectionPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					0.7);
			vmAllocationPolicy = new PowerVmAllocationPolicyMigrationMedianAbsoluteDeviation(
					hostList,
					vmSelectionPolicy,
					parameter,
					fallbackVmSelectionPolicy);
		} else if (vmAllocationPolicyName.equals("lr")) {
			PowerVmAllocationPolicyMigrationAbstract fallbackVmSelectionPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					0.7);
			vmAllocationPolicy = new PowerVmAllocationPolicyMigrationLocalRegression(
					hostList,
					vmSelectionPolicy,
					parameter,
					Constants.SCHEDULING_INTERVAL,
					fallbackVmSelectionPolicy);
		} else if (vmAllocationPolicyName.equals("lrr")) {
			PowerVmAllocationPolicyMigrationAbstract fallbackVmSelectionPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					0.7);
			vmAllocationPolicy = new PowerVmAllocationPolicyMigrationLocalRegressionRobust(
					hostList,
					vmSelectionPolicy,
					parameter,
					Constants.SCHEDULING_INTERVAL,
					fallbackVmSelectionPolicy);
		} else if (vmAllocationPolicyName.equals("thr")) {
			vmAllocationPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					parameter);
        } else if (vmAllocationPolicyName.equals("dt")) {
                        vmAllocationPolicy = new PowerVmAllocationPolicyDoubleThresholdMinimizationOfMigrations(
                                        hostList,
                                        BSC.LOWER_THRESHOLD,
                                        BSC.UPPER_THRESHOLD
                        );
		} else if (vmAllocationPolicyName.equals("dvfs")) {
			vmAllocationPolicy = new PowerVmAllocationPolicySimple(hostList);
		} 
		// NEW VM ALLOCATION POLICIES FROM ALGORITHMS PROJECT - SERGI
		else if(vmAllocationPolicyName.equals("lago")) {
			vmAllocationPolicy = new LagoAllocator(hostList, vmSelectionPolicy, 1.0);
		} else if (vmAllocationPolicyName.equals("chowdm")) {
			vmAllocationPolicy = new MWFDVP(hostList, vmSelectionPolicy, 0.7);
		} else if (vmAllocationPolicyName.equals("chowds")) {
			vmAllocationPolicy = new SWFDVP(hostList, vmSelectionPolicy, 0.7);
		} else if (vmAllocationPolicyName.equals("guazzone")) {
			vmAllocationPolicy = new GuazzoneBFD(hostList, vmSelectionPolicy, 0.7);
		} else if (vmAllocationPolicyName.equals("perc")) {
			vmAllocationPolicy = new PercentageUtil(hostList, vmSelectionPolicy, 0.7);
		} else if (vmAllocationPolicyName.equals("abs")) {
			vmAllocationPolicy = new AbsoluteCapacity(hostList, vmSelectionPolicy, 0.7);
		} else if (vmAllocationPolicyName.equals("calavecchia") || vmAllocationPolicyName.equals("calcavecchia")) {
			vmAllocationPolicy = new DemandRisk(hostList, vmSelectionPolicy, 0.7);
		}
		
		// TESTING ALLOCATION
		else if (vmAllocationPolicyName.equals("test")) {
			vmAllocationPolicy = new PowerVmAllocationPolicyCustomTest(hostList, vmSelectionPolicy);
		}
		
		// NEW ALLOCATION
		else if (vmAllocationPolicyName.equals("mdgiqr")) {
			PowerVmAllocationPolicyMigrationAbstract fallbackVmSelectionPolicy = new PowerVmAllocationPolicyMigrationStaticThreshold(
					hostList,
					vmSelectionPolicy,
					0.7);
			vmAllocationPolicy = new MinDistanceGroupAllocationPolicyIQR(
					hostList,
					vmSelectionPolicy,
					parameter,
					fallbackVmSelectionPolicy);
		}
		else {
			System.out.println("Unknown VM allocation policy: " + vmAllocationPolicyName);
			System.exit(0);
		}
		
		return vmAllocationPolicy;
	}

	/**
	 * Gets the vm selection policy.
	 * 
	 * @param vmSelectionPolicyName the vm selection policy name
	 * @return the vm selection policy
	 */
	/* CURRENTLY NOT USED */
	protected PowerVmSelectionPolicy getVmSelectionPolicy(String vmSelectionPolicyName) {
		PowerVmSelectionPolicy vmSelectionPolicy = null;
		if (vmSelectionPolicyName.equals("mc")) {
			vmSelectionPolicy = new PowerVmSelectionPolicyMaximumCorrelation(
					new PowerVmSelectionPolicyMinimumMigrationTime());
		} else if (vmSelectionPolicyName.equals("mmt")) {
			vmSelectionPolicy = new PowerVmSelectionPolicyMinimumMigrationTime();
		} else if (vmSelectionPolicyName.equals("mu")) {
			vmSelectionPolicy = new PowerVmSelectionPolicyMinimumUtilization();
		} else if (vmSelectionPolicyName.equals("rs")) {
			vmSelectionPolicy = new PowerVmSelectionPolicyRandomSelection();
			
		// TEST	SELECTION
		} else if (vmSelectionPolicyName.equals("test")) {
			vmSelectionPolicy = new PowerVmSelectionPolicyCustomTest();
		} else if (vmSelectionPolicyName.equals("pearson")) {
			vmSelectionPolicy = new PowerVmSelectionPolicyPearsonCorrelation(
					new PearsonMathCorrelation(),
					new PowerVmSelectionPolicyMinimumMigrationTime()
					);
		} else if (vmSelectionPolicyName.equals("pearsonDiff")) {
			vmSelectionPolicy = new PowerVmSelectionPolicyPearsonDiff(
					new PearsonMathCorrelation(),
					new PowerVmSelectionPolicyMinimumMigrationTime()
					);
		} else if(vmSelectionPolicyName.equals("pearsonDiffAbs")) {
			vmSelectionPolicy = new PowerVmSelectionPolicyPearsonDiffAbs(
					new PearsonMathCorrelation(),
					new PowerVmSelectionPolicyMinimumMigrationTime()
					);
		} else if(vmSelectionPolicyName.equals("pearsonNear")) {
			Pair<Double, Double> optimalPoint = new Pair<>(0.65, 0.65);
			double maxDistance = 0.2;
			vmSelectionPolicy = new PowerVmSelectionPolicyPearsonNear(
					new PearsonMathCorrelation(),
					new PowerVmSelectionPolicyMinimumMigrationTime(),
					optimalPoint, maxDistance);
		} else if (vmSelectionPolicyName.equals("psp")) {
			vmSelectionPolicy = new PowerVMSelectionPolicyPSP(
				new PearsonMathCorrelation(),
				new PowerVmSelectionPolicyMinimumMigrationTime()
				);
			new CorrelationsResults().start();
		} else {
			System.out.println("-Unknown VM selection policy: " + vmSelectionPolicyName);
			System.exit(0);
		}
		/* CURRENTLY NOT USED */
		return vmSelectionPolicy;
	}

	/**
	 * Sets the enable output.
	 * 
	 * @param enableOutput the new enable output
	 */
	public void setEnableOutput(boolean enableOutput) {
		RunnerAbstract.enableOutput = enableOutput;
	}

	/**
	 * Checks if is enable output.
	 * 
	 * @return true, if is enable output
	 */
	public boolean isEnableOutput() {
		return enableOutput;
	}

}
