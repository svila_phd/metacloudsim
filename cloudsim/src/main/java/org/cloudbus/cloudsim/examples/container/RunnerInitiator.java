package org.cloudbus.cloudsim.examples.container;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;

import java.util.Calendar;
import svila.containersExperiment.ConstantsCustom;
import svila.containersExperiment.HelperExCustom;

/**
 * This is the modified version of {@link org.cloudbus.cloudsim.examples.power.planetlab.PlanetLabRunner} in CloudSim Package.
 * Created by sareh on 18/08/15.
 */

public class RunnerInitiator extends RunnerAbs {


    /**
     * Instantiates a new runner.
     *
     * @param enableOutput       the enable output
     * @param outputToFile       the output to file
     * @param inputFolder        the input folder
     * @param outputFolder       the output folder
     *                           //     * @param workload the workload
     * @param vmAllocationPolicy the vm allocation policy
     * @param vmSelectionPolicy  the vm selection policy
     */
    public RunnerInitiator(
            boolean enableOutput,
            boolean outputToFile,
            String inputFolder,
            String outputFolder,
            String vmAllocationPolicy,
            String containerAllocationPolicy,
            String vmSelectionPolicy,
            String containerSelectionPolicy,
            String hostSelectionPolicy,
            double overBookingFactor, String runTime, String logAddress) {


        super(enableOutput,
                outputToFile,
                inputFolder,
                outputFolder,
                vmAllocationPolicy,
                containerAllocationPolicy,
                vmSelectionPolicy,
                containerSelectionPolicy,
                hostSelectionPolicy,
                overBookingFactor, runTime, logAddress);

    }

    /*
     * (non-Javadoc)
     *
     * @see RunnerAbs
     */
    @Override
    protected void init(String inputFolder, double overBookingFactor) {
        try {
            CloudSim.init(1, Calendar.getInstance(), false);
//            setOverBookingFactor(overBookingFactor);
            broker = HelperExCustom.createBroker(overBookingFactor);
            int brokerId = broker.getId();
            cloudletList = HelperExCustom.createContainerCloudletList(brokerId, inputFolder, ConstantsCustom.NUMBER_CLOUDLETS);
            containerList = HelperExCustom.createContainerList(brokerId, ConstantsCustom.NUMBER_CLOUDLETS);
            vmList = HelperExCustom.createVmList(brokerId, ConstantsCustom.NUMBER_VMS);
            hostList = HelperExCustom.createHostList(ConstantsCustom.NUMBER_HOSTS);

        } catch (Exception e) {
            e.printStackTrace();
            Log.printLine("The simulation has been terminated due to an unexpected error");
            System.exit(0);
        }
    }


}
