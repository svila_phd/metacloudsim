package org.cloudbus.cloudsim;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import svila.planetlabNetwork.StaticResources;

/**
 * Defines the resource utilization model based on 
 * a <a href="https://www.planet-lab.org">PlanetLab</a>
 * datacenter trace file.
 */
public class UtilizationModelPlanetLabInMemory implements UtilizationModel {
	
	/** The scheduling interval. */
	protected double schedulingInterval;

	/** The data (5 min * 288 = 24 hours). */
	protected double[] data;
	
	public double[] load;
	
	protected String path;
	private double lastTime;
	private double lastUtilization;
	
	protected UtilizationModelPlanetLabInMemory() {
	}
	
	/**
	 * Instantiates a new PlanetLab resource utilization model from a trace file.
	 * 
	 * @param inputPath The path of a PlanetLab datacenter trace.
         * @param schedulingInterval
	 * @throws NumberFormatException the number format exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public UtilizationModelPlanetLabInMemory(String inputPath, double schedulingInterval, int linesToJump, int numData)
			throws NumberFormatException,
			IOException {
		this.lastTime = -1.0;
		data = new double[numData]; // 289
		load = new double[numData];
		setSchedulingInterval(schedulingInterval);
        System.out.println("UtilizationModelPlanetLabInMemory path: " + inputPath);
		BufferedReader input = new BufferedReader(new FileReader(inputPath));
		int n = data.length;
		if(getJumpFirstLine()) {
			input.readLine();
		}

		jumpLines(input, linesToJump);
		
		for (int i = 0; i < n - 1; i++) {
			data[i] = readData(input.readLine()) / 100.0;
			load[i] = 1.0;
		}
		data[n - 1] = data[n - 2];
		input.close();
		path = inputPath;
	}
	
	protected double readData(String line) {
		return Integer.valueOf(line);
	}
	
	protected boolean getJumpFirstLine() {
		return false;
	}
	
	protected void jumpLines(BufferedReader input, int linesToJump) throws IOException {
		for(int i=0; i<linesToJump; i++) {
			input.readLine();
		}
	}
	
	/**
	 * Instantiates a new PlanetLab resource utilization model with variable data samples
         * from a trace file.
	 * 
	 * @param inputPath The path of a PlanetLab datacenter trace.
	 * @param dataSamples number of samples in the file
	 * @throws NumberFormatException the number format exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public UtilizationModelPlanetLabInMemory(String inputPath, double schedulingInterval, int dataSamples)
			throws NumberFormatException,
			IOException {
		this.lastTime = -1;
		setSchedulingInterval(schedulingInterval);
		data = new double[dataSamples];
		BufferedReader input = new BufferedReader(new FileReader(inputPath));
		int n = data.length;
		for (int i = 0; i < n - 1; i++) {
			data[i] = readData(input.readLine()) / 100.0;
		}
		data[n - 1] = data[n - 2];
		input.close();
	}

	@Override
	public double getUtilization(double time) {
		if(time != this.lastTime) {
			this.lastUtilization = getRealUtilization(time) * getCustomUtilization(time);
			this.lastTime = time;
		}
		return this.lastUtilization;
	}
	
	private double getRealUtilization(double time) {
		if (time % getSchedulingInterval() == 0) {
			return data[(int) time / (int) getSchedulingInterval()];
		}
		int time1 = (int) Math.floor(time / getSchedulingInterval());
		int time2 = (int) Math.ceil(time / getSchedulingInterval());
		double utilization1 = data[time1];
		double utilization2 = data[time2];
		double delta = (utilization2 - utilization1) / ((time2 - time1) * getSchedulingInterval());
		double utilization = utilization1 + delta * (time - time1 * getSchedulingInterval());
		
		return utilization;
	}
	
	private double getCustomUtilization(double time) {
		if (time % getSchedulingInterval() == 0) {
			return load[(int) time / (int) getSchedulingInterval()];
		}
		int time1 = (int) Math.floor(time / getSchedulingInterval());
		int time2 = (int) Math.ceil(time / getSchedulingInterval());
		double utilization1 = load[time1];
		double utilization2 = load[time2];
		double delta = (utilization2 - utilization1) / ((time2 - time1) * getSchedulingInterval());
		double utilization = utilization1 + delta * (time - time1 * getSchedulingInterval());
		
		return utilization;
	}

	/**
	 * Sets the scheduling interval.
	 * 
	 * @param schedulingInterval the new scheduling interval
	 */
	public void setSchedulingInterval(double schedulingInterval) {
		this.schedulingInterval = schedulingInterval;
	}

	/**
	 * Gets the scheduling interval.
	 * 
	 * @return the scheduling interval
	 */
	public double getSchedulingInterval() {
		return schedulingInterval;
	}
	
	public double[] getData(){
		return data;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setData(double[] data) {
		this.data = data;
	}

	public double[] getLoad() {
		return load;
	}

	public void setLoad(double[] load) {
		this.load = load;
	}
}
