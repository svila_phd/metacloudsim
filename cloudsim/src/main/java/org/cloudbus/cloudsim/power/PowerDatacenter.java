/*
 * Title:        CloudSim Toolkit
 * Description:  CloudSim (Cloud Simulation) Toolkit for Modeling and Simulation of Clouds
 * Licence:      GPL - http://www.gnu.org/copyleft/gpl.html
 *
 * Copyright (c) 2009-2012, The University of Melbourne, Australia
 */

package org.cloudbus.cloudsim.power;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerDynamicWorkload;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.UtilizationModelNull;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;
import org.cloudbus.cloudsim.core.SimEvent;
import org.cloudbus.cloudsim.core.predicates.PredicateType;
import org.cloudbus.cloudsim.examples.power.RunnerAbstract;

import svila.JSONOutput;
import svila.StaticLog;
import svila.SvilaPrinter;
import svila.planetlabNetwork.Graph;
import svila.planetlabNetwork.Interaction;
import svila.planetlabNetwork.InteractionsContainer;
import svila.planetlabNetwork.MigrationDelayer;
import svila.planetlabNetwork.PlanetLabNetworkConstants;
import svila.planetlabNetwork.PlanetLabNetworkRunner;
import svila.planetlabNetwork.SimulatedInteractionsManager;
import svila.planetlabNetwork.Snapshot;
import svila.planetlabNetwork.StaticResources;
import svila.planetlabNetwork.VMLoadBalancer;
import svila.planetlabNetwork.results.EnergyContainer;
import svila.planetlabNetwork.results.MigrationResults;
import svila.planetlabNetwork.results.NetworkPhysicalResults;
import svila.planetlabNetwork.results.NetworkResults;
import svila.planetlabNetwork.results.ResultsManager;
import svila.workloads.UtilizationModelFactory;

/**
 * PowerDatacenter is a class that enables simulation of power-aware data centers.
 * 
 * <br/>If you are using any algorithms, policies or workload included in the power package please cite
 * the following paper:<br/>
 * 
 * <ul>
 * <li><a href="http://dx.doi.org/10.1002/cpe.1867">Anton Beloglazov, and Rajkumar Buyya, "Optimal Online Deterministic Algorithms and Adaptive
 * Heuristics for Energy and Performance Efficient Dynamic Consolidation of Virtual Machines in
 * Cloud Data Centers", Concurrency and Computation: Practice and Experience (CCPE), Volume 24,
 * Issue 13, Pages: 1397-1420, John Wiley & Sons, Ltd, New York, USA, 2012</a>
 * </ul>
 * 
 * @author Anton Beloglazov
 * @since CloudSim Toolkit 2.0
 */
public class PowerDatacenter extends Datacenter {

	/** The datacenter consumed power. */
	private double power;

	/** Indicates if migrations are disabled or not. */
	private boolean disableMigrations;

	/** The last time submitted cloudlets were processed. */
	private double cloudletSubmitted;

	/** The VM migration count. */
	private int migrationCount;

	public List<Snapshot> snapshots;
	
	public MigrationDelayer migrationDelayer;
	
	public DatacenterBroker datacenterBroker;
	
	public int slotNum;
	
	// Sergi
	private int delayedMigrationCount;
	
	/**
	 * Instantiates a new PowerDatacenter.
	 * 
	 * @param name the datacenter name
	 * @param characteristics the datacenter characteristics
	 * @param schedulingInterval the scheduling interval
	 * @param vmAllocationPolicy the vm provisioner
	 * @param storageList the storage list
	 * @throws Exception the exception
	 */
	public PowerDatacenter(
			String name,
			DatacenterCharacteristics characteristics,
			VmAllocationPolicy vmAllocationPolicy,
			List<Storage> storageList,
			double schedulingInterval) throws Exception {
		super(name, characteristics, vmAllocationPolicy, storageList, schedulingInterval);

		setPower(0.0);
		setDisableMigrations(false);
		setCloudletSubmitted(-1);
		setMigrationCount(0);
		snapshots = new ArrayList<>();
		slotNum = 0;
		ResultsManager.init(snapshots, this);
		EnergyContainer.init(getHostList());
	}

	public void setDatacenterBroker(DatacenterBroker datacenterBroker) {
		this.datacenterBroker = datacenterBroker;
	}
	
	public void setMigrationDelayer(MigrationDelayer migrationDelayer) {
		this.migrationDelayer = migrationDelayer;
	}
	
	private void updateMigrationDelayer() {
		if(this.migrationDelayer != null) {
			this.migrationDelayer.update();
		}
	}
	
	private boolean migrationDelayerAllowsMigrations() {
		return this.migrationDelayer == null || this.migrationDelayer.canMigrate();
	}

	@Override
	protected void updateCloudletProcessing() {
		if (getCloudletSubmitted() == -1 || getCloudletSubmitted() == CloudSim.clock()) {
			CloudSim.cancelAll(getId(), new PredicateType(CloudSimTags.VM_DATACENTER_EVENT));
			schedule(getId(), getSchedulingInterval(), CloudSimTags.VM_DATACENTER_EVENT);
			return;
		}
		double currentTime = CloudSim.clock();
		
		// SERGI
		//System.out.println(currentTime);
		
		
		// if some time passed since last processing
		if (currentTime > getLastProcessTime()) {
			//System.out.print(currentTime + " ");
			//System.out.println("------------updateCloudletProcessing");
			//System.out.println(slotNum);
			Map<Host, Integer> hostVmCount = new HashMap<>();
			/*for(Vm vm : getVmList()) {
				System.out.println("VM " + vm.getId() + " to host " + vm.getHost().getId());
			}*/
			
			/*for(Vm vm : getVmList()) {
				Host host = vm.getHost();
				if(!hostVmCount.containsKey(host)) {
					hostVmCount.put(host, 0);
				}
				hostVmCount.put(host, hostVmCount.get(host) + 1);
			}*/
				
			/*for(Entry<Host, Integer> entry : hostVmCount.entrySet()) {
				System.out.println(entry.getKey().getId() + " " + entry.getValue());
			}*/
		
			SvilaPrinter.print("Num VMs: " + this.getVmList().size());
			SvilaPrinter.print("slotNum: " + slotNum);
			SvilaPrinter.print("Time: " + currentTime);

			//if(slotNum == 0) {
				//VMLoadBalancer.init(this.getVmList(), this.datacenterBroker.getCloudletList());
	        	
			//	VMLoadBalancer.init(this.datacenterBroker);
			//	VMLoadBalancer.startVms();
			//}
		
			//slotNum++;
			slotNum = (int)(currentTime - 0.1) / 300;
			
			double minTime = updateCloudetProcessingWithoutSchedulingFutureEventsForce();
			
			// Sergi
			// S'afegeix històric a 0 per les VMs que encara no han estat engegades
//			for(Vm vm : VMLoadBalancer.allVmList) {
//				PowerVm powerVm = (PowerVm) vm;
//				if(vm.getStateHistory().size() < slotNum+1) {
//					powerVm.addUtilizationHistoryValue(0.0);
//					powerVm.addExternalBWtilizationHistoryValue(0.0);
//					powerVm.addStateHistoryEntry(currentTime, 0.0, 0.0, false);
//				}
//				System.out.println(vm.getId() + ": " + vm.getStateHistory().size());
//			}
			
			updateMigrationDelayer();
			
			Snapshot lastSnapshot = snapshots.size() > 1 ? snapshots.get(snapshots.size()-1) : null;
			
			
			Graph baseNetwork = PlanetLabNetworkRunner.getDatacenterBroker().getBaseTopology();
			Graph logicalNetwork = PlanetLabNetworkRunner.getDatacenterBroker().getInteractionsContainer().getLogicalGraphForTime(currentTime, getVmList());
			
			Snapshot snapshot = new Snapshot(
					currentTime,
					Integer.parseInt(String.valueOf(Math.round(currentTime/PlanetLabNetworkConstants.SCHEDULING_INTERVAL))),
					migrationDelayerAllowsMigrations(),
					this.getHostList(),
					baseNetwork,
					logicalNetwork,
					lastSnapshot);
				
			snapshots.add(snapshot);
			//System.out.println(snapshot);
			
			// Write and delete data to avoid memory overflow
			ResultsManager.experimentsResultsStream.update();
			
			
			// Original
			getVmAllocationPolicy().update();

			
			//
			// Pre-calcular forecastings per no haver de calcular cada cop les dades
			// Problema, si hi ha canvis en les VMs, s'haurien de recalcular de nou
			//
			
			//clearCachedData();
			
			// Netejar dades càlculs
			
			
			triggerMigrations(snapshot, currentTime);
			
			// Mogut per obtenir les dades actualitzades després de les migracions
			// Realment ha d'estar abans, la planificació de les migracions no canvia
			// l'estat actual
			//getVmAllocationPolicy().update();

			// Migration results
			MigrationResults.getInstance().calculatePost(slotNum-1);
			NetworkResults.instance.calculateMigrationData(snapshot, slotNum-1);
			NetworkPhysicalResults.instance.calculateMigrationData(snapshot, slotNum-1);


			// schedules an event to the next time
			if (minTime != Double.MAX_VALUE) {
				CloudSim.cancelAll(getId(), new PredicateType(CloudSimTags.VM_DATACENTER_EVENT));
				send(getId(), getSchedulingInterval(), CloudSimTags.VM_DATACENTER_EVENT);
			}

			setLastProcessTime(currentTime);
			
			//VMLoadBalancer.loadNext();
			//List<Vm> nextVms = VMLoadBalancer.getNextVms();
			//List<Cloudlet> nextCloudlets = VMLoadBalancer.getNextCloudlets();
			//System.out.println("Adding " + nextVms.size() + " new VMs");
			//for(Vm vm : nextVms) {
			//	System.out.println("Adding VM net id: " + vm.getNetworkId());
			//}
			//this.datacenterBroker.createNewVmsInDatacenter(0, nextVms);
			//this.datacenterBroker.submitNewCloudlets(nextCloudlets);
			//this.datacenterBroker.submitVmList(nextVms);
        	//this.datacenterBroker.submitCloudletList(nextCloudlets);
			
			
			VMLoadBalancer.loadNext();
			SvilaPrinter.print("Added " + VMLoadBalancer.getNextVms().size() + " vms");
			datacenterBroker.submitVmList(VMLoadBalancer.getNextVms());
			datacenterBroker.submitCloudletList(VMLoadBalancer.getNextCloudlets());
			double sendTime = (slotNum+1)*300.0-0.1;
			SvilaPrinter.print(sendTime);
			datacenterBroker.createVmsInDatacenterInTime(this.getId(), sendTime);			
		}
	}
	
	private void triggerMigrations(Snapshot snapshot, double currentTime) {

		if (!isDisableMigrations() && migrationDelayerAllowsMigrations()) {
			List<Map<String, Object>> migrationMap = getVmAllocationPolicy().optimizeAllocation(
					getVmList());

			if (migrationMap != null) {
				snapshot.setMigrationMap(migrationMap);
				getVmAllocationPolicy().updateTriggeredMigrations(migrationMap);
				for (Map<String, Object> migrate : migrationMap) {
					Vm vm = (Vm) migrate.get("vm");
					PowerHost targetHost = (PowerHost) migrate.get("host");
					PowerHost oldHost = (PowerHost) vm.getHost();

					if (oldHost == null) {
						Log.formatLine(
								"%.2f: Migration of VM #%d to Host #%d is started",
								currentTime,
								vm.getId(),
								targetHost.getId());
					} else {
						Log.formatLine(
								"%.2f: Migration of VM #%d from Host #%d to Host #%d is started",
								currentTime,
								vm.getId(),
								oldHost.getId(),
								targetHost.getId());
					}

					//targetHost.addMigratingInVm(vm);
					//incrementMigrationCount();

					/** VM migration delay = RAM / bandwidth **/
					// we use BW / 2 to model BW available for migration purposes, the other
					// half of BW is for VM communication
					// around 16 seconds for 1024 MB using 1 Gbit/s network
					
					// ORIGINAL
					//send(
					//		getId(),
					//		vm.getRam() / ((double) targetHost.getBw() / (2 * 8000)),
					//		CloudSimTags.VM_MIGRATE,
					//		migrate);
					
					// EDITED, NO DELAY
					//send(
					//		getId(),
					//		0.0,
					//		CloudSimTags.VM_MIGRATE,
					//		migrate);
					
					// NEW Correct until now
					/*send(
							getId(),
							vm.getRam() * 0.016,
							CloudSimTags.VM_MIGRATE,
							migrate);
					*/
					
					// Migrations to opened hosts
					if(targetHost.getVmList().size() > 0) {
						targetHost.addMigratingInVm(vm);
						incrementMigrationCount();
						send(
								getId(),
								vm.getRam() * 0.016,
								CloudSimTags.VM_MIGRATE,
								migrate);
					} else { // Migrations to closed hosts
						send(
								getId(),
								154.2,
								CloudSimTags.DELAYED_MIGRATION,
								migrate);
					}
					
					// PROPOSED
					//send(
					//		getId(),
					//		vm.getRam() / ((double) 1000 / (2 * 8000))/1000,
					//		CloudSimTags.VM_MIGRATE,
					//		migrate);
				}
			}
		}
	}
	
	private void clearCachedData() {
		for(Vm vm : this.getVmList()) {
			vm.getVMCalculator().getVmCache().clearCache();
		}
		
		for(Host host : this.getHostList()) {
			
		}
	}

	/**
	 * Update cloudet processing without scheduling future events.
	 * 
	 * @return the double
         * @see #updateCloudetProcessingWithoutSchedulingFutureEventsForce() 
         * @todo There is an inconsistence in the return value of this
         * method with return value of similar methods
         * such as {@link #updateCloudetProcessingWithoutSchedulingFutureEventsForce()},
         * that returns {@link Double#MAX_VALUE} by default.
         * The current method returns 0 by default.
	 */
	protected double updateCloudetProcessingWithoutSchedulingFutureEvents() {
		if (CloudSim.clock() > getLastProcessTime()) {
			return updateCloudetProcessingWithoutSchedulingFutureEventsForce();
		}
		return 0;
	}

	/**
	 * Update cloudet processing without scheduling future events.
	 * 
	 * @return expected time of completion of the next cloudlet in all VMs of all hosts or
	 *         {@link Double#MAX_VALUE} if there is no future events expected in this host
	 */
	protected double updateCloudetProcessingWithoutSchedulingFutureEventsForce() {
		double currentTime = CloudSim.clock();
		double minTime = Double.MAX_VALUE;
		double timeDiff = currentTime - getLastProcessTime();
		double timeFrameDatacenterEnergy = 0.0;
		
		int currentStep = (int) Math.ceil((currentTime-0.1) / 300.0) - 1;
			// It is deleted 0.1 because the currentTime corresponding to an snapshot
			// must be the last register inside the 300 seconds, 300.1 is outside

		Log.printLine("\n\n--------------------------------------------------------------\n\n");
		Log.formatLine("New resource usage for the time frame starting at %.2f:", currentTime);

		for (PowerHost host : this.<PowerHost> getHostList()) {
			Log.printLine();

			double time = host.updateVmsProcessing(currentTime); // inform VMs to update processing
			if (time < minTime) {
				minTime = time;
			}

			Log.formatLine(
					"%.2f: [Host #%d] utilization is %.2f%%",
					currentTime,
					host.getId(),
					host.getUtilizationOfCpu() * 100);
		}

		if (timeDiff > 0) {
			Log.formatLine(
					"\nEnergy consumption for the last time frame from %.2f to %.2f:",
					getLastProcessTime(),
					currentTime);

			for (PowerHost host : this.<PowerHost> getHostList()) {
				double previousUtilizationOfCpu = host.getPreviousUtilizationOfCpu();
				double utilizationOfCpu = host.getUtilizationOfCpu();
				double timeFrameHostEnergy = host.getEnergyLinearInterpolation(
						previousUtilizationOfCpu,
						utilizationOfCpu,
						timeDiff);
				timeFrameDatacenterEnergy += timeFrameHostEnergy;
				
				EnergyContainer.addHostValue(currentStep, host.getNetworkId(), timeFrameHostEnergy);
				EnergyContainer.addHostValue(currentTime, host.getNetworkId(), timeFrameHostEnergy);
				
				Log.printLine();
				Log.formatLine(
						"%.2f: [Host #%d] utilization at %.2f was %.2f%%, now is %.2f%%",
						currentTime,
						host.getId(),
						getLastProcessTime(),
						previousUtilizationOfCpu * 100,
						utilizationOfCpu * 100);
				Log.formatLine(
						"%.2f: [Host #%d] energy is %.2f W*sec",
						currentTime,
						host.getId(),
						timeFrameHostEnergy);
			}

			Log.formatLine(
					"\n%.2f: Data center's energy is %.2f W*sec\n",
					currentTime,
					timeFrameDatacenterEnergy);
		}

		setPower(getPower() + timeFrameDatacenterEnergy);

		
		EnergyContainer.addSnapshotValue(currentStep, timeFrameDatacenterEnergy);
		EnergyContainer.addSnapshotValue(currentTime, timeFrameDatacenterEnergy);

		
		checkCloudletCompletion();

		/** Remove completed VMs **/
		for (PowerHost host : this.<PowerHost> getHostList()) {
			for (Vm vm : host.getCompletedVms()) {
				getVmAllocationPolicy().deallocateHostForVm(vm);
				getVmList().remove(vm);
				Log.printLine("VM #" + vm.getId() + " has been deallocated from host #" + host.getId());
			}
		}

                StaticLog.showHostInfo(this.<PowerHost> getHostList());
                JSONOutput.addPowerDatacenterState(this);
                
		Log.printLine();

		setLastProcessTime(currentTime);
		return minTime;
	}

	@Override
	protected void processVmMigrate(SimEvent ev, boolean ack) {
		updateCloudetProcessingWithoutSchedulingFutureEvents();
		super.processVmMigrate(ev, ack);
		SimEvent event = CloudSim.findFirstDeferred(getId(), new PredicateType(CloudSimTags.VM_MIGRATE));
		if (event == null || event.eventTime() > CloudSim.clock()) {
			updateCloudetProcessingWithoutSchedulingFutureEventsForce();
		}
	}

	@Override
	protected void processCloudletSubmit(SimEvent ev, boolean ack) {
		super.processCloudletSubmit(ev, ack);
		setCloudletSubmitted(CloudSim.clock());
	}
	
	// Sergi
	@Override
	protected void processDelayedMigration(SimEvent ev) {
		updateCloudetProcessingWithoutSchedulingFutureEvents();
		Object tmp = ev.getData();
		if (!(tmp instanceof Map<?, ?>)) {
			throw new ClassCastException("The data object must be Map<String, Object>");
		}

		@SuppressWarnings("unchecked")
		Map<String, Object> migrate = (HashMap<String, Object>) tmp;

		Vm vm = (Vm) migrate.get("vm");
		Host targetHost = (Host) migrate.get("host");
			
		targetHost.addMigratingInVm(vm);
		incrementMigrationCount();
		incrementDelayedMigrationCount();

		send(
				getId(),
				vm.getRam() * 0.016,
				CloudSimTags.VM_MIGRATE,
				migrate);
	}

	/**
	 * Gets the power.
	 * 
	 * @return the power
	 */
	public double getPower() {
		return power;
	}

	/**
	 * Sets the power.
	 * 
	 * @param power the new power
	 */
	protected void setPower(double power) {
		this.power = power;
	}

	/**
	 * Checks if PowerDatacenter is in migration.
	 * 
	 * @return true, if PowerDatacenter is in migration; false otherwise
	 */
	protected boolean isInMigration() {
		boolean result = false;
		for (Vm vm : getVmList()) {
			if (vm.isInMigration()) {
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * Checks if migrations are disabled.
	 * 
	 * @return true, if  migrations are disable; false otherwise
	 */
	public boolean isDisableMigrations() {
		return disableMigrations;
	}

	/**
	 * Disable or enable migrations.
	 * 
	 * @param disableMigrations true to disable migrations; false to enable
	 */
	public void setDisableMigrations(boolean disableMigrations) {
		this.disableMigrations = disableMigrations;
	}

	/**
	 * Checks if is cloudlet submited.
	 * 
	 * @return true, if is cloudlet submited
	 */
	protected double getCloudletSubmitted() {
		return cloudletSubmitted;
	}

	/**
	 * Sets the cloudlet submitted.
	 * 
	 * @param cloudletSubmitted the new cloudlet submited
	 */
	protected void setCloudletSubmitted(double cloudletSubmitted) {
		this.cloudletSubmitted = cloudletSubmitted;
	}

	/**
	 * Gets the migration count.
	 * 
	 * @return the migration count
	 */
	public int getMigrationCount() {
		return migrationCount;
	}

	/**
	 * Sets the migration count.
	 * 
	 * @param migrationCount the new migration count
	 */
	protected void setMigrationCount(int migrationCount) {
		this.migrationCount = migrationCount;
	}

	/**
	 * Increment migration count.
	 */
	public void incrementMigrationCount() {
		setMigrationCount(getMigrationCount() + 1);
	}

	public int getDelayedMigrationCount() {
		return delayedMigrationCount;
	}

	public void setDelayedMigrationCount(int delayedMigrationCount) {
		this.delayedMigrationCount = delayedMigrationCount;
	}
	
	public void incrementDelayedMigrationCount() {
		setDelayedMigrationCount(getDelayedMigrationCount() + 1);
	}

}
