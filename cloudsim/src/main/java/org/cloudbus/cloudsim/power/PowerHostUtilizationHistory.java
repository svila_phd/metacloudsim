/*
 * Title:        CloudSim Toolkit
 * Description:  CloudSim (Cloud Simulation) Toolkit for Modeling and Simulation of Clouds
 * Licence:      GPL - http://www.gnu.org/copyleft/gpl.html
 *
 * Copyright (c) 2009-2012, The University of Melbourne, Australia
 */

package org.cloudbus.cloudsim.power;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.VmScheduler;
import org.cloudbus.cloudsim.power.PowerVm.ResourceType;
import org.cloudbus.cloudsim.power.models.PowerModel;
import org.cloudbus.cloudsim.provisioners.BwProvisioner;
import org.cloudbus.cloudsim.provisioners.RamProvisioner;
import org.cloudbus.cloudsim.util.MathUtil;

import svila.Tools;
import svila.metricsLogic.HostCache;
import svila.metricsLogic.CPUCalculator.HostCalculator;

/**
 * A host that stores its CPU utilization percentage history. The history is used by VM allocation
 * and selection policies.
 * 
 * <br/>If you are using any algorithms, policies or workload included in the power package please cite
 * the following paper:<br/>
 * 
 * <ul>
 * <li><a href="http://dx.doi.org/10.1002/cpe.1867">Anton Beloglazov, and Rajkumar Buyya, "Optimal Online Deterministic Algorithms and Adaptive
 * Heuristics for Energy and Performance Efficient Dynamic Consolidation of Virtual Machines in
 * Cloud Data Centers", Concurrency and Computation: Practice and Experience (CCPE), Volume 24,
 * Issue 13, Pages: 1397-1420, John Wiley & Sons, Ltd, New York, USA, 2012</a>
 * </ul>
 * 
 * @author Anton Beloglazov
 * @since CloudSim Toolkit 2.0
 */
public class PowerHostUtilizationHistory extends PowerHost {

	// SERGI
	public enum ResourceType {CPU, EXTERNAL_BW};
	private ResourceType currentResourceType;
	
	HostCalculator hostCalculator;
	HostCache hostCache;
	private boolean isBlocked;
	

	/**
	 * Instantiates a new PowerHostUtilizationHistory.
	 * 
	 * @param id the host id
	 * @param ramProvisioner the ram provisioner
	 * @param bwProvisioner the bw provisioner
	 * @param storage the storage capacity
	 * @param peList the host's PEs list
	 * @param vmScheduler the vm scheduler
	 * @param powerModel the power consumption model
	 */
	public PowerHostUtilizationHistory(
			int id,
			RamProvisioner ramProvisioner,
			BwProvisioner bwProvisioner,
			long storage,
			List<? extends Pe> peList,
			VmScheduler vmScheduler,
			PowerModel powerModel) {
		super(id, ramProvisioner, bwProvisioner, storage, peList, vmScheduler, powerModel);
		this.setCPUUtilizationHistory();
		this.isBlocked = false;
	}
	
	// Sergi
	public PowerHostUtilizationHistory(
			int id,
			RamProvisioner ramProvisioner,
			BwProvisioner bwProvisioner,
			long storage,
			List<? extends Pe> peList,
			VmScheduler vmScheduler,
			PowerModel powerModel, double energyOn, double energyOff) {
		super(id, ramProvisioner, bwProvisioner, storage, peList, vmScheduler, powerModel);
		this.setCPUUtilizationHistory();
		this.isBlocked = false;
		// Sergi
		this.setEnergyOn(energyOn);
		this.setEnergyOff(energyOff);
	}
	
	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	
	public HostCalculator getHostCalculator() {
		return hostCalculator;
	}

	public void setHostCalculator(HostCalculator hostCalculator) {
		this.hostCalculator = hostCalculator;
	}
	
	public HostCache getHostCache() {
		return hostCache;
	}

	public void setHostCache(HostCache hostCache) {
		this.hostCache = hostCache;
	}

		//SERGI		
		public void setCPUUtilizationHistory() {
			this.currentResourceType = ResourceType.CPU;
		}
		
		public void setExternalBWUtilizationHistory() {
			this.currentResourceType = ResourceType.EXTERNAL_BW;
		}
		
		public void setResourceUtilizationHistory(ResourceType resourceType) {
			switch(resourceType) {
			case EXTERNAL_BW:
				setExternalBWUtilizationHistory();
			case CPU:
			default:
				setCPUUtilizationHistory();
			}
		}
		
		public ResourceType getCurrentResourceType() {
			return this.currentResourceType;
		}
	
	/**
	 * Gets the host CPU utilization percentage history.
	 * 
	 * @return the host CPU utilization percentage history
	 */
	public double[] getUtilizationHistory() {
		switch(this.currentResourceType) {
		case EXTERNAL_BW:
			return this.getExternalBWUtilizationHistory();
		case CPU:
		default:
			return this.getCPUUtilizationHistory();
		}
	}

	/**
	 * Gets the host CPU utilization percentage history.
	 * 
	 * @return the host CPU utilization percentage history
	 */
	public double[] getCPUUtilizationHistory() {
		double[] utilizationHistory = new double[PowerVm.HISTORY_LENGTH];
		double hostMips = getTotalMips();
		for (PowerVm vm : this.<PowerVm> getVmList()) {
			for (int i = 0; i < vm.getUtilizationHistory().size(); i++) {
				utilizationHistory[i] += vm.getUtilizationHistory().get(i) * vm.getMips() / hostMips;
			}
		}
		double[] result = MathUtil.trimZeroTail(utilizationHistory);
		//this.hostCache.putArray(HostCache.CPU_utilization_history, result);
		return result;
	}
	
	// Sergi
	public double[] getCPUUtilizationHistory0NewNOld() {
		double[] result = getCPUUtilizationHistory();
	    //System.out.println("Original Array: " + Arrays.toString(result)); 
		//Collections.reverse(Arrays.asList(result)); // Totally incorrect
		Tools.reverseArray(result);
	    //System.out.println("Reversed Array: " + Arrays.toString(result)); 

		return result;
	}
	
	public double[] getCPUMIPSHistory() {
		double[] utilizationHistory = new double[PowerVm.HISTORY_LENGTH];
		for (PowerVm vm : this.<PowerVm> getVmList()) {
			for (int i = 0; i < vm.getUtilizationHistory().size(); i++) {
				utilizationHistory[i] += vm.getUtilizationHistory().get(i) * vm.getMips();
			}
		}
		return MathUtil.trimZeroTail(utilizationHistory);
	}
	
	// SERGI
	// NOT READY, DO NOT USE
	/**
	 * Gets the host externalBW utilization percentage history.
	 * 
	 * @return the host externalBW utilization percentage history
	 */
	public double[] getExternalBWUtilizationHistory() {
		double[] utilizationHistory = new double[PowerVm.HISTORY_LENGTH];
		for (PowerVm vm : this.<PowerVm> getVmList()) {
			for (int i = 0; i < vm.getUtilizationHistory().size(); i++) {
				utilizationHistory[i] += vm.getUtilizationHistory().get(i);
			}
		}
		return MathUtil.trimZeroTail(utilizationHistory);
	}
}
