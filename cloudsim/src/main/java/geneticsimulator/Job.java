package geneticsimulator;

import java.util.HashMap;

import geneticsimulator.Job;

public class Job {
	
	private int id;
	private int requirements;
	private double time_base;
	private double sigma;
	private double ppbw;
	private int finishTime;
	private double startTime;
	private int[] preAssignation;
	private int coAssignationNumber;
	private HashMap<Integer,Integer> allocation;
	
	/** Instantiates a job with the specified parameters
	 * 
	 * @param id the ID of the job
	 * @param requirements the requirements of the job
	 * @param time_base the time this job spends as a base
	 * @param sigma the proportion between communication and processing
	 * @param ppbw the bandwidth consumed by the process
	 */
	public Job(int id,int requirements,double time_base, double sigma,double ppbw){
		this.id=id;
		this.requirements=requirements;
		this.time_base=time_base;
		this.sigma=sigma;
		this.ppbw=ppbw;
		this.allocation=new HashMap<Integer,Integer>();
	}
	/** Instantiates a new job copying the data from another
	 * 
	 * @param job the Job to copy
	 */
	public Job(Job job){
		this.id=job.id;
		this.requirements=job.requirements;
		this.time_base=job.time_base;
		this.sigma=job.sigma;
		this.ppbw=job.ppbw;
		this.allocation=new HashMap<Integer,Integer>();
	}
	
	/** Clones a job **/
	public Job clone(){
		Job j=new Job(this);
		return j;
	}
	/**
	 * Obtains the id of the Job
	 * @return The id of this Job
	 */
	public int getId(){
		return id;
	}
	/** Obtains the requirements of the job
	 * 
	 * @return The requirements of the job
	 */
	public int getRequirements(){
		return requirements;
	}
	/** Obtains the time base of the job
	 * 
	 * @return the time base of the job
	 */
	public double getTimeBase(){
		return time_base;
	}
	/** Obtains the sigma of the Job: The proportion between communication and processing
	 * 
	 * @return the sigma value of the Job
	 */
	public double getSigma(){
		return sigma;
	}
	/** Obtain the allocation of this job
	 * 
	 * @return the allocation of the Job
	 */
	public HashMap<Integer,Integer> getAllocation(){
		return this.allocation;
	}
	/** Allocates the job to the cluster specified
	 * 
	 * @param clusterid the id to allocate the cluster
	 * @param numNodes the number of nodes to use 
	 */
	public void allocate(Integer clusterid, Integer numNodes){
		this.allocation.put(clusterid, numNodes);
	}
	/** get the bandwidth consumed by the job
	 * 
	 * @return the bandwidth consumed
	 */
	public double getPpbw(){
		return this.ppbw;
	}
	/** set the execution finish time of this job
	 * 
	 * @param finishTime the finish time of the job
	 */
	public void setFinishTime(int finishTime){
		
		this.finishTime=finishTime;
	}
	/** get the execution finish time of this job
	 * 
	 * @return the execution finish time
	 */
	public double getFinishTime(){
		return this.finishTime;
	}
	
	/** Set the starting time of this Job
	 * 
	 * @param startTime the execution starting time
	 */
	public void setStartTime(double startTime){
		this.startTime=startTime;
	}
	
	/** Get the execution starting time of this Job
	 * 
	 * @return the execution starting time
	 */
	public double getStartTime(){
		return this.startTime;
	}
	
	
	/** Set a preallocation of this job to nodes
	 * 
	 * @param preAssign the preallocation to set
	 */
	public void setPreAssign(int[] preAssign){
		this.preAssignation=preAssign;
	}
	/** Get the number of co-assignated clusters
	 * 
	 * @return the number of clusters used for co-allocation
	 */
	public int getCoAssignationNumber(){
		return this.coAssignationNumber;
	}
	/** Get the preAllocation of this job to nodes
	 * 
	 * @return the preAllocation of the Job
	 */
	public int[] getPreAssign(){
		if(preAssignation!=null){
			int[] preAlloc=new int[coAssignationNumber+1];
			for(int i=0;i<coAssignationNumber+1;i++){
				preAlloc[i]=preAssignation[i];
			}
			return preAlloc;
		}
		return null;
	}
	/** Set the co-allocation number of clusters 
	 * 
	 * @param co The number of co-allocated clusters
	 */
	public void setCoAssignationNumber(int co){
		this.coAssignationNumber=co;
	}
	
	/** (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String r= new String("Job: " + id +" "+requirements+" "+time_base+" "+sigma);
		if(allocation!=null){
			for(int i:allocation.keySet()){
				r+=" "+i;
			}
		}
		return r;
	}
	/** Reset the job to the initial situation
	 * 
	 */
	public void reset(){
		this.startTime=0;
		this.finishTime=0;
		this.allocation=new HashMap<Integer,Integer>();
		this.coAssignationNumber=0;
		this.preAssignation=null;
	}
}
