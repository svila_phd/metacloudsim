package geneticsimulator;

import java.util.ArrayList;

import geneticsimulator.Job;


public class JobList {
	private static boolean seting=false;
	private static JobList instance;
	private ArrayList<Job> jobList;
	
	
	/** Set an instance of all jobs to allocate
	 * 
	 * @param jobs the jobs to allocate
	 */
	public static void setInstance(ArrayList<Job>jobs){
		instance=new JobList(jobs);
		seting=true;
	}
	/** Obtain the list of all jobs
	 * 
	 * @return The current list of jobs
	 * @throws IllegalStateException
	 */
	public static JobList instance() throws IllegalStateException{
		if(seting==false){
			throw new IllegalStateException();
		}
		return instance;
	}
	
	/** Create the list of jobs**/
	private JobList(ArrayList<Job> jobs){
		jobList=new ArrayList<Job>();
		jobList.addAll(jobs);
	}
	/** Obtain the number of jobs in the queue
	 * 
	 * @return the number of jobs in the queue
	 */
	public int size(){
		return jobList.size();
	}
	/** Get the job in the specified position
	 * 
	 * @param pos The position to obtain the job
	 * @return the Job at the specified position
	 */
	public Job get(int pos){
		return jobList.get(pos);
	}
	/** (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String r=new String("");
		for(Job j:jobList){
			r+=j+"|";
		}
		return r;
	}
	/** Resets all the jobs in the list
	 * 
	 */
	public void reset(){
		for(Job j:jobList){
			j.reset();
		}
	}
}
