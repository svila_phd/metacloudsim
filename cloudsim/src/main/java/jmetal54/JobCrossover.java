package jmetal54;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.uma.jmetal54.operator.CrossoverOperator;
import org.uma.jmetal54.util.pseudorandom.JMetalRandom;

public class JobCrossover implements org.uma.jmetal54.operator.CrossoverOperator<SchedulingSolution> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3230382680659426448L;
	/**
	 * 
	 */
	private JMetalRandom randomGenerator ;
	double probability;

	  /** Constructor */
	  public JobCrossover() {
	    randomGenerator = JMetalRandom.getInstance() ;
	    this.probability = 50;
	  }
	  
	  public JobCrossover(double probability) {
		    randomGenerator = JMetalRandom.getInstance() ;
		    this.probability = probability;
		  }

	public List<SchedulingSolution> execute(List<SchedulingSolution> source) {
		
		int prov = randomGenerator.nextInt(0, 100);
		CrossoverOperator<SchedulingSolution>crossover;
		if(prov<probability){
			crossover=new JobAllocCrossover();
		}else{
			crossover = new JobOrderCrossover();
		}
		List<SchedulingSolution> solutions=crossover.execute(source);
		for(SchedulingSolution s : solutions){
			s.repairSolution();
		}
		return solutions;
		
	}

	@Override
	public int getNumberOfRequiredParents() {
		return 2;
	}

	@Override
	public int getNumberOfGeneratedChildren() {
		return 2;
	}
}

