package jmetal54;


import geneticsimulator.Job;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.uma.jmetal54.solution.PermutationSolution;
import org.uma.jmetal54.solution.Solution;
import org.uma.jmetal54.solution.impl.AbstractGenericSolution;



public class SchedulingSolution extends AbstractGenericSolution<JobData,SchedulingProblem> implements PermutationSolution<JobData>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1566646965472012015L;
	Random r = new Random();

	public SchedulingSolution(SchedulingProblem problem) {
		super(problem);

	    //overallConstraintViolationDegree = 0.0 ;
	    //numberOfViolatedConstraints = 0 ;
	    List<JobData> randomSequence = new ArrayList<JobData>(problem.getPermutationLength());

	    for (int j = 0; j < problem.getPermutationLength(); j++) {
	    	randomSequence.add(initializeJobData(problem.getJobList().get(j)));
	    }

	    java.util.Collections.shuffle(randomSequence);

	    for (int i = 0; i < getNumberOfVariables(); i++) {
	      setVariableValue(i, randomSequence.get(i)) ;
	    }
	    repairSolution();
	}
	
	public SchedulingSolution(SchedulingProblem problem,boolean diff) {
		super(problem);

	    //overallConstraintViolationDegree = 0.0 ;
	    //numberOfViolatedConstraints = 0 ;
	    List<JobData> randomSequence = new ArrayList<JobData>(problem.getPermutationLength());

	    for (int j = 0; j < problem.getPermutationLength(); j++) {
	    	randomSequence.add(initializeJobData(problem.getJobList().get(j)));
	    }

	    for (int i = 0; i < getNumberOfVariables(); i++) {
	      setVariableValue(i, randomSequence.get(i)) ;
	    }
	    repairSolution();
	}
	
	public SchedulingSolution(SchedulingSolution sol){
		super(sol.problem);
		//overallConstraintViolationDegree = 0.0 ;
	    //numberOfViolatedConstraints = 0 ;
	    for (int i = 0; i < getNumberOfVariables(); i++) {
	    	JobData prev=sol.getVariableValue(i);
	    	setVariableValue(i, new JobData(prev)) ;
		}
	    repairSolution();
	}

	public String getVariableValueString(int index) {
		JobData variable=this.getVariableValue(index);
		String solution="Job "+variable.job.getId()+" {";
		for(int i=0;i<variable.forbidenNodes.length;i++){
			solution+=variable.forbidenNodes[i]+",";
		}
		return solution+"}";
	}

	public Solution<JobData> copy() {
		return new SchedulingSolution(this);
	}
	
	public JobData initializeJobData(Job j){
		JobData n=new JobData(j,problem.getMultiClusterLength());
		for(int i=0;i<n.forbidenNodes.length;i++){
			n.forbidenNodes[i]=this.randomGenerator.nextDouble(0d, 1d);
		}
		return n;
	}
	
	public void repairSolution(){
		for(int i=0;i<this.getNumberOfVariables();i++){
			JobData j=this.getVariableValue(i);
			boolean valid=false;
			while(!valid){
				int availableNodes=0;
				for(int c=0;c<j.forbidenNodes.length;c++){
					if(j.forbidenNodes[c]>1){j.forbidenNodes[c]=1;}
					if(j.forbidenNodes[c]<0){j.forbidenNodes[c]=0;}
				}
				for(int c=0;c<j.forbidenNodes.length;c++){
					int numNodesc=this.problem.mc.getClusterInPos(c).getMaxResources();
					availableNodes+=(int) (numNodesc*(1-j.forbidenNodes[c]));
				}
				if(availableNodes>=j.job.getRequirements()){
					valid=true;
				}else{
					ArrayList<Integer>cid = new ArrayList<Integer>();
					for(int c=0;c<j.forbidenNodes.length;c++){
						if(j.forbidenNodes[c]>0){
							cid.add(c);
						}
					}
					int ctemp=0;
					if(cid.size()>0){
						ctemp=r.nextInt(cid.size());
					}
					j.forbidenNodes[cid.get(ctemp)]-=0.1;
				}
			}
		}
	}
	
}

