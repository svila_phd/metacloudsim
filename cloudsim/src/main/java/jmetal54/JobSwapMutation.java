package jmetal54;

import org.uma.jmetal54.util.pseudorandom.JMetalRandom;

public class JobSwapMutation implements org.uma.jmetal54.operator.MutationOperator<SchedulingSolution> {

	double probability;
	int numberOfMutations;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6311197190464734663L;

	private JMetalRandom randomGenerator ;

	  /** Constructor */
	public JobSwapMutation() {
		randomGenerator = JMetalRandom.getInstance();
	    this.probability = 5;
	    this.numberOfMutations = 1;
	}
	
	public JobSwapMutation(double probability, int numberOfMutations) {
	    randomGenerator = JMetalRandom.getInstance();
	    this.probability = probability;
	    this.numberOfMutations = numberOfMutations;
	}
	
	public SchedulingSolution execute(SchedulingSolution source) {
		int prob1 = randomGenerator.nextInt(0, 100);
		
		SchedulingSolution solution = new SchedulingSolution(source);
		
		for(int mutation=0; mutation<this.numberOfMutations; mutation++) {
			if(prob1<probability){
				solution = new SchedulingSolution(source);
				int ptype = randomGenerator.nextInt(0, 100);
				if(ptype<50){
					int s1=randomGenerator.nextInt(0, source.getNumberOfVariables()-1);
					int s2=randomGenerator.nextInt(0, source.getNumberOfVariables()-1);
					JobData temp = solution.getVariableValue(s1);
					solution.setVariableValue(s1, source.getVariableValue(s2));
					solution.setVariableValue(s2, temp);
					solution.repairSolution();
				}else{
					for(int i=0;i<source.getNumberOfVariables();i++){
						int pmut=randomGenerator.nextInt(0, 100);
						if(pmut>50){
							JobData job=solution.getVariableValue(i);
							for(int j=0;j<job.forbidenNodes.length;j++){
								job.forbidenNodes[j]=randomGenerator.nextDouble(0, 1);
							}	
							solution.setVariableValue(i, job);
						}
					}
					solution.repairSolution();
				}
			}
		}
		
		return solution;
		/*int prob1 = randomGenerator.nextInt(0, 100);
		if(prob1<probability){
			SchedulingSolution solution = new SchedulingSolution(source);
			int ptype = randomGenerator.nextInt(0, 100);
			if(ptype<50){
				int s1=randomGenerator.nextInt(0, source.getNumberOfVariables()-1);
				int s2=randomGenerator.nextInt(0, source.getNumberOfVariables()-1);
				JobData temp = solution.getVariableValue(s1);
				solution.setVariableValue(s1, source.getVariableValue(s2));
				solution.setVariableValue(s2, temp);
				solution.repairSolution();
			}else{
				for(int i=0;i<source.getNumberOfVariables();i++){
					int pmut=randomGenerator.nextInt(0, 100);
					if(pmut>50){
						JobData job=solution.getVariableValue(i);
						for(int j=0;j<job.forbidenNodes.length;j++){
							job.forbidenNodes[j]=randomGenerator.nextDouble(0, 1);
						}	
						solution.setVariableValue(i, job);
					}
				}
				solution.repairSolution();
			}
			return solution;
		}else{
			return source;
		}*/
	}
}
