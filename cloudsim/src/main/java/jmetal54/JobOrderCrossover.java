package jmetal54;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.uma.jmetal54.operator.CrossoverOperator;
import org.uma.jmetal54.util.pseudorandom.JMetalRandom;

public class JobOrderCrossover implements CrossoverOperator<SchedulingSolution> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3230382680659426448L;
	/**
	 * 
	 */
	private JMetalRandom randomGenerator ;

	  /** Constructor */
	  public JobOrderCrossover() {
	    randomGenerator = JMetalRandom.getInstance() ;
	  }

	public List<SchedulingSolution> execute(List<SchedulingSolution> source) {
		SchedulingSolution parent1 = source.get(0);
		SchedulingSolution parent2 = source.get(1);
		SchedulingSolution child1 = new SchedulingSolution(parent1);
		SchedulingSolution child2 = new SchedulingSolution(parent2);
		
		ArrayList<SchedulingSolution> solutions = new ArrayList<SchedulingSolution>();
		int[] mask = new int[parent1.getNumberOfVariables()];
		
		for(int i=0;i<mask.length;i++){
			mask[i]=randomGenerator.nextInt(0, 1);
		}
		ArrayList<Integer> missingJobs1= new ArrayList<Integer>();
		ArrayList<Integer> missingJobs2= new ArrayList<Integer>();
		
		for(int i=0;i<mask.length;i++){
			if(mask[i]==0){
				JobData jobParent1=parent1.getVariableValue(i);
				JobData jobParent2=parent2.getVariableValue(i);
				for(int j=0;j<mask.length;j++){
					JobData temp1=parent2.getVariableValue(j);
					JobData temp2=parent1.getVariableValue(j);
					if(jobParent1.equals(temp1)){
						missingJobs1.add(j);
					}
					if(jobParent2.equals(temp2)){
						missingJobs2.add(j);
					}
				}
			}
		}
		Collections.sort(missingJobs1);
		Collections.sort(missingJobs2);
		
		for(int i=0,j=0;i<mask.length;i++){
			if(mask[i]==0){
				child1.setVariableValue(i,parent2.getVariableValue(missingJobs1.get(j)));
				child2.setVariableValue(i,parent1.getVariableValue(missingJobs2.get(j)));
				j++;
			}
		}
		solutions.add(child1);
		solutions.add(child2);
		return solutions;
	}

	@Override
	public int getNumberOfRequiredParents() {
		return 2;
	}

	@Override
	public int getNumberOfGeneratedChildren() {
		return 2;
	}
}
