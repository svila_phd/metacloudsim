package jmetal54;

import geneticsimulator.Job;

public class JobData {
	public Job job;
	public double[] forbidenNodes;

	public JobData(Job j,int mcSize){
		this.job=j;
		this.forbidenNodes=new double[mcSize];
	}
	public JobData(JobData j){
		this.job=new Job(j.job);
		this.forbidenNodes=new double[j.forbidenNodes.length];
		for(int i=0;i<this.forbidenNodes.length;i++){
			this.forbidenNodes[i]=j.forbidenNodes[i];
		}
	}
	
	boolean equals(JobData p2){
		return this.job.getId()==p2.job.getId();
	}
	
	public String toString(){
		String ret=job.getId()+" {";
		for(int i=0;i<this.forbidenNodes.length;i++){
			ret += this.forbidenNodes[i];
			if(i<this.forbidenNodes.length-1){
				ret +=",";
			}
		}
		ret +="}";
		return ret;
	}
}

