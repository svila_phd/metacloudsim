package genetic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

public class JobAllocCrossover implements CrossoverOperator<SchedulingSolution> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3230382680659426448L;
	/**
	 * 
	 */
	private JMetalRandom randomGenerator ;

	  /** Constructor */
	  public JobAllocCrossover() {
	    randomGenerator = JMetalRandom.getInstance() ;
	  }

	public List<SchedulingSolution> execute(List<SchedulingSolution> source) {
		SchedulingSolution parent1 = source.get(0);
		SchedulingSolution parent2 = source.get(1);
		SchedulingSolution child1 = new SchedulingSolution(parent1);
		SchedulingSolution child2 = new SchedulingSolution(parent2);
		ArrayList<SchedulingSolution> solutions = new ArrayList<SchedulingSolution>();
		double cross=randomGenerator.nextDouble(-0.2, 1.2);
		
		for(int i=0;i<parent1.getNumberOfVariables();i++){
			JobData parent1Data=parent1.getVariableValue(i);
			for(int j=0;j<parent2.getNumberOfVariables();j++){
				JobData parent2Data=parent2.getVariableValue(j);
				if(parent1Data.equals(parent2Data)){
					JobData child1Data=child1.getVariableValue(i);
					JobData child2Data=child2.getVariableValue(j);
					for(int fn=0;fn<parent1Data.forbidenNodes.length;fn++){
						child1Data.forbidenNodes[fn]=cross*parent1Data.forbidenNodes[fn]+(1-cross)*parent2Data.forbidenNodes[fn];
						child2Data.forbidenNodes[fn]=cross*parent2Data.forbidenNodes[fn]+(1-cross)*parent1Data.forbidenNodes[fn];
					}
					child1.setVariableValue(i,child1Data);
					child2.setVariableValue(j,child2Data);
				}
			}
		}
	
		solutions.add(child1);
		solutions.add(child2);
		return solutions;
	}
}
