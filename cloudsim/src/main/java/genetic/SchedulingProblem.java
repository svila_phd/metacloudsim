package genetic;


import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;

import org.uma.jmetal.problem.impl.AbstractGenericProblem;
import org.uma.jmetal.solution.PermutationSolution;


public abstract class SchedulingProblem extends AbstractGenericProblem<SchedulingSolution> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2874411385013627221L;
	protected JobList jobs;
	protected MultiCluster mc;
	
	public SchedulingProblem(JobList jobs,MultiCluster mc){
		this.jobs=jobs;
		this.mc=mc;
	}
	
	public SchedulingSolution createSolution() {
		return new SchedulingSolution(this);
	}
	public SchedulingSolution createSolutionOrdered(){
		return new SchedulingSolution(this,true);
	}

	public int getPermutationLength() {
		return jobs.size();
	}
	
	public int getMultiClusterLength() {
		return mc.size();
	}
	
	public JobList getJobList(){
		return jobs;
	}
}
