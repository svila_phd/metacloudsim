package genetic;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAII;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.evaluator.SolutionListEvaluator;

public class NSGALog extends NSGAII<SchedulingSolution> {
	long time;
	PrintWriter writer;
	public NSGALog(Problem problem, int maxIterations, int populationSize,
			CrossoverOperator crossoverOperator,
			MutationOperator mutationOperator,
			SelectionOperator selectionOperator, SolutionListEvaluator evaluator,String logfile) {
		super(problem, maxIterations, populationSize, crossoverOperator,
				mutationOperator, selectionOperator, evaluator);
		time=System.currentTimeMillis();
		try {
			writer = new PrintWriter(logfile, "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void initProgress(){
		super.initProgress();
		List<SchedulingSolution> l=this.getNonDominatedSolutions(getResult());
		double bigestm=l.get(0).getObjective(1);
		for(SchedulingSolution ss:l){
			if(bigestm>ss.getObjective(0)){
				bigestm=ss.getObjective(0);
			}
		}
		double bigeste=l.get(0).getObjective(1);
		for(SchedulingSolution ss:l){
			if(bigeste>ss.getObjective(1)){
				bigeste=ss.getObjective(1);
			}
		}
		System.out.println(bigestm);
		writer.println(bigestm+";"+bigeste+";"+(float)(System.currentTimeMillis()-this.time)/(float)1000);
		writer.flush();
	}
	@Override
	public void updateProgress(){
		super.updateProgress();
		if(this.iterations%1==0){
			List<SchedulingSolution> l=this.getNonDominatedSolutions(getResult());
			double bigestm=l.get(0).getObjective(1);
			for(SchedulingSolution ss:l){
				if(bigestm>ss.getObjective(0)){
					bigestm=ss.getObjective(0);
				}
			}
			double bigeste=l.get(0).getObjective(1);
			for(SchedulingSolution ss:l){
				if(bigeste>ss.getObjective(1)){
					bigeste=ss.getObjective(1);
				}
			}
			System.out.println(bigestm);
			writer.println(bigestm+";"+bigeste+";"+(float)(System.currentTimeMillis()-this.time)/(float)1000);
			writer.flush();
		}
	}
}
