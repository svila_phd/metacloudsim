package genetic;


import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;

import org.uma.jmetal.problem.impl.AbstractGenericProblem;
import org.uma.jmetal.solution.PermutationSolution;


public  class SchedulingProblemMultiObjective extends SchedulingProblem {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2874411385013627221L;
	
	public SchedulingProblemMultiObjective(JobList jobs,MultiCluster mc){
		super(jobs, mc);
		this.setName("schedulingProblem");
		this.setNumberOfObjectives(2);
		this.setNumberOfVariables(jobs.size());
		
	}
	
	public void evaluate(SchedulingSolution solution) {
		try{
			mc.reset();
			for(int i=0;i<getNumberOfVariables();i++){
				JobData j = solution.getVariableValue(i);
				Integer[] alloc=mc.getMESDAllocForbiden(j.job, j.forbidenNodes);
				while(!mc.sheduleWithAllocation(j.job,alloc)){
					mc.execute();
					alloc=mc.getMESDAllocForbiden(j.job, j.forbidenNodes);
				}
			}
			solution.setObjective(0,mc.finishExecution());
			solution.setObjective(1,mc.getEnergyConsumition());
		}catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
		
		
	}

}
