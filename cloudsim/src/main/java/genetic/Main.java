package genetic;

import geneticsimulator.BadAllocationException;
import geneticsimulator.Cluster;
import geneticsimulator.Job;
import geneticsimulator.JobList;
import geneticsimulator.MultiCluster;

import java.util.ArrayList;

import org.uma.jmetal.algorithm.multiobjective.omopso.OMOPSO;
import org.uma.jmetal.operator.impl.mutation.NonUniformMutation;
import org.uma.jmetal.operator.impl.mutation.UniformMutation;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.evaluator.SolutionListEvaluator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;

import PSOMO2.OMPSOLog;
import PSOMO2.PSOProblemMultiObjective;

public class Main{
	public static void main(String[] args) throws Exception {
		//creem els jobs
		ArrayList<Job> jobs=new ArrayList<Job>();
		jobs.add(new Job(1,15,(double)500,(double)0.4,(double)0.9));
		jobs.add(new Job(2,15,(double)300,(double)0.8,(double)0.9));
		jobs.add(new Job(3,15,(double)200,(double)0.7,(double)0.9));
		jobs.add(new Job(4,7,(double)800,(double)0.6,(double)1));
		jobs.add(new Job(5,1,(double)200,(double)0.6,(double)0.4));
		jobs.add(new Job(6,13,(double)600,(double)0.8,(double)0.3));
		//jobs.add(new Job(7,40,(double)1000,(double)0.8,(double)0.4));
		jobs.add(new Job(8,10,(double)300,(double)0.7,(double)0.6));
		jobs.add(new Job(9,6,(double)800,(double)0.5,(double)0.5));
		jobs.add(new Job(10,15,(double)100,(double)0.6,(double)0.1));

		JobList.setInstance(jobs);
		//creem el multicluster
		ArrayList<Cluster> clusters=new ArrayList<Cluster>();
		clusters.add(new Cluster(1,10,1000,1,300,350,400,200));
		clusters.add(new Cluster(2,10,1000,1,300,350,400,200));
		clusters.add(new Cluster(3,10,1000,1,300,350,400,200));
		try {
			MultiCluster.setInstance(clusters,null);
		} catch (BadAllocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	    
	    PSOProblemMultiObjective problem;
	    problem = new PSOProblemMultiObjective(JobList.instance(),MultiCluster.instance());
	    SolutionListEvaluator<DoubleSolution> evaluator = new SequentialSolutionListEvaluator<DoubleSolution>();	
		UniformMutation uMutation= new UniformMutation(0.02,0.4);
		NonUniformMutation nuMutation = new NonUniformMutation(0.02,0.02,100);
		 
		OMOPSO algorithm=new OMPSOLog(problem, evaluator,100,100,10,uMutation,nuMutation,"convergence");
		 AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
		 .execute();
	    System.out.println();

	    

	   
	    //crossover = new JobCrossover() ;

	    //mutation = new JobSwapMutation();

	    //selection = new BinaryTournamentSelection<SchedulingSolution>(new RankingAndCrowdingDistanceComparator<SchedulingSolution>());

	    //algorithm = new NSGAIIBuilder<SchedulingSolution>(problem,crossover,mutation)
	    //        .setSelectionOperator(selection)
	    //        .setMaxIterations(500)
	    //        .setPopulationSize(100)
	    //        .build() ;
	    //algorithm=new SchedulingPSO(problem,20,20,0.3,0.2,0.8);
	    //AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
	     //       .execute() ;

	    //List<SchedulingSolution> population = algorithm.getResult() ;
	    //long computingTime = algorithmRunner.getComputingTime() ;

	    //new SolutionSetOutput.Printer(population)
	     //       .setSeparator("\t")
	       //     .setVarFileOutputContext(new DefaultFileOutputContext("VAR.tsv"))
	         //   .setFunFileOutputContext(new DefaultFileOutputContext("FUN.tsv"))
	           // .print();
	           
	    /*for(SchedulingSolution p :population){
		    System.out.println(p.getObjective(0));//+" "+p.getObjective(1));

	    }*/
	   // JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");
	  //  JMetalLogger.logger.info("Objectives values have been written to file FUN.tsv");
	    //JMetalLogger.logger.info("Variables values have been written to file VAR.tsv");
	  }
}
