package genetic;

import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

public class JobSwapMutation implements MutationOperator<SchedulingSolution>, org.uma.jmetal54.operator.MutationOperator<SchedulingSolution> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6311197190464734663L;

	private JMetalRandom randomGenerator ;

	  /** Constructor */
	public JobSwapMutation() {
	    randomGenerator = JMetalRandom.getInstance() ;
	}
	
	public SchedulingSolution execute(SchedulingSolution source) {
		int prob1 = randomGenerator.nextInt(0, 100);
		if(prob1<5){
			SchedulingSolution solution = new SchedulingSolution(source);
			int ptype = randomGenerator.nextInt(0, 100);
			if(ptype<50){
				int s1=randomGenerator.nextInt(0, source.getNumberOfVariables()-1);
				int s2=randomGenerator.nextInt(0, source.getNumberOfVariables()-1);
				JobData temp = solution.getVariableValue(s1);
				solution.setVariableValue(s1, source.getVariableValue(s2));
				solution.setVariableValue(s2, temp);
				solution.repairSolution();
			}else{
				for(int i=0;i<source.getNumberOfVariables();i++){
					int pmut=randomGenerator.nextInt(0, 100);
					if(pmut>50){
						JobData job=solution.getVariableValue(i);
						for(int j=0;j<job.forbidenNodes.length;j++){
							job.forbidenNodes[j]=randomGenerator.nextDouble(0, 1);
						}	
						solution.setVariableValue(i, job);
					}
				}
				solution.repairSolution();
			}
			return solution;
		}else{
			return source;
		}
	}
}
