package org.uma.jmetal54.algorithm.multiobjective.abyss.util;

import org.uma.jmetal54.solution.Solution;
import org.uma.jmetal54.util.solutionattribute.impl.GenericSolutionAttribute;

/**
 * Created by cbarba on 24/3/15.
 */
@SuppressWarnings("serial")
public class MarkAttribute extends GenericSolutionAttribute<Solution<?>,Boolean> {
}
