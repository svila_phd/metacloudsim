package org.uma.jmetal54.algorithm.multiobjective.mombi.util;

import org.uma.jmetal54.solution.Solution;
import org.uma.jmetal54.util.solutionattribute.impl.GenericSolutionAttribute;

/**
 * Created by ajnebro on 10/9/15.
 */
@SuppressWarnings("serial")
public class R2RankingAttribute<T extends Solution<?>> extends GenericSolutionAttribute<T, R2SolutionData> {
}