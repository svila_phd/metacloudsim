



//




//



package org.uma.jmetal54.experiment;

import org.uma.jmetal54.algorithm.Algorithm;
import org.uma.jmetal54.algorithm.multiobjective.mocell.MOCellBuilder;
import org.uma.jmetal54.algorithm.multiobjective.mochc.MOCHCBuilder;
import org.uma.jmetal54.algorithm.multiobjective.nsgaii.NSGAIIBuilder;
import org.uma.jmetal54.algorithm.multiobjective.spea2.SPEA2Builder;
import org.uma.jmetal54.operator.CrossoverOperator;
import org.uma.jmetal54.operator.MutationOperator;
import org.uma.jmetal54.operator.SelectionOperator;
import org.uma.jmetal54.operator.impl.crossover.HUXCrossover;
import org.uma.jmetal54.operator.impl.crossover.SinglePointCrossover;
import org.uma.jmetal54.operator.impl.mutation.BitFlipMutation;
import org.uma.jmetal54.operator.impl.selection.RandomSelection;
import org.uma.jmetal54.operator.impl.selection.RankingAndCrowdingSelection;
import org.uma.jmetal54.problem.BinaryProblem;
import org.uma.jmetal54.problem.Problem;
import org.uma.jmetal54.problem.multiobjective.OneZeroMax;
import org.uma.jmetal54.problem.multiobjective.zdt.ZDT5;
import org.uma.jmetal54.qualityindicator.impl.Epsilon;
import org.uma.jmetal54.qualityindicator.impl.GenerationalDistance;
import org.uma.jmetal54.qualityindicator.impl.InvertedGenerationalDistance;
import org.uma.jmetal54.qualityindicator.impl.InvertedGenerationalDistancePlus;
import org.uma.jmetal54.qualityindicator.impl.Spread;
import org.uma.jmetal54.qualityindicator.impl.hypervolume.PISAHypervolume;
import org.uma.jmetal54.solution.BinarySolution;
import org.uma.jmetal54.util.JMetalException;
import org.uma.jmetal54.util.evaluator.impl.SequentialSolutionListEvaluator;
import org.uma.jmetal54.util.experiment.Experiment;
import org.uma.jmetal54.util.experiment.ExperimentBuilder;
import org.uma.jmetal54.util.experiment.component.ComputeQualityIndicators;
import org.uma.jmetal54.util.experiment.component.ExecuteAlgorithms;
import org.uma.jmetal54.util.experiment.component.GenerateBoxplotsWithR;
import org.uma.jmetal54.util.experiment.component.GenerateFriedmanTestTables;
import org.uma.jmetal54.util.experiment.component.GenerateLatexTablesWithStatistics;
import org.uma.jmetal54.util.experiment.component.GenerateReferenceParetoFront;
import org.uma.jmetal54.util.experiment.component.GenerateWilcoxonTestTablesWithR;
import org.uma.jmetal54.util.experiment.util.ExperimentAlgorithm;
import org.uma.jmetal54.util.experiment.util.ExperimentProblem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Example of experimental study based on solving two binary problems with four algorithms: NSGAII,
 * SPEA2, MOCell, and MOCHC
 *
 * This experiment assumes that the reference Pareto front are not known, so the names of files
 * containing them and the directory where they are located must be specified.
 *
 * Six quality indicators are used for performance assessment.
 *
 * The steps to carry out the experiment are: 1. Configure the experiment 2. Execute the algorithms
 * 3. Generate the reference Pareto fronts 4. Compute que quality indicators 5. Generate Latex
 * tables reporting means and medians 6. Generate Latex tables with the result of applying the
 * Wilcoxon Rank Sum Test 7. Generate Latex tables with the ranking obtained by applying the
 * Friedman test 8. Generate R scripts to obtain boxplots
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
public class BinaryProblemsStudy {
  private static final int INDEPENDENT_RUNS = 25;

  public static void main(String[] args) throws IOException {
    if (args.length != 1) {
      throw new JMetalException("Needed arguments: experimentBaseDirectory");
    }
    String experimentBaseDirectory = args[0];

    List<ExperimentProblem<BinarySolution>> problemList = new ArrayList<>();
    problemList.add(new ExperimentProblem<>(new ZDT5()));
    problemList.add(new ExperimentProblem<>(new OneZeroMax(512)));

    List<ExperimentAlgorithm<BinarySolution, List<BinarySolution>>> algorithmList =
            configureAlgorithmList(problemList);

    Experiment<BinarySolution, List<BinarySolution>> experiment;
    experiment = new ExperimentBuilder<BinarySolution, List<BinarySolution>>("BinaryProblemsStudy")
            .setAlgorithmList(algorithmList)
            .setProblemList(problemList)
            .setExperimentBaseDirectory(experimentBaseDirectory)
            .setOutputParetoFrontFileName("FUN")
            .setOutputParetoSetFileName("VAR")
            .setReferenceFrontDirectory(experimentBaseDirectory+"/referenceFronts")
            .setIndicatorList(Arrays.asList(
                    new Epsilon<BinarySolution>(), new Spread<BinarySolution>(), new GenerationalDistance<BinarySolution>(),
                    new PISAHypervolume<BinarySolution>(),
                    new InvertedGenerationalDistance<BinarySolution>(),
                    new InvertedGenerationalDistancePlus<BinarySolution>())
            )
            .setIndependentRuns(INDEPENDENT_RUNS)
            .setNumberOfCores(8)
            .build();

    new ExecuteAlgorithms<>(experiment).run();
    new GenerateReferenceParetoFront(experiment).run();
    new ComputeQualityIndicators<>(experiment).run();
    new GenerateLatexTablesWithStatistics(experiment).run();
    new GenerateWilcoxonTestTablesWithR<>(experiment).run();
    new GenerateFriedmanTestTables<>(experiment).run();
    new GenerateBoxplotsWithR<>(experiment).setRows(1).setColumns(2).setDisplayNotch().run();

  }

  /**
   * The algorithm list is composed of pairs {@link Algorithm} + {@link Problem} which form part of
   * a {@link ExperimentAlgorithm}, which is a decorator for class {@link Algorithm}.
   */

  static List<ExperimentAlgorithm<BinarySolution, List<BinarySolution>>> configureAlgorithmList(
          List<ExperimentProblem<BinarySolution>> problemList) {
    List<ExperimentAlgorithm<BinarySolution, List<BinarySolution>>> algorithms = new ArrayList<>();

    for (int i = 0; i < problemList.size(); i++) {
      Algorithm<List<BinarySolution>> algorithm = new NSGAIIBuilder<BinarySolution>(
              problemList.get(i).getProblem(),
              new SinglePointCrossover(1.0),
              new BitFlipMutation(1.0 / ((BinaryProblem) problemList.get(i).getProblem()).getNumberOfBits(0)))
              .setMaxEvaluations(25000)
              .setPopulationSize(100)
              .build();
      algorithms.add(new ExperimentAlgorithm<>(algorithm, problemList.get(i).getTag()));
    }

    for (int i = 0; i < problemList.size(); i++) {
      Algorithm<List<BinarySolution>> algorithm = new SPEA2Builder<BinarySolution>(
              problemList.get(i).getProblem(),
              new SinglePointCrossover(1.0),
              new BitFlipMutation(1.0 / ((BinaryProblem) problemList.get(i).getProblem()).getNumberOfBits(0)))
              .setMaxIterations(250)
              .setPopulationSize(100)
              .build();
      algorithms.add(new ExperimentAlgorithm<>(algorithm, problemList.get(i).getTag()));
    }

    for (int i = 0; i < problemList.size(); i++) {
      Algorithm<List<BinarySolution>> algorithm = new MOCellBuilder<BinarySolution>(
              problemList.get(i).getProblem(),
              new SinglePointCrossover(1.0),
              new BitFlipMutation(1.0 / ((BinaryProblem) problemList.get(i).getProblem()).getNumberOfBits(0)))
              .setMaxEvaluations(25000)
              .setPopulationSize(100)
              .build();
      algorithms.add(new ExperimentAlgorithm<>(algorithm, problemList.get(i).getTag()));
    }

    for (int i = 0; i < problemList.size(); i++) {
      CrossoverOperator<BinarySolution> crossoverOperator;
      MutationOperator<BinarySolution> mutationOperator;
      SelectionOperator<List<BinarySolution>, BinarySolution> parentsSelection;
      SelectionOperator<List<BinarySolution>, List<BinarySolution>> newGenerationSelection;

      crossoverOperator = new HUXCrossover(1.0);
      parentsSelection = new RandomSelection<BinarySolution>();
      newGenerationSelection = new RankingAndCrowdingSelection<BinarySolution>(100);
      mutationOperator = new BitFlipMutation(0.35);
      Algorithm<List<BinarySolution>> algorithm = new MOCHCBuilder(
              (BinaryProblem) problemList.get(i).getProblem())
              .setInitialConvergenceCount(0.25)
              .setConvergenceValue(3)
              .setPreservedPopulation(0.05)
              .setPopulationSize(100)
              .setMaxEvaluations(25000)
              .setCrossover(crossoverOperator)
              .setNewGenerationSelection(newGenerationSelection)
              .setCataclysmicMutation(mutationOperator)
              .setParentSelection(parentsSelection)
              .setEvaluator(new SequentialSolutionListEvaluator<BinarySolution>())
              .build();
      algorithms.add(new ExperimentAlgorithm<>(algorithm, problemList.get(i).getTag()));
    }

    return algorithms;
  }
}
