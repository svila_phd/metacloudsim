package org.uma.jmetal54.runner.multiobjective;

import org.uma.jmetal54.algorithm.Algorithm;
import org.uma.jmetal54.algorithm.multiobjective.nsgaii.NSGAIIBuilder;
import org.uma.jmetal54.operator.CrossoverOperator;
import org.uma.jmetal54.operator.MutationOperator;
import org.uma.jmetal54.operator.SelectionOperator;
import org.uma.jmetal54.operator.impl.crossover.SBXCrossover;
import org.uma.jmetal54.operator.impl.mutation.PolynomialMutation;
import org.uma.jmetal54.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal54.problem.Problem;
import org.uma.jmetal54.problem.multiobjective.ebes.Ebes;
import org.uma.jmetal54.runner.AbstractAlgorithmRunner;
import org.uma.jmetal54.solution.DoubleSolution;
import org.uma.jmetal54.util.AlgorithmRunner;
import org.uma.jmetal54.util.JMetalException;
import org.uma.jmetal54.util.JMetalLogger;
import org.uma.jmetal54.util.comparator.RankingAndCrowdingDistanceComparator;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Class to configure and run the NSGA-II algorithm to solve the Ebes problem
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
public class NSGAIIEbesRunner extends AbstractAlgorithmRunner {
  /**
   * @param args Command line arguments.
   * @throws JMetalException
   * @throws FileNotFoundException
   * Invoking command:
    java org.uma.jmetal54.runner.multiobjective.NSGAIIRunner problemName [referenceFront]
   */
  public static void main(String[] args) throws JMetalException, FileNotFoundException {
    Problem<DoubleSolution> problem;
    Algorithm<List<DoubleSolution>> algorithm;
    CrossoverOperator<DoubleSolution> crossover;
    MutationOperator<DoubleSolution> mutation;
    SelectionOperator<List<DoubleSolution>, DoubleSolution> selection;
    String referenceParetoFront = "" ;

    problem = new Ebes("ebes/Mobile_Bridge_25N_35B_8G_16OrdZXY.ebe", new String[]{"W", "D", "ENS"}) ;

    double crossoverProbability = 0.9 ;
    double crossoverDistributionIndex = 20.0 ;
    crossover = new SBXCrossover(crossoverProbability, crossoverDistributionIndex) ;

    double mutationProbability = 1.0 / problem.getNumberOfVariables() ;
    double mutationDistributionIndex = 20.0 ;
    mutation = new PolynomialMutation(mutationProbability, mutationDistributionIndex) ;

    selection = new BinaryTournamentSelection<DoubleSolution>(new RankingAndCrowdingDistanceComparator<DoubleSolution>());

    algorithm = new NSGAIIBuilder<DoubleSolution>(problem, crossover, mutation)
        .setSelectionOperator(selection)
        .setMaxEvaluations(25000)
        .setPopulationSize(100)
        .build() ;

    AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
        .execute() ;

    List<DoubleSolution> population = algorithm.getResult() ;
    long computingTime = algorithmRunner.getComputingTime() ;

    JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");

    printFinalSolutionSet(population);
    if (!referenceParetoFront.equals("")) {
      printQualityIndicators(population, referenceParetoFront) ;
    }
  }
}
