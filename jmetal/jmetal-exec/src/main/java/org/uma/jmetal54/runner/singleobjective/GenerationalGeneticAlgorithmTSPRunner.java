



//




// 



package org.uma.jmetal54.runner.singleobjective;

import org.uma.jmetal54.algorithm.Algorithm;
import org.uma.jmetal54.algorithm.singleobjective.geneticalgorithm.GeneticAlgorithmBuilder;
import org.uma.jmetal54.operator.CrossoverOperator;
import org.uma.jmetal54.operator.MutationOperator;
import org.uma.jmetal54.operator.SelectionOperator;
import org.uma.jmetal54.operator.impl.crossover.PMXCrossover;
import org.uma.jmetal54.operator.impl.mutation.PermutationSwapMutation;
import org.uma.jmetal54.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal54.problem.PermutationProblem;
import org.uma.jmetal54.problem.singleobjective.TSP;
import org.uma.jmetal54.solution.PermutationSolution;
import org.uma.jmetal54.util.AlgorithmRunner;
import org.uma.jmetal54.util.JMetalLogger;
import org.uma.jmetal54.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal54.util.fileoutput.SolutionListOutput;
import org.uma.jmetal54.util.fileoutput.impl.DefaultFileOutputContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to configure and run a generational genetic algorithm. The target problem is OneMax.
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
public class GenerationalGeneticAlgorithmTSPRunner {
  /**
   * Usage: java org.uma.jmetal54.runner.singleobjective.BinaryGenerationalGeneticAlgorithmRunner
   */
  public static void main(String[] args) throws Exception {
    PermutationProblem<PermutationSolution<Integer>> problem;
    Algorithm<PermutationSolution<Integer>> algorithm;
    CrossoverOperator<PermutationSolution<Integer>> crossover;
    MutationOperator<PermutationSolution<Integer>> mutation;
    SelectionOperator<List<PermutationSolution<Integer>>, PermutationSolution<Integer>> selection;

    problem = new TSP("/tspInstances/kroA100.tsp");

    crossover = new PMXCrossover(0.9) ;

    double mutationProbability = 1.0 / problem.getNumberOfVariables() ;
    mutation = new PermutationSwapMutation<Integer>(mutationProbability) ;

    selection = new BinaryTournamentSelection<PermutationSolution<Integer>>(new RankingAndCrowdingDistanceComparator<PermutationSolution<Integer>>());

    algorithm = new GeneticAlgorithmBuilder<>(problem, crossover, mutation)
            .setPopulationSize(100)
            .setMaxEvaluations(250000)
            .setSelectionOperator(selection)
            .build() ;

    AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
            .execute() ;

    PermutationSolution<Integer> solution = algorithm.getResult() ;
    List<PermutationSolution<Integer>> population = new ArrayList<>(1) ;
    population.add(solution) ;

    long computingTime = algorithmRunner.getComputingTime() ;

    new SolutionListOutput(population)
            .setSeparator("\t")
            .setVarFileOutputContext(new DefaultFileOutputContext("VAR.tsv"))
            .setFunFileOutputContext(new DefaultFileOutputContext("FUN.tsv"))
            .print();

    JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");
    JMetalLogger.logger.info("Objectives values have been written to file FUN.tsv");
    JMetalLogger.logger.info("Variables values have been written to file VAR.tsv");

  }
}
