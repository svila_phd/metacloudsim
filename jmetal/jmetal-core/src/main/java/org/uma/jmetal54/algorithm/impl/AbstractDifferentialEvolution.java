package org.uma.jmetal54.algorithm.impl;

import org.uma.jmetal54.operator.impl.crossover.DifferentialEvolutionCrossover;
import org.uma.jmetal54.operator.impl.selection.DifferentialEvolutionSelection;
import org.uma.jmetal54.solution.DoubleSolution;

/**
 * Abstract class representing differential evolution (DE) algorithms
 *
 * @author Antonio J. Nebro
 * @version 1.0
 */
@SuppressWarnings("serial")
public abstract class AbstractDifferentialEvolution<Result> extends AbstractEvolutionaryAlgorithm<DoubleSolution, Result> {
  protected DifferentialEvolutionCrossover crossoverOperator ;
  protected DifferentialEvolutionSelection selectionOperator ;
}
