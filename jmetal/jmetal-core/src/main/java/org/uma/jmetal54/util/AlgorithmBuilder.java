package org.uma.jmetal54.util;

import org.uma.jmetal54.algorithm.Algorithm;

/**
 * Interface representing algorithm builders
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
public interface AlgorithmBuilder<A extends Algorithm<?>> {
  public A build() ;
}
