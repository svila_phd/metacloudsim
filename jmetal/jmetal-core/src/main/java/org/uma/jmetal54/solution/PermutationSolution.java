package org.uma.jmetal54.solution;

/**
 * Interface representing permutation based solutions
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
public interface PermutationSolution<T> extends Solution<T> {
}
