package org.uma.jmetal54.util.solutionattribute.impl;

import org.uma.jmetal54.solution.Solution;

/**
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
@SuppressWarnings("serial")
public class HypervolumeContributionAttribute<S extends Solution<?>>
    extends GenericSolutionAttribute<S, Double>  {
}
