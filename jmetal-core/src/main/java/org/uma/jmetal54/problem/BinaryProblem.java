package org.uma.jmetal54.problem;

import org.uma.jmetal54.solution.BinarySolution;

/**
 * Interface representing binary problems
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */
public interface BinaryProblem extends Problem<BinarySolution> {
  public int getNumberOfBits(int index) ;
  public int getTotalNumberOfBits() ;
}
