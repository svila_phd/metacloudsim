package org.uma.jmetal54.problem.impl;

import org.uma.jmetal54.problem.PermutationProblem;
import org.uma.jmetal54.solution.PermutationSolution;
import org.uma.jmetal54.solution.impl.DefaultIntegerPermutationSolution;

@SuppressWarnings("serial")
public abstract class AbstractIntegerPermutationProblem
    extends AbstractGenericProblem<PermutationSolution<Integer>> implements
    PermutationProblem<PermutationSolution<Integer>> {

  /* Getters */

  /* Setters */

  @Override
  public PermutationSolution<Integer> createSolution() {
    return new DefaultIntegerPermutationSolution(this) ;
  }
}
