package org.uma.jmetal54.runner.multiobjective;

import org.uma.jmetal54.algorithm.Algorithm;
import org.uma.jmetal54.algorithm.multiobjective.wasfga.WASFGA;
import org.uma.jmetal54.operator.CrossoverOperator;
import org.uma.jmetal54.operator.MutationOperator;
import org.uma.jmetal54.operator.SelectionOperator;
import org.uma.jmetal54.operator.impl.crossover.SinglePointCrossover;
import org.uma.jmetal54.operator.impl.mutation.BitFlipMutation;
import org.uma.jmetal54.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal54.problem.BinaryProblem;
import org.uma.jmetal54.runner.AbstractAlgorithmRunner;
import org.uma.jmetal54.solution.BinarySolution;
import org.uma.jmetal54.util.AlgorithmRunner;
import org.uma.jmetal54.util.JMetalException;
import org.uma.jmetal54.util.JMetalLogger;
import org.uma.jmetal54.util.ProblemUtils;
import org.uma.jmetal54.util.evaluator.impl.SequentialSolutionListEvaluator;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class WASFGABinaryRunner extends AbstractAlgorithmRunner {
  /**
   * @param args Command line arguments.
   * @throws JMetalException
   * @throws FileNotFoundException
   * Invoking command:
  java org.uma.jmetal54.runner.multiobjective.WASFGARunner problemName [referenceFront]
   */
  public static void main(String[] args) throws JMetalException, FileNotFoundException {
    BinaryProblem problem;
    Algorithm<List<BinarySolution>> algorithm;
    CrossoverOperator<BinarySolution> crossover;
    MutationOperator<BinarySolution> mutation;
    SelectionOperator<List<BinarySolution>, BinarySolution> selection;
    String referenceParetoFront = "" ;
    List<Double> referencePoint = null;

    String problemName ;
    if (args.length == 1) {
      problemName = args[0];
    } else if (args.length == 2) {
      problemName = args[0] ;
      referenceParetoFront = args[1] ;
    } else {
      problemName = "org.uma.jmetal54.problem.multiobjective.zdt.ZDT5";
    }

    problem = (BinaryProblem) ProblemUtils.<BinarySolution> loadProblem(problemName);
    
    referencePoint = new ArrayList<>();
    referencePoint.add(10.0);
    referencePoint.add(4.0);

    double crossoverProbability = 0.9 ;
    crossover = new SinglePointCrossover(crossoverProbability) ;

    double mutationProbability = 1.0 / problem.getNumberOfBits(0) ;
    mutation = new BitFlipMutation(mutationProbability) ;

    selection = new BinaryTournamentSelection<BinarySolution>() ;

    algorithm = new WASFGA<BinarySolution>(problem, 100, 250, crossover, mutation, selection,new SequentialSolutionListEvaluator<BinarySolution>(),referencePoint) ;

    AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
            .execute() ;

    List<BinarySolution> population = algorithm.getResult() ;
    long computingTime = algorithmRunner.getComputingTime() ;

    JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");

    printFinalSolutionSet(population);
    if (!referenceParetoFront.equals("")) {
      printQualityIndicators(population, referenceParetoFront) ;
    }
  }
}
