package org.uma.jmetal54.runner.multiobjective;

import org.uma.jmetal54.algorithm.Algorithm;
import org.uma.jmetal54.algorithm.multiobjective.omopso.OMOPSOBuilder;
import org.uma.jmetal54.operator.impl.mutation.NonUniformMutation;
import org.uma.jmetal54.operator.impl.mutation.UniformMutation;
import org.uma.jmetal54.problem.DoubleProblem;
import org.uma.jmetal54.runner.AbstractAlgorithmRunner;
import org.uma.jmetal54.solution.DoubleSolution;
import org.uma.jmetal54.util.AlgorithmRunner;
import org.uma.jmetal54.util.JMetalLogger;
import org.uma.jmetal54.util.ProblemUtils;
import org.uma.jmetal54.util.evaluator.impl.SequentialSolutionListEvaluator;

import java.util.List;

/**
 * Class for configuring and running the OMOPSO algorithm
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */

public class OMOPSORunner extends AbstractAlgorithmRunner {
  /**
   * @param args Command line arguments.
   * @throws org.uma.jmetal54.util.JMetalException
   * @throws java.io.IOException
   * @throws SecurityException
   * Invoking command:
  java org.uma.jmetal54.runner.multiobjective.OMOPSORunner problemName [referenceFront]
   */
  public static void main(String[] args) throws Exception {
    DoubleProblem problem;
    Algorithm<List<DoubleSolution>> algorithm;

    String referenceParetoFront = "" ;

    String problemName ;
    if (args.length == 1) {
      problemName = args[0];
    } else if (args.length == 2) {
      problemName = args[0] ;
      referenceParetoFront = args[1] ;
    } else {
      problemName = "org.uma.jmetal54.problem.multiobjective.zdt.ZDT1";
      referenceParetoFront = "jmetal-problem/src/test/resources/pareto_fronts/ZDT1.pf" ;
    }

    problem = (DoubleProblem) ProblemUtils.<DoubleSolution> loadProblem(problemName);

    double mutationProbability = 1.0 / problem.getNumberOfVariables() ;

    algorithm = new OMOPSOBuilder(problem, new SequentialSolutionListEvaluator<>())
        .setMaxIterations(250)
        .setSwarmSize(100)
        .setUniformMutation(new UniformMutation(mutationProbability, 0.5))
        .setNonUniformMutation(new NonUniformMutation(mutationProbability, 0.5, 250))
        .build();

    AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
        .execute();

    List<DoubleSolution> population = algorithm.getResult();
    long computingTime = algorithmRunner.getComputingTime();

    JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");

    printFinalSolutionSet(population);
    if (!referenceParetoFront.equals("")) {
      printQualityIndicators(population, referenceParetoFront) ;
    }
  }
}
